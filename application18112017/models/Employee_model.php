<?php

Class Employee_model extends CI_Model {

    public function empPersonalInfo($emp_id)
    {
        $row = $this->db->query("select a.*, b.LKP_NAME as LKP_MARITAL_STATUS , c.NATIONALITY as LKP_NATIONALITY,
                        d.LKP_NAME as LKP_RELIGION, e.DEPT_NAME from hr_emp a 
                        left join m00_lkpdata b on a.MARITAL_STATUS=b.LKP_ID 
                        left join country c on a.NATIONALITY=c.id
                        left join m00_lkpdata d on a.RELIGION=d.LKP_ID
                        left join ins_dept e on a.DEPT_ID=e.DEPT_ID
                        WHERE a.EMP_ID = '$emp_id'")->row();
        //echo "<pre>"; print_r($row); exit; echo "</pre>";
        return $row;
    }
    public function empInfo()
    {
        $row = $this->db->query("select a.*,c.DEPT_NAME,d.DESIGNATION from hr_emp a 
                                    left join hr_edeptdesi b on a.EMP_ID = b.EMP_ID
                                    left join ins_dept c on b.DEPT_ID=c.DEPT_ID
                                    left join hr_desig d on b.DESIG_ID = d.DESIG_ID group by a.EMP_ID")->result();
        //echo "<pre>"; print_r($row); exit; echo "</pre>";
        return $row;
    }

    public function findEmpById($emp_id)
    {
        $row = $this->db->query("select a.*,b.DEPT_ID as department_id, b.DESIG_ID as designation_id, b.DEFAULT_FG, c.DEPT_NAME,d.DESIGNATION from hr_emp a 
                                    left join hr_edeptdesi b on a.EMP_ID = b.EMP_ID
                                    left join ins_dept c on b.DEPT_ID=c.DEPT_ID
                                    left join hr_desig d on b.DESIG_ID = d.DESIG_ID WHERE a.EMP_ID = $emp_id")->row();
        //echo "<pre>"; print_r($row); exit; echo "</pre>";
        return $row;
    }
}