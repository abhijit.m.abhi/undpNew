<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User_google extends CI_Model{
	function __construct() {
		$this->tableName = 'users';
		$this->primaryKey = 'userId';
	}
	public function checkUser($data = array()){
		$this->db->select($this->primaryKey);
		$this->db->from($this->tableName);
		$this->db->where(array('login_method'=>$data['login_method'],'gmail_id'=>$data['gmail_id']));
		$query = $this->db->get();
		$check = $query->num_rows();
		
		if($check > 0){
			$result = $query->row_array();
			$data['updated_at'] = date("Y-m-d H:i:s");
			$update = $this->db->update($this->tableName,$data,array('userId'=>$result['userId']));
			$userID = $result['userId'];
		}else{
			$data['created_at'] = date("Y-m-d H:i:s");
			$data['updated_at'] = date("Y-m-d H:i:s");
			$insert = $this->db->insert($this->tableName,$data);
			$userID = $this->db->insert_id();
		}

		return $userID?$userID:false;
    }
}
