<?php

Class Blog_model extends CI_Model
{
    /**
     * @methodName
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    ########################################### ********* Start ************ #########################################


    public function getAllBlogPosts($limit=0, $offset=0)
    {
        return $this->db->query("SELECT bg.*, usr.fname author, usr.userId author_id, lst.title organization_name, ut.name url_type, ut.value url_data FROM blog as bg 
                                  LEFT JOIN listing as lst on lst.id = bg.organization_id 
                                  LEFT JOIN users as usr on usr.userId = lst.userId 
                                  LEFT JOIN url_type as ut ON ut.id = bg.url_type_id
                                  ORDER BY bg.post_date DESC LIMIT $limit OFFSET $offset")->result();
    }

    public function increaseView($blog_id)
    {
        $this->db->query("UPDATE blog SET view_count = view_count+1 WHERE id = $blog_id");
    }

    public function getBlogData($blog_id)
    {
        return $this->db->query("SELECT bg.*, ut.name url_type, ut.value url_data, ut.meta_tag meta_data  FROM blog as bg 
                                    LEFT JOIN url_type as ut ON ut.id = bg.url_type_id 
                                    WHERE bg.id = $blog_id")->row();
    }

    public function getOrganizaionData($organization_id)
    {
        return $this->db->query("SELECT listing.*, users.email as org_email, users.fname as author, users.userId as author_id FROM listing 
                                  LEFT JOIN users ON users.userId = listing.userId
                                  WHERE id = $organization_id ")->row();
    }

    public function organizationLogo($user_id)
    {
        return $this->db->query("SELECT image FROM users 
                                    WHERE userId = $user_id ")->row();
    }

    public function getBlogComment($blog_id)
    {
        return $this->db->query("SElECT bc.*, us.fname as author_name, us.image author_image, ls.title as org_name FROM blog_comment as bc 
                                  JOIN users as us ON bc.user_id = us.userId 
                                  LEFT JOIN listing ls ON ls.userId = us.userId 
                                  WHERE bc.blog_id = $blog_id ORDER  BY bc.id ASC")->result();
    }

    public function updateInterest($blog_type, $user_id, $blog_id)
    {
        $count = $this->db->query("SELECT * FROM blog_interest WHERE blog_id= $blog_id AND user_id = $user_id")->num_rows();

        if($count == 0)
        {
            if($this->db->query("INSERT INTO blog_interest (type, blog_id, user_id) VALUES ($blog_type , $blog_id , $user_id)"))
            {
                return array('ack' => true);
            }
        }
        else
        {
            if($this->db->query("UPDATE blog_interest SET type = $blog_type WHERE blog_id = $blog_id  AND user_id = $user_id"))
            {
                return array('ack' => true);
            }
        }
    }

    public function getBlogInterest($blogID)
    {
        return $this->db->query("SElECT type, count(type) interest_count FROM blog_interest WHERE blog_id='$blogID' GROUP BY type")->result();
    }

    public function getBlogInterest2($blogID, $userID)
    {
        return $this->db->query("SElECT type, count(type) interest_count FROM blog_interest WHERE blog_id='$blogID' AND user_id='$userID' GROUP BY type")->result();
    }

    public function deleteUpdateInterest($blog_type, $user_id, $control_id)
    {
        if($this->db->delete('blog_interest', array('user_id' => $user_id, 'blog_id' => $control_id, 'type' => $blog_type)))
        {
            return array('ack' => true);
        }
    }

    public function getBlog($user_id)
    {
        return $this->db->query("SELECT bg.*, org.title as org_name, ut.name url_type, ut.value url_data FROM blog as bg 
                                  INNER JOIN listing as org ON org.id = bg.organization_id 
                                  LEFT JOIN url_type as ut ON ut.id = bg.url_type_id 
                                  WHERE org.userId = $user_id
                                  ORDER  BY bg.id DESC ")->result();
    }

    public function getOrganization($user_id)
    {
        return $this->db->query("SELECT * FROM listing
                                  WHERE userId = $user_id")->row();
    }

    public function urlTypes()
    {
        return $this->db->query("SElECT * FROM url_type")->result();
    }

    public function deletePost($blog_id)
    {
        if($this->db->delete('blog', array('id' => $blog_id)))
        {
            return "Deleted";
        }
    }

    public function addComment($description, $blog_id, $user_id)
    {
//        $dec = strip_tags(htmlspecialchars_decode(html_entity_decode($description)));
        $dec = htmlentities($description);

        if($this->db->query("INSERT INTO blog_comment (comment, blog_id, user_id, created_at) 
                          VALUES ('$dec', $blog_id, $user_id, NOW())"))
        {
            return "Inserted";
        }
    }

    public function deleteComment($comment_id, $user_id)
    {
        if($this->db->query("DELETE FROM blog_comment
                          WHERE id = $comment_id 
                          AND user_id = $user_id"))
        {
            return 'Deleted';
        }
    }

    public function searchPost($blog_type, $order, $search, $limit=0, $offset=0)
    {

        $srch = $this->db->escape_str($search);

        $sql = '';
        $type = '';

        if (!empty($order)) {
            switch ($order) {
                case ('desc') :
                    $sql = " ORDER BY bg.post_date desc";
                    break;
                case ('asc') :
                    $sql = " ORDER BY bg.post_date asc";
                    break;
                case ('today') :
                    $sql = " AND DATE(post_date) = '" . date('Y-m-d') . "' ORDER BY bg.post_date desc";
                    break;
                case ('week') :
                    $_str_before_7_days = strtotime("-7 day");
                    $before_7_days = date('Y-m-d H:i:s', $_str_before_7_days);

                    $_str_after_7_days = strtotime("+7 day");
                    $after_7_days = date('Y-m-d H:i:s', $_str_after_7_days);

                    $sql = " AND post_date BETWEEN ' $before_7_days' AND '$after_7_days' ORDER BY bg.post_date desc";
                    break;
                default :
                    $sql = " ORDER BY opp.post_date desc";
            }
        } else {
            $sql = " ORDER BY bg.post_date desc ";
        }

        if (!empty($blog_type)) {
            $type = " AND bg.type = '$blog_type' ";
        }

        $row_count = $this->db->query("SELECT bg.*, usr.fname author, usr.userId author_id, lst.title organization_name, ut.name url_type, ut.value url_data FROM blog as bg 
                                    LEFT JOIN listing as lst on lst.id = bg.organization_id 
                                    LEFT JOIN users as usr on usr.userId = lst.userId 
                                    LEFT JOIN url_type as ut ON ut.id = bg.url_type_id
                                    WHERE ( bg.title LIKE '%$srch%' OR lst.title LIKE '%$srch%')
                                   $type $sql")->result();

        $query = $this->db->query("SELECT bg.*, usr.fname author, usr.userId author_id, lst.title organization_name, ut.name url_type, ut.value url_data FROM blog as bg 
                                    LEFT JOIN listing as lst on lst.id = bg.organization_id 
                                    LEFT JOIN users as usr on usr.userId = lst.userId 
                                    LEFT JOIN url_type as ut ON ut.id = bg.url_type_id
                                    WHERE ( bg.title LIKE '%$srch%' OR lst.title LIKE '%$srch%')
                                   $type $sql LIMIT $limit OFFSET $offset")->result();

        $num_rows = count($row_count);

        $this->session->set_userdata('blog_row_count', array(
            'num_rows' => $num_rows,
        ));

        return $query;
    }

    function count_items(){
        return $this->db->count_all('blog');
    }

    public function deleteBlogImage($blog_id, $image_name)
    {
        $result = $this->db->query("SELECT * FROM blog WHERE id = $blog_id")->row();

        $imgPath = 'upload/blogs/';

        if (strpos($result->images, $image_name) !== false) {
            $key = array($image_name . '___', '___' . $image_name, $image_name);
            $value = array('', '', '');
            $images = str_replace($key, $value, $result->images, $count);

            if ($count > 0) {
                $this->db->query("UPDATE blog SET images = '$images' WHERE id = $result->id");
                if (file_exists($imgPath . $image_name)) {
                    unlink($imgPath . $image_name);
                    return array('ack' => true, 'msg' => 'Image deleted');
                } else {
                    return array('ack' => false, 'msg' => 'Image not found.');
                }
            }
        }
    }

    public function findAllBlogTitleList()
    {
        return $this->db->query("SELECT bg.title FROM blog bg ORDER BY id DESC ")->result();
    }



    ########################################### ********* End ************ #########################################
}