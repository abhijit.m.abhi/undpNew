<?php

Class Inclusivebd_model extends CI_Model
{
    /**
     * @methodName
     * @access
     * @param  none
     * @author Md.Rokibuzzaman <rokibuzzaman@atilimited.net>
     * @return
     */

    ########################################### ********* Start ************ #########################################

     public function getInclusiveBd($user_id)
    {
        return $this->db->query("SELECT bd.*,u.fname,u.oname,u.userId, ut.name url_type, ut.value url_data,lk.* FROM inclusive_bd as bd
                                  LEFT JOIN url_type as ut ON ut.id = bd.url_type_id
                                  LEFT JOIN sa_lookup_data as lk ON lk.LOOKUP_DATA_ID = bd.category_id
                                  left join users as u on u.userId=bd.created_by
                                  WHERE bd.created_by = $user_id
                                  ORDER  BY bd.id DESC ")->result();
    }


        function count_items(){
        return $this->db->count_all('inclusive_bd');
    }
    function countItemsByCategory($cat){

        $row_count = $this->db->get_where('inclusive_bd', array('category_id' => $cat))->result();

        $num_rows = count($row_count);

        return $num_rows;
    }

     public function deleteBlogImage($blog_id, $image_name)
    {
        $result = $this->db->query("SELECT * FROM inclusive_bd WHERE id = $blog_id")->row();

        $imgPath = 'upload/inclusivebd/';

        if (strpos($result->images, $image_name) !== false) {
            $key = array($image_name . '___', '___' . $image_name, $image_name);
            $value = array('', '', '');
            $images = str_replace($key, $value, $result->images, $count);

            if ($count > 0) {
                $this->db->query("UPDATE inclusive_bd SET images = '$images' WHERE id = $result->id");
                if (file_exists($imgPath . $image_name)) {
                    unlink($imgPath . $image_name);
                    return array('ack' => true, 'msg' => 'Image deleted');
                } else {
                    return array('ack' => false, 'msg' => 'Image not found.');
                }
            }
        }
    }


     public function getAllInclusiveSearchList()
    {
        return $this->db->query("SELECT i.title,i.category,i.type,i.description,i.post_date FROM inclusive_bd i ORDER BY id DESC ")->result();
    }


         public function getAllInclusiveYBd($limit=0, $offset=0, $cat=0)
    {
        return $this->db->query("SELECT bd.*,u.fname,u.oname,u.userId, ut.name url_type, ut.value url_data,lk.* FROM inclusive_bd as bd
                                  LEFT JOIN url_type as ut ON ut.id = bd.url_type_id
                                  LEFT JOIN sa_lookup_data as lk ON lk.LOOKUP_DATA_ID = bd.category_id
                                  left join users as u on u.userId=bd.created_by
                                  WHERE bd.category_id='$cat' and active_status='Y'
                                  ORDER  BY bd.id DESC LIMIT $limit OFFSET $offset ")->result();
    }

      public function getAllInclusiveOpBd($limit=0, $offset=0)
    {
        return $this->db->query("SELECT bd.*,u.fname,u.oname,u.userId, ut.name url_type, ut.value url_data,lk.* FROM inclusive_bd as bd
                                  LEFT JOIN url_type as ut ON ut.id = bd.url_type_id
                                  LEFT JOIN sa_lookup_data as lk ON lk.LOOKUP_DATA_ID = bd.category_id
                                  left join users as u on u.userId=bd.created_by
                                  WHERE bd.category_id=2 and active_status='Y'
                                  ORDER  BY bd.id DESC LIMIT $limit OFFSET $offset")->result();
    }



    public function getAllInclusiveAreaBasedBd($limit=0, $offset=0)
    {
        return $this->db->query("SELECT bd.*,u.fname,u.oname,u.userId, ut.name url_type, ut.value url_data,lk.* FROM inclusive_bd as bd
                                  LEFT JOIN url_type as ut ON ut.id = bd.url_type_id
                                  LEFT JOIN sa_lookup_data as lk ON lk.LOOKUP_DATA_ID = bd.category_id
                                  left join users as u on u.userId=bd.created_by
                                  WHERE bd.category_id=3 and active_status='Y'
                                  ORDER  BY bd.id DESC LIMIT $limit OFFSET $offset ")->result();
    }


        public function increaseView($inclusive_id)
    {
        $this->db->query("UPDATE inclusive_bd SET view_count = view_count+1 WHERE id = $inclusive_id");
    }





      public function getInclusiveBdData($inclusive_id)
    {
        return $this->db->query("SELECT bd.*, ut.name url_type, ut.value url_data,lk.* FROM inclusive_bd as bd
                                LEFT JOIN url_type as ut ON ut.id = bd.url_type_id
                                LEFT JOIN sa_lookup_data as lk ON lk.LOOKUP_DATA_ID = bd.category_id
                                WHERE bd.id = $inclusive_id")->row();
    }

      public function getInclusiveBdCat()
    {
       $result=$this->db->query("SELECT lp.*  FROM sa_lookup_data as lp WHERE lp.LOOKUP_GRP_ID=1
                                 ORDER BY lp.LOOKUP_DATA_ID asc")->result();
       return $result;
    }


       public function urlTypes()
    {
        return $this->db->query("SElECT * FROM url_type")->result();
    }

     public function deletePost($inclusive_id)
    {
        if($this->db->delete('inclusive_bd', array('id' => $inclusive_id)))
        {
            return "Deleted";
        }
    }


       public function searchPost($cat_type, $order, $search, $limit=0, $offset=0)
    {
        $srch = $this->db->escape_str($search);
        $sql = '';
        $type = '';

        if (!empty($order)) {
            switch ($order) {
                case ('desc') :
                    $sql = " ORDER BY bg.post_date desc";
                    break;
                case ('asc') :
                    $sql = " ORDER BY bg.post_date asc";
                    break;
                case ('today') :
                    $sql = " AND DATE(post_date) = '" . date('Y-m-d') . "' ORDER BY bg.post_date desc";
                    break;
                case ('week') :
                    $_str_before_7_days = strtotime("-7 day");
                    $before_7_days = date('Y-m-d H:i:s', $_str_before_7_days);

                    $_str_after_7_days = strtotime("+7 day");
                    $after_7_days = date('Y-m-d H:i:s', $_str_after_7_days);

                    $sql = " AND post_date BETWEEN ' $before_7_days' AND '$after_7_days' ORDER BY bg.post_date desc";
                    break;
                default :
                    $sql = " ORDER BY opp.post_date desc";
            }
        } else {
            $sql = " ORDER BY bg.post_date desc ";
        }

        if (!empty($cat_type)) {
            $type = " AND bg.category_id = '$cat_type' ";
        }

        $row_count =  $this->db->query("SELECT bg.*,u.fname,u.oname,u.userId, ut.name url_type, ut.value url_data FROM inclusive_bd as bg
                                 LEFT JOIN url_type as ut ON ut.id = bg.url_type_id
                                 left join users as u on u.userId=bg.created_by
                                 WHERE ( bg.title LIKE '%$srch%' OR u.oname LIKE '%$srch%')
                                   $type $sql" )->result();

        $query = $this->db->query("SELECT bg.*,u.fname,u.oname,u.userId, ut.name url_type, ut.value url_data, lk.* FROM inclusive_bd as bg
                                 LEFT JOIN url_type as ut ON ut.id = bg.url_type_id
                                 left join users as u on u.userId=bg.created_by
                                 LEFT JOIN sa_lookup_data as lk ON lk.LOOKUP_DATA_ID = bg.category_id
                                 WHERE ( bg.title LIKE '%$srch%' OR u.oname LIKE '%$srch%')
                                   $type $sql LIMIT $limit OFFSET $offset" )->result();

        $num_rows = count($row_count);

        $this->session->set_userdata('inclusive_row_count', array(
            'num_rows' => $num_rows,
        ));

        return $query;
    }




     public function searchAllPostInclusive($category, $order,$search,$user_id)
    {
        $srch = strip_tags(html_entity_decode($search));
        $sql = '';
        $category_type = '';

        if (!empty($order)) {
            switch ($order) {
                case ('desc') :
                    $sql = " ORDER BY bg.post_date desc";
                    break;
                case ('asc') :
                    $sql = " ORDER BY bg.post_date asc";
                    break;
                case ('today') :
                    $sql = " AND DATE(post_date) = '" . date('Y-m-d') . "' ORDER BY bg.post_date desc";
                    break;
                case ('week') :
                    $_str_before_7_days = strtotime("-7 day");
                    $before_7_days = date('Y-m-d H:i:s', $_str_before_7_days);

                    $_str_after_7_days = strtotime("+7 day");
                    $after_7_days = date('Y-m-d H:i:s', $_str_after_7_days);

                    $sql = " AND post_date BETWEEN ' $before_7_days' AND '$after_7_days' ORDER BY bg.post_date desc";
                    break;
                default :
                    $sql = " ORDER BY opp.post_date desc";
            }
        } else {
            $sql = " ORDER BY bg.post_date desc ";
        }

        if (!empty($category)) {
            $category_type = " AND bg.category_id = '$category' ";
        }

        return $this->db->query("SELECT bg.*,u.fname,u.oname,u.userId, ut.name url_type, ut.value url_data,lk.* FROM inclusive_bd as bg
                                 LEFT JOIN url_type as ut ON ut.id = bg.url_type_id
                                 left join users as u on u.userId=bg.created_by
                                 LEFT JOIN sa_lookup_data as lk ON lk.LOOKUP_DATA_ID = bg.category_id
                                 WHERE bg.created_by=$user_id and ( bg.title LIKE '%$srch%' OR u.oname LIKE '%$srch%')
                                   $category_type $sql" )->result();
    }



       public function searchAllPost2($category, $order,$search, $limit=0, $offset=0)
    {
        $srch = strip_tags(html_entity_decode($search));
        $sql = '';
        $category_type = '';

        if (!empty($order)) {
            switch ($order) {
                case ('desc') :
                    $sql = " ORDER BY bg.post_date desc";
                    break;
                case ('asc') :
                    $sql = " ORDER BY bg.post_date asc";
                    break;
                case ('today') :
                    $sql = " AND DATE(post_date) = '" . date('Y-m-d') . "' ORDER BY bg.post_date desc";
                    break;
                case ('week') :
                    $_str_before_7_days = strtotime("-7 day");
                    $before_7_days = date('Y-m-d H:i:s', $_str_before_7_days);

                    $_str_after_7_days = strtotime("+7 day");
                    $after_7_days = date('Y-m-d H:i:s', $_str_after_7_days);

                    $sql = " AND post_date BETWEEN ' $before_7_days' AND '$after_7_days' ORDER BY bg.post_date desc";
                    break;
                default :
                    $sql = " ORDER BY opp.post_date desc";
            }
        } else {
            $sql = " ORDER BY bg.post_date desc ";
        }

        if (!empty($category)) {
            $category_type = " AND bg.category_id = '$category' ";
        }

        $row_count = $this->db->query("SELECT bg.*,u.fname,u.oname,u.userId, ut.name url_type, ut.value url_data,lk.* FROM inclusive_bd as bg
                                 LEFT JOIN url_type as ut ON ut.id = bg.url_type_id
                                 left join users as u on u.userId=bg.created_by
                                 LEFT JOIN sa_lookup_data as lk ON lk.LOOKUP_DATA_ID = bg.category_id
                                 WHERE ( bg.title LIKE '%$srch%' OR u.oname LIKE '%$srch%')
                                   $category_type $sql" )->result();

        $num_rows = count($row_count);

        $this->session->set_userdata('inclusive_row_count', array(
            'num_rows' => $num_rows,
        ));


        return $this->db->query("SELECT bg.*,u.fname,u.oname,u.userId, ut.name url_type, ut.value url_data,lk.* FROM inclusive_bd as bg
                                 LEFT JOIN url_type as ut ON ut.id = bg.url_type_id
                                 left join users as u on u.userId=bg.created_by
                                 LEFT JOIN sa_lookup_data as lk ON lk.LOOKUP_DATA_ID = bg.category_id
                                 WHERE ( bg.title LIKE '%$srch%' OR u.oname LIKE '%$srch%')
                                   $category_type $sql LIMIT $limit OFFSET $offset" )->result();
    }



     public function getInclusiveComment($inclusive_id)
    {
        return $this->db->query("SELECT bc.*, us.fname as author_name, us.image author_image ,ls.title as org_name FROM inclusivebd_comment as bc
                                 JOIN users as us ON bc.user_id = us.userId
                                 LEFT JOIN listing ls ON ls.userId = us.userId 
                                 WHERE bc.inclusivebd_id = $inclusive_id ORDER  BY bc.id ASC")->result();
    }


       public function addComment($description, $inclusive_id, $user_id)
    {
//        $dec = strip_tags(htmlspecialchars_decode(html_entity_decode($description)));
        $dec = htmlentities($description);

        if($this->db->query("INSERT INTO inclusivebd_comment (comment, inclusivebd_id, user_id, created_at)
                          VALUES ('$dec', $inclusive_id, $user_id, NOW())"))
        {
            return "Inserted";
        }
    }

      public function deleteComment($comment_id, $user_id)
    {
        if($this->db->query("DELETE FROM inclusivebd_comment
                          WHERE id = $comment_id
                          AND user_id = $user_id"))
        {
            return 'Deleted';
        }
    }

       public function getInclusiveInterest($inclusive_id)
    {
        return $this->db->query("SElECT type, count(type) interest_count FROM inclusivebd_interest WHERE inclusivebd_id='$inclusive_id' GROUP BY type")->result();
    }

        public function updateInclusiveBdInterest($blog_type, $user_id, $blog_id)
    {
        $count = $this->db->query("SELECT * FROM inclusivebd_interest WHERE inclusivebd_id= $blog_id AND user_id = $user_id")->num_rows();

        if($count == 0)
        {
            if($this->db->query("INSERT INTO inclusivebd_interest (type, inclusivebd_id, user_id) VALUES ($blog_type , $blog_id , $user_id)"))
            {
                return array('ack' => true);
            }
        }
        else
        {
            if($this->db->query("UPDATE inclusivebd_interest SET type = $blog_type WHERE inclusivebd_id = $blog_id  AND user_id = $user_id"))
            {
                return array('ack' => true);
            }
        }
    }


      public function getInclusiveBdInterest($blogID)
    {
        return $this->db->query("SElECT type, count(type) interest_count FROM inclusivebd_interest WHERE inclusivebd_id='$blogID' GROUP BY type")->result();
    }



    public function getAllInclusiveBdPosts()
    {
        return $this->db->query("SELECT bd.*,u.fname,u.oname,u.userId, ut.name url_type, ut.value url_data FROM inclusive_bd as bd
                                  LEFT JOIN url_type as ut ON ut.id = bd.url_type_id
                                  left join users as u on u.userId=bd.created_by
                                  ORDER  BY bd.id DESC")->result();
    }

    public function getBlogData($blog_id)
    {
        return $this->db->query("SELECT bg.*, ut.name url_type, ut.value url_data, ut.meta_tag meta_data  FROM blog as bg
                                    LEFT JOIN url_type as ut ON ut.id = bg.url_type_id
                                    WHERE bg.id = $blog_id")->row();
    }

    public function getOrganizaionData($organization_id)
    {
        return $this->db->query("SELECT listing.*, users.email as org_email, users.fname as author, users.userId as author_id FROM listing
                                  LEFT JOIN users ON users.userId = listing.userId
                                  WHERE id = $organization_id ")->row();
    }

    public function organizationLogo($user_id)
    {
        return $this->db->query("SELECT * FROM organization_images
                                    WHERE organization_id = $user_id  AND type = 'logo'")->row();
    }

    public function getBlogComment($blog_id)
    {
        return $this->db->query("SElECT bc.*, us.fname as author_name, us.image author_image, ls.title as org_name FROM blog_comment as bc
                                  JOIN users as us ON bc.user_id = us.userId
                                  LEFT JOIN listing ls ON ls.userId = us.userId
                                  WHERE bc.blog_id = $blog_id ORDER  BY bc.id ASC")->result();
    }

    public function updateInterest($blog_type, $user_id, $blog_id)
    {
        $count = $this->db->query("SELECT * FROM blog_interest WHERE blog_id= $blog_id AND user_id = $user_id")->num_rows();

        if($count == 0)
        {
            if($this->db->query("INSERT INTO blog_interest (type, blog_id, user_id) VALUES ($blog_type , $blog_id , $user_id)"))
            {
                return array('ack' => true);
            }
        }
        else
        {
            if($this->db->query("UPDATE blog_interest SET type = $blog_type WHERE blog_id = $blog_id  AND user_id = $user_id"))
            {
                return array('ack' => true);
            }
        }
    }


    public function getBlog($user_id)
    {
        return $this->db->query("SElECT bg.*, org.title as org_name, ut.name url_type, ut.value url_data FROM blog as bg
                                  INNER JOIN listing as org ON org.id = bg.organization_id
                                  LEFT JOIN url_type as ut ON ut.id = bg.url_type_id
                                  WHERE org.userId = $user_id
                                  ORDER  BY bg.id DESC ")->result();
    }

    public function getOrganization($user_id)
    {
        return $this->db->query("SELECT * FROM listing
                                  WHERE userId = $user_id")->row();
    }

    public function updateInterest2($imo_type, $user_id, $inclusive_id)
    {
        $count = $this->db->query("SELECT * FROM inclusivebd_interest WHERE inclusivebd_id= $inclusive_id AND user_id = $user_id")->num_rows();

        if ($count == 0) {
            if ($this->db->query("INSERT INTO inclusivebd_interest (type, inclusivebd_id, user_id) VALUES ($imo_type , $inclusive_id , $user_id)")) {
                return array('ack' => true);
            }
        } else {
            if ($this->db->query("UPDATE inclusivebd_interest SET type = $imo_type WHERE inclusivebd_id = $inclusive_id  AND user_id = $user_id")) {
                return array('ack' => true);
            }
        }
    }

    public function getOppInterest($inclusiveID)
    {
        return $this->db->query("SElECT type, count(type) interest_count FROM inclusivebd_interest WHERE inclusivebd_id='$inclusiveID' GROUP BY type")->result();
    }

    public function getInclusiveInterest2($inclusiveID, $userID)
    {
        return $this->db->query("SElECT type, count(type) interest_count FROM inclusivebd_interest WHERE inclusivebd_id='$inclusiveID' AND user_id='$userID' GROUP BY type")->result();
    }



    ########################################### ********* End ************ #########################################
}
