<?php

class Template {

    protected $_ci;

    function __construct() {

        $this->_ci = &get_instance();
    }

    function display($data = null) {
        
        $data['pageContentTitle'] = ((isset($data['pageTitle']) == '') ? ' ' : $data['pageTitle']);
        $data['pageTitle'] = 'Youthopia Bangla' . ((isset($data['pageTitle']) == '') ? ' ' : ' || ' . $data['pageTitle']);
        $data['breadcrumbs'] = ((isset($data['breadcrumbs']) == '') ? array() : $data['breadcrumbs']);
        $data['_content'] = $this->_ci->load->view((isset($data['content_view_page']) == '') ? 'template/content' : $data['content_view_page'], $data, true);
        
        
        $data['_bottom_effect'] = '' . (((isset($data['bottom_effect']) == '') ? ' ' : '' . $data['bottom_effect']));
        $data['_slider'] = '' . (((isset($data['slider']) == '') ? ' ' : '' . $data['slider']));
        
        
        $this->_ci->load->view('template.php', $data);
    }


       function display_individual($data = null) {
        $data['pageContentTitle'] = ((isset($data['pageTitle']) == '') ? ' ' : $data['pageTitle']);
        $data['pageTitle'] = 'Youthopia Bangla' . ((isset($data['pageTitle']) == '') ? ' ' : ' || ' . $data['pageTitle']);
        $data['breadcrumbs'] = ((isset($data['breadcrumbs']) == '') ? array() : $data['breadcrumbs']);
        $data['_content'] = $this->_ci->load->view((isset($data['content_view_page']) == '') ? 'template/content' : $data['content_view_page'], $data, true);
        
        
        $data['_bottom_effect'] = '' . (((isset($data['bottom_effect']) == '') ? ' ' : '' . $data['bottom_effect']));
        $data['_slider'] = '' . (((isset($data['slider']) == '') ? ' ' : '' . $data['slider']));
        
        
        $this->_ci->load->view('loggedin_individual_org.php', $data);
    }

}

?>