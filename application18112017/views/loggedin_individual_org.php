

<html class="no-js" lang="en"><!--<![endif]-->
<head>
    <title>Home | Youthopia Bangla</title>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="description" content="Youthopia.bangla | Youth Platform at Bangladesh"/>
    <meta name="keywords" content="youthopia.bangla, youthopia, youthopia bangladesh, youth bangladesh, undp, unv, youthopia bangla"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Youthopia.bangla">
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/youthopia_logo.ico">
    <!--google sign in meta-->
    <meta name="google-signin-client_id"
          content="620679733936-id09lrcd16t9bku12u675v9ec6q5quuc.apps.googleusercontent.com">
    <!-- CSS -->
    <link href="<?php echo base_url(); ?>assets/css/portal-css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/portal-css/datepicker.css">
    <link href="<?php echo base_url(); ?>assets/css/portal-css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/portal-css/ionicons.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/vendors/fileuploader/src/jquery.fileuploader.css" media="all" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/portal-css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/portal-css/responsive.css" rel="stylesheet">
    
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <!--[if (gte IE 6)&(lte IE 8)]>
    <script type="text/javascript" src="selectivizr.js"></script>
    <noscript>
        <link rel="stylesheet" href="[fallback css]"/>
    </noscript>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<style>
    div#homeline1 span#a {
        display: inline;
    }

    div#homeline1:hover span#a {
        display: none;
    }

    div#homeline1 span#b {
        display: none;
    }

    div#homeline1:hover span#b {
        display: inline;
    }

    @font-face {
        font-family: SolaimanLipi;
        src: url('fonts/SolaimanLipi_new.ttf');
    }
</style>
<style>
    .modal-backdrop {
        position: relative;
    }
</style>
<?php $this->load->view('modal/modal_new'); ?>



<div class="home_page_wrp">
    <section class="home_content_wrp">
        <div class="container">
            <header class="header">
                <nav class="navbar navbar-default main_mav">
                    <div class="">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse"
                                    data-target=".navbar-ex1-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand home_logo" href="#"><img src="<?php echo base_url(); ?>assets/portal-img/img/logo.png" alt=""/></a>
                        </div>
                         <?php
                           $session_info_fb = $this->session->userdata("userData");
                            $session_info = $this->session->userdata("user_logged_user");
                          //echo '<pre>';print_r($session_info);exit;
                           ?>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                       <div class="collapse navbar-collapse navbar-ex1-collapse">
                            <ul class="nav navbar-nav navbar-right admin_bar">
                                       <li class="dropdown">
                                    <a href="#" class="dropdown-toggle"
                                       data-toggle="dropdown"><span>
                                        <?php
                           if (!empty($session_info_fb["facebookId"])){
                              
                            ?>
                                             <?php
                           if (!empty($session_info_fb["image"])){
                              $name = $session_info_fb["image"];
                            ?>

                             <img style="" width="30px" height="20px" src="<?php echo $session_info_fb["image"]; ?>" class="img"/>

                             <?php 
                            }else{ ?>
                            <img style="" width="30px" height="20px"src="<?php echo base_url(); ?>assets/portal-img/assets/portal-img/img/placeholder.png" class="img"/>

                                  <?php 
                            }
                            
                           ?>
                           </span> <b> 
                                       

                                    <?php
                                    if (!empty($session_info_fb["facebookId"])) {

                                         if ($session_info_fb["type"] = "individual") {
                                            echo $session_info_fb["fname"];
                                    }
                                        
                                       
                                        } ?>
                                    </b>
                                        <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                     

                                         <?php
                           if($session_info_fb["type"]= "oranization" && $session_info_fb["type"]!= "individual"){
                            ?>
                             <li><a href="listing.php">Edit Profile</a></li>
                                            <li><a href="">My Events</a></li>
                                            <li><a href="">My Media Blog</a></li>
                                            <li><a href="#report_problem" data-toggle="modal">Report Problem</a></li>
                                            <li><a href="#reset_password" data-toggle="modal">Reset Password</a></li>
                                                                    <li><a href="#In_active_account_modal" data-toggle="modal">Inactive Account</a></li>
                                            <li><a href="<?php echo site_url("auth/userLogout"); ?>">Logout</a></li></li>

                             <?php 
                            }else{ ?>
                              <li><a href="#edit_individual" data-toggle="modal">Edit Profile</a></li>
                                            <li><a href="#reset_password" data-toggle="modal">Reset Password</a></li>
                                                                    <li><a href="#In_active_account_modal" data-toggle="modal">Inactive Account</a></li>
                                            <li><a href="<?php echo site_url("auth/userLogoutByFb"); ?>">Logout</a></li></li>
                                  <?php 
                            }
                            
                           ?>
                           <?php }else{ ?>
                                         
                                       <?php
                           if (!empty($session_info["image"])){
                              $name = $session_info["image"];
                            ?>

                             <img style="" width="30px" height="20px" src="<?php echo base_url(''.$session_info["image"]); ?>" class="img"/>

                             <?php 
                            }else{ ?>
                            <img style="" width="30px" height="20px"src="<?php echo base_url(); ?>assets/portal-img/assets/portal-img/img/placeholder.png" class="img"/>

                                  <?php 
                            }
                            
                           ?>
                           </span> <b> 
                                       

                                    <?php
                                    if (empty($session_info_fb["facebookId"])) {
                                        if ($session_info["type"]= "organization" && $session_info["type"]!= "individual") {
                                            echo $session_info["fname"];
                                        }
                                        if ($session_info["type"] = "individual" && $session_info["type"] != "organization") {
                                            echo $session_info["fname"];
                                        } 
                                       
                                    }
                                        ?>
                                    </b>
                                        <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                     

                                         <?php
                           if($session_info["type"]= "organization" && $session_info["type"]!= "individual"){
                            ?>
                             <li><a href="listing.php">Edit Profile</a></li>
                                            <li><a href="">My Events</a></li>
                                            <li><a href="">My Media Blog</a></li>
                                            <li><a href="#report_problem" data-toggle="modal">Report Problem</a></li>
                                            <li><a href="#reset_password" data-toggle="modal">Reset Password</a></li>
                                                                    <li><a href="#In_active_account_modal" data-toggle="modal">Inactive Account</a></li>
                                            <li><a href="<?php echo site_url("auth/userLogout"); ?>">Logout</a></li></li>

                             <?php 
                            }else{ ?>
                              <li><a href="#edit_individual" data-toggle="modal">Edit Profile</a></li>
                                            <li><a href="#reset_password" data-toggle="modal">Reset Password</a></li>
                                                                    <li><a href="#In_active_account_modal" data-toggle="modal">Inactive Account</a></li>
                                            <li><a href="<?php echo site_url("auth/userLogout"); ?>">Logout</a></li></li>
                                  <?php 
                            }
                            
                           ?>
                           <?php } ?>
                           

                                    </ul>
                                    
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </header>
            <div class="home_content">

                <div id="homeline1" style="width: 330px;">
                    <span id="a">
                            <h1>Youthopia.bangla</h1>
                            <h4>building the country <span style="color:#E85D30;">WE</span> want</h4>
                            <h4>revealing the culture <span style="color:#E85D30;">WE</span> are part of</h4>
                    </span>
                    <span id="b">
                                <h1 style="font-family: SolaimanLipi; font-size: 36px">ইয়ুথওপিয়া.বাংলা</h1>
                                <h4 style="font-family: SolaimanLipi;">সম্ভাবনাময় দেশ গড়ি</h4>
                            <h4 style="font-family: SolaimanLipi;">বিশ্বময় নিজ সংস্কৃতি তুলে ধরি  </h4></span>
                </div>
                <!--
                <div width=300px;>
                    <h4><span class="replies">building the country <span style="color:#E85D30;">WE</span> want</span></h4>
                    <h4><span class="comment">zahed the country <span style="color:#E85D30;">WE</span> want</span></h4>
                    <h4>revealing the culture <span style="color:#E85D30;">WE</span> are part of</h4>
                </div> -->
            </div>
            <!--            <div class="home_icons_row">
                            <a href="orgdirectory.php" class="home_icons_col home_icons_col1">
                                <span class="home_icons_title">জংশন</span> <br>
                                <span class="home_icons_img"><img src="<?php echo base_url(); ?>assets/portal-img/img/3.png" alt=""></span> <br>
                                <span class="home_icons_text">Directory</span>
                            </a>
                            <a href="org-event-calender.php" class="home_icons_col home_icons_col4">
                                <span class="home_icons_title">দিনোপাখ্যান</span> <br>
                                <span class="home_icons_img"><img src="<?php echo base_url(); ?>assets/portal-img/img/1.png" alt=""></span> <br>
                                <span class="home_icons_text">Calendar</span>
                            </a>
                            <a href="opportunities.php" class="home_icons_col home_icons_col3">
                                <span class="home_icons_title">দূরবীক্ষণ</span> <br>
                                <span class="home_icons_img"><img src="<?php echo base_url(); ?>assets/portal-img/img/4.png" alt=""></span> <br>
                                <span class="home_icons_text">Opportunities</span>
                            </a>
                            <a href=media_blogs.php class="home_icons_col home_icons_col2">
                                <span class="home_icons_title">প্রতিবিম্ব</span> <br>
                                <span class="home_icons_img"><img src="<?php echo base_url(); ?>assets/portal-img/img/2.png" alt=""></span> <br>
                                <span class="home_icons_text">Media Blog</span>
                            </a>
                        </div>-->
            <div class="home_icons_row">
            
                
            <div class="col-md-12 hidden-xs">
                <a href="orgdirectory.php" class="home_icons_col home_icons_col1">
                    <span class="home_icons_title">Directory</span> <br>
                    <span class="home_icons_img">
                                <img src="<?php echo base_url(); ?>assets/portal-img/img/33.png"
                                     onmouseover="this.src='<?php echo base_url(); ?>assets/portal-img/img/3.png'"
                                     onmouseout="this.src='<?php echo base_url(); ?>assets/portal-img/img/33.png'"
                                     border="0" alt=""/>
                            </span> <br>
                    <span class="home_icons_text">জংশন</span>
                </a>
                <a href="org-event-calender.php" class="home_icons_col home_icons_col4">
                    <span class="home_icons_title">Calendar</span> <br>
                    <span class="home_icons_img">
                                <img src="<?php echo base_url(); ?>assets/portal-img/img/11.png"
                                     onmouseover="this.src='<?php echo base_url(); ?>assets/portal-img/img/1.png'"
                                     onmouseout="this.src='<?php echo base_url(); ?>assets/portal-img/img/11.png'"
                                     border="0" alt=""/>
                            </span> <br>
                    <span class="home_icons_text">দিনোপাখ্যান</span>
                </a>
                <a href="opportunities.php" class="home_icons_col home_icons_col3">
                    <span class="home_icons_title">Opportunities</span> <br>
                    <span class="home_icons_img">
                                <img src="<?php echo base_url(); ?>assets/portal-img/img/44.png"
                                     onmouseover="this.src='<?php echo base_url(); ?>assets/portal-img/img/4.png'"
                                     onmouseout="this.src='<?php echo base_url(); ?>assets/portal-img/img/44.png'"
                                     border="0" alt=""/>
                            </span> <br>
                    <span class="home_icons_text">দূরবীক্ষণ</span>
                </a>
                <a href="media_blogs.php" class="home_icons_col home_icons_col2">
                    <span class="home_icons_title">Media Blog</span> <br>
                    <span class="home_icons_img">
                                <img src="<?php echo base_url(); ?>assets/portal-img/img/22.png"
                                     onmouseover="this.src='<?php echo base_url(); ?>assets/portal-img/img/2.png'"
                                     onmouseout="this.src='<?php echo base_url(); ?>assets/portal-img/img/22.png'"
                                     border="0" alt=""/>
                            </span> <br>
                    <span class="home_icons_text">প্রতিবিম্ব</span>
                </a>
                </div>

            <div class="col-xs-6 hidden-lg hidden-md hidden-sm">
                <a href="orgdirectory.php" class="home_icons_col home_icons_col1">
                    <span class="home_icons_title">Directory</span> <br>
                    <span class="home_icons_img">
                                <img src="<?php echo base_url(); ?>assets/portal-img/img/33.png"
                                     onmouseover="this.src='<?php echo base_url(); ?>assets/portal-img/img/3.png'"
                                     onmouseout="this.src='<?php echo base_url(); ?>assets/portal-img/img/33.png'"
                                     border="0" alt=""/>
                            </span> <br>
                    <span class="home_icons_text">জংশন</span>
                </a>
                <a href="org-event-calender.php" class="home_icons_col home_icons_col4">
                    <span class="home_icons_title">Calendar</span> <br>
                    <span class="home_icons_img">
                                <img src="<?php echo base_url(); ?>assets/portal-img/img/11.png"
                                     onmouseover="this.src='<?php echo base_url(); ?>assets/portal-img/img/1.png'"
                                     onmouseout="this.src='<?php echo base_url(); ?>assets/portal-img/img/11.png'"
                                     border="0" alt=""/>
                            </span> <br>
                    <span class="home_icons_text">দিনোপাখ্যান</span>
                </a>
                </div>
                <div class="col-xs-6 hidden-lg hidden-md hidden-sm">
                <a href="opportunities.php" class="home_icons_col home_icons_col3">
                    <span class="home_icons_title">Opportunities</span> <br>
                    <span class="home_icons_img">
                                <img src="<?php echo base_url(); ?>assets/portal-img/img/44.png"
                                     onmouseover="this.src='<?php echo base_url(); ?>assets/portal-img/img/4.png'"
                                     onmouseout="this.src='<?php echo base_url(); ?>assets/portal-img/img/44.png'"
                                     border="0" alt=""/>
                            </span> <br>
                    <span class="home_icons_text">দূরবীক্ষণ</span>
                </a>
                <a href="media_blogs.php" class="home_icons_col home_icons_col2">
                    <span class="home_icons_title">Media Blog</span> <br>
                    <span class="home_icons_img">
                                <img src="<?php echo base_url(); ?>assets/portal-img/img/22.png"
                                     onmouseover="this.src='<?php echo base_url(); ?>assets/portal-img/img/2.png'"
                                     onmouseout="this.src='<?php echo base_url(); ?>assets/portal-img/img/22.png'"
                                     border="0" alt=""/>
                            </span> <br>
                    <span class="home_icons_text">প্রতিবিম্ব</span>
                </a>
                </div>                
            </div>
        </div>
    </section>
  <?php $this->load->view('template/footer'); ?>
</div>


<!-- ALL THE SCRIPTS/PLUGINS -->
<!-- Jquery, Modernizer, Selectivizr & Prefix Free Js -->
<script src="<?php echo base_url(); ?>assets/js/portal-js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/portal-js/modernizr-2.8.3.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/portal-js/selectivizr-min.js"></script>
<!-- Bootstrap Js -->
<script src="<?php echo base_url(); ?>assets/js/portal-js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/portal-js/bootstrap-datetimepicker.min.js"></script>
<!-- Theme Main Js -->
<script src="assets/vendors/fileuploader/src/jquery.fileuploader.min.js" type="text/javascript"></script>
<script src="assets/vendors/fileuploader/examples/default-upload/js/custom.js" type="text/javascript"></script>

<script src="<?php echo base_url(); ?>assets/js/portal-js/main.js"></script>
<!-- AJAX JS-->
<script src="<?php echo base_url(); ?>assets/js/portal-js/ajax.js"></script>
<!--FACEBOOK JS Script-->
<script src="<?php echo base_url(); ?>assets/js/portal-js/fbzahed.js"></script>
<!--<script>
    $(window).load(function () {
        $('#myModal').modal('show');
    });
</script>-->
<!--   <script>
    $(window).load(function()
    {
    $('#sign_up').modal('show');
    });
    </script> -->
<script type="text/javascript">
    $(document).ready(function () {
        $('.date_picker').datepicker({
            format: "yyyy-mm-dd"
        });
    });
</script>
<script>
    $(document).ready(function () {
        $('[data-toggle="popover"]').popover();
    });
</script>
</body>
</html>