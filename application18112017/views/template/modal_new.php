<!--Login -Model START-->
<div class="modal fade login_modal_wrp" id="log_in" style="padding:0px;">
    <div class="modal-dialog">
        <div class="modal-content" style="padding:0px;">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close" style="color: black;"></i>
            </button>
            <div class="login_modal_title"><h1>WELCOME!</h1></div>
            <div class="login_modal_form_wrp">
                <div class="social_login">
                    <!--<a class="facebook_icon" href="<?php //echo $faceBookClient->getAuthUrl(); ?>"><i class="fa fa-facebook"></i></a>-->
                    <!-- <a class="facebook_icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Sign up with Facebook"><i class="fa fa-facebook"></i></a> -->
                    <a class="facebook_icon" id="facebook_url_for_login"
                       href="<?php echo $faceBookClient->getAuthUrl(); ?>"><i class="fa fa-facebook"></i></a>
                    <span>/</span>

                    <a class="gplus_icon" href="<?php echo $googlePlusAuth->getAuthUrl(); ?>"><i
                                class="fa fa-google-plus"></i></a>
                    <p>or</p>
                </div>
                <div class="login_modal_form">
                    <div id="messageLogin"></div>
                    <form id="formLogin" action="" method="POST">
                        <div class="form_group">
                            <input class="login_mail" type="email" id="username" name="username" placeholder="email"
                                   required="required"/>
                        </div>
                        <div class="form_group">
                            <input class="login_pas" type="password" name="password" id="password"
                                   placeholder="password" required="required"/>
                        </div>
                        <div class="forgot_pass_row">
                            <span class="remember_me">
                                <input type="checkbox" name="" id="remember-me"/>
                                <label for="remember-me">remember me</label>
                            </span>
                            <a href="#forget_password" data-toggle="modal" class="forgot_pass">forgot your password?</a>
                            <div class="clearfix"></div>
                        </div>
                        <input hidden type="text" name="formLogin" id="formLogin" value="formLogin">
                        <div class="login_btn">
                            <button type="submit">LOGIN</button>
                        </div>
                    </form>
                    <p class="not_reg_yet "><a href="#sign_up" data-toggle="modal" data-dismiss="modal">Not registered
                            yet? </a></p>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Login -Model END-->

<!-- Sign Up -Model START -->
<div class="modal fade signup_modal_wrp" id="sign_up">
    <div class="modal-dialog">
        <div class="modal-content" id="hideIndi">
            <button type="button" class="close" data-dismiss="modal" style="color: black ;opacity:1;"><i
                        class="fa fa-close"></i></button>
            <div class="signup_modal_tab_wrp">
                <h1>SIGN UP AS</h1>
                <ul class="signup_modal_tab">
                    <li class="active"><a style="background-color: #FFCC33" href="#individual" data-toggle="tab"
                                          id="Ind">individual</a></li>
                    <li><a href="#organisation" data-toggle="tab" id="Org">organization</a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div id="individual" class="tab-pane active">

                        <div class="signup_individual_form">
                            <div class="social_signup">
                                <a id="facebook_url_for_signup" class="facebook_icon"
                                   href="<?php echo $faceBookClient->getAuthUrl(); ?>"><i
                                            class="fa fa-facebook"></i></a>
                                <span>/</span>
                                <a class="gplus_icon" href="<?php echo $googlePlusAuth->getAuthUrl(); ?>"><i
                                            class="fa fa-google-plus"></i></a>
                                <p>or,</p>
                            </div>
                            <div class="signup_modal_form individual">
                                <div id="messageIndividual"></div>
                                <form id="formIndividual" action="" method="POST" enctype="multipart/form-data">
                                    <div class="form_group">
                                        <input class="signup_ind_name" type="text" name="fname" id="ind_full_name"
                                               placeholder="Full Name *" required="required"/>
                                    </div>
                                    <div class="form_group">
                                        <input class="signup_ind_contact" type="text" name="phoneNumber"
                                               id="phoneNumber" placeholder="Mobile Number *" required="required"/>
                                    </div>
                                    <div class="form_group">
                                        <input class="signup_ind_birth date_picker" type="text" id="date" name="date"
                                               placeholder="DOB (YY-MM-DD) *" required="required"/>
                                        <select style="color: #F9C332;" class="signup_ind_gender" id="ind_gender"
                                                name="gender" required="required">
                                            <option value="">Select gender*</option>
                                            <option value="Male">Male</option>
                                            <option value="Female">Female</option>
                                            <option value="Others">Others</option>
                                        </select>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="form-group"
                                         style="display:inline-flex; color:#FFCC33; margin-bottom: 6px;width: 100%">
                                        <!--                                                <label class="control-label" for="file" style="width: 131px;">Image(max-5Mb):</label>-->
                                        <input type="file" name="individual-file" id="individual-file">
                                        <!--                                                <input type="file" name="file" id="file" style="margin-left:1px" required="required" title="image">-->
                                    </div>
                                    <div class="form_group">
                                        <input class="signup_ind_email" type="email" name="email" id="ind_email"
                                               placeholder="Email ID *" required="required"/>
                                    </div>
                                    <div class="form_group">
                                        <input class="signup_ind_pass1" type="password" name="password"
                                               id="ind_password" placeholder="Password *" required="required"/>
                                    </div>
                                    <div class="form_group">
                                        <input class="signup_ind_pass2" type="password" name="re_pass" id="ind_re_pass"
                                               placeholder="Conﬁrm Password *" required="required"/>
                                    </div>
                                    <div class="signup_modal_chkbox">
                                        <div><input class="signup_ind_chkbox" type="checkbox" id="agree" name="agree"
                                                    value="agree" checked/>
                                            <label for="agree">I ACCEPT THE <a href="Terms_and_Conditions.pdf"
                                                                               target="_blank">TERMS &amp;
                                                    CONDITIONS </a><label> and <a href="Privacy_Policy.pdf"
                                                                                  target="_blank"> PRIVACY &amp;
                                                        POLICY</a></label></div>
                                        <input type="hidden" name="individual" id="individual" value="individual">
                                        <input type="hidden" name="facebook_user_id" id="facebook_user_id" value=""/>
                                        <span id="individual-hidden"></span>
                                        <input hidden type="text" name="submitIndividual" id=""
                                               value="submitIndividual">
                                        <button class="btn btn-success" type="submit" id="submitIndividual"
                                                name="submitIndividual" disabled="disabled">Request for Registration
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div id="organisation" class="tab-pane fade">
                        <div class="signup_organization_form">
                            <div class="signup_modal_form organisation">
                                <div id="messageOrganization"></div>
                                <form id="formOrganization" action="" method="POST" enctype="multipart/form-data">
                                    <div class="form_group">
                                        <input class="signup_org_name" type="text" name="oname" id="oname"
                                               placeholder="Organization Name *" required="required"/>
                                    </div>
                                    <div class="form_group">
                                        <input class="signup_org_email" type="email" name="oemail" id="oemail"
                                               placeholder="Organization Email *" required="required"/>
                                    </div>
                                    <div class="form_group">
                                        <input class="signup_org_fb" type="text" name="facebook" id="facebook"
                                               placeholder="Facebook Page Link *" required="required"/>
                                    </div>
                                    <div class="form_group">
                                        <input class="signup_org_web" type="text" name="website" id="website"
                                               placeholder="Website"/>
                                    </div>
                                    <div class="form_group">
                                        <input class="signup_org_contact_name" type="text" name="fname" id="org_fname"
                                               placeholder="Contact Person Name *" required="required"/>
                                    </div>
                                    <div class="form_group">
                                        <input class="signup_org_contact_email" type="email" name="email" id="org_email"
                                               placeholder="Contact Person Email*" required="required"/>
                                    </div>
                                    <div class="form_group">
                                        <input class="signup_org_mobile" type="text" name="number" id="org_number"
                                               placeholder="Contact Person Mobile Number *" required="required"/>
                                    </div>
                                    <div class="form-group"
                                         style="display:inline-flex; color:#FFCC33; margin-bottom: 6px;width: 100%">
                                        <!--                                                <label class="control-label" for="file" style="width: 118px;">Logo(max-5Mb):</label>-->
                                        <!--                                                <input type="file" name="file" id="file"  style="margin-left:10px" required="required" title="image">-->
                                        <input type="file" name="organization-file" id="organization-file">

                                    </div>
                                    <div class="form_group">
                                        <input class="signup_org_password1" type="password" name="password"
                                               id="org_password" placeholder="Password *" required="required"/>
                                    </div>
                                    <div class="form_group">
                                        <input class="signup_org_password2" type="password" name="re_pass"
                                               id="org_re_pass" placeholder="Conﬁrm Password *" required="required"/>
                                    </div>
                                    <div class="signup_modal_chkbox">
                                        <div><input class="signup_ind_chkbox" type="checkbox" id="i_accept2"
                                                    name="agree" id="agree" value="agree" checked/>
                                            <label for="i_accept2">I ACCEPT THE <a href="Terms_and_Conditions.pdf"
                                                                                   target="_blank">TERMS &amp;
                                                    CONDITIONS </a><label> and <a href="Privacy_Policy.pdf"
                                                                                  target="_blank"> PRIVACY &amp;
                                                        POLICY</a></label></div>
                                        <input type="hidden" name="organization" id="organization" value="organization">
                                        <span id="organization-hidden"></span>
                                        <input type="hidden" name="submitOrganization" id="" value="submitOrganization">
                                        <button type="submit" class="btn btn-success" id="submitOrganization"
                                                name="submitOrganization" disabled>Request for Registration
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Sign Up -Model END-->

<!-- EDIT Individual Model START-->
<div class="modal fade signup_modal_wrp" id="edit_individual">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" style="color: black; opacity:1;"><i
                        class="fa fa-close"></i></button>
            <div class="signup_modal_tab_wrp">

                <div id="individual">
                    <div class="signup_individual_form">
                        <div class="signup_modal_form">
                            <div id="messageUpdateIndividual"></div>
                            <form id="formUpdateIndividual" action="" method="POST" enctype="multipart/form-data">
                                <br>
                                <br>
                                <?php $data = $auth->getAllData($id) ?>
                                <div class="form_group">
                                    <input class="signup_ind_name" type="text" name="fname" id="fname"
                                           value="<?php echo $data['fname'] ?>" placeholder="Full Name"
                                           required="required"/>
                                </div>
                                <div class="form_group">
                                    <input class="signup_ind_birth date_picker" type="text"
                                           value="<?php echo $data['date'] ?>" id="date" name="date"
                                           placeholder="Date of Birth" required="required"/>
                                    <select class="signup_ind_gender" id="gender" name="gender" style="padding: 0px;">

                                        <option value="Male"<?php if ($data['gender'] === "Male") {
                                            echo "selected";
                                        } ?>>Male
                                        </option>
                                        <option value="Female" <?php if ($data['gender'] === "Female") {
                                            echo "selected";
                                        } ?>>Female
                                        </option>
                                        <option value="Other" <?php if ($data['gender'] === "Other") {
                                            echo "selected";
                                        } ?>>Other
                                        </option>
                                    </select>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form_group">
                                    <input class="signup_ind_contact" type="text" value="<?php echo $data['number'] ?>"
                                           name="phoneNumber" id="phoneNumber" value="<?php echo $data['password']; ?>"
                                           placeholder="Contact Number" required="required"/>
                                </div>
                                <div class="form-group" style="display:inline-flex; color:#FFCC33;">
                                    <label class="control-label" for="file">Image:</label>

                                    <input type="file" name="indi_file" value="<?php $data['image'] ?>" id="indi_file"
                                           multiple style="margin-left:10px" title="image">

                                </div>
                                <div class="form_group">
                                    <input class="signup_ind_email" readonly type="email"
                                           value="<?php echo $data['email']; ?>" name="email" id="email"
                                           placeholder="Email ID" required="required"/>
                                </div>
                                <!--
                                <div class="form_group">
                                    <input class="signup_ind_pass1" type="password" name="password" id="password" placeholder="Password" required="required"/>
                                </div>
                                <div class="form_group">
                                    <input class="signup_ind_pass2" type="password" name="re_pass" id="re_pass" placeholder="Conﬁrm Password" required="required"/>
                                </div>-->
                                <div class="signup_modal_chkbox">
                                    <input hidden type="text" name="updateIndividual" id="updateIndividual"
                                           value="updateIndividual">
                                    <button type="submit" id="updateIndividual" name="updateIndividual">Update Profile
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>


        </div>
    </div>
</div>

<!-- EDIT Individual Model START-->
<!-- Reset Password Model START-->
<div class="modal fade" id="reset_password">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Password Reset Request</h4>
            </div>
            <div class="modal-body">
                <div class="forget_pass">
                    <div class="row">
                        <div class="col-md-9 col-md-offset-2">
                            <div id="resetPassMessage"></div>
                            <form id="resetPassForm" action="" method="POST" novalidate>
                                <div id="resetMessage"></div>
                                <div class="form-group">
                                    <label for="">Enter You Current Password</label>
                                    <input type="password" class="form-control" id="current_password"
                                           name="current_password">

                                </div>
                                <div class="form-group">
                                    <label for="">Enter You New Password</label>
                                    <input type="password" class="form-control" id="new_password" name="new_password">
                                    <input hidden type="text" id="resetnewpassword" value="1" name="resetnewpassword">
                                </div>
                                <button type="submit" class="btn btn-info">Submit</button>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Reset Password Model END-->
<!-- Inactive Model START-->
<div class="modal fade" id="In_active_account_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Inactive Account</h4>
            </div>
            <div class="modal-body">
                <div class="forget_pass">
                    <div class="row">
                        <div class="col-md-9 col-md-offset-2">
                            <div id="resetPassMessage"></div>
                            <form id="inactiveForm" action="" method="POST" novalidate>
                                <div id="InactiveMessage"></div>

                                <div class="form-group">
                                    <label for="">Do You want to Inactive your profile?</label>
                                    <button type="submit" class="btn btn-info">Yes</button>

                                    <button type="button" class="btn btn-info" data-dismiss="modal" aria-hidden="true">
                                        No
                                    </button>
                                    <input type="hidden" id="inactiveValue" value="1" name="inactiveValue">
                                </div>


                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Inactive Model END-->
<!-- Quick Contact Model Start -->
<div class="modal fade signup_modal_wrp" id="contact_envelope">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" style="top:-15px; right:-7px" data-dismiss="modal"><i
                        class="fa fa-close"></i></button>
            <div class="modal-body">

                <div class="quick_contact">
                    <h4>Send Us A Quick Message</h4>
                    <div id="messageContact"></div>
                    <form id="contactForm_submit" action="" method="POST" role="form" novalidate>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="name">Name *</label>
                                    <input type="text" required="required" class="form-control" name="name" id="name"
                                           placeholder="Name *">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="email">Email *</label>
                                    <input type="text" required="required" class="form-control" name="email" id="email"
                                           placeholder="Email *">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="subject">Subject</label>
                                    <input type="text" class="form-control" name="subject" id="subject"
                                           placeholder="Subject *" required="required">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="message">Message *</label>
                                    <textarea name="message" id="message" placeholder="Message *" class="form-control"
                                              rows="3" required="required" spellcheck="true"></textarea>
                                </div>
                            </div>
                            <input type="hidden" id="contactForm" name="contactForm" value="contactForm">

                            <div class="col-md-12">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success btn-block btn-sm">Submit</button>
                                </div>
                            </div>
                        </div>


                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- Quick Contact Model END -->
<!-- forget_password Model START  -->
<div class="modal fade" id="forget_password">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Password Reset Request</h4>
            </div>
            <div class="modal-body">
                <div class="forget_pass">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div id="forgetPassMessage"></div>
                            <form id="forgetPassForm" action="" method="POST">
                                <div id="forgetMessage"></div>
                                <div class="form-group">
                                    <label for="">Enter You Email *</label>
                                    <input type="text" class="form-control" id="forget_pass" name="forget_pass"
                                           placeholder="Enter You Email" required>
                                    <input hidden type="text" id="forgetPass" value="forgetPass" name="forgetPass">
                                </div>
                                <button type="submit" name="forgetPass" id="forgetPass" class="btn btn-info">Submit
                                </button>
                                <!-- <a href="javascript:void(0)" -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- forget_password Model END  -->


