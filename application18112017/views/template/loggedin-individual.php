

<html class="no-js" lang="en"><!--<![endif]-->
<head>
    <title>Home | Youthopia Bangla</title>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="description" content="Youthopia.bangla | Youth Platform at Bangladesh"/>
    <meta name="keywords" content="youthopia.bangla, youthopia, youthopia bangladesh, youth bangladesh, undp, unv, youthopia bangla"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Youthopia.bangla">
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/youthopia_logo.ico">
    <!--google sign in meta-->
    <meta name="google-signin-client_id"
          content="620679733936-id09lrcd16t9bku12u675v9ec6q5quuc.apps.googleusercontent.com">
    <!-- CSS -->
    <link href="<?php echo base_url(); ?>assets/css/portal-css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/portal-css/datepicker.css">
    <link href="<?php echo base_url(); ?>assets/css/portal-css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/portal-css/ionicons.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/vendors/fileuploader/src/jquery.fileuploader.css" media="all" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/portal-css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/portal-css/responsive.css" rel="stylesheet">
    
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <!--[if (gte IE 6)&(lte IE 8)]>
    <script type="text/javascript" src="selectivizr.js"></script>
    <noscript>
        <link rel="stylesheet" href="[fallback css]"/>
    </noscript>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<style>
    div#homeline1 span#a {
        display: inline;
    }

    div#homeline1:hover span#a {
        display: none;
    }

    div#homeline1 span#b {
        display: none;
    }

    div#homeline1:hover span#b {
        display: inline;
    }

    @font-face {
        font-family: SolaimanLipi;
        src: url('fonts/SolaimanLipi_new.ttf');
    }
</style>
<style>
    .modal-backdrop {
        position: relative;
    }
</style>

<style>


body{
    font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
    background-image: url("https://www.youthopiabangla.org/img/home_bg.jpg");
    background-repeat: no-repeat;
}
.logo i {
    font-size: 31px;
    margin-right: 4px;
    word-spacing: 14px;
}
.logo{
    color: white;
    margin: 0;
    font-size: 20px;
    padding: 4px 0;
    padding-bottom: 15px;

}
.login-bottom-text{
    margin-top: 0px;
    margin-bottom: 0px;
    font-size: 12px;
    color: white;
    line-height: 19px;
}

header{
    /*background: #3b5998;*/
    padding-top: 20px;
}
header .form-group{
    margin-bottom: 0px;
}
header .btn-header-login{
    margin-bottom: 15px;    
}
.login-main{
    margin-top: 130px;
}
.multibox{
    padding-left: 0px;
    padding-bottom: 10px;
}
.login-main span{
    font-size: 12px;
} 



footer hr{
    margin-top: 0px;
    padding-top: 0px;
}
.footer-options ul{
    clear: both;
    padding: 0px;
    margin: 0px;
}
.footer-options ul li{
    float:left;
    list-style: none;
    padding: 5px;
    font-size: 12px;
}
.footer-options ul li a{
    text-decoration: none;
    color: #000;
}
.copyrights{
    margin-top: 25px;
}

</style>
<?php $this->load->view('modal/modal_new'); ?>

<header>
    <div class="container">
    <div class="row">
        <div class="col-sm-6">
            <div class="logo"><img src="https://www.youthopiabangla.org/img/logo.png" alt=""></div>
        </div>
     
        <div class="col-sm-6 hidden-xs">
            <div class="row">
              <?php
                           $session_info = $this->session->userdata("user_logged_user");
                          //echo '<pre>';print_r($session_info);exit;
                           ?>
                        <?php
                           if(!$this->session->userdata('user_logged_user')){
                            ?>
                             <a href="#"  class="btn btn-link" style="color: green;text-decoration: underline;font-size: 14px;font-weight: 900;font-style: italic;">Login</a>
                             <a href="#"  class="btn btn-link" style="color: green;text-decoration: underline;font-size: 14px;font-weight: 900;font-style: italic;">Register</a>

                             <?php 
                            }else{ ?>
                             <a href="#"     class="btn btn-link" style="color: green;font-size: 14px;font-weight: 900;font-style: italic;"><?php echo $session_info["fname"]." ".$session_info["fname"];?> </a> <a href="<?php echo site_url("auth/userLogout"); ?>" class="btn btn-link" style="color: green;text-decoration: underline;font-size: 14px;font-weight: 900;font-style: italic;">Logout</a>
                                  <?php 
                            }
                            
                           ?>
                
            </div>  
        </div>
    </div>
    </div>
</header>


<article class="container">
        <div class="row">
<!--             <div class="col-sm-8">
                <div class="login-main">
                    <h4><i class="fa fa-dashboard"></i> Gorgeous color and design</h4>
                    <span>Some sample description text about the template goes here</span>

                    <h4> <i class="fa fa-money"></i> 100%  fully responsive </h4>
                    <span>Another description text about the template goes here</span>

                    <h4><i class="fa fa-mobile-phone"></i> Competible with all browers and mobile devices</h4>
                    <span>Yet another sample description text can be placed in one line</span>

                    <h4> <i class="fa fa-trophy"></i> Easy to use and custmize with mobile friendly and responsive</h4>
                    <span>Your last description text about your startup or business</span>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="">
                
                <h3><i class="fa fa-shield"></i> Register now</h3>
                <hr>
                <div class="form-group">
                  <label class="control-label" for="">Email Address</label>
                  <input type="text" class="form-control" placeholder="Email Address">
                </div>

                <div class="form-group">
                  <label class="control-label" for="">Password</label>
                  <input type="text" class="form-control" placeholder="Password">
                </div>

                <div class="form-group">
                  <label class="control-label" for="">Repeat Password</label>
                  <input type="text" class="form-control" placeholder="Repeat Password">
                </div>

                <div class="">
                    <label>Birthday</label>
                  <div class="form-group">
                      <div class="col-sm-4 multibox">
                        <select class="form-control">
                            <option>Day</option>
                        </select>
                      </div>
                       <div class="col-sm-4 multibox">
                        <select class="form-control">
                            <option>Month</option>
                        </select>
                      </div>
                       <div class="col-sm-4 multibox">
                        <select class="form-control">
                            <option>Year</option>
                        </select>
                      </div>
                   
                  </div>
                </div>
              
                <small>
                    By clicking Sign Up, you agree to our Terms and that you have read our
                     Data Use Policy, including our Cookie Use.
                </small>     
                <div style="height:10px;"></div>
                <div class="form-group">
                  <label class="control-label" for=""></label>
                  <input type="submit" value="Submit" class="btn btn-primary">
                </div>   

                  </div>
            </div>
            </div> -->
        </div>

</article>
<?php $this->load->view('template/footer'); ?>

 



<!-- ALL THE SCRIPTS/PLUGINS -->
<!-- Jquery, Modernizer, Selectivizr & Prefix Free Js -->
<script src="<?php echo base_url(); ?>assets/js/portal-js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/portal-js/modernizr-2.8.3.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/portal-js/selectivizr-min.js"></script>
<!-- Bootstrap Js -->
<script src="<?php echo base_url(); ?>assets/js/portal-js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/portal-js/bootstrap-datetimepicker.min.js"></script>
<!-- Theme Main Js -->
<script src="assets/vendors/fileuploader/src/jquery.fileuploader.min.js" type="text/javascript"></script>
<script src="assets/vendors/fileuploader/examples/default-upload/js/custom.js" type="text/javascript"></script>

<script src="<?php echo base_url(); ?>assets/js/portal-js/main.js"></script>
<!-- AJAX JS-->
<script src="<?php echo base_url(); ?>assets/js/portal-js/ajax.js"></script>
<!--FACEBOOK JS Script-->
<script src="<?php echo base_url(); ?>assets/js/portal-js/fbzahed.js"></script>
<!--<script>
    $(window).load(function () {
        $('#myModal').modal('show');
    });
</script>-->
<!--   <script>
    $(window).load(function()
    {
    $('#sign_up').modal('show');
    });
    </script> -->
<script type="text/javascript">
    $(document).ready(function () {
        $('.date_picker').datepicker({
            format: "yyyy-mm-dd"
        });
    });
</script>
<script>
    $(document).ready(function () {
        $('[data-toggle="popover"]').popover();
    });
</script>
</body>
</html>