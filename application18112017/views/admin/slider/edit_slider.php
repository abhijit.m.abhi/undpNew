
<link href="<?php echo base_url(); ?>assets/vendors/fileuploader/src/jquery.fileuploader.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/vendors/fileuploader/src/jquery.fileuploaderSlider.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/redactor/redactor.css"/>
<script src="<?php echo base_url(); ?>assets/redactor/redactor.min.js"></script>

<script type="text/javascript">
    $(document).ready(
        function () {
            $('.redactor').redactor();
        }
        );
    </script>

    <style type="text/css">
    .thumb {
        width: 50px;
        height: 50px;
    }

    #list {
        position: absolute;
        top: 0px;
    }

    #list div {
        float: left;
        margin-right: 10px;
    }

    .hovereffect {
        
        margin: auto;
        overflow: hidden;
        transition: all 1s ease 0s;
    }

    .z-img img {
        transition: all 0.6s ease 0s;
    }

    .hovereffect:hover .z-img img {
        transform: scale(1.9);
    }

    .z-text {
        position: absolute;
        width: 40%;
        height: 100px;
        border: 1px solid gray;
        top: -199px;
        z-index: 1111;
        display: none;
        color: white;
        text-align: center;
        padding: 10px;
        border: 1px solid white;

    }

</style>

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h4>Edit Slider</h4>
            </div>
            <div class="ibox-content">
                <br>

                <div class="row">
                  
                       <form  method="post" action="<?php echo site_url().'/admin/insertEditedsliderSetup/'.$all_slider->ID ?>" enctype="multipart/form-data">
     
                    <div class="form-group">
                       
                        <label class="col-sm-2 control-label">Slider Image Title</label>

                        <div class="col-sm-4">
                           
                               <?php echo form_input(array('class' => 'form-control', 'placeholder' => 'Slider Title ', 'id' => 'title', 'name' => 'title', 'value' => $all_slider->IMAGE_TITLE,'required' => 'required')); ?>      
                            
                        </div>
                        <br clear="all"/>
                    </div>
                    <div class="form-group">
                        
                         <label class="col-sm-2 control-label">Slider Description</label>

                        <div class="col-sm-4">
                       <!--    <?php echo form_input(array('class' => 'redactor', 'placeholder' => 'Slider Description ', 'id' => 'description', 'name' => 'description', 'value' => $all_slider->IMAGE_DESC,'required' => 'required')); ?> -->    
                            
                                <textarea class="redactor"  name="description" value=""><?php echo $all_slider->IMAGE_DESC ?></textarea>
                            
                        </div>
                        <br clear="all"/>
                    </div>
          


                    <div class="form-group">
                        
                         <label class="col-sm-2 control-label"></label>

                        <div class="col-sm-4">
                         <input type="hidden" name="ext_image" value="<?php echo $all_slider->IMAGE_PATH; ?>">

                <?php $images = explode('___', $all_slider->IMAGE_PATH); ?>


                <span id="blk-image" class="toHide" style="display:block; padding: 0px; margin: 0px;">
                    <?php if (!empty($all_slider->IMAGE_PATH)) { ?>
                        <div class="row new_rz_box2">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        
                            <?php foreach ($images as $image): ?>
                                <div class="col-sm-3">
                                    <div class="hovereffect">
                                        <div class="z-text">
                                            <button type="button" style="margin-top: 30%"
                                                    class="btn btn-danger delete-button" title="Delete"
                                                    blog-id="<?php echo $all_slider->ID ?>"
                                                    image-name="<?php echo $image ?>"><i class="fa fa-remove"
                                                                                         aria-hidden="true"></i></button>
                                        </div>
                                        <div class="z-img">
                                            <img src="<?php echo base_url(); ?>upload/inclusive_slider/<?php echo $image ?>" alt="post img"
                                                 class="image pull-left img-responsive postImg img-thumbnail margin10"
                                                 height="200px">
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <?php } ?>
                            
                                  <input type="file" name="files" class="form-control " id="photo" >
                                  <span style="color:red;font-size:14px;">(Maximum Image size:1137*400)</span>
                            
                        </div>
                        <br clear="all"/>
                    </div>
          

          
                  
                    <button class="col-sm-offset-2 btn btn-primary btn-sm " type="submit">Submit</button>
                  </form>
                </div>

            </div>
        </div>

<script type="text/javascript">
  $('input[name="files"]').fileuploader({
            // Options will go here
  });
</script>



<script>
//    $(function () {
        $('.blog-type input[type=radio]').on('change', function () {

//            alert($(this).val());

            $('.toHide').hide();
            $("#blk-" + $(this).val()).fadeIn(1000);
        });


        $('.video-url-type input[type=radio]').on('change', function () {
            $("#video-url-name").text($(this).attr("text"));
        });
//    });
</script>

<script>
    initSample();
</script>
<script>
    function handleFileSelect(evt) {
        var files = evt.target.files; // FileList object

        // Loop through the FileList and render image files as thumbnails.
        for (var i = 0, f; f = files[i]; i++) {

            // Only process image files.
            if (!f.type.match('image.*')) {
                continue;
            }

            var reader = new FileReader();

            // Closure to capture the file information.
            reader.onload = (function (theFile) {
                return function (e) {
                    // Render thumbnail.
                    var span = document.createElement('div');
                    span.setAttribute('class', 'img_wrpv');
                    span.innerHTML = ['<a href="#" style="color:red; font-size:18px; position: relative; z-index: 200;" class="img_revg">x</a><img width="50px" height="50px" class="thumb" src="', e.target.result,
                        '" title="', escape(theFile.name), '"/>'].join('');
                    document.getElementById('list').insertBefore(span, null);
                };
            })(f);

            // Read in the image file as a data URL.
            reader.readAsDataURL(f);

            //remove image
            $('.img_revg').on('click', function (e) {
                e.preventDefault();
                var parent = $(this).parent();
                parent.remove();
            });

        }
    }

    document.getElementById('galleryfiles').addEventListener('change', handleFileSelect, true);
</script>

<script>
    $(function () {
        $("#datepicker").datepicker({format: 'yyyy-mm-dd'});
        $("#datepicker2").datepicker({format: 'yyyy-mm-dd'});
    });
</script>

<script>
    $(document).ready(function () {
        $('.hovereffect').hover(function () {
            $(this).children('.z-text').css('top', '0').fadeToggle(600);
        });
    });

</script>

<script>
    $(document).ready(function () {
        $(".delete-button").click(function () {
            var opp_id = $(this).attr('blog-id');
            var image_name = $(this).attr('image-name');
            var removeImage = $(this).parent().parent().parent();
            $.ajax({
                url: '../delete_slider_edit',
                type: 'POST',
                data: {
                    opp_id: opp_id,
                    img_name: image_name,
                    //action: 'blogImageDelete'
                }
            }).done(function (response) {
                var res = $.parseJSON(response);
                alert(res.msg);
                if (res.ack) {
                    removeImage.fadeOut();
                }
            })
        });
    });
</script>