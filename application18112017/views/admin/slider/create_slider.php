
<link href="<?php echo base_url(); ?>assets/vendors/fileuploader/src/jquery.fileuploader.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/vendors/fileuploader/src/jquery.fileuploaderSlider.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/redactor/redactor.css"/>
<script src="<?php echo base_url(); ?>assets/redactor/redactor.min.js"></script>

<script type="text/javascript">
    $(document).ready(
        function () {
            $('.redactor').redactor();
        }
        );
    </script>

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h4>Create Slider</h4>
            </div>
            <div class="ibox-content">
                <br>

                <div class="row">
                    <?php echo form_open_multipart('admin/createNewSlider', "class='form-horizontal margin-none'"); ?>
     
                    <div class="form-group">
                       
                        <label class="col-sm-2 control-label">Slider Image Title</label>

                        <div class="col-sm-4">
                           
                               <?php echo form_input(array('class' => 'form-control', 'placeholder' => 'Slider Title ', 'id' => 'title', 'name' => 'title', 'value' => set_value('title'), 'required' => 'required')); ?>      
                            
                        </div>
                        <br clear="all"/>
                    </div>
                    <div class="form-group">
                        
                         <label class="col-sm-2 control-label">Slider Description</label>

                        <div class="col-sm-4">
                            
                                <textarea class="redactor" name="description"></textarea>
                            
                        </div>
                        <br clear="all"/>
                    </div>
          


                    <div class="form-group">
                        
                         <label class="col-sm-2 control-label"></label>

                        <div class="col-sm-4">
                            
                                  <input type="file" name="files" class="form-control " id="photo" required >
                                  <span style="color:red;font-size:14px;">(Maximum Image size:1137*400)</span>
                            
                        </div>
                        <br clear="all"/>
                    </div>
          

          
                  
                    <button class="col-sm-offset-2 btn btn-primary btn-sm " type="submit">Submit</button>
                    <?php echo form_close(); ?>
                </div>

            </div>
        </div>

<script type="text/javascript">
  $('input[name="files"]').fileuploader({
            // Options will go here
  });
</script>