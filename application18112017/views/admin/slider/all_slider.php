<style>
  
    .slider_img{ height: 80px; width: 80px; }
</style> 
        
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Slider List</h5>

                <div class="ibox-tools">
        
                         <a title="View user information" class="btn btn-primary btn-xs pull-right" href="<?php echo site_url() ?>/admin/createNewSlider" target=""> Create New</a>   

                </div>

            </div>

            <div class="ibox-content">

                <div class="table-responsive contentArea">
                    <div class="row">

                    </div>
                    <br>

                    <?php
                    if (!empty($all_slider)) {
                        ?>
                       <table class="table table-striped table-bordered table-hover gridTable">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Slider Image Title</th>
                                <th>Slider Description</th>
                                <th>Slider Image</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i = 1;
                            foreach ($all_slider as $all_sliders) {
                                ?>
                                <tr id="slider_row_<?php echo $all_sliders->ID; ?>">
                                  <td><?php echo $i++; ?></td>
                                 
                                    <td><?php echo $all_sliders->IMAGE_TITLE; ?></td>
                                    <td><?php echo $all_sliders->IMAGE_DESC; ?></td>
                                    <td><img class="slider_img" src="<?php echo base_url('upload/inclusive_slider/'.$all_sliders->IMAGE_PATH); ?>"/></td>
                                 
                                <td>    <?php if($rejectPerm>0){ ?>
                                        <a class="btn btn-warning btn-xs" href="<?php echo site_url().'/Admin/editsliderSetup/'.$all_sliders->ID; ?>"
                                              title="Update Image Slider"><i
                                                class="fa fa-edit"></i></a>
                                            <?php 
                                            }
                                            ?> 
                                        <?php if($rejectPerm>0){ ?>
                                        <span class="btn btn-danger btn-xs sliderDltBtn"
                                              slider_id="<?php echo $all_sliders->ID; ?>" title="Delete"><i
                                                class="fa fa-trash-o"></i></span>
                                            <?php 
                                            }
                                            ?>
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    <?php
                    } else {
                        echo "<p class='text-danh text-danger'><br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No Data Found</p>";
                    }
                    ?>
                </div>
            </div>

        </div>

<script src="<?php echo base_url(); ?>assets/security_access/bootgrid/jquery.bootgrid.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#data-table-basic").bootgrid({
            css: {
                icon: 'md icon',
                iconColumns: 'md-view-module',
                iconDown: 'md-expand-more',
                iconRefresh: 'md-refresh',
                iconUp: 'md-expand-less'
            }
        });
        $(document).on("click", "#createModule", function () {
            var mod_name = $("#txtModuleName").val();
            var mod_name_bn = $("#txtModuleNameBn").val();
            var sl_no = $("#SL_NO").val();
            var status = $("#ACTIVE_STATUS").prop('checked');

            $.ajax({
                type: "POST",
                url: "<?php echo site_url('securityAccess/createModule'); ?>",
                data: {mod_name: mod_name, mod_name_bn: mod_name_bn, sl_no: sl_no, status: status},
                success: function (result) {
                    $(".modal_msg").html(result);
                    window.location.reload(true);
                }
            });
        });
        $(document).on("click", ".statusType", function () {
            var status = $(this).attr('status');
            var linkId = $(this).attr('data-linkId');
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('admin/setup/changeModuleStatus'); ?>",
                data: {linkid: linkId, status: status},
                success: function (result) {
                    window.location.reload(true);
                }
            });
        });


        $(".sliderDltBtn").click(function () {
            if (confirm("Are you want to delete Slider?")) {
                var slider_id = $(this).attr('slider_id');
                $.ajax({
                    url: '<?php echo site_url('admin/delete_slider_from_db'); ?>',
                    type: 'POST',
                    data: {slider_id: slider_id},
                    success: function (data) {
                        if (data == 1) {
                            alert('Slider deleted Successfully');
                            $('#slider_row_' + slider_id).remove();
                        }
                    }
                });
            } else {
                return false;
            }
        });
    });
</script>