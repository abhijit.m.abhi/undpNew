    <style type="text/css">
     img.img-circled{
          max-width: 150px;
          height: auto;
          vertical-align: middle;
            }
    </style>

    <h3 class="box-title">User Details</h3>
    <table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
        <tr>
            <th width="20%">ID</th>
            <td><?php echo $regIndividual_detail->userId?></td>
        </tr>
        <tr>
            <th width="20%">Full Name</th>
            <td><?php echo $regIndividual_detail->fname?></td>
        </tr>
        <tr>
            <th>Email</th>
            <td><?php  echo $regIndividual_detail->email?></td>
        </tr>
        <tr>
            <th>Facebook ID</th>
            <td><?php  echo $regIndividual_detail->facebookId?></td>
        </tr>
        <tr>
            <th>Gmail ID</th>
            <td><?php  echo $regIndividual_detail->gmail_id?></td>
        </tr>
        <tr>
            <th>Date of Birth</th>
            <td><?php echo $regIndividual_detail->date?></td>
        </tr>
        <tr>
            <th>Contact Number</th>
            <td><?php echo $regIndividual_detail->number?></td>
        </tr>
        <tr>
            <th>Gender</th>
            <td><?php echo $regIndividual_detail->gender?></td>
        </tr>
        <tr>
            <th>Photo</th>
            <td>  

             <?php if ($regIndividual_detail->login_method =='D') : ?>
                    <?php
                        $user_img = "".$regIndividual_detail->image;
                        ?>
                        <pull-left>
                        <img class="img-circled"  src="<?php echo base_url($user_img) ?>" alt="" />
                      </pull-left>
                 

              <?php elseif($regIndividual_detail->login_method=='F') : ?>
                <?php 
                $user_img_fb =$regIndividual_detail->image;
                      ?>
                <pull-left>
                    <img class="img-circled" src="<?php echo $user_img_fb; ?>" alt=""/>
                     </pull-left>

            <?php elseif($regIndividual_detail->login_method=='G') : ?>
                <?php 
                   $user_img_gmail =$regIndividual_detail->image;
                      ?>
              <pull-left>
                    <img class="img-circled" src="<?php echo $user_img_gmail; ?>" alt=""/>
                    </pull-left>
                <?php else : ?>
                    <pull-left>
                    <img class="img-circled" src="<?php echo base_url(); ?>assets/img/avatar.png" alt=""/>
                     </pull-left>




             <?php endif; ?>

 

                      </td>
        </tr>
        <tr>
            <th>Type</th>
            <td><?php echo $regIndividual_detail->type?></td>
        </tr>
        <tr>
       

          <tr>
            <th>Status</th>
            <td><?php echo ($regIndividual_detail->active == 1) ? '<span class="btn btn-success btn-xs">Active</span>' : '<span class="btn btn-danger btn-xs">Inactive</span>'; ?></td>
        </tr>
        <tr>
            <th>Registration Date & Time</th>
            <td><?php echo $regIndividual_detail->created_at?></td>
        </tr>
         <tr>
            <th>Login With </th>
               <td class="center">
                                    <?php

                                    if (!empty($regIndividual_detail->facebookId)) {
                                        echo  '<span class="btn btn-success btn-xs">Facebook</span>';
                                    } elseif (!empty($regIndividual_detail->gmail_id)) {
                                        echo  '<span class="btn btn-success btn-xs">Gmail</span>';
                                    } else {
                                        echo  '<span class="btn btn-success btn-xs">Website</span>';
                                    }
                                    ?>
                                    </td>
         </tr>

         <tr>
            <th>Last Login</th>
            <td><?php echo $regIndividual_detail->login_time?></td>
        </tr>
    </table>


<script src="<?php echo base_url(); ?>assets/security_access/bootgrid/jquery.bootgrid.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#data-table-basic").bootgrid({
            css: {
                icon: 'md icon',
                iconColumns: 'md-view-module',
                iconDown: 'md-expand-more',
                iconRefresh: 'md-refresh',
                iconUp: 'md-expand-less'
            }
        });
        $(document).on("click", "#createModule", function () {
            var mod_name = $("#txtModuleName").val();
            var mod_name_bn = $("#txtModuleNameBn").val();
            var sl_no = $("#SL_NO").val();
            var status = $("#ACTIVE_STATUS").prop('checked');

            $.ajax({
                type: "POST",
                url: "<?php echo site_url('securityAccess/createModule'); ?>",
                data: {mod_name: mod_name, mod_name_bn: mod_name_bn, sl_no: sl_no, status: status},
                success: function (result) {
                    $(".modal_msg").html(result);
                    window.location.reload(true);
                }
            });
        });
        $(document).on("click", ".statusType", function () {
            var status = $(this).attr('status');
            var linkId = $(this).attr('data-linkId');
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('admin/setup/changeModuleStatus'); ?>",
                data: {linkid: linkId, status: status},
                success: function (result) {
                    window.location.reload(true);
                }
            });
        });


        $(".modDltBtn").click(function () {
            if (confirm("Are you want to delete?")) {
                var mod_id = $(this).attr('mod_id');
                $.ajax({
                    url: '<?php echo site_url('securityAccess/delete_module_from_db'); ?>',
                    type: 'POST',
                    data: {mod_id: mod_id},
                    success: function (data) {
                        if (data == 1) {
                            alert('Module deleted Successfully');
                            $('#mod_row_' + mod_id).remove();
                        }
                    }
                });
            } else {
                return false;
            }
        });
    });
</script>