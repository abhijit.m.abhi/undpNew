 <style type="text/css">
     img.img-circled{
          max-width: 150px;
          height: auto;
          vertical-align: middle;
            }
    </style>

    <style>
  
    .slider_img{ height: 200px; width: 200px; }
   </style> 

    
    <form class="form-horizontal frmContent" id="event" method="post">
    <div class="block-flat">
        <span class="frmMsg"></span><br>
        <h3 class="box-title">Directory Details</h3>
    <table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
      
         
        <tr>
            <th>Name</th>
            <td><?php echo $organization_detail->title?></td>
            <th>Email</th>
            <td><?php echo $organization_detail->oemail?></td>
            <th>Category</th>
            <td><?php echo $organization_detail->category?></td>

        </tr>
     
        </tr>
         <tr>
            <th>Description</th>
            <td><?php echo $organization_detail->description?></td>
            <th>Location</th>
            <td><?php echo $organization_detail->location?></td>
            <th>Latitude</th>
            <td><?php echo $organization_detail->latitude?></td>
        </tr>

        <tr>
            <th>Longitude</th>
            <td><?php echo $organization_detail->longitude?></td>
            <th>District</th>
            <td><?php echo $organization_detail->district?></td>
            <th>Country</th>
            <td><?php echo $organization_detail->country?></td>
        </tr>
   
         <tr>
            <th>Phone</th>
            <td><?php echo $organization_detail->phone?></td>
            <th>Mobile</th>
            <td><?php echo $organization_detail->mobile?></td>
            <th>Website</th>
            <td><?php echo $organization_detail->url?></td>
            </tr>
            <tr>
            <th>Facebook</th>
            <td><?php echo $organization_detail->facebook?></td>
            <th>Twitter</th>
            <td><?php echo $organization_detail->twitter?></td>
            <th>Youtube</th>
            <td><?php echo $organization_detail->youtube?></td>
            </tr>
            <tr>
            <th>Pinterest</th>
            <td><?php echo $organization_detail->pinterest?></td>
            <th>Head of Organization</th>
            <td><?php echo $organization_detail->hoo?></td>
            <th>Designation</th>
            <td><?php echo $organization_detail->hoodesignation?></td>
            
           </tr>

            <tr>
            <th>Email Address</th>
            <td><?php echo $organization_detail->hooemail?></td>
            <th>contact person</th>
            <td><?php echo $organization_detail->contactperson?></td>
            <th>Designation</th>
            <td><?php echo $organization_detail->cpdesignation?></td>
            
           </tr>
            <tr>
            <th>Phone</th>
            <td><?php echo $organization_detail->cpperson?></td>
            <th>Email Address</th>
            <td><?php echo $organization_detail->cpemail?></td>
            <th>Establishment</th>
            <td><?php echo $organization_detail->edo?></td>
            
           </tr>
            <tr>
            <th>District</th>
            <td><?php echo $organization_detail->nod?></td>
            <th>Full Time</th>
            <td><?php echo $organization_detail->fte?></td>
            <th>Part Time</th>
            <td><?php echo $organization_detail->pte?></td>
           </tr>
            <tr>
            <th>volunteers</th>
            <td><?php echo $organization_detail->volunteers?></td>
            <th>Interest Area</th>
            <td><?php echo $organization_detail->iao?></td>
            <th>Registration Authority</th>
            <td><?php echo $organization_detail->rauthority?></td>
            
           </tr>

          <tr>
            <th>Gallery Image</th>
            <td> 
            <?php if($organization_detail->path != ""){
                      $images = explode('___', $organization_detail->path); 
                      ?>
                              <?php foreach ($images as $image): ?>
                          <div class="col-sm-3">
                           <img  class="img-responsive" src="<?php echo base_url(); ?>upload/organization/gallery/<?php echo $image; ?>"/>
                           </div>
                            <?php endforeach; ?>
                     
                      <?php } else{?>
                       <img style="max-height:100px; width:100px; padding-bottom: 4px;" src="<?php echo base_url(); ?>assets/img/avatar.png" alt="User Photo"/>
               
                     
                      <?php } ?> <br>
                      
            </td>
            <th>Learn About</th>
            <td><?php echo $organization_detail->learnabout?></td>
              <th>Logo</th>
            <td> 
            <?php if($organization_detail->logo != ""){
                      $images = explode('___', $organization_detail->logo); 
                      ?>
                              <?php foreach ($images as $image): ?>
                          <div class="col-sm-3">
                           <img  class="img-responsive" src="<?php echo base_url(); ?>upload/organization/logo/<?php echo $image; ?>"/>
                           </div>
                            <?php endforeach; ?>
                     
                      <?php } else{?>
                       <img style="max-height:100px; width:100px; padding-bottom: 4px;" src="<?php echo base_url(); ?>assets/img/avatar.png" alt="User Photo"/>
               
                     
                      <?php } ?> <br>
                      
            </td>
        </tr>

           <tr>
            <th>Govt.Registration number</th>
            <td><?php echo $organization_detail->registration_no?></td>
            <th>Govt.Registration date</th>
            <td><?php echo $organization_detail->govt_reg_date?></td>
            <th>Foreign Authority Reg Name.</th>
            <td><?php echo $organization_detail->foreign_reg?></td>
            
           </tr>
            <tr>
            <th>Foreign Authority Reg no.</th>
            <td><?php echo $organization_detail->reg_college?></td>
            <th>Foreign Authority Reg Date</th>
            <td><?php echo $organization_detail->non_gov_reg?></td>
            <th>Reg. College Name</th>
            <td><?php echo $organization_detail->college_name?></td>
        
            
           </tr>

           
    </table>

    </div>        
    <div class="hr-line-dashed"></div>

</form>