
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>User List</h5>

                <div class="ibox-tools">
                    <!-- <span class="btn btn-primary btn-xs pull-right  " data-toggle="modal" href="#modalDefault">Create
                        New </span> -->

                </div>

            </div>

            <div class="ibox-content">
                <div class="table-responsive contentArea">
                    <div class="row">

                    </div>
                    <br>

                    <?php
                    if (!empty($review_org_list)) {
                        ?>
                      <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Org Name</th>
                                <th>Org Email</th>
                                <th>Contact person Name</th>
                                <th>Contact person Email</th>
                                
                                
                                <th>Phone</th>
                                <th>Website</th>
                                <th>Facebook</th>
                                <th>Location</th>
                                <th>Latitude</th>
                                <th>Longitude</th>
                                
                                <th>Registration Date-Time</th>
                                
                                <th>Login With</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i = 1;
                            foreach ($review_org_list as $review_org_lists) {
                                ?>
                                <tr id="org_row_<?php echo $review_org_lists->userId; ?>">
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $review_org_lists->oname; ?></td>
                                    <td><?php echo $review_org_lists->oemail; ?></td>
                                    <td><?php echo $review_org_lists->fname; ?></td>
                                    <td><?php echo $review_org_lists->email; ?></td>
                                    <td><?php echo $review_org_lists->number; ?></td>
                                    <td><?php echo $review_org_lists->website; ?></td>
                                    <td><?php echo $review_org_lists->facebook; ?></td>
                                    <td><?php echo $review_org_lists->Location; ?></td>
                                    <td><?php echo $review_org_lists->Latitude; ?></td>
                                    <td><?php echo $review_org_lists->Longitude; ?></td>
                                  
                                    <td><?php echo $review_org_lists->created_at; ?></td>
                                 
                                      <td class="center"><?php echo ($review_org_lists->facebookId!= '') ? '<span class="btn btn-success btn-xs">Facebook</span>' : '<span class="btn btn-danger btn-xs">Website</span>'; ?></td>
                                        <td class="center"><?php echo ($review_org_lists->active == 2) ? '<span class="btn btn-success btn-xs">Pending<br>Verification</span>' : '<span class="btn btn-danger btn-xs">Approval<br>Verification</span>'; ?></td>

                                    <td class="text-center">
                                     <?php if($rejectPerm>0){ ?>
                                        <a class="label label-primary  visitorOrgApprove"
                                                       data-visitor-org="<?php echo $review_org_lists->userId; ?>"
                                                       title="Click For Approved"><i
                                                class="fa fa-check"></i></a>
                                                   <?php 
                                        }
                                        ?>
                                                   <span class="btn btn-danger btn-xs mailSendBtn"
                                                  USER_ID="<?php echo $review_org_lists->userId; ?>"
                                                  FULL_NAME="<?php echo $review_org_lists->oname; ?>"
                                                   USER_MAIL="<?php echo $review_org_lists->oemail; ?>"
                                               title="Click For Send Mail"><i
                                        class="fa fa-envelope"></i></span>
                                         <?php if($rejectPerm>0){ ?>
                                                       <a class="btn btn-danger btn-xs   orgDltBtn"
                                                       org_id="<?php echo $review_org_lists->userId; ?>"
                                                       title="Click For Delete"><i
                                                class="fa fa-remove"></i></a>
                                                           <?php 
                                        }
                                        ?>
                                                
                                    </td>

                                </tr>
                            <?php
                              $i++;
                            }

                            ?>
                            </tbody>
                        </table>
                    <?php
                    } else {
                        echo "<p class='text-danh text-danger'><br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No Data Found</p>";
                    }
                    ?>
                </div>
            </div>

        </div>
        <div class="modal inmodal appModal">
    <div class="modal-dialog">
        <div class="modal-content animated">
            <div class="modal-header">
                <button data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span><span
                            class="sr-only">Close</span></button>
                <h4 class="modal-title"></h4>
                <small class="font-bold"></small>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-white" type="button">Close</button>
            </div>
        </div>
    </div>
</div>

        <script>
    $(document).ready(function() {
        $('#example1').DataTable( {
            "scrollX": true,
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'pdf',
                    orientation:'landscape',
                    exportOptions: {
                        columns: [0,1,2,3,4,5,6,7,8,9,10,11,12]
                    }
                },

                {
                    extend: 'excel',
                    exportOptions: {
                        columns: [0,1,2,3,4,5,6,7,8,9,10,11,12]
                    }
                }

            ]
        } );
    } );
</script>

<!-- <div class="modal fade" id="modalDefault" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span
                        aria-hidden="true">×</span></button>
                <h4 id="gridModalLabel" class="modal-title">Create Module</h4>
            </div>
            <div class="modal-body">

                <div class="form-group">
                    <label class="col-sm-3 control-label" for="txtModuleName">Module Name</label>

                    <div class="col-sm-8">
                        <div class="fg-line">
                            <input type="text" name="txtModuleName" class="form-control" id="txtModuleName" value=""
                                   placeholder="Enter Module Name" />
                        </div>
                    </div>
                    <br clear="all" />
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="txtModuleNameBn">Module Name Bangla</label>

                    <div class="col-sm-8">
                        <div class="fg-line">
                            <input type="text" name="txtModuleNameBn" id="txtModuleNameBn" class="form-control" value=""
                                   placeholder="Enter Module Bangla Name" />
                        </div>
                    </div>
                    <br clear="all" />
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="SL_NO">Serial No</label>

                    <div class="col-sm-8">
                        <div class="fg-line">
                            <input type="text" name="SL_NO" id="SL_NO" class="form-control" value=""
                                   placeholder="Enter Module Serial" />
                        </div>
                    </div>
                    <br clear="all" />
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="SL_NO">Module Icon</label>

                    <div class="col-sm-8">
                        <div class="fg-line">
                            <input type="text" name="MODULE_ICON" id="MODULE_ICON" class="form-control" value=""
                                   placeholder="Enter Module Icon" />
                        </div>
                    </div>
                    <br clear="all" />
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="txtModLink">Action </label>

                    <div class="col-sm-8">
                        <div class="fg-line">
                            <?php
                            $chkStatus = array(
                                'name' => 'ACTIVE_STATUS',
                                'id' => 'ACTIVE_STATUS',
                                'value' => '1',
                                'style' => 'margin-right:5px'
                            );
                            echo form_checkbox($chkStatus);
                            ?>
                        </div>
                    </div>
                    <br clear="all" />
                </div>

            </div>
            <div class="modal-footer">
                <span class="modal_msg pull-left" style="color: green"></span>
                <button type="button" class="btn btn-primary" id="createModule">Save changes</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div> -->

<script src="<?php echo base_url(); ?>assets/security_access/bootgrid/jquery.bootgrid.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () 
    {
        $("#data-table-basic").bootgrid({
            css: {
                icon: 'md icon',
                iconColumns: 'md-view-module',
                iconDown: 'md-expand-more',
                iconRefresh: 'md-refresh',
                iconUp: 'md-expand-less'
            }
        });
         $(".mailSendBtn").click(function () {
            if (confirm("Are you want to send mail?")) {
                var USER_ID = $(this).attr('USER_ID');
                var USER_MAIL =$(this).attr('USER_MAIL');
                var FULL_NAME = $(this).attr('FULL_NAME');
                $.ajax({
                    url: '<?php echo site_url('admin/again_send_mail_review'); ?>',
                    type: 'POST',
                    data: {USERID: USER_ID,FULL_NAME: FULL_NAME,USER_MAIL: USER_MAIL},
                    success: function (data) {
                        if (data == 1) {
                            alert('Mail Send Successfully');
                            
                        }
                    }
                });
            } else {
                return false;
            }
        });
        $(document).on("click", "#createModule", function () {
            var mod_name = $("#txtModuleName").val();
            var mod_name_bn = $("#txtModuleNameBn").val();
            var sl_no = $("#SL_NO").val();
            var status = $("#ACTIVE_STATUS").prop('checked');

            $.ajax({
                type: "POST",
                url: "<?php echo site_url('securityAccess/createModule'); ?>",
                data: {mod_name: mod_name, mod_name_bn: mod_name_bn, sl_no: sl_no, status: status},
                success: function (result) {
                    $(".modal_msg").html(result);
                    window.location.reload(true);
                }
            });
        });
        $(document).on("click", ".statusType", function () {
            var status = $(this).attr('status');
            var linkId = $(this).attr('data-linkId');
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('admin/setup/changeModuleStatus'); ?>",
                data: {linkid: linkId, status: status},
                success: function (result) {
                    window.location.reload(true);
                }
            });
        });


        $(".modDltBtn").click(function () {
            if (confirm("Are you want to delete?")) {
                var mod_id = $(this).attr('mod_id');
                $.ajax({
                    url: '<?php echo site_url('securityAccess/delete_module_from_db'); ?>',
                    type: 'POST',
                    data: {mod_id: mod_id},
                    success: function (data) {
                        if (data == 1) {
                            alert('Module deleted Successfully');
                            $('#mod_row_' + mod_id).remove();
                        }
                    }
                });
            } else {
                return false;
            }
        }); 

$(document).on("click", ".visitorOrgApprove", function () {
        var orgId = $(this).attr("data-visitor-org");
        //alert(orgId);
        $(".appModal").modal();
        $.ajax({
            type: "POST",
            data: {orgId: orgId},
            url: "<?php echo site_url() ?>/Admin/user_org_detail",
            beforeSend: function () {
                $(".appModal .modal-title").html("Organization Details");
                $(".appModal .modal-body").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
            },
            success: function (data) {
                $(".appModal .modal-body").html(data);
                if (data == 'Mail Send successfully') {

                }
            }
        });
    });

$(document).on("click", ".orgDetails", function () {
        var orgId = $(this).attr("data-visitor-org");
        //alert(orgId);
        $(".appModal").modal();
        $.ajax({
            type: "POST",
            data: {orgId: orgId},
            url: "<?php echo site_url() ?>/Admin/user_org_detail",
            beforeSend: function () {
                $(".appModal .modal-title").html("Organization Details");
                $(".appModal .modal-body").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
            },
            success: function (data) {
                $(".appModal .modal-body").html(data);
                if (data == 'Mail Send successfully') {

                }
            }
        });
    });


 $(document).on("click", ".formApproveSubmit", function () {
        if (confirm("Are You Sure?")) {
            var USER_ID = $("#USER_ID").val();
            //alert(USER_ID);
            var ACTIVE_FLAG = $("#ACTIVE_FLAG").val();
            var FULL_NAME = $("#FULL_NAME").val();
            var USER_MAIL = $("#USER_MAIL").val(); 
            $.ajax({
                type: "post",
                url: '<?php echo site_url('admin/update_visitor') ?>',
                data: {USER_ID: USER_ID, ACTIVE_FLAG: ACTIVE_FLAG,FULL_NAME: FULL_NAME,USER_MAIL: USER_MAIL},
                success: function (data) {
                    $(".frmMsg").html(data);
                 window.location.reload(true);
                }
            });
        } else {
            return false;
        }
    });
  $(".orgDltBtn").click(function () {
            if (confirm("Are you want to delete?")) {
                var org_id = $(this).attr('org_id');
                $.ajax({
                    url: '<?php echo site_url('admin/delete_review_org_from_db'); ?>',
                    type: 'POST',
                    data: {org_id: org_id},
                    success: function (data) {
                        if (data == 1) {
                            alert('Organization review deleted Successfully');
                            $('#org_row_' + org_id).remove();
                        }
                    }
                });
            } else {
                return false;
            }
        });
    });


</script>