 <style type="text/css">
     img.img-circled{
          max-width: 150px;
          height: auto;
          vertical-align: middle;
            }
    </style>

    
    <form class="form-horizontal frmContent" id="event" method="post">
    <div class="block-flat">
        <span class="frmMsg"></span><br>
        <h3 class="box-title">User Details</h3>

     <div class="form-group"><label class="col-lg-3 control-label">Active?</label>

                    <div class="col-lg-9">
                    <?php echo form_checkbox(array('name' => 'ACTIVE_FLAG', 'id' => 'ACTIVE_FLAG', 'value' => $ban_org_detail->active, 'checked' => ($ban_org_detail->active == 0) ? TRUE : FALSE)); ?>
                    <input type="hidden" name="FULL_NAME" id="FULL_NAME" value="<?php echo $user_detail->oname; ?>">
                    <input type="hidden" name="USER_MAIL" id="USER_MAIL" value="<?php echo $user_detail->oemail; ?>">          
                    </div>
                </div>
    </div>        
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <div class="col-lg-offset-2 col-lg-10"> 
            <input type="hidden" name="USER_ID" id="USER_ID" value="<?php echo  $user_detail->userId; ?>">
            <input type="button" class="btn btn-primary btn-sm formApproveSubmit" value="Submit">    
            <input type="reset" class="btn btn-default btn-sm" value="Reset">
            <span class="loadingImg"></span>
        </div>
    </div>
</form>