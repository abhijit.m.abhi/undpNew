 <style type="text/css">
     img.img-circled{
          max-width: 150px;
          height: auto;
          vertical-align: middle;
            }
    </style>

    
    <form class="form-horizontal frmContent" id="event" method="post">
    <div class="block-flat">
        <span class="frmMsg"></span><br>
        <h3 class="box-title">User Details</h3>
    <table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
      
         
        <tr>
            <th>Name</th>
            <td><?php echo $inclusive_bd_detail->oname?></td>
            <th>Email</th>
            <td><?php echo $inclusive_bd_detail->email?></td>
            <th>Organization/Individual</th>
            <td><?php echo $inclusive_bd_detail->type?></td>
        </tr>
      
        <tr>
            <th>Post Type</th>
            <td><?php echo $inclusive_bd_detail->LOOKUP_DATA_NAME?></td>
            <th>Post Title</th>
            <td><?php echo $inclusive_bd_detail->title?></td>
            <th>Post Date</th>
            <td><?php echo $inclusive_bd_detail->post_date?></td>
        </tr>
  

            <tr>
            <th>Inclusive Bangladesh Image</th>
            <td> 
               <?php if($inclusive_bd_detail->images != ""){
                      $images = explode('___', $inclusive_bd_detail->images); 
                      ?>
                              <?php foreach ($images as $image): ?>
                          <div class="col-sm-3">
                           <img  class="img-responsive" src="<?php echo base_url(); ?>upload/inclusivebd/<?php echo $image; ?>"/>
                           </div>
                            <?php endforeach; ?>
                     
                      <?php } else{?>
                       <img style="max-height: 200px; width: auto; padding-bottom: 4px;" src="<?php echo base_url(); ?>assets/img/avatar.png" alt="User Photo"/>
               
                     
                      <?php } ?> <br>
           </td>
           <th>URL Type</th>
            <td><?php echo $inclusive_bd_detail->url_type?></td>
            <th>Total View</th>
            <td><?php echo $inclusive_bd_detail->view_count?></td>
        </tr>

         <tr>
            <th>Total Emo</th>
            <td><?php echo $inclusive_bd_detail->TOTAL_EMO?></td>
            <th>Status</th>
            <td><?php
                  if ($inclusive_bd_detail->active_status=='Y') {
                        echo '<span style="color:orange;font-size:12px;">Approved</span>';
                    } elseif ($inclusive_bd_detail->active_status=='N') {
                        echo '<span style="color:green;">Pending</span>';
                    } 
                     elseif ($inclusive_bd_detail->active_status=='R') {
                        echo '<span style="color:red;">Rejected</span>';
                    } else {
                        echo " ";
                    }
                    ?></td>
        </tr>

           <tr>
             <th>Post Description</th>
             <td colspan="5"><?php echo htmlspecialchars_decode(html_entity_decode($inclusive_bd_detail->description)); ?></td>

           </tr>
    </table>
     <div class="form-group"><label class="col-lg-3 control-label">Active?</label>

                    <div class="col-lg-9">
                    <?php echo form_checkbox(array('name' => 'active_status', 'id' => 'active_status', 'value' => $inclusive_bd_detail->active_status, 'checked' => ($inclusive_bd_detail->active_status =='Y') ? TRUE : FALSE)); ?>
                    
                    </div>
                </div>
    </div>        
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <div class="col-lg-offset-2 col-lg-10"> 
            <input type="hidden" name="id" id="id" value="<?php echo  $inclusive_bd_detail->id; ?>">

            <input type="button" class="btn btn-primary btn-sm formApproveSubmit" value="Submit">    
            <input type="reset" class="btn btn-default btn-sm" value="Reset">
            <span class="loadingImg"></span>
        </div>
    </div>
</form>

<script type="text/javascript">

 $(document).on('click', '#active_status', function () {
        var checkbox_value = ($(this).is(':checked')) ? 'Y' : 'N';
        $("#active_status").val(checkbox_value);
    });

</script>