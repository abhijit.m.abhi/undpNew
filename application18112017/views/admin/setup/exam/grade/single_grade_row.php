<td <?php echo ($row->ACTIVE_STATUS == 1) ? "" : "class='inactive'"; ?>>
    <span><?php echo $sn++; ?></span><span class="hidden"
                                           id="loader_<?php echo $row->GR_ID; ?>"></span>
</td>
<td <?php echo ($row->ACTIVE_STATUS == 1) ? "" : "class='inactive'"; ?>><?php echo $row->GR_POLICY_NAME; ?></td>
<td <?php echo ($row->ACTIVE_STATUS == 1) ? "" : "class='inactive'"; ?>><?php echo $row->DEGREE_NAME; ?></td>
<td class="text-center" <?php echo ($row->ACTIVE_STATUS == 1) ? "" : "class='inactive'"; ?>><?php echo $row->GR_MARKS_FROM; ?></td>
<td class="text-center" <?php echo ($row->ACTIVE_STATUS == 1) ? "" : "class='inactive'"; ?>><?php echo $row->GR_MARKS_TO; ?></td>
<td class="text-center" <?php echo ($row->ACTIVE_STATUS == 1) ? "" : "class='inactive'"; ?>><?php echo $row->GR_LETTER; ?></td>
<td class="text-center" <?php echo ($row->ACTIVE_STATUS == 1) ? "" : "class='inactive'"; ?>><?php echo $row->GRADE_POINT; ?></td>
<td class="text-center" <?php echo ($row->ACTIVE_STATUS == 1) ? "" : "class='inactive'"; ?>>

    <?php if (1) { ?>
        <a class="label label-default openModal" id="<?php echo $row->GR_ID; ?>"
           title="Update Exam Policy Information" data-action="exam/policyFormUpdate"
           data-type="edit"><i class="fa fa-pencil"></i></a>
        <?php
    }
    if (1) {
        ?>
        <a class="label label-danger deleteItem2" id="<?php echo $row->GR_ID; ?>"
           title="Click For Delete" data-type="delete" data-field="GR_ID"
           data-action="exam/deleteItem"
           data-tbl="exam_grade"><i
                class="fa fa-times"></i></a>
        <?php
    }

    if (1) {
        ?>
        <a class="itemStatus2" id="<?php echo $row->GR_ID; ?>"
           data-status="<?php echo $row->ACTIVE_STATUS ?>" data-fieldId="GR_ID"
           data-field="ACTIVE_STATUS" data-tbl="exam_grade" data-action="exam/statusItem"
           data-su-url="exam/gradeById">
            <?php echo ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Inactive">Active</span>' ?>
        </a>
        <?php
    }
    ?>
</td>