 <style type="text/css">
     img.img-circled{
          max-width: 150px;
          height: auto;
          vertical-align: middle;
            }
    </style>

    <style>
  
    .slider_img{ height: 200px; width: 200px; }
   </style> 

    
    <form class="form-horizontal frmContent" id="event" method="post">
    <div class="block-flat">
        <span class="frmMsg"></span><br>
        <h3 class="box-title">Directory Details</h3>
    <table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
      
         
        <tr>
            <th>Name</th>
            <td><?php echo $directorytDetails->title?></td>
            <th>Email</th>
            <td><?php echo $directorytDetails->oemail?></td>
            <th>Category</th>
            <td><?php echo $directorytDetails->category?></td>

        </tr>
     
        </tr>
         <tr>
            <th>Description</th>
            <td><?php echo $directorytDetails->description?></td>
            <th>Location</th>
            <td><?php echo $directorytDetails->location?></td>
            <th>Latitude</th>
            <td><?php echo $directorytDetails->latitude?></td>
        </tr>

        <tr>
            <th>Longitude</th>
            <td><?php echo $directorytDetails->longitude?></td>
            <th>District</th>
            <td><?php echo $directorytDetails->district?></td>
            <th>Country</th>
            <td><?php echo $directorytDetails->country?></td>
        </tr>
   
         <tr>
            <th>Phone</th>
            <td><?php echo $directorytDetails->phone?></td>
            <th>Mobile</th>
            <td><?php echo $directorytDetails->mobile?></td>
            <th>Website</th>
            <td><?php echo $directorytDetails->url?></td>
            </tr>
            <tr>
            <th>Facebook</th>
            <td><?php echo $directorytDetails->facebook?></td>
            <th>Twitter</th>
            <td><?php echo $directorytDetails->twitter?></td>
            <th>Youtube</th>
            <td><?php echo $directorytDetails->youtube?></td>
            </tr>
            <tr>
            <th>Pinterest</th>
            <td><?php echo $directorytDetails->pinterest?></td>
            <th>Head of Organization</th>
            <td><?php echo $directorytDetails->hoo?></td>
            <th>Designation</th>
            <td><?php echo $directorytDetails->hoodesignation?></td>
            
           </tr>

            <tr>
            <th>Email Address</th>
            <td><?php echo $directorytDetails->hooemail?></td>
            <th>contact person</th>
            <td><?php echo $directorytDetails->contactperson?></td>
            <th>Designation</th>
            <td><?php echo $directorytDetails->cpdesignation?></td>
            
           </tr>
            <tr>
            <th>Phone</th>
            <td><?php echo $directorytDetails->cpperson?></td>
            <th>Email Address</th>
            <td><?php echo $directorytDetails->cpemail?></td>
            <th>Establishment</th>
            <td><?php echo $directorytDetails->edo?></td>
            
           </tr>
            <tr>
            <th>District</th>
            <td><?php echo $directorytDetails->nod?></td>
            <th>Full Time</th>
            <td><?php echo $directorytDetails->fte?></td>
            <th>Part Time</th>
            <td><?php echo $directorytDetails->pte?></td>
           </tr>
            <tr>
            <th>volunteers</th>
            <td><?php echo $directorytDetails->volunteers?></td>
            <th>Interest Area</th>
            <td><?php echo $directorytDetails->iao?></td>
            <th>Registration Authority</th>
            <td><?php echo $directorytDetails->rauthority?></td>
            
           </tr>

          <tr>
            <th>Gallery Image</th>
            <td> 
            <?php if($directorytDetails->path!= ""){
                      $images = explode('___', $directorytDetails->path); 
                      ?>
                              <?php foreach ($images as $image): ?>
                          <div class="col-sm-3">
                           <img  class="img-responsive" src="<?php echo base_url(); ?>upload/organization/gallery/<?php echo $image; ?>"/>
                           </div>
                            <?php endforeach; ?>
                     
                      <?php } else{?>
                       <img style="max-height:100px; width: 100px; padding-bottom: 4px;" src="<?php echo base_url(); ?>assets/img/avatar.png" alt="User Photo"/>
               
                     
                      <?php } ?> <br>
                      
            </td>
            <th>Learn About</th>
            <td><?php echo $directorytDetails->learnabout?></td>
              <th>Logo</th>
            <td> 
            <?php if($directorytDetails->logo != ""){
                      $images = explode('___', $directorytDetails->logo); 
                      ?>
                              <?php foreach ($images as $image): ?>
                          <div class="col-sm-3">
                           <img  class="img-responsive" src="<?php echo base_url(); ?>upload/organization/logo/<?php echo $image; ?>"/>
                           </div>
                            <?php endforeach; ?>
                     
                      <?php } else{?>
                       <img style="max-height:100px; width: 100px; padding-bottom: 4px;" src="<?php echo base_url(); ?>assets/img/avatar.png" alt="User Photo"/>
               
                     
                      <?php } ?> <br>
                      
            </td>
        </tr>

           <tr>
            <th>Govt.Registration number</th>
            <td><?php echo $directorytDetails->registration_no?></td>
            <th>Govt.Registration date</th>
            <td><?php echo $directorytDetails->govt_reg_date?></td>
            <th>Foreign Authority Reg Name.</th>
            <td><?php echo $directorytDetails->foreign_reg?></td>
            
           </tr>
            <tr>
            <th>Foreign Authority Reg no.</th>
            <td><?php echo $directorytDetails->reg_college?></td>
            <th>Foreign Authority Reg Date</th>
            <td><?php echo $directorytDetails->non_gov_reg?></td>
            <th>Reg. College Name</th>
            <td><?php echo $directorytDetails->college_name?></td>
        
            
           </tr>

           
    </table>

    </div>        
    <div class="hr-line-dashed"></div>

</form>