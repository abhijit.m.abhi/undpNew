

<html class="no-js" lang="en">
   <!--<![endif]-->
   <head>
      <title>Home | Youthopia Bangla</title>
      <meta charset="utf-8"/>
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
      <meta name="description" content="Youthopia.bangla | Youth Platform at Bangladesh"/>
      <meta name="keywords" content="youthopia.bangla, youthopia, youthopia bangladesh, youth bangladesh, undp, unv, youthopia bangla"/>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="author" content="Youthopia.bangla">
      <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/youthopia_logo.ico">
      <!--google sign in meta-->
      <meta name="google-signin-client_id"
         content="620679733936-id09lrcd16t9bku12u675v9ec6q5quuc.apps.googleusercontent.com">
      <!-- CSS -->
      <link href="<?php echo base_url(); ?>assets/css/portal-css/bootstrap.min.css" rel="stylesheet">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/portal-css/datepicker.css" rel="stylesheet">
      <link href="<?php echo base_url(); ?>assets/css/portal-css/font-awesome.min.css" rel="stylesheet">
      <link href="<?php echo base_url(); ?>assets/css/portal-css/ionicons.min.css" rel="stylesheet">
      <link href="<?php echo base_url(); ?>assets/vendors/fileuploader/src/jquery.fileuploader.css" media="all" rel="stylesheet">
      <link href="<?php echo base_url(); ?>assets/css/portal-css/style.css" rel="stylesheet">
      <link href="<?php echo base_url(); ?>assets/css/portal-css/responsive.css" rel="stylesheet">
      <script src="https://apis.google.com/js/platform.js" async defer></script>
      <!--[if (gte IE 6)&(lte IE 8)]>
      <script type="text/javascript" src="selectivizr.js"></script>
      <noscript>
         <link rel="stylesheet" href="[fallback css]"/>
      </noscript>
      <![endif]-->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
   </head>
   <body>
      <style>
         div#homeline1 span#a {
         display: inline;
         }
         label {
         display: inline-block;
         max-width: 100%;
         margin-bottom: 1px;
         font-weight: 700;
         }
         div#homeline1:hover span#a {
         display: none;
         }
         div#homeline1 span#b {
         display: none;
         }
         div#homeline1:hover span#b {
         display: inline;
         }
         @font-face {
         font-family: SolaimanLipi;
         src: url('fonts/SolaimanLipi_new.ttf');
         }
      </style>
      <style>
         .modal-backdrop {
         position: relative;
         }
      </style>
      <style>
         body{
         font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
         background-image: url("../assets/portal-img/img/home_bg.jpg");
         background-repeat: no-repeat;
         }
         .logo i {
         font-size: 31px;
         margin-right: 4px;
         word-spacing: 14px;
         }
         .logo{
         color: white;
         margin: 0;
         font-size: 20px;
         padding: 4px 0;
         padding-bottom: 15px;
         }
         .login-bottom-text{
         margin-top: 0px;
         margin-bottom: 0px;
         font-size: 12px;
         color: white;
         line-height: 19px;
         }
         header{
         /*background: #3b5998;*/
         padding-top: 20px;
         }
         header .form-group{
         margin-bottom: 0px;
         }
         .form-group {
         margin-bottom: 1px;
         }
         header .btn-header-login{
         margin-bottom: 15px;    
         }
         .login-main{
         margin-top: 130px;
         }
         .multibox{
         padding-left: 0px;
         padding-bottom: 10px;
         }
         .login-main span{
         font-size: 12px;
         } 
         footer hr{
         margin-top: 0px;
         padding-top: 0px;
         }
         .footer-options ul{
         clear: both;
         padding: 0px;
         margin: 0px;
         }
         .footer-options ul li{
         float:left;
         list-style: none;
         padding: 5px;
         font-size: 12px;
         }
         .footer-options ul li a{
         text-decoration: none;
         color: #000;
         }
         .copyrights{
         margin-top: 25px;
         }
      </style>
      <header>
         <div class="container">
            <div class="row">
               <div class="col-sm-6">
                  <div class="logo"><img src="https://www.youthopiabangla.org/img/logo.png" alt=""></div>
               </div>
               <?php echo form_open('auth/userLogin', "class='form-vertical'"); ?>
               <?php if (validation_errors()): ?>
               <div class="row">
                  <button data-dismiss="alert" class="close" type="button">×</button>
                  <div class="alert alert-danger">
                     <?php echo validation_errors(); ?>
                  </div>
               </div>
               <?php endif; ?>
               <div class="col-sm-6 hidden-xs">
                  <div class="row">
                     <div class="col-sm-5">
                        <div class="form-group">
                           <input type="text" name="email" class="form-control" value="<?php echo set_value('email'); ?>" placeholder="Email">
                           <div class="login-bottom-text checkbox hidden-sm">
                              <label>
                              <input type="checkbox" id="">
                              Keep me sign in
                              </label>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-5">
                        <div class="form-group">
                           <input type="password" class="form-control" name="password"  placeholder="Password">
                           <div class="login-bottom-text hidden-sm"> Forgot your password?  </div>
                        </div>
                     </div>
                     <div class="col-sm-2">
                        <div class="form-group">
                           <input type="submit" value="login" class="btn btn-default btn-header-login">
                        </div>
                        <?php echo form_close(); ?>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </header>
      <article class="container">
         <div class="row">
            <div class="col-sm-8">
               <div class="login-main">
                  <!--<h4><i class="fa fa-dashboard"></i> Gorgeous color and design</h4>
                     <span>Some sample description text about the template goes here</span>
                     
                     <h4> <i class="fa fa-money"></i> 100%  fully responsive </h4>
                     <span>Another description text about the template goes here</span>
                     
                     <h4><i class="fa fa-mobile-phone"></i> Competible with all browers and mobile devices</h4>
                     <span>Yet another sample description text can be placed in one line</span>
                     
                     <h4> <i class="fa fa-trophy"></i> Easy to use and custmize with mobile friendly and responsive</h4>
                     <span>Your last description text about your startup or business</span>-->
               </div><!--login-main-->
            </div><!--col-sm-8-->
            <div class="col-sm-4">
               <div class="">
                  <h3><i class="fa fa-shield"></i> Register now</h3>
                  <form id='form-id'>
                     <input id='watch-me' name='test' type='radio' value="a" />Individual
                     <input name='test' type='radio' value="b" />Organization
                  </form>
             <div id='show-individual' style='display:none'>
                     <hr>
                     <?php echo form_open_multipart('portal/individualRegis', "class='form-horizontal margin-none'"); ?>
                     <div class="msg">
                        <?php
                           if (validation_errors() != false) {
                               ?>
                        <div class="alert alert-danger">
                           <button data-dismiss="alert" class="close" type="button">×</button>
                           <?php echo validation_errors(); ?>
                        </div>
                        <?php
                           }
                           ?>
                     </div>
                     <div class="form-group">
                        <label class="control-label" for="" style="color:white;">Full Name<span style="color:red;">*</span></label>
                        <input type="text" class="form-control" name="fname" placeholder="Full Name" required="required">
                     </div>
                     <div class="form-group">
                        <label class="control-label" for=""  style="color:white;">Mobile Number <span style="color:red;">*</span></label>
                        <input type="text" class="form-control" name="number" placeholder="Full Name" required="required">
                     </div>
                     <div class="">
                        <br>
                        <div class="form-group">
                           <div class="col-sm-6 multibox">
                              <input class="form-control date_picker" type="text" id="date" name="date"
                                 placeholder="DOB (YY-MM-DD) *" required="required"/>
                           </div>
                           <div class="col-sm-6 multibox">
                              <select class="form-control" name="gender" required="required">
                                 <option value="">Select gender*</option>
                                 <option value="Male">Male</option>
                                 <option value="Female">Female</option>
                                 <option value="Others">Others</option>
                              </select>
                           </div>
                        </div>
                        </div>
                        <div class="form-group"
                           style="display:inline-flex; color:white; margin-bottom: 6px;width: 100%">
                           <!--                                                <label class="control-label" for="file" style="width: 131px;">Image(max-5Mb):</label>-->
                           <input type="file" name="main_image" id="individual-file">
                           <!--                                                <input type="file" name="file" id="file" style="margin-left:1px" required="required" title="image">-->
                        </div>
                        <div class="form-group">
                           <label class="control-label" for="" style="color:white;">Email ID<span style="color:red;">*</span></label>
                           <input type="email" class="form-control" name="email" placeholder="Email ID" required="required">
                        </div>
                        <div class="form-group">
                           <label class="control-label" for="" style="color:white;">Password</label>
                           <input type="password" class="form-control" name="password" placeholder="Password">
                        </div>
                        <div class="form-group" style="color:white;">
                           <label class="control-label" for="">Confirm Password</label>
                           <input type="password" class="form-control" name="password_conf" placeholder="Confirm Password">
                        </div>
                        <div class="form-group" style="color:white;">
                           <p><input class="" type="checkbox" id="agree" name="agree"
                              value="agree" checked/><label for="agree" style="color:Black;">I ACCEPT THE<a href="Terms_and_Conditions.pdf" target="_blank">TERMS &amp;CONDITIONS </a><label> and <a href="Privacy_Policy.pdf" target="_blank"> PRIVACY &amp; POLICY</a></label>
                           </p>
                        </div>
                        <small>
                           <!-- By clicking Sign Up, you agree to our Terms and that you have read our
                              Data Use Policy, including our Cookie Use. -->
                        </small>
                        <div style="height:10px;"></div>
                        <div class="form-group">
                           <label class="control-label" for=""></label>
                           <input type="submit" value="Request for Registration" class="btn btn-primary">
                        </div>
                        <?php echo form_close(); ?>  
                     
                  </div><!--show individual -->
                  <div id='show-org' style='display:none'>
                     <hr>
                     <?php echo form_open_multipart('portal/organizationRegis', "class='form-horizontal margin-none'"); ?>
                     <div class="msg">
                        <?php
                           if (validation_errors() != false) {
                               ?>
                        <div class="alert alert-danger">
                           <button data-dismiss="alert" class="close" type="button">×</button>
                           <?php echo validation_errors(); ?>
                        </div>
                        <?php
                           }
                           ?>
                     </div>
                     <div class="form-group">
                        <label class="control-label" for="" style="color:white;">Organization Name<span style="color:red;">*</span></label>
                        <input type="text" class="form-control" name="oname" placeholder="Organization Name" required="required">
                     </div>
                     <div class="form-group">
                        <label class="control-label" for=""  style="color:white;">Organization Email <span style="color:red;">*</span></label>
                        <input type="email" class="form-control" name="oemail" placeholder="Organization Email" required="required">
                     </div>
                     <div class="form-group">
                        <label class="control-label" for="" style="color:white;">Facebook Page Link <span style="color:red;">*</span></label>
                        <input type="text" class="form-control" name="facebook" placeholder="Facebook Page Link" required="required">
                     </div>
                     <div class="form-group">
                        <label class="control-label" for="" style="color:white;">Website</label>
                        <input type="text" class="form-control" name="website" placeholder="website" required="required">
                     </div>
                     <div class="form-group">
                        <label class="control-label" for="" style="color:white;">Contact Person Name <span style="color:red;">*</span></label>
                        <input type="text" class="form-control" name="fname" placeholder="Contact Person Name" required="required">
                     </div>
                     <div class="form-group">
                        <label class="control-label" for="" style="color:white;">Contact Person Email <span style="color:red;">*</span></label>
                        <input type="email" class="form-control" name="email" placeholder="Contact Person Email" required="required">
                     </div>
                     <div class="form-group">
                        <label class="control-label" for="" style="color:white;">Contact Person Mobile Number <span style="color:red;">*</span></label>
                        <input type="text" class="form-control" name="number" placeholder="Contact Person Mobile Number" required="required">
                     </div>
                     <!-- <div class=""> -->
                        <br>
                        <div class="form-group"
                           style="display:inline-flex; color:white; margin-bottom: 6px;width: 100%">
                           <!--                                                <label class="control-label" for="file" style="width: 131px;">Image(max-5Mb):</label>-->
                           <input type="file" name="main_image" id="individual-file">
                           <!--                                                <input type="file" name="file" id="file" style="margin-left:1px" required="required" title="image">-->
                        </div>
                        <div class="form-group">
                           <label class="control-label" for="" style="color:white;">Password</label>
                           <input type="password" class="form-control" name="password" placeholder="Password">
                        </div>
                        <div class="form-group" style="color:white;">
                           <label class="control-label" for="">Confirm Password</label>
                           <input type="password" class="form-control" name="password_conf" placeholder="Confirm Password">
                        </div>
                        <div class="form-group" style="color:white;">
                           <p><input class="" type="checkbox" id="agree" name="agree"
                              value="agree" checked/><label for="agree" style="color:Black;">I ACCEPT THE<a href="Terms_and_Conditions.pdf" target="_blank">TERMS &amp;CONDITIONS </a><label> and <a href="Privacy_Policy.pdf" target="_blank"> PRIVACY &amp; POLICY</a></label>
                           </p>
                        </div>
                        <small>
                           <!-- By clicking Sign Up, you agree to our Terms and that you have read our
                              Data Use Policy, including our Cookie Use. -->
                        </small>
                        <div style="height:10px;"></div>
                        <div class="form-group">
                           <label class="control-label" for=""></label>
                           <input type="submit" value="Request for Registration" class="btn btn-primary">
                        </div>
                        <?php echo form_close(); ?>  
                     </div><!--show org -->
                  </div><!--div faka -->

               </div><!--col-sm-4 finished -->
            </div><!--aticle row finished -->
         
      </article> <!--aticle finished -->
      <?php $this->load->view('template/footer'); ?>
      <!-- ALL THE SCRIPTS/PLUGINS -->
      <!-- Jquery, Modernizer, Selectivizr & Prefix Free Js -->
      <script src="<?php echo base_url(); ?>assets/js/portal-js/jquery.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/js/portal-js/modernizr-2.8.3.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/js/portal-js/selectivizr-min.js"></script>
      <!-- Bootstrap Js -->
      <script src="<?php echo base_url(); ?>assets/js/portal-js/bootstrap.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/js/portal-js/bootstrap-datetimepicker.min.js"></script>
      <!-- Theme Main Js -->
      <script src="assets/vendors/fileuploader/src/jquery.fileuploader.min.js" type="text/javascript"></script>
      <script src="assets/vendors/fileuploader/examples/default-upload/js/custom.js" type="text/javascript"></script>
      <script src="<?php echo base_url(); ?>assets/js/portal-js/main.js"></script>
      <!-- AJAX JS-->
      <script src="<?php echo base_url(); ?>assets/js/portal-js/ajax.js"></script>
      <!--FACEBOOK JS Script-->
      <script src="<?php echo base_url(); ?>assets/js/portal-js/fbzahed.js"></script>
      <!--<script>
         $(window).load(function () {
             $('#myModal').modal('show');
         });
         </script>-->
      <!--   <script>
         $(window).load(function()
         {
         $('#sign_up').modal('show');
         });
         </script> -->
      <script type="text/javascript">
         $(document).ready(function () {
             $('.date_picker').datepicker({
                 format: "yyyy-mm-dd"
             });
         });
         $("input[name='test']").click(function () {
         $('#show-individual').css('display', ($(this).val() === 'a') ? 'block':'none');
         });
         $("input[name='test']").click(function () {
         $('#show-org').css('display', ($(this).val() === 'b') ? 'block':'none');
         });
      </script>
      <script>
         $(document).ready(function () {
             $('[data-toggle="popover"]').popover();
         });
      </script>
   </body>
</html>

