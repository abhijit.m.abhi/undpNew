<style>
    .new_rz_box {
        display: block;
        margin-top: 40px;
        padding: 10px 0;
        border-bottom: 1px solid #ccc;
    }

    .new_rz_box h6 {
        color: #446678;
        text-transform: uppercase;
    }

    .new_rz_box h5 {
        color: #446678;
        text-transform: uppercase;
    }

    .new_rz_box h3 {
        color: #446678;
        text-transform: uppercase;
    }

    .new_rz_box h4 {
        color: #333;
    }

    .new_rz_box2 {
        display: block;
        padding: 10px 0;
    }

    .new_rz_box2 h3 {
        color: #446678;
        margin-bottom: 10px;
    }

    .new_rz_box2 input {
        border: 1px solid #d9d9d9;
        width: 100%;
        padding: 5px;
    }

    .new_rz_box2 textarea {
        border: 1px solid #d9d9d9;
        width: 100%;
        padding: 10px 20px;
    }

    .new_rz_box2 select {
        border: 1px solid #d9d9d9;
        width: 100%;
        padding: 5px 10px;
    }

    .rz_button3 {
        margin-top: 40px;
        border: 0;
        background-color: #012F49;
        color: #FFF;
        font-size: 25px;
        text-align: center;
        padding: 10px 20px;
        text-transform: uppercase;
        border-radius: 10px;
    }

    .rz_button3:hover {
        color: #FFF;
    }
</style>
<style type="text/css">
    .thumb {
        width: 50px;
        height: 50px;
    }

    #list {
        position: absolute;
        top: 0px;
    }

    #list div {
        float: left;
        margin-right: 10px;
    }

    .hovereffect {
        height: 200px;
        margin: auto;
        overflow: hidden;
        transition: all 1s ease 0s;
    }

    .z-img img {
        transition: all 0.6s ease 0s;
    }

    .hovereffect:hover .z-img img {
        transform: scale(1.9);
    }

    .z-text {
        position: absolute;
        width: 90%;
        height: 200px;
        border: 1px solid gray;
        top: -199px;
        z-index: 1111;
        display: none;
        color: white;
        text-align: center;
        padding: 10px;
        border: 1px solid white;

    }

</style>

<div class="others_page_wrp">
    <section class="org_map_wrp">
        <div class="container">

            <form class="box" method="post" action="<?php echo site_url().'/Opportunity/insertEditedOpportunity/'.$opportunityDetails->id; ?>" enctype="multipart/form-data">

                <input type="hidden" name="user_id" value="<?php echo $this->session->userdata['user_logged_user']['userId']; ?>"/>
                <div class="row new_rz_box">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <h5>Name of Organization *</h5>
                        <h4><?php echo (!empty($organizationData->title)) ? $organizationData->title : ''; ?></h4>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <h6>Full Address *</h6>
                        <h4><?php echo (!empty($organizationData->location)) ? $organizationData->location : ''; ?></h4>
                    </div>
                </div>

                <div class="row new_rz_box2">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h3>Job Title *</h3>
                        <input type="text" name="title" value="<?php echo $opportunityDetails->title ?>" placeholder="Event title" maxlength="255" required/>
                    </div>
                </div>
                <div class="row new_rz_box2">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h3>Category *</h3>
                        <select name="category" required>
                            <option value="">Select category</option>
                            <option value="Competition" <?php echo ($opportunityDetails->category == 'Competition') ? 'selected="selected"' : ''; ?> >Competition</option>
                            <option value="Conference" <?php echo ($opportunityDetails->category == 'Conference') ? 'selected="selected"' : ''; ?>>Conference</option>
                            <option value="Exchange-program" <?php echo ($opportunityDetails->category == 'Exchange-program"') ? 'selected="selected"' : ''; ?>>Exchange-program</option>
                            <option value="Fellowship" <?php echo ($opportunityDetails->category == 'Fellowship') ? 'selected="selected"' : ''; ?>>Fellowship</option>
                            <option value="Internship" <?php echo ($opportunityDetails->category == 'Internship') ? 'selected="selected"' : ''; ?>>Internship</option>
                            <option value="Scholarship" <?php echo ($opportunityDetails->category == 'Scholarship') ? 'selected="selected"' : ''; ?>>Scholarship</option>
                            <option value="Workshop" <?php echo ($opportunityDetails->category == 'Workshop') ? 'selected="selected"' : ''; ?>>Workshop</option>
                            <option value="Training" <?php echo ($opportunityDetails->category == 'Training') ? 'selected="selected"' : ''; ?>>Training</option>
                            <option value="Miscellaneous" <?php echo ($opportunityDetails->category == 'Miscellaneous') ? 'selected="selected"' : ''; ?>>Miscellaneous</option>
                        </select>
                    </div>
                </div>

                <input type="hidden" name="ext_image" value="<?php echo $opportunityDetails->images; ?>">

                <?php $images = explode('___', $opportunityDetails->images); ?>


                <span id="blk-image" class="toHide" style="display:block; padding: 0px; margin: 0px;">
                    <?php if (!empty($opportunityDetails->images)) { ?>
                        <div class="row new_rz_box2">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <h3>Old Images</h3>
                            <?php foreach ($images as $image): ?>
                                <div class="col-sm-3">
                                    <div class="hovereffect">
                                        <div class="z-text">
                                            <button type="button" style="margin-top: 30%"
                                                    class="btn btn-danger delete-button" title="Delete"
                                                    blog-id="<?php echo $opportunityDetails->id ?>"
                                                    image-name="<?php echo $image ?>"><i class="fa fa-remove"
                                                                                         aria-hidden="true"></i></button>
                                        </div>
                                        <div class="z-img">
                                            <img src="<?php echo base_url(); ?>upload/opportunities/<?php echo $image ?>" alt="post img"
                                                 class="image pull-left img-responsive postImg img-thumbnail margin10"
                                                 height="200px">
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="row new_rz_box2">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <h3>Gallery</h3>
                            <input type="file" name="files">
                        </div>
                    </div>
                </span>

                <div class="row new_rz_box2">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h3>Description *</h3>
                        <textarea name="description" id="description" rows="3" required><?php echo $opportunityDetails->description; ?></textarea>
                    </div>
                </div>
                <div class="row new_rz_box2">
                    <div class="col-sm-12 col-xs-12">
                        <h3>Apply DATE *</h3>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <p><i style="position:absolute; right:15%; top:25%;"
                                      class="glyphicon glyphicon-calendar fa fa-calendar"></i><input type="text"
                                                                                                     value="<?php echo $opportunityDetails->start_date; ?>"
                                                                                                     name="start_date"
                                                                                                     id="datepicker"
                                                                                                     placeholder="Start"/>
                                </p>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <p><i style="position:absolute; right:15%; top:25%;"
                                      class="glyphicon glyphicon-calendar fa fa-calendar"></i><input type="text"
                                                                                                     value="<?php echo $opportunityDetails->end_date; ?>"
                                                                                                     name="end_date"
                                                                                                     id="datepicker2"
                                                                                                     placeholder="End date"/>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row new_rz_box2">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="district">District *</label>
                            <select name="district" id="district">
                                <option value="">Select District</option>
                                <?php foreach ($city as $cities): ?>
                                    <option value="<?php echo $cities->name; ?>" <?php echo ($opportunityDetails->district == $cities->name) ? 'selected="selected"' : '' ?> ><?php echo$cities->name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row new_rz_box2">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h3>Job Registration Link</h3>
                        <input type="text" name="registration_link" value="<?php  echo $opportunityDetails->registration_link; ?>"
                               placeholder="http://www.facebook.com/opportunity/"/>
                    </div>
                </div>
                <p style="text-align:center; margin-bottom:60px;">
                    <button class="rz_button3" type="submit" name="submitOpportunity">Publish Opportunity</button>
                </p>


            </form>

        </div>
    </section>
</div>


<script src="<?php echo base_url();?>old/assets/vendors/ckeditor2/ckeditor.js"></script>
<script src="<?php echo base_url();?>old/assets/vendors/ckeditor2/samples/js/sample.js"></script>

<!--<script>-->
<!--    //    $(function () {-->
<!--    $('.blog-type input[type=radio]').on('change', function () {-->
<!---->
<!--//            alert($(this).val());-->
<!---->
<!--        $('.toHide').hide();-->
<!--        $("#blk-" + $(this).val()).fadeIn(1000);-->
<!--    });-->
<!---->
<!---->
<!--    $('.video-url-type input[type=radio]').on('change', function () {-->
<!--        $("#video-url-name").text($(this).attr("text"));-->
<!--    });-->
<!--    //    });-->
<!--</script>-->

<script>
    initSample();
</script>
<script>
    function handleFileSelect(evt) {
        var files = evt.target.files; // FileList object

        // Loop through the FileList and render image files as thumbnails.
        for (var i = 0, f; f = files[i]; i++) {

            // Only process image files.
            if (!f.type.match('image.*')) {
                continue;
            }

            var reader = new FileReader();

            // Closure to capture the file information.
            reader.onload = (function (theFile) {
                return function (e) {
                    // Render thumbnail.
                    var span = document.createElement('div');
                    span.setAttribute('class', 'img_wrpv');
                    span.innerHTML = ['<a href="#" style="color:red; font-size:18px; position: relative; z-index: 200;" class="img_revg">x</a><img width="50px" height="50px" class="thumb" src="', e.target.result,
                        '" title="', escape(theFile.name), '"/>'].join('');
                    document.getElementById('list').insertBefore(span, null);
                };
            })(f);

            // Read in the image file as a data URL.
            reader.readAsDataURL(f);

            //remove image
            $('.img_revg').on('click', function (e) {
                e.preventDefault();
                var parent = $(this).parent();
                parent.remove();
            });

        }
    }

    document.getElementById('galleryfiles').addEventListener('change', handleFileSelect, true);
</script>

<script>
    $(function () {
        $("#datepicker").datepicker({format: 'yyyy-mm-dd'});
        $("#datepicker2").datepicker({format: 'yyyy-mm-dd'});
    });
</script>

<script>
    $(document).ready(function () {
        $('.hovereffect').hover(function () {
            $(this).children('.z-text').css('top', '0').fadeToggle(600);
        });
    });

</script>

<script>
    $(document).ready(function () {
        $(".delete-button").click(function () {
            var opp_id = $(this).attr('blog-id');
            var image_name = $(this).attr('image-name');
            var removeImage = $(this).parent().parent().parent();
            $.ajax({
                url: '../calSearch',
                type: 'POST',
                data: {
                    opp_id: opp_id,
                    img_name: image_name,
                    action: 'blogImageDelete'
                }
            }).done(function (response) {
                var res = $.parseJSON(response);
                alert(res.msg);
                if (res.ack) {
                    removeImage.fadeOut();
                }
            })
        });
    });
</script>