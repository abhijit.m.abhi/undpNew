<header class="header">
                        <nav class="navbar navbar-default main_mav">
                            <div class="">
                                <!-- Brand and toggle get grouped for better mobile display -->
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                    <a class="navbar-brand home_logo" href="#"><img src="img/logo.png" alt="" /></a>
                                </div>
                        
                                <!-- Collect the nav links, forms, and other content for toggling -->
                                <div class="collapse navbar-collapse navbar-ex1-collapse">
                                    <ul class="nav navbar-nav navbar-right admin_bar">
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span> <?php if(!empty($auth->getProfileImage($id))){
                                                $name=$auth->getProfileImage($id);
                        
                                                echo " <img class='img' style='' width=30px' height='20px' src='$name'";

                                            }else{ echo "<img style=';' class='img' width='30px' height='20px' src='img/placeholder.png'>";} ?></span> <?php 
                                            if($type=="organization" && $type!="individual"){
                                                echo $oname;
                                            }if($type=="individual" && $type!="oranization"){
                                                 echo $fname;
                                            }?>

                                            <b class="caret"></b></a>
                                            <ul class="dropdown-menu">
                                            <?php 
                                                if($type=="organization" && $type!="individual"){
                                                echo '
                                                <li><a href="#" data-modal-external-file="modal_submit.php" data-target="modal-submit">Edit Profile</a></li>
                                                <li><a href="event_list.php">Event management</a></li>
                                                <li><a href="media_blog_list.php">Media Blog management</a></li>
                                                <li><a href="#contact_envelope" data-toggle="modal">Report Problem</a></li>
                                                <li><a href="#reset_password" data-toggle="modal">Reset Password</a></li>
                                                <li><a href="#In_active_account_modal" data-toggle="modal">Inactive Account</a></li>
                                                <li><a href="logout.php">Logout</a></li></li>';

                                            }else{

                                                echo '
                                                <li><a href="#edit_individual" data-toggle="modal">Edit Profile</a></li>
                                                <li><a href="#reset_password" data-toggle="modal">Reset Password</a></li>
                                                <li><a href="#In_active_account_modal" data-toggle="modal">Inactive Account</a></li>
                                                <li><a href="logout.php">Logout</a></li></li>';
                                            }

                                            ?>
                                            </ul>
                                            <?php
                                                if(!$listing->checkProfileStatus($auth->getCurrentOrganizationId()) && $type!="individual"):
                                            ?>
                                               <span style="font-weight: 800; font-size: 16px; color: #ffffff;">Please Complete Your Profile!</span>
                                            <?php
                                                endif;
                                            ?>
                                        </li>
                                    </ul>
                                </div><!-- /.navbar-collapse -->
                            </div>
                        </nav>
                        <div class="clearfix"></div>
                    </header>