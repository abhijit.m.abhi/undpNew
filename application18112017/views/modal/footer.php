<style>
    @font-face {
        font-family: SolaimanLipi;
        src: url('fonts/SolaimanLipi_new.ttf');
    }
</style>
<!-- <script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-91240946-1', 'auto');
    ga('send', 'pageview');
</script> -->
<footer>
    <div class="container-fluid">
        <div class="col-sm-6 col-md-4 col-xs-12 footer_col1 text-center">
            <a href="#" data-toggle="modal" data-target="#myModal" style="
                border: #E43433;
                padding: 6px 12px;
                background: #E43433;
                color: white;
                border-radius: 25px;
            "><b>Café Registration</b></a>
            <a href="#" data-toggle="modal" data-target="#about_us_modal"><b>ABOUT US</b></a>
            <a href="#" data-toggle="modal" data-target="#about_us_disclaimer"><b>DISCLAIMER</b></a>
        </div>
        <div class="col-sm-6 col-md-4 col-xs-12 footer_col2 text-center">
            <a href="http://www.unv.org/" target="_blank"><img src="img/unvbd.png" alt="" class="unvbd_img"/>
                <a href="http://www.bd.undp.org/" target="_blank"><img src="img/undp.png" alt="" class="undp_img"/></a>
        </div>
        <div class="col-sm-12 col-md-4 col-xs-12 footer_col3 text-left">
            <a href="https://www.facebook.com/youthopia.bangla" target="_blank" class="facebook_icon"><i
                        class="fa fa-facebook"></i></a>
            <a href="https://twitter.com/youthopiabangla" target="_blank" class="twitter_icon"><i
                        class="fa fa-twitter"></i></a>
            <a href="#contact_envelope" data-toggle="modal" class="email_icon"><i class="fa fa-envelope-o"></i></a>
        </div>
        <div class="clearfix"></div>
    </div>
</footer>
<!-- About Us Model START -->
<div class="modal fade aboutus_modal_wrp" id="about_us_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="aboutus_modal_content">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
                <h1><span>ABOUT US</span></h1>
                <div class="aboutus_modal_text text-left" style="margin-top: 10px">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#menu1">English</a></li>
                        <li><a data-toggle="tab" href="#home">বাংলা</a></li>

                    </ul>
                    <div class="tab-content">
                        <div id="menu1" class="tab-pane fade in active">
                            <p style="margin-top: 10px;">Youthopia.bangla is a space created with Bangladeshi youth in
                                mind. Here you can dream about your ideal place and shape it along with likeminded
                                people.</p>
                            <h4 style="font-size: x-large;">Vision</h4>
                            <p style="text-align: justify">A country of equal opportunities for all, backed by knowledge
                                sharing, participation & innovation.</p>
                            <h4 style="font-size: x-large;">Mission</h4>
                            <p style="text-align: justify">To build bridges between the dreams of the Bangladeshi youth
                                & the reality of the country by facilitating the access to the information required to
                                take informed decisions.</p>
                            <br>
                        </div>

                        <div id="home" class="tab-pane fade" style="font-family: SolaimanLipi">
                            <p style="margin-top: 15px;">ইয়ুথওপিয়া.বাংলা বাংলাদেশের একমাত্র ইয়ুথ নেটওয়ার্ক যা দেশের
                                উন্নয়নে যুবাদের অবদান, তাদের কর্মকাণ্ড ও চাহিদার প্রতিচ্ছবি। এই নেটওয়ার্কের মাধ্যমে
                                তরুণরা যথাযথ তথ্যের ভিত্তিতে সিধান্ত গ্রহণ, সমমনা সংগঠনদের সাথে সংযোগ স্থাপন এবং নিজেদের
                                দক্ষতা বৃদ্ধিতে সচেষ্ট হবে। বাংলাদেশকে নিয়ে এ দেশের যুবসমাজ যে স্বপ্ন দেখে, তার
                                বাস্তবায়ন তরুণরা ইয়ুথওপিয়া.বাংলাকে সাথে নিয়ে এগিয়ে যাবে।</p>
                            <h4 style="font-size: x-large;">লক্ষ্য </h4>
                            <p style="text-align: justify">উদ্ভাবন, অংশগ্রহণ ও তথ্যের সহজপ্রাপতার মধ্য দিয়ে একটি
                                সম্ভবনাময় বাংলাদেশ গড়ে তোলা।</p>
                            <h4 style="font-size: x-large;">উদ্দেশ্য </h4>
                            <p style="text-align: justify">যথাযথ ও যুগোপযোগী তথ্য সরবরাহের মাধ্যমে বাংলাদেশের তরুণদের
                                স্বপ্ন ও বাস্তবতার মাঝে সেতু বন্ধন তৈরি করা।</p>
                            <br>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- About Us Model END  -->
<!-- Disclaimer Model START -->
<div class="modal fade aboutus_modal_wrp" id="about_us_disclaimer">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="aboutus_modal_content">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
                <h1><span>DISCLAIMER</span></h1>
                <div class="aboutus_modal_text text-left">
                    <h4>Disclaimer</h4>
                    <p style="text-align: justify">
                        Last updated: April 03, 2017
                        <br>
                        <br>
                        The information contained on <a href="https://www.youthopiabangla.org">https://www.youthopiabangla.org</a>
                        website (the "Service") is for general information purposes only.
                        <br>
                        <br>
                        Youthopia.bangla assumes no responsibility for errors or omissions in the contents on the
                        Service.
                        <br>
                        <br>
                        In no event shall Youthopia.bangla be liable for any special, direct, indirect, consequential,
                        or incidental damages or any damages whatsoever, whether in an action of contract, negligence or
                        other tort, arising out of or in connection with the use of the Service or the contents of the
                        Service. Youthopia.bangla reserves the right to make additions, deletions, or modification to
                        the contents on the Service at any time without prior notice.
                        <br>
                        <br>
                        Youthopia.bangla does not warrant that the website is free of viruses or other harmful
                        components.
                        <br><br>
                    <h4>External links disclaimer</h4>
                    https://www.youthopiabangla.org website may contain links to external websites that are not provided
                    or maintained by or in any way affiliated with Youthopia.bangla
                    <br>
                    <br>
                    <p>Please note that the Youthopia.bangla does not guarantee the accuracy, relevance, timeliness, or
                    completeness of any information on these external websites.</p><br>
                    <label>Please find details in our <a href="Terms_and_Conditions.pdf" target="_blank"><u> TERMS &amp;
                                CONDITIONS</u></a></label> <label>and <a href="Privacy_Policy.pdf" target="_blank"><u>
                                Privacy &amp; Policy</u></a></label>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Disclaimer Model END  -->
<!-- CAFE Model START  -->
<div class="modal fade aboutus_modal_wrp" id="myModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-dialog modal-lg"
        " style="padding: 18px;">
        <div class="modal-content" style="padding: 18px;">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close" style="color: black;"></i>
            </button>
            <h3></h3>
            <div class="thumbnail">
                <img src="img/youthopia_flayers.jpg" alt="" style="max-height: 100%;">
            </div>
            <div class="container-fluid">
                <h3 style="
                        color: #FBC433;
                        ">Limited seats.<br>
                    Register your organization now!</h3>
                <a href="cafe_reg.php" style="
                            border: #E43433;
                            padding: 6px 12px;
                            background: #E43433;
                            color: white;
                            border-radius: 25px;
                            float:right;
                            font-size: 18px;">
                    <b>Café Registration</b></a></div>
        </div>
    </div>
</div>
<!-- CAFE Model END  -->