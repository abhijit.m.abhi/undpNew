<header id="page-header">
    <nav>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <a href="index.php" class="brand"><img src="img/logo.png" alt=""></a>
        </div>
        <!--end left-->
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="text-align:center;">
            <div class="primary-nav has-mega-menu middle_menu" style="margin-right:0; padding-right:0;">
                <ul class="navigation" style="padding-left:0; position:relative;">

                    <li title="Directory"><a href="orgdirectory.php">
                            <img src="img/3.png">
                        </a></li>
                    <li title="Calendar"><a href="org-event-calender.php">
                            <img src="img/1.png">
                        </a></li>
                    <li title="Opportunities"><a href="opportunities.php">
                            <img src="img/4.png">
                        </a></li>
                    <li title="Media Blog"><a href="media_blogs.php">
                            <img src="img/2.png">
                        </a></li>

                    <li title="Home"><a href="index.php">
                            <img src="img/5.png">
                        </a></li>
                </ul>
                <!--end navigation-->
            </div>
            <!--end primary-nav-->
            <!--<a href="#" class="btn btn-primary btn-small btn-rounded icon shadow add-listing" data-modal-external-file="modal_submit.php" data-target="modal-submit"><i class="fa fa-plus"></i><span>Add listing</span></a>-->
            <div class="nav-btn">
                <i></i>
                <i></i>
                <i></i>
            </div>
            <!--end nav-btn-->
        </div>
        <div class="col-lg-4 col-md-4 col-sm-2 col-xs-5">
            <?php
            if (!isset($_SESSION['username']) || !isset($_SESSION['fname'])) {
                echo '
                   <div class="secondary-nav">
                        <a data-toggle="modal" href="#sign_up">sign up</a>
                        <a data-toggle="modal" href="#log_in">login</a>
                        
                    </div>';
            } else {
                ?>

                <ul class="nav navbar-nav navbar-right admin_bar" style="margin-top:13px;">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle rz_button"
                           data-toggle="dropdown"><span> <?php if (!empty($auth->getProfileImage($id))) {
                                    $name = $auth->getProfileImage($id);

                                    echo " <img class='img' style='' width=30px' height='20px' src='$name'";

                                } else {
                                    echo "<img style=';' class='img' width='30px' height='20px' src='img/placeholder.png'>";
                                } ?></span> <?php
                            if ($type == "organization" && $type != "individual") {
                                echo $oname;
                            }
                            if ($type == "individual" && $type != "oranization") {
                                echo $fname;
                            } ?>

                            <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <?php
                            if ($type == "organization" && $type != "individual") {
                                echo '
                                                <li><a href="#" data-modal-external-file="modal_submit.php" data-target="modal-submit">Edit Profile</a></li>
                                                <li><a href="event_list.php">My Events</a></li>
                                                <li><a href="media_blog_list.php">My Media Blog</a></li>
                                                <li><a href="#contact_envelope" data-toggle="modal">Report Problem</a></li>
                                                <li><a href="#reset_password" data-toggle="modal">Reset Password</a></li>
                                                <li><a href="#In_active_account_modal" data-toggle="modal">Inactive Account</a></li>
                                                <li><a href="logout.php">Logout</a></li></li>';

                            } else {

                                echo '
                                                <li><a href="#edit_individual" data-toggle="modal">Edit Profile</a></li>
                                                <li><a href="#reset_password" data-toggle="modal">Reset Password</a></li>
                                                <li><a href="#In_active_account_modal" data-toggle="modal">Inactive Account</a></li>
                                                <li><a href="logout.php">Logout</a></li></li>';
                            }

                            ?>
                        </ul>
                        <?php
                        if (!$listing->checkProfileStatus($auth->getCurrentOrganizationId()) && $type != "individual"):
                            ?>
                            <span style="font-weight: 800; font-size: 16px; color: #ffffff;">Please Complete Your Profile!</span>
                            <?php
                        endif;
                        ?>
                    </li>
                </ul>


            <?php } ?>

        </div>

        <!--end right-->
    </nav>
    <!--end nav-->
</header>
<!--end page-header-->

   