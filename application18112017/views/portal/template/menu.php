<?php
$userInfo=$this->session->userdata('user_logged_user');
$this->load->view('portal/globalPhpFunction');
if(isset($userInfo['userId']))
{
  $permUserId=$userInfo['userId'];
}
else
{
  $permUserId=0;
}
$permission=getUserPermission($permUserId);
$url= 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
$actual_link=str_replace("/","--",$url);
?>
<?php
$eventPermission= getPermission(2,$permission);
$mediaBlogPermission= getPermission(3,$permission);
$opportunitiesPermission= getPermission(4,$permission);
$inclussiveBdPermission= getPermission(6,$permission);

?>
<link rel="stylesheet" href="<?php echo base_url(); ?>portalAssets/css/style.css" type="text/css">

<link rel="stylesheet" href="<?php echo base_url(); ?>event/css/style.css">
<link rel="stylesheet" href="<?php echo base_url('portalAssets/homePage/icons/styles.css'); ?>">
<style media="screen">
  .img-rounded{
    height: 30px!important;
    width: auto;
  }
  ul.navigation li{
    vertical-align: middle;
  }
  .menuIcon{
    font-size: 30px;
    width: 50px;
    height: 50px;
    padding: 10px;
    box-shadow: 0px 0px 0px 2px #FFD64A;
    border-radius: 50%;
    transition: .5s;
    //vertical-align: middle;
  }
  .menuIcon:hover{
    font-size: 30px;
    /*width: 50px;
    height: 50px;*/
    color:#FFD64A;
    padding: 10px;
    box-shadow: 0px 0px 0px 5px #FFD64A;
    border-radius: 50%;
    transition: .5s;
    //vertical-align: middle;
  }
.rz_button222:hover, .rz_button222:focus, .rz_button222:active{
  /*background-color: transparent !important;
  color: #fff;*/
}
.nav .open>.rz_button222, .nav .open>.rz_button222:hover, .nav .open>.rz_button222:focus{
  background-color: transparent !important;
  color: #fff;
}

.admin_bar{
  margin-right: 0px;
}
.modal-header h4 {
    border-bottom: 0px;
    padding-bottom: 0px;
}


.modal-header h4 {
    color: #681156;
    margin-top: 15px;
    font-size: 25px;
    margin-bottom: 8px;
    /* border-bottom: 1px solid #ddd; */
    /* padding-bottom: 20px; */
}

@media (max-width: 750px){
  .admin_bar{
    display: inline-block;
    float: left;
    margin-top: 13px;
  }
  .nav-btn22{
    margin-top: 25px !important;
  }
  .has-mega-menu{
    margin-top: 90px !important;
  }
}

button.close2 {
    right: 7px !important;
}





.dir{
   background: url(<?php echo base_url(); ?>portalAssets/homePage/img/icon/Directory_yellow.png) no-repeat 50% 50%;
   background-size: 24px auto;
}
.dir:hover{
    background: url(<?php echo base_url(); ?>portalAssets/homePage/img/icon/Directory_yellow.png) no-repeat 50% 50%;
    background-size: 24px auto;
}

.cal{
   background: url(<?php echo base_url(); ?>portalAssets/homePage/img/icon/Calender_yellow.png) no-repeat 50% 50%;
   background-size: 24px auto;
}
.cal:hover{
    background: url(<?php echo base_url(); ?>portalAssets/homePage/img/icon/Calender_yellow.png) no-repeat 50% 50%;
    background-size: 24px auto;
}


.blog{
   background: url(<?php echo base_url(); ?>portalAssets/homePage/img/icon/Media-blog_yellow.png) no-repeat 50% 50%;
   background-size: 27px auto;
}
.blog:hover{
    background: url(<?php echo base_url(); ?>portalAssets/homePage/img/icon/Media-blog_yellow.png) no-repeat 50% 50%;
    background-size: 27px auto;
}

.opp{
   background: url(<?php echo base_url(); ?>portalAssets/homePage/img/icon/Opportunity_yellow.png) no-repeat 50% 50%;
   background-size: 35px auto;
}
.opp:hover{
    background: url(<?php echo base_url(); ?>portalAssets/homePage/img/icon/Opportunity_yellow.png) no-repeat 50% 50%;
    background-size: 35px auto;
}
.skill{
   background: url(<?php echo base_url(); ?>portalAssets/homePage/img/icon/Skill-development_yellow.png) no-repeat 50% 50%;
   background-size: 30px auto;
}
.skill:hover{
    background: url(<?php echo base_url(); ?>portalAssets/homePage/img/icon/Skill-development_yellow.png) no-repeat 50% 50%;
    background-size: 30px auto;
}
.inclu{
   background: url(<?php echo base_url(); ?>portalAssets/homePage/img/icon/Inclusive-Bangladesh_yellow.png) no-repeat 50% 50%;
   background-size: 35px auto;
}
.inclu:hover{
    background: url(<?php echo base_url(); ?>portalAssets/homePage/img/icon/Inclusive-Bangladesh_yellow.png) no-repeat 50% 50%;
    background-size: 35px auto;
}
.home{
   background: url(<?php echo base_url(); ?>portalAssets/homePage/img/icon/Home_yellow.png) no-repeat 50% 50%;
   background-size: 33px auto;
}
.home:hover{
    background: url(<?php echo base_url(); ?>portalAssets/homePage/img/icon/Home_yellow.png) no-repeat 50% 50%;
    background-size: 33px auto;
}

@media (min-width: 740px) and (max-width: 1040px){
  .middle_menu>ul>li>a{
    padding-left:5px !important;
    padding-right: 0px !important;
  }
  #page-header .secondary-nav a:not(.btn), #page-header .primary-nav a:not(.btn) {
    padding-left:3px !important;
  }







}





.rz_button:hover {
    /*color: #fff !important;
    background-color: transparent !important;*/
}

.nav .open>a, .nav .open>a:focus, .nav .open>a:hover {
   background-color: transparent !important;
   color: #fff !important;
}
/*#page-footer{
  display: none;
}*/





</style>
<?php

$session_info_fb = $this->session->userdata("userData");
if(!empty($userInfo or $session_info_fb));
{
  $portalUser=$this->session->userdata('user_logged_user');
  $portalUserFb = $this->session->userdata("userData");
  //=$_SESSION['user_logged_user'];
}

?>
<?php
 if (ISSET($portalUser)) {
     $userId=$portalUser['userId'];
    $userDataFormdb= $this->db->query("select u.fname,u.image,u.oname  from users u WHERE u.userId=$userId")->row();
    }
?>
<header id="page-header" style="position: relative;">
  <nav>
    <div class=" col-md-3 col-sm-2 col-xs-4">
      <a href="<?php echo site_url('portal') ?>" class="brand img-rounded"><img src="<?php echo base_url(); ?>portalAssets/icon-img/logo.png" alt="" class="img-responsive"></a>
    </div>
    <!--end left-->
    <div class=" col-md-5 col-sm-7 col-xs-8" style="text-align:center;">
      <div class="primary-nav has-mega-menu middle_menu" style="margin-right:0; padding-right:0;">
        <ul class="navigation" style="padding-left:0; position:relative;">
          <li title="Directory"><a href="<?php echo base_url() ?>Orgdirectory">
            <div class=" menuIcon dir"></div>
          </a></li>
          <li title="Calendar"><a href="<?php echo site_url('event/eventCalendar') ?>">
            <div class="cal menuIcon"></div>
          </a></li>
          <li title="Opportunities"><a href="<?php echo site_url('opportunity/opportunities') ?>">
            <div class="opp menuIcon"></div>
          </a></li>
          <li title="Media Blog"><a href="<?php echo site_url('blog/mediaBlogs') ?>">
            <div class="blog menuIcon"></div>
          </a></li>
          <li title="Skill Development"><a href="<?php echo site_url('SkillDevelopment') ?>">
            <div class="skill menuIcon"></div>
          </a></li>
          <li title="Inclusive Bangladesh"><a href="<?php echo site_url('InclusiveBd/inclusiveBdAllList') ?>">
            <div class="inclu menuIcon"></div>
          </a></li>
          <li title="Home"><a href="<?php echo site_url('portal/index') ?>">
            <div class="home menuIcon"></div>
          </a></li>
        </ul>
        <!--end navigation-->
      </div>
      <!--end primary-nav-->
      <!--<a href="#" class="btn btn-primary btn-small btn-rounded icon shadow add-listing" data-modal-external-file="modal_submit.php" data-target="modal-submit"><i class="fa fa-plus"></i><span>Add listing</span></a>-->
      <div class="nav-btn nav-btn22">
        <i></i>
        <i></i>
        <i></i>
      </div>
      <!--end nav-btn-->
    </div>
    <?php

    if(ISSET($portalUser))
    {

      if($portalUser['type']=='individual')
      {
        ?>
        <ul class="nav navbar-nav navbar-right admin_bar" style="margin-top:13px;">
          <li class="dropdown">
            <a href="#" class="dropdown-toggle rz_button" data-toggle="dropdown" aria-expanded="false"><span>

              <img src="<?php
              if($portalUser['login_method']=='D')
              {
                echo base_url($userDataFormdb->image);
              }
              else {
                echo $userDataFormdb->image;
              }
               ?>" class="img-rounded" height="30px" width="auto" alt="">
              <?php echo $userDataFormdb->fname; ?>
              <b class="caret"></b></span></a>
              <ul class="dropdown-menu">
              <li><a href="<?php echo site_url("/portal/visitor_profile_edit_data/".$portalUser['userId']."/".$actual_link) ?>" class="modalLink" data-modal-size="modal-lg" title="Edit User">Edit Profile</a></li>
              <li><a href="<?php echo site_url("portal/visitor_profile_edit_password/".$portalUser['userId']) ?>" class="modalLink" data-modal-size="modal-md" title="Reset Password" >Reset Password</a></li>
              <li><a href="<?php echo site_url().'/portal/inactive_account/'.$portalUser['userId']."/".$actual_link ?>" class="modalLink" title="Inactive Account" data-modal-size="modal-md">Inactive Account</a></li>
                <li><a href="<?php echo site_url("auth/userLogout/".$actual_link); ?>">Logout</a></li></li></ul>
                <!-- <span style="font-weight: 800; font-size: 16px; color: #ffffff;">Please Complete Your Profile!</span> -->
              </li>
            </ul>
            <?php
          }
          else if($portalUser['type']=='organization')
          {

            ?>
            <ul class="nav navbar-nav navbar-right admin_bar" style="margin-top:13px;">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle rz_button rz_button222" data-toggle="dropdown" aria-expanded="false"><span>  <img class="img-rounded" style=""  src="<?php echo base_url($userDataFormdb->image) ?>" <="" span="">
                  <?php echo $userDataFormdb->oname; ?>
                  <?php //echo $portalUserFb['fname']; ?>
                  <b class="caret"></b></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="<?php echo site_url("/portal/visitor_profile_edit_data_org/".$portalUser['userId']."/".$actual_link) ?>" class="modalLink" data-modal-size="modal-lg" title="Edit Organization">Edit Profile</a></li>

                    <?php
                      if($eventPermission>0)
                      {
                        echo '<li><a href="'.site_url('event/eventList').'">My Events</a></li>';
                      }
                      if($mediaBlogPermission>0)
                      {
                        echo '<li><a href="'.site_url('blog/blogList').'">My Media Blog</a></li>';
                      }
                      if($opportunitiesPermission>0)
                      {
                        echo '<li><a href="'.site_url('Opportunity/opportunityList').'">My Opportunities</a></li>';
                      }
                      if($inclussiveBdPermission>0)
                      {
                        echo '<li><a href="'.site_url('InclusiveBd/inclusivebdList').'">My Inclusive Bangladesh</a></li>';
                      }
                    ?>


                     <li><a href="<?php echo site_url("portal/report_problem/".$portalUser['userId']."/".$actual_link) ?>" class="modalLink" data-modal-size="modal-md" title="Send Us A Quick Message">Report Problem</a></li>
                     <li><a href="<?php echo site_url("portal/visitor_profile_edit_password/".$portalUser['userId']) ?>" class="modalLink" data-modal-size="modal-md" title="Reset Password" >Reset Password</a></li>
                      <li><a href="<?php echo site_url().'/portal/inactive_account/'.$portalUser['userId']."/".$actual_link ?>" class="modalLink" title="Inactive Account" data-modal-size="modal-md">Inactive Account</a></li>
                    <li><a href="<?php echo site_url("auth/userLogout/".$actual_link); ?>">Logout</a></li></li></ul>
                    <?php
                     $userId=$userInfo['userId'];
                      $columns=$this->db->query("SELECT  COLUMN_NAME
                                        FROM INFORMATION_SCHEMA.COLUMNS
                                        WHERE table_name = 'listing'")->result();
                     $totalColumns=count($columns);
                     $totalColumns=$totalColumns-4;
                     $completed=0;
                     foreach($columns as $col)
                     {
                       $colName=$col->COLUMN_NAME;
                        $exist=$this->db->query("SELECT count(*) COUNT
                        FROM listing
                        WHERE $colName!=''  AND userId=$userId")->row();
                        $completed=$completed+$exist->COUNT;
                     }
                     $percentange=($completed*100)/$totalColumns;
                     $percentange=floor($percentange);
                    ?>
                    <span class="" style="font-weight: 800; font-size: 16px; color: #ffffff; float:right"> Your account is <?php echo $percentange; ?>% Completed</span>
                  </li>
                </ul>

                <?php
              }
            }
            else
            {
              ?>
              <style media="screen">
                a.modalLink{
                  color: #000!important;
                }
              </style>
              <div class=" col-md-3 col-sm-3 col-xs-5">

                <div class="secondary-nav">
                  <div class="dropdown">
                    <button class="btn btn-primary dropdown-toggle sign-btn" type="button" data-toggle="dropdown">Sign Up
                      <span class="caret"></span></button>
                      <ul class="dropdown-menu pull-right">
                        <li><a class="modalLink"  title="SIGN UP AS INDIVIDUAL" href="<?php echo site_url('portal/individualRegisForm/'.$actual_link); ?>" data-modal-size="modal-md">Sign Up As Individual</a></li>
                        <li><a  class="modalLink" href="<?php echo site_url('portal/orgRegisForm/'.$actual_link); ?>" data-modal-size="modal-md" title="SIGN UP AS ORGANIZATION">Sign Up As Organization</a></li>
                      </ul>
                      <button class="btn btn-primary modalLink " title="WELCOME, FRIEND!" href="<?php echo site_url('portal/loginModalForm/'.$actual_link); ?>" data-modal-size="modal-md" type="button" data-toggle="">Login
                      </button>
                    </div>
                  </div>
                </div>
                <?php
              }
              ?>

              <!--end right-->
            </nav>
            <!--end nav-->
        <?php   $errorMsg=$this->session->flashdata('error'); ?>
        <?php   $successMsg=$this->session->flashdata('success'); ?>

          </header>
          <?php
          if (!empty($errorMsg)) {
            ?>
            <div class="container"><div class="row"><div class="col-sm-12">
            <div class="alert alert-danger">
              <button data-dismiss="alert" class="close close2" type="button">×</button>
              <?php echo $errorMsg; ?>
            </div>
            </div>
            </div>
            </div>
            <?php
          }
          ?>
          <?php
          if (!empty($successMsg)) {
            ?>
            <div class="container"><div class="row"><div class="col-sm-12">
            <div class="alert alert-success">
              <button data-dismiss="alert" class="close close2" type="button">×</button>
              <?php echo $successMsg; ?>
            </div>
            </div>
            </div>
            </div>
            <?php
          }

          ?>
          <script type="text/javascript">
          $(document).ready(function () {
            $('.date_picker').datepicker({
              format: "yyyy-mm-dd"
            });
          });
          </script>
          <script>
          $(document).ready(function () {
            $('[data-toggle="popover"]').popover();
          });
          </script>
