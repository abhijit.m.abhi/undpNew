<head>
  <meta charset="UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="author" content="ThemeStarz">
  <link rel="shortcut icon" href="<?php echo base_url(); ?>favicon.ico"/>
  <link href="<?php echo base_url(); ?>portalAssets/fonts/font-awesome.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url(); ?>portalAssets/fonts/elegant-fonts.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700,900,400italic' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="<?php echo base_url(); ?>portalAssets/bootstrap/css/bootstrap.css" type="text/css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>portalAssets/css/zabuto_calendar.min.css" type="text/css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>portalAssets/css/owl.carousel.css" type="text/css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>portalAssets/css/trackpad-scroll-emulator.css" type="text/css">





  <script type="text/javascript" src="<?php echo base_url(); ?>portalAssets/js/jquery-2.2.1.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>portalAssets/js/jquery-2.2.1.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>portalAssets/js/jquery-migrate-1.2.1.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>portalAssets/bootstrap/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>portalAssets/js/bootstrap-select.min.js"></script>
<!--  <script type="text/javascript" src="--><?php //echo base_url(); ?><!--portalAssets/js/richmarker-compiled.js"></script>-->




  <title><?php echo $pageTitle; ?></title>
  <style>
  /*.form-control,btn{
    max-height:20px;
  }*/
  #page-header{
    background-color:green!important;
  }
  .secondary-nav{
    vertical-align:middle;
  }
  .rcorners2 {
    border-radius: 50%;
    border: 2px solid #fff;
    padding: 20px;
    width: 100%;
    height: auto;
    color: #fff;
    background-color: #3b5998;
    text-align: center;
    vertical-align: middle;
}
.rcorners2:hover{
  border: 2px solid #000;
  background-color: #4611a7;
}
.alignMent{
  vertical-align: middle;
}
.giFont{
 font-size: 30px;
 text-align: center;
}
.bottomText{
  display: none;
}
.socialIcon{
  background-color: #fff;
  color: blue;
}
#page-content{
  background:url(<?php echo base_url(); ?>portalAssets/img/home_bg.jpg);
  background-size: 100% 100%;
  background-attachment: fixed;
}

.registerBg h3{
  text-align: center;
  margin-top: 10px;
}


  </style>
</head>
