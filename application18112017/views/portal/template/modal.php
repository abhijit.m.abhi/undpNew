
<div class="modal inmodal appModal">
  <div class="modal-dialog"  id="modalSize" >
    <div class="modal-content animated">
      <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span><span
          class="sr-only">Close</span></button>
          <h4 class="modal-title" id="showDetaildModalTile"></h4>
          <!-- <hr> -->
          <small class="font-bold"></small>
        </div>

        <div class="modal-body"></div>
        <div class="modal-footer">
          <button data-dismiss="modal" class="btn btn-white" type="button">Close</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">

                  <h4 class="modal-title showDetaildModalTile" id=""></h4>


                <button data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span><span
          class="sr-only">Close</span></button>

              </div>
              <div class="modal-body modalBody">

              </div>
              <div class="modal-footer">

              </div>
          </div>
      </div>
  </div>

  <script src="<?php echo base_url(); ?>assets/vendors/fileuploader/src/jquery.fileuploader.js"></script>
   <script src="<?php echo base_url('portalAssets/sweetAlerts/sweetAlert.min.js') ?>"></script>
   <link href="<?php echo base_url(); ?>assets/vendors/fileuploader/src/jquery.fileuploader.css" rel="stylesheet">
   <link href="<?php echo base_url(); ?>event/css/datepicker.css" rel="stylesheet">
   <script src="<?php echo base_url(); ?>assets/js/portal-js/bootstrap-datetimepicker.min.js"></script>

  <script type="text/javascript">
  $(document).on("click", ".modalLink", function (e) {
            $("#modalSize").removeClass('modal-lg');
            $("#modalSize").removeClass('modal-sm');
            $("#modalSize").removeClass('modal-md');
             var modal_size = $(this).attr('data-modal-size');
        //     alert(modal_size);
             if ( modal_size!=='' && typeof modal_size !== typeof undefined && modal_size !== false ) {
                 $("#modalSize").addClass(modal_size);
             }
             else{
                 $("#modalSize").addClass('modal-lg');
             }
             var title = $(this).attr('title');
             $("#showDetaildModalTile").text(title);
             var data_title = $(this).attr('data-original-title');
             $("#showDetaildModalTile").text(data_title);

             $.ajax({
                 type: "POST",
                 url: $(this).attr('href'),
                 data: {'<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>'},
                 success: function (data) {
                     $(".modal-body").html(data);
                     $(".appModal").modal('show');

                 }
             });
             e.preventDefault();
         });
  </script>
  <script type="text/javascript">
  $(document).on("click", ".homeModal", function (e) {
     var title = $(this).attr('title');
     $(".showDetaildModalTile").text(title);
             $.ajax({
                 type: "POST",
                 url: $(this).attr('href'),
                 data: {'<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>'},
                 success: function (data) {
                     $(".modalBody").html(data);
                     $("#myModal").modal('show');

                 }
             });
             e.preventDefault();

         });
  </script>
<script type="text/javascript">
$(document).ready(function(){
  $(".fileuploader-input-caption").html("jdfh");
})

</script>
