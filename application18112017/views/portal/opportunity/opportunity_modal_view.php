<?php $actual_link = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']); ?>
<?php
//$dateCountDown = date('m/d/Y', strtotime($opportunityDetails->start_date));
//?>


<style type="text/css">
    .caro {
        width: 850px;
        padding: 0px 20px;
    }

    #interested_btn {
        background-color: #F1F3F4;
        border-radius: 15px;
        padding: 3px 8px;
        margin-right: 5px;
        margin-top: 4px;
    }

    #going_btn img, #interested_btn img {
        height: 20px;
        width: auto;
    }

    .frame2 {      
    height: 400px;
    display: table;
    width: 100%;
    text-align: center;
    padding: 0px; 
}

.helper222 {
    display: table-cell;
    text-align: center;
    vertical-align: middle;
    height: 400px;
}

.frame2>span>img {
    vertical-align: middle;
    display: inline-block !important;
    width: auto  !important;
    max-height: 100%;
    max-width: 100%;
}
</style>

<?php

$id = '';

if (isset($this->session->userdata['user_logged_user']['userId'])) {
    $id = $this->session->userdata['user_logged_user']['userId'];
    $linkShare=$id;
} else {
    $id = '';
    $linkShare=0;
}
?>


<div class="row">
    <div class="col-md-12">
        <div class="caro">
            <div class="calender_event_view_slider">
                <div id="event_view_slider">
                    <?php foreach ($images as $image): ?>
                        <div class="slideritem thumbnail frame2" style="margin-bottom: 5px;">
                        <span class="helper222"><img
                                    src="<?php echo base_url(); ?>upload/opportunities/<?php echo $image ?>" alt=""/></span>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="title_of_event"><?php echo $opportunityDetails->title; ?></div>
                <div class="pull-left" style="margin-top: 5px">
                    <i class="fa fa-tags fa-flip-horizontal" aria-hidden="true"></i> <span
                            style="background: lightgray; padding: 6px 12px; font-size: 14px; border-radius: 25px;"><?php echo $opportunityDetails->category; ?></span>
                </div>
                <div class=" share_box_wrp">
                                        <span class="likes_box pull-right"><i
                                                    class="fa fa-eye"></i> <span><?php echo $opportunityDetails->view_count; ?></span></span>

                    <a class="interested_btn pull-right" role="button" id="interested_btn" title="Interested"
                       data-event-id="<?php echo $opportunityDetails->id; ?>" data-user-id="<?php echo $user_id; ?>">
                        <img src="<?php echo base_url() . 'assets/img2/star.png'; ?>">
                        <span id="interested_count"><?php echo $interest_count; ?></span></a>


                </div>
                <div class="clearfix"></div>
            </div>
            <div class="calender_event_view_slider" style="min-width: 100%;">
                <div class="event_view_right_reg_box" style="padding-bottom: 20px">

                    <div class="event_view_reg_row2" style="padding-bottom: 40px;">
                        <div class="time_count_box col-md-6 text-center col-xs-12"><i class="fa fa-calendar"
                                                                                      aria-hidden="true"></i>
                            <b>End Date: </b><?php echo date('M d, Y', strtotime($opportunityDetails->end_date)) ?>
                        </div>
                        <div class="time_count_box col-md-6 col-xs-12 col-sm-6 ">
                            <ul class="countdown text-center">
                                <li>
                                    <span class="days">00</span>
                                    <p class="days_ref">days</p>
                                </li>
                                <li>
                                    <span class="hours">00</span>
                                    <p class="hours_ref">hours</p>
                                </li>
                                <li>
                                    <span class="minutes">00</span>
                                    <p class="minutes_ref">minutes</p>
                                </li>
                                <li>
                                    <span class="seconds">00</span>
                                    <p class="seconds_ref">seconds</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <hr style="margin: 0.5em;">
            <div class="col-xs-12 event_view_right">
                <div class="event_view_right_top" style="text-align:justify;">
                    <h3>Details:</h3>
                    <p class="directory_profile_text1"
                       style="text-align:justify;"><?php echo htmlspecialchars_decode(html_entity_decode($opportunityDetails->description)); ?></p>

                </div>
                <hr style="margin: 0.5em;">
            </div>
            <div class="event_view_reg_row2 text-center">
                <a href="<?php echo prep_url($opportunityDetails->registration_link); ?>" target="_blank"
                   class="register_btn">Vist: <?php echo $opportunityDetails->registration_link ?></a>
            </div>
            <hr style="margin: 0.5em;">
            <div class="col-xs-12">
                <div class="directory_profile_social_share">
                    <i style="color: #012F49" class="fa fa-share-alt" aria-hidden="true"></i>
                    <a class="facebook customer share" style="color: #4267b2"
                       href="http://www.facebook.com/sharer/sharer.php?u=<?php echo $actual_link . '/opportunity_view.php?opportunity_id=' . $opportunityDetails->id; ?>"
                       title="Facebook share" target="_blank"><i class="fa fa fa-facebook fa-lg" style="margin: 0 8px;"
                                                                 aria-hidden="true"></i></a>
                    <a class="twitter customer share" style="color: #1b95e0"
                       href="http://twitter.com/share?url=<?php echo $actual_link . '/opportunity_view.php?opportunity_id=' . $opportunityDetails->id; ?>&amp;hashtags=youthopiabangla"
                       title="Twitter share" target="_blank"><i class="fa fa-twitter fa-lg" style="margin: 0 8px;"
                                                                aria-hidden="true"></i></a>
                    <a class="google_plus customer share" style="color: #db4437"
                       href="https://plus.google.com/share?url=<?php echo $actual_link . '/opportunity_view.php?opportunity_id=' . $opportunityDetails->id; ?>"
                       title="Google Plus Share" target="_blank"><i class="fa fa-google-plus fa-lg"
                                                                    style="margin: 0 8px;"
                                                                    aria-hidden="true"></i></a>
                    <a class="email modalLink" data-modal-size="modal-md"
                       style="color: #79B04A;"
                       href="<?php echo site_url() . '/opportunity/report_problem/'.$opportunityDetails->id.'/'. $linkShare.'/'.$url  ?>"
                       title="Mail to us"><i
                                class="fa fa-envelope fa-lg"
                                style="margin: 0 8px;" aria-hidden="true"></i></a>

                </div>
            </div>
        </div>
    </div>
    <!-- JS STARTS -->


    <script>
        $(document).ready(function () {

            $('.countdown').downCount(
                {
                    date: '<?php echo date('m/d/Y H:i:s', strtotime($opportunityDetails->start_date . ' ' . '00:00:00')) ?>',
                    offset: +6
                }, function () {

                }
            );


            $("#event_view_slider").owlCarousel({
                autoPlay: 3000,
                pagination: false,
                navigation: true,
                singleItem: true,
            });


            $('#interested_btn').on('click', function () {

                var user_id = $(this).data('user-id');
                var opportunity_id = $(this).data('event-id');
                if (!user_id) {
                    alert('Please login to perform this task');
                    return false;
                }
                $.ajax({
                    url: '<?php echo site_url()?>opportunity/calSearch',
                    type: 'POST',
                    data: {
                        user_id: user_id,
                        opportunity_id: opportunity_id,
                        action: 'opportunityInterest'
                    }
                }).done(function (response) {
                    var res = $.parseJSON(response);

                    $('#interested_count').html(res.count);
                    if (!res.ack) {
                        alert(res.msg);
                    }
                });
            });

            $('#going_btn').on('click', function () {

                var user_id = $(this).data('user-id');
                var event_id = $(this).data('event-id');
                if (!user_id) {
                    alert('Please login to perform this task');
                    return false;
                }
                $.ajax({
                    url: '../calSearch',
                    type: 'POST',
                    data: {
                        user_id: user_id,
                        event_id: event_id,
                        action: 'going'
                    }
                }).done(function (response) {
                    var res = $.parseJSON(response);
                    $('#going_count').html(res.count);
                    if (!res.ack) {
                        alert(res.msg);
                    }
                });
            })
        });

    </script>

