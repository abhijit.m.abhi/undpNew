<style>
    footer {
        background-color: #F8F8F8 !important;
    }

    /** ====================
     * Lista de Comentarios
     =======================*/
    .comment {
        overflow: hidden;
        padding: 0 0 1em;
        border-bottom: 1px solid #ddd;
        margin: 0 0 1em;
        *zoom: 1;
    }

    .comment-img {
        float: left;
        margin-right: 33px;
        border-radius: 5px;
        overflow: hidden;
    }

    .comment-img img {
        display: block;
    }

    .comment-body {
        overflow: hidden;
    }

    .comment .text {
    }

    .comment .text p:last-child {
        margin: 0;
    }

    .comment .attribution {
        margin: 0.5em 0 0;
        font-size: 14px;
        color: #666;
    }

    /* Decoration */

    .blog-comments,
    .comment {
        position: relative;
    }

    .blog-comments:before,
    .comment:before,
    .comment .text:before {
        content: "";
        position: absolute;
        top: 0;
        left: 65px;
    }

    .blog-comments:before {
        width: 3px;
        top: 0px;
        bottom: 0px;
        background: rgba(0, 0, 0, 0.1);
    }

    .comment:before {
        width: 9px;
        height: 9px;
        border: 3px solid #fff;
        border-radius: 100px;
        margin: 16px 0 0 -6px;
        box-shadow: 0 1px 1px rgba(0, 0, 0, 0.2), inset 0 1px 1px rgba(0, 0, 0, 0.1);
        background: #ccc;
    }

    .comment:hover:before {
        background: orange;
    }

    .comment .text:before {
        top: 18px;
        left: 78px;
        width: 9px;
        height: 9px;
        border-width: 0 0 1px 1px;
        border-style: solid;
        border-color: #e5e5e5;
        background: #fff;
        -webkit-transform: rotate(45deg);
        -moz-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        -o-transform: rotate(45deg);
    }

    ​
</style>
<style media="screen">
    .easyPaginateNav {
        clear: both;
        margin-top: 20px;
        text-align: center;
    }

    .easyPaginateNav a {
        padding: 8px;
    }

    .easyPaginateNav a.current {
        font-weight: bold;
        text-decoration: underline;
        font-size: 24px;
    }

    .frame {
        height: 390px;
        display: table;
        width: 100%;
        text-align: center;
        padding: 0px;
    }

    .helper {
        display: table-cell;
        text-align: center;
        vertical-align: middle;
        height: 390px;
    }

    .frame > span > img {
        vertical-align: middle;
        display: inline-block !important;
        width: auto !important;
        max-height: 100%;
        max-width: 100%;
    }

    #interested_btn {
        background-color: #F1F3F4;
        border-radius: 15px;
        padding: 3px 8px;
        margin-right: 5px;
        margin-top: 4px;
    }

    #going_btn img, #interested_btn img {
        height: 20px;
        width: auto;
    }

    /*.frame{*/
    /*height: 400px;*/
    /*display: table;*/
    /*width: 100%;*/
    /*text-align: center;*/
    /*padding: 0px;*/
    /*}*/
    /*.helper{*/
    /*height: 400px;*/
    /*display: table;*/
    /*width: 100%;*/
    /*text-align: center;*/
    /*padding: 0px;*/
    /*}*/

    /*.frame>span>img{*/
    /*vertical-align: middle;*/
    /*display: inline-block !important;*/
    /*width: auto !important;*/
    /*max-height: 100%;*/
    /*max-width: 100%;*/
    /*}*/


</style>

<div id="fb-root"></div>
<div class="others_page_wrp">
    <section class="calender_event_view_wrp">
        <div class="container">
            <div class="calender_event_view_top">

                <div class="event_view_top_left">
                    <span class="view_avatar"><img
                                src="<?php echo (!empty($organizationLogo)) ? base_url() . $organizationLogo->image : ''; ?>"
                                alt="" style="width: 70px;"/></span>
                    <div class="event_view_top_left_title">
                        <h2 style="margin: 12px 0px 0px 0px; color: #012F49;"><?php echo $organizationDetails->title; ?></h2>
                        <span><?php echo $organizationDetails->location; ?></span>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <br>

                <?php
                $id = '';
                $likeReaction = 0;
                $smileReaction = 0;
                $loveReaction = 0;
                $wowReaction = 0;
                $images = explode('___', $opportunityDetails->images);

                if (isset($this->session->userdata['user_logged_user']['userId'])) {
                    $id = $this->session->userdata['user_logged_user']['userId'];
                    $linkShare = $id;
                } else {
                    $id = '';
                    $linkShare = 0;
                }

                $interests = $this->blog_model->getBlogInterest($opportunityDetails->id);
                ?>

                <?php foreach ($interests as $interest) { ?>
                    <?php
                    if ($interest->type == '1') {
                        $likeReaction = $interest->interest_count;
                    } elseif ($interest->type == '2') {
                        $smileReaction = $interest->interest_count;
                    } elseif ($interest->type == '3') {
                        $loveReaction = $interest->interest_count;
                    } elseif ($interest->type == '4') {
                        $wowReaction = $interest->interest_count;
                    }
                    ?>

                <?php } ?>

                <div class="calender_event_view_slider">

                    <div id="event_view_slider">
                        <?php foreach ($images as $image): ?>
                            <div class="slideritem thumbnail frame" style="margin-bottom: 5px">
                                <span class="helper"><img style=""
                                                          src="<?php echo base_url(); ?>upload/opportunities/<?php echo $image ?>"
                                                          alt=""/></div>
                            </span><?php endforeach; ?>
                    </div>

                    <div class="title_of_event "><?php echo $opportunityDetails->title ?></div>
                    <div class="pull-left" style="margin-top: 5px">
                        <i class="fa fa-tags fa-flip-horizontal" aria-hidden="true"></i> <span
                                style="background: lightgray; padding: 6px 12px; font-size: 14px; border-radius: 25px;"><?php echo $opportunityDetails->category ?></span>
                    </div>

                    <div class="share_box_wrp">
                     <span class="likes_box pull-right"><i
                                 class="fa fa-eye"></i> <span><?php echo $opportunityDetails->view_count ?></span></span>
                        <a class="interested_btn pull-right" role="button" id="interested_btn" title="Interested"
                           data-event-id="<?php echo $opportunityDetails->id; ?>"
                           data-user-id="<?php echo $user_id; ?>">
                            <img src="<?php echo base_url() . 'assets/img2/star.png'; ?>">
                            <span id="interested_count"><?php echo $interest_count; ?></span></a>
                    </div>
                    <div class="clearfix"></div>
                    <div class="calender_event_view_slider" style="min-width: 100%;">
                        <div class="event_view_right_reg_box" style="padding-bottom: 20px">
                            <!--                <div class="event_view_reg_row1">
                                                        <div class="col-xs-12 event_view_reg_col text-center">
                                                            <div class="pull-right" style="margin:0 3% 3% 0">
                                                                <i class="fa fa-calendar" aria-hidden="true"></i> <span ><b>Posted on: </b><?php /*echo date('M d Y', strtotime($opportunityDetails['start_date'])) */ ?> </span>
                                                                - <span ><b>End Date: </b><?php /*echo date('M d Y', strtotime($opportunityDetails['end_date'])) */ ?></span>
                                                            </div>
                                                        </div>
                                                    </div>-->
                            <div class="event_view_reg_row2" style="padding-bottom: 40px;">
                                <div class="time_count_box col-md-6 text-center col-xs-12"><i class="fa fa-calendar"
                                                                                              aria-hidden="true"></i>
                                    <b>End
                                        Date: </b><?php echo date('M d, Y', strtotime($opportunityDetails->end_date)) ?>
                                </div>
                                <div class="time_count_box col-md-6 col-xs-12 col-sm-6 ">
                                    <ul class="countdown text-center">
                                        <li>
                                            <span class="days">00</span>
                                            <p class="days_ref">days</p>
                                        </li>
                                        <li>
                                            <span class="hours">00</span>
                                            <p class="hours_ref">hours</p>
                                        </li>
                                        <li>
                                            <span class="minutes">00</span>
                                            <p class="minutes_ref">minutes</p>
                                        </li>
                                        <li>
                                            <span class="seconds">00</span>
                                            <p class="seconds_ref">seconds</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr style="margin: 0.5em;">
                    <div class="col-xs-12 event_view_right">
                        <div class="event_view_right_top">
                            <h3>Details:</h3>
                            <p class="directory_profile_text1"><?php echo htmlspecialchars_decode(html_entity_decode($opportunityDetails->description)); ?></p>

                        </div>
                        <hr style="margin: 0.5em;">
                        <div class="event_view_reg_row2 text-center">
                            <a href="<?php echo prep_url($opportunityDetails->registration_link); ?>" target="_blank"
                               class="register_btn">visit: <?php echo $opportunityDetails->registration_link; ?></a>
                        </div>
                    </div>

                    <?php $fullUrl = base_url() . $this->uri->uri_string(); ?>
                    <?php $url= 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
                    $actual_link_report=str_replace("/","--",$url);
                    ?>

                    <div class="col-xs-12">
                        <div class="directory_profile_social_share">
                            <i style="color: #012F49" class="fa fa-share-alt" aria-hidden="true"></i>
                            <a class="facebook customer share" style="color: #5D7FBF"
                               href="http://www.facebook.com/sharer.php?u=<?php echo urlencode($fullUrl); ?>"
                               title="Facebook share"
                               target="_blank"><i class="fa fa fa-facebook fa-lg" style="margin: 0 8px;"
                                                  aria-hidden="true"></i></a>
                            <a class="twitter customer share" style="color: #1b95e0"
                               href="http://twitter.com/share?url=<?php echo urlencode($fullUrl); ?>&amp;hashtags=youthopiabangla"
                               title="Twitter share" target="_blank"><i class="fa fa-twitter fa-lg"
                                                                        style="margin: 0 8px;"
                                                                        aria-hidden="true"></i></a>
                            <a class="google_plus customer share" style="color: #db4437"
                               href="https://plus.google.com/share?url=<?php echo urlencode($fullUrl); ?>"
                               title="Google Plus Share"
                               target="_blank"><i class="fa fa-google-plus fa-lg" style="margin: 0 8px;"
                                                  aria-hidden="true"></i></a>
                            <a class="email modalLink" data-modal-size="modal-md"
                               style="color: #79B04A;"
                               href="<?php echo site_url() . '/opportunity/report_problem/' . $opportunityDetails->id . '/' . $linkShare . '/' . $actual_link_report ?>"
                               title="Mail to us"><i
                                        class="fa fa-envelope fa-lg"
                                        style="margin: 0 8px;" aria-hidden="true"></i></a>

                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <hr/>
                </div>


                <!--             <div class="container">
                    <h3>Comment Section</h3>
                    <section class="blog-comments" id="easyPaginate">
                        <?php
                if (!empty($blogComments)) {
                    foreach ($blogComments as $blogComment) { ?>
                                <article class="comment show_block_elem">
                                    <a class="comment-img" href="">
                                        <img src="<?php echo !empty($blogComment->author_image) && file_exists($blogComment->author_image) ? base_url() . $blogComment->author_image : base_url() . 'upload/avater.png'; ?>"
                                             alt="<?php echo $blogComment->author_name ?>"
                                             title="<?php echo (isset($blogComment->org_name) && !empty($blogComment->org_name)) ? $blogComment->org_name : $blogComment->author_name ?>"
                                             width="50" height="50">
                                    </a>
                                    <div class="comment-body ">
                                        <div class="panel panel-default text">

                                            <div class="panel-body"><?php echo htmlspecialchars_decode(html_entity_decode($blogComment->comment)) ?></div>
                                            <div class="panel-footer attribution clearfix">
                                                <p class="pull-left text-info">by
                                                    - <?php echo (isset($blogComment->org_name) && !empty($blogComment->org_name)) ? $blogComment->org_name : $blogComment->author_name ?></p>
                                                <p class="pull-right text-muted"><?php
//                                                    $created = new \Carbon\Carbon($blogComment['created_at']);
//                                                    echo $created->diffForHumans(\Carbon\Carbon::now());

                        $nowtime = time();
                        $oldtime = strtotime($blogComment->created_at);

                        echo $this->human_readable_time->time_elapsed_A($nowtime - $oldtime) . "\n";
//                                                  echo "time_elapsed_B: ".$this->human_readable_time->time_elapsed_B($nowtime-$oldtime)."\n";


                        ?>
                                                    <?php if (isset($this->session->userdata['user_logged_user']['userId']) && !empty($this->session->userdata['user_logged_user']['userId']) && $blogComment->user_id == $this->session->userdata['user_logged_user']['userId']) { ?>
                                                        |
                                                        <a href="<?php echo site_url() . '/blog/deleteComment/' . $blogDetails->id . '/' . $blogComment->id; ?>"
                                                           class="pull-right text-danger" title="Delete Comment"
                                                           onclick="return confirm('Confirm delete?')"><i
                                                                    class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                    <?php } ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            <?php }
                } else {
                    echo '<p class="text-center">This media blog have no comment</p>';
                } ?>
                    </section>
                    <?php if (isset($this->session->userdata['user_logged_user']['userId']) && !empty($this->session->userdata['user_logged_user']['userId'])) { ?>
                        <form class="box" method="post" id="blog-comment-form"
                              action="<?php echo site_url() . '/blog/submitComment/' . $blogDetails->id ?>"
                              enctype="multipart/form-data">
                            <div class="form-group row new_rz_box2">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <h3>Comment *</h3>
                                    <textarea name="description" id="description" rows="3"></textarea>
                                </div>
                            </div>

                            <input type="hidden" name="blog-id" value="<?php echo $blogDetails->id ?>">
                            <input type="hidden" name="user-id" value="<?php echo $this->session->userdata['user_logged_user']['userId'] ?>">
                            <div class="form-group row new_rz_box2">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <button class="rz_button3 btn btn-success btn-lg" type="submit" name="submitComment"
                                            id="submitComment">Submit Listing
                                    </button>
                                </div>
                            </div>

                        </form>
                    <?php } ?>
                </div> -->

                <div class="clearfix"></div>


            </div>
        </div>
    </section>
</div>

<!--<script src="--><?php //echo base_url();?><!--old/assets/vendors/ckeditor2/ckeditor.js"></script>-->
<!--<script src="--><?php //echo base_url();?><!--old/assets/vendors/ckeditor2/samples/js/sample.js"></script>-->

<script>
    //    initSample();
    $("#description").attr("required", "required");
    $("#submitComment").click(function () {
        CKEDITOR.instances.description.updateElement();
    });

    $(function () {
        $('#easyPaginate').easyPaginate({
            paginateElement: '.show_block_elem',
            elementsPerPage: 10,
            effect: 'default'
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#event_view_slider").owlCarousel({
            autoPlay: 3000,
            pagination: false,
            navigation: true,
            singleItem: true,
        });
    });

    function reactionFunction(type, blog_id, user_id = null) {

        if (!user_id) {
            alert('Please login to perform this task');
            return false;
        }
        $.ajax({
            url: '../calSearch',
            type: 'POST',
            data: {
                blog_type: type,
                user_id: user_id,
                blog_id: blog_id,
                action: 'blogInterest'
            }
        }).done(function (response) {
            var res = $.parseJSON(response);
            console.log(res);

            $('#like-count').html(0);
            $('#smiley-count').html(0);
            $('#love-count').html(0);
            $('#wow-count').html(0);
            res.count.forEach(function (counts) {
                if (counts.type == '1') {
                    $('#like-count').html(counts.interest_count);
                }
                else if (counts.type == '2') {
                    $('#smiley-count').html(counts.interest_count);
                }
                else if (counts.type == '3') {
                    $('#love-count').html(counts.interest_count);
                }
                else if (counts.type == '4') {
                    $('#wow-count').html(counts.interest_count);
                }
                else {
                }
            });
            if (!res.ack) {
                alert(res.msg);
            }
        });
    }
</script>
<script>
    //    (function(d, s, id) {
    //        var js, fjs = d.getElementsByTagName(s)[0];
    //        if (d.getElementById(id)) return;
    //        js = d.createElement(s); js.id = id;
    //        js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.8";
    //        fjs.parentNode.insertBefore(js, fjs);
    //    }(document, 'script', 'facebook-jssdk'));
</script>
<script>
    //    window.twttr = (function(d, s, id) {
    //        var js, fjs = d.getElementsByTagName(s)[0],
    //            t = window.twttr || {};
    //        if (d.getElementById(id)) return t;
    //        js = d.createElement(s);
    //        js.id = id;
    //        js.src = "https://platform.twitter.com/widgets.js";
    //        fjs.parentNode.insertBefore(js, fjs);
    //
    //        t._e = [];
    //        t.ready = function(f) {
    //            t._e.push(f);
    //        };
    //
    //        return t;
    //    }(document, "script", "twitter-wjs"));
</script>
<script>
    //    window.___gcfg = {
    //        parsetags: 'onload'
    //    };
</script>
<script>
    $(function ($) {
        /**
         * jQuery function to prevent default anchor event and take the href * and the title to make a share popup
         *
         * @param  {[object]} e           [Mouse event]
         * @param  {[integer]} intWidth   [Popup width defalut 500]
         * @param  {[integer]} intHeight  [Popup height defalut 400]
         * @param  {[boolean]} blnResize  [Is popup resizeabel default true]
         */
        $.fn.customerPopup = function (e, intWidth, intHeight, blnResize) {
            // Prevent default anchor event
            e.preventDefault();
            // Set values for window
            intWidth = intWidth || '500';
            intHeight = intHeight || '400';
            strResize = (blnResize ? 'yes' : 'no');
            // Set title and open popup with focus on it
            var strTitle = ((typeof this.attr('title') !== 'undefined') ? this.attr('title') : 'Social Share'),
                strParam = 'width=' + intWidth + ',height=' + intHeight + ',resizable=' + strResize,
                objWindow = window.open(this.attr('href'), strTitle, strParam).focus();
        }
        /* ================================================== */
        $(document).ready(function ($) {
            $('.customer.share').on("click", function (e) {
                $(this).customerPopup(e);
            });
        });
    }(jQuery));
</script>
<script src="https://apis.google.com/js/platform.js" async defer></script>
<!-- JS ENDS -->

<script>
    //    initializeReadMore();
</script>
<script>
    $(".alert").fadeTo(2000, 500).slideUp(500, function () {
        $(".alert").alert('close');
    });
</script>

<script>
    $(document).ready(function () {

        $('.countdown').downCount(
            {
                date: '<?php echo date('m/d/Y H:i:s', strtotime($opportunityDetails->start_date . ' ' . '00:00:00')) ?>',
                offset: +6
            }, function () {

            }
        );

        $('#interested_btn').on('click', function () {
            var user_id = $(this).data('user-id');
            var opportunity_id = $(this).data('event-id');
            if (!user_id) {
                alert('Please login to perform this task');
                return false;
            }
            $.ajax({
                url: '../calSearch',
                type: 'POST',
                data: {
                    user_id: user_id,
                    opportunity_id: opportunity_id,
                    action: 'opportunityInterest'
                }
            }).done(function (response) {
                var res = $.parseJSON(response);

                $('#interested_count').html(res.count);
                if (!res.ack) {
                    alert(res.msg);
                }
            });
        });

        $('#going_btn').on('click', function () {

            var user_id = $(this).data('user-id');
            var event_id = $(this).data('event-id');
            if (!user_id) {
                alert('Please login to perform this task');
                return false;
            }
            $.ajax({
                url: '../calSearch',
                type: 'POST',
                data: {
                    user_id: user_id,
                    event_id: event_id,
                    action: 'going'
                }
            }).done(function (response) {
                var res = $.parseJSON(response);
                $('#going_count').html(res.count);
                if (!res.ack) {
                    alert(res.msg);
                }
            });
        })
    });
</script>