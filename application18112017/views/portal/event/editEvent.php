<style>
    .new_rz_box {
        display: block;
        margin-top: 40px;
        padding: 10px 0;
        border-bottom: 1px solid #ccc;
    }

    .new_rz_box h6 {
        color: #446678;
        text-transform: uppercase;
    }

    .new_rz_box h5 {
        color: #446678;
        text-transform: uppercase;
    }

    .new_rz_box h3 {
        color: #446678;
        text-transform: uppercase;
    }

    .new_rz_box h4 {
        color: #333;
    }

    .new_rz_box2 {
        display: block;
        padding: 10px 0;
    }

    .new_rz_box2 h3 {
        color: #446678;
        margin-bottom: 10px;
    }

    .new_rz_box2 input {
        border: 1px solid #d9d9d9;
        width: 100%;
        padding: 5px;
    }

    .new_rz_box2 textarea {
        border: 1px solid #d9d9d9;
        width: 100%;
        padding: 10px 20px;
    }

    .new_rz_box2 select {
        border: 1px solid #d9d9d9;
        width: 100%;
        padding: 5px 10px;
    }

    .rz_button3 {
        margin-top: 40px;
        border: 0;
        background-color: #012F49;
        color: #FFF;
        font-size: 25px;
        text-align: center;
        padding: 10px 20px;
        text-transform: uppercase;
        border-radius: 10px;
    }

    .rz_button3:hover {
        color: #FFF;
    }
</style>
<style type="text/css">
    .thumb {
        width: 50px;
        height: 50px;
    }

    #list {
        position: absolute;
        top: 0px;
    }

    #list div {
        float: left;
        margin-right: 10px;
    }

    .hovereffect {
        height: 200px;
        margin: auto;
        overflow: hidden;
        transition: all 1s ease 0s;
    }

    .z-img img {
        transition: all 0.6s ease 0s;
    }

    .hovereffect:hover .z-img img {
        transform: scale(1.9);
    }

    .z-text {
        position: absolute;
        width: 90%;
        height: 200px;
        border: 1px solid gray;
        top: -199px;
        z-index: 1111;
        display: none;
        color: white;
        text-align: center;
        padding: 10px;
        border: 1px solid white;

    }

</style>
<style type="text/css">
    .thumb {
        width: 50px;
        height: 50px;
    }

    #list {
        position: absolute;
        top: 0px;
    }

    #list div {
        float: left;
        margin-right: 10px;
    }
</style>


<div class="others_page_wrp">
    <section class="org_map_wrp">
        <div class="container">
            <form class="box" method="post"
                  action="<?php echo site_url(); ?>event/insertEditedEvent/<?php echo $event_info->id; ?>"
                  enctype="multipart/form-data">

                <input type="hidden" name="org_id" value="<?php echo $event_info->organization_id; ?>">
                <input type="hidden" name="img" value="<?php echo $event_info->images; ?>">

                <div class="row new_rz_box">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <h5>Name of Organization *</h5>
                        <h4><?php echo (!empty($organizationDetails->title)) ? $organizationDetails->title : ''; ?></h4>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <h6>Full Address *</h6>
                        <h4><?php echo (!empty($organizationDetails->location)) ? $organizationDetails->location : ''; ?></h4>
                    </div>
                </div>

                <div class="row new_rz_box2">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h3>Event Title</h3>
                        <input type="text" name="event_name"
                               value="<?php echo (!empty($event_info->event_name)) ? $event_info->event_name : '' ?>"
                               placeholder="Event title" maxlength="100"
                               required/>
                    </div>
                </div>
                <div class="row new_rz_box2">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h3>Category</h3>
                        <?php $categoryList = array('Local', 'National', 'International'); ?>
                        <select name="event_category" required>
                            <option value="">Select Category</option>
                            <?php foreach ($categoryList as $cat_list) : ?>
                                <option value="<?php echo $cat_list; ?>" <?php echo ($cat_list == $event_info->category) ? 'selected = "selected"' : '' ?> ><?php echo $event_info->category; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="row new_rz_box2">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h3>About Event</h3>
                        <textarea class="redactor" name="event_description"
                                  placeholder="Don't include any link URL directly. You can just type the link URL"
                                  rows="3"
                                  required><?php echo html_entity_decode($event_info->description); ?> </textarea>
                    </div>
                </div>

                <!-- DatePicker -->


                <!-- DatePicker -->

                <script>
                    $(function () {
                        $("#datepicker").datepicker({format: 'dd-mm-yyyy'});
                        $("#datepicker2").datepicker({format: 'dd-mm-yyyy'});
                    });
                </script>

                <div class="row new_rz_box2">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <h3>Event Date:</h3>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <p><i style="position:absolute; right:15%; top:25%;"
                                      class="glyphicon glyphicon-calendar fa fa-calendar"></i><input type="text"
                                                                                                     name="start_date"
                                                                                                     value="<?php echo date('d-m-Y', strtotime($event_info->start_date)); ?>"
                                                                                                     id="datepicker"
                                                                                                     placeholder="Start"/>
                                </p>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <p><i style="position:absolute; right:15%; top:25%;"
                                      class="glyphicon glyphicon-calendar fa fa-calendar"></i><input type="text"
                                                                                                     name="end_date"
                                                                                                     value="<?php echo date('d-m-Y', strtotime($event_info->end_date)); ?>"
                                                                                                     id="datepicker2"
                                                                                                     placeholder="End"/>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <h3>Event Time:</h3>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <?php
                                $exStartTime = DateTime::createFromFormat('H:i:s', $event_info->start_time);
                                $exStartTime = $exStartTime->format('g:i A');
                                ?>
                                <select name="start_time">
                                    <?php foreach ($timeRange as $time): ?>
                                        <option value="<?php echo $time; ?>" <?php echo ($time == $exStartTime) ? 'selected = "selected' : '' ?>><?php echo $time; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <?php
                                $exEndTime = DateTime::createFromFormat('H:i:s', $event_info->end_time);
                                $exEndTime = $exEndTime->format('g:i A');
                                ?>

                                <select name="end_time">
                                    <?php foreach ($timeRange as $time): ?>
                                        <option value="<?php echo $time; ?>" <?php echo ($time == $exEndTime) ? 'selected = "selected' : '' ?>><?php echo $time; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row new_rz_box2">
                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                        <h3>SEAT LIMIT:</h3>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                        <input style="float:left; width:auto;" type="radio" name="limit_type" class="limit_type"
                               value="no_limit" <?php echo ($event_info->seat_limit < 1) ? 'checked="checked"' : '' ?>
                        <label style="float:left; margin-left:5px;">NO LIMIT</label>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                        <input style="float:left; width:auto;" type="radio" name="limit_type" class="limit_type"
                               value="limit" <?php echo ($event_info->seat_limit > 0) ? 'checked="checked"' : '' ?> />
                        <label style="float:left; margin-left:5px;">LIMITED</label>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <input type="number" name="seat_limit"
                               id="seat_limit" <?php echo ($event_info->seat_limit > 0) ? '' : 'disabled="disabled"' ?>
                               value="<?php echo ($event_info->seat_limit > 0) ? $event_info->seat_limit : '' ?>"/>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <h4 style="text-align:center;">Person</h4>
                    </div>
                </div>

                <div class="row new_rz_box2" style="margin-top:10px;">
                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                        <h3>COST:</h3>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                        <input style="float:left; width:auto;" <?php echo ($event_info->event_fee < 1) ? 'checked="checked"' : '' ?>
                               class="cost_type" type="radio" name="cost_type" value="free"/> <label
                                style="float:left; margin-left:5px;">FREE</label>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                        <input style="float:left; width:auto;" class="cost_type" type="radio" name="cost_type"
                               value="paid" <?php echo ($event_info->event_fee > 0) ? 'checked="checked"' : '' ?> />
                        <label style="float:left; margin-left:5px;">PAID</label>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <input type="number" name="event_cost" id="event_cost"
                               value="<?php echo ($event_info->event_fee > 0) ? $event_info->event_fee : '' ?>" <?php echo ($event_info->event_fee > 0) ? '' : 'disabled="disabled"' ?> />
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <h4 style="text-align:center;">TAKA</h4>
                    </div>
                </div>
                <div class="row new_rz_box2">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="city">District</label>
                            <select name="district" id="city">
                                <option value="">Select District</option>
                                <?php foreach ($cities as $city): ?>
                                    <option value="<?php echo $city->name; ?>" <?php echo ($city->name == $event_info->district) ? 'selected = "selected"' : '' ?> ><?php echo $city->name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row new_rz_box2">
                    <div class="col-md-12 col-sm-12">
                        <div class="form-group">
                            <label for="address-autocomplete">Full Address *</label>
                            <input type="text" class="form-control" name="address" id="address-autocomplete"
                                   required="required" value="<?php echo $event_info->address; ?>">
                        </div>
                        <!--end form-group-->
                        <!--end map-->
                        <div class="form-group hidden">
                            <input type="text" class="form-control" id="latitude" name="latitude" hidden=""
                                   value="<?php echo $event_info->latitude; ?>">
                            <input type="text" class="form-control" id="longitude" name="longitude" hidden=""
                                   value="<?php echo $event_info->longitude; ?>">
                        </div>
                    </div>
                </div>
                <div style="height: 200px; width: 100%; display: block">
                    <div id="map-canvas" style="width: 100%; height: 100%"></div>
                </div>


                <input type="hidden" name="ext_image" value="<?php echo $event_info->images; ?>">

                <?php $images = explode('___', $event_info->images); ?>


                <span id="blk-image" class="toHide" style="display:block; padding: 0px; margin: 0px;">
                    <?php if (!empty($event_info->images)) { ?>
                        <div class="row new_rz_box2">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <h3>Old Images</h3>
                            <?php foreach ($images as $image): ?>
                                <div class="col-sm-3">
                                    <div class="hovereffect">
                                        <div class="z-text">
                                            <button type="button" style="margin-top: 30%"
                                                    class="btn btn-danger delete-button" title="Delete"
                                                    event-id="<?php echo $event_info->id ?>"
                                                    image-name="<?php echo $image ?>"><i class="fa fa-remove"
                                                                                         aria-hidden="true"></i></button>
                                        </div>
                                        <div class="z-img">
                                            <img src="<?php echo base_url(); ?>upload/events/<?php echo $image ?>"
                                                 alt="post img"
                                                 class="image pull-left img-responsive postImg img-thumbnail margin10"
                                                 height="200px">
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="row new_rz_box2">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <h3>Gallery</h3>
                            <input type="file" name="files">
                        </div>
                    </div>
                </span>


                <!--                <div class="row new_rz_box2">-->
                <!--                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">-->
                <!--                        <h3>Gallery</h3>-->
                <!---->
                <!--                        <div class="file-upload-2">-->
                <!--                            <input type="file" class="multi with-preview" id="galleryfiles" maxlength="10"-->
                <!--                                   onchange="loadFile(event)"   accept="image/gif, image/jpg, image/jpeg, image/png" name="galleryfiles" multiple/>-->
                <!--                            <img src="--><?php //echo base_url();?><!--upload/events/-->
                <?php //echo $event_info->images; ?><!--" style="height: 80px; width: 80px; display: block;" id="output"/>-->
                <!--                        </div>-->
                <!--                    </div>-->
                <!--                </div>-->
                <div class="row new_rz_box2">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h3>Event Registration Link:</h3>
                        <input type="text" name="registration_link"
                               value="<?php echo $event_info->registration_link; ?>"
                               placeholder="http://www.facebook.com/event/"/>
                    </div>
                </div>


                <div class="row new_rz_box2">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h3>Email:</h3>
                        <input type="email" name="email"
                               value="<?php echo (!empty($event_info->event_name)) ? $event_info->email : '' ?>"
                               placeholder="Email Address" maxlength="100"
                        />
                    </div>
                </div>

                <div class="row new_rz_box2">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h3>Mobile:</h3>
                        <input type="number" name="mobile"
                               value="<?php echo (!empty($event_info->mobile)) ? $event_info->mobile : '' ?>"
                               placeholder="Mobile Number" maxlength="100"
                        />
                    </div>
                </div>

                <div class="row new_rz_box2">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h3>Phone:</h3>
                        <input type="number" name="phone"
                               value="<?php echo (!empty($event_info->phone)) ? $event_info->phone : '' ?>"
                               placeholder="Phone Number" maxlength="100"
                        />
                    </div>
                </div>

                <div class="row new_rz_box2">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h3>Website:</h3>
                        <input type="text" name="website"
                               value="<?php echo (!empty($event_info->url)) ? $event_info->url : '' ?>"
                               placeholder="Website" maxlength="100"
                        />
                    </div>
                </div>

                <div class="row new_rz_box2">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h3>Facebook:</h3>
                        <input type="text" name="facebook"
                               value="<?php echo (!empty($event_info->facebook)) ? $event_info->facebook : '' ?>"
                               placeholder="Website" maxlength="100"
                        />
                    </div>
                </div>

                <div class="row new_rz_box2">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h3>Twitter:</h3>
                        <input type="text" name="twitter"
                               value="<?php echo (!empty($event_info->twitter)) ? $event_info->twitter : '' ?>"
                               placeholder="Twitter" maxlength="100"
                        />
                    </div>
                </div>


                <p style="text-align:center; margin-bottom:60px;">
                    <button class="rz_button3" type="submit" name="submitEvent">Publish Event</button>
                </p>


            </form>
        </div>
    </section>
</div>

<script type="text/javascript"
        src="https://maps.google.com/maps/api/js?key=AIzaSyCgGR-bhDxioHdiX8mtFYkNliRAY6CsLao&libraries=places"></script>
<script>
    var markers = [];

    function initialize() {
        geocoder = new google.maps.Geocoder();
        var input = document.getElementById('address-autocomplete');
        var autocomplete = new google.maps.places.Autocomplete(input);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();
            $('#latitude').val(place.geometry.location.lat());
            $('#longitude').val(place.geometry.location.lng());
            codeAddress();
        });

        var mapOptions = {
            center: new google.maps.LatLng(23.778593, 90.398086),
            zoom: 12,
            styles: [{"elementType": "geometry", "stylers": [{"color": "#f5f5f5"}]}, {
                "elementType": "labels.icon",
                "stylers": [{"visibility": "off"}]
            }, {
                "elementType": "labels.text.fill",
                "stylers": [{"color": "#616161"}]
            }, {
                "elementType": "labels.text.stroke",
                "stylers": [{"color": "#f5f5f5"}]
            }, {
                "featureType": "administrative.land_parcel",
                "elementType": "labels.text.fill",
                "stylers": [{"color": "#bdbdbd"}]
            }, {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [{"color": "#eeeeee"}]
            }, {
                "featureType": "poi",
                "elementType": "labels.text.fill",
                "stylers": [{"color": "#757575"}]
            }, {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [{"color": "#e5e5e5"}]
            }, {
                "featureType": "poi.park",
                "elementType": "labels.text.fill",
                "stylers": [{"color": "#9e9e9e"}]
            }, {
                "featureType": "road",
                "elementType": "geometry",
                "stylers": [{"color": "#ffffff"}]
            }, {
                "featureType": "road.arterial",
                "elementType": "labels.text.fill",
                "stylers": [{"color": "#757575"}]
            }, {
                "featureType": "road.highway",
                "elementType": "geometry",
                "stylers": [{"color": "#dadada"}]
            }, {
                "featureType": "road.highway",
                "elementType": "labels.text.fill",
                "stylers": [{"color": "#616161"}]
            }, {
                "featureType": "road.local",
                "elementType": "labels.text.fill",
                "stylers": [{"color": "#9e9e9e"}]
            }, {
                "featureType": "transit.line",
                "elementType": "geometry",
                "stylers": [{"color": "#e5e5e5"}]
            }, {
                "featureType": "transit.station",
                "elementType": "geometry",
                "stylers": [{"color": "#eeeeee"}]
            }, {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [{"color": "#c9c9c9"}]
            }, {"featureType": "water", "elementType": "labels.text.fill", "stylers": [{"color": "#9e9e9e"}]}]
        };
        map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
        var ex_latitude = $('#latitude').val();
        var ex_longitude = $('#longitude').val();
        if (ex_latitude != '' && ex_longitude != '') {

            map.setCenter(new google.maps.LatLng(ex_latitude, ex_longitude));//center the map over the result

            var marker = new google.maps.Marker(
                {
                    map: map,
                    draggable: true,
                    //icon: "https://www.ghurbo.com/uploads/global/default/marker.png",
                    animation: google.maps.Animation.DROP,
                    position: new google.maps.LatLng(ex_latitude, ex_longitude)
                });
            markers.push(marker);
            google.maps.event.addListener(marker, 'dragend', function () {
                var marker_positions = marker.getPosition();
                $('#latitude').val(marker_positions.lat());
                $('#longitude').val(marker_positions.lng());
            });
        }
    }

    function codeAddress() {

        var address = $('#address-autocomplete').val();
        // var address = [main_address,city, state, country].join();
        if (address != '') {
            setAllMap(null); //Clears the existing marker
            geocoder.geocode({address: address}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    $('#latitude').val(results[0].geometry.location.lat());
                    $('#longitude').val(results[0].geometry.location.lng());
                    map.setCenter(results[0].geometry.location);//center the map over the result

                    //place a marker at the location
                    var marker = new google.maps.Marker(
                        {
                            map: map,
                            draggable: true,
                            animation: google.maps.Animation.DROP,
                            position: results[0].geometry.location
                        });
                    markers.push(marker);
                    google.maps.event.addListener(marker, 'dragend', function () {
                        var marker_positions = marker.getPosition();
                        $('#latitude').val(marker_positions.lat());
                        $('#longitude').val(marker_positions.lng());
                    });
                    var bounds = new google.maps.LatLngBounds();
                    for (var i = 0; i < markers.length; i++) {
                        bounds.extend(markers[i].getPosition());
                    }
//                    map.fitBounds(bounds);
                    map.setZoom(14);
                } else {
                    alert('Geocode was not successful for the following reason: ' + status);
                }
            });
        }
        else {
            alert('You must enter at least Address');
        }
    }

    function setAllMap(map) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    }

    google.maps.event.addDomListener(window, 'load', initialize);
</script>

<script>
    $(function () {
        $('.cost_type').on('click', function () {
            var val = $('input[name="cost_type"]:checked').val();
            if (val === 'paid') {
                $('#event_cost').prop('disabled', false);
            } else {
                $('#event_cost').val('').prop('disabled', true);
            }
//            alert(val);
        })

        $('.limit_type').on('click', function () {
            var val = $('input[name="limit_type"]:checked').val();
            if (val === 'limit') {
                $('#seat_limit').prop('disabled', false);
            } else {
                $('#seat_limit').val('').prop('disabled', true);
            }
//            alert(val);
        })
    })
</script>

<script>

    function handleFileSelect(evt) {
        var files = evt.target.files; // FileList object

        // Loop through the FileList and render image files as thumbnails.
        for (var i = 0, f; f = files[i]; i++) {

            // Only process image files.
            if (!f.type.match('image.*')) {
                continue;
            }

            var reader = new FileReader();

            // Closure to capture the file information.
            reader.onload = (function (theFile) {
                return function (e) {
                    // Render thumbnail.
                    var span = document.createElement('div');
                    span.setAttribute('class', 'img_wrpv');
                    span.innerHTML = ['<a href="#" style="color:red; font-size:18px; position: relative; z-index: 200;" class="img_revg">x</a><img width="50px" height="50px" class="thumb" src="', e.target.result,
                        '" title="', escape(theFile.name), '"/>'].join('');
                    document.getElementById('list').insertBefore(span, null);
                };
            })(f);

            // Read in the image file as a data URL.
            reader.readAsDataURL(f);

            //remove image
            $('.img_wrpv').on('click', function (e) {
                e.preventDefault();
                var parent = $(this).parent();
                parent.remove();
            });

        }
    }

    //remove image
    $('.img_revg').on('click', function () {

        var parent = $(this).parent();
        parent.remove();
    });
    //            $(document).on('change','#galleryfiles', handleFileSelect);
    //            document.getElementById('galleryfiles').addEventListener('change', handleFileSelect, true);
    //    var a_href = $('#facebook_url_for_signup').attr('href');
    //
    //    $('#facebook_url_for_login').attr('href', a_href);
</script>

<script>
    var loadFile = function (event) {
        $("#output").css("display", "block");
        var output = document.getElementById('output');
        output.src = URL.createObjectURL(event.target.files[0]);
    };
</script>

<script>
    $(document).ready(function () {
        $('.hovereffect').hover(function () {
            $(this).children('.z-text').css('top', '0').fadeToggle(600);
        });
    });

</script>

<script>
    $(document).ready(function () {
        $(".delete-button").click(function () {
            var event_id = $(this).attr('event-id');
            var image_name = $(this).attr('image-name');
            var removeImage = $(this).parent().parent().parent();
            $.ajax({
                url: '../calSearch',
                type: 'POST',
                data: {
                    event_id: event_id,
                    img_name: image_name,
                    action: 'eventImageDelete'
                }
            }).done(function (response) {
                var res = $.parseJSON(response);
                alert(res.msg);
                if (res.ack) {
                    removeImage.fadeOut();
                }
            })
        });
    });


</script>



