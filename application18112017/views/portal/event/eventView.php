<!--  **************************** FB Imo Start *************************************************** -->
<script>
    $(document).ready(function () {
        $('.FB_reactions').facebookReactions2({
            postUrl: "<?php echo base_url() . 'event/fbImo';?>"
        });

        $('.FB_reactions').on('click', function () {

            var $this = $(this);
            var user_id = $this.attr('data-user-id');
            var control_id = $this.attr('data-unique-id');
            var value = $this.attr('data-emoji-id');
            var imo_track = 'imo_track';

//            alert(value);

            if (user_id.length === 0 || user_id == null) {
                alert("Please login to perform this task!");
            } else
            {
                $.ajax({
                    type: 'POST',
                    url: '<?php echo site_url() . '/event/fbImo';?>',
                    data: {user_id: user_id, control_id: control_id, value: value, imo_track: imo_track}, // our data object
                    success: function (response) {

                        var res = $.parseJSON(response);
                        console.log(res);
//                    alert(res.un_chg);
                        if(res.un_chg == 1)
                        {
                            $('.this_cls_' + res.event_id).attr('data-emoji-id', 'like');
                        }
                        else if(res.un_chg == 2)
                        {
                            $('.this_cls_' + res.event_id).attr('data-emoji-id', '');
                        }




                        console.log( Object.keys(res).length );

                        $('#like-count' + res.event_id).html(0);
                        $('#smiley-count' + res.event_id).html(0);
                        $('#love-count' + res.event_id).html(0);
                        $('#wow-count' + res.event_id).html(0);

                        res.count.forEach(function (counts) {
                            if (counts.type == '1') {
                                $('#like-count' + res.event_id).html(counts.interest_count);
                            }
                            else if (counts.type == '2') {
                                $('#smiley-count' + res.event_id).html(counts.interest_count);
                            }
                            else if (counts.type == '3') {
                                $('#love-count' + res.event_id).html(counts.interest_count);
                            }
                            else if (counts.type == '4') {
                                $('#wow-count' + res.event_id).html(counts.interest_count);
                            }
                            else {
                            }
                        });
                        if (!res.ack) {
                            alert(res.msg);
                        }


                    },
                    error: function () {

                        // alert('<p>An error has occurred</p>');
                        console.log('An error has occurred');
                    }
                });

            }


        });
    });

</script>

<style>
    .c_btn {
        background-color: ; /* Green */
        border: none;
        color: darkblue;
        padding: 04px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 13px;
        margin: 4px 2px;
        cursor: pointer;
        font-weight: 400;
    }


.frame {      
    height: 390px;
    display: table;
    width: 100%;
    text-align: center;
    padding: 0px; 
}

.helper22 {
    display: table-cell;
    text-align: center;
    vertical-align: middle;
    height: 390px;
}

.frame>span>img {
    vertical-align: middle;
    display: inline-block !important;
    width: auto  !important;
    max-height: 100%;
    max-width: 100%;
}
.frame2 {      
    height: 120px;
    display: table;
    width: 100%;
    text-align: center;
    padding: 0px; 
}

.helper222 {
    display: table-cell;
    text-align: center;
    vertical-align: middle;
    height: 120px;
}

.frame2>span>img {
    vertical-align: middle;
    display: inline-block !important;
    width: auto  !important;
    max-height: 100%;
    max-width: 100%;
}




    .frame222{
        height: 390px;
        display: table;
        width: 100%;
        text-align: center;
        padding: 0px;
    }

    .helper23 {
        display: table-cell;
        text-align: center;
        vertical-align: middle;
        height: 390px;
    }

    .frame222>span>img {
        vertical-align: middle;
        display: inline-block !important;
        width: auto  !important;
        max-height: 100%;
        max-width: 100%;
    }

</style>


<!--  **************************** FB Imo End *************************************************** -->


<style>
    .event_calender_slider {
        margin-bottom: 60px;
    }

    .rz_button {
        background-color: #012F49;
        color: #FFF;
        text-align: center;
        display: block;
        margin-top: 10px;
        border-radius: 3px;
    }

    .rz_button2 {
        background-color: #e63433;
        color: #FFF;
        font-size: 15px;
        text-align: center;
        padding: 6px 12px;
        text-transform: uppercase;
        border-radius: 25px;
    }

    .rz_button2:hover {
        color: #FFF;
    }

    .event_calender_slider:after {
        color: #FFFFFF;
        content: "PASSESD EVENTS";
        font-family: 'ralewaysemibold';
        font-size: 15px;
        left: 0;
        line-height: 1.3;
        position: absolute;
        text-align: center;
        top: 35%;
        width: 106px;
    }
    #going_btn, #interested_btn{
        background-color: #F1F3F4;
        border-radius: 15px;
        padding: 3px 8px;
        margin-right: 5px;
        margin-top: 4px;
    }
    #going_btn img, #interested_btn img{
        height: 20px;
        width: auto;
    }

    .share_box_wrp .likes_box {
        padding: 5px 10px;
        margin-top: 4px;
    }




</style>

<div class="others_page_wrp">
    <section class="calender_event_view_wrp">
        <div class="container">
            <div class="calender_event_view_top">
                <div class="event_view_top_left">
                    <span class="view_avatar"><img src="<?php echo base_url();
                        echo $eventDetails->image; ?>" alt=""
                                                   style="width: 70px;"/></span>
                    <div class="event_view_top_left_title">
                        <a class="" href="<?php echo site_url().'event/viewEventModal/'.$eventDetails->id; ?>" target="_blank">
                            <h2
                                style="margin: 12px 0px 0px 0px; color: #012F49;"><?php echo $eventDetails->org_name; ?></h2></a>
                        <span><?php echo $organizationDetails->location; ?></span>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>


            <?php
            $id = '';
            $likeReaction = 0;
            $smileReaction = 0;
            $loveReaction = 0;
            $wowReaction = 0;
            $action = '';
            $action_name = '';


            if (isset($this->session->userdata['user_logged_user']['userId'])) {
                $id = $this->session->userdata['user_logged_user']['userId'];
                $linkShare = $id;
            } else {
                $id = '';
                $linkShare = 0;
            }

            $interests = $this->event_model->getEventInterest2($eventDetails->id, $user_id);
            $inter = $this->event_model->getEventInterest($eventDetails->id, $user_id);

            ?>

            <?php foreach ($interests as $interes) { ?>
                <?php
                if ($interes->type == '1') {
                    $likeReaction = $interes->interest_count;
                    $action = 'like';
                    $action_name = 'LIKE';
                } elseif ($interes->type == '2') {
                    $smileReaction = $interes->interest_count;
                    $action = 'haha';
                    $action_name = 'HAHA';
                } elseif ($interes->type == '3') {
                    $loveReaction = $interes->interest_count;
                    $action = 'love';
                    $action_name = 'LOVE';
                } elseif ($interes->type == '4') {
                    $wowReaction = $interes->interest_count;
                    $action = 'wow';
                    $action_name = 'WOW';
                }
                ?>

            <?php } ?>

            <?php foreach ($inter as $intere) : ?>
                <?php
                if ($intere->type == '1') {
                    $likeReaction = $intere->interest_count;
                } elseif ($intere->type == '2') {
                    $smileReaction = $intere->interest_count;
                } elseif ($intere->type == '3') {
                    $loveReaction = $intere->interest_count;
                } elseif ($intere->type == '4') {
                    $wowReaction = $intere->interest_count;
                }
                ?>
            <?php endforeach; ?>




            <div class="calender_event_view_slider">
                <div id="event_view_slider">

                    <?php
                    $images = explode('___', $eventDetails->images);

                    ?>
                    <?php foreach ($images as $image): ?>
                        <div class="slideritem thumbnail frame222" style="margin-bottom: 5px">
                                <span class="helper23"><img
                                            style=""
                                            src="<?php echo base_url(); ?>upload/events/<?php echo $image ?>"
                                            alt=""/></span></div>
                    <?php endforeach; ?>

                </span></div>
                <div class="title_of_event"><?php echo $eventDetails->event_name; ?></div>
                <div class=" share_box_wrp">

                     <span id="Fb_Imo">
                         <a class="FB_reactions this_cls_<?php echo $eventDetails->id; ?>"
                                          data-reactions-type='horizontal'
                                          data-emoji-id="<?php echo (!empty($action)) ? $action : ''; ?>"
                                          data-user-id="<?php echo $user_id; ?>"
                                          data-unique-id="<?php echo $eventDetails->id; ?>"
                                          data-emoji-class="<?php echo (!empty($action)) ? $action : ''; ?>">
                             <span style=""><?php echo (!empty($action_name)) ? $action_name : 'LIKE'; ?></span>
                         </a>
                     </span>


                    <span class="my_buttons" style="padding: 7px; font-size: 14px;">
                        <span><button class="c_btn" style="border-radius: 12px" href=""> <img style="height: 20px; width: auto;" src="<?php echo base_url().'fb_imo/emojis/like.svg';?>" class="emoji" > <span id="like-count<?php echo $eventDetails->id;?>"><?php echo $likeReaction; ?></span></button></span>
                        <span><button class="c_btn" style="border-radius: 12px" href=""> <img style="height: 20px; width: auto;" src="<?php echo base_url().'fb_imo/emojis/love.svg';?>" class="emoji" > <span id="love-count<?php echo $eventDetails->id;?>"> <?php echo $loveReaction;  ?> </span> </button></span>
                        <span><button class="c_btn" style="border-radius: 12px" href=""> <img style="height: 20px; width: auto;" src="<?php echo base_url().'fb_imo/emojis/haha.svg';?>" class="emoji" > <span id="smiley-count<?php echo $eventDetails->id; ?>"> <?php echo  $smileReaction; ?> </span> </button></span>
                        <span><button class="c_btn" style="border-radius: 12px" href=""> <img style="height: 20px; width: auto;" src="<?php echo base_url().'fb_imo/emojis/wow.svg';?>" class="emoji" > <span id="wow-count<?php echo $eventDetails->id;?>"> <?php echo $wowReaction; ?> </span> </button></span>
                    </span>



                    <span class="likes_box pull-right"><i
                                class="fa fa-eye"></i><span><?php echo $eventDetails->view_count; ?></span></span>

                    <a class="interested_btn pull-right" role="button" id="going_btn" title="Going"
                       data-event-id="<?php echo $eventDetails->id; ?>" data-user-id="<?php echo $user_id; ?>">

                        <img src="<?php echo base_url().'assets/img2/running.png'; ?>">

                        <span id="going_count"><?php echo $going->int_count; ?></span></a>


                    <a class="interested_btn pull-right" role="button" id="interested_btn" title="Interested"
                       data-event-id="<?php echo $eventDetails->id; ?>" data-user-id="<?php echo $user_id; ?>">
                        <img  src="<?php echo base_url().'assets/img2/star.png'; ?>">
                        <span id="interested_count"><?php echo $interest->int_count; ?></span></a>

                </div>
                <div class="clearfix"></div>
            </div>

            <div class="col-sm-5 col-xs-12 event_view_left">
                <div class="event_view_left_map">
                    <h3>Event Location</h3> <br>
                    <p> <?php echo $eventDetails->address; ?> </p>
                    <div id="google_map" style="width: 100%; height: 195px;"></div>
                </div>
                <div class="directory_profile_contact">
                    <h3>Contact</h3>
                    <ul>
                        <li>
                            <span><i class="glyphicon glyphicon-map-marker"></i></span> <?php echo $organizationDetails->location; ?>
                        </li>
                        <?php if (!empty($organizationDetails->phone)): ?>
                            <li><span><i class="fa fa-phone-square"></i></span> <a
                                        href="tel:<?php echo $organizationDetails->phone; ?>"><?php echo $organizationDetails->phone; ?></a>
                            </li>
                        <?php endif; ?>
                        <?php if (!empty($organizationDetails->mobile)): ?>
                            <li><span><i class="fa fa-phone"></i></span> <a
                                        href="tel:<?php echo $organizationDetails->mobile; ?>"><?php echo $organizationDetails->mobile; ?></a>
                            </li>
                        <?php endif; ?>

                        <?php if (!empty($organizationDetails->url)): ?>
                            <li><span><i class="fa fa-globe"></i></span> <a
                                        href="<?php echo $organizationDetails->url; ?>"
                                        target="_blank"><?php echo $organizationDetails->url; ?></a></li>
                        <?php endif; ?>
                        <?php if (!empty($organizationDetails->org_email)): ?>
                            <li><span><i class="fa fa-envelope-o"></i></span> <a
                                        href="mailto:<?php echo $organizationDetails->org_email; ?>"><?php echo $organizationDetails->org_email; ?></a>
                            </li>
                        <?php endif; ?>
                        <?php if (!empty($organizationDetails->facebook)): ?>
                            <li><span><i class="fa fa-facebook"></i></span> <a
                                        href="<?php echo $organizationDetails->facebook; ?>"
                                        target="_blank"><?php echo $organizationDetails->facebook; ?></a></li>
                        <?php endif; ?>
                        <?php if (!empty($organizationDetails->twitter)): ?>
                            <li><span><i class="fa fa-twitter"></i></span> <a
                                        href="<?php echo $organizationDetails->twitter; ?>"
                                        target="_blank"><?php echo $organizationDetails->twitter; ?></a></li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>


            <div class="col-sm-7 col-xs-12 event_view_right">
                <div class="event_view_right_top">
                    <h3>About Event</h3>
                    <div class="read-more" data-collapse-height="75"><p
                                class="directory_profile_text1"><?php echo htmlspecialchars_decode(html_entity_decode($eventDetails->description)); ?></p>
                    </div>
                    <!--                            <a href="" class="directory_profile_more_btn">More</a>-->
                </div>
                <div class="event_view_right_reg_box">
                    <div class="event_view_reg_row1">
                        <div class="col-xs-4 event_view_reg_col">
                            <div class="event_view_reg_content">
                                <p>Date</p>
                                <span><?php echo date('M d Y', strtotime($eventDetails->start_date)) ?>
                                    - <?php echo date('M d Y', strtotime($eventDetails->end_date)) ?></span>
                            </div>
                        </div>
                        <div class="col-xs-3 event_view_reg_col">
                            <div class="event_view_reg_content">
                                <p>Time</p>
                                <?php
                                $startTime = DateTime::createFromFormat('H:i:s', $eventDetails->start_time);
                                $endTime = DateTime::createFromFormat('H:i:s', $eventDetails->end_time);
                                ?>
                                <span><?php ?><?php echo $startTime->format('g:i A') ?>
                                    - <?php echo $endTime->format('g:i A') ?></span>
                            </div>
                        </div>
                        <div class="col-xs-2 event_view_reg_col">
                            <div class="event_view_reg_content">
                                <p>Cost</p>
                                <span><?php echo ($eventDetails->event_fee > 0) ? 'Tk ' . $eventDetails->event_fee : 'Free'; ?></span>
                            </div>
                        </div>
                        <div class="col-xs-3 event_view_reg_col">
                            <div class="event_view_reg_content1">
                                <p>Seat Limit</p>
                                <span><?php echo ($eventDetails->seat_limit > 0) ? $eventDetails->seat_limit : 'Unlimited'; ?></span>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="event_view_reg_row2 text-center">
                        <?php
                        $startDateTime = $eventDetails->start_date . ' ' . $eventDetails->start_time;
                        $endDateTime = $eventDetails->end_date . ' ' . $eventDetails->end_time;

                        $dates = date('Ymd', strtotime($startDateTime)) . 'T' . date('Hi00', strtotime('-6 hours', strtotime($startDateTime))) . 'Z/' . date('Ymd', strtotime($endDateTime)) . 'T' . date('Hi00', strtotime('-6 hours', strtotime($endDateTime))) . 'Z';

                        ?>

                        <!--<a class="google_calender_btn" href="http://www.google.com/calendar/event?action=TEMPLATE&text=<?php echo $eventDetails->event_name; ?>&dates=<?php echo $dates ?>&details=<?php echo $eventDetails->description ?>&location=<?php echo $eventDetails->address ?>&trp=false&sprop=&sprop=name:" target="_blank" rel="nofollow">+ Add to Google Calender</a> -->
                        <a href="<?php echo prep_url($eventDetails->registration_link); ?>" target="_blank" class="register_btn">Register</a>
                    </div>
                </div>
                <div class="time_count_box">
                    <ul class="countdown">
                        <li>
                            <span class="days">00</span>
                            <p class="days_ref">days</p>
                        </li>
                        <li>
                            <span class="hours">00</span>
                            <p class="hours_ref">hours</p>
                        </li>
                        <li>
                            <span class="minutes">00</span>
                            <p class="minutes_ref">minutes</p>
                        </li>
                        <li>
                            <span class="seconds">00</span>
                            <p class="seconds_ref">seconds</p>
                        </li>
                    </ul>
                </div>

                <div class="latest_reviews_box event_view_calender_relavent">
                    <h4>Other Events</h4>
                    <div id="event-calender-slider2">
                    <?php if (!empty($relEventData)): ?>
                        <?php foreach ($relEventData as $event): $images = explode('___', $event->images);
                            $image = $images[0]; ?>

                                <a href="<?php echo site_url() ?>event/eventView/<?php echo $event->id; ?>">
                                    <div class="thumbnail frame2" style="margin-bottom: 5px">
                                        <span class="helper222">
                                            <img style="" src="<?php echo base_url(); ?>upload/events/<?php echo $image; ?>" alt=""/>
                                        </span>
                                    </div>
                                    <div class="event_calender_txt">
                                        <p><?php echo $event->event_name; ?></p>
                                        <p>By <?php echo $event->title; ?>
                                            <br>
                                            START <span
                                                    class="event_dates"><?php echo date('d M, Y', strtotime($event->start_date)); ?></span>
                                            <br>
                                            END <span
                                                    class="event_dates"><?php echo date('d M, Y', strtotime($event->end_date)); ?></span>
                                        </p>
                                    </div>
                                </a>

                        <?php endforeach; ?>
                        </div>

                    <?php else: ?>
                        <p> No Relevant Event Found </p>
                    <?php endif; ?>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="clearfix"></div>


            <?php $fullUrl = base_url().$this->uri->uri_string(); ?>
            <?php $url= 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
            $actual_link=str_replace("/","--",$url);
             ?>

            <hr style="margin: 0.5em;">
            <div class="directory_profile_social_share">
                <i style="color: #012F49" class="fa fa-share-alt" aria-hidden="true"></i>
                <a class="facebook customer share" style="color: #5D7FBF"
                   href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode($fullUrl); ?>" title="Facebook share"
                   target="_blank"><i class="fa fa fa-facebook fa-lg" style="margin: 0 8px;" aria-hidden="true"></i></a>
                <a class="twitter customer share" style="color: #1b95e0"
                   href="http://twitter.com/share?url=<?php echo urlencode($fullUrl); ?>&amp;hashtags=youthopiabangla"
                   title="Twitter share" target="_blank"><i class="fa fa-twitter fa-lg" style="margin: 0 8px;"
                                                            aria-hidden="true"></i></a>
                <a class="google_plus customer share" style="color: #db4437"
                   href="https://plus.google.com/share?url=<?php echo urlencode($fullUrl); ?>" title="Google Plus Share"
                   target="_blank"><i class="fa fa-google-plus fa-lg" style="margin: 0 8px;" aria-hidden="true"></i></a>

                <a class="email modalLink" data-modal-size="modal-md"
                   style="color: #79B04A;"
                   href="<?php echo site_url().'event/report_problem/'.$eventDetails->id.'/'.$linkShare.'/'.$actual_link ?>"
                   title="Share"><i
                            class="fa fa-envelope fa-lg"
                            style="margin: 0 8px;" aria-hidden="true"></i></a>
            </div>

        </div>
    </section>
</div>

<!--<script src="https://apis.google.com/js/platform.js" async defer></script>-->
<!---->
<!--<script>-->
<!--    var optimizedDatabaseLoading = 0;-->
<!--    var _latitude = 23.6850;-->
<!--    var _longitude = 90.3563;-->
<!--    var element = "map-homepage";-->
<!--    var markerTarget = "modal"; // use "sidebar", "infobox" or "modal" - defines the action after click on marker-->
<!--    var sidebarResultTarget = "modal"; // use "sidebar", "modal" or "new_page" - defines the action after click on marker-->
<!--    var showMarkerLabels = false; // next to every marker will be a bubble with title-->
<!--    var mapDefaultZoom = 7; // default zoom-->
<!--    heroMap(_latitude, _longitude, element, markerTarget, sidebarResultTarget, showMarkerLabels, mapDefaultZoom);-->
<!--</script>-->


<script>
    $(document).ready(function () {
        $("#event_view_slider").owlCarousel({
            autoPlay: 3000,
            pagination: false,
            navigation: true,
            singleItem: true,
        });


        $('.countdown').downCount(
            {
                date: '<?php echo date('m/d/Y H:i:s', strtotime($eventDetails->start_date . ' ' . $eventDetails->start_time)) ?>',
                offset: +6
            }, function () {

            }
        );

        $('#interested_btn').on('click', function () {
            var user_id = $(this).data('user-id');
            var event_id = $(this).data('event-id');
            if (!user_id) {
                alert('Please login to perform this task');
                return false;
            }
            $.ajax({
                url: '../calSearch',
                type: 'POST',
                data: {
                    user_id: user_id,
                    event_id: event_id,
                    action: 'interest'
                }
            }).done(function (response) {
                var res = $.parseJSON(response);
                $('#interested_count').html(res.count);
                if (!res.ack) {
                    alert(res.msg);
                }
            });
        });

        $('#going_btn').on('click', function () {

            var user_id = $(this).data('user-id');
            var event_id = $(this).data('event-id');
            if (!user_id) {
                alert('Please login to perform this task');
                return false;
            }
            $.ajax({
                url: '../calSearch',
                type: 'POST',
                data: {
                    user_id: user_id,
                    event_id: event_id,
                    action: 'going'
                }
            }).done(function (response) {
                var res = $.parseJSON(response);
                $('#going_count').html(res.count);
                if (!res.ack) {
                    alert(res.msg);
                }
            });
        })
    });
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgGR-bhDxioHdiX8mtFYkNliRAY6CsLao&callback=initMap" async
        defer>
</script>
<script>
    function initMap() {
        // Styles a map in night mode.
        var latLong = {lat: <?php echo $eventDetails->latitude; ?>, lng: <?php echo $eventDetails->longitude; ?>};
        var map = new google.maps.Map(document.getElementById('google_map'), {
            center: latLong,
            zoom: 16,
            styles: [{"elementType": "geometry", "stylers": [{"color": "#f5f5f5"}]}, {
                "elementType": "labels.icon",
                "stylers": [{"visibility": "off"}]
            }, {
                "elementType": "labels.text.fill",
                "stylers": [{"color": "#616161"}]
            }, {
                "elementType": "labels.text.stroke",
                "stylers": [{"color": "#f5f5f5"}]
            }, {
                "featureType": "administrative.land_parcel",
                "elementType": "labels.text.fill",
                "stylers": [{"color": "#bdbdbd"}]
            }, {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [{"color": "#eeeeee"}]
            }, {
                "featureType": "poi",
                "elementType": "labels.text.fill",
                "stylers": [{"color": "#757575"}]
            }, {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [{"color": "#e5e5e5"}]
            }, {
                "featureType": "poi.park",
                "elementType": "labels.text.fill",
                "stylers": [{"color": "#9e9e9e"}]
            }, {
                "featureType": "road",
                "elementType": "geometry",
                "stylers": [{"color": "#ffffff"}]
            }, {
                "featureType": "road.arterial",
                "elementType": "labels.text.fill",
                "stylers": [{"color": "#757575"}]
            }, {
                "featureType": "road.highway",
                "elementType": "geometry",
                "stylers": [{"color": "#dadada"}]
            }, {
                "featureType": "road.highway",
                "elementType": "labels.text.fill",
                "stylers": [{"color": "#616161"}]
            }, {
                "featureType": "road.local",
                "elementType": "labels.text.fill",
                "stylers": [{"color": "#9e9e9e"}]
            }, {
                "featureType": "transit.line",
                "elementType": "geometry",
                "stylers": [{"color": "#e5e5e5"}]
            }, {
                "featureType": "transit.station",
                "elementType": "geometry",
                "stylers": [{"color": "#eeeeee"}]
            }, {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [{"color": "#c9c9c9"}]
            }, {"featureType": "water", "elementType": "labels.text.fill", "stylers": [{"color": "#9e9e9e"}]}]
        });

        var marker = new google.maps.Marker({
            position: latLong,
            map: map,
            icon: '<?php echo base_url(); ?>old/assets/img/marker.png'
        });
    }
</script>




<script>
    function initializeReadMore(){

        $.ajax({
            type: "GET",
            url: "<?php echo base_url();?>old/assets/js/readmore.min.js",
            success: readMoreCallBack,
            dataType: "script",
            cache: true
        });

        function readMoreCallBack(){
            var collapseHeight;
            var $readMore = $(".read-more");
            if( $readMore.attr("data-collapse-height") ){
                collapseHeight =  parseInt( $readMore.attr("data-collapse-height"), 10 );
            }else {
                collapseHeight = 55;
            }
            $readMore.readmore({
                speed: 500,
                collapsedHeight: collapseHeight,
                blockCSS: 'display: inline-block; width: auto; min-width: 120px;',
                moreLink: '<a href="#" class="btn btn-primary btn-xs btn-light-frame btn-framed btn-rounded">More<i class="icon_plus"></i></a>',
                lessLink: '<a href="#" class="btn btn-primary btn-xs btn-light-frame btn-framed btn-rounded">Less<i class="icon_minus-06"></i></a>'
            });
        }
    }


    initializeReadMore();
</script>

<script>
    $(document).ready(function () {
        $("#event-calender-slider2").owlCarousel({
            autoPlay: 3000,
            pagination: false,
            navigation: true,
            items : 3
        });
    });
</script>
<script>
    (function ($) {
        /**
         * jQuery function to prevent default anchor event and take the href * and the title to make a share popup
         *
         * @param  {[object]} e           [Mouse event]
         * @param  {[integer]} intWidth   [Popup width defalut 500]
         * @param  {[integer]} intHeight  [Popup height defalut 400]
         * @param  {[boolean]} blnResize  [Is popup resizeabel default true]
         */
        $.fn.customerPopup = function (e, intWidth, intHeight, blnResize) {
            // Prevent default anchor event
            e.preventDefault();
            // Set values for window
            intWidth = intWidth || '500';
            intHeight = intHeight || '400';
            strResize = (blnResize ? 'yes' : 'no');
            // Set title and open popup with focus on it
            var strTitle = ((typeof this.attr('title') !== 'undefined') ? this.attr('title') : 'Social Share'),
                strParam = 'width=' + intWidth + ',height=' + intHeight + ',resizable=' + strResize,
                objWindow = window.open(this.attr('href'), strTitle, strParam).focus();
        }
        /* ================================================== */
        $(document).ready(function ($) {
            $('.customer.share').on("click", function (e) {
                $(this).customerPopup(e);
            });
        });
    }(jQuery));
</script>



