

<!--  **************************** FB Imo Start *************************************************** -->
<script>
    $(document).ready(function () {

        $('.FB_reactions').facebookReactions4({
            postUrl: "<?php echo base_url() . 'InclusiveBd/fbImo';?>",
        });

        $('.FB_reactions').on('click', function () {

            var $this = $(this);
            var user_id = $this.attr('data-user-id');
            var control_id = $this.attr('data-unique-id');
            var value = $this.attr('data-emoji-id');
            var imo_track = 'imo_track';

//            alert(value);

            if (user_id.length === 0 || user_id == null) {
                alert("Please login to perform this task!");
            } else {
                $.ajax({
                    type: 'POST',
                    url: '<?php echo site_url() . '/opportunity/fbImo';?>',
                    data: {user_id: user_id, control_id: control_id, value: value, imo_track: imo_track}, // our data object
                    success: function (response) {

                        var res = $.parseJSON(response);
                        console.log(res);
//                    alert(res.un_chg);
                        if (res.un_chg == 1) {
                            $('.this_cls_' + res.inclusive_id).attr('data-emoji-id', 'like');
                        }
                        else if (res.un_chg == 2) {
                            $('.this_cls_' + res.inclusive_id).attr('data-emoji-id', '');
                        }


                        console.log(Object.keys(res).length);

                        $('#like-count' + res.inclusive_id).html(0);
                        $('#smiley-count' + res.inclusive_id).html(0);
                        $('#love-count' + res.inclusive_id).html(0);
                        $('#wow-count' + res.inclusive_id).html(0);

                        res.count.forEach(function (counts) {
                            if (counts.type == '1') {
                                $('#like-count' + res.inclusive_id).html(counts.interest_count);
                            }
                            else if (counts.type == '2') {
                                $('#smiley-count' + res.inclusive_id).html(counts.interest_count);
                            }
                            else if (counts.type == '3') {
                                $('#love-count' + res.inclusive_id).html(counts.interest_count);
                            }
                            else if (counts.type == '4') {
                                $('#wow-count' + res.inclusive_id).html(counts.interest_count);
                            }
                            else {
                            }
                        });
                        if (!res.ack) {
                            alert(res.msg);
                        }


                    },
                    error: function () {

                        // alert('<p>An error has occurred</p>');
                        console.log('An error has occurred');
                    }
                });

            }


        });
    });

</script>

<style>
    .c_btn {
        background-color:; /* Green */
        border: none;
        color: darkblue;
        padding: 04px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 13px;
        margin: 4px 2px;
        cursor: pointer;
        font-weight: 400;
    }
</style>


<!--  **************************** FB Imo End *************************************************** -->






<?php
// foreach($category as $cat)
// {
//     echo $cat->LOOKUP_DATA_NAME;
//     echo "<br>";
// }
// echo "<pre>";
// print_r($category);
// exit;
?>
<style media="screen">
    #easyPaginate, #easyPaginateBlog {
        width: 100%;
    }

    .easyPaginateNav {
        clear: both;
        margin-top: 20px;
        text-align: center;
    }

    .easyPaginateNav a {
        padding: 8px;
    }

    .easyPaginateNav a.current {
        font-weight: bold;
        text-decoration: underline;
        font-size: 24px;
    }

    #easyPaginate, #easyPaginateBlog {
        display: -webkit-flex; /* Safari */
        display: flex;
        flex-wrap: wrap;
    }
</style>
<style>
    .panelD .panel-heading, .panelD .panel-footer {
        background-color: #fff !important;
    }

    .panel-image img.panel-image-preview {
        width: 100%;
        border-radius: 4px 4px 0px 0px;
    }

    .panel-heading ~ .panel-image img.panel-image-preview {
        border-radius: 0px;
    }

    .panel-heading .list-inline {
        margin: 0px 0px 0px -5px !important;
    }

    .panel-body {
        display: block;
        padding: 2px 5px 0px;
    }

    .panel-body blockquote {
        margin: 5px 0 5px;
    }

    .panel-image ~ .panel-footer a {
        padding: 0px 10px;
        color: rgb(100, 100, 100);
    }

    .panel-image ~ .panel-footer span {
        color: rgb(100, 100, 100);
    }

    .panel-footer .list-inline {
        margin: 0 0 0 -15px !important;
    }

    .panel-image.hide-panel-body ~ .panel-body {
        height: 0px;
        padding: 0px;
    }

    /*==========  Mobile First Method  ==========*/
    /* Custom, iPhone Retina */
    @media only screen and (max-width: 320px) {
        .level-line-up {
            position: relative;
            top: -4px;
        }
    }
</style>

<style>
    .new_rz_box {
        display: block;
        margin-top: 40px;
        padding: 10px 0;
        border-bottom: 1px solid #ccc;
    }

    .new_rz_box h6 {
        color: #446678;
        text-transform: uppercase;
    }

    .new_rz_box h5 {
        color: #446678;
        text-transform: uppercase;
    }

    .new_rz_box h3 {
        color: #446678;
        text-transform: uppercase;
    }

    .new_rz_box h4 {
        color: #333;
    }

    .new_rz_box2 {
        display: block;
        padding: 10px 0;
    }

    .new_rz_box2 h3 {
        color: #446678;
        margin-bottom: 10px;
    }

    .new_rz_box2 input {
        border: 1px solid #d9d9d9;
        width: 100%;
        padding: 5px;
    }

    .new_rz_box2 textarea {
        border: 1px solid #d9d9d9;
        width: 100%;
        padding: 10px 20px;
    }

    .new_rz_box2 select {
        border: 1px solid #d9d9d9;
        width: 100%;
        padding: 5px 10px;
    }
.new_rz_box2 input {
        border: 1px solid #d9d9d9 !important;
        padding: 8px ! important;
    }
    .row > div {
        display: inline-block;

    }
    .form-group{
        display: block !important;
    }
    .rz_button3 {
        margin-top: 40px;
        border: 0;
        background-color: #012F49;
        color: #FFF;
        font-size: 25px;
        text-align: center;
        padding: 10px 20px;
        text-transform: uppercase;
        border-radius: 10px;
    }

    .rz_button3:hover {
        color: #FFF;
    }
    .btn.btn-default1 {
    background-color: #d43f3a;
    color: white;
    border-color: #d43f3a;
    border-radius: 25px;
}
</style>
<style type="text/css">
    .thumb {
        width: 50px;
        height: 50px;
    }

    #list {
        position: absolute;
        top: 0px;
    }

    #list div {
        float: left;
        margin-right: 10px;
    }
    .nav22>li>a{
        border: 4px solid transparent;
    }
    .nav22>li.active>a, .nav22>li.active>a:focus, .nav22>li.active>a:hover {
        cursor: default;
        border: 4px solid #a5a4a4;
        border-bottom-color: transparent;
        font-weight: bold;
        background: url("<?php echo base_url("portalAssets/img/Designed-button.png") ?>") no-repeat;
        width: 100% !important;
        background-size: cover;
        color: white;
    }
    .col-xs-12>img.img-responsive{
        width: 100%;
    }

    @media (max-width: 750px){
        .search-btn{
            margin-left: 7px;
            margin-top: 10px;
            padding: 8px 15px !important;
        }
    }

    .frame {
    height: 200px;
    display: table;
    width: 100%;
    text-align: center;
    padding: 0px;
}

.helper22 {

    display: table-cell;
    text-align: center;
    vertical-align: middle;
    height: 200px;

}
.frame > span > img {
    vertical-align: middle;
    display: inline-block !important;
    width: auto !important;
    max-height: 100%;
    max-width: 100%;
}


#myCSS {
    cursor: default;
    border: 4px solid #a5a4a4;
    border-bottom-color: transparent;
    font-weight: bold;
    background: url("<?php echo base_url(); ?>portalAssets/img/Designed-button.png") no-repeat;
    width: 100% !important;
    background-size: cover;
    color: white;
}
.nnav>li>a{
        border: 4px solid transparent;
    }

.nnav>li>a:hover, .nnav>li>a:focus, .nnav>li>a:active {
        cursor: default;
        border: 4px solid #a5a4a4;
        border-bottom-color: transparent;
        font-weight: bold;
        background: url("<?php echo base_url(); ?>portalAssets/img/Designed-button.png") no-repeat;
        width: 100% !important;
        background-size: cover;
        color: white;
}

@media (max-width: 650px){
   /* footer {
        bottom: -430px !important;
        position: absolute !important;
        width: 100% !important;
    }*/
}

</style>
<?php
$userInfo=$this->session->userdata('user_logged_user');
if(isset($userInfo['userId']))
{
  $permUserId=$userInfo['userId'];
}
else
{
  $permUserId=0;
}
$prm=$this->db->query("SELECT COUNT(*) PER FROM front_user_permission WHERE MODULE_ID=6 AND USER_ID=$permUserId")->row();

//  $eventPermission= getPermission(2,$permission);
?>



  <div class="row">
    <div class="col-xs-12 col-sm-5">
      <img src="<?php echo base_url(); ?>portalAssets/img/designbanner.png"
                                        class="img-responsive"/>
    </div>
     <div class="col-xs-12 col-sm-2">
     <h1 style="font-size:26px;color: red;" align="center">Inclusive Bangladesh</h1>

    </div>
    <div class="col-xs-12 col-sm-5">
      <img src="<?php echo base_url(); ?>portalAssets/img/designbanner.png"
                                         class="img-responsive"/>
    </div>
  </div>




<ul class="nav nav-justified nnav" id="">
 <li id="" style="text-align: center;"><a href="<?php echo site_url().'InclusiveBd/inclusiveBdAllList/1' ?>" >Youth Actions</a></li>

  <li style="text-align: center;"><a href="<?php echo site_url().'InclusiveBd/inclusiveBdAllList/2' ?>">Opportunities</a></li>

  <li style="text-align: center;"><a  href="<?php echo site_url().'InclusiveBd/inclusiveBdAllList/3' ?>">Area Based News</a></li>
</ul>



<div class="container">
<?php if($cate == 1) : ?>
    <?php $this->load->view('portal/inclusiveBd/youth_action_view'); ?>
<?php endif; ?>

<?php if($cate == 2) : ?>
    <?php $this->load->view('portal/inclusiveBd/opportunities_view'); ?>
<?php endif; ?>

<?php if($cate == 3) : ?>
<!--    --><?php //echo "Hello"; exit; ?>
    <?php $this->load->view('portal/inclusiveBd/area_based_view'); ?>
<?php endif; ?>
</div>

<?php if($cate == '') : ?>
    <div class="others_page_wrp">
        <section class="org_map_wrp">
            <div id="">
                <div class="col-lg-12 text-center">
                    <form action="<?php echo site_url() . 'inclusiveBd/searchInclusivePost' ?>" class="" method="post">

                        <div class="row new_rz_box2">
                            <div class="col-lg-12 text-center">
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                    <select class="form-control" name="LOOKUP_DATA_ID" id="LOOKUP_DATA_ID"
                                            data-tags="true" data-placeholder="Select Category" data-allow-clear="true">
                                        <option value="">Select Category</option>
                                        <?php foreach($category as $categorys) : ?>
                                            <option value="<?php echo $categorys->LOOKUP_DATA_ID; ?>" <?php echo ($cate == $categorys->LOOKUP_DATA_ID) ? 'selected' : '' ?> ><?php echo $categorys->LOOKUP_DATA_NAME; ?></option>
                                        <?php endforeach; ?>

                                    </select>
                                </div>

                                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                                    <select name="order">
                                        <option value="">Select order</option>
                                        <option value="desc" <?php echo ($or == 'desc') ? 'selected' : '' ?>>Newest to oldest</option>
                                        <option value="asc" <?php echo ($or == 'asc') ? 'selected' : '' ?>>Oldest to newest</option>
                                        <option value="today" <?php echo ($or == 'today') ? 'selected' : '' ?>>Today's post</option>
                                        <option value="week" <?php echo ($or == 'week') ? 'selected' : '' ?>>This week post</option>
                                    </select>
                                </div>

                                <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">
                                    <input list="search" type="text" class="" placeholder="Search for..."
                                           name="search"
                                           style="min-width: 120px;     border-radius: 6px; margin: 0 3px 0 7px;   padding: 5px;">

                                    <datalist id="search">
                                        <?php foreach ($inclusive_search_list as $i_list) : ?>
                                        <option value="<?php echo $i_list->title; ?>">
                                            <?php endforeach; ?>
                                    </datalist>

                                </div>


                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                    <input type="submit" class="btn btn-default btn-block" type="button" name="search_btn"
                                           value="Search">
                                    <!--  <input type="submit" class="btn btn-default btn-block  search-btn" type="button" name="search_btn" -->



                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">

                                    <?php
                                    if($permission->PER>0){
                                        ?>
                                        <div class="dropdown">
                                            <button class="btn btn-default1 btn-block dropdown-toggle" type="button" data-toggle="dropdown">Create
                                                <span class="caret"></span></button>
                                            <ul class="dropdown-menu">
                                                <li><a href="<?php echo site_url();?>inclusiveBd/createInclusivebd/1">YOUTH ACTION</a></li>
                                                <li><a href="<?php echo site_url();?>inclusiveBd/createInclusivebd/2">OPPORTUNITY</a></li>
                                                <li><a href="<?php echo site_url();?>inclusiveBd/createInclusivebd/3">NEWS</a></li>
                                            </ul>
                                        </div>

                                        <?php
                                    }

                                    ?>


                                    <!-- <a class="btn btn-default1 btn-block" type="button" href="<?php echo site_url();?>/InclusiveBd/createInclusivebd">Create</a> -->

                                </div>


                            </div>
                        </div>
                    </form>
                </div><br><br>


                <div class="container">
                    <h2></h2>
                    <div class="container-fluid" style="padding: 0px;">

                    <section class="slider">
                         <?php foreach($sliders as $slider){?>
                        <img width="100%" height="100%" src="<?php echo base_url('upload/inclusive_slider/'.$slider->IMAGE_PATH); ?>" alt="Bangladesh Tribal" >
                           <?php } ?>
                    </section>

                    </div>
                <!--     <div id="myCarousel" class="carousel slide" data-ride="carousel">

                        <ol class="carousel-indicators">
                            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel" data-slide-to="1"></li>
                            <li data-target="#myCarousel" data-slide-to="2"></li>
                            <li data-target="#myCarousel" data-slide-to="3"></li>
                            <li data-target="#myCarousel" data-slide-to="4"></li>
                            <li data-target="#myCarousel" data-slide-to="5"></li>
                            <li data-target="#myCarousel" data-slide-to="6"></li>
                        </ol>

                        <style type="text/css">
                            .item{
                                margin-bottom: 0px ! important;
                            }
                            @media (min-width: 1050px){
                                .carousel-inner{
                                    height: 400px;
                                }
                            }
                        </style>
                        <div class="carousel-inner" role="listbox"  >
                            <div class="item active">
                                <img src="<?php echo base_url('upload/inclusive_slider/'.$slider->IMAGE_PATH); ?>" alt="Bangladesh Tribal" style="width:100%;">
                                <div class="carousel-caption">
                                    <h3><a href="#"><?php echo $slider->IMAGE_TITLE?></a></h3>

                                </div>
                            </div>
                            <?php foreach($sliders as $slider){?>
                                <div class="item ">
                                    <img src="<?php echo base_url('upload/inclusive_slider/'.$slider->IMAGE_PATH); ?>" alt="Bangladesh Tribal" style="width:100%;">
                                    <div class="carousel-caption">
                                        <h3><?php echo $slider->IMAGE_TITLE?></h3>

                                    </div>
                                </div>
                            <?php } ?>

                        </div>




                        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div> -->
                </div>
            </div>
        </section>


    </div>


<?php endif; ?>



 <br>




 <script type="text/javascript">
$("#LOOKUP_DATA_ID option[id='1']").attr("selected", "selected");
</script>
 <script type="text/javascript">
        $(document).on('click','#hidden_slider',function(){
            //var status=$(this).val();
            //alert();

                $("#welcomeDiv").hide();

        });

    </script>
<script type="text/javascript">

function showDiv() {
   document.getElementById('welcomeDiv').style.display = "block";
}
</script>

<script>
    $(function ($) {
        /**
         * jQuery function to prevent default anchor event and take the href * and the title to make a share popup
         *
         * @param  {[object]} e           [Mouse event]
         * @param  {[integer]} intWidth   [Popup width defalut 500]
         * @param  {[integer]} intHeight  [Popup height defalut 400]
         * @param  {[boolean]} blnResize  [Is popup resizeabel default true]
         */
        $.fn.customerPopup = function (e, intWidth, intHeight, blnResize) {
            // Prevent default anchor event
            e.preventDefault();
            // Set values for window
            intWidth = intWidth || '500';
            intHeight = intHeight || '400';
            strResize = (blnResize ? 'yes' : 'no');
            // Set title and open popup with focus on it
            var strTitle = ((typeof this.attr('title') !== 'undefined') ? this.attr('title') : 'Social Share'),
                strParam = 'width=' + intWidth + ',height=' + intHeight + ',resizable=' + strResize,
                objWindow = window.open(this.attr('href'), strTitle, strParam).focus();
        }
        /* ================================================== */
        $(document).ready(function ($) {
            $('.customer.share').on("click", function (e) {
                $(this).customerPopup(e);
            });
        });
    }(jQuery));
</script>
