<?php echo form_open('auth/userLogin', "class='form-vertical'"); ?>
<div class="row">
  <div class=" inputField col-md-5">
    <input type="text" class="form-control" name="email" id="emailAddress" placeholder="Email Address">
  </div>
  <div class="inputField col-md-5">
    <input type="password" class="form-control" name="password" id="password" placeholder="password">
  </div>
  <div class="inputField col-md-2">
    <button type="submit" class="btn btn-primary btn-xs">Login</button>
  </div>
</div>
<?php
echo form_close();
?>
