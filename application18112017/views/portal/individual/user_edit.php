<style>
    .error {
        border-color: #EBCCD1;
        color: #B94A48;
    }
    .modal-header h4 {
    color: #681156;
    margin-top: 15px;
    font-size: 25px;
    margin-bottom: 8px;
    border-bottom: 1px solid #ddd !important;
    padding-bottom: 20px !important;
}

select.form-control2 {
    padding: 0px ! important;
    padding-left: 10px !important;
}

input[type=text] {
    width: 100% !important;
}
</style>
<script src="<?php echo base_url(); ?>assets/js/portal-js/bootstrap-datetimepicker.min.js"></script>
 <link href="<?php echo base_url(); ?>assets/vendors/fileuploader/src/jquery.fileuploader.css" rel="stylesheet">
 <script src="<?php echo base_url(); ?>assets/vendors/fileuploader/src/jquery.fileuploader.min.js"></script>
<div class="row">
   <!--  <?php echo form_open("portal/update_visitor_profile_data/" . $user_detail->userId); ?> -->

    <?php
$attribute = array('class' => 'form-horizontal', 'id' => '', 'role' => 'form', 'enctype' => 'multipart/form-data');
echo form_open('portal/update_visitor_profile_data/'.$user_detail->userId, $attribute);
?>
    <div class="msg">
        <?php
        if (validation_errors() != false) {
            ?>
            <div class="alert alert-danger">
                <button data-dismiss="alert" class="close" type="button">×</button>
                <?php echo validation_errors(); ?>
            </div>
            <?php
        }
        ?>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="fname">Name</label>
        <div class="col-sm-8">
            <div class="fg-line">
                <input type="text" name="fname" required="required" class="form-control" id="fname"
                value="<?php echo $user_detail->fname; ?>" placeholder="Enter  Name"/>
                 <input type="hidden" name="redirectUrl" value="<?php echo $url; ?>">
            </div>
        </div>
        <br clear="all"/>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="date">Date</label>

        <div class="col-sm-8">
            <div class="fg-line">

                <input class="form-control date_picker" value="<?php echo date('d-m-Y', strtotime($user_detail->date)); ?>" type="text" id="date" name="date"
                placeholder="DOB (YY-MM-DD)"  required="required"/>
                
            </div>
        </div>
        <br clear="all"/>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="id">Gender</label>
        <div class="col-sm-4">
          <select class="form-control form-control2" name="gender" required="required">
           <option value="">Select gender</option>
           <option value="Male"<?php echo ($user_detail->gender == 'Male') ? 'selected' : '' ?>>Male</option>
           <option value="Female"<?php echo ($user_detail->gender == 'Female') ? 'selected' : '' ?>>Female</option>
           <option value="Others"<?php echo ($user_detail->gender == 'Others') ? 'selected' : '' ?>>Others</option>
       </select>
   </div>
   <br clear="all"/>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label" for="SL_NO">Contact Number</label>

    <div class="col-sm-8">
        <div class="fg-line">
            <input type="text" name="number" id="number" class="form-control onlyNumber"
            value="<?php echo $user_detail->number; ?>" placeholder="Enter Number" required="required"/>
        </div>
    </div>
    <br clear="all"/>
</div>
<div class="form-group">
<label class="col-sm-3 control-label" for="SL_NO"></label>
<div class="col-sm-8">
<div class="fg-line">
 <span id="blk-image" class="toHide"   padding: 0px; margin: 0px;">
    <?php if (!empty($user_detail->image)) { ?>
    <div class="row new_rz_box2">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-sm-3">
                <div>
                    <img src="<?php echo base_url(); ?><?php echo $user_detail->image ?>" alt="Profile img"
                    width="100" height="100">
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
</span>
  </div>
</div>
<br clear="all"/>
</div>
<div class="form-group">
<label class="col-sm-3 control-label" for="SL_NO">Image</label>
    <div class="col-sm-8">
        <div class="fg-line">
         <input type="file" name="main_image" id="" value="<?php echo $user_detail->image; ?>" placeholder=""/>
  </div>
</div>
<br clear="all"/>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label" for="SL_NO">Email</label>

    <div class="col-sm-8">
        <div class="fg-line">
        <input type="hidden" name="email" value="<?php echo $user_detail->email; ?>"> 
        <input type="email" name="email" id="email" class="form-control"
        value="<?php echo $user_detail->email; ?>" disabled  placeholder="Enter Email"  required="required"/>
        </div>
    </div>
    <br clear="all"/>
</div>

<button class="col-sm-offset-3 btn btn-primary btn-sm " type="submit">Submit</button>
<?php echo form_close(); ?>
</div>
<script type="text/javascript">
 $(document).ready(function () {
   $('.date_picker').datepicker({
     format: "dd-mm-yyyy"
 });
});

</script>
 <script src="<?php echo base_url(); ?>assets/js/portal-js/jquery.alphanum.js"></script>
 <script type="text/javascript">
 $(document).ready(function () {
   $('.onlyNumber').numeric();
 
 });
 </script>
 <script type="text/javascript">
  $('input[name="main_image"]').fileuploader({
            // Options will go here
        });
    </script>