<link rel="stylesheet" href="<?php echo base_url('portalAssets/homePage') ?>/css/main.css">
    <script src="<?php echo base_url(); ?>assets/js/portal-js/bootstrap-datetimepicker.min.js"></script>
<?php
$this->load->library('facebook');
$googleUrl=$this->google->loginURL();
$authUrl=$this->facebook->login_url();
?>
<style media="screen">
.modal .modal-header, .modal .modal-body{
  text-align:center;
  }
.modal-dialog{
    width: 400px !important;
    margin-left: 60%;
  }
  .modal-dialog h4{
    border-bottom: 1px solid #ddd;
    padding-bottom: 20px;
  }
  .social{
    margin-top: 0px;
  }
.modal-header {
    padding-bottom: 5px !important;
  }
  .social img {
      height: 35px;
  }
  .form-control{
    height: 30px;
  }
  .form-group>label{
    margin:0px;
  }

  .fileuploader {
    padding: 0px !important;
    background: transparent !important;
    margin:7px 0px !important;
  }
  .modal-footer {
      padding: 0px !important;
  }
  .fileuploader-input-caption {
    padding: 7px 10px !important;
  }
  .fileuploader-input-button {
      padding: 7px 20px !important;
  }

  .fileuploader-item {
    padding: 15px 15px  !important;
  }

  .social>p{
    margin-bottom: 0px !important;
  }
  .form-group {
      margin-bottom: 10px;
  }
input[type=text] {
    width: 100% ! important;
}
</style>



<div class="social" style="position:relative;overflow:hidden">
  <a href="<?php echo $authUrl; ?>"><img src="<?php echo base_url('portalAssets/homePage') ?>/img/facebook.jpg">
  </a>
  <div class="hr"></div>
  <a href="<?php echo $googleUrl; ?>"><img src="<?php echo base_url('portalAssets/homePage') ?>/img/g-plus.jpg"></a>
   <p><b>OR</b><p>

</div>

<form action="<?php echo site_url('portal/individualRegis') ?>" method="post" class="formSubmit" enctype='multipart/form-data' novalidate  autocomplete="off">
<div class="firstBlock">
  <div class="col-md-12">
  <div class="form-group">
    <label for="exampleInputEmail1">FULL NAME <span>*</span></label>
    <input type="email"  class="form-control" name="fname" id="fullName" required >
    <input type="hidden" name="redirectUrl" value="<?php echo $url; ?>">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">MOBILE NUMBER <span>*</span></label>
    <input type="number" class="form-control onlyNumber" value="" name="number" id="mobileNumber" required >
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">GENDER </label>
    <select class="form-control select-gen" style="padding:0px; border-radius: 0px;" name="gender" required="required">
      <option value="Male">Male</option>
      <option value="Female">Female</option>
      <option value="Others">Others</option>
    </select>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">PHOTO <span>*</span></label>
    <input type="file" name="files" class="form-control " id="photo" required >
  </div>


  <div class="form-group">
    <label for="start_date">DATE OF BIRTH<span>*</span></label>
     <input type="text" class="form-control dob" name="start_date" id="datepicker" placeholder="Please Select Date Of Birth"/>

  </div>
</div>
  <div class="row">
    <center>
      <button type="button" class="btn btn-warning   nextBtn" id="signUp">NEXT PAGE</button>
    </center>
  </div>

</div>
<div class="secondBlock" style="display:none">

  <div class="form-group">
    <label for="exampleInputEmail1">EMAIL ID <span>*</span></label>
    <input type="email" name="email" class="form-control emailCheck" id="emailId" required >
    <div class="checkUsermail"></div>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">PASSWORD <span>*</span></label>
    <input type="password" class="form-control minLength passwordInd password" name="password" id="password" placeholder="Password" required>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">REPEAT PASSWORD <span>*</span></label>
    <input type="password" class="form-control password_conf " name="password_conf" id="password_conf" placeholder="Confirm password" required>
  </div>

  <label for="checkbox"> <input type="checkbox" name="terms" value="YES" class="checkBox">  I ACCEPT <a href="<?php echo base_url();?>upload/Terms_and_Conditions.pdf" target="_blank"><span>TERMS &amp; CONDITIONS</span></a> AND <a href="<?php echo base_url();?>upload/Privacy_Policy.pdf" target="_blank"><span>PRIVACY POLICY</span></a></label>
  <button type="button" class="btn btn-warning pre-btn prevButton"  >PREVIOUS PAGE</button>
  <button type="submit" class="btn btn-default pre-btn sub-btn submitButton">REQUEST FOR REGISTRATION</button>
</div>
  <?php echo form_close(); ?>
<script type="text/javascript">
function isValidEmailAddress(emailAddress) {
  var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
  return pattern.test(emailAddress);
};
$(document).ready(function(){
  $("button.nextBtn").click(function(){
    var fullName=$("input#fullName").val();
    var mobileNumber=$("input#mobileNumber").val()
    var emailId=$("input#emailId").val()
     var photo=$("input#photo").val()
    var dateOfBirth=$("input.dob").val()

    if(fullName=='')
    {
      swal({
            text: "Please Enter Full Name",
          });
    }
    else if(mobileNumber=='')
    {
      swal({
            text: "Please Enter Mobile Number",
          });
    }
     else if(photo=='')
    {
      swal({
            text: "Please Enter Photo",
          });
    }

    else if(dateOfBirth=='')
    {
      swal({
            text: "Please Enter Date Of Birth",
          });
    }
    else
    {
      $("div.secondBlock").show('slide');
      $("div.firstBlock").hide();
    }

  });
  $("button.prevButton").click(function(){
    $("div.firstBlock").show('slide');
    $("div.secondBlock").hide();
  });
});
</script>
<script type="text/javascript">
$(document).on("blur", ".emailCheck", function() {
  var email = $(this).val();
  //alert("okay");
  if(email!='')
  {
    var url = '<?php echo site_url('portal/checkUserEmail') ?>';
    $.ajax({
      type: "POST",
      url: url,
      dataType : 'html',
      data: {email: email},
      success: function(data1) {
        //console.log(data1);
        if(data1 == 1){
          $(this).val('');
          $('.checkUsermail').html("<span class='emailExist' style='color:red'>This Email ID Already Registered</span>");
          $("input.emailCheck").val("");
        }else{
          $('.checkUsermail').html('');
        }

      }
    });
  }
  else
  {
    $("span.emailExist").remove();
  }
});
$(function () {
  $("#password_conf").on('blur', function(){
    var password = $(".passwordInd").val();
    var confirmPassword = $(".password_conf").val();
    if (password != confirmPassword) {
      swal({
            text: "These passwords miss match.",
          });
      $(".password").val('');
      $(".password_conf").val('');
      return false;
    }
    return true;
  });
});

$('.formSubmit').on('submit', function (e) {
  var emailId=$("input#emailId").val();
  var validEmail=isValidEmailAddress(emailId);
  var password=$("input.password").val();
  var passwordConf=$("input.password_conf").val();
//  var checkBox=$("input.checkBox").val();

  if($('input.checkBox').prop('checked'))
  {
    var checkBox=1;
  }
  else
  {
    var checkBox=0;
  }
  //alert(checkBox);
 if(emailId=='' || validEmail==false)
  {
    swal({
          text: "Please Enter Valid Email Id",
        });
    e.preventDefault();
  }
  else if(password=='')
  {
    swal({
          text: "Please Enter Password",
        });
    e.preventDefault();
  }
  else if(passwordConf=='')
  {
    swal({
          text: "Please Enter Confirm Password",
        });

    e.preventDefault();
  }
  else if(checkBox==0)
  {
    swal({
          text: "Please Check Disclemer",
        });
    e.preventDefault();
  }
  else
  {
//  e.preventDefault();
  }

});
</script>
  <script type="text/javascript">
         $(document).ready(function () {
           $('.date_picker').datepicker({
             format: "yyyy-mm-dd"
           });
         });
         $("input[name='test']").click(function () {
           $('#show-individual').css('display', ($(this).val() === 'a') ? 'block':'none');
         });
         $("input[name='test']").click(function () {
           $('#show-org').css('display', ($(this).val() === 'b') ? 'block':'none');
           var value=$(this).val();
           if(value=='b')
           {
           //  // $('#show-map').initMap();
         //initMap();

           }
         });
      </script>
<script type="text/javascript">
  $('input[name="files"]').fileuploader({
            // Options will go here
  });
</script>
  <script src="<?php echo base_url(); ?>assets/js/portal-js/jquery.alphanum.js"></script>
 <script type="text/javascript">
 $(document).ready(function () {
   $('.onlyNumber').numeric();

 });
 </script>


    <script>
        $(function () {
            $("#datepicker").datepicker({format: 'dd-mm-yyyy'});
            $("#datepicker2").datepicker({format: 'dd-mm-yyyy'});
        });
    </script>
