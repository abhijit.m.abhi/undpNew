<?php
$this->load->library('facebook');
$this->load->library('google');
$googleUrl=$this->google->loginURL();
$authUrl=$this->facebook->login_url();
?>
<link rel="stylesheet" href="<?php echo base_url('portalAssets/homePage') ?>/css/main.css">

<style media="screen">
.modal .modal-header, .modal .modal-body{
  text-align:center;
  }
.modal-dialog{
    width: 400px !important;
    margin-left: 60%;
  }
  .modal-dialog h4{
    border-bottom: 1px solid #ddd;
    padding-bottom: 20px;
  }
  .social{
    margin-top: 0px;
  }
.modal-header {
    padding-bottom: 5px !important;
  }
  
  .social img {
      height: 35px;
  }
</style>
<div class="social">
    <a href="<?php echo $authUrl; ?>"><img src="<?php echo base_url('portalAssets/homePage') ?>/img/facebook.jpg"></a>
    <div class="hr"></div>
    <a href="<?php echo $googleUrl; ?>"><img src="<?php echo base_url('portalAssets/homePage') ?>/img/g-plus.jpg"></a>
    
    <p><b>OR</b><p>

</div>


    <?php echo form_open('auth/userLogin', "class='form-vertical'"); ?>
      <div class="form-group">
          <label for="exampleInputEmail1">Email </label>
          <input type="email" name="email" class="form-control" id="exampleInputEmail1" required>
          <input type="hidden" name="redirectUrl" value="<?php echo $url; ?>">
      </div>
      <div class="form-group">
          <label for="exampleInputPassword1">Password</label>
          <input type="password" name="password" class="form-control" id="exampleInputPassword1" required>
      </div>
      <button type="submit" class="btn btn-primary login-btn" id="signin">LOGIN</button>
      <a href="<?php echo site_url();?>/forgetPassword/forgot_password" class="forget-pass">Forget Password?</a>

<?php echo form_close(); ?>
