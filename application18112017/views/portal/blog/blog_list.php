<style>
    .event_calender_slider {
        margin-bottom: 60px;
    }

    .rz_button {
        background-color: #012F49;
        color: #FFF;
        text-align: center;
        display: block;
        margin-top: 10px;
        border-radius: 3px;
    }

    .rz_button2 {
        background-color: #e63433;
        color: #FFF;
        font-size: 15px;
        text-align: center;
        padding: 6px 12px;
        text-transform: uppercase;
        border-radius: 25px;
    }

    .rz_button2:hover {
        color: #FFF;
    }

    .event_calender_slider:after {
        color: #FFFFFF;
        content: "PASSESD EVENTS";
        font-family: 'ralewaysemibold';
        font-size: 15px;
        left: 0;
        line-height: 1.3;
        position: absolute;
        text-align: center;
        top: 35%;
        width: 106px;
    }

    .frame{
        height: 145px;
        display: table;
        width: 100%;
        text-align: center;
        padding: 0px;
    }
    .helper{
        display: table-cell;
        text-align: center;
        vertical-align: middle;
        height: 145px;
    }
    .frame>span>img {
        vertical-align: middle;
        display: inline-block !important;
        width: auto !important;
        max-height: 100%;
        max-width: 100%;
    }
    .new_rz_box2 input {
        border: 1px solid #d9d9d9 !important;
    }
</style>
<?php
$userInfo=$this->session->userdata('user_logged_user');
if(isset($userInfo['userId']))
{
  $permUserId=$userInfo['userId'];
}
else
{
  $permUserId=0;
}
$prm=$this->db->query("SELECT COUNT(*) PER FROM front_user_permission WHERE MODULE_ID=3 AND USER_ID=$permUserId")->row();

//  $eventPermission= getPermission(2,$permission);
?>
<div class="others_page_wrp">
    <section class="org_map_wrp">
        <div class="container">
            <br/>
            <div class="col-md-12">
                <h2 class="pull-left" style="color:#012F49;">MY BLOG</h2>
                <?php
                if($prm->PER>0)
                {
                  ?>
                <a class="rz_button2 pull-right" href="<?php echo site_url();?>blog/createBlog">Create New Media Blog</a>
                <?php
              }
                ?>
            </div>
            <br/> <br/>
            <hr/>

            <?php if(!empty($blogs)) : ?>
            <div class="row">

                <?php
                foreach ($blogs as $blog) {
                    $imgName = explode('___', $blog->images);
                    $imgName = $imgName[0];
                    $postTitle = $blog->title;
                    if (strlen($postTitle) > 80) {
                        // truncate string
                        $postTitle = substr($postTitle, 0, 80);
                        // make sure it ends in a word so assassinate doesn't become ass...
                        $postTitle = substr($postTitle, 0, strrpos($postTitle, ' ')) . '....';
                    }
                    ?>
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <a href="<?php echo site_url().'blog/blogView/'.$blog->id; ?>" target="_blank">
                            <!--                                <a href="#" data-modal-external-file="event_view.php?event_id=-->
                            <?php //echo $event['id'];
                            ?><!--" data-target="modal-submit">-->
                            <div class="panel panelD" style="border: 1px solid #ddd">
                                <div class="thumbnail panel-image embed-responsive embed-responsive-16by9 " style="margin-bottom: 2px">
                                    <?php if ($blog->type == 'image') { ?>
                                        <div class="frame">
                                        <span class="helper"><img style=""
                                             src="<?php echo base_url().'upload/blogs/' . $imgName ?>"
                                             alt="<?php echo $blog->title; ?>"/></span></div>
                                    <?php } else {
                                        $url_link = '';
                                        if ($blog->url_type == 'Youtube') {
                                            $url2Embed = str_replace("watch?v=", "embed/", $blog->urls);
                                            $url_link = str_replace("[URL]", $url2Embed, $blog->url_data);
                                        } elseif ($blog->url_type == 'SoundCloud') {
                                            $url_link = str_replace("[URL]", $blog->urls, $blog->url_data);
                                        } elseif ($blog->url_type == 'DailyMotion') {
                                            $url2Embed = str_replace("video", "embed/video", $blog->urls);
                                            $url_link = str_replace("[URL]", $url2Embed, $blog->url_data);
                                        } elseif ($blog->url_type == 'Vimeo') {
                                            $url2Embed = str_replace("vimeo.com", "player.vimeo.com/video", $blog->urls);
                                            $url_link = str_replace("[URL]", $url2Embed, $blog->url_data);
                                        } else {
                                            echo '<div width="100%" height="258" style="margin-bottom: 0px;"><h1>Something Missing</h1></div>';
                                        }
                                        echo $url_link;
                                    } ?>
                                </div>
                                <div class="opportunity_calender_txt">
                                    <p style="min-height: 50px; max-height: 50px; overflow-y: hidden"><?php echo $postTitle; ?></p>
                                    <p>By
                                        <span style="min-height: 20px; max-height: 20px; overflow-y: hidden"><?php echo $blog->org_name; ?> </span><br/><span
                                            class="event_dates"> <?php echo date('d M, Y', strtotime($blog->post_date)); ?></span>
                                    </p>
                                </div>

                                <p><a class="rz_button" href="<?php echo site_url().'blog/editBlogPost/'.$blog->id; ?>">Edit</a>
                                </p>
                                <p><a class="rz_button" onClick="return confirm('Are you sure you want to delete?')"
                                      href="<?php echo site_url().'blog/deleteBlogPost/'.$blog->id; ?>">Delete</a>
                                </p>
                            </div>
                        </a>
                    </div>
                    <?php
                }
                ?>
            </div>

            <?php else: ?>
                <p class="alert alert-warning">No Blog post to show!</p>
            <?php endif; ?>

        </div>
    </section>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        $("#event-calender-slider").owlCarousel({
            autoPlay: 3000,
            pagination: false,
            navigation: true,
            itemsCustom: [[0, 1], [479, 2], [768, 4], [991, 5], [1199, 5]],
        });

        $(function () {
            window.prettyPrint && prettyPrint();

        });
    });
</script>
<script>
    $(document).ready(function () {
        $("#event-calender-slider2").owlCarousel({
            autoPlay: 3000,
            pagination: false,
            navigation: true,
            itemsCustom: [[0, 1], [479, 2], [768, 4], [991, 5], [1199, 5]],
        });

    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.date_picker').datepicker({
            format: "yyyy-mm-dd"
        });
    });
</script>
