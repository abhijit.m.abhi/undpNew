<!--  **************************** FB Imo Start *************************************************** -->
<script>
    $(document).ready(function () {
        $('.FB_reactions').facebookReactions({
            postUrl: "<?php echo base_url() . 'blog/fbImo';?>"
        });

        $('.FB_reactions').on('click', function () {

            var $this = $(this);
            var user_id = $this.attr('data-user-id');
            var control_id = $this.attr('data-unique-id');
            var value = $this.attr('data-emoji-id');
            var imo_track = 'imo_track';

//            alert(value);

            if (user_id.length === 0 || user_id == null) {
                alert("Please login to perform this task!");
            } else {
                $.ajax({
                    type: 'POST',
                    url: '<?php echo site_url() . '/blog/fbImo';?>',
                    data: {user_id: user_id, control_id: control_id, value: value, imo_track: imo_track}, // our data object
                    success: function (response) {

                        var res = $.parseJSON(response);
                        console.log(res);
//                    alert(res.un_chg);
                        if (res.un_chg == 1) {
                            $('.this_cls_' + res.blog_id).attr('data-emoji-id', 'like');
                        }
                        else if (res.un_chg == 2) {
                            $('.this_cls_' + res.blog_id).attr('data-emoji-id', '');
                        }


                        console.log(Object.keys(res).length);

                        $('#like-count' + res.blog_id).html(0);
                        $('#smiley-count' + res.blog_id).html(0);
                        $('#love-count' + res.blog_id).html(0);
                        $('#wow-count' + res.blog_id).html(0);

                        res.count.forEach(function (counts) {
                            if (counts.type == '1') {
                                $('#like-count' + res.blog_id).html(counts.interest_count);
                            }
                            else if (counts.type == '2') {
                                $('#smiley-count' + res.blog_id).html(counts.interest_count);
                            }
                            else if (counts.type == '3') {
                                $('#love-count' + res.blog_id).html(counts.interest_count);
                            }
                            else if (counts.type == '4') {
                                $('#wow-count' + res.blog_id).html(counts.interest_count);
                            }
                            else {
                            }
                        });
                        if (!res.ack) {
                            alert(res.msg);
                        }


                    },
                    error: function () {

                        // alert('<p>An error has occurred</p>');
                        console.log('An error has occurred');
                    }
                });

            }


        });
    });

</script>

<style>
    .c_btn {
        background-color:; /* Green */
        border: none;
        color: darkblue;
        padding: 04px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 13px;
        margin: 4px 2px;
        cursor: pointer;
        font-weight: 400;
    }

    .frame2 {      
    height: 390px;
    display: table;
    width: 100%;
    text-align: center;
    padding: 0px; 
}

.helper222 {
    display: table-cell;
    text-align: center;
    vertical-align: middle;
    height: 390px;
}

.frame2>span>img {
    vertical-align: middle;
    display: inline-block !important;
    width: auto  !important;
    max-height: 100%;
    max-width: 100%;
}

.new_rz_box2 input {
        border: 1px solid #d9d9d9 !important;
    }
</style>


<!--  **************************** FB Imo End *************************************************** -->

<style>
    footer {
        background-color: #F8F8F8 !important;
    }

    /** ====================
     * Lista de Comentarios
     =======================*/
    .comment {
        overflow: hidden;
        padding: 0 0 1em;
        border-bottom: 1px solid #ddd;
        margin: 0 0 1em;
        *zoom: 1;
    }

    .comment-img {
        float: left;
        margin-right: 33px;
        border-radius: 5px;
        overflow: hidden;
    }

    .comment-img img {
        display: block;
    }

    .comment-body {
        overflow: hidden;
    }

    .comment .text {
    }

    .comment .text p:last-child {
        margin: 0;
    }

    .comment .attribution {
        margin: 0.5em 0 0;
        font-size: 14px;
        color: #666;
    }

    /* Decoration */

    .blog-comments,
    .comment {
        position: relative;
    }

    .blog-comments:before,
    .comment:before,
    .comment .text:before {
        content: "";
        position: absolute;
        top: 0;
        left: 65px;
    }

    .blog-comments:before {
        width: 3px;
        top: 0px;
        bottom: 0px;
        background: rgba(0, 0, 0, 0.1);
    }

    .comment:before {
        width: 9px;
        height: 9px;
        border: 3px solid #fff;
        border-radius: 100px;
        margin: 16px 0 0 -6px;
        box-shadow: 0 1px 1px rgba(0, 0, 0, 0.2), inset 0 1px 1px rgba(0, 0, 0, 0.1);
        background: #ccc;
    }

    .comment:hover:before {
        background: orange;
    }

    .comment .text:before {
        top: 18px;
        left: 78px;
        width: 9px;
        height: 9px;
        border-width: 0 0 1px 1px;
        border-style: solid;
        border-color: #e5e5e5;
        background: #fff;
        -webkit-transform: rotate(45deg);
        -moz-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        -o-transform: rotate(45deg);
    }

    ​
</style>
<style media="screen">
    .easyPaginateNav {
        clear: both;
        margin-top: 20px;
        text-align: center;
    }

    .easyPaginateNav a {
        padding: 8px;
    }

    .easyPaginateNav a.current {
        font-weight: bold;
        text-decoration: underline;
        font-size: 24px;
    }

</style>


<div id="fb-root"></div>
<div class="others_page_wrp">
    <section class="calender_event_view_wrp">
        <div class="container">
            <div class="calender_event_view_top">

                <div class="event_view_top_left">
                    <span class="view_avatar"><img
                                src="<?php echo (!empty($organizationLogo)) ? base_url() . $organizationLogo->image : ''; ?>"
                                alt="" style="width: 70px;"/></span>
                    <div class="event_view_top_left_title">
                        <h2 style="margin: 12px 0px 0px 0px; color: #012F49;"><?php echo $organizationDetails->title; ?></h2>
                        <span><?php echo $organizationDetails->location; ?></span>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <?php
                $id = '';
                $likeReaction = 0;
                $smileReaction = 0;
                $loveReaction = 0;
                $wowReaction = 0;
                $action = '';
                $action_name = '';
                $images = explode('___', $blogDetails->images);

                if (isset($this->session->userdata['user_logged_user']['userId'])) {
                    $id = $this->session->userdata['user_logged_user']['userId'];
                } else {
                    $id = '';
                }

                $interests = $this->blog_model->getBlogInterest2($blogDetails->id, $id);
                $inter = $this->blog_model->getBlogInterest($blogDetails->id);
                ?>

                <?php foreach ($interests as $interest) { ?>
                    <?php
                    if ($interest->type == '1') {
                        $likeReaction = $interest->interest_count;
                        $action = 'like';
                        $action_name = 'LIKE';
                    } elseif ($interest->type == '2') {
                        $smileReaction = $interest->interest_count;
                        $action = 'haha';
                        $action_name = 'HAHA';
                    } elseif ($interest->type == '3') {
                        $loveReaction = $interest->interest_count;
                        $action = 'love';
                        $action_name = 'LOVE';
                    } elseif ($interest->type == '4') {
                        $wowReaction = $interest->interest_count;
                        $action = 'wow';
                        $action_name = 'WOW';
                    }
                    ?>

                <?php } ?>

                <?php foreach ($inter as $intere) : ?>
                    <?php
                    if ($intere->type == '1') {
                        $likeReaction = $intere->interest_count;
                    } elseif ($intere->type == '2') {
                        $smileReaction = $intere->interest_count;
                    } elseif ($intere->type == '3') {
                        $loveReaction = $intere->interest_count;
                    } elseif ($intere->type == '4') {
                        $wowReaction = $intere->interest_count;
                    }
                    ?>
                <?php endforeach; ?>

                <div class="calender_event_view_slider">
                    <?php if ($blogDetails->type == 'image') { ?>
                        <div id="event_view_slider">
                            <?php foreach ($images as $image): ?>
                                <div class="slideritem thumbnail frame2" style="margin-bottom: 5px">
                                <span class="helper222"><img
                                            style=""
                                            src="<?php echo base_url(); ?>upload/blogs/<?php echo $image ?>"
                                            alt=""/></span></div>
                            <?php endforeach; ?>
                        </div>
                    <?php } else {
                        $url_link = '';
                        if ($blogDetails->url_type == 'Youtube') {
                            $url2Embed = str_replace("watch?v=", "embed/", $blogDetails->urls);
                            $url_link = str_replace("[URL]", $url2Embed, $blogDetails->url_data);
                            $url_link = str_replace('height="250"', '', $url_link);
                        } elseif ($blogDetails->url_type == 'SoundCloud') {
                            $url_link = str_replace("[URL]", $blogDetails->urls, $blogDetails->url_data);
                            $url_link = str_replace('height="258"', '', $url_link);
                        } elseif ($blogDetails->url_type == 'DailyMotion') {
                            $url2Embed = str_replace("video", "embed/video", $blogDetails->urls);
                            $url_link = str_replace("[URL]", $url2Embed, $blogDetails->url_data);
                            $url_link = str_replace('height="250"', '', $url_link);
                        } elseif ($blogDetails->url_type == 'Vimeo') {
                            $url2Embed = str_replace("vimeo.com", "player.vimeo.com/video", $blogDetails->urls);
                            $url_link = str_replace("[URL]", $url2Embed, $blogDetails->url_data);
                            $url_link = str_replace('height="250"', '', $url_link);
                        } else {
                            echo '<div width="100%" style="margin-bottom: 0px;"><h1>Something Missing</h1></div>';
                        }

                        echo '<div class="embed-responsive embed-responsive-16by9">' . $url_link . '</div>';
                    }
                    ?>

                    <div class="title_of_event "><?php echo $blogDetails->title ?></div>
                    <span class="pull-left" style="margin-top: 5px;">
                <i class="fa fa-calendar" aria-hidden="true"></i>
                    <b>Posted On: </b><?php echo date("M d Y", strtotime($blogDetails->post_date)) ?>
                </span>

                    <div class="event_view_share_group share_box_wrp">


                        <div align="center">

                            <div id="" style="padding-left: 5px; padding-top: 5px;">
                                <input type="hidden" class="c_id" name="c_id" value="<?php echo $blogDetails->id; ?>">
                                <div style="" align="left">

                                    <span id="Fb_Imo"><a class="FB_reactions this_cls_<?php echo $blogDetails->id; ?>"
                                                         data-reactions-type='horizontal'
                                                         data-emoji-id="<?php echo (!empty($action)) ? $action : ''; ?>"
                                                         data-user-id="<?php echo $id; ?>"
                                                         data-unique-id="<?php echo $blogDetails->id; ?>"
                                                         data-emoji-class="<?php echo (!empty($action)) ? $action : ''; ?>">
                                        <span style=""><?php echo (!empty($action_name)) ? $action_name : 'LIKE'; ?></span>
                                    </a></span>

                                    <span class="my_buttons" style="padding: 7px; font-size: 14px;">
                                        <span><button class="c_btn" style="border-radius: 12px" href=""> <img
                                                        style="height: 20px; width: auto;"
                                                        src="<?php echo base_url() . 'fb_imo/emojis/like.svg'; ?>"
                                                        class="emoji"> <span
                                                        id="like-count<?php echo $blogDetails->id; ?>"><?php echo $likeReaction; ?></span></button></span>
                                        <span><button class="c_btn" style="border-radius: 12px" href=""> <img
                                                        style="height: 20px; width: auto;"
                                                        src="<?php echo base_url() . 'fb_imo/emojis/love.svg'; ?>"
                                                        class="emoji"> <span
                                                        id="love-count<?php echo $blogDetails->id; ?>"> <?php echo $loveReaction; ?> </span> </button></span>
                                        <span><button class="c_btn" style="border-radius: 12px" href=""> <img
                                                        style="height: 20px; width: auto;"
                                                        src="<?php echo base_url() . 'fb_imo/emojis/haha.svg'; ?>"
                                                        class="emoji"> <span
                                                        id="smiley-count<?php echo $blogDetails->id; ?>"> <?php echo $smileReaction; ?> </span> </button></span>
                                        <span><button class="c_btn" style="border-radius: 12px" href=""> <img
                                                        style="height: 20px; width: auto;"
                                                        src="<?php echo base_url() . 'fb_imo/emojis/wow.svg'; ?>"
                                                        class="emoji"> <span
                                                        id="wow-count<?php echo $blogDetails->id; ?>"> <?php echo $wowReaction; ?> </span> </button></span>
                                    </span>

                                    <span class="likes_box"><i
                                                class="fa fa-eye"></i> <span> <?php echo $blogDetails->view_count; ?></span></span>

                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="clearfix"></div>
                    <hr style="margin: 0.5em;">
                    <div class="col-xs-12 event_view_right">
                        <div class="event_view_right_top">
                            <h3>Details: <?php echo $blogDetails->url_type; ?></h3>
                            <p class="directory_profile_text1"><?php echo htmlspecialchars_decode(html_entity_decode($blogDetails->description)); ?></p>

                        </div>
                        <hr style="margin: 0.5em;">
                    </div>
                    <div class="col-xs-12">
                        <div class="directory_profile_social_share">
                            <i style="color: #012F49" class="fa fa-share-alt" aria-hidden="true"></i>
                            <a class="facebook customer share" style="color: #5D7FBF"
                               href="http://www.facebook.com/sharer.php?u=<?php // echo urlencode($fullUrl); ?>"
                               title="Facebook share"
                               target="_blank"><i class="fa fa fa-facebook fa-lg" style="margin: 0 8px;"
                                                  aria-hidden="true"></i></a>
                            <a class="twitter customer share" style="color: #1b95e0"
                               href="http://twitter.com/share?url=<?php // echo urlencode($fullUrl); ?>&amp;hashtags=youthopiabangla"
                               title="Twitter share" target="_blank"><i class="fa fa-twitter fa-lg"
                                                                        style="margin: 0 8px;"
                                                                        aria-hidden="true"></i></a>
                            <a class="google_plus customer share" style="color: #db4437"
                               href="https://plus.google.com/share?url=<?php // echo urlencode($fullUrl); ?>"
                               title="Google Plus Share"
                               target="_blank"><i class="fa fa-google-plus fa-lg" style="margin: 0 8px;"
                                                  aria-hidden="true"></i></a>
                            <a class="email modalLink" data-modal-size="modal-md"
                               style="color: #79B04A;"
                               href="<?php echo site_url().'/blog/report_problem/'.$blogDetails->id.'/'.$id ?>"
                               title="Mail to us"><i
                                        class="fa fa-envelope fa-lg"
                                        style="margin: 0 8px;" aria-hidden="true"></i></a>

                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <hr/>
                </div>


                <div class="container">
                    <h3>Comment Section</h3>
                    <section class="blog-comments" id="easyPaginate">
                        <?php
                        if (!empty($blogComments)) {
                            foreach ($blogComments as $blogComment) { ?>
                                <article class="comment show_block_elem">
                                    <a target="_blank" class="comment-img" href="<?php echo site_url().'blog/viewInfo/'.$blogComment->user_id; ?>">
                                        <img src="<?php echo !empty($blogComment->author_image) && file_exists($blogComment->author_image) ? base_url() . $blogComment->author_image : base_url() . 'upload/avater.png'; ?>"
                                             alt="<?php echo $blogComment->author_name ?>"
                                             title="<?php echo (isset($blogComment->org_name) && !empty($blogComment->org_name)) ? $blogComment->org_name : $blogComment->author_name ?>"
                                             width="50" height="50">
                                    </a>
                                    <div class="comment-body ">
                                        <div class="panel panel-default text">

                                            <div class="panel-body"><?php echo htmlspecialchars_decode(html_entity_decode($blogComment->comment)) ?></div>
                                            <div class="panel-footer attribution clearfix">
                                                <p class="pull-left text-info">by
                                                    - <?php echo (isset($blogComment->org_name) && !empty($blogComment->org_name)) ? $blogComment->org_name : $blogComment->author_name ?></p>
                                                <p class="pull-right text-muted"><?php
                                                    //                                                    $created = new \Carbon\Carbon($blogComment['created_at']);
                                                    //                                                    echo $created->diffForHumans(\Carbon\Carbon::now());

                                                    $nowtime = time();
                                                    $oldtime = strtotime($blogComment->created_at);

                                                    echo $this->human_readable_time->time_elapsed_A($nowtime - $oldtime) . "\n";
                                                    //                                                  echo "time_elapsed_B: ".$this->human_readable_time->time_elapsed_B($nowtime-$oldtime)."\n";


                                                    ?>
                                                    <?php if (isset($this->session->userdata['user_logged_user']['userId']) && !empty($this->session->userdata['user_logged_user']['userId']) && $blogComment->user_id == $this->session->userdata['user_logged_user']['userId']) { ?>
                                                        |
                                                        <a href="<?php echo site_url() . 'blog/deleteComment/' . $blogDetails->id . '/' . $blogComment->id; ?>"
                                                           class="pull-right text-danger" title="Delete Comment"
                                                           onclick="return confirm('Confirm delete?')"><i
                                                                    class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                    <?php } ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            <?php }
                        } else {
                            echo '<p class="text-center">This media blog have no comment</p>';
                        } ?>
                    </section>
                    <?php if (isset($this->session->userdata['user_logged_user']['userId']) && !empty($this->session->userdata['user_logged_user']['userId'])) { ?>
                        <form class="box" method="post" id="blog-comment-form"
                              action="<?php echo site_url() . 'blog/submitComment/' . $blogDetails->id ?>"
                              enctype="multipart/form-data">


                            <div class="row new_rz_box2">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <h3>Comment *</h3>
                                    <textarea class="redactor" name="description" id="description"  rows="5" required> </textarea>
                                </div>
                            </div>


                            <input type="hidden" name="blog-id" value="<?php echo $blogDetails->id ?>">
                            <input type="hidden" name="user-id"
                                   value="<?php echo $this->session->userdata['user_logged_user']['userId'] ?>">
                            <div class="form-group row new_rz_box2">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <button class="rz_button3 btn btn-success btn-lg" type="submit" name="submitComment"
                                            id="submitComment">Submit Listing
                                    </button>
                                </div>




                            </div>

                        </form>
                    <?php } ?>
                </div>

                <div class="clearfix"></div>


            </div>
        </div>
    </section>
</div>

<!--<script src="--><?php //echo base_url(); ?><!--old/assets/vendors/ckeditor2/ckeditor.js"></script>-->
<!--<script src="--><?php //echo base_url(); ?><!--old/assets/vendors/ckeditor2/samples/js/sample.js"></script>-->

<script>
//    initSample();
    $("#description").attr("required", "required");
    $("#submitComment").click(function () {
        CKEDITOR.instances.description.updateElement();
    });

    $(function () {
        $('#easyPaginate').easyPaginate({
            paginateElement: '.show_block_elem',
            elementsPerPage: 10,
            effect: 'default'
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#event_view_slider").owlCarousel({
            autoPlay: 3000,
            pagination: false,
            navigation: true,
            singleItem: true,
        });
    });

    function reactionFunction(type, blog_id, user_id = null) {

        if (!user_id) {
            alert('Please login to perform this task');
            return false;
        }
        $.ajax({
            url: '../calSearch',
            type: 'POST',
            data: {
                blog_type: type,
                user_id: user_id,
                blog_id: blog_id,
                action: 'blogInterest'
            }
        }).done(function (response) {
            var res = $.parseJSON(response);
            console.log(res);

            $('#like-count').html(0);
            $('#smiley-count').html(0);
            $('#love-count').html(0);
            $('#wow-count').html(0);
            res.count.forEach(function (counts) {
                if (counts.type == '1') {
                    $('#like-count').html(counts.interest_count);
                }
                else if (counts.type == '2') {
                    $('#smiley-count').html(counts.interest_count);
                }
                else if (counts.type == '3') {
                    $('#love-count').html(counts.interest_count);
                }
                else if (counts.type == '4') {
                    $('#wow-count').html(counts.interest_count);
                }
                else {
                }
            });
            if (!res.ack) {
                alert(res.msg);
            }
        });
    }
</script>

<script>
    ;(function ($) {
        /**
         * jQuery function to prevent default anchor event and take the href * and the title to make a share popup
         *
         * @param  {[object]} e           [Mouse event]
         * @param  {[integer]} intWidth   [Popup width defalut 500]
         * @param  {[integer]} intHeight  [Popup height defalut 400]
         * @param  {[boolean]} blnResize  [Is popup resizeabel default true]
         */
        $.fn.customerPopup = function (e, intWidth, intHeight, blnResize) {
            // Prevent default anchor event
            e.preventDefault();
            // Set values for window
            intWidth = intWidth || '500';
            intHeight = intHeight || '400';
            strResize = (blnResize ? 'yes' : 'no');
            // Set title and open popup with focus on it
            var strTitle = ((typeof this.attr('title') !== 'undefined') ? this.attr('title') : 'Social Share'),
                strParam = 'width=' + intWidth + ',height=' + intHeight + ',resizable=' + strResize,
                objWindow = window.open(this.attr('href'), strTitle, strParam).focus();
        }
        /* ================================================== */
        $(document).ready(function ($) {
            $('.customer.share').on("click", function (e) {
                $(this).customerPopup(e);
            });
        });
    }(jQuery));
</script>
<script src="https://apis.google.com/js/platform.js" async defer></script>
<!-- JS ENDS -->

<script>
    //    initializeReadMore();
</script>
<script>
    $(".alert").fadeTo(2000, 500).slideUp(500, function () {
        $(".alert").alert('close');
    });
</script>