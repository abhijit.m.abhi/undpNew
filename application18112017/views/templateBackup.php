

<html class="no-js" lang="en">
   <!--<![endif]-->
   <head>
      <title>Home | Youthopia Bangla</title>
      <meta charset="utf-8"/>
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
      <meta name="description" content="Youthopia.bangla | Youth Platform at Bangladesh"/>
      <meta name="keywords" content="youthopia.bangla, youthopia, youthopia bangladesh, youth bangladesh, undp, unv, youthopia bangla"/>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="author" content="Youthopia.bangla">
      <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/youthopia_logo.ico">
      <!--google sign in meta-->
      <meta name="google-signin-client_id"
         content="620679733936-id09lrcd16t9bku12u675v9ec6q5quuc.apps.googleusercontent.com">
      <!-- CSS -->
      <link href="<?php echo base_url(); ?>assets/css/portal-css/bootstrap.min.css" rel="stylesheet">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/portal-css/datepicker.css" rel="stylesheet">
      <link href="<?php echo base_url(); ?>assets/css/portal-css/font-awesome.min.css" rel="stylesheet">
      <link href="<?php echo base_url(); ?>assets/css/portal-css/ionicons.min.css" rel="stylesheet">
      <link href="<?php echo base_url(); ?>assets/vendors/fileuploader/src/jquery.fileuploader.css" media="all" rel="stylesheet">
      <link href="<?php echo base_url(); ?>assets/css/portal-css/style.css" rel="stylesheet">
      <link href="<?php echo base_url(); ?>assets/css/portal-css/responsive.css" rel="stylesheet">
      <script src="https://apis.google.com/js/platform.js" async defer></script>
      <!--[if (gte IE 6)&(lte IE 8)]>
      <script type="text/javascript" src="selectivizr.js"></script>
      <noscript>
         <link rel="stylesheet" href="[fallback css]"/>
      </noscript>
      <![endif]-->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
   </head>
   <body>
      <style>
         div#homeline1 span#a {
         display: inline;
         }
         label {
         display: inline-block;
         max-width: 100%;
         margin-bottom: 1px;
         font-weight: 700;
         }
         div#homeline1:hover span#a {
         display: none;
         }
         div#homeline1 span#b {
         display: none;
         }
         div#homeline1:hover span#b {
         display: inline;
         }
         @font-face {
         font-family: SolaimanLipi;
         src: url('fonts/SolaimanLipi_new.ttf');
         }
      </style>
      <style>
         .modal-backdrop {
         position: relative;
         }
      </style>
      <style>
         body{
         font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
         background-image: url("<?php echo base_url(); ?>/assets/portal-img/img/home_bg.jpg");
         background-repeat: no-repeat;
         background-size: 1366px 720px;
         }
         .logo i {
         font-size: 31px;
         margin-right: 4px;
         word-spacing: 14px;
         }
         .logo{
         color: white;
         margin: 0;
         font-size: 20px;
         padding: 4px 0;
         padding-bottom: 15px;
         }
         .login-bottom-text{
         margin-top: 0px;
         margin-bottom: 0px;
         font-size: 12px;
         color: white;
         line-height: 19px;
         }
         header{
         /*background: #3b5998;*/
         padding-top: 20px;
         }
         header .form-group{
         margin-bottom: 0px;
         }
         .form-group {
         margin-bottom: 1px;
         }
         header .btn-header-login{
         margin-bottom: 15px;
         }
         .login-main{
         margin-top: 130px;
         }
         .multibox{
         padding-left: 0px;
         padding-bottom: 10px;
         }
         .login-main span{
         font-size: 12px;
         }
         footer hr{
         margin-top: 0px;
         padding-top: 0px;
         }
         .footer-options ul{
         clear: both;
         padding: 0px;
         margin: 0px;
         }
         .footer-options ul li{
         float:left;
         list-style: none;
         padding: 5px;
         font-size: 12px;
         }
         .footer-options ul li a{
         text-decoration: none;
         color: #000;
         }
         .copyrights{
         margin-top: 25px;
         }
      </style>
      <header>
         <div class="container">
            <div class="row">
               <div class="col-sm-6">
                  <div class="logo"><img src="<?php echo base_url(); ?>assets/portal-img/img/logo.png" alt=""></div>
               </div>
               <?php echo form_open('auth/userLogin', "class='form-vertical'"); ?>
               <div class="col-sm-4"> <?php echo $this->session->flashdata('user_check'); ?></div>
               <div class="col-sm-6 hidden-xs">
                  <div class="row">
                     <div class="col-sm-5">
                        <div class="form-group">
                           <input type="text" name="email" class="form-control" value="<?php echo set_value('email'); ?>" placeholder="Email">
                           <div class="login-bottom-text checkbox hidden-sm">
                              <!--  <label>
                                 <input type="checkbox" id="">
                                 Keep me sign in
                                 </label> -->
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-5">
                        <div class="form-group">
                           <input type="password" class="form-control" name="password"  placeholder="Password">
                           <div class="login-bottom-text hidden-sm"> Forgot your password?  </div>
                        </div>
                     </div>
                     <div class="col-sm-2">
                        <div class="form-group">
                           <input type="submit" value="login" class="btn btn-default btn-header-login">
                        </div>
                        <?php echo form_close(); ?>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </header>
      <article class="container">
         <div class="row">
            <div class="col-sm-8">
               <div class="login-main">
                  <div class="col-sm-12">
                     <br>
                     <div class="social_login" align="left">
                        <!--<a class="facebook_icon" href="<?php //echo $faceBookClient->getAuthUrl(); ?>"><i class="fa fa-facebook"></i></a>-->
                        <!-- <a class="facebook_icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Sign up with Facebook"><i class="fa fa-facebook"></i></a> -->
                        <b>Login With:</b><a class="facebook_icon" id="facebook_url_for_login"
                        <?php
                           if(!empty($authUrl)){
                             //echo "<pre>";print_r($authUrl);exit();
                             echo '<a href="'.$authUrl.'"><i class="fa fa-facebook"></i></a>';
                           }
                           ?>
                        <a class="gplus_icon" href="#"><i
                           class="fa fa-google-plus"></i></a>
                     </div>
                     <br><br>
                     <div class="col-md-12 hidden-xs">
                        <a href="orgdirectory.php" class="home_icons_col home_icons_col1">
                        <span class="home_icons_title">Directory</span> <br>
                        <span class="home_icons_img">
                        <img src="<?php echo base_url(); ?>assets/portal-img/img/33.png"
                           onmouseover="this.src='<?php echo base_url(); ?>assets/portal-img/img/3.png'"
                           onmouseout="this.src='<?php echo base_url(); ?>assets/portal-img/img/33.png'"
                           border="0" alt=""/>
                        </span> <br>
                        <span class="home_icons_text">জংশন</span>
                        </a>
                        <a href="org-event-calender.php" class="home_icons_col home_icons_col4">
                        <span class="home_icons_title">Calendar</span> <br>
                        <span class="home_icons_img">
                        <img src="<?php echo base_url(); ?>assets/portal-img/img/11.png"
                           onmouseover="this.src='<?php echo base_url(); ?>assets/portal-img/img/1.png'"
                           onmouseout="this.src='<?php echo base_url(); ?>assets/portal-img/img/11.png'"
                           border="0" alt=""/>
                        </span> <br>
                        <span class="home_icons_text">দিনোপাখ্যান</span>
                        </a>
                        <a href="opportunities.php" class="home_icons_col home_icons_col3">
                        <span class="home_icons_title">Opportunities</span> <br>
                        <span class="home_icons_img">
                        <img src="<?php echo base_url(); ?>assets/portal-img/img/44.png"
                           onmouseover="this.src='<?php echo base_url(); ?>assets/portal-img/img/4.png'"
                           onmouseout="this.src='<?php echo base_url(); ?>assets/portal-img/img/44.png'"
                           border="0" alt=""/>
                        </span> <br>
                        <span class="home_icons_text">দূরবীক্ষণ</span>
                        </a>
                        <a href="media_blogs.php" class="home_icons_col home_icons_col2">
                        <span class="home_icons_title">Media Blog</span> <br>
                        <span class="home_icons_img">
                        <img src="<?php echo base_url(); ?>assets/portal-img/img/22.png"
                           onmouseover="this.src='<?php echo base_url(); ?>assets/portal-img/img/2.png'"
                           onmouseout="this.src='<?php echo base_url(); ?>assets/portal-img/img/22.png'"
                           border="0" alt=""/>
                        </span> <br>
                        <span class="home_icons_text">প্রতিবিম্ব</span>
                        </a>
                     </div>
                     <div class="col-xs-6 hidden-lg hidden-md hidden-sm">
                        <a href="orgdirectory.php" class="home_icons_col home_icons_col1">
                        <span class="home_icons_title">Directory</span> <br>
                        <span class="home_icons_img">
                        <img src="<?php echo base_url(); ?>assets/portal-img/img/33.png"
                           onmouseover="this.src='<?php echo base_url(); ?>assets/portal-img/img/3.png'"
                           onmouseout="this.src='<?php echo base_url(); ?>assets/portal-img/img/33.png'"
                           border="0" alt=""/>
                        </span> <br>
                        <span class="home_icons_text">জংশন</span>
                        </a>
                        <a href="org-event-calender.php" class="home_icons_col home_icons_col4">
                        <span class="home_icons_title">Calendar</span> <br>
                        <span class="home_icons_img">
                        <img src="<?php echo base_url(); ?>assets/portal-img/img/11.png"
                           onmouseover="this.src='<?php echo base_url(); ?>assets/portal-img/img/1.png'"
                           onmouseout="this.src='<?php echo base_url(); ?>assets/portal-img/img/11.png'"
                           border="0" alt=""/>
                        </span> <br>
                        <span class="home_icons_text">দিনোপাখ্যান</span>
                        </a>
                     </div>
                     <div class="col-xs-6 hidden-lg hidden-md hidden-sm">
                        <a href="opportunities.php" class="home_icons_col home_icons_col3">
                        <span class="home_icons_title">Opportunities</span> <br>
                        <span class="home_icons_img">
                        <img src="<?php echo base_url(); ?>assets/portal-img/img/44.png"
                           onmouseover="this.src='<?php echo base_url(); ?>assets/portal-img/img/4.png'"
                           onmouseout="this.src='<?php echo base_url(); ?>assets/portal-img/img/44.png'"
                           border="0" alt=""/>
                        </span> <br>
                        <span class="home_icons_text">দূরবীক্ষণ</span>
                        </a>
                        <a href="media_blogs.php" class="home_icons_col home_icons_col2">
                        <span class="home_icons_title">Media Blog</span> <br>
                        <span class="home_icons_img">
                        <img src="<?php echo base_url(); ?>assets/portal-img/img/22.png"
                           onmouseover="this.src='<?php echo base_url(); ?>assets/portal-img/img/2.png'"
                           onmouseout="this.src='<?php echo base_url(); ?>assets/portal-img/img/22.png'"
                           border="0" alt=""/>
                        </span> <br>
                        <span class="home_icons_text">প্রতিবিম্ব</span>
                        </a>
                     </div>
                  </div>
               </div>
               <!--login-main-->
            </div>
            <!--col-sm-8-->
            <div class="col-sm-4">
               <div class="">
                  <h3><i class="fa fa-shield"></i> Register now</h3>
                  <form id='form-id'>
                     <input id='watch-me' name='test' type='radio' value="a" /> Individual
                     <input name='test' type='radio' value="b" /> Organization
                     <div class="social_login" align="left">
                        <a class="facebook_icon" id="facebook_url_for_login"
                        <?php
                           if(!empty($authUrl)){
                             //echo "<pre>";print_r($authUrl);exit();
                             echo '<a href="'.$authUrl.'"><i class="fa fa-facebook "></i></a>';
                           }
                           ?>
                        <a class="gplus_icon"  href="#"><i
                           class="fa fa-google-plus"></i></a>
                     </div>
                  </form>
                  <?php echo form_open_multipart('portal/individualRegis', "class='form-horizontal margin-none'"); ?>
                  <div id='show-individual' style='display:'>
                     <hr>
                     <?php echo $this->session->flashdata('msg'); ?>
                     <?php echo $this->session->flashdata('Error'); ?>
                     <div class="msg">
                        <?php
                           if (validation_errors() != false) {
                             ?>
                        <div class="alert alert-danger">
                           <button data-dismiss="alert" class="close" type="button">×</button>
                           <?php echo validation_errors(); ?>
                        </div>
                        <?php
                           }
                           ?>
                     </div>
                     <div class="form-group">
                        <label class="control-label" for="" style="color:white;">Full Name<span style="color:red;">*</span></label>
                        <?php echo form_input(array('class' => 'form-control', 'required' => 'required', 'placeholder' => 'Full Name', 'id' => 'fname', 'name' => 'fname', 'value' => set_value('fname'))); ?>
                     </div>
                     <div class="form-group">
                        <label class="control-label" for=""  style="color:white;">Mobile Number <span style="color:red;">*</span></label>
                        <input type="text" class="form-control" name="number" id="number" value="<?php echo set_value('number'); ?>" placeholder="Mobile Number" required="required">
                     </div>
                     <div class="">
                        <br>
                        <div class="form-group">
                           <div class="col-sm-6 multibox">
                              <input class="form-control date_picker" type="text" id="date" name="date"
                                 placeholder="DOB (YY-MM-DD)*"  required="required"/>
                           </div>
                           <div class="col-sm-6 multibox">
                              <select class="form-control" name="gender" required="required">
                                 <option value="">Select gender*</option>
                                 <option value="Male">Male</option>
                                 <option value="Female">Female</option>
                                 <option value="Others">Others</option>
                              </select>
                           </div>
                        </div>
                     </div>
                     <label class="control-label" for="" style="color:white;">Image</label>
                     <div class="form-group"
                        style="display:inline-flex; color:white; margin-bottom: 6px;width: 100%">
                        <input type="file" name="main_image" onchange="ValidateSize(this)" id="individual-file">

                     </div>
                      <div id="file_error_indi" style="color:red;"></div>
                     <div class="form-group">
                        <label class="control-label" for="" style="color:white;">Email ID<span style="color:red;">*</span></label>
                        <?php echo form_input(array('class' => 'form-control', 'placeholder' => 'Email ID', 'id' => 'email', 'name' => 'email','type' => 'email',  'required' => 'required')); ?>
                        <div id="checkUsermail"></div>
                     </div>
                     <div class="form-group">
                        <label class="control-label" for="" style="color:white;">Password<span style="color:red;">*</span></label>
                        <?php echo form_input(array('class' => 'minLength password form-control', 'placeholder' => 'Password', 'id' => 'password', 'name' => 'password','type' => 'password',  'required' => 'required')); ?>
                     </div>
                     <div class="form-group" style="color:white;">
                        <label class="control-label" for="">Confirm Password</label>
                        <?php echo form_input(array('class' => 'password_conf form-control', 'placeholder' => 'Confirm Password', 'id' => 'password_conf','type' => 'password', 'name' => 'password_conf', 'value' => set_value('password_conf'), 'required' => 'required')); ?>
                     </div>
                     <div class="form-group" style="color:white;">
                        <p style="font-size:14px;"><input class="" type="checkbox" id="agree" name="agree"
                           value="agree" required="required"  checked/><label for="agree" style="color:white;">I ACCEPT THE <a href="Terms_and_Conditions.pdf" target="_blank" style="color: white; text-decoration:underline;">TERMS &amp;CONDITIONS </a> and <a href="Privacy_Policy.pdf" target="_blank" style="color: white; text-decoration:underline;"> PRIVACY &amp; POLICY</a></label>
                        </p>
                     </div>
                     <small>
                        <!-- By clicking Sign Up, you agree to our Terms and that you have read our
                           Data Use Policy, including our Cookie Use. -->
                     </small>
                     <div style="height:10px;"></div>
                     <div class="form-group">
                        <label class="control-label" for=""></label>
                        <input type="submit"  value="Request for Registration" class="btn btn-primary">
                     </div>
                     <?php echo form_close(); ?>
                  </div>
                  <!--show individual -->
                  <?php echo form_open_multipart('portal/organizationRegis', "class='form-horizontal margin-none'"); ?>
                  <div id='show-org' style='display:none'>
                     <hr>
                     <?php echo $this->session->flashdata('msg'); ?>
                     <?php echo $this->session->flashdata('Error'); ?>
                     <div class="msg">
                        <?php
                           if (validation_errors() != false) {
                             ?>
                        <div class="alert alert-danger">
                           <button data-dismiss="alert" class="close" type="button">×</button>
                           <?php echo validation_errors(); ?>
                        </div>
                        <?php
                           }
                           ?>
                     </div>

                     <div class="form-group">
                        <label class="control-label" for="" style="color:white;">Organization Name<span style="color:red;">*</span></label>
                        <?php echo form_input(array('class' => 'form-control', 'placeholder' => 'Organization Name', 'id' => 'oname', 'name' => 'oname', 'required' => 'required','value' => set_value('oname'))); ?>
                     </div>
                     <div class="form-group">
                        <label class="control-label" for=""  style="color:white;">Organization Email <span style="color:red;">*</span></label>
                        <?php echo form_input(array('class' => 'form-control', 'placeholder' => 'Organization Email', 'id' => 'oemail', 'name' => 'oemail','type' => 'email',  'required' => 'required')); ?>
                        <div id="checkUserOrgmail"></div>
                     </div>
                     <div class="form-group">
                        <label class="control-label" for="" style="color:white;">Facebook Page Link <span style="color:red;">*</span></label>
                        <input type="text" class="form-control" name="facebook" placeholder="Facebook Page Link" required="required">
                     </div>
                     <div class="form-group">
                        <label class="control-label" for="" style="color:white;">Website</label>
                        <input type="text" class="form-control" name="website" placeholder="website" required="required">
                     </div>
                     <div class="form-group">
                        <label class="control-label" for="" style="color:white;">Contact Person Name <span style="color:red;">*</span></label>
                        <input type="text" class="form-control" name="fname" placeholder="Contact Person Name" required="required">
                     </div>
                     <div class="form-group">
                        <label class="control-label" for="" style="color:white;">Contact Person Email <span style="color:red;">*</span></label>
                        <input type="email" class="form-control" name="email" placeholder="Contact Person Email" required="required">
                     </div>
                     <div class="form-group">
                        <label class="control-label" for="" style="color:white;">Contact Person Mobile Number <span style="color:red;">*</span></label>
                        <input type="text" class="form-control" name="number"  placeholder="Contact Person Mobile Number" id="number_org" required="required">
                     </div>
                     <!-- <div class=""> -->
                     <label class="control-label" for="" style="color:white;">Image </label>
                     <div class="form-group"
                        style="display:inline-flex; color:white; margin-bottom: 6px;width: 100%">
                        <!--                                                <label class="control-label" for="file" style="width: 131px;">Image(max-5Mb):</label>-->
                        <input type="file" name="main_image" id="individual-file" onchange="ValidateSizeOrg(this)">
                        <!--                                                <input type="file" name="file" id="file" style="margin-left:1px" required="required" title="image">-->
                     </div>
                      <div id="file_error_org" style="color:red;"></div>
                     <?php $this->load->view('template/googleMap'); ?>
                     <div id="infowindow-content">
                        <img src="" width="16" height="16" id="place-icon">
                        <span id="place-name"  class="title"></span><br>
                        <span id="place-address"></span>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="control-label" for="" style="color:black;">Password <span style="color:red;">*</span></label>
                     <?php echo form_input(array('class' => 'minLengthOrg password_org form-control', 'placeholder' => 'Password', 'id' => 'password_org', 'name' => 'password','type' => 'password', 'value' => set_value('password_org'), 'required' => 'required')); ?>
                  </div>
                  <div class="form-group" style="color:black;">
                     <label class="control-label" for="">Confirm Password <span style="color:red;">*</span></label>
                     <?php echo form_input(array('class' => 'password_conf_org form-control', 'placeholder' => 'Confirm Password', 'id' => 'password_conf_org','type' => 'password', 'name' => 'password_conf_org', 'value' => set_value('password_conf_org'), 'required' => 'required')); ?>
                  </div>
                  <div class="form-group" style="color:white;">
                     <p style="font-size:14px;"><input class="" type="checkbox" id="agree" name="agree"
                        value="agree" required="required" checked /><label for="agree" style="color:Black;">I ACCEPT THE <a href="Terms_and_Conditions.pdf" target="_blank" style="text-decoration:underline;">TERMS &amp;CONDITIONS </a><label> and <a href="Privacy_Policy.pdf" target="_blank" style="text-decoration:underline;"> PRIVACY &amp; POLICY</a></label>
                     </p>
                  </div>
                  <small>
                     <!-- By clicking Sign Up, you agree to our Terms and that you have read our
                        Data Use Policy, including our Cookie Use. -->
                  </small>
                  <div style="height:10px;"></div>
                  <div class="form-group">
                     <label class="control-label" for=""></label>
                     <input type="submit" value="Request for Registration" class="btn btn-primary">
                  </div>
                  <?php echo form_close(); ?>
               </div>
               <!--show org -->
            </div>
            <!--div faka -->
         </div>
         <!--col-sm-4 finished -->
         </div><!--aticle row finished -->
      </article>
      <!--aticle finished -->
      <?php $this->load->view('template/footer'); ?>
      <!-- ALL THE SCRIPTS/PLUGINS -->
      <!-- Jquery, Modernizer, Selectivizr & Prefix Free Js -->
      <script src="<?php echo base_url(); ?>assets/js/portal-js/jquery.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/js/portal-js/modernizr-2.8.3.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/js/portal-js/selectivizr-min.js"></script>
      <!-- Bootstrap Js -->
      <script src="<?php echo base_url(); ?>assets/js/portal-js/bootstrap.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/js/portal-js/bootstrap-datetimepicker.min.js"></script>
      <!-- Theme Main Js -->
      <script src="<?php echo base_url(); ?>assets/js/portal-js/main.js"></script>
      <script src="<?php echo base_url(); ?>assets/js/portal-js/jquery.alphanum.js"></script>
      <!-- AJAX JS-->
      <script src="<?php echo base_url(); ?>assets/js/portal-js/ajax.js"></script>
      <!--FACEBOOK JS Script-->
      <script src="<?php echo base_url(); ?>assets/js/portal-js/fbzahed.js"></script>
      //  <script type="text/javascript">
         //   $(document).ready(function() {
         //    $('#click_form').click(function(){
         //     jQuery.ajax({
         //       type: "POST",
         //       url: "<?php echo site_url('portal/individualRegis') ?>",
         //       data: $("#reg_form").serialize(),
         //       success: function(res) {
         //         alert("Okay");
         //         // if(res == 'N'){
         //         //   $(".ErrorMessage").html(res);
         //         // }
         //         $(".ErrorMessage").html(res);
         //       }
         //     });
         //   });
         //  });
         //
      </script>
      <script type="text/javascript">
         $('#individual-filefg').bind('change', function() {
             confirm('This file size is: ' + this.files[0].size/1024/1024 + "MB");
         });
      </script>
      <script type="text/javascript">
         $("#individual-file11").change(function () {
          //alert('fgg');
            var fileSize = this.files[0];
            var sizeInMb = fileSize.size/1024;
            var sizeLimit= 1024*1;
            if (sizeInMb > sizeLimit) {
              confirm('dsd');


            }
            else {


            }
          });



      </script>
      <script type="text/javascript">
         function ValidateSize(file) {
         var FileSize = file.files[0].size / 1024 / 1024; // in MB
         if (FileSize > 5) {
             //confirm('File size exceeds 5 MB');
             $("#file_error_indi").html("File Size Exceeds 5MB");
             return false;
            // $(file).val(''); //for clearing with Jquery
         }
         return true;
         }
      </script>

      <script type="text/javascript">
         function ValidateSizeOrg(file) {
         var FileSize = file.files[0].size / 1024 / 1024; // in MB
         if (FileSize > 5) {
             //confirm('File size exceeds 5 MB');
             $("#file_error_org").html("File Size Exceeds 5MB");
             return false;
            // $(file).val(''); //for clearing with Jquery
         }
         return true;
         }
      </script>
      <script type="text/javascript">
         $(document).ready(function() {
           $('#click_form_org').click(function(){
             jQuery.ajax({
               type: "POST",
               url: "<?php echo site_url('portal/organizationRegis') ?>",
               data: $("#reg_form_org").serialize(),
               success: function(res) {
                 $(".ajax_response_result_org").html(res);
               }
             });
           });
         });
      </script>
      <!--<script>
         $(window).load(function () {
         $('#myModal').modal('show');
         });
         </script>-->
      <!--   <script>
         $(window).load(function()
         {
         $('#sign_up').modal('show');
         });
         </script> -->
      <script type="text/javascript">
         function validateFeedback() {
           var number = document.getElementById("number").value;
           var reg = new RegExp("[0-9 .-]*");

           return reg.test(number);
         }
      </script>
      <script type="text/javascript">
         $(function () {
           $("#password_conf").on('blur', function(){
             var password = $(".password").val();
             var confirmPassword = $(".password_conf").val();
             if (password != confirmPassword) {
               alert("These passwords miss match.");
               $(".password").val('');
               $(".password_conf").val('');
               return false;
             }
             return true;
           });
         });
      </script>
      <script type="text/javascript">
         $('.minLength').on('blur', function(){
           if($(this).val().length < 6){
             alert('You have to enter at least 6 digit!');
             $(".password").val('');
           }
         });
      </script>
      <script type="text/javascript">
         $(document).on("blur", "#email", function() {
           var email = $(this).val();
           if(email!='')
           {
             var url = '<?php echo site_url('portal/checkUserEmail') ?>';
             $.ajax({
               type: "POST",
               url: url,
               dataType : 'html',
               data: {email: email},
               success: function(data1) {
                 //console.log(data1);
                 if(data1 == 1){
                   $('#email').val('');
                   $('#checkUsermail').html("<span class='emailExist' style='color:red'>This Email ID Already Registered By Individual</span>");
                 }else{
                   $('#checkUsermail').html('');
                 }

               }
             });
           }
           else
           {
             $("span.emailExist").remove();
           }

         });



      </script>
      <script type="text/javascript">
         $(document).ready(function () {
           $('#CHAR_LOOKUP').alpha();
           $('#number').numeric();
           $('#number_org').numeric();

         });
      </script>
      <script type="text/javascript">
         $(function () {
           $("#password_conf_org").on('blur', function(){
             var password = $(".password_org").val();
             var confirmPassword = $(".password_conf_org").val();
             if (password != confirmPassword) {
               alert("These passwords miss match.");
               $(".password_org").val('');
               $(".password_conf_org").val('');
               return false;
             }
             return true;
           });
         });
      </script>
      <script type="text/javascript">
         $('.minLengthOrg').on('blur', function(){
           if($(this).val().length < 6){
             alert('You have to enter at least 6 digit!');
             $(".password").val('');
           }
         });
      </script>
      <script type="text/javascript">
         $(document).on("blur", "#oemail", function() {
           var oemail = $(this).val();
           //alert(oemail);
           var url = '<?php echo site_url('portal/checkUserOrgEmail') ?>';
           $.ajax({
             type: "POST",
             url: url,
             dataType : 'html',
             data: {oemail: oemail},
             success: function(data1) {
               //console.log(data1);
               // alert(data1);
               if(data1 == 1){
                 $('#oemail').val('');
                 $('#checkUserOrgmail').html("<span style='color:red'>This Email ID Already Registered By Organization</span>");
               }else{
                 $('#checkUserOrgmail').html('');
               }

             }
           });
         });
      </script>
      <script type="text/javascript">
         function readURL(input) {

           if (input.files && input.files[0]) {
             var reader = new FileReader();

             reader.onload = function (e) {
               $("#blah").show();
               $('#blah').attr('src', e.target.result);
             }

             reader.readAsDataURL(input.files[0]);
           }
         }

         $("input#PROFILE_IMG").change(function(){
           readURL(this);
         });
      </script>
      <script type="text/javascript">
         $(document).ready(function () {
           $('.date_picker').datepicker({
             format: "yyyy-mm-dd"
           });
         });
         $("input[name='test']").click(function () {
           $('#show-individual').css('display', ($(this).val() === 'a') ? 'block':'none');
         });
         $("input[name='test']").click(function () {
           $('#show-org').css('display', ($(this).val() === 'b') ? 'block':'none');
           var value=$(this).val();
           if(value=='b')
           {
           //  // $('#show-map').initMap();
         //initMap();

           }
         });
      </script>
      <script>
         $(document).ready(function () {
           $('[data-toggle="popover"]').popover();
         });
      </script>
   </body>
</html>
