    <!-- orris -->
    <link href="<?php echo base_url(); ?>assets/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
           <div class="row">
                        <div class="col-lg-3">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <span class="label label-success pull-right">New Request</span>
                                    <h5>New Request</h5>
                                </div>
                                <div class="ibox-content">
                                    <h1 class="no-margins"><?php echo $total_request->TOTAL_REQUEST; ?></h1>
                                    <div class="stat-percent font-bold text-success"><a style="font-size:14px;" href="<?php echo site_url('admin/reviewOrg'); ?>"> More Info <i class="fa fa-bolt"></i></a></div>
                                    <h4>Total Request</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <span class="label label-info pull-right">Organization Listed</span>
                                    <h5>Organization</h5>
                                </div>
                                <div class="ibox-content">
                                    <h1 class="no-margins"><?php echo $total_organization->total_organization; ?></h1>
                                    <div class="stat-percent font-bold text-info"><a style="font-size:14px;" href="<?php echo site_url('admin/regOrg'); ?>"> More Info <i class="fa fa-level-up"></i></a></div>
                                    <h4>Reg.Organization</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <span class="label label-primary pull-right">Individual</span>
                                    <h5>Individual</h5>
                                </div>
                                <div class="ibox-content">
                                    <h1 class="no-margins"><?php echo $total_individual->total_individual; ?></h1>
                                    <div class="stat-percent font-bold text-navy"><a style="font-size:14px;" href="<?php echo site_url('admin/regIndividual'); ?>"> More Info <i class="fa fa-level-up"></i></a></div>
                                    <h4>Individual Subscriber</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <span class="label label-danger pull-right">Total Page View</span>
                                    <h5>Total Page View</h5>
                                </div>
                                <div class="ibox-content">
                                    <!--  <h1 class="no-margins"><?php echo $total_visit_page->total_visit_page; ?></h1> -->
                                     <h1 class="no-margins"><?php echo $total; ?></h1>
                                    <div class="stat-percent font-bold text-danger"><a style="font-size:14px;" href="#"> More Info <i class="fa fa-level-down"></i></a></div>
                                    <h4>Total Page View</h4>
                                </div>
                            </div>
                        </div>
                    </div>
   <div class="clearfix"></div>

    