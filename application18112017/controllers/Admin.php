<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller 
{
    private $user_session;
    
    public function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('logged_in') == FALSE) {
            redirect('auth/login', 'refresh');
        }
        $this->user_session = $this->session->userdata('logged_in');
        header("Expires: Thu, 19 Nov 1981 08:52:00 GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        $this->load->model('utilities');
        $this->load->model('User');
    }

    /*
     * @methodName checkPrevilege()
     * @access
     * @param  none
     * @return Mixed View
     */

    public function checkPrevilege($param = "")
    {
        if ($param == "") {
            $controller = $this->uri->segment(1, 'dashboard');
            $action = $this->uri->segment(2, 'index');
            $link = "$controller/$action";
        } else {
            $link = "$param";
        }
        return $this->security_model->get_all_checked_module_links_by_user($link, $this->user_session['USERGRP_ID'], $this->user_session['USERLVL_ID'], $this->user_session['USER_ID']);
    }

    /**
     * @methodName  index()
     * @access
     * @param
     * @author      Rakib Roni <rakib@atilimited.net>
     * @return      dashboard
     */
    public function index()
    {
        $data['contentTitle'] = 'Dashboard';
        $data["breadcrumbs"] = array(
            "Admin" => "admin/index",
            "Dashboard" => '#'
        );
        $data['pageTitle'] = 'Youthopia Bangla Admin Panel';
        $data['total_request'] = $this->db->query("SELECT COUNT(u.userId) as TOTAL_REQUEST FROM users u where u.active!=1 and u.type='organization' ")->row();
        $data['total_organization'] = $this->db->query("SELECT COUNT(u.userId) as total_organization FROM users u where u.active=1 and u.type='organization' ")->row();
        $data['total_individual'] = $this->db->query("SELECT COUNT(u.userId) as total_individual FROM users u where  u.type='individual' AND u.active='1'")->row();
        $data['total_facebook'] = $this->db->query("SELECT COUNT(u.userId) as total_facebook FROM users u where  u.facebookId!='' ")->row();
        $data['total_visit_page'] = $this->db->query("SELECT COUNT(l.visitcounter) as total_visit_page FROM listing l")->row();
        $total_inclusive_count= $this->db->query("SELECT sum(nl.view_count) as total_inclusive_count FROM inclusive_bd nl")->result();
        $total_events_count = $this->db->query("SELECT sum(e.view_count) as total_events_count FROM events e")->result();
        $total_blog_count = $this->db->query("SELECT sum(b.view_count) as total_blog_count FROM blog b")->result();
        $total_opportunity_count= $this->db->query("SELECT sum(o.view_count) as total_opportunity_count FROM opportunity o")->result();
        $total_listing_count= $this->db->query("SELECT sum(l.visitcounter) as total_listing_count FROM listing l")->result();
        $data['total']=$total_inclusive_count[0]->total_inclusive_count+$total_events_count[0]->total_events_count+$total_blog_count[0]->total_blog_count+$total_opportunity_count[0]->total_opportunity_count
        +$total_listing_count[0]->total_listing_count;
      // echo '</pre>'; print_r($total_inclusive_count[0]->total_inclusive_count);exit();


        $data['content_view_page'] = 'dashboard';
        $this->admin_template->display($data);
    }



     public function allUserList(){
      $session_info = $this->session->userdata('logged_in');
      $userGrp=$session_info['USERGRP_ID'];
     
      $data['contentTitle'] = 'User List';
      $data['breadcrumbs'] = array(
        'Admin' => '#',
        'User List' => '#',
        );

        
        $data['user_list'] = $this->User->userInfo(); 
        $view=$this->User->userPrivilege($userGrp,'V'); 
        $data['viewPerm'] =$view->PERMSN_EXIST;

        $approve=$this->User->userPrivilege($userGrp,'A'); 
        $data['rejectPerm'] =$approve->PERMSN_EXIST;

        $data['content_view_page'] = 'admin/user/user_list';
        $this->admin_template->display($data);
      }

       function sliderSetup()
    {
        $session_info = $this->session->userdata('logged_in');
        $userGrp=$session_info['USERGRP_ID'];
        $data['contentTitle'] = 'Slider Setup';
        $data["breadcrumbs"] = array(
            "Slider Setup" => "admin/index",
            "Slider Setup" => '#'
        );
        $data['all_slider'] = $this->utilities->findAllFromView('inclusive_bd_slider');
        $view=$this->User->userPrivilege($userGrp,'V'); 
        $data['viewPerm'] =$view->PERMSN_EXIST;
        $approve=$this->User->userPrivilege($userGrp,'A'); 
        $data['rejectPerm'] =$approve->PERMSN_EXIST;
        $data['content_view_page'] = 'admin/slider/all_slider';
        $this->admin_template->display($data);
    }

         function editsliderSetup()
    {
        $slider_id = $this->uri->segment(3);
        $data['contentTitle'] = 'Slider Setup';
        $data["breadcrumbs"] = array(
            "Slider Setup" => "admin/index",
            "Slider Setup" => '#'
        );
        $data['all_slider'] = $this->User->slider($slider_id); 
        $data['content_view_page'] = 'admin/slider/edit_slider';
        $this->admin_template->display($data);
    }





    /**
     * @methodName createNewSlider
     * @access
     * @param  none
     * @author rokibuzzaman <rokibuzzaman@atilimited.net>
     * @return
     */

    function createNewSlider()
    {
      if (isset($_POST['title'])) {
        $title = $this->input->post('title');
        $description = $this->input->post('description');
        

            $FileUploader = new FileUploader('files', array(
                'limit' => 5,
                'maxSize' => 10,
                'fileMaxSize' => 2,
                'extensions' => ['jpg', 'jpeg', 'png', 'gif', 'bmp'],
                'required' => true,
                'uploadDir' => 'upload/inclusive_slider/',
                'title' => '{timestamp}--{file_name}',
                'replace' => false,
                'listInput' => true,
                'files' => null
            ));

            // call to upload the files
            $data = $FileUploader->upload();

            // if uploaded and success
            if ($data['isSuccess'] && count($data['files']) > 0) {
                // get uploaded files
                $uploadedFiles = $data['files'];
                $images = '';
                $first = true;
                foreach ($uploadedFiles as $uploadedFile) {
                    if ($first == false) {
                        $images .= '___';
                    }
                    $images .= $uploadedFile['name'];
                    $first = false;
                }
            }




        $newPostData = array(
            'IMAGE_TITLE' => $title,
            'IMAGE_DESC' => $description,
            'IMAGE_PATH' => $images,
            

        );

        $this->db->insert('inclusive_bd_slider', $newPostData);
        $this->session->set_flashdata('Success','Successfully Created.');
        redirect('Admin/sliderSetup', 'refresh');
      }
      else {
            $data['content_view_page'] = 'admin/slider/create_slider';
            $this->admin_template->display($data);
        }
    }



        function insertEditedsliderSetup()
    {
        $slider_id = $this->uri->segment(3);

        $title = $this->input->post('title');
        $description = $this->input->post('description');

        $hasFile = $this->input->post('fileuploader-list-files');
        $ext_image = $this->input->post('ext_image');

               $FileUploader = new FileUploader('files', array(
                'limit' => 5,
                'maxSize' => 10,
                'fileMaxSize' => 2,
                'extensions' => ['jpg', 'jpeg', 'png', 'gif', 'bmp'],
                'required' => true,
                'uploadDir' => 'upload/inclusive_slider/',
                'title' => '{timestamp}--{file_name}',
                'replace' => false,
                'listInput' => true,
                'files' => null
            ));

            // call to upload the files
            $data = $FileUploader->upload();

            // if uploaded and success
            if ($data['isSuccess'] && count($data['files']) > 0) {
                // get uploaded files
                $uploadedFiles = $data['files'];
                $images = '';
                $first = true;
                foreach ($uploadedFiles as $uploadedFile) {
                    if ($first == false) {
                        $images .= '___';
                    }
                    $images .= $uploadedFile['name'];
                    $first = false;
                }

                    $newPostData = array(
                    'IMAGE_TITLE' => $title,
                    'IMAGE_DESC' => $description,
                    'IMAGE_PATH' => $images,
                  
                );


            }else{
                 $newPostData = array(
                    'IMAGE_TITLE' => $title,
                    'IMAGE_DESC' => $description,
                  
                  
                );

            }



           

        if($this->db->update('inclusive_bd_slider', $newPostData, array('ID' => $slider_id)))
        {
            $this->session->set_flashdata('Success','Successfully Updated.');
            redirect('Admin/sliderSetup');
        }
      }

    



    public function delete_slider_edit()
    {
      $opp_id = $_POST['opp_id'];
      $image_name = $_POST['img_name'];
      $result = $this->User->deleteSliderImage($opp_id, $image_name);
      $msg = $result['msg'];
      if(!$result['ack']){
        $ack = false;
         }else{
      $ack = true;
          }
      echo json_encode(array('ack' => $ack, 'msg' => $msg, 'ID' => $opp_id));
    }





    public function delete_slider_from_db()
    {

        $slider_id = $this->input->post('slider_id');
        if ($this->db->query("DELETE FROM inclusive_bd_slider WHERE ID = $slider_id")) {
            echo 1;
        } else {
            echo 0;
        }
    }


    public function delete_sa_users_from_db()
    {

        $user_id = $this->input->post('user_id');
        if ($this->db->query("DELETE FROM sa_users WHERE USER_ID = $user_id")) {
            echo 1;
        } else {
            echo 0;
        }
    }


      public function regOrg(){
      $session_info = $this->session->userdata('logged_in');
      $userGrp=$session_info['USERGRP_ID'];
      $data['contentTitle'] = 'Registered Organization List';
      $data['breadcrumbs'] = array(
        'Admin' => '#',
        'Registered Organization List' => '#',
        );
        
        $data['reg_list'] = $this->User->regOrg();
        $view=$this->User->userPrivilege($userGrp,'V'); 
        $data['viewPerm'] =$view->PERMSN_EXIST;
        $approve=$this->User->userPrivilege($userGrp,'A'); 
        $data['rejectPerm'] =$approve->PERMSN_EXIST;
        $data['content_view_page'] = 'admin/org/reg_list';
        $this->admin_template->display($data);
      }


       public function regIndividual(){
      $session_info = $this->session->userdata('logged_in');
      $userGrp=$session_info['USERGRP_ID'];
      $data['contentTitle'] = 'Registration Individual List';
      $data['breadcrumbs'] = array(
        'Admin' => '#',
        'Registered Individual List' => '#',
        );
        
        $data['regIndividual'] = $this->User->regIndividual(); 
        $view=$this->User->userPrivilege($userGrp,'V'); 
        $data['viewPerm'] =$view->PERMSN_EXIST;

        $approve=$this->User->userPrivilege($userGrp,'A'); 
        $data['rejectPerm'] =$approve->PERMSN_EXIST;// select all data from  faculty
        $data['content_view_page'] = 'admin/individual/regIndividual';
        $this->admin_template->display($data);
      }

         function ban_detail_individual($USER_ID) {
       // echo $USER_ID ;exit;
        $status= $this->db->query("UPDATE users SET active = 0 WHERE userId = $USER_ID");
         if ($status == TRUE) {
                   $this->session->set_flashdata('msg','<div class="alert alert-danger text-center">Individual Registered Is Banned<button data-dismiss="alert" class="close" type="button">×</button></div>');
                 }
         
        redirect('admin/regIndividual');

    }



      function regIndividual_detail($userId) {
        $data['regIndividual_detail'] = $this->db->query("SELECT us.*, ls.title as org_title, ls.latitude, ls.longitude, ul1.login_time 
                                    FROM users us 
                                    LEFT JOIN listing ls 
                                    ON ls.userId = us.userId
                                    LEFT JOIN user_login ul1
                                    ON us.userId = ul1.user_id
                                    LEFT OUTER JOIN user_login ul2
                                    ON us.userId = ul2.user_id
                                    AND (
                                          ul1.login_time < ul2.login_time 
                                       OR ul1.login_time = ul2.login_time AND ul1.id < ul2.id
                                      )
                                    WHERE ul2.id IS NULL AND us.active='1' and us.type='individual' and us.userId=$userId")->row();
        //echo "<pre>";print_r($data['visitor_info']);exit;
       // $this->load->view('user/user_details', $data);
        $data['content_view_page'] = 'admin/individual/regIndividual_detail';
        $this->admin_template->display($data);

    }



       public function delete_individual_from_db()
    {

        $userId = $this->input->post('userId');
        if ($this->db->query("DELETE FROM users WHERE userId = $userId")) {
            echo 1;
        } else {
            echo 0;
        }
    }



       public function delete_userList_from_db()
    {

        $userId = $this->input->post('userId');
        if ($this->db->query("DELETE FROM users WHERE userId = $userId")) {
            echo 1;
        } else {
            echo 0;
        }
    }


      public function delete_reg_org_from_db()
    {

        $mod_id = $this->input->post('mod_id');
        if ($this->db->query("DELETE FROM users WHERE userId = $mod_id")) {
            echo 1;
        } else {
            echo 0;
        }
    }





      public function reviewOrg(){
      $session_info = $this->session->userdata('logged_in');
      $userGrp=$session_info['USERGRP_ID'];     
      $data['contentTitle'] = 'User List';
      $data['breadcrumbs'] = array(
        'Admin' => '#',
        'User List' => '#',
        );
        
        $data['review_org_list'] = $this->User->review_org();
        $view=$this->User->userPrivilege($userGrp,'V'); 
        $data['viewPerm'] =$view->PERMSN_EXIST;
        $approve=$this->User->userPrivilege($userGrp,'A'); 
        $data['rejectPerm'] =$approve->PERMSN_EXIST;
        $data['content_view_page'] = 'admin/org/review_org';
        $this->admin_template->display($data);
      }



      public function inclusiveBd(){ 
      $session_info = $this->session->userdata('logged_in');
      $userGrp=$session_info['USERGRP_ID'];
      $data['contentTitle'] = 'Inclusive Bangladesh';
      $data['breadcrumbs'] = array(
        'Inclusive Bangladesh' => '#',
        'Inclusive Bangladesh' => '#',
        );
        
        $data['inclusive_bd_list'] = $this->User->inclusive_bd_list();
        $view=$this->User->userPrivilege($userGrp,'V'); 
        $data['viewPerm'] =$view->PERMSN_EXIST;
        $approve=$this->User->userPrivilege($userGrp,'A'); 
        $data['rejectPerm'] =$approve->PERMSN_EXIST;
        $data['content_view_page'] = 'admin/inclusivebd/inclusive_bd_list';
        $this->admin_template->display($data);
      }

      public function mediaBlog(){ 
      $session_info = $this->session->userdata('logged_in');
      $userGrp=$session_info['USERGRP_ID'];
      $data['contentTitle'] = 'Media Blog';
      $data['breadcrumbs'] = array(
        'Media Blog' => '#',
        'Media Blog' => '#',
        );
        
        $data['media_blog_list'] = $this->User->mediaBlogList();
        $view=$this->User->userPrivilege($userGrp,'V'); 
        $data['viewPerm'] =$view->PERMSN_EXIST;
        $approve=$this->User->userPrivilege($userGrp,'A'); 
        $data['rejectPerm'] =$approve->PERMSN_EXIST;
        $data['content_view_page'] = 'admin/media_blog/media_blog_list';
        $this->admin_template->display($data);
      }


      public function opportunityList(){ 
      $session_info = $this->session->userdata('logged_in');
      $userGrp=$session_info['USERGRP_ID'];
      $data['contentTitle'] = 'Opportunities';
      $data['breadcrumbs'] = array(
        'Opportunities' => '#',
        'Opportunities' => '#',
        );
        
        $data['opportunity_list'] = $this->User->opportunityList();
        $view=$this->User->userPrivilege($userGrp,'V'); 
        $data['viewPerm'] =$view->PERMSN_EXIST;
        $approve=$this->User->userPrivilege($userGrp,'A'); 
        $data['rejectPerm'] =$approve->PERMSN_EXIST;
        $data['content_view_page'] = 'admin/opportunities/opportunities_list';
        $this->admin_template->display($data);
      }


    public function eventList(){ 
      $session_info = $this->session->userdata('logged_in');
      $userGrp=$session_info['USERGRP_ID'];
      $data['contentTitle'] = 'Events';
      $data['breadcrumbs'] = array(
        'Events' => '#',
        'Events' => '#',
        );
        
        $data['event_list'] = $this->User->eventList();
        $view=$this->User->userPrivilege($userGrp,'V'); 
        $data['viewPerm'] =$view->PERMSN_EXIST;
        $approve=$this->User->userPrivilege($userGrp,'A'); 
        $data['rejectPerm'] =$approve->PERMSN_EXIST;
        $data['content_view_page'] = 'admin/events/events_list';
        $this->admin_template->display($data);
      }



     public function directoryList()
     { 
      $session_info = $this->session->userdata('logged_in');
      $userGrp=$session_info['USERGRP_ID'];
      $data['contentTitle'] = 'Organization Director';
      $data['breadcrumbs'] = array(
        'Organization Directory' => '#',
        'Organization Directory' => '#',
        );
        
        $data['directory_list'] = $this->User->directoryList();
        $view=$this->User->userPrivilege($userGrp,'V'); 
        $data['viewPerm'] =$view->PERMSN_EXIST;
        $approve=$this->User->userPrivilege($userGrp,'A'); 
        $data['rejectPerm'] =$approve->PERMSN_EXIST;
        $data['content_view_page'] = 'admin/directory/directory_list';
        $this->admin_template->display($data);
      }

        function directoryDetails() 
      {        
         $id= $this->input->post("id");
         $data['directorytDetails'] = $this->db->query("SELECT l.*,u.* ,oi.*FROM listing as l
                                                       left join users as u on u.userId=l.userId
                                                       left join organization_images as oi on oi.organization_id=l.id
                                                       where l.id=$id order by l.id desc
                                                   ")->row();
         $this->load->view('admin/directory/directory_detail', $data);
        
       }





      function inclusive_bd_approve_detail() 
      {        
         $inclusive_id= $this->input->post("id");
         $data['inclusive_bd_detail'] = $this->db->query("SELECT bg.*,u.fname,u.oname,u.userId,u.email,u.oemail,u.type,(SELECT COUNT(*) FROM inclusivebd_interest where inclusivebd_id=bg.id ) TOTAL_EMO, ut.name url_type, ut.value url_data,lk.* FROM inclusive_bd as bg
                                 LEFT JOIN url_type as ut ON ut.id = bg.url_type_id 
                                 left join users as u on u.userId=bg.created_by
                                 LEFT JOIN sa_lookup_data as lk ON lk.LOOKUP_DATA_ID = bg.category_id 
                                 where bg.id=$inclusive_id order by bg.id desc")->row();
         $this->load->view('admin/inclusivebd/inclusive_bd_approve_detail', $data);
        
       }


        function inclusive_bd_detail() 
      {        
         $inclusive_id= $this->input->post("id");
         $data['inclusive_bd_detail'] = $this->db->query("SELECT bg.*,u.fname,u.oname,u.userId,u.email,u.oemail,u.type,(SELECT COUNT(*) FROM inclusivebd_interest where inclusivebd_id=bg.id ) TOTAL_EMO, ut.name url_type, ut.value url_data,lk.* FROM inclusive_bd as bg
                                 LEFT JOIN url_type as ut ON ut.id = bg.url_type_id 
                                 LEFT JOIN inclusivebd_interest as ii ON ii.inclusivebd_id = bg.id 
                                 left join users as u on u.userId=bg.created_by
                                 LEFT JOIN sa_lookup_data as lk ON lk.LOOKUP_DATA_ID = bg.category_id 
                                 where bg.id=$inclusive_id order by bg.id desc")->row();
         $this->load->view('admin/inclusivebd/inclusive_bd_detail', $data);
        
       }


        function mediaBlogDetails() 
      {        
         $id= $this->input->post("id");
         $data['media_blog_detail'] = $this->db->query("SELECT md.*,l.title as Title,count(*) interest_count,ut.name url_type,u.oemail, ut.value url_data FROM blog as md
                                  left join listing as l on l.id=md.organization_id
                                  left join users as u on u.userId=l.userId
                                  LEFT JOIN url_type as ut ON ut.id = md.url_type_id 
                                  LEFT JOIN blog_interest as bi ON bi.blog_id = md.id 
                                  where md.id=$id order by md.id desc")->row();
         $this->load->view('admin/media_blog/media_blog_detail', $data);
        
       }

        function opportunityDetails() 
      {        
         $id= $this->input->post("id");
         $data['opportunity_detail'] = $this->db->query("SELECT op.*,l.title as Title,u.oemail,(SELECT COUNT(*) FROM opportunity_interest where opportunity_id=op.id ) TOTAL_EMO,(SELECT COUNT(*) FROM opportunity_interest where opportunity_id=op.id and type is null) TOTAL_INT FROM opportunity as op
                                     left join listing as l on l.id=op.organization_id
                                     left join users as u on u.userId=l.userId
                                     where op.id=$id order by op.id desc
                                     ")->row();
         $this->load->view('admin/opportunities/opportunities_detail', $data);
        
       }


           function eventDetails() 
      {        
         $id= $this->input->post("id");
         $data['eventDetails'] = $this->db->query("SELECT e.*,l.title as Title,u.oemail,(SELECT COUNT(*) FROM event_going where event_id=e.id ) TOTAL_GOING,(SELECT COUNT(*) FROM event_interest where event_id=e.id ) TOTAL_EMO,(SELECT COUNT(*) FROM event_interest where event_id=e.id and type is null) TOTAL_INT FROM events as e
                                                   left join listing as l on l.id=e.organization_id
                                                   left join users as u on u.userId=l.userId
                                                   where e.id=$id order by e.id desc
                                                   ")->row();
         $this->load->view('admin/events/events_detail', $data);
        
       }


            public function update_status1() {  //reazul
            $id = $this->input->post('id');
            
            $status= $this->db->query("UPDATE inclusive_bd SET active = 'Y' WHERE id = $id");
        if ($status) {
            echo "<div class='alert alert-success'>Approved successfully</div>";
        }

     
        else {
            echo "<div class='alert alert-danger'>Approved failed</div>";
        }
       
    }


            public function update_status() {  //reazul
            $id = $this->input->post('id');
            //$x = $_POST['active_status'];
           // echo $x; exit;


             $activeStatus = array(
            //'active_status' => isset($_POST['active_status']) ? 'Y' : 'N',
            'active_status' => $_POST['active_status']
            );
         $update = $this->utilities->updateData('inclusive_bd', $activeStatus, array("id" => $_POST['id']));
        if ($update) {
            echo "<div class='alert alert-success'>Status Changed successfully</div>";
        }

     
        else {
            echo "<div class='alert alert-danger'>Status Changed failed</div>";
        }
       
    }


     public function delete_inclusive_bd_from_db() 

    {

        $inclusive_id = $this->input->post('inclusive_id');
        if ($this->db->query("DELETE FROM inclusive_bd WHERE id = $inclusive_id")) {
            echo 1;
        } else {
            echo 0;
        }
    }

       public function deleteMediaBlog() 

    {

        $id = $this->input->post('id');
        if ($this->db->query("DELETE FROM blog WHERE id = $id")) {
            echo 1;
        } else {
            echo 0;
        }
    }


    public function deleteEvent() 

    {

        $id = $this->input->post('id');
        if ($this->db->query("DELETE FROM events WHERE id = $id")) {
            echo 1;
        } else {
            echo 0;
        }
    }

        public function deleteDirectory() 

    {

        $id = $this->input->post('id');
        if ($this->db->query("DELETE FROM listing WHERE id = $id")) {
            echo 1;
        } else {
            echo 0;
        }
    }



    function user_org_detail() {        
       $user_id= $this->input->post("orgId");
        $data['user_detail'] = $this->db->query("select u.* from users u
         
         WHERE u.userId= $user_id order by u.userId")->row();
        //echo "<pre>";print_r($data['user_detail']);exit;
        $this->load->view('admin/user/user_org_detail', $data);
        
    }




      function organization_detail() {       
        $user_id= $this->input->post("orgId");
        $data['organization_detail'] = $this->db->query("select u.* from users u
         
         WHERE u.userId= $user_id order by u.userId")->row();
        //echo "<pre>";print_r($data['user_detail']);exit;
        $this->load->view('admin/org/user_org_detail', $data);
        
    }



      function organization_detail_org() {        
       $user_id= $this->input->post("orgId");
       $data['organization_detail'] = $this->db->query("SELECT us.userId,us.fname,us.email,us.oemail,us.oname,us.website,ls.*,oi.* FROM users us LEFT JOIN listing ls ON ls.userId = us.userId
        left join organization_images as oi on oi.organization_id=ls.id
         WHERE us.active='1' AND us.userId=$user_id")->row();
        //echo "<pre>";print_r($data['user_detail']);exit;
       $this->load->view('admin/org/organization_detail', $data);
        
    }



         function ban_org_detail() {
       $user_id= $this->input->post("orgId");
        $data['ban_org_detail'] = $this->db->query("select u.* from users u
         
         WHERE u.userId= $user_id order by u.userId")->row();
        //echo "<pre>";print_r($data['user_detail']);exit;
        $this->load->view('admin/org/ban_org_detail', $data);
        
    }

        function ban_detail_organization($USER_ID) {
       // echo $USER_ID ;exit;
        $status= $this->db->query("UPDATE users SET active = 0 WHERE userId = $USER_ID");
         if ($status == TRUE) {
                   $this->session->set_flashdata('msg','<div class="alert alert-danger text-center">Organisation Registered Is Banned<button data-dismiss="alert" class="close" type="button">×</button></div>');
                 }
         
        redirect('admin/regOrg');

    }


      public function again_send_mail_ban_org() {  
        $USER_ID = $this->input->post('USERID');
        $userid= $_POST['USERID'];
        $oname = $this->input->post('FULL_NAME');
        $oemail = $this->input->post('USER_MAIL');
        $status= $this->db->query("UPDATE users SET active = 3 WHERE userId = $userid");
        $message = "Dear Friend,<br/><br>
        We have reviewed your profile and we are  inform you that your account has been Banned.<br>
        You are an official Youthopia.bangla banned now!<br>
        Your Information below:<br><br> website:www.Youthopia.bangla <br>Email-ID= " . $oemail ."<br>Organisation:
         " . $oname. " <br><br>
        Warm welcome!<br>
        Youthopia.bangla team";

        $subject = "Banned Confirmation";

             //echo $message;exit;
        require 'gmail_app/class.phpmailer.php';
        $mail = new PHPMailer;
        $mail->IsSMTP();
        $mail->Host = "mail.harnest.com";
        $mail->Port = "465";
        $mail->SMTPAuth = true;
        $mail->Username = "dev@atilimited.net";
        $mail->Password = "@ti321$#";
        $mail->SMTPSecure = 'ssl';
        $mail->From = "noreply@youthopiabangla.org";
        $mail->FromName = "Youthopia.bangla";
        $mail->AddAddress($oemail);
             //$mail->AddReplyTo($emp_info->EMPLOYEE);
        $mail->IsHTML(TRUE);
        $mail->Subject = $subject;
        $mail->Body = $message;
      
        if ($mail->Send()){
     
            echo "<div class='alert alert-success'>Mail send successfully</div>";
        }
        else {
            echo "<div class='alert alert-danger'>Mail send failed</div>";
        }
       
    }



         public function again_send_mail_ban_individual() {  
        $USER_ID = $this->input->post('USERID');
        $userid= $_POST['USERID'];
        $fname = $this->input->post('FULL_NAME');
        $email = $this->input->post('USER_MAIL');
        $status= $this->db->query("UPDATE users SET active = 3 WHERE userId = $userid");
        $message = "Dear Friend,<br/><br>
        We have reviewed your profile and we are  inform you that your account has been Banned.<br>
        You are an official Youthopia.bangla banned now!<br>
        Your Information below:<br><br> website:www.Youthopia.bangla <br>Email-ID= " . $email ."<br>Individual:
         " . $fname. " <br><br>
        Warm welcome!<br>
        Youthopia.bangla team";

        $subject = "Banned Confirmation";

             //echo $message;exit;
        require 'gmail_app/class.phpmailer.php';
        $mail = new PHPMailer;
        $mail->IsSMTP();
        $mail->Host = "mail.harnest.com";
        $mail->Port = "465";
        $mail->SMTPAuth = true;
        $mail->Username = "dev@atilimited.net";
        $mail->Password = "@ti321$#";
        $mail->SMTPSecure = 'ssl';
        $mail->From = "noreply@youthopiabangla.org";
        $mail->FromName = "Youthopia.bangla";
        $mail->AddAddress($email);
             //$mail->AddReplyTo($emp_info->EMPLOYEE);
        $mail->IsHTML(TRUE);
        $mail->Subject = $subject;
        $mail->Body = $message;
      
        if ($mail->Send()){
     
            echo "<div class='alert alert-success'>Mail send successfully</div>";
        }
        else {
            echo "<div class='alert alert-danger'>Mail send failed</div>";
        }
       
    }



    public function delete_review_org_from_db() 

    {

        $org_id = $this->input->post('org_id');
        if ($this->db->query("DELETE FROM users WHERE userId = $org_id")) {
            echo 1;
        } else {
            echo 0;
        }
    }


       function user_detail($USER_ID) { 
        $data['user_detail'] = $this->db->query("select u.* from users u
         
         WHERE u.userId= $USER_ID order by u.userId")->row();
        //echo "<pre>";print_r($data['visitor_info']);exit;
       // $this->load->view('user/user_details', $data);
        $data['content_view_page'] = 'admin/user/user_details';
        $this->admin_template->display($data);


    }


       function userModule($userId) { 

        if(isset($_POST['userId']))
        {

          $session_info = $this->session->userdata('logged_in');
          $groupId=$session_info['USERGRP_ID'];
          $adminPrev=$this->db->query("SELECT * FROM front_mod_usergrp WHERE USERGRP_ID=$groupId")->result();
          foreach($adminPrev as $ap)
          {
            $this->db->where('USER_ID', $userId);
            $this->db->where('MODULE_ID', $ap->MODULE_ID);
            $this->db->delete('front_user_permission');
          }

          $this->db->where('USER_ID', $userId);
          $this->db->delete('front_user_permission');
          $moduleId=$this->input->post('moduleId');
          foreach($moduleId as $key=>$value)
          {
            $attr = array(
                "USER_ID" => $userId,
                "MODULE_ID" =>$moduleId[$key],
            );
            $this->utilities->insertData($attr, "front_user_permission");
          }
          redirect('admin/allUserList');
        }
        $session_info = $this->session->userdata('logged_in');
        $userGrp=$session_info['USERGRP_ID'];
        $modules=$this->db->query("select MODULE_ID,MODULE_NAME from front_mod_usergrp
                          LEFT JOIN front_end_module USING(MODULE_ID)
                          WHERE USERGRP_ID=$userGrp")->result();
        $pervAssignModule=$this->db->query("SELECT * FROM front_user_permission WHERE USER_ID=$userId;")->result();
        $m=$data['modules']=$modules;
        $data['userId']=$userId;
        $data['prevModule']=$pervAssignModule;
        //$this->pr($modules);


        echo $this->load->view("admin/user/user_module", $data, true);


    }

       function ban_detail($USER_ID) {
       // echo $USER_ID ;exit;
         $this->db->query("UPDATE users SET active = 0 WHERE userId = $USER_ID");
         
        redirect('admin/regOrg');

    }
    private function pr($data)
    {
      echo "<pre>";
      print_r($data);
      exit;
    }

      public function again_send_mail_regIndividual() {  //reazul
        $USER_ID = $this->input->post('USERID');
        $fname = $this->input->post('FULL_NAME');
        $email = $this->input->post('USER_MAIL');

        $message = "Dear $fname,<br><br>Welcome to the Youthopia.bangla community!<br><br> You are one step away to become an official member. Please, follow the link below to activate your account. If you cannot click the URL<br> below, please copy and paste it into your web browser.  <br><br>" . base_url("portal/againUserActivition/$USER_ID") . " <br><br>After that, you can immediately start using your new credentials on www.youthopiabangla.org<br><br>Email ID= " . $email . ' <br><br> Welcome aboard! <br>Youthopia.bangla team';


        $subject = "Account verification";

             //echo $message;exit;
        require 'gmail_app/class.phpmailer.php';
        $mail = new PHPMailer;
        $mail->IsSMTP();
        $mail->Host = "mail.harnest.com";
        $mail->Port = "465";
        $mail->SMTPAuth = true;
        $mail->Username = "dev@atilimited.net";
        $mail->Password = "@ti321$#";
        $mail->SMTPSecure = 'ssl';
        $mail->From = "noreply@youthopiabangla.org";
        $mail->FromName = "Youthopia.bangla";
        $mail->AddAddress($email);
             //$mail->AddReplyTo($emp_info->EMPLOYEE);
        $mail->IsHTML(TRUE);
        $mail->Subject = $subject;
        $mail->Body = $message;
        if ($mail->Send()){
     
            echo "<div class='alert alert-success'>Mail send successfully</div>";
        }
        else {
            echo "<div class='alert alert-danger'>Mail send failed</div>";
        }
       
    }



     public function again_send_mail_regSubscriber() {  //reazul
        $USER_ID = $this->input->post('USERID');
        $oname = $this->input->post('FULL_NAME');
        $oemail = $this->input->post('USER_MAIL');

         $message = "Dear Friend,<br/><br>
        We have reviewed your profile and we are glad to inform you that your account has been activated.<br>
        You are an official Youthopia.bangla member now!<br>
        Please find  your log-in credentials below:<br><br> website: ". base_url("portal/againUserActivitionByOrgSubscriber/$USER_ID") ." <br>Email-ID= " . $oemail ."<br><br>
        Youthopia.bangla team strongly recommends to finalize your profile as soon as possible. Only after completing 100% of your profile, " . $oname. " will be included in the Organisation Directory module and you will be able to access all the tools available on the web portal. You can start using your new credentials on our Website.<br><br>
        Looking forward to your active participation,<br/><br>
        Warm welcome!<br>
        Youthopia.bangla team";

        $subject = "Approval Confirmation";

             //echo $message;exit;
        require 'gmail_app/class.phpmailer.php';
        $mail = new PHPMailer;
        $mail->IsSMTP();
        $mail->Host = "mail.harnest.com";
        $mail->Port = "465";
        $mail->SMTPAuth = true;
        $mail->Username = "dev@atilimited.net";
        $mail->Password = "@ti321$#";
        $mail->SMTPSecure = 'ssl';
        $mail->From = "noreply@youthopiabangla.org";
        $mail->FromName = "Youthopia.bangla";
        $mail->AddAddress($oemail);
             //$mail->AddReplyTo($emp_info->EMPLOYEE);
        $mail->IsHTML(TRUE);
        $mail->Subject = $subject;
        $mail->Body = $message;
        if ($mail->Send()){
     
            echo "<div class='alert alert-success'>Mail send successfully</div>";
        }
        else {
            echo "<div class='alert alert-danger'>Mail send failed</div>";
        }
       
    }

     public function again_send_mail_review() {  //reazul
        $USER_ID = $this->input->post('USERID');
        $oname = $this->input->post('FULL_NAME');
        $oemail = $this->input->post('USER_MAIL');

         $message = "Dear Friend,<br/><br>
        We have reviewed your profile and we are glad to inform you that your account has been activated.<br>
        You are an official Youthopia.bangla member now!<br>
        Please find  your log-in credentials below:<br><br> website: ". base_url("portal/userActivition/$USER_ID") ." <br>Email-ID= " . $oemail ."<br><br>
        Youthopia.bangla team strongly recommends to finalize your profile as soon as possible. Only after completing 100% of your profile, " . $oname. " will be included in the Organisation Directory module and you will be able to access all the tools available on the web portal. You can start using your new credentials on our Website.<br><br>
        Looking forward to your active participation,<br/><br>
        Warm welcome!<br>
        Youthopia.bangla team";

        $subject = "Approval Confirmation";

             //echo $message;exit;
        require 'gmail_app/class.phpmailer.php';
        $mail = new PHPMailer;
        $mail->IsSMTP();
        $mail->Host = "mail.harnest.com";
        $mail->Port = "465";
        $mail->SMTPAuth = true;
        $mail->Username = "dev@atilimited.net";
        $mail->Password = "@ti321$#";
        $mail->SMTPSecure = 'ssl';
        $mail->From = "noreply@youthopiabangla.org";
        $mail->FromName = "Youthopia.bangla";
        $mail->AddAddress($oemail);
             //$mail->AddReplyTo($emp_info->EMPLOYEE);
        $mail->IsHTML(TRUE);
        $mail->Subject = $subject;
        $mail->Body = $message;
        if ($mail->Send()){
     
            echo "<div class='alert alert-success'>Mail send successfully</div>";
        }
        else {
            echo "<div class='alert alert-danger'>Mail send failed</div>";
        }
       
    }


       


        public function update_visitor() {  //reazul
        $USER_ID = $this->input->post('USER_ID');
        $oname = $this->input->post('FULL_NAME');
        $oemail = $this->input->post('USER_MAIL');

        $message = "Dear Friend,<br/><br>
        We have reviewed your profile and we are glad to inform you that your account has been activated.<br>
        You are an official Youthopia.bangla member now!<br>
        Please find  your log-in credentials below:<br><br> website: ". base_url("portal/userActivitionOrg/$USER_ID") ." <br>Email-ID= " . $oemail ."<br><br>
        Youthopia.bangla team strongly recommends to finalize your profile as soon as possible. Only after completing 100% of your profile, " . $oname. " will be included in the Organisation Directory module and you will be able to access all the tools available on the web portal. You can start using your new credentials on our Website.<br><br>
        Looking forward to your active participation,<br/><br>
        Warm welcome!<br>
        Youthopia.bangla team";

        $subject = "Approval Confirmation";

             //echo $message;exit;
        require 'gmail_app/class.phpmailer.php';
        $mail = new PHPMailer;
        $mail->IsSMTP();
        $mail->Host = "mail.harnest.com";
        $mail->Port = "465";
        $mail->SMTPAuth = true;
        $mail->Username = "dev@atilimited.net";
        $mail->Password = "@ti321$#";
        $mail->SMTPSecure = 'ssl';
        $mail->From = "noreply@youthopiabangla.org";
        $mail->FromName = "Youthopia.bangla";
        $mail->AddAddress($oemail);
             //$mail->AddReplyTo($emp_info->EMPLOYEE);
        $mail->IsHTML(TRUE);
        $mail->Subject = $subject;
        $mail->Body = $message;
        if ($mail->Send()){
             $activeStatus = array(
            'active' => isset($_POST['ACTIVE_FLAG']) ? 2 : 0,
            //'MODULE_ID' =>'2',
            );
            $data = array(
            //'active' => isset($_POST['ACTIVE_FLAG']) ? 2 : 0,
             'MODULE_ID' =>'2',
             'USER_ID' =>$USER_ID,
            );
         //$insert = $this->utilities->insertData('front_user_permission', $activeStatus1, array("userId" => $_POST['USER_ID']));
           $dataInsert = $this->utilities->insertData($data, 'front_user_permission');
           $data1 = array(
            //'active' => isset($_POST['ACTIVE_FLAG']) ? 2 : 0,
             'MODULE_ID' =>'3',
             'USER_ID' =>$USER_ID,
            );
         //$insert = $this->utilities->insertData('front_user_permission', $activeStatus1, array("userId" => $_POST['USER_ID']));
         $dataInsert = $this->utilities->insertData($data1, 'front_user_permission');
         $update = $this->utilities->updateData('users', $activeStatus, array("userId" => $_POST['USER_ID']));
         
        if ($update) {
            echo "<div class='alert alert-success'>Approved successfully</div>";
        }

        }
        else {
            echo "<div class='alert alert-danger'>Approved failed</div>";
        }
       
    }

     



      public function userListManage(){
      $data['contentTitle'] = 'User List';
      $data['breadcrumbs'] = array(
        'Admin' => '#',
        'User List' => '#',
        );
        
        $data['userList'] = $this->User->userList(); // select all data from  faculty
        $data['content_view_page'] = 'admin/user/userList';
        $this->admin_template->display($data);
      }



      public function delete_user_from_db()
    {

        $admin_id = $this->input->post('admin_id');
        if ($this->db->query("DELETE FROM admin WHERE admin_id = $admin_id")) {
            echo 1;
        } else {
            echo 0;
        }
    }


           public function adduserManager()
    {
        $this->form_validation->set_rules('admin_username', 'User Name', 'required');
        $this->form_validation->set_rules('admin_pass', 'Password', 'required');
        $this->form_validation->set_rules('admin_email', 'Email', 'trim|required|valid_email');
            if ($this->form_validation->run() == FALSE) {
              //$data['pageTitle'] = "Add Page";
              $data['content_view_page'] = 'admin/user/adduser';
              $this->admin_template->display($data);
            }else {
            //$titles = count($this->input->post('title'));
            $admin_username    = $this->input->post('admin_username');
            $admin_email = $this->input->post('admin_email');
            $admin_type = $this->input->post('admin_type');
            $admin_pass =md5($this->input->post('admin_pass'));
            $status = $this->input->post('status');
            


            
            $data = array(
                'admin_username' => $admin_username,
                'admin_email' => $admin_email,
                'admin_type' => $admin_type,
                'admin_pass' => $admin_pass,
                'status' => $status
                
            );
            // print_r($data);
            // exit;
            
            //$data['IMAGE_PATH'] = 'asdasdsad';
            
             if($data != ""){
                  $dataInsert = $this->utilities->insertData($data, 'admin');
                  if ($dataInsert == TRUE) {
                   $this->session->set_flashdata('Success','User Added Successfully.');
                 }
               }
               else{
                $this->session->set_flashdata('Error', 'User Not Added Successfully!.');
              }
            
        
        
        redirect('admin/adduserManager', 'refresh');
        }
    }



    public function createUserManager() {
            $user = array(
                'admin_username' => $this->input->post('admin_username'),
                'admin_email' => $this->input->post('admin_email'),
                'admin_type' => $this->input->post('admin_type'),
                'admin_pass' => $this->input->post('admin_pass'),
                'status' => $this->input->post('status'),
                
            );
            if ($this->utilities->insertData($user, 'admin')) {
                $this->session->set_flashdata('Success', 'New User Added Successfully.');
                redirect('admin/user/adduser');
            }
        }




    /**
     * @methodName  users()
     * @access
     * @param
     * @author      Rakib Roni <rakib@atilimited.net>
     * @return      all staff list
     */
    function users()
    {

        $data['contentTitle'] = 'User List';
        $data["breadcrumbs"] = array(
            "Admin" => "admin/index",
            "User List" => '#'
        );
        $data['pageTitle'] = 'User List';
        $data['users'] = $this->utilities->findAllByAttributeWithJoin("sa_users", "designations", "DESIGNATION_ID", "DESIGNATION_ID", "DESIGNATION");
        $data["groups"] = $this->utilities->dropdownFromTableWithCondition("sa_user_group", "Select A Group", "USERGRP_ID", "USERGRP_NAME", array("ORG_ID" => 1, "ACTIVE_STATUS" => 1)); // ORG_ID should be session value
        $data["departments"] = $this->utilities->dropdownFromTableWithCondition("department", "Select Department", "DEPT_ID", "DEPT_NAME", array("ACTIVE_STATUS" => 1)); // All departments value
        $data["fields"] = $this->db->field_data('sa_users');

        $data['content_view_page'] = 'admin/users';
        $this->admin_template->display($data);
    }

    /**
     * @methodName  get_userLevelByGroup()
     * @access
     * @param
     * @author      Nurullah<nurul@atilimited.net>
     * @return      group wise user Level list
     */
    function get_userLevelByGroup()
    {
        $group_id = $_POST['groupId'];
        $query = $this->utilities->findAllByAttribute('sa_ug_level', array("USERGRP_ID" => $group_id, "ACTIVE_STATUS" => 1));
        $returnVal = '<option value="">Select One</option>';
        if (!empty($query)) {
            foreach ($query as $row) {
                $returnVal .= '<option value="' . $row->UG_LEVEL_ID . '">' . $row->UGLEVE_NAME . '</option>';
            }
        }
        echo $returnVal;
    }

    /**
     * @methodName  get_designationByDept()
     * @access
     * @param
     * @author      Nurullah<nurul@atilimited.net>
     * @return      Department wise Designation list
     */
    function get_designationByDept()
    {
        $dept_id = $_POST['dept_id'];
        $query = $this->utilities->findAllByAttribute('designations', array("DEPT_ID" => $dept_id, "ACTIVE_STATUS" => 1));
        $returnVal = '<option value="">Select One</option>';
        if (!empty($query)) {
            foreach ($query as $row) {
                $returnVal .= '<option value="' . $row->DESIGNATION_ID . '">' . $row->DESIGNATION . '</option>';
            }
        }
        echo $returnVal;
    }

    /**
     * @methodName  searchUserList()
     * @access
     * @param
     * @author      Nurullah<nurul@atilimited.net>
     * @return      Submit (Group id, user level, department id, gender)---> user list
     */
    function searchUserList()
    {
        $group_id = $_POST['cmbGroup'];
        $userLevel_id = $_POST['userLevel'];
        $department_id = $_POST['cmbDepartment'];
        $designation_id = $_POST['userDesignation'];
        $gender_id = $_POST['userGender'];
        $data["filtered_result"] = $this->utilities->userlist($group_id, $userLevel_id, $department_id, $designation_id, $gender_id);
        echo $this->load->view("admin/user_list", $data, true);
    }

    /**
     * @methodName  advSearchUserList()
     * @access
     * @param
     * @author      Nurullah<nurul@atilimited.net>
     * @return      Submit (fild Name, operator, Keyword)---> user list
     */
    function advSearchUserList()
    {
        $fild_name = $_POST['fildName'];
        $operator = $_POST['operator'];
        $textKeyword = $_POST['textKeyword'];
        $operatorType = $_POST['operatorType'];
        if ($operatorType == 1) {
            $data["filtered_result"] = $this->db->query("SELECT u.USER_ID, u.USER_IMG, u.FULL_NAME, u.MOBILE,u.EMAIL,(SELECT d.DESIGNATION FROM designations d WHERE d.DESIGNATION_ID = u.DESIGNATION_ID)DESIGNATION"
                . " FROM sa_users u WHERE u.$fild_name $operator '$textKeyword%'")->result();
            echo $this->load->view("admin/user_list", $data, true);
        } elseif ($operatorType == 2) {
            $data["filtered_result"] = $this->db->query("SELECT u.USER_ID, u.USER_IMG, u.FULL_NAME, u.MOBILE,u.EMAIL,(SELECT d.DESIGNATION FROM designations d WHERE d.DESIGNATION_ID = u.DESIGNATION_ID)DESIGNATION"
                . " FROM sa_users u WHERE u.$fild_name $operator '%$textKeyword%'")->result();
            echo $this->load->view("admin/user_list", $data, true);
        } else {
            $data["filtered_result"] = $this->db->query("SELECT u.USER_ID, u.USER_IMG, u.FULL_NAME, u.MOBILE,u.EMAIL,(SELECT d.DESIGNATION FROM designations d WHERE d.DESIGNATION_ID = u.DESIGNATION_ID)DESIGNATION"
                . " FROM sa_users u WHERE u.$fild_name $operator '$textKeyword'")->result();
            echo $this->load->view("admin/user_list", $data, true);
        }
    }

    /**
     * @methodName  addUser()
     * @access
     * @param
     * @author      Rakib Roni <rakib@atilimited.net>
     * @return      Insert a new user
     */
    function addUser()
    {
        $data['contentTitle'] = 'Add New User';
        $data["breadcrumbs"] = array(
            "Admin" => "admin/index",
            "Add  User" => '#'
        );

        $data['pageTitle'] = 'Add User';
        $this->form_validation->set_rules('USERNAME', 'user name', 'required');
        $this->form_validation->set_rules('USERPW', 'password', 'required');
        $this->form_validation->set_rules('DEPT_ID', 'department ', 'required');
        $this->form_validation->set_rules('user_group', 'user group ', 'required');
        $this->form_validation->set_rules('FIRST_NAME', 'first name ', 'required');
        $this->form_validation->set_rules('LAST_NAME', 'last name', 'required');
        $this->form_validation->set_rules('EMAIL', 'email', 'trim|required|valid_email');
        $this->form_validation->set_rules('NID', 'national id', 'required');
        $this->form_validation->set_rules('DOB', 'date of birth', 'required');
        $this->form_validation->set_rules('GENDER', 'gender', 'required');
        $this->form_validation->set_rules('HIRE_DATE', 'hire date', 'required');
        $this->form_validation->set_rules('MOBILE', 'mobile', 'required');
        $this->form_validation->set_rules('RELIGION', 'religion', 'required');
        $this->form_validation->set_rules('NATIONALITY', 'nationality', 'required');
        $this->form_validation->set_rules('PRE_ADDRESS', 'present address', 'required');
        $this->form_validation->set_rules('PER_ADDRESS', 'permanent address', 'required');
//$this->form_validation->set_rules('photo', 'photo', 'required');

        if ($this->form_validation->run() == FALSE) {
            $session_info = $this->session->userdata('logged_in');
            $data['nationality'] = $this->utilities->getAll('country');
            $data['religion'] = $this->utilities->findAllByAttribute('m00_lkpdata', array('GRP_ID' => 3));
            $data['blood_group'] = $this->utilities->findAllByAttribute('m00_lkpdata', array('GRP_ID' => 4));
            $data['user_group'] = $this->utilities->findAllByAttribute('sa_user_group', array('ORG_ID' => $session_info['ORG_ID']));
            $data['faculty'] = $this->utilities->findAllByAttribute("faculty", array("ACTIVE_STATUS" => 1));
            $data['designations'] = $this->utilities->findAllByAttribute('designations', array('ORG_ID' => $session_info['ORG_ID']));
            $data['content_view_page'] = 'admin/add_user';
        } else {
            $is_duplicate = $this->utilities->hasInformationByThisId('sa_users', array('USERNAME' => $this->input->post('USERNAME')));
            if ($is_duplicate > 0) {
                $this->session->set_flashdata('Error', 'Sorry this user already exits !');
                redirect('admin/addUser', 'refresh');
            } else {
// $files = $_FILES;
// print_r($files);exit;

                if (!empty($_FILES)) {
                    $this->load->library('upload');
                    $this->load->helper('string');
                    $config['upload_path'] = 'upload/';
//$config['allowed_types'] = '*';
                    $config['allowed_types'] = 'gif|jpg|jpeg|png';
                    $config['overwrite'] = false;
                    $config['remove_spaces'] = true;
//$config['max_size']	= '100';// in KB
                    $this->upload->initialize($config);
                    if ($this->upload->do_upload('photo')) {
                        $file_data = $this->upload->data();
                        $file_name = $file_data['file_name'];
//$this->utilities->insertData($data, 'profile_picture');
                        $add_file = array(
                            'FIRST_NAME' => $this->input->post('FIRST_NAME'),
                            'MIDDLE_NAME' => $this->input->post('MIDDLE_NAME'),
                            'LAST_NAME' => $this->input->post('LAST_NAME'),
                            'FULL_NAME' => $this->input->post('FIRST_NAME') . ' ' . $this->input->post('MIDDLE_NAME') . ' ' . $this->input->post('LAST_NAME'),
                            'EMAIL' => $this->input->post('EMAIL'),
                            'ALT_EMAIL' => $this->input->post('ALT_EMAIL'),
                            'NID' => $this->input->post('NID'),
                            'DOB' => date('Y-m-d', strtotime($this->input->post('DOB'))),
                            'AGE' => $this->input->post('AGE'),
                            'BLOOD_GROUP' => $this->input->post('BLOOD_GROUP'),
                            'HEIGHT_FEET' => $this->input->post('HEIGHT_FEET'),
                            'HEIGHT_CM' => $this->input->post('HEIGHT_CM'),
                            'WEIGHT_KG' => $this->input->post('WEIGHT_KG'),
                            'WEIGHT_LBS' => $this->input->post('WEIGHT_LBS'),
                            'GENDER' => $this->input->post('GENDER'),
                            'RELIGION' => $this->input->post('RELIGION'),
                            'NATIONALITY' => $this->input->post('NATIONALITY'),
                            'HIRE_DATE' => date('Y-m-d', strtotime($this->input->post('HIRE_DATE'))),
                            'USER_IMG' => $file_name,
                            'FATHERS_NAME' => $this->input->post('FATHERS_NAME'),
                            'MOTHERS_NAME' => $this->input->post('MOTHERS_NAME'),
                            'HOME_PHONE' => $this->input->post('HOME_PHONE'),
                            'MOBILE' => $this->input->post('MOBILE'),
                            'MARITAL_STATUS' => ((isset($_POST['MARITAL_STATUS'])) ? 1 : 0),
                            'SPOUSE_NAME' => $this->input->post('SPOUSE_NAME'),
                            'PLACE_OF_BIRTH' => $this->input->post('PLACE_OF_BIRTH'),
                            'PASSPORT_NO' => $this->input->post('PASSPORT_NO'),
                            'DATE_OF_ISSUE' => date('Y-m-d', strtotime($this->input->post('DATE_OF_ISSUE'))),
                            'EXPIRE_DATE' => date('Y-m-d', strtotime($this->input->post('EXPIRE_DATE'))),
                            'PLACE_OF_ISSUE' => $this->input->post('PLACE_OF_ISSUE'),
                            'PRE_ADDRESS' => $this->input->post('PRE_ADDRESS'),
                            'PER_ADDRESS' => $this->input->post('PER_ADDRESS'),
                            'CONTACT_PERSON' => $this->input->post('CONTACT_PERSON'),
                            'CONTACT_PERSON_ADD' => $this->input->post('CONTACT_PERSON_ADD'),
                            'CONTACT_PERSON_PHN' => $this->input->post('CONTACT_PERSON_PHN'),
                            'RELATION' => $this->input->post('RELATION'),
                            'USERGRP_ID' => $this->input->post('user_group'),
                            'USERLVL_ID' => $this->input->post('user_group_lavel'),
                            'FACULTY_ID' => $this->input->post('FACULTY_ID'),
                            'DEPT_ID' => $this->input->post('DEPT_ID'),
                            'DESIGNATION_ID' => $this->input->post('designation'),
                            'BIOMETRIC_ID' => $this->input->post('BIOMETRIC_ID'),
                            'OFFICIAL_PHONE_NO' => $this->input->post('OFFICIAL_PHONE_NO'),
                            'OFFICIAL_PHONE_EXTENSION' => $this->input->post('OFFICIAL_PHONE_EXTENSION'),
                            'USERNAME' => $this->input->post('USERNAME'),
                            'USERPW' => $this->input->post('USERPW')
                        );

                        $emp_id = $this->utilities->insert('sa_users', $add_file);
                        $session_info = $this->session->userdata('logged_in');
                        $ALT_MOBILE = $this->input->post('ALT_MOBILE');

                        if ($ALT_MOBILE) {
                            foreach ($ALT_MOBILE as $key => $value) {
                                $insert_mobile = array(
                                    'CONTACT_INFO' => $ALT_MOBILE [$key],
                                    'CONTACT_TYPE' => 'M',
                                    'EMP_ID' => $emp_id,
                                    'ORG_ID' => $session_info["ORG_ID"],
                                    'DEFAULT_FG' => 1,
                                    'ACTIVE_STATUS' => 1
                                );

                                $this->utilities->insertData($insert_mobile, 'hr_emp_cinfo');
                            }
                        }
                        $this->session->set_flashdata('Success', 'Congratulation ! User Information Added Successfully.');
                        redirect('admin/users', 'refresh');
                    }
                }
            }
        }
        $this->admin_template->display($data);
    }

    /**
     * @methodName  userGroupByOrgId()
     * @access
     * @param
     * @author      Rakib Roni <rakib@atilimited.net>
     * @return      organization wise user groups
     */
    function userGroupByOrgId()
    {
        $org_id = $_POST['org_id'];
        $data['user_groups'] = $this->utilities->findAllByAttribute('sa_user_group', array('ORG_ID' => $org_id));
        $this->load->view('admin/ajax_ug_list', $data);
    }

    /**
     * @methodName  viewUser()
     * @access
     * @param
     * @author      Rakib Roni <rakib@atilimited.net>
     * @return      view user details in modal
     */
    function viewUser()
    {
        $user_id = $_POST['param'];
        $data['user_details'] = $this->db->query("SELECT u.*,
            f.FACULTY_NAME AS faculty,
            de.DEPT_NAME as department,
            g.USERGRP_NAME AS USER_GRP_NAME,
            l.UGLEVE_NAME AS USER_GRP_LVL_NAME,
            d.DESIGNATION AS DESIGNATION,
            c.nationality AS NATIONALITES
            FROM sa_users u
            LEFT JOIN faculty f ON u.FACULTY_ID = f.FACULTY_ID
            LEFT JOIN department de ON u.DEPT_ID = de.DEPT_ID
            LEFT JOIN sa_user_group g ON u.USERGRP_ID = g.USERGRP_ID
            LEFT JOIN sa_ug_level l ON u.USERLVL_ID = l.UG_LEVEL_ID
            LEFT JOIN designations d ON u.DESIGNATION_ID = d.DESIGNATION_ID
            LEFT JOIN country c ON u.NATIONALITY = c.id
            WHERE u.USER_ID = $user_id")->row();
        $this->load->view('admin/user_details', $data);
    }

    /**
     * @methodName  editUser()
     * @access
     * @param
     * @author      Rakib Roni <rakib@atilimited.net>
     * @return      edit user's previous data
     */
    // function editUser($id)
    // {
    //     $data['contentTitle'] = 'Edit User';
    //     $data["breadcrumbs"] = array(
    //         "Admin" => "admin/index",
    //         "Edit  User" => '#'
    //     );
    //     $USER_ID = $id;
    //     $previous_info = $this->utilities->findByAttribute('sa_users', array('USER_ID' => $USER_ID));
    //     $data['previous_info'] = $previous_info;
    //     $session_info = $this->session->userdata('logged_in');
    //     $data['religion'] = $this->utilities->findAllByAttribute('m00_lkpdata', array('GRP_ID' => 3));
    //     $data['blood_group'] = $this->utilities->findAllByAttribute('m00_lkpdata', array('GRP_ID' => 4));
    //     $data['nationality'] = $this->utilities->getAll('country');
    //     $data['group_lavel'] = $this->utilities->getAll('sa_ug_level');
    //     $data['faculty'] = $this->utilities->findAllByAttribute("faculty", array("ADMINISTRATION" => 1));
    //     $data['department'] = $this->utilities->getAll('department');
    //     $data['mobiles'] = $this->utilities->findAllByAttribute('hr_emp_cinfo', array('EMP_ID' => $USER_ID));
    //     $data['user_group'] = $this->utilities->findAllByAttribute('sa_user_group', array('ORG_ID' => $session_info['ORG_ID']));
    //     $data['section'] = $this->utilities->findAllByAttribute('sa_org_sections', array('ORG_ID' => $session_info['ORG_ID']));
    //     $data['designations'] = $this->utilities->findAllByAttribute('designations', array('ORG_ID' => $session_info['ORG_ID']));
    //     $data['content_view_page'] = 'admin/edit_user';
    //     $this->admin_template->display($data);
    // }

    /**
     * @methodName  updateUser()
     * @access
     * @param
     * @author      Rakib Roni <rakib@atilimited.net>
     * @return      update user information
     */
    function updateUser1()
    {
//        $files = $_FILES;
//         print_r();exit;
        $file_name = "";
        if (!empty($_FILES)) {
            $this->load->library('upload');
            $this->load->helper('string');
            $config['upload_path'] = 'upload/';
//$config['allowed_types'] = '*';
            $config['allowed_types'] = 'gif|jpg|jpeg|png';
            $config['overwrite'] = false;
            $config['remove_spaces'] = true;
//$config['max_size']	= '100';// in KB
            $this->upload->initialize($config);
            if ($this->upload->do_upload('photo')) {
                $file_data = $this->upload->data();
                $file_name = $file_data['file_name'];
//$this->utilities->insertData($data, 'profile_picture');
            }
        }
        $update_data = array(
            'FIRST_NAME' => $this->input->post('FIRST_NAME'),
            'MIDDLE_NAME' => $this->input->post('MIDDLE_NAME'),
            'LAST_NAME' => $this->input->post('LAST_NAME'),
            'FULL_NAME' => $this->input->post('FIRST_NAME') . ' ' . $this->input->post('MIDDLE_NAME') . ' ' . $this->input->post('LAST_NAME'),
            'EMAIL' => $this->input->post('EMAIL'),
            'ALT_EMAIL' => $this->input->post('ALT_EMAIL'),
            'NID' => $this->input->post('NID'),
            'DOB' => date('Y-m-d', strtotime($this->input->post('DOB'))),
            'AGE' => $this->input->post('AGE'),
            'BLOOD_GROUP' => $this->input->post('BLOOD_GROUP'),
            'HEIGHT_FEET' => $this->input->post('HEIGHT_FEET'),
            'HEIGHT_CM' => $this->input->post('HEIGHT_CM'),
            'WEIGHT_KG' => $this->input->post('WEIGHT_KG'),
            'WEIGHT_LBS' => $this->input->post('WEIGHT_LBS'),
            'GENDER' => $this->input->post('GENDER'),
            'RELIGION' => $this->input->post('RELIGION'),
            'NATIONALITY' => $this->input->post('NATIONALITY'),
            'HIRE_DATE' => date('Y-m-d', strtotime($this->input->post('HIRE_DATE'))),
            'FATHERS_NAME' => $this->input->post('FATHERS_NAME'),
            'MOTHERS_NAME' => $this->input->post('MOTHERS_NAME'),
            'HOME_PHONE' => $this->input->post('HOME_PHONE'),
            'MOBILE' => $this->input->post('MOBILE'),
            'MARITAL_STATUS' => ((isset($_POST['MARITAL_STATUS'])) ? 1 : 0),
            'SPOUSE_NAME' => $this->input->post('SPOUSE_NAME'),
            'PLACE_OF_BIRTH' => $this->input->post('PLACE_OF_BIRTH'),
            'PASSPORT_NO' => $this->input->post('PASSPORT_NO'),
            'DATE_OF_ISSUE' => date('Y-m-d', strtotime($this->input->post('DATE_OF_ISSUE'))),
            'EXPIRE_DATE' => date('Y-m-d', strtotime($this->input->post('EXPIRE_DATE'))),
            'PLACE_OF_ISSUE' => $this->input->post('PLACE_OF_ISSUE'),
            'PRE_ADDRESS' => $this->input->post('PRE_ADDRESS'),
            'PER_ADDRESS' => $this->input->post('PER_ADDRESS'),
            'CONTACT_PERSON' => $this->input->post('CONTACT_PERSON'),
            'CONTACT_PERSON_ADD' => $this->input->post('CONTACT_PERSON_ADD'),
            'CONTACT_PERSON_PHN' => $this->input->post('CONTACT_PERSON_PHN'),
            'RELATION' => $this->input->post('RELATION'),
            'USERGRP_ID' => $this->input->post('user_group'),
            'USERLVL_ID' => $this->input->post('user_group_lavel'),
            'FACULTY_ID' => $this->input->post('FACULTY_ID'),
            'DEPT_ID' => $this->input->post('DEPT_ID'),
            'DESIGNATION_ID' => $this->input->post('designation'),
            'BIOMETRIC_ID' => $this->input->post('BIOMETRIC_ID'),
            'OFFICIAL_PHONE_NO' => $this->input->post('OFFICIAL_PHONE_NO'),
            'OFFICIAL_PHONE_EXTENSION' => $this->input->post('OFFICIAL_PHONE_EXTENSION'),
            'USERNAME' => $this->input->post('USERNAME'),
            'USERPW' => $this->input->post('USERPW')
        );
        if ($file_name != "") {
            $update_data["USER_IMG"] = $file_name;
        }

        $session_info = $this->session->userdata('logged_in');
        $ALT_MOBILE = $this->input->post('ALT_MOBILE');
        $EMP_CI_ID = $this->input->post('EMP_CI_ID');

        if ($ALT_MOBILE) {
            foreach ($ALT_MOBILE as $key => $value) {
                $update_mobile = array(
                    'CONTACT_INFO' => $ALT_MOBILE [$key],
                    'CONTACT_TYPE' => 'M',
                    'EMP_ID' => $this->input->post('USER_ID'),
                    'ORG_ID' => $session_info["ORG_ID"],
                    'DEFAULT_FG' => 1,
                    'ACTIVE_STATUS' => 1
                );
                $this->utilities->updateData('hr_emp_cinfo', $update_mobile, array('EMP_CI_ID' => $EMP_CI_ID[$key]));
            }
        }

        $this->utilities->updateData('sa_users', $update_data, array('USER_ID' => $this->input->post('USER_ID')));
        $this->session->set_flashdata('Success', 'Congratulation ! User Information Updated Successfully.');
        redirect('admin/users', 'refresh');
    }

    /**
     * @methodName  sectionByGrId()
     * @access
     * @param
     * @author      Rakib Roni <rakib@atilimited.net>
     * @return      user group wise section list
     */
    function sectionByOrgId()
    {
        $org_id = $_POST['org_id'];
        $data['section'] = $this->utilities->findAllByAttribute('sa_org_sections', array('ORG_ID' => $org_id));
        $this->load->view('admin/ajax_section_list', $data);
    }

    /**
     * @methodName  userLavelByGrId()
     * @access
     * @param
     * @author      Rakib Roni <rakib@atilimited.net>
     * @return      user group wise lavel list
     */
    function userLavelByGrId()
    {
        $user_group_id = $_POST['user_group_id'];
        $data['user_group_lavel'] = $this->utilities->findAllByAttribute('sa_ug_level', array('USERGRP_ID' => $user_group_id));
        $this->load->view('admin/ajax_lavel_list', $data);
    }

    /**
     *
     *
     * /**
     * @methodName  departmentByFaculty()
     * @access
     * @param
     * @author      Rakib Roni <rakib@atilimited.net>
     * @return      ajax department list by faculty ID
     */
    function departmentByFaculty()
    {
        $faculty_id = $_POST['faculty_id'];
        $data['department'] = $this->utilities->findAllByAttribute('department', array('FACULTY_ID' => $faculty_id));
        $this->load->view('admin/ajax_dep_list', $data);
    }

    /*
     * @methodName ajax_get_program()
     * @access none
     * @param  none
     * @return Mixed Template
     * @author Emdadul Huq <Emdadul@atilimited.net>
     */

    public function ajax_get_designaion()
    {
        $dept_id = $_POST['dept_id'];
        $query = $this->utilities->findAllByAttribute('designations', array("DEPT_NO" => $dept_id, "ACTIVE_STATUS" => 1));
        $returnVal = '<option value="">Select one</option>';
        if (!empty($query)) {
            foreach ($query as $row) {
                $returnVal .= '<option value="' . $row->DESIGNATION_ID . '">' . $row->DESIGNATION . '</option>';
            }
        }
        echo $returnVal;
    }

    /**
     * @methodName  registration()
     * @access
     * @param
     * @author      Rakib Roni <rakib@atilimited.net>
     * @return      exixting registrated student list
     */
    function registration()
    {
        $data['contentTitle'] = 'Existing Students';
        $data["breadcrumbs"] = array(
            "Admin" => "admin/index",
            "Existing Student" => '#'
        );
        $data['registered_student'] = $this->utilities->getAll('regi_student');
        $data['content_view_page'] = 'admin/registered_student';
        $this->admin_template->display($data);
    }

    /**
     * @methodName  addRegStu()
     * @access
     * @param
     * @author      Rakib Roni <rakib@atilimited.net>
     * @return      add registration student
     */
    function addRegStu()
    {
        $data['contentTitle'] = 'Add Students';
        $data["breadcrumbs"] = array(
            "Admin" => "admin/index",
            "Add Student" => '#'
        );

        $data['content_view_page'] = 'admin/add_reg_stu';
        $this->admin_template->display($data);
    }

  
    /**
     * @methodName  createUser()
     * @access      public
     * @param
     * @author      Rakib Roni <rakib@atilimited.net>
     * @return      User create by supur admin
     */
    function createUser()
    {
        $data['contentTitle'] = 'Create User';
        $data["breadcrumbs"] = array(
            "User" => "#",
            "Create User" => '#'
        );
    
        $data['pageTitle'] = 'Create User';
        $data['user_group'] = $this->utilities->getAll('sa_user_group');
        //$data['user_list'] = $
        $data['emp_list'] = $this->utilities->getAll('hr_emp');
        //$data['user_type'] = $this->db->query("select * from user_type_view where CHAR_LKP='c'")->result();
        $data['faculty'] = $this->db->query("select * from ins_dept ")->result();
        $data['content_view_page'] = 'admin/userlist/create_user';
        $this->admin_template->display($data);
    }

       function editUser()
    {
        $userId = $this->uri->segment(3);
        $data['contentTitle'] = 'Edit User';
        $data["breadcrumbs"] = array(
            "Edit User" => "admin/index",
            "Edit User" => '#'
        );
        $data['user_group'] = $this->utilities->getAll('sa_user_group');
        $data['user_lv'] = $this->utilities->getAll('sa_ug_level');
        $data["user"] = $this->utilities->findByAttribute("sa_users", array("USER_ID" => $userId));
        $data['content_view_page'] = 'admin/userlist/update_user_form';
        $this->admin_template->display($data);
    }





      function updateUserLevel()
    {
        $userId = $this->input->post("param");
        $data['emp_list'] = $this->utilities->getAll('hr_emp');


        $data['user_group'] = $this->utilities->getAll('sa_user_group');
        $data['user_lv'] = $this->utilities->getAll('sa_ug_level');
        $data["user"] = $this->utilities->findByAttribute("sa_users", array("USER_ID" => $userId));

        //echo "<pre>"; print_r($data["user"]); exit; echo "</pre>";

        $this->load->view('admin/userlist/user_update_form', $data);
    }


    /**
     * @methodName  saveUser()
     * @access      public
     * @param
     * @author      Rakib Roni <rakib@atilimited.net>
     * @return      Save user
     */
    function saveUser()
    {
        $pass = $this->input->post('USERPW');
        if (isset($_POST['USERNAME'])) {
        //echo $f = $this->input->post('file'); exit;
    
        // add additional parameters
        $options = array(
            'cost' => 11,
            'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),
        );
        // creating the salt
        $h_pass = password_hash($pass, PASSWORD_BCRYPT, $options);
        $FileUploader = new FileUploader('files', array(
                'limit' => 5,
                'maxSize' => 10,
                'fileMaxSize' => 2,
                'extensions' => ['jpg', 'jpeg', 'png', 'gif', 'bmp', 'JPG'],
                'required' => true,
                'uploadDir' => 'upload/employee/photo/',
                'title' => '{timestamp}--{file_name}',
                'replace' => false,
                'listInput' => true,
                'files' => null
            ));

            // call to upload the files
            $data = $FileUploader->upload();

            // if uploaded and success
            if ($data['isSuccess'] && count($data['files']) > 0) {
              //echo "hello";exit();
                // get uploaded files
                $uploadedFiles = $data['files'];
                $images = '';
                $first = true;
                foreach ($uploadedFiles as $uploadedFile) {
                    if ($first == false) {
                        $images .= '___';
                    }
                    $images .= $uploadedFile['name'];
                    $first = false;
                }
            }

        // $user_dept_info = $this->utilities->findByAttribute('hr_edeptdesi', array('EMP_ID' => $this->input->post('EMP_ID'), 'DEFAULT_FG' => 1));
        // $emp_image_name = $this->utilities->findByAttribute('hr_emp', array('EMP_ID' => $this->input->post('EMP_ID')));

        //print_r($emp_image_name);exit;
        $user_data = array(
            //'EMP_ID' => $this->input->post('EMP_ID'),
            'USERGRP_ID' => $this->input->post('USERGRP_ID'),
            'USERLVL_ID' => $this->input->post('USER_GRP_LVL_ID'),
            'USERNAME' => $this->input->post('USERNAME'),
            'FULL_NAME' => $this->input->post('FULL_NAME'),
            // 'FATHERS_NAME' => $this->input->post('FATHERS_NAME'),
            // 'MOTHERS_NAME' => $this->input->post('MOTHERS_NAME'),
            // 'PLACE_OF_BIRTH' => $this->input->post('PLACE_OF_BIRTH'),
            'DOB' => date('Y-m-d', strtotime($this->input->post('DOB'))),
            'MOBILE' => $this->input->post('MOBILE'),
            'EMAIL' => $this->input->post('EMAIL'),
            'USERPW' =>$h_pass,
            'USER_IMG' => $images,
        );


    

        $this->db->insert('sa_users', $user_data);
        $this->session->set_flashdata('Success','Successfully Created.');
        redirect('Admin/userList', 'refresh');
      }
      else {
            $data['content_view_page'] = 'admin/userlist/create_user';
            $this->admin_template->display($data);
        }
    }


        function updateUser()
    {
        $userId = $this->uri->segment(3);
        $pass = $this->input->post('USERPW');

        $options = array(
            'cost' => 11,
            'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),
        );
        // creating the salt
        $h_pass = password_hash($pass, PASSWORD_BCRYPT, $options);

               $FileUploader = new FileUploader('files', array(
                'limit' => 5,
                'maxSize' => 10,
                'fileMaxSize' => 2,
                'extensions' => ['jpg', 'jpeg', 'png', 'gif', 'bmp', 'JPG'],
                'required' => true,
                'uploadDir' => 'upload/employee/photo/',
                'title' => '{timestamp}--{file_name}',
                'replace' => false,
                'listInput' => true,
                'files' => null
            ));

            // call to upload the files
            $data = $FileUploader->upload();

            // if uploaded and success
            if ($data['isSuccess'] && count($data['files']) > 0) {
                // get uploaded files
                $uploadedFiles = $data['files'];
                $images = '';
                $first = true;
                foreach ($uploadedFiles as $uploadedFile) {
                    if ($first == false) {
                        $images .= '___';
                    }
                    $images .= $uploadedFile['name'];
                    $first = false;
                }

                    $newPostData = array(
            'USERGRP_ID' => $this->input->post('USERGRP_ID'),
            'USERLVL_ID' => $this->input->post('USER_GRP_LVL_ID'),
            'USERNAME' => $this->input->post('USERNAME'),
            'FULL_NAME' => $this->input->post('FULL_NAME'),
            // 'FATHERS_NAME' => $this->input->post('FATHERS_NAME'),
            // 'MOTHERS_NAME' => $this->input->post('MOTHERS_NAME'),
            // 'PLACE_OF_BIRTH' => $this->input->post('PLACE_OF_BIRTH'),
            'DOB' => date('Y-m-d', strtotime($this->input->post('DOB'))),
            'MOBILE' => $this->input->post('MOBILE'),
            'EMAIL' => $this->input->post('EMAIL'),
            'USERPW' =>$h_pass,
            'USER_IMG' => $images,
                  
                );


            }else{
                 $newPostData = array(
            'USERGRP_ID' => $this->input->post('USERGRP_ID'),
            'USERLVL_ID' => $this->input->post('USER_GRP_LVL_ID'),
            'USERNAME' => $this->input->post('USERNAME'),
            'FULL_NAME' => $this->input->post('FULL_NAME'),
            // 'FATHERS_NAME' => $this->input->post('FATHERS_NAME'),
            // 'MOTHERS_NAME' => $this->input->post('MOTHERS_NAME'),
            // 'PLACE_OF_BIRTH' => $this->input->post('PLACE_OF_BIRTH'),
             'DOB' => date('Y-m-d', strtotime($this->input->post('DOB'))),
            'MOBILE' => $this->input->post('MOBILE'),
            'EMAIL' => $this->input->post('EMAIL'),
            'USERPW' =>$h_pass,

            
                  
                );

            }

        if($this->db->update('sa_users', $newPostData, array('USER_ID' => $userId)))
        {
            $this->session->set_flashdata('Success','Successfully Updated.');
            redirect('Admin/userList');
        }
      }



    function editUserLevel()
    {
        $user_id = $this->input->post('user_id');
        $emp_id = $this->input->post('EMP_ID');
        $USER_GRP_ID = $this->input->post('USERGRP_ID');
        $USER_GRP_LVL_ID = $this->input->post('USER_GRP_LVL_ID');
        $USERNAME = $this->input->post('USERNAME');
        $password = $this->input->post('USERPW');

        // checking if Program with this name is already exist
//        $check = $this->utilities->hasInformationByThisId("sa-user", array("USER_ID" => $user_id));
//        if (empty($check)) {// if Program name available
        // preparing data to insert
        $user = array(
            'USERGRP_ID' => $USER_GRP_ID,
            'USERLVL_ID' => $USER_GRP_LVL_ID,
            'USERNAME' => $USERNAME,
            'USERPW' => $password,
            'EMP_ID' => $emp_id,
        );

        //echo "<pre>"; print_r($user); exit; echo "</pre>";

        if ($this->utilities->updateData('sa_users', $user, array('USER_ID' => $user_id))) { // if data update successfully
            echo "<div class='alert alert-success'>User Update successfully</div>";
        } else { // if data update failed
            echo "<div class='alert alert-danger'>User Update failed</div>";
        }
//        } else {// if faculty name not available
//            echo "<div class='alert alert-danger'>User Name Already Exist</div>";
//        }
    }

    function userById()
    {
        $user_id = $this->input->post('param'); // degree name
        // $data["previlages"] = $this->checkPrevilege("setup/program");

        $data["previlages"] = $this->checkPrevilege();
        $data['user_group'] = $this->utilities->getAll('sa_user_group');

        $data['user'] = $this->utilities->findAllByAttributeFromUserWithId($user_id);

        $this->load->view('admin/single_user_row', $data);
    }

    /**
     * @param       none
     * @author      Emdadul Huq<Emdadul@atilimited.net>
     * @return      none
     */
    function updateUserInformation()
    {
        $USER_ID = $this->input->post('userId');
        $pass = $this->input->post('USERPW');
        // add additional parameters
        $options = array(
            'cost' => 11,
            'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),
        );
        // creating the salt
        $h_pass = password_hash($pass, PASSWORD_BCRYPT, $options);
        $user_data = array(
            'FULL_NAME' => $this->input->post('USERNAME'),
            'USERGRP_ID' => $this->input->post('USERGRP_ID'),
            'USERLVL_ID' => $this->input->post('USER_GRP_LVL_ID'),
            'USERNAME' => $this->input->post('USERNAME'),
            'USERPW' => $h_pass,
            'EMAIL' => $this->input->post('EMAIL'),
            'BIOMETRIC_ID' => $this->input->post('BIOMETRIC_ID'),
            'HIRE_DATE' => date('Y-m-d', strtotime($this->input->post('HIRE_DATE'))),
            'MOBILE' => $this->input->post('MOBILE'),
            'USER_TYPE' => $this->input->post('USER_TYPE'),
            'DEPT_ID' => $this->input->post('DEPARTMENT_ID'),
            'DESIGNATION_ID' => $this->input->post('DESIGNATION_ID')
        );
        if ($this->utilities->updateData('sa_users', $user_data, array('USER_ID' => $USER_ID))) { // if data update successfully
            echo "<div class='alert alert-success'>User Update successfully</div>";
        } else { // if data update failed
            echo "<div class='alert alert-danger'>User Update failed</div>";
        }
    }

    /**
     * @param       user id, email
     * @author      Emdadul Huq<Emdadul@atilimited.net>
     * @return      none
     */
    function checkEmailAddress()
    {
        $user_id = $this->input->post("user_id");
        $Email = $this->input->post("Email");
        $check = $this->utilities->hasInformationByThisId("sa_users", array("EMAIL" => $Email, "USER_ID !=" => $user_id));
        if (!empty($check)) {
            echo "used";
        } else {
            echo "";
        }
    }

    /**
     * @param       user id, BIOMETRIC_ID.
     * @author      Emdadul Huq<Emdadul@atilimited.net>
     * @return      none
     */
    function checkBiometricId()
    {
        $user_id = $this->input->post("user_id");
        $BIOMETRIC_ID = $this->input->post("BIOMETRIC_ID");
        $check = $this->utilities->hasInformationByThisId("sa_users", array("BIOMETRIC_ID" => $BIOMETRIC_ID, "USER_ID !=" => $user_id));
        if (!empty($check)) {
            echo "used";
        } else {
            echo "";
        }
    }

    /**
     * @param       User Id
     * @author      Emdadul Huq<Emdadul@atilimited.net>
     * @return      none
     */
    function UserInformationById()
    {
        $userId = $this->input->post("param");
        $data["previlages"] = $this->checkPrevilege("admin/userList");
        // print_r( $data["previlages"] );exit;
        $data['user'] = $this->db->query("SELECT su.*, d.DEPT_NAME, des.DESIGNATION, sug.USERGRP_NAME, sul.UGLEVE_NAME
            FROM sa_users su 
            INNER JOIN department d on d.DEPT_ID = su.DEPT_ID
            INNER JOIN designations des on des.DESIGNATION_ID = su.DESIGNATION_ID
            INNER JOIN sa_user_group sug on sug.USERGRP_ID = su.USERGRP_ID
            INNER JOIN sa_ug_level sul on sul.UG_LEVEL_ID = su.USERLVL_ID
                                            WHERE su.USER_ID = $userId")->row(); // select single element
        $this->load->view('admin/faculty/single_user_row', $data);
    }

    /**
     * @param       user id, user level
     * @author      Emdadul Huq<Emdadul@atilimited.net>
     * @return      none
     */
    function searchUser()
    {
        $USERGRP_IDS = $this->input->post("USERGRP_IDS");
        $USER_LVL = $this->input->post("USER_LVL");
        $data['user'] = $this->db->query("SELECT su.*, d.DEPT_NAME, des.DESIGNATION, sug.USERGRP_NAME, sul.UGLEVE_NAME
            FROM sa_users su 
            INNER JOIN department d on d.DEPT_ID = su.DEPT_ID
            LEFT JOIN designations des on des.DESIGNATION_ID = su.DESIGNATION_ID
            INNER JOIN sa_user_group sug on sug.USERGRP_ID = su.USERGRP_ID
            INNER JOIN sa_ug_level sul on sul.UG_LEVEL_ID = su.USERLVL_ID
                                            WHERE su.USERGRP_ID = $USERGRP_IDS AND su.USERLVL_ID = $USER_LVL")->result(); // select single element
        $this->load->view('admin/faculty/search_user_list', $data);
    }

    /**
     * @param       userID
     * @author      Emdadul Huq<Emdadul@atilimited.net>
     * @return      none
     */
    function viewUserLevel()
    {
        $userId = $this->input->post("param");
        $data['user'] = $this->db->query("SELECT su.*, d.DEPT_NAME, des.DESIGNATION, sug.USERGRP_NAME, sul.UGLEVE_NAME
            FROM sa_users su 
            INNER JOIN department d on d.DEPT_ID = su.DEPT_ID
            LEFT JOIN designations des on des.DESIGNATION_ID = su.DESIGNATION_ID
            INNER JOIN sa_user_group sug on sug.USERGRP_ID = su.USERGRP_ID
            INNER JOIN sa_ug_level sul on sul.UG_LEVEL_ID = su.USERLVL_ID
                                            WHERE su.USER_ID = $userId")->row(); // select single element
        $this->load->view('admin/faculty/user_view', $data);
    }

    /**
     * @param       none
     * @author      Md.Rokibuzzaman<rokibuzzaman@atilimited.net>
     * @return      none
     */
    function userList()
    {  
        $session_info = $this->session->userdata('logged_in');
        $userGrp=$session_info['USERGRP_ID'];
        $data['contentTitle'] = 'User';
        $data['breadcrumbs'] = array(
            'Admin' => '#',
            'User List' => '#',
        );
        $data["previlages"] = $this->checkPrevilege();
        $data['user_group'] = $this->utilities->getAll('sa_user_group');

        $data['user'] = $this->utilities->getAllUserInfo();
        $view=$this->User->userPrivilege($userGrp,'V'); 
        $data['viewPerm'] =$view->PERMSN_EXIST;
        $approve=$this->User->userPrivilege($userGrp,'A'); 
        $data['rejectPerm'] =$approve->PERMSN_EXIST;
        $data['content_view_page'] = 'admin/faculty/user_list';
        $this->admin_template->display($data);
    }


}
