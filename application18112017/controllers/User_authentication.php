<?php defined('BASEPATH') OR exit('No direct script access allowed');
class User_Authentication extends CI_Controller
{
    function __construct() {
    parent::__construct();
     $this->load->model('utilities');
      $this->load->model('auth_model');
      $this->load->helper(array('form'));
      $this->load->library('form_validation');
      $this->load->library('session');
      //$this->load->library(array('session', 'form_validation', 'email');
      $this->load->library('upload');
      $this->load->helper('url');

      // Load facebook library
     $this->load->library('facebook');

      //Load user model
      $this->load->model('user');
    }

    /**
     * @param      Facebook defining Registration userId 
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      Process facebook registration  and login data
     */

    public function index(){
    $userData = array();

    // Check if user is logged in
    if($this->facebook->is_authenticated()){
      // Get user facebook profile details
      $userProfile = $this->facebook->request('get', '/me?fields=id,first_name,last_name,gender,picture');
      //print_r($userProfile );exit();
        $fbid=$userProfile['id'];
          $userInfo=$this->db->query("select * from users where facebookid=$fbid")->row();
          $exist=count($userInfo);
          if($exist>0)
          {
            $userData['facebookId'] = $userProfile['id'];
            //$userData['email'] = $userProfile['email'];
            $userData['fname'] =$userInfo->fname; //$userProfile['first_name'].' '.$userProfile['last_name'];
            $userData['gender'] =$userInfo->gender; //$userProfile['gender'];
           // $userData['profile_url'] = 'https://www.facebook.com/'.$userProfile['id'];
            $userData['image'] =$userInfo->image; //$userProfile['picture']['data']['url'];
            $userData['agree'] = 'agree';
            $userData['type'] =$userInfo->type; //'individual';
            $userData['login_method'] =$userInfo->login_method; //'F';
            $userData['active'] = 1;
          }
          else
          {
            $userData['facebookId'] = $userProfile['id'];
            //$userData['email'] = $userProfile['email'];
            $userData['fname'] =$userProfile['first_name'].' '.$userProfile['last_name'];
            $userData['gender'] =$userProfile['gender'];
           // $userData['profile_url'] = 'https://www.facebook.com/'.$userProfile['id'];
            $userData['image'] =$userProfile['picture']['data']['url'];
            $userData['agree'] = 'agree';
            $userData['type'] ='individual';
            $userData['login_method'] ='F';
            $userData['active'] = 1;
          }
            // Preparing data for database insertion
            //$userData['facebook_user_id'] = 'facebook';


           // $userData['locale'] = $userProfile['locale'];

            // Insert or update user data
            $userID = $this->user->checkUser($userData);
            $userData['userId'] = $userID;
            //print_r($userID);exit();
      // Check user data insert or update status
            if(!empty($userID)){
                $data['userData'] = $userData;
                $this->session->set_userdata('user_logged_user',$userData);
            } else {
               $data['user_logged_user'] = array();
            }

      // Get logout URL
      $data['logoutUrl'] = $this->facebook->logout_url();
    }else{
            $fbuser = '';

      // Get login URL
            $data['authUrl'] =  $this->facebook->login_url();
             //echo "<pre>";print_r($data['authUrl']);exit();

        }

    // Load login & profile view
       // $session_info_fb = $this->session->userdata("userData");
   // echo "<pre>";print_r($session_info_fb);exit();
  //$this->load->view('portal/portalController/index',$data);
          //$this->ptemplate->display($data);
          redirect("portal/index");
    }


/**
     * @param      Facebook defining Registration userId 
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      Process facebook registration  logout
     */

  public function logout() {
    // Remove local Facebook session
    $this->facebook->destroy_session();
    // Remove user data from session
    $this->session->unset_userdata('userData');
    // Redirect to login page
    redirect('portal/index', 'refresh');
    }
}
