<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Opportunity extends CI_Controller
{

    /**
     * @methodName opportunities
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function opportunities()
    {
        $userId = $this->getUserId();
        $data['permission'] = $this->Portal_model->getUserPermission(4, $userId);

        $result_per_page = 9;

        $config['base_url'] = site_url() . '/opportunity/opportunities/';
        $config['total_rows'] = $this->opportunity_model->count_items();
        $config['per_page'] = $result_per_page;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
        $this->pagination->initialize($config);

        $offset = (!empty($this->uri->segment(3))) ? $this->uri->segment(3) : 0;

        $data['opportunities'] = $this->opportunity_model->getAllOpportunites($result_per_page, $offset);
        $data['opportunity_title_list'] = $this->opportunity_model->findAllOpportunityTitleList();

        $data['content_view_page'] = 'portal/opportunity/opportunity_list';
        $this->event_template->display($data);
    }

    private function getUserId()
    {

        if (!empty($this->session->userdata['user_logged_user'])) {
            $userId = $data['user_id'] = $this->session->userdata['user_logged_user']['userId'];
        } else {
            $userId = '';
        }
        return $userId;
    }

    /**
     * @methodName searchOpportunities
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function searchOpportunities()
    {
        $category_type = $this->input->post('category');
        $order = $this->input->post('order');
        $search = $this->input->post('search');

        $data['cat_ty'] = $category_type;
        $data['or'] = $order;
        $data['src'] = $search;

        $result_per_page = 9;

        $data['opportunities'] = $this->opportunity_model->searchOpportunity($category_type, $order, $search, $result_per_page);

        $config['base_url'] = site_url() . '/opportunity/paginateOpportunities/';
        $config['total_rows'] = $this->session->userdata["row_count"]["num_rows"];
        $config['per_page'] = $result_per_page;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
        $this->pagination->initialize($config);

        $this->session->set_userdata('pagination', array(
            'category' => $category_type,
            'order' => $order,
            'search' => $search,
        ));

        $data['opportunity_title_list'] = $this->opportunity_model->findAllOpportunityTitleList();

        $data['content_view_page'] = 'portal/opportunity/search_opportunity_list';
        $this->event_template->display($data);

    }

    /**
     * @methodName paginateOpportunities
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function paginateOpportunities()
    {
        $category_type = $this->session->userdata["pagination"]["category"];
        $order = $this->session->userdata["pagination"]["order"];
        $search = $this->session->userdata["pagination"]["search"];
        $num_rows = $this->session->userdata["row_count"]["num_rows"];

        $data['cat_ty'] = $category_type;
        $data['or'] = $order;
        $data['src'] = $search;

        $offset = (!empty($this->uri->segment(3))) ? $this->uri->segment(3) : 0;

        $result_per_page = 9;

        $data['opportunities'] = $this->opportunity_model->searchOpportunity($category_type, $order, $search, $result_per_page, $offset);

        $config['base_url'] = site_url() . '/opportunity/paginateOpportunities/';
        $config['total_rows'] = $num_rows;
        $config['per_page'] = $result_per_page;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
        $this->pagination->initialize($config);

        $data['opportunity_title_list'] = $this->opportunity_model->findAllOpportunityTitleList();

        $data['content_view_page'] = 'portal/opportunity/search_opportunity_list';
        $this->event_template->display($data);
    }

    /**
     * @methodName fbImo
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function fbImo()
    {
        $control_id = $this->input->post('control_id');
        $value = $this->input->post('value');
        $user_id = $this->input->post('user_id');

        $imo_track = $this->input->post('imo_track');

        if ($value == 'like') {
            $imo_type = 1;
        } elseif ($value == 'haha') {
            $imo_type = 2;
        } elseif ($value == 'love') {
            $imo_type = 3;
        } elseif ($value == 'wow') {
            $imo_type = 4;
        } elseif ($value == 'sad') {
            $imo_type = 5;
        } elseif ($value == 'angry') {
            $imo_type = 6;
        }

        if ($imo_track == 'imo_track' && $value == '') {
            $result = $this->opportunity_model->updateInterest(1, $user_id, $control_id);

            $msg = '';
            if (!$result['ack']) {
                $msg = $result['msg'];
            }
            $interestCount = $this->opportunity_model->getBlogInterest($control_id);
            if (!empty($msg)) {
                $ack = false;
            } else {
                $ack = true;
            }
            echo json_encode(array('ack' => $ack, 'msg' => $msg, 'count' => $interestCount, 'opp_id' => $control_id, 'un_chg' => 1));
        } elseif ($imo_track == 'imo_track') {
            $result = $this->opportunity_model->deleteUpdateInterest($imo_type, $user_id, $control_id);
//            echo $result['ack']; exit;
            $msg = '';
            if (!$result['ack']) {
                $msg = $result['msg'];
            }
            $interestCount = $this->opportunity_model->getBlogInterest($control_id);

//            echo $interestCount; exit;
            if (!empty($msg)) {
                $ack = false;
            } else {
                $ack = true;
            }
            echo json_encode(array('ack' => $ack, 'msg' => $msg, 'count' => $interestCount, 'opp_id' => $control_id, 'un_chg' => 2));
        } else {
            $result = $this->opportunity_model->updateInterest2($imo_type, $user_id, $control_id);
            $msg = '';
            if (!$result['ack']) {
                $msg = $result['msg'];
            }
            //get the new request
            $interestCount = $this->opportunity_model->getOppInterest($control_id);
            if (!empty($msg)) {
                $ack = false;
            } else {
                $ack = true;
            }
            echo json_encode(array('ack' => $ack, 'msg' => $msg, 'count' => $interestCount, 'opp_id' => $control_id));

//        echo 'Control ID: '.$control_id.' Value: '.$value.' User ID: '.$user_id;
        }

    }

    /**
     * @methodName viewModal
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function viewModal($id,$url)
    {
        $data['url']=$url;
        $opportunity_id = $this->uri->segment(3);

        if (!empty($this->session->userdata['user_logged_user'])) {
            $data['user_id'] = $this->session->userdata['user_logged_user']['userId'];
        } else {
            $data['user_id'] = "";
        }
       
        $this->opportunity_model->increaseView($opportunity_id);

        $data['opportunityDetails'] = $this->opportunity_model->getOpportunityData($opportunity_id);
        $organizationDetails = $this->opportunity_model->getOrganizationData($data['opportunityDetails']->organization_id);
        $organizationLogo = $this->opportunity_model->organizationLogo($organizationDetails->userId);
        $relOpportunityData = $this->opportunity_model->relOpportunity($opportunity_id);

        $data['images'] = explode('___', $data['opportunityDetails']->images);

        $interest = $this->opportunity_model->getInterestCount($opportunity_id);
        $data['interest_count'] = $interest->int_count;

        $this->load->view("portal/opportunity/opportunity_modal_view", $data);
    }


    /**
     * @methodName calSearch
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function calSearch()
    {
        $action = (!empty($_POST['action'])) ? $_POST['action'] : '';

        switch ($action) {

            case 'opportunityInterest':

                $user_id = $_POST['user_id'];
                $opportunity_id = $_POST['opportunity_id'];

                $result = $this->opportunity_model->updateInterest($user_id, $opportunity_id);
                $msg = '';
                if (!$result['ack']) {
                    $msg = $result['msg'];
                }
                //get the new request
                $interestCount = $this->opportunity_model->getInterestCount($opportunity_id);

                if (!empty($msg)) {
                    $ack = false;
                } else {
                    $ack = true;
                }
                echo json_encode(array('ack' => $ack, 'msg' => $msg, 'count' => $interestCount->int_count, 'opportunity_id' => $opportunity_id));
                exit;
                break;

            case 'blogImageDelete':

                $opp_id = $_POST['opp_id'];
                $image_name = $_POST['img_name'];
                $result = $this->opportunity_model->deleteOpportunityImage($opp_id, $image_name);
                $msg = $result['msg'];
                if (!$result['ack']) {
                    $ack = false;
                } else {
                    $ack = true;
                }
                echo json_encode(array('ack' => $ack, 'msg' => $msg, 'blog_id' => $opp_id));
                exit;
                break;
        }

    }


    /**
     * @methodName opportunityList
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function opportunityList()
    {
        $userId = $this->getUserId();
        $m = $data['permission'] = $this->Portal_model->getUserPermission(4, $userId);
        if ($m->PER == 0) {
            redirect('Opportunity/opportunities');
        }

        $user_id = $this->session->userdata['user_logged_user']['userId'];

        $data['opportunitylist'] = $this->opportunity_model->getOpportunites($user_id);
        $data['previousOpportunities'] = $this->opportunity_model->previousOpportunities();

        $data['content_view_page'] = 'portal/Opportunity/opportunities';
        $this->event_template->display($data);
    }


    /**
     * @methodName createOpportunity
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function createOpportunity()
    {
        $userId = $this->getUserId();
        $m = $data['permission'] = $this->Portal_model->getUserPermission(4, $userId);
        if ($m->PER == 0) {
            redirect('Opportunity/opportunities');
        }
        $user_id = $this->session->userdata['user_logged_user']['userId'];

        $data['organizationData'] = $this->opportunity_model->getOrganization($user_id);
        $data['city'] = $this->opportunity_model->cities();

        $data['content_view_page'] = 'portal/opportunity/create_opportunity';
        $this->event_template->display($data);
    }

    /**
     * @methodName editOpportunityPost
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function editOpportunityPost()
    {
        $opportunity_id = $this->uri->segment(3);

        $user_id = $this->session->userdata['user_logged_user']['userId'];
        $data['organizationData'] = $this->opportunity_model->getOrganization($user_id);
        $data['opportunityDetails'] = $this->opportunity_model->getOpportunityData($opportunity_id);
        $data['city'] = $this->opportunity_model->cities();
        $data['content_view_page'] = 'portal/opportunity/edit_opportunity';
        $this->event_template->display($data);


    }


    function opportunityView()
    {
        $opportunity_id = $this->uri->segment(3);
        $this->opportunity_model->increaseView($opportunity_id);

        if (!isset($this->session->userdata['user_logged_user'])) {

            $data['user_id'] = '';
        } else {
            $data['user_id'] = $this->session->userdata['user_logged_user']['userId'];
        }

//        echo  $data['user_id']; exit;

        $data['opportunityDetails'] = $this->opportunity_model->getOpportunityData($opportunity_id);
        $data['organizationDetails'] = $this->opportunity_model->getOrganizaionData($data['opportunityDetails']->organization_id);
        $data['organizationLogo'] = $this->opportunity_model->organizationLogo($data['organizationDetails']->userId);
        //$data['blogComments'] = $this->blog_model->getBlogComment($blog_id);

        $interest = $this->opportunity_model->getInterestCount($opportunity_id);
        $data['interest_count'] = $interest->int_count;

//        echo "<pre>"; print_r($data['organizationLogo']); exit;

        $data['content_view_page'] = 'portal/opportunity/opportunity_view';
        $this->event_template->display($data);
    }


    /**
     * @methodName createNewBlogPost
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function createNewOpportunityPost()
    {

        $user_id = $this->input->post('user_id');
        $org_id = $this->db->query("SELECT ls.id FROM listing ls WHERE ls.userId = $user_id")->row();
        $title = $this->input->post('title');
        $category = $this->input->post('category');
        $description = $this->input->post('description');
        $start_d = $this->input->post('start_date');
        $start_date = date('Y-m-d', strtotime($start_d));
        $end_d = $this->input->post('end_date');
        $end_date = date('Y-m-d', strtotime($end_d));
        if(empty($end_d)) { $end_date = $start_date;}
        $district = $this->input->post('district');
        $registration_link = $this->input->post('registration_link');

        $FileUploader = new FileUploader('files', array(
            'limit' => 5,
            'maxSize' => 10,
            'fileMaxSize' => 2,
            'extensions' => ['jpg', 'jpeg', 'png', 'gif', 'bmp', 'JPG', 'JPEG'],
            'required' => true,
            'uploadDir' => 'upload/opportunities/',
            'title' => '{timestamp}--{file_name}',
            'replace' => false,
            'listInput' => true,
            'files' => null
        ));

        // call to upload the files
        $data = $FileUploader->upload();

        // if uploaded and success
        if ($data['isSuccess'] && count($data['files']) > 0) {
            // get uploaded files
            $uploadedFiles = $data['files'];
            $images = '';
            $first = true;
            foreach ($uploadedFiles as $uploadedFile) {
                if ($first == false) {
                    $images .= '___';
                }
                $images .= $uploadedFile['name'];
                $first = false;
            }
        }


        //}

        $newPostData = array(

            'organization_id' => $org_id->id,
            'title' => $title,
            'category' => $category,
            'description' => $description,
            'start_date' => $start_date,
            'end_date' => $end_date,
            'images' => $images,
            'registration_link' => $registration_link,
            'district' => $district,

        );

        $this->db->insert('opportunity', $newPostData);

        $this->session->set_flashdata('success', 'Successfully Created.');

        redirect('Opportunity/opportunityList', 'refresh');
    }


    /**
     * @methodName insertEditedOpportunity
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */


    function insertEditedOpportunity()
    {
        $opportunity_id = $this->uri->segment(3);

        $title = $this->input->post('title');
        $category = $this->input->post('category');
        $description = $this->input->post('description');
        $start_d = $this->input->post('start_date');
        $start_date = date('Y-m-d', strtotime($start_d));

        $end_d = $this->input->post('end_date');
        $end_date = date('Y-m-d', strtotime($end_d));
        $district = $this->input->post('district');
        $registration_link = $this->input->post('registration_link');

        $hasFile = $this->input->post('fileuploader-list-files');
        $ext_image = $this->input->post('ext_image');

        if (!empty($hasFile)) {

            $FileUploader = new FileUploader('files', array(
                'limit' => 5,
                'maxSize' => 10,
                'fileMaxSize' => 2,
                'extensions' => ['jpg', 'jpeg', 'png', 'gif', 'bmp', 'JPG', 'JPEG'],
                'required' => true,
                'uploadDir' => 'upload/opportunities/',
                'title' => '{timestamp}--{file_name}',
                'replace' => false,
                'listInput' => true,
                'files' => null
            ));

            // call to upload the files
            $data = $FileUploader->upload();

            // if uploaded and success
            if ($data['isSuccess'] && count($data['files']) > 0) {
                // get uploaded files
                $uploadedFiles = $data['files'];
                $images = '';
                $first = true;
                foreach ($uploadedFiles as $uploadedFile) {
                    if ($first == false) {
                        $images .= '___';
                    }
                    $images .= $uploadedFile['name'];
                    $first = false;
                }
            }

            if ($ext_image != '' || $ext_image != null) {
                $newPostData = array(

                    'title' => $title,
                    'category' => $category,
                    'description' => $description,
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                    'images' => $ext_image . '___' . $images,
                    'registration_link' => $registration_link,
                    'district' => $district,

                );
            } else {
                $newPostData = array(

                    'title' => $title,
                    'category' => $category,
                    'description' => $description,
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                    'images' => $images,
                    'registration_link' => $registration_link,
                    'district' => $district,

                );
            }

        } else {
            $newPostData = array(
                'title' => $title,
                'category' => $category,
                'description' => $description,
                'start_date' => $start_date,
                'end_date' => $end_date,
//                'images' => $images,
                'registration_link' => $registration_link,
                'district' => $district,

            );
        }

        if ($this->db->update('opportunity', $newPostData, array('id' => $opportunity_id))) {
            $this->session->set_flashdata('success', 'Successfully Updated.');
            redirect('opportunity/opportunityList');
        }

    }


    /**
     * @methodName deleteOpportunityPost
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function deleteOpportunityPost()
    {
        $opportunity_id = $this->uri->segment(3);

        $result = $this->opportunity_model->deleteOpportunityPost($opportunity_id);

        if ($result == "Deleted") {
            redirect('Opportunity/opportunityList', 'refresh');
        }
    }

    /**
     * @methodName report_problem
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    public function report_problem($opp_id, $id,$url)
    {

        if (!empty($id)) {
             
            $data['user_detail'] = $this->db->query("select u.* from users u
                                                  WHERE u.userId= $id order by u.userId")->row();
        }
        $data['url']=$url;
        $data['link'] = site_url() . 'opportunity/opportunityView/' . $opp_id;

        $this->load->view('portal/opportunity/report_problem', $data);

    }

    /**
     * @methodName reportProblemFormInsert
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    public function reportProblemFormInsert()
    {
        if($_POST)
        {
            $redirectUrl=$this->input->post('redirectUrl');
            $redirect=str_replace("--","/",$redirectUrl);
            $name = $this->input->post('name');
            $email = $this->input->post('email');
            $subject = $this->input->post('subject');
            $message = $this->input->post('message');
            $link = $this->input->post('link');

            if (!empty($name) || !empty($email) || !empty($subject) || !empty($message)) {
                $body = "Email From: " . $name . "<br>" . $message . "<br>" . $link;

                $to = $email;
                require 'gmail_app/class.phpmailer.php';
                $mail = new PHPMailer;
                $mail->IsSMTP();
                $mail->Host = "mail.harnest.com";
                $mail->Port = "465";
                $mail->SMTPAuth = true;
                $mail->Username = "dev@atilimited.net";
                $mail->Password = "@ti321$#";
                $mail->SMTPSecure = 'ssl';
                $mail->From = "noreply@youthopiabangla.org";
                $mail->FromName = "Youthopia.bangla";
                $mail->AddAddress($to);
                $mail->IsHTML(TRUE);
                $mail->Subject = $subject;
                $mail->Body = $body;
                 if ($mail->Send()) {
                     $this->session->set_flashdata('success',' Your message has been received.We will get back to you as soon as possible.');
                }

            }
             else{
          $this->session->set_flashdata('Error','You are not successfully message send!.');
        }
       redirect($redirect);

        }
    }
}

