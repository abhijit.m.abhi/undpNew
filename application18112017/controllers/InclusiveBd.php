<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class InclusiveBd extends CI_Controller
{



     /**
     * @methodName inclusivebdList
     * @access
     * @param  none
     * @author Md.Rokibuzzaman <rokibuzzaman@atilimited.net>
     * @return
     */
     private function getUserId()
     {

       if (!empty($this->session->userdata['user_logged_user'])) {
           $userId=$data['user_id'] = $this->session->userdata['user_logged_user']['userId'];
       } else {
          $userId='';
       }
       return $userId;
     }
    function inclusivebdList()
    {
      $userId=$this->getUserId();
      $m=$data['permission']=$this->Portal_model->getUserPermission(6,$userId);
      if($m->PER==0)
      {
        redirect('InclusiveBd/inclusiveBdAllList');
      }
        $user_id = $this->session->userdata['user_logged_user']['userId'];

        $data['incliusiveBds'] = $this->inclusivebd_model->getInclusiveBd($user_id);
        $data['category']=$this->inclusivebd_model->getInclusiveBdCat();
        $data['inclusive_search_list'] = $this->inclusivebd_model->getAllInclusiveSearchList();
        $data['content_view_page'] = 'portal/inclusiveBd/inclusive_bd';
        $this->event_template->display($data);
    }


    /**
     * @methodName InclusiveBd
     * @access
     * @param  none
     * @author Md.Rokibuzzaman <rokibuzzaman@atilimited.net>
     * @return
     */

    function inclusiveBdAllList()
    {
      $userId=$this->getUserId();
      $m=$data['permission']=$this->Portal_model->getUserPermission(6,$userId);

        $cat = $this->uri->segment(3);

        $data['cate'] = $cat;
        $data['or'] = '';
        $data['src'] = '';

        $this->session->set_userdata('current_category', array(
            'category' => $cat,
        ));

        $result_per_page = 9;

        $config['base_url'] = site_url() . '/InclusiveBd/paginateIndexBdAllList/';

        if(!empty($cat))
        {
            $config['total_rows'] = $this->inclusivebd_model->countItemsByCategory($cat);

        } else {

            $config['total_rows'] = $this->inclusivebd_model->count_items();
        }


        $config['per_page'] = $result_per_page;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
        $this->pagination->initialize($config);

//        echo $config['total_rows']; exit;

//        $offset = (!empty($this->uri->segment(3))) ? $this->uri->segment(3) : 0;

        $offset = 0;

        $data['inclusive_posts_y'] = $this->inclusivebd_model->getAllInclusiveYBd($result_per_page, $offset, $cat);

        $data['category'] =$this->inclusivebd_model->getInclusiveBdCat();
        $data['sliders'] = $this->db->query("SELECT * FROM inclusive_bd_slider")->result();
        $data['slider'] = $this->db->query("SELECT * FROM inclusive_bd_slider")->row();
        $data['category']=$this->inclusivebd_model->getInclusiveBdCat();

//        echo "<pre>"; print_r($data['inclusive_posts_y']); exit;


        $data['content_view_page'] = 'portal/inclusiveBd/inclusivebd';
        $this->event_template->display($data);
    }

    function paginateIndexBdAllList()
    {
        $cat = $this->session->userdata["current_category"]["category"];

        $data['cate'] = $cat;
        $data['or'] = '';
        $data['src'] = '';

        $result_per_page = 9;

        $config['base_url'] = site_url() . '/InclusiveBd/paginateIndexBdAllList/';

        if(!empty($cat))
        {
            $config['total_rows'] = $this->inclusivebd_model->countItemsByCategory($cat);

        } else {

            $config['total_rows'] = $this->inclusivebd_model->count_items();
        }


        $config['per_page'] = $result_per_page;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
        $this->pagination->initialize($config);

//        echo $config['total_rows']; exit;

        $offset = (!empty($this->uri->segment(3))) ? $this->uri->segment(3) : 0;

//        $offset = 0;

        $data['inclusive_posts_y'] = $this->inclusivebd_model->getAllInclusiveYBd($result_per_page, $offset, $cat);


        $data['category'] =$this->inclusivebd_model->getInclusiveBdCat();
        $data['sliders'] = $this->db->query("SELECT * FROM inclusive_bd_slider")->result();
        $data['slider'] = $this->db->query("SELECT * FROM inclusive_bd_slider")->row();
        $data['category']=$this->inclusivebd_model->getInclusiveBdCat();


        $data['content_view_page'] = 'portal/inclusiveBd/inclusivebd';
        $this->event_template->display($data);
    }



    function searchInclusivePost()
    {
      $userId=$this->getUserId();
      $m=$data['permission']=$this->Portal_model->getUserPermission(6,$userId);

        $cat = $this->input->post('LOOKUP_DATA_ID');
        $order = $this->input->post('order');
        $search = $this->input->post('search');


        $data['cate'] = $cat;
        $data['or'] = $order;
        $data['src'] = $search;


        $result_per_page = 9;

//        $data['blog_posts'] = $this->blog_model->searchPost($blog_type, $order, $search, $result_per_page);

        $data['inclusive_posts_y'] = $this->inclusivebd_model->searchPost($cat, $order, $search, $result_per_page);

        $config['base_url'] = site_url() . '/InclusiveBd/paginateInclusivePosts/';
        $config['total_rows'] = $this->session->userdata["inclusive_row_count"]["num_rows"];
        $config['per_page'] = $result_per_page;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
        $this->pagination->initialize($config);

        $this->session->set_userdata('inclu_pagination', array(
            'category' => $cat,
            'order' => $order,
            'search' => $search,
        ));

        $data['category'] =$this->inclusivebd_model->getInclusiveBdCat();
        $data['sliders'] = $this->db->query("SELECT * FROM inclusive_bd_slider")->result();
        $data['slider'] = $this->db->query("SELECT * FROM inclusive_bd_slider")->row();
        $data['category']=$this->inclusivebd_model->getInclusiveBdCat();

//        echo "<pre>"; print_r($data['inclusive_posts_y'] ); exit;

        $data['content_view_page'] = 'portal/inclusiveBd/inclusivebd';
        $this->event_template->display($data);
    }



    function paginateInclusivePosts()
    {
        $cat = $this->session->userdata["inclu_pagination"]["category"];
        $order = $this->session->userdata["inclu_pagination"]["order"];
        $search = $this->session->userdata["inclu_pagination"]["search"];
        $num_rows = $this->session->userdata["inclusive_row_count"]["num_rows"];

        $data['cate'] = $cat;


        $data['or'] = $order;
        $data['src'] = $search;

        $offset = (!empty($this->uri->segment(3))) ? $this->uri->segment(3) : 0;

        $result_per_page = 9;

        $data['inclusive_posts_y'] = $this->inclusivebd_model->searchPost($cat, $order, $search, $result_per_page, $offset);

        $config['base_url'] = site_url() . '/InclusiveBd/paginateInclusivePosts/';
        $config['total_rows'] = $num_rows;
        $config['per_page'] = $result_per_page;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
        $this->pagination->initialize($config);

        $data['category'] =$this->inclusivebd_model->getInclusiveBdCat();
        $data['sliders'] = $this->db->query("SELECT * FROM inclusive_bd_slider")->result();
        $data['slider'] = $this->db->query("SELECT * FROM inclusive_bd_slider")->row();
        $data['category']=$this->inclusivebd_model->getInclusiveBdCat();

        $data['content_view_page'] = 'portal/inclusiveBd/inclusivebd';
        $this->event_template->display($data);
    }



           /**
     * @methodName report_problem
     * @access
     * @param  none
     * @author Md.Rokibuzzaman <rokibuzzaman@atilimited.net>
     * @return
     */

    public function report_problem($inclusive_id ,$id,$url)
    {
        if(!empty($id))
        {
            $data['user_detail'] = $this->db->query("select u.* from users u
                                                  WHERE u.userId= $id order by u.userId")->row();
        }
          $data['url']=$url;

        $data['link'] = site_url().'/InclusiveBd/inclusiveBdView/'.$inclusive_id;

        $this->load->view('portal/inclusiveBd/report_problem',$data);

    }



        /**
     * @methodName reportProblemFormInsert
     * @access
     * @param  none
     * @author Md.Rokibuzzaman <rokibuzzaman@atilimited.net>
     * @return
     */

    public function reportProblemFormInsert()
    {
         if($_POST)
        {
            $redirectUrl=$this->input->post('redirectUrl');
            $redirect=str_replace("--","/",$redirectUrl);
            $name= $this->input->post('name');
            $email= $this->input->post('email');
            $subject= $this->input->post('subject');
            $message= $this->input->post('message');
            $link= $this->input->post('link');

            if (!empty($name) || !empty($email) || !empty($subject) || !empty($message)) {
        
                $body = "Email From: ".$name."<br>".$message . "<br>" . $link;
                $to = $email;
                require 'gmail_app/class.phpmailer.php';
                $mail = new PHPMailer;
                $mail->IsSMTP();
                $mail->Host = "mail.harnest.com";
                $mail->Port = "465";
                $mail->SMTPAuth = true;
                $mail->Username = "dev@atilimited.net";
                $mail->Password = "@ti321$#";
                $mail->SMTPSecure = 'ssl';
                $mail->From = "noreply@youthopiabangla.org";
                $mail->FromName = "Youthopia.bangla";
                $mail->AddAddress($to);
                $mail->IsHTML(TRUE);
                $mail->Subject = $subject;
                $mail->Body = $body;
                  if ($mail->Send()) {
                     $this->session->set_flashdata('success',' Your message has been received.We will get back to you as soon as possible.');
                }

            }
             else{
          $this->session->set_flashdata('Error','You are not successfully message send!.');
        }
       redirect($redirect);

        }
    }



    /**
     * @methodName fbImo
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function fbImo()
    {
        $control_id = $this->input->post('control_id');
        $value = $this->input->post('value');
        $user_id = $this->input->post('user_id');


        $imo_track = $this->input->post('imo_track');

        if ($value == 'like') {
            $imo_type = 1;
        } elseif ($value == 'haha') {
            $imo_type = 2;
        } elseif ($value == 'love') {
            $imo_type = 3;
        } elseif ($value == 'wow') {
            $imo_type = 4;
        } elseif ($value == 'sad') {
            $imo_type = 5;
        } elseif ($value == 'angry') {
            $imo_type = 6;
        }

        if ($imo_track == 'imo_track' && $value == '') {
            $result = $this->inclusivebd_model->updateInterest(1, $user_id, $control_id);

            $msg = '';
            if (!$result['ack']) {
                $msg = $result['msg'];
            }
            $interestCount = $this->inclusivebd_model->getBlogInterest($control_id);
            if (!empty($msg)) {
                $ack = false;
            } else {
                $ack = true;
            }
            echo json_encode(array('ack' => $ack, 'msg' => $msg, 'count' => $interestCount, 'inclusive_id' => $control_id, 'un_chg' => 1));
        } elseif ($imo_track == 'imo_track') {
            $result = $this->inclusivebd_model->deleteUpdateInterest($imo_type, $user_id, $control_id);
//            echo $result['ack']; exit;
            $msg = '';
            if (!$result['ack']) {
                $msg = $result['msg'];
            }
            $interestCount = $this->inclusivebd_model->getBlogInterest($control_id);

//            echo $interestCount; exit;
            if (!empty($msg)) {
                $ack = false;
            } else {
                $ack = true;
            }
            echo json_encode(array('ack' => $ack, 'msg' => $msg, 'count' => $interestCount, 'inclusive_id' => $control_id, 'un_chg' => 2));
        } else {
            $result = $this->inclusivebd_model->updateInterest2($imo_type, $user_id, $control_id);
            $msg = '';
            if (!$result['ack']) {
                $msg = $result['msg'];
            }
            //get the new request
            $interestCount = $this->inclusivebd_model->getOppInterest($control_id);
            if (!empty($msg)) {
                $ack = false;
            } else {
                $ack = true;
            }
            echo json_encode(array('ack' => $ack, 'msg' => $msg, 'count' => $interestCount, 'inclusive_id' => $control_id));

//        echo 'Control ID: '.$control_id.' Value: '.$value.' User ID: '.$user_id;
        }

    }






    /**
     * @methodName createInclusivebd
     * @access
     * @param  none
     * @author Md.Rokibuzzaman <rokibuzzaman@atilimited.net>
     * @return
     */

    function createInclusivebd()
    {
      $userId=$this->getUserId();
      $m=$data['permission']=$this->Portal_model->getUserPermission(6,$userId);
      if($m->PER==0)
      {
        redirect('InclusiveBd/inclusiveBdAllList');
      }
        $user_id = $this->session->userdata['user_logged_user']['userId'];
         $inclusive_id = $this->uri->segment(3);
         $data['cat_id'] = $inclusive_id;

        //$data['organizationData'] = $this->blog_model->getOrganization($user_id);
        $data['url_types'] = $this->inclusivebd_model->urlTypes();
        $data['category']=$this->inclusivebd_model->getInclusiveBdCat();
        $data['content_view_page'] = 'portal/inclusiveBd/create_inclusivebd';
        $this->event_template->display($data);
    }




        /**
     * @methodName createNewBlogPost
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function createNewInclusiveBdPost()
    {

        $user_id = $this->input->post('user_id');
        $created_by = $this->db->query("SELECT u.userId FROM users u WHERE u.userId = $user_id")->row();
        $title = $this->input->post('title');
        $category = $this->input->post('LOOKUP_DATA_ID');
        $type = $this->input->post('blog_type');
        $audio_url_type = $this->input->post('audio_url_type');
        $audioUrl = $this->input->post('audio_url');
        $video_url_type = $this->input->post('video_url_type');
        $videoUrl = $this->input->post('video_url');
        $post_d = $this->input->post('post_date');
        $post_date = date('Y-m-d', strtotime($post_d));
        $description = $this->input->post('description');
        $type_value = $this->input->post('type_value');
        $type_value_op = $this->input->post('type_value_op');
        $urlType = '';
        $urls = '';
         if($type_value == 'type_value'){
           $urlType = '';

            // call to upload the files


             $newPostData = array(

            'created_by' => $created_by->userId,
            'title' => $title,
            'category_id' =>$category,
            'type' => 'News',
            'urls' => $urls,
            'url_type_id' => $urlType,
            'post_date' => $post_date,
            //'images' => $images,
            'description' => htmlentities($description),

        );
        $this->db->insert('inclusive_bd', $newPostData);
        $this->session->set_flashdata('success','Successfully Created.');
        redirect('InclusiveBd/inclusivebdList', 'refresh');

}

        if ($type == 'audio') {
            $urlType = 2;
            $urls = $audioUrl;
            $newPostData = array(

            'created_by' => $created_by->userId,
            'title' => $title,
            'category_id' =>$category,
            'type' => $type,
            'urls' => $urls,
            'url_type_id' => $urlType,
            'post_date' => $post_date,
            //'images' => $images,
            'description' => htmlentities($description),

        );
        $this->db->insert('inclusive_bd', $newPostData);
        $this->session->set_flashdata('success','Successfully Created.');
        redirect('InclusiveBd/inclusivebdList', 'refresh');
        } elseif ($type == 'video') {
            $urlType = $video_url_type;
            $urls = $videoUrl;
             $newPostData = array(

            'created_by' => $created_by->userId,
            'title' => $title,
            'category_id' =>$category,
            'type' => $type,
            'urls' => $urls,
            'url_type_id' => $urlType,
            'post_date' => $post_date,
            //'images' => $images,
            'description' => htmlentities($description),

        );
        $this->db->insert('inclusive_bd', $newPostData);
        $this->session->set_flashdata('success','Successfully Created.');
        redirect('InclusiveBd/inclusivebdList', 'refresh');

            //echo "string";exit();
        } elseif ($type == 'image') {
            $urlType = null;
            // initialize FileUploader
            $FileUploader = new FileUploader('files', array(
                'limit' => 5,
                'maxSize' => 10,
                'fileMaxSize' => 2,
                'extensions' => ['jpg', 'jpeg', 'png', 'gif', 'bmp', 'JPG', 'JPEG'],
                'required' => true,
                'uploadDir' => 'upload/inclusivebd/',
                'title' => '{timestamp}--{file_name}',
                'replace' => false,
                'listInput' => true,
                'files' => null
            ));

            // call to upload the files
            $data = $FileUploader->upload();

            // if uploaded and success
            if ($data['isSuccess'] && count($data['files']) > 0) {
                // get uploaded files
                $uploadedFiles = $data['files'];
                $images = '';
                $first = true;
                foreach ($uploadedFiles as $uploadedFile) {
                    if ($first == false) {
                        $images .= '___';
                    }
                    $images .= $uploadedFile['name'];
                    $first = false;
                }
            }

             $newPostData = array(

            'created_by' => $created_by->userId,
            'title' => $title,
            'category_id' =>$category,
            'type' => $type,
            'urls' => $urls,
            'url_type_id' => $urlType,
            'post_date' => $post_date,
            'images' => $images,
            'description' => htmlentities($description),

        );
        $this->db->insert('inclusive_bd', $newPostData);
        $this->session->set_flashdata('success','Successfully Created.');
        redirect('InclusiveBd/inclusivebdList', 'refresh');


        }
        elseif($type_value_op == 'type_value_op') {

            $urlType = null;
            // initialize FileUploader
                $FileUploader = new FileUploader('files', array(
                'limit' => 5,
                'maxSize' => 10,
                'fileMaxSize' => 2,
                'extensions' => ['jpg', 'jpeg', 'png', 'gif', 'bmp', 'JPG', 'JPEG'],
                'required' => true,
                'uploadDir' => 'upload/inclusivebd/',
                'title' => '{timestamp}--{file_name}',
                'replace' => false,
                'listInput' => true,
                'files' => null
            ));

            // call to upload the files
            $data = $FileUploader->upload();

            // if uploaded and success
            if ($data['isSuccess'] && count($data['files']) > 0) {
                // get uploaded files
                $uploadedFiles = $data['files'];
                $images = '';
                $first = true;
                foreach ($uploadedFiles as $uploadedFile) {
                    if ($first == false) {
                        $images .= '___';
                    }
                    $images .= $uploadedFile['name'];
                    $first = false;
                }
            }

             $newPostData = array(

            'created_by' => $created_by->userId,
            'title' => $title,
            'category_id' =>$category,
            'type' => $type,
            'urls' => $urls,
            'url_type_id' => $urlType,
            'post_date' => $post_date,
            'images' => $images,
            'description' => htmlentities($description),

        );
             //print_r($newPostData);exit();
        $this->db->insert('inclusive_bd', $newPostData);
        $this->session->set_flashdata('success','Successfully Created.');
        redirect('InclusiveBd/inclusivebdList', 'refresh');




}



        // $newPostData = array(

        //     'created_by' => $created_by->userId,
        //     'title' => $title,
        //     'category_id' =>$category,
        //     'type' => $type,
        //     'urls' => $urls,
        //     'url_type_id' => $urlType,
        //     'post_date' => $post_date,
        //     //'images' => $images,
        //     'description' => htmlentities($description),

        // );
        // //print_r($newPostData);exit();

        // $this->db->insert('inclusive_bd', $newPostData);
        // $this->session->set_flashdata('success','Successfully Created.');
        // redirect('InclusiveBd/inclusivebdList', 'refresh');
    }



     /**
     * @methodName editInclusiveBdPost
     * @access
     * @param  none
     * @author Md.Rokibuzzaman <rokibuzzaman@atilimited.net>
     * @return
     */

    function editInclusiveBdPost()
    {
        $inclusive_id = $this->uri->segment(3);
        $user_id = $this->session->userdata['user_logged_user']['userId'];

        $data['incliusiveBds'] = $this->inclusivebd_model->getInclusiveBdData($inclusive_id);
        $data['url_types'] = $this->inclusivebd_model->urlTypes();
        $data['category']=$this->inclusivebd_model->getInclusiveBdCat();

        $data['incliusiveBds'] = $this->inclusivebd_model->getInclusiveBdData($inclusive_id);
        $data['url_types'] = $this->inclusivebd_model->urlTypes();
        $data['category']=$this->inclusivebd_model->getInclusiveBdCat();


        $data['content_view_page'] = 'portal/inclusiveBd/edit_inclusivebd';
        $this->event_template->display($data);


    }


        function insertEditedInclusiveBdPost1()
    {
        $inclusive_id = $this->uri->segment(3);



        $user_id = $this->input->post('user_id');
        $title = $this->input->post('title');
        $type = $this->input->post('blog_type');
        $category = $this->input->post('LOOKUP_DATA_ID');
        $audio_url_type = $this->input->post('audio_url_type');
        $audioUrl = $this->input->post('audio_url');
        $video_url_type = $this->input->post('video_url_type');
        $videoUrl = $this->input->post('video_url');
        $post_d = $this->input->post('post_date');
        $post_date = date('Y-m-d', strtotime($post_d));
        $description = htmlentities($this->input->post('description'));
        $urlType = '';
        $urls = '';

        if ($type == 'audio') {
            $urlType = 2;

            $urls = $audioUrl;
        } elseif ($type == 'video') {
            $urlType = 1;
            $urls = $videoUrl;
        }

        $newPostData = array(

            'created_by' => $user_id,
            'title' => $title,
            'category_id' => $category,
            'type' => $type,
            'urls' => $urls,
            'url_type_id' => $urlType,
            'post_date' => $post_date,
            'description' => $description,

        );

        $this->db->update('inclusive_bd', $newPostData, array('id' => $inclusive_id));
        $this->session->set_flashdata('success','Successfully Updated.');
        redirect('InclusiveBd/inclusivebdList');
    }




        function insertEditedInclusiveBdPost()
    {
        $inclusive_id = $this->uri->segment(3);
        $user_id = $this->input->post('user_id');
        $created_by = $this->db->query("SELECT u.userId FROM users u WHERE u.userId = $user_id")->row();
        $title = $this->input->post('title');
        $category = $this->input->post('LOOKUP_DATA_ID');
        $type = $this->input->post('blog_type');
        $audio_url_type = $this->input->post('audio_url_type');
        $audioUrl = $this->input->post('audio_url');
        $video_url_type = $this->input->post('video_url_type');
        $videoUrl = $this->input->post('video_url');
        $post_d = $this->input->post('post_date');
        $post_date = date('Y-m-d', strtotime($post_d));
        $description = $this->input->post('description');
        $type_value = $this->input->post('type_value');
        $type_value_op = $this->input->post('type_value_op');
        $hasFile = $this->input->post('fileuploader-list-files');
        $ext_image = $this->input->post('ext_image');
        $ext_image2 = $this->input->post('ext_image2');

        //echo $ext_image;exit();
        $urlType = '';
        $urls = '';
         if($type_value == 'type_value'){
           $urlType = '';

            // call to upload the files


             $newPostData = array(

            'created_by' => $created_by->userId,
            'title' => $title,
            'category_id' =>$category,
            'type' => 'News',
            'urls' => $urls,
            'url_type_id' => $urlType,
            'post_date' => $post_date,
            //'images' => $images,
            'description' => htmlentities($description),

        );
        $this->db->update('inclusive_bd', $newPostData, array('id' => $inclusive_id));
        $this->session->set_flashdata('success','Successfully Updated.');
        redirect('InclusiveBd/inclusivebdList');

}

        if ($type == 'audio') {

            //unlink image
            if(!empty($ext_image))
            {
                $arr = explode('___', $ext_image);
                $imgPath = 'upload/inclusivebd/';

                for($i=0; $i<count($arr); $i++)
                {
                    unlink($imgPath . $arr[$i]);
                }
            }

            $urlType = 2;
            $urls = $audioUrl;
             $newPostData = array(

            'created_by' => $created_by->userId,
            'title' => $title,
            'category_id' =>$category,
            'type' => $type,
            'urls' => $urls,
            'url_type_id' => $urlType,
            'post_date' => $post_date,
            'images' => '',
            'description' => htmlentities($description),

        );
        $this->db->update('inclusive_bd', $newPostData, array('id' => $inclusive_id));
        $this->session->set_flashdata('success','Successfully Updated.');
        redirect('InclusiveBd/inclusivebdList');

        } elseif ($type == 'video') {

            //unlink image
            if(!empty($ext_image))
            {
                $arr = explode('___', $ext_image);
                $imgPath = 'upload/inclusivebd/';

                for($i=0; $i<count($arr); $i++)
                {
                    unlink($imgPath . $arr[$i]);
                }
            }

            $urlType = $video_url_type;
            $urls = $videoUrl;
             $newPostData = array(

            'created_by' => $created_by->userId,
            'title' => $title,
            'category_id' =>$category,
            'type' => $type,
            'urls' => $urls,
            'url_type_id' => $urlType,
            'post_date' => $post_date,
            'images' => '',
            'description' => htmlentities($description),

        );
        $this->db->update('inclusive_bd', $newPostData, array('id' => $inclusive_id));
        $this->session->set_flashdata('success','Successfully Updated.');
        redirect('InclusiveBd/inclusivebdList');

            //echo "string";exit();
        } elseif ($type == 'image') {
            $urlType = null;
            // initialize FileUploader
            if (!empty($hasFile)) {
                      $FileUploader = new FileUploader('files', array(
                'limit' => 5,
                'maxSize' => 10,
                'fileMaxSize' => 2,
                'extensions' => ['jpg', 'jpeg', 'png', 'gif', 'bmp', 'JPG', 'JPEG'],
                'required' => true,
                'uploadDir' => 'upload/inclusivebd/',
                'title' => '{timestamp}--{file_name}',
                'replace' => false,
                'listInput' => true,
                'files' => null
            ));

            // call to upload the files
            $data = $FileUploader->upload();

            // if uploaded and success
            if ($data['isSuccess'] && count($data['files']) > 0) {
                // get uploaded files
                $uploadedFiles = $data['files'];
                $images = '';
                $first = true;
                foreach ($uploadedFiles as $uploadedFile) {
                    if ($first == false) {
                        $images .= '___';
                    }
                    $images .= $uploadedFile['name'];
                    $first = false;
                }
            }



               if ($ext_image != '' || $ext_image != null) {
                $newPostData = array(

            'created_by' => $created_by->userId,
            'title' => $title,
            'category_id' =>$category,
            'type' => $type,
            'urls' => $urls,
            'url_type_id' => $urlType,
            'post_date' => $post_date,
            'images' => $ext_image . '___' . $images,
            'description' => htmlentities($description)

                );
            } else {

                   $newPostData = array(

                       'created_by' => $created_by->userId,
                       'title' => $title,
                       'category_id' =>$category,
                       'type' => $type,
                       'urls' => $urls,
                       'url_type_id' => $urlType,
                       'post_date' => $post_date,
                       'images' =>  $images,
                       'description' => htmlentities($description)

                   );

               }

                $this->db->update('inclusive_bd', $newPostData, array('id' => $inclusive_id));
                $this->session->set_flashdata('success','Successfully Updated.');
                redirect('InclusiveBd/inclusivebdList');



        }
            else {
                $newPostData = array(

            'created_by' => $created_by->userId,
            'title' => $title,
            'category_id' =>$category,
            'type' => $type,
            'urls' => $urls,
            'url_type_id' => $urlType,
            'post_date' => $post_date,
            //'images' => $images,
            'description' => htmlentities($description)

                );
            }



        $this->db->update('inclusive_bd', $newPostData, array('id' => $inclusive_id));
        $this->session->set_flashdata('success','Successfully Updated.');
        redirect('InclusiveBd/inclusivebdList');


        }
        elseif($type_value_op == 'type_value_op') {
            //echo 'ffdfd';exit();

            $urlType = null;
            // initialize FileUploader
                if (!empty($hasFile)) {


                $FileUploader = new FileUploader('files', array(
                'limit' => 5,
                'maxSize' => 10,
                'fileMaxSize' => 2,
                'extensions' => ['jpg', 'jpeg', 'png', 'gif', 'bmp', 'JPG', 'JPEG'],
                'required' => true,
                'uploadDir' => 'upload/inclusivebd/',
                'title' => '{timestamp}--{file_name}',
                'replace' => false,
                'listInput' => true,
                'files' => null
            ));

            // call to upload the files
            $data = $FileUploader->upload();

            // if uploaded and success
            if ($data['isSuccess'] && count($data['files']) > 0) {
                // get uploaded files
                $uploadedFiles = $data['files'];
                $images = '';
                $first = true;
                foreach ($uploadedFiles as $uploadedFile) {
                    if ($first == false) {
                        $images .= '___';
                    }
                    $images .= $uploadedFile['name'];
                    $first = false;
                }
            }




               if ($ext_image2 != '' || $ext_image2 != null) {
                 //echo 'ffdfd';exit();
                $newPostData = array(

            'created_by' => $created_by->userId,
            'title' => $title,
            'category_id' =>$category,
            'type' => $type,
            'urls' => $urls,
            'url_type_id' => $urlType,
            'post_date' => $post_date,
            'images' => $ext_image2 . '___' . $images,
            'description' => htmlentities($description)

                );
               // print_r($newPostData);exit();

            }



        }
         else {
                $newPostData = array(

            'created_by' => $created_by->userId,
            'title' => $title,
            'category_id' =>$category,
            'type' => $type,
            'urls' => $urls,
            'url_type_id' => $urlType,
            'post_date' => $post_date,
            //'images' => $images,
            'description' => htmlentities($description)

                );
            }

        $this->db->update('inclusive_bd', $newPostData, array('id' => $inclusive_id));
        $this->session->set_flashdata('success','Successfully Updated.');
        redirect('InclusiveBd/inclusivebdList');

}


    }






    /**
     * @methodName searchInclusiveBdPost
     * @access
     * @param  none
     * @author Md.Rokibuzzaman <rokibuzzaman@atilimited.net>
     * @return
     */

    function searchInclusiveBdPost1()
    {
        if (!isset($this->session->userdata['user_logged_user'])) {
            redirect('InclusiveBd/InclusiveBd', 'refresh');
        }

        $user_id = $this->session->userdata['user_logged_user']['userId'];
        //echo $user_id;exit();
        $blog_type = $this->input->post('blog_type');
        $order = $this->input->post('order');
        $search = $this->input->post('search');

        $data['blog_ty'] = $blog_type;
        $data['or'] = $order;
        $data['src'] = $search;


        $data['incliusiveBds'] = $this->inclusivebd_model->searchPost($blog_type, $order,$search,$user_id );

        $data['content_view_page'] = 'portal/inclusiveBd/search_inclusive_bd';
        $this->event_template->display($data);
    }

        function searchInclusiveBdPost()
    {

         if (!isset($this->session->userdata['user_logged_user'])) {
            redirect('InclusiveBd/inclusiveBdAllList', 'refresh');
        }

        $user_id = $this->session->userdata['user_logged_user']['userId'];



        $category = $this->input->post('LOOKUP_DATA_ID');
        $category_data = $this->db->get_where('sa_lookup_data', array('LOOKUP_DATA_ID' => $category))->row();
        //$this->pr($category_data);
        if (!empty($category_data))
        {
        $data['category_name'] = $category_data->LOOKUP_DATA_NAME;
        }
        else
        {
        $data['category_name'] ='';

        }


        //$data['category_name'] = $category_data->LOOKUP_DATA_NAME;
        $order = $this->input->post('order');
        $search = $this->input->post('search');
        $data['category']=$this->inclusivebd_model->getInclusiveBdCat();
        $data['inclusive_search_list'] = $this->inclusivebd_model->getAllInclusiveSearchList();
        //$data['category_name'] = $category_data->LOOKUP_DATA_NAME;

        // echo "<pre>"; print_r($data['category']); exit;

        $data['category_id'] = $category;

        $data['or'] = $order;
        $data['src'] = $search;

        $data['incliusiveBds'] = $this->inclusivebd_model->searchAllPostInclusive($category, $order,$search,$user_id);

        $data['content_view_page'] = 'portal/inclusiveBd/search_inclusive_bd';
        $this->event_template->display($data);

     }




     /**
     * @methodName searchInclusiveBdPost
     * @access
     * @param  none
     * @author Md.Rokibuzzaman <rokibuzzaman@atilimited.net>
     * @return
     */
    private function pr($data)
    {
        echo "<pre>";
        print_r($data);
        exit;
    }
    function searchInclusiveBdAllPost()
    {

        $category = $this->input->post('LOOKUP_DATA_ID');
        $category_data = $this->db->get_where('sa_lookup_data', array('LOOKUP_DATA_ID' => $category))->row();
        //$this->pr($category_data);
        if (!empty($category_data))
        {
        $data['category_name'] = $category_data->LOOKUP_DATA_NAME;
        }
        else
        {
        $data['category_name'] ='';

        }


        //$data['category_name'] = $category_data->LOOKUP_DATA_NAME;
        $order = $this->input->post('order');
        $search = $this->input->post('search');
        $data['category']=$this->inclusivebd_model->getInclusiveBdCat();
        //$data['category_name'] = $category_data->LOOKUP_DATA_NAME;

        // echo "<pre>"; print_r($data['category']); exit;

        $data['category_id'] = $category;

        $data['or'] = $order;
        $data['src'] = $search;

        $result_per_page = 3;

        $data['inclusive_posts'] = $this->inclusivebd_model->searchAllPost2($category, $order,$search, $result_per_page);

        $config['base_url'] = site_url() . '/inclusiveBd/paginateInclusiveBd/';
        $config['total_rows'] = $this->session->userdata["inclusive_row_count"]["num_rows"];

//        echo $config['total_rows']; exit;

        $config['per_page'] = $result_per_page;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);

//        echo $config['num_links']; exit;

        $this->pagination->initialize($config);

//        echo "<pre>"; print_r($data['inclusive_posts']); exit;

        $data['content_view_page'] = 'portal/inclusiveBd/search_inclusiveall_bd';
        $this->event_template->display($data);

     }




    /**
     * @methodName deleteInclusiveBdPost
     * @access
     * @param  none
     * @author Md.Rokibuzzaman <rokibuzzaman@atilimited.net>
     * @return
     */

    function deleteInclusiveBdPost()
    {
        $inclusive_id = $this->uri->segment(3);

        $result = $this->inclusivebd_model->deletePost($inclusive_id);

        if ($result == "Deleted") {
            $this->session->set_flashdata('error','Successfully Deleted.');
            redirect('InclusiveBd/inclusivebdList', 'refresh');
        }
    }




     /**
     * @methodName inclusiveBdView
     * @access
     * @param  none
     * @author Md.Rokibuzzaman <rokibuzzaman@atilimited.net>
     * @return
     */

    function inclusiveBdView()
    {
        $inclusive_id = $this->uri->segment(3);
        $data['incliusiveDetail'] = $this->inclusivebd_model->getInclusiveBdData($inclusive_id);

//        echo "<pre>"; print_r($data['incliusiveDetail']); exit;


        //$data['organizationDetails'] = $this->blog_model->getOrganizaionData($data['blogDetails']->organization_id);
        //$data['organizationLogo'] = $this->blog_model->organizationLogo($data['organizationDetails']->userId);
        $data['inclusiveComments'] = $this->inclusivebd_model->getInclusiveComment($inclusive_id);
        $this->inclusivebd_model->increaseView($inclusive_id);
        $data['content_view_page'] = 'portal/inclusiveBd/inclusivebd_view';
        $this->event_template->display($data);
    }



        /**
     * @methodName submitComment
     * @access
     * @param  none
     * @author Md.Rokibuzzaman <rokibuzzaman@atilimited.net>
     * @return
     */

    function submitComment()
    {
        $inclusive_id = $this->uri->segment(3);

        $user_id = $this->session->userdata['user_logged_user']['userId'];
        $description = $this->input->post('description');

        $result = $this->inclusivebd_model->addComment($description, $inclusive_id, $user_id);

        if ($result == "Inserted") {
            redirect('InclusiveBd/inclusiveBdView/'.$inclusive_id, 'refresh');
        }
    }




    /**
     * @methodName deleteComment
     * @access
     * @param  none
     * @author Md.Rokibuzzaman <rokibuzzaman@atilimited.net>
     * @return
     */

    function deleteComment()
    {
        $inclusive_id = $this->uri->segment(3);
        $comment_id = $this->uri->segment(4);
        $user_id = $this->session->userdata['user_logged_user']['userId'];

        $result = $this->inclusivebd_model->deleteComment($comment_id, $user_id);

        if($result == 'Deleted')
        {
            redirect('InclusiveBd/inclusiveBdView/'.$inclusive_id, 'refresh');
        }
    }






       /**
     * @methodName InclusiveBd
     * @access
     * @param  none
     * @author Md.Rokibuzzaman <rokibuzzaman@atilimited.net>
     * @return
     */

    // function InclusiveBd()
    // {
    //     $data['blog_posts'] = $this->blog_model->getAllBlogPosts();

    //     $data['content_view_page'] = 'portal/blog/media_blogs';
    //     $this->event_template->display($data);
    // }


    /**
     * @methodName mediaBlogs
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function mediaBlogs()
    {
        $data['blog_posts'] = $this->blog_model->getAllBlogPosts();

        $data['content_view_page'] = 'portal/blog/media_blogs';
        $this->event_template->display($data);
    }



    /**
     * @methodName blogView
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function blogView()
    {
        $blog_id = $this->uri->segment(3);
        $data['blogDetails'] = $this->blog_model->getBlogData($blog_id);
        $data['organizationDetails'] = $this->blog_model->getOrganizaionData($data['blogDetails']->organization_id);
        $data['organizationLogo'] = $this->blog_model->organizationLogo($data['organizationDetails']->userId);
        $data['blogComments'] = $this->blog_model->getBlogComment($blog_id);

        $data['content_view_page'] = 'portal/blog/blog_view';
        $this->event_template->display($data);
    }

    /**
     * @methodName calSearch
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function calSearch()
    {
        $action = (!empty($_POST['action'])) ? $_POST['action'] : '';

        switch ($action) {

            case 'blogInterest':

                $blog_type = $_POST['blog_type'];
                $user_id = $_POST['user_id'];
                $blog_id = $_POST['blog_id'];

                $result = $this->inclusivebd_model->updateInclusiveBdInterest($blog_type, $user_id, $blog_id);
                $msg = '';
                if (!$result['ack']) {
                    $msg = $result['msg'];
                }
                //get the new request
                $interestCount = $this->inclusivebd_model->getInclusiveBdInterest($blog_id);
                if (!empty($msg)) {
                    $ack = false;
                } else {
                    $ack = true;
                }
                echo json_encode(array('ack' => $ack, 'msg' => $msg, 'count' => $interestCount, 'blog_id' => $blog_id));
                exit;
                break;

                case 'blogImageDelete':

                $blog_id = $_POST['blog_id'];
                $image_name = $_POST['img_name'];
                $result = $this->inclusivebd_model->deleteBlogImage($blog_id, $image_name);
                $msg = $result['msg'];
                if (!$result['ack']) {
                    $ack = false;
                } else {
                    $ack = true;
                }
                echo json_encode(array('ack' => $ack, 'msg' => $msg, 'blog_id' => $blog_id));
                exit;
                break;

        }

    }

    /**
     * @methodName blogList
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function blogList()
    {
        if (!isset($this->session->userdata['user_logged_user'])) {
            redirect('blog/mediaBlogs', 'refresh');
        }

        $user_id = $this->session->userdata['user_logged_user']['userId'];

        $data['blogs'] = $this->blog_model->getBlog($user_id);

        $data['content_view_page'] = 'portal/blog/blog_list';
        $this->event_template->display($data);
    }

    /**
     * @methodName createBlog
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function createBlog()
    {
        $user_id = $this->session->userdata['user_logged_user']['userId'];

        $data['organizationData'] = $this->blog_model->getOrganization($user_id);
        $data['url_types'] = $this->blog_model->urlTypes();

        $data['content_view_page'] = 'portal/blog/create_blog';
        $this->event_template->display($data);
    }


    /**
     * @methodName deleteBlogPost
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function deleteBlogPost()
    {
        $blog_id = $this->uri->segment(3);

        $result = $this->blog_model->deletePost($blog_id);

        if ($result == "Deleted") {
            redirect('blog/blogList', 'refresh');
        }
    }

    /**
     * @methodName editBlogPost
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function editBlogPost()
    {
        $blog_id = $this->uri->segment(3);

        $user_id = $this->session->userdata['user_logged_user']['userId'];
        $data['organizationData'] = $this->blog_model->getOrganization($user_id);
        $data['blogData'] = $this->blog_model->getBlogData($blog_id);
        $data['url_types'] = $this->blog_model->urlTypes();

        $data['content_view_page'] = 'portal/blog/edit_blog';
        $this->event_template->display($data);


    }

    /**
     * @methodName insertEditedBlogPost
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function insertEditedBlogPost()
    {
        $blog_id = $this->uri->segment(3);

        $org_id = $this->input->post('org_id');
        $title = $this->input->post('title');
        $type = $this->input->post('blog_type');
        $audio_url_type = $this->input->post('audio_url_type');
        $audioUrl = $this->input->post('audio_url');
        $video_url_type = $this->input->post('video_url_type');
        $videoUrl = $this->input->post('video_url');
        $post_date = $this->input->post('post_date');
        $description = htmlentities($this->input->post('description'));
        $urlType = '';
        $urls = '';

        if ($type == 'audio') {
            $urlType = 2;
            $urls = $audioUrl;
        } elseif ($type == 'video') {
            $urlType = 1;
            $urls = $videoUrl;
        }

        $newPostData = array(

            'organization_id' => $org_id,
            'title' => $title,
            'type' => $type,
            'urls' => $urls,
            'url_type_id' => $urlType,
            'post_date' => $post_date,
            'description' => $description,

        );

        $this->db->update('blog', $newPostData, array('id' => $blog_id));
        $this->session->set_flashdata('success','Successfully Updated.');
        redirect('blog/blogList');
    }






}
