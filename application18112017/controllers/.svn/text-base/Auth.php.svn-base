<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller
{

    /**
     * @methodName login()
     * @access
     * @param
     * @author      Rakib Roni <rakib@atilimited.net>
     * @return      login template
     */
    function login()
    {
        if ($this->session->userdata('logged_in') == TRUE) {
            redirect('admin/index', 'refresh');
        }
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');

        $this->form_validation->set_rules('user_name', 'Username', 'required|callback_username_check');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('login');
        } else {
            $session_info = $this->session->userdata('logged_in');
            if ($session_info['ACTIVE_STATUS'] == 1) {
                $this->session->set_flashdata('Info', "Welcome To Student Management System Application!");
                redirect('admin/index', 'refresh');
            }
        }
    }

    /**
     * @methodName checkDatabase()
     * @access
     * @param
     * @author      Rakib Roni <rakib@atilimited.net>
     * @return      check user
     */
    function username_check($username)
    {
        $password = $this->input->post('password');
        $check_user = $this->auth_model->login($username);
        if (!empty($check_user)) {
            if (password_verify($password, $check_user->USERPW)) {
                $sess_array = array(
                    'USER_ID' => $check_user->USER_ID,
                    'USERNAME' => $check_user->USERNAME,
                    'USERPW' => $check_user->USERPW,
                    'ORG_ID' => $check_user->ORG_ID,
                    'USERGRP_ID' => $check_user->USERGRP_ID,
                    'USERLVL_ID' => $check_user->USERLVL_ID,
                    'ACTIVE_STATUS' => $check_user->ACTIVE_STATUS,
                    'USER_TYPE' => $check_user->USER_TYPE,
                    'USER_IMG' => $check_user->USER_IMG,
                    'DEPT_ID' => $check_user->DEPT_ID,
                    'DESIGNATION_ID' => $check_user->DESIGNATION_ID,
                    'IS_ADMIN' => $check_user->IS_ADMIN,
                    'PKPLUS' => date('y') . '000000000000',
                    'FULL_NAME' => $check_user->FULL_NAME
                );
                if ($check_user->ACTIVE_STATUS != 0) {
                    $this->session->set_userdata('logged_in', $sess_array);
                    return TRUE;
                } else {
                    $this->form_validation->set_message('username_check', 'User Account Still Inactivated By the Admin.');
                    return false;
                }
            } else {
                $this->form_validation->set_message('username_check', "The Password you entered don't match");
                return FALSE;
            }
        } else {
            $this->form_validation->set_message('username_check', "The Username you entered don't match");
            return FALSE;
        }
    }

    /**
     * @methodName logut()
     * @access
     * @param
     * @author     Rakib Roni <rakib@atilimited.net>
     * @return     logut user
     */
    function logout()
    {
        $this->session->unset_userdata('logged_in');
        redirect('auth/login', 'refresh');
    }

    /**
     * @methodName  studentLogin()
     * @access
     * @param
     * @author      Rakib Roni <rakib@atilimited.net>
     * @return      Student login template
     */
    function studentLogin()
    {
        if ($this->session->userdata('stu_logged_in') == TRUE) {
            redirect('student/index', 'refresh');
        }
        $this->form_validation->set_rules('stu_user_name', 'Username', 'required|callback_checkStudentLogin');
        $this->form_validation->set_rules('stu_password', 'Password', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('student_login');
        } else {
            $session_info = $this->session->userdata('stu_logged_in');

            if ($session_info['ACTIVE_STATUS'] == 1) {
                //  $this->session->set_flashdata('Info', "Welcome To KYAU - Student Panel!");
                redirect('student/studentDetails', 'refresh');
            }
        }
    }

    /**
     * @methodName checkStudentLogin()
     * @access
     * @param
     * @author      Rakib Roni <rakib@atilimited.net>
     * @return      check studnet login permission
     */
    function checkStudentLogin($stu_user_name)
    {

        $password = $this->input->post('stu_password');
        //$hashPassword = $this->utilities->get_field_value_by_attribute('sa_users', 'USERPW', array('USERNAME' => $username));
        $result = $this->auth_model->stuLogin($stu_user_name, $password);

        //if (password_verify($password, $hashPassword)) {
        if (!empty($result)) {
            $sess_array = array(
                'STUDENT_ID' => $result->STUDENT_ID,
                'ROLL_NO' => $result->ROLL_NO,
                'BATCH_ID' => $result->BATCH_ID,
                'FULL_NAME_EN' => $result->FULL_NAME_EN,
                'PASSWORD' => $result->PASSWORD,
                'STUD_PHOTO' => $result->STUD_PHOTO,
                'FATHER_NAME' => $result->FATHER_NAME,
                'MOTHER_NAME' => $result->MOTHER_NAME,
                'USER_TYPE' => 'Student',
                'ACTIVE_STATUS' => $result->ACTIVE_STATUS
            );
            if ($result->ACTIVE_STATUS != 0) {
                $this->session->set_userdata('stu_logged_in', $sess_array);
                return TRUE;
            } else {
                $this->form_validation->set_message('check_database', 'User Account Still  Inactivated By the Admin.');
                return false;
            }
        } else {
            $this->form_validation->set_message('check_database', 'Whoops! We didn\'t recognise your username or password. Please try again.');
            return false;
        }
    }

    /**
     * @methodName stuLogout()
     * @access
     * @param
     * @author      Rakib Roni <rakib@atilimited.net>
     * @return      this function use for student log out
     */
    function stuLogout()
    {
        $this->session->unset_userdata('stu_logged_in');
        redirect('auth/studentLogin', 'refresh');
    }

    /**
     * @methodName  parentsLogin()
     * @access
     * @param
     * @author      Rakib Roni <rakib@atilimited.net>
     * @return      parents login page
     */
    function parentsLogin()
    {
        if ($this->session->userdata('parents_logged_in') == TRUE) {
            redirect('parents/index', 'refresh');
        }
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');

        $this->form_validation->set_rules('parents_user_name', 'Username', 'required|callback_checkParentsLogin');
        $this->form_validation->set_rules('parents_password', 'Password', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('parents_login');
        } else {
            $session_info = $this->session->userdata('parents_logged_in');
            if ($session_info['ACTIVE_STATUS'] == 1) {
                $this->session->set_flashdata('Info', "Welcome To KYAU - Parents Panel!");
                redirect('parents/index', 'refresh');
            }
        }
    }

    /**
     * @methodName checkParentsLogin()
     * @access
     * @param
     * @author      Rakib Roni <rakib@atilimited.net>
     * @return      check parents credential and create parents session
     */
    function checkParentsLogin($parents_user_name)
    {
        $password = $this->input->post('parents_password');
        $check_user = $this->auth_model->parentsLogin($parents_user_name);
        //echo $check_user;exit;
        if (!empty($check_user)) {
            if ($password === $check_user->PASSWORD) {


                $sess_array = array(
                    'PARENT_PRO_ID' => $check_user->PARENT_PRO_ID,
                    'MOBILE_NO' => $check_user->MOBILE_NO,
                    'ACTIVE_STATUS' => $check_user->ACTIVE_STATUS,
                    'USER_TYPE' =>'Parents'
                );
                if ($check_user->ACTIVE_STATUS != 0) {
                    $this->session->set_userdata('parents_logged_in', $sess_array);
                    return TRUE;
                } else {
                    $this->form_validation->set_message('checkParentsLogin', 'User Account Still Inactivated By the Admin.');
                    return false;
                }
            } else {
                $this->form_validation->set_message('checkParentsLogin', "The Password you entered don't match");
                return FALSE;
            }
        } else {
            $this->form_validation->set_message('checkParentsLogin', "The Username you entered don't match");
            return FALSE;
        }
    }
    function parentsLogout()
    {
        $this->session->unset_userdata('parents_logged_in');
        redirect('auth/parentsLogin', 'refresh');
    }


    function teacher()
    {
        if ($this->session->userdata('tr_logged_in') == TRUE) {
            redirect('teacher/index', 'refresh');
        }
        $this->form_validation->set_rules('tr_user_name', 'Username', 'required|callback_checkteacherLogin');
        $this->form_validation->set_rules('tr_password', 'Password', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('teacher_login');
        } else {
            $session_info = $this->session->userdata('tr_logged_in');

            if ($session_info['ACTIVE_STATUS'] == 1) {
                //  $this->session->set_flashdata('Info', "Welcome To KYAU - Student Panel!");
                redirect('teacher/index', 'refresh');
            }
        }
    }

    function checkteacherLogin($tr_user_name)
    {

        $password = $this->input->post('tr_password');
        //$hashPassword = $this->utilities->get_field_value_by_attribute('sa_users', 'USERPW', array('USERNAME' => $username));
        $result = $this->auth_model->trLogin($tr_user_name, $password);

        //if (password_verify($password, $hashPassword)) {
        if (!empty($result)) {
            $sess_array = array(
                'TEACHER_ID' => $result->TEACHER_ID,
                'USER_NAME' => $result->USER_NAME,
                'FULL_NAME_EN' => $result->FULL_NAME_EN,
                'PASSWORD' => $result->PASSWORD,
                'TEACHER_PHOTO' => $result->TEACHER_PHOTO,
                'USER_TYPE' => 'Teacher',
                'ACTIVE_STATUS' => $result->ACTIVE_STATUS
            );
            if ($result->ACTIVE_STATUS != 0) {
                $this->session->set_userdata('tr_logged_in', $sess_array);
                return TRUE;
            } else {
                $this->form_validation->set_message('check_database', 'User Account Still  Inactivated By the Admin.');
                return false;
            }
        } else {
            $this->form_validation->set_message('check_database', 'Whoops! We didn\'t recognise your username or password. Please try again.');
            return false;
        }
    }

    function trLogout()
    {
        $this->session->unset_userdata('tr_logged_in');
        redirect('auth/teacher', 'refresh');
    }

}
