jQuery(document).ready(function(){

    $('#formIndividual').on('submit',function(e){

        var fd = new FormData(this);
        //var submit=$('#submitIndividual');
            $.ajax({
                type: "POST",
                url: "process.php",
                data: fd,
                contentType:false,
                cache: false,
                processData: false,
                success: function (data) {
                     $('#messageIndividual').html(data);
                }
            });

        e.preventDefault();
        return false;


    });
    // Function to preview image after validation
    $(function() {
        $("#file").change(function() {
            $("#messageIndividual").empty(); // To remove the previous error message
            var file = this.files[0];
            var imagefile = file.type;
            var match= ["image/jpeg","image/png","image/jpg"];
            if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
            {

                $("#messageIndividual").html("<p id='error'>Please Select A valid Image File</p>"+"<h4>Note</h4>"+"<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                return false;
            }
            else
            {
                var reader = new FileReader();
                reader.onload = imageIsLoaded;
                reader.readAsDataURL(this.files[0]);
            }
        });
    });
    function imageIsLoaded(e) {
        $("#file").css("color","green");
        $('#image_previewIndividual').css("display", "block");
        $('#previewing').attr('src', e.target.result);
        $('#previewing').attr('width', '150px');
        $('#previewing').attr('height', '150px');
    };

    $('#formOrganization').on('submit',function(e){
        var fd = new FormData(this);
        //var submit=$('#submitOrganization');

        $.ajax({
            type:"POST",
            url:"process.php",
            data:fd,
            contentType: false,
            cache:false,
            processData:false,
            success:function(data)
            {
                $('#messageOrganization').html(data);
            },
            error:function(err)
            {
                $("#messageOrganization").html(err);
            }

        });
        e.preventDefault();
        return false;
    });
    // Function to preview image after validation
    $(function() {
        $("#file").change(function() {
            $("#messageOrganization").empty(); // To remove the previous error message
            var file = this.files[0];
            var imagefile = file.type;
            var match= ["image/jpeg","image/png","image/jpg"];
            if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
            {

                $("#messageOrganization").html("<p id='error'>Please Select A valid Image File</p>"+"<h4>Note</h4>"+"<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                return false;
            }
            else
            {
                var reader = new FileReader();
                reader.onload = imageIsLoadedOrganization;
                reader.readAsDataURL(this.files[0]);
            }
        });
    });
    function imageIsLoadedOrganization(e) {
        $("#file").css("color","green");
        $('#image_previewOrganization').css("display", "block");
        $('#previewingOrganization').attr('src', e.target.result);
        $('#previewingOrganization').attr('width', '150px');
        $('#previewingOrganization').attr('height', '150px');
    };

    //ajax code for login

    $('#formLogin').on('submit',function(e){
        var fd = new FormData(this);
        $.ajax({
            type: "POST",
            url: "process.php",
            data: fd,
            contentType:false,
            cache: false,
            processData: false,
            success: function (data) {
                $('#messageLogin').html(data);
            },
            error:function(err)
            {
                $("#messageLogin").html(err);
            }
        });

        e.preventDefault();
        return false;


    });
    //for foget pass
    $('#forgetPassForm').on('submit',function(e){

        var fd = new FormData(this);
        $.ajax({
            type: "POST",
            url: "process.php",
            data: fd,
            contentType:false,
            cache: false,
            processData: false,
            success: function (data) {
                $('#forgetPassMessage').html(data);
            },
            error:function(err)
            {
                $("#forgetPassMessage").html(err);
            }
        });

        e.preventDefault();
        return false;


    });

    //update profile
    $('#formUpdateIndividual').on('submit',function(e){
        var fd = new FormData(this);
        $.ajax({
            type: "POST",
            url: "process.php",
            data: fd,
            contentType:false,
            cache: false,
            processData: false,
            success: function (data) {
                $('#messageUpdateIndividual').html(data);
            },
            error:function(err)
            {
                $("#messageUpdateIndividual").html(err);
            }
        });

        e.preventDefault();
        return false;


    });
    $('#formUpdateOrganization').on('submit',function(e){

        var fd = new FormData(this);
        $.ajax({
            type: "POST",
            url: "process.php",
            data: fd,
            contentType:false,
            cache: false,
            processData: false,
            success: function (data) {
                $('#messageIndividual').html(data);
            },
            error:function(err)
            {
                $("#messageIndividual").html(err);
            }
        });

        e.preventDefault();
        return false;


    });
    //contact form
    $('#contactForm').on('submit',function(e){

        var fd = new FormData(this);
        $.ajax({
            type: "POST",
            url: "process.php",
            data: fd,
            contentType:false,
            cache: false,
            processData: false,
            success: function (data) {
                $('#messageContact').html(data);
            },
            error:function(err)
            {
                $("#messageContact").html(err);
            }
        });

        e.preventDefault();
        return false;


    });
    
    //report problem 
    $('#reportForm').on('submit',function(e){

        var fd = new FormData(this);
        $.ajax({
            type: "POST",
            url: "process.php",
            data: fd,
            contentType:false,
            cache: false,
            processData: false,
            success: function (data) {
                $('#messageReport').html(data);
            },
            error:function(err)
            {
                $("#messageReport").html(err);
            }
        });

        e.preventDefault();
        return false;


    });


});