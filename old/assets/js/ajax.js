jQuery(document).ready(function() {

    $('#listingForm').on('submit', function (e) {
		e.preventDefault();

        var fd = new FormData(this);
        //var submit=$('#submitIndividual');
        $.ajax({
            type: "POST",
            url: "process.php",
            data: fd,
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                $('#listingMessage').html(data);
            }
        });

        e.preventDefault();
        return false;


    });
});


 $('#resetPassForm').on('submit',function(e){
     
        var fd = new FormData(this);
        //var submit=$('#submitIndividual');
            $.ajax({
                type: "POST",
                url: "process.php",
                data: fd,
                contentType:false,
                cache: false,
                processData: false,
                success: function (data) {
                     $('#resetPassMessage').html(data);
                }
            });

        e.preventDefault();
        return false;


    });
	
	
	 $('#inactiveForm').on('submit',function(e){
     
        var fd = new FormData(this);
        //var submit=$('#submitIndividual');
            $.ajax({
                type: "POST",
                url: "process.php",
                data: fd,
                contentType:false,
                cache: false,
                processData: false,
                success: function (data) {
                     $('#InactiveMessage').html(data);
       setTimeout(function(){
       window.location='index.php';
    }, 5000);
                }
            });

        e.preventDefault();
        return false;


    });

 //contact form
    $('#contactForm_submit').on('submit',function(e){

        var fd = new FormData(this);
        $.ajax({
            type: "POST",
            url: "process.php",
            data: fd,
            contentType:false,
            cache: false,
            processData: false,
            success: function (data) {
                $('#messageContact').html(data);
            },
            error:function(err)
            {
                $("#messageContact").html(err);
            }
        });

        e.preventDefault();
        return false;


    });