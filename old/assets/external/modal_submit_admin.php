<?php
error_reporting(0);
require('../../dbConfig.php');
$id=$_GET['id'];
$data=$listing->getAllData($id);
$result=$listing->getAllListingData($id);
$images = $listing->getAllImagesData($id);
$ltitle=$result['title'];
$category=$result['category'];
$description=$result['description'];
$address=$result['location'];
$latitude=$result['latitude'];
$longitude=$result['longitude'];
$district=$result['district'];
$postalcode=$result['postalcode'];
$country=$result['country'];
$url=$result['url'];
$lfacebook=$result['facebook'];
$twitter=$result['twitter'];
$mobile=$result['mobile'];
$fax=$result['fax'];
$telephone=$result['phone'];
$hoo=$result['hoo'];
$web = $result['wab'];
$hoodesignation=$result['hoodesignation'];
$hooemail=$result['hooemail'];
$contactperson=$result['contactperson'];
$cpdesignation=$result['cpdesignation'];
$cpphone=$result['cpperson'];
$cpemail=$result['cpemail'];
$edo=$result['edo'];
$nod=$result['nod'];
$fte=$result['fte'];
$pte=$result['pte'];
$volunteers=$result['volunteers'];
$learnabout=$result['learnabout'];
$logo=$result['logo'];
$gallery=$result['galleryfiles'];
$vogt_name = $result['vogt_name'];

$registration_no = $result['registration_no'];
$govt_reg_date = $result['govt_reg_date'];
$college_name = $result['college_name'];
$non_gov_reg = $result['non_gov_reg'];
$foreign_reg = $result['foreign_reg'];
$Foreign_reg_number = $result['reg_college'];

$ia_of_org = $result['ia_of_org'];
$aff_with_other = $result['aff_with_other'];
$aff_with_other_1 = $result['aff_with_other_1'];

$orgLogo = [];
$orgImages = [];
$prevGallery = '';
$prevLogo = '';
if(count($images)) {
    foreach($images as $imageKey => $image) {
        $imageId = $image['id'];
        $imageType = $image['type'];
        $imagePath = $image['path'];

        if($image['type'] == 'logo') {
            $orgLogo = $image;
            $prevLogo .= '<span style="padding:0 5px" data-id="'.  $imageId .'" id="event_'.$imageId.'"><img width="50px" height="50px" src="../'.$imagePath.'"></span>';
        }
        else {

            $prevGallery .= '<span style="padding:0 5px" data-id="'.$imageId.'" id="event_'.$imageId.'"><img width="50px" height="50px" src="../'.$imagePath.'"></span>';
        }
    }
}

?>


<div class="modal-dialog width-800px" role="document" data-latitude="23.6850" data-longitude="90.3563" data-marker-drag="true" id="modal_for_admin">
    
        <div class="modal-body">

        <div id="listingMessage"></div>
            <form class="form inputs-underline" id="listingForm" method="POST" action="" >
                <section>
                    <h3>Organizaion information</h3>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label for="title">Name of Organization:</label>
                                <?php echo $ltitle; ?>
                            </div>
                             
                            <!--end form-group-->
                        </div>
                        <!--end col-md-9-->
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label for="category">The Scope of Your Organization:</label>
								<?php echo $category; ?>
                            </div>
                            <!--end form-group-->
                        </div>
                        <!--col-md-3-->
                    </div>
                    <!--end row-->
                    <div class="form-group">
                        <label for="description">Tell the world what your organisation is doing:</label>
                        <?php echo $description; ?>
                    </div>
                    
                
                </section>
                <section>
                    <h3>Organizaion Contact Information</h3>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="form-group">
                                <label for="address-autocomplete">Full Address: </label>
                                
                            <?php echo $address; ?>
							</div>
                            <!--end form-group-->
                            <div class="map height-200px shadow" id="map-modal"></div>
                            <!--end map-->
                            <div class="form-group hidden">
                                <input type="text" class="form-control" id="latitude" name="latitude" hidden="" value="'.$latitude.'">
                                <input type="text" class="form-control" id="longitude" name="longitude" hidden="" value="'.$longitude.'">
                            </div>
                            <p class="note">Enter the exact address or drag the map marker to position</p>
                        </div>
                        <!--end col-md-6-->
                        <div class="col-md-12 col-sm-12">
                            <div class="form-group col-md-4 col-sm-4">
                                <label for="region">District: </label>
								<?php echo $result["district"]; ?>
                               </div>
                            <!--end form-group-->
                            <div class="form-group col-md-4 col-sm-4">
                                <label for="phone">Postal Code: </label>
                               
								<?php echo $postalcode; ?>
                            </div>
                            <!--end form-group-->
                             <div class="form-group col-md-4 col-sm-4">
                                <label for="email">Country:</label>
                                <?php echo $country; ?>
                            </div>
                            </div>
                            <div class="col-md-12 col-sm-12">
                            <div class="form-group col-md-4 col-sm-4">
                                <label for="email">Org Telephone Number:</label>
                                
                            <?php echo $telephone; ?>
							</div>
                            <!--end form-group-->
                            <div class="form-group col-md-4 col-sm-4">
                                <label for="website">Org Mobile Number: </label>
								<?php echo $mobile; ?>
                                
                            </div>
                            <!--end form-group-->
                            <!--end form-group-->
                            <div class="form-group col-md-4 col-sm-4">
                                <label for="website">Fax</label>
                                <?php echo $fax; ?>
                            </div>
                            <!--end form-group-->
                             <!--end form-group-->
                            <div class="form-group col-md-6 col-sm-6">
                                <label for="website">Org E-mail</label>
                                <?php echo $oemail; ?>
                                
                            </div>
                            
                            <!--end form-group-->
                            <!--end form-group-->
                            <div class="form-group col-md-6 col-sm-6">
                                <label for="website">Website</label>
                                
								<?php echo $website; ?>
                            </div>
                            <!--end form-group-->
                              <section class="col-md-12 col-sm-12">
                            
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label for="facebook">Facebook URL </label>
                                            <?php echo $facebook; ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <!--end form-group-->
                                        <div class="form-group">
                                            <label for="twitter">Twitter URL</label>
                                            <?php echo $twitter; ?>
                                        </div>
                                    </div>

                                    <!--end col-md-6-->
                                </div>
                                <!--end row-->
                            </section>
                                <!--end form-group-->
                                <div class="form-group col-md-4 col-sm-4">
                                    <label for="hoo">Head of Organization </label>
                                    
									<?php echo $hoo; ?>
                                </div>
                                <!--end form-group-->
                                 <!--end form-group-->
                                <div class="form-group col-md-4 col-sm-4">
                                    <label for="hoodestination">Designation </label>
                                   <?php echo $hoodesignation; ?>
                                </div>
                                <!--end form-group-->
                                <!--end form-group-->
                                <div class="form-group col-md-4 col-sm-4">
                                    <label for="hooemail">Email Address</label>
                                    <?php echo $hooemail; ?>
                                </div>
                                <!--end form-group-->
                                <!--end form-group-->
                                <div class="form-group col-md-3 col-sm-3">
                                    <label for="contactperson">Name of the contact person with in your organization </label>
                                    
                               <?php echo $contactperson; ?>
							   </div>
                                <!--end form-group-->
                                 <!--end form-group-->
                                <div class="form-group col-md-3 col-sm-3">
                                    <label for="cpdestination">Destination </label>
                                   <?php echo $cpdesignation; ?>
                                </div>
                                <!--end form-group-->
                                <!--end form-group-->
                                <div class="form-group col-md-3 col-sm-3">
                                    <label for="cpperson">Phone </label>
                                    <?php echo $cpphone; ?>
                                </div>
                                <!--end form-group-->
                                <!--end form-group-->
                                <div class="form-group col-md-3 col-sm-3">
                                    <label for="cpemail">Email Address  </label>
                                   <?php echo $cpemail; ?>
                                </div>
                                <!--end form-group-->
                                <!--end form-group-->
                            <div class="row col-md-12 col-sm-12">
                                <div class="form-group col-md-4 col-sm-4">
                                    <label for="edo">Establishment Date of Organization </label>
                                    <?php echo $edo; ?>
                                </div>
                                <!--end form-group-->
                                <!--end form-group-->
                                <div class="form-group col-md-8 col-sm-8">
                                    <label for="wab">Working Area in Bangladesh </label>
                                    <br><br>
                                    <?php echo $web; ?>
                                  
                                </div>

                                <div class="form-group">
                                    <div class="form-group col-md-12 col-sm-12">
                                        <h4>Number of district/s </h4>
                                        <?php echo $nod; ?>
                                    </div>
                                    <div class="form-group col-md-12 col-sm-12">
                                        <h5>Division area</h5>
                                       
                                        <div id="rediv" class="division_item">
										 <?php $array = explode(',', $result["divarea"]);
                                       $i = 1;
                                      foreach($array as $val){
										  echo $val.', ';
									        echo "<br />";
										  $i++;
									  }
									  ?>
										
										</div>
                                    
                                    </div>
								 </div>
                                </div>

                            </div>
                            <!--end form-group-->
                            
                        </div>
                        <!--end col-md-12-->
                    
                </section>
                <!--number of employee section-->
                <section>
                <h3>Number of Employees</h3>
                <div class="form-group">
				
                <div class="col-md-12 col-sm-12">
                    <div class="col-md-4 col-sm-4">
                    <label for="">Full Time </label>
					<?php echo $fte; ?>
                   
                    </div>
                    <div class="col-md-4 col-sm-4">
                    <label for="">Part-Time </label>
                    <?php echo $pte; ?>
                    </div>
                    <div class="col-md-4 col-sm-4">
                    <label for="">Volunteers </label>
                   
				   <?php echo $volunteers; ?>
                    </div>
                </div>    
                
                </div>
                <h3>Interest Area of the organization </h3>
                <div class="form-group">
                <?php $iao=explode(",",$result["iao"]); ?>
                <div class="col-md-12 col-sm-12">
                <?php echo $result["iao"]; ?>
                </div>
                
                </div>
                 <h3>Please specify your registration / affiliation with others</h3>
                <div class="form-group">
                    <div class="col-md-12 col-sm-12">
                        <div class="col-md-6 col-sm-6">
                            <h6><strong>Registration Authority </strong></h6>
                             
                            <?php echo $result["rauthority"]; ?>
                            
                        </div>
                        


                        <div class="col-md-12 col-sm-12" id="regau">
                            <h3>Select from List of Govt. body</h3>
                            <div class="form-group">
							
                                <div class="col-md-6   col-sm-12">
                                    <?php echo $result["vogt_name"].', '.$aff_with_other_1; ?>
                                </div>
                                

                                <div class="col-md-12">
                                    <div class="col-md-6  col-sm-6">
                                        <h4>GOVT. Registration number </h4>
										<?php echo $registration_no; ?>
                                        
                                    </div>
                                    <div class="col-md-6  col-sm-6">
                                        <h4>GOVT. Registration date </h4>
										
										<?php echo $govt_reg_date; ?>
                                        
                                    </div>
                                </div>


                            </div>
                        </div>



                        <div class="col-md-12" id="reguc">
                            <h4>Please specify the name of university / college</h4>
							<?php echo $college_name; ?>
                        </div>

                       

                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-md-7">
                                    <h4>Foreign Authority Registration number</h4>
                                    <?php echo $Foreign_reg_number; ?>
                                </div>
                                <div class="col-md-5">
                                    <h4>Foreign Authority Registration Date</h4>
                                    <?php echo $non_gov_reg; ?>
                                </div>
                                <div class="col-md-12">
                                    <h4>Please specify the name of foreign registration authority</h4>
                                    <?php echo $foreign_reg; ?>
                                </div>
                            </div>
                        </div>

                    </div>
               
                
                </div>
                <div class="form-group">
                    <div class="col-md-12 col-sm-12">
                        <label>How did you learn about Youthopia.bangla ? </label>
                        <?php echo $learnabout; ?>
                    </div>
                </div>
                
                </section>
                <!--number of employee section end-->
              
                <section>
                    <h3>Gallery</h3><p>Ratio : width: 350px X height: 200px; </p>
                    <div class="file-upload-previews"><?php echo $prevGallery; ?> </div>
                    
                    <!--end form-group-->
                </section>
                 <section>
                    <h3>logo</h3><p>Ratio : width: 100px X height:  100px</p>
                    <div class="file-upload-previews"><?php echo $prevLogo; ?></div>
                    
                    <!--end form-group-->
                </section>
                <hr>
          
               
            </form>
            <!--end form-->
        </div>
        <!--end modal-body-->

<!--end modal-dialog-->
<style type="text/css">
    .thumb {
      width: 50px;
      height: 50px;
    }

    #list {
      position: absolute;
      top: 0px;
    }
    #list div {
      float: left;
      margin-right: 10px;
    }
</style>


<link rel="stylesheet" href="css/style.css" >
<link rel="stylesheet" href="css/responsive.css">