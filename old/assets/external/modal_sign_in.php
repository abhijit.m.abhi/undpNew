<?php
echo '
<div class="modal-dialog width-400px" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="section-title">
                <h2>WELCOME</h2>
            </div>
        </div>
        <div class="modal-body">
            <div class="col-md-12 text-center login_btn">
                <a href="" class="btn_fb"><span class="fa fa-facebook"><span></a>
                <a href="" class="btn_gm"><span class="fa fa-google-plus"><span></a>
            </div>
            <form class="form inputs-underline" id="formLogin" action="process.php" method="POST">
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" name="email" id="email" placeholder="Your email">
                </div>
                <!--end form-group-->
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" name="password" id="password" placeholder="Your password">
                </div>
                <div class="form-group center">
                    <button type="submit" name="loginSubmit" class="btn btn-primary width-100">Sign In</button>
                </div>
                <!--end form-group-->
            </form>

            <hr>

           <!-- <a href="#" data-modal-external-file="modal_reset_password.php" data-target="modal-reset-password">I have forgot my password</a>-->
            <!--end form-->
        </div>
        <!--end modal-body-->
    </div>
    <!--end modal-content-->
</div>
<!--end modal-dialog-->
'?>



<style type="text/css">
    .btn_fb{
      width: 40px;
      height: 40px;
      background: #5D7FBF;
      color: #fff;
      display: inline-block;
      line-height: 40px;
      margin-right: 30px;
      border-radius: 50%;
    }

    .btn_gm{
      width: 40px;
      height: 40px;
      background: #E43433;
      color: #fff;
      display: inline-block;
      line-height: 40px;
      border-radius: 50%;
    }
</style>