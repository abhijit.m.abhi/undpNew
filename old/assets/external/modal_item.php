<?php

$currentLocation = "";

// ---------------------------------------------------------------------------------------------------------------------
// Here comes your script for loading from the database.
// ---------------------------------------------------------------------------------------------------------------------

// Remove this example in your live site and replace it with a connection to database //////////////////////////////////
session_start();
ob_start();
include '../../dbConfig.php';
include 'data.php';

ob_end_clean();

for( $i=0; $i < count($data); $i++){
    if( $data[$i]['id'] == $_POST['id'] ){
        $id=$_POST['id'];
        $currentLocation = $data[$i]; // Loaded data must be stored in the "$currentLocation" variable
    }
}
$counter->visitCounter($_POST['id']);
// End of example //////////////////////////////////////////////////////////////////////////////////////////////////////

// Modal HTML code

$latitude = "";
$longitude = "";
$address = "";

if( !empty($currentLocation['latitude']) ){
    $latitude = $currentLocation['latitude'];
}

if( !empty($currentLocation['longitude']) ){
    $longitude = $currentLocation['longitude'];
}

if( !empty($currentLocation['address']) ){
    $address = $currentLocation['address'];
}

$images = $listing->getAllImagesData($_POST['id']);

$upcommingEvent = $event->getEventsByOrgID($_POST['id']);
$upcommingOpportunity = $opportunity->getOpportunitiesByOrgID($_POST['id']);
$upcommingBlogs = $blog->getBlogsByOrgID($_POST['id']);

$totalEvents = $event->getEventsCountByOrgID($_POST['id']);
$totalOpportunities = $opportunity->getOpportunitiesCountByOrgID($_POST['id']);
$totalblogs = $blog->getBlogsCountByOrgID($_POST['id']);
//echo $totalEvents;exit;
//echo '<pre>';
//print_r($upcommingEvent);
//echo '</pre>';
?>
<style>
    section.org_profile_section{
        padding: 10px;
        border: 1px solid red;
        margin-bottom: 5px;
    }
</style>
<?php
echo

    '<div class="modal-item-detail modal-dialog" role="document" data-latitude="'. $latitude .'" data-longitude="'. $longitude .'" data-address="'. $address .'">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="section-title">
                <img src="'.$currentLocation['logo'].'" class="pull-left"/> 
                <div class="com_info list-inline">
                    <h2 style="font-size: 22px;">'. $currentLocation['title'] .'</h2>
                    <h4>'.$currentLocation['district'].','.$currentLocation['country'].'</h4>
                    <div class="info_items list-inline">
                        <div class="info_item">
                            <i class="fa fa-eye"></i>
                            <span>'.$counter->getCount($id).'</span>
                        </div>
                        <div class="info_item">
                            Events : 
                            <span>'. $totalEvents .'</span>
                        </div>
                        <div class="info_item">
                            Media Post : 
                            <span>'. $totalblogs .'</span>
                        </div>
                    </div>
                </div>
                ';



/* 

                        <div class="info_item">
                            Opportunities : 
                            <span>'. $totalOpportunities.'</span>
                        </div>
*/
// Ribbon ------------------------------------------------------------------------------------------

/*                if( !empty($currentLocation['ribbon']) ){
                    echo
                        '<figure class="ribbon">'. $currentLocation['ribbon'] .'</figure>';
                } */

// Rating ------------------------------------------------------------------------------------------

/*   if( !empty($currentLocation['rating']) ){
       echo
       '<div class="rating-passive" data-rating="'. $currentLocation['rating'] .'">
           <span class="stars"></span>
           <span class="reviews">'. $currentLocation['reviews_number'] .'</span>
       </div>';
   } */

echo
'<!--<div class="controls-more">
                    <ul>
                        <li><a href="#">Add to favorites</a></li>
                        <li><a href="#">Add to watchlist</a></li>
                    </ul>
                </div>-->
                <!--end controls-more-->
            </div>
            <!--end section-title-->
        </div>
        <!--end modal-header-->
        <div class="modal-body">
            <div class="left">';

// Gallery -----------------------------------------------------------------------------------------

/*if( !empty($currentLocation['galleryfiles']) ){
    //$currentLocation['galleryfiles']=explode(" ",$currentLocation['galleryfiles']);
    $gallery = "";
    for($i=0; $i < count($currentLocation['galleryfiles']); $i++){
        $gallery .= '<img src="'. $currentLocation['galleryfiles'][$i] .'" alt="">';
    }
    echo
    '<div class="gallery owl-carousel" data-owl-nav="1" data-owl-dots="0">'. $gallery .'</div>
    <!--end gallery-->';
}*/
if( !empty($images) ){

    $gallery = "";
    foreach($images as $imageKey => $image){
        if($image['type'] == 'gallery')
            $gallery .= '<img src="'. $image['path'] .'" alt="" width="450" height="250" style="width: 350px; height: 200px;">';
    }

    echo
        '<div class="gallery owl-carousel" data-owl-nav="1" data-owl-dots="0">'. $gallery .'</div>
                    <!--end gallery-->';
}

echo
'<div class="map" id="map-modal"></div>
                <!--end map-->

                <section>
                <h3>Contact</h3>';
// Contact -----------------------------------------------------------------------------------------

if( !empty($currentLocation['location']) ){
    echo
        '<h5><i class="fa fa-map-marker"></i>'. $currentLocation['location'] .'</h5>';
}

// Phone -------------------------------------------------------------------------------------------

if( !empty($currentLocation['phone']) ){
    echo
        '<h5><i class="fa fa-phone"></i>'. $currentLocation['phone'] .'</h5>';
}
// Mobile -------------------------------------------------------------------------------------------

if( !empty($currentLocation['mobile']) ){
    echo
        '<h5><i class="fa fa-phone"></i>'. $currentLocation['mobile'] .'</h5>';
}


// Email -------------------------------------------------------------------------------------------

if( !empty($currentLocation['org_email']) ){
    echo
        '<h5><i class="fa fa-envelope"></i>'. $currentLocation['org_email'] .'</h5>';
}

// Website -------------------------------------------------------------------------------------------

if( !empty($currentLocation['url']) ){
    echo
        '<h5><i class="fa fa-globe"></i>'. $currentLocation['url'] .'</h5>';
}

// Facebook -------------------------------------------------------------------------------------------

if( !empty($currentLocation['facebook']) ){
    echo
        '<h5><i class="fa fa-facebook"></i>'. $currentLocation['facebook'] .'</h5>';
}

// Twitter -------------------------------------------------------------------------------------------

if( !empty($currentLocation['twitter']) ){
    echo
        '<h5><i class="fa fa-twitter"></i>'. $currentLocation['twitter'] .'</h5>';
}
//youtube
if( !empty($currentLocation['youtube']) ){
    echo
        '<h5><i class="fa fa-youtube"></i>'. $currentLocation['youtube'] .'</h5>';
}
//pinterest
if( !empty($currentLocation['pinterest']) ){
    echo
        '<h5><i class="fa fa-pinterest"></i>'. $currentLocation['pinterest'] .'</h5>';
}
if (isset($_SESSION['org_url']) && !empty($_SESSION['org_url'])){
    $fullUrl = $_SESSION['org_url'];
    unset($_SESSION['org_url']);
}
else {
    $fullUrl = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
    $fullUrl .= '/orgdirectory.php';
}
echo
    '</section>
                <section>
                <hr>
                    <i style="color: #012F49" class="fa fa-share-alt" aria-hidden="true"></i>
                <a class="facebook customer share" style="color: #5D7FBF"
                   href="http://www.facebook.com/sharer.php?u='.urlencode($fullUrl).'" title="Facebook share" target="_blank">
                   <i class="fa fa fa-facebook fa-lg" style="margin: 0 8px;" aria-hidden="true"></i></a>
                <a class="twitter customer share" style="color: #1b95e0"
                   href="http://twitter.com/share?url='.urlencode($fullUrl).'&amp;hashtags=youthopiabangla" title="Twitter share" target="_blank">
                   <i class="fa fa-twitter fa-lg" style="margin: 0 8px;"aria-hidden="true"></i></a>
                <a class="google_plus customer share" style="color: #db4437"
                   href="https://plus.google.com/share?url='.urlencode($fullUrl).'" title="Google Plus Share" target="_blank">
                   <i class="fa fa-google-plus fa-lg" style="margin: 0 8px;" aria-hidden="true"></i></a>
                </section>
            </div>
            <!--end left -->
            <div class="right">
                <section>
                    <h3>About</h3>
                    <div class="read-more"><p>'. $currentLocation['description'] .'</p></div>
                </section>
                <!--end about-->';

// Tags ----------------------------------------------------------------------------------------------------------------
echo
'<section>
                    <h3>INTEREST AREA OF THE ORGANIZATION</h3>
                    <ul class="tags">';
if( !empty($currentLocation['iao']) ) {
    $iao = explode(",", $currentLocation['iao']);

    foreach ($iao as $v) {
        echo '<li style="width:24px; text-align:center;" title="'.$v.'"><span class="' . $listing->getIaoIcon($v) . '"></span></li>
                        ';
    }
}

echo '</ul>
                    </section>
                    <!--end tags-->';



// REGISTRATION --------------------------------------------------------------------------------------

echo
'<section>
                                        <h3>REGISTRATION / AFFILIATION</h3>
                                        <ul class="tags">';
if( !empty($currentLocation['rauthority']) ) {
    $iao = explode(",", $currentLocation['rauthority']);

    foreach ($iao as $v) {
        echo '<li style="background-color: #fff;color:red;border:1px solid red; font-size: 13px; padding: 0px 2px;">'.$v.'</li>
                                            ';
    }
}

echo '</ul>
                    </section>';

echo
'<section>
                                        <h3>LATEST ACTIVITY</h3>
                                        
                    </section>';

// Today Menu --------------------------------------------------------------------------------------

/*if( !empty($currentLocation['today_menu']) ){
    echo
    '<section>
        <h3>Today menu</h3>';
    for($i=0; $i < count($currentLocation['today_menu']); $i++){
        echo
            '<ul class="list-unstyled list-descriptive icon">
                <li>
                    <i class="fa fa-cutlery"></i>
                    <div class="description">
                        <strong>'. $currentLocation['today_menu'][$i]['meal_type'] .'</strong>
                        <p>'. $currentLocation['today_menu'][$i]['meal'] .'</p>
                    </div>
                </li>
            </ul>
            <!--end list-descriptive-->';
    }
    echo
    '</section>
    <!--end today-menu-->';
}*/

// Schedule ----------------------------------------------------------------------------------------

/*if( !empty($currentLocation['schedule']) ){
    echo
    '<section>
        <h3>Schedule</h3>';
    for($i=0; $i < count($currentLocation['schedule']); $i++){
        echo
            '<ul class="list-unstyled list-schedule">
                <li>
                    <div class="left">
                        <strong class="promoted">'. $currentLocation['schedule'][$i]['date'] .'</strong>
                        <figure>'. $currentLocation['schedule'][$i]['time'] .'</figure>
                    </div>
                    <div class="right">
                        <strong>'. $currentLocation['schedule'][$i]['location_title'] .'</strong>
                        <figure>'. $currentLocation['schedule'][$i/*]['location_address'] .'</figure>
                    </div>
                </li>
            </ul>
            <!--end list-schedule-->';
    }
    echo
    '</section>
    <!--end schedule-->';
}*/

// Video -------------------------------------------------------------------------------------------

/*if( !empty($currentLocation['video']) ){
    echo
    '<section>
        <h3>Video presentation</h3>
        <div class="video">'. $currentLocation['video'] .'</div>
    </section>
    <!--end video-->';
}/**/

// Description list --------------------------------------------------------------------------------

/* if( !empty($currentLocation['description_list']) ){
     echo
     '<section>
         <h3>Listing Details</h3>';
         for($i=0; $i < count($currentLocation['description_list']); $i++){
             echo
                 '<dl>
                     <dt>'. $currentLocation['description_list'][$i]['title'] .'</dt>
                     <dd>'. $currentLocation['description_list'][$i]['value'] .'</dd>
                 </dl>
                 <!--end property-details-->';
         }
     echo
     '</section>
     <!--end description-list-->';
 }*/

// Evenet Calender item -----------------------------------------------------------------------------------------

if( !empty($currentLocation['reviews']) ){
    echo
    '<section>
                        <h3>Event Calender</h3>';
    for($i=0; $i < 2; $i++){
        echo
            '<div class="review">
                                <div class="image">
                                    <div class="bg-transfer" style="background-image: url('. $currentLocation['reviews'][$i]['author_image'] .')"></div>
                                </div>
                                <div class="description">
                                    <figure>
                                        <div class="rating-passive" data-rating="'. $currentLocation['reviews'][$i]['rating'] .'">
                                            <span class="stars"></span>
                                        </div>
                                        <span class="date">'. $currentLocation['reviews'][$i]['date'] .'</span>
                                    </figure>
                                    <p>'. $currentLocation['reviews'][$i]['review_text'] .'</p>
                                </div>
                            </div>
                            <!--end review-->';
    }
    echo
    '</section>
                    <!--end reviews-->';
}

?>

<section class="org_profile_section">
    <h3 style="margin: 9px 0px 5px;">Event Calendar</h3>

    <div class="calen_items">
        <div class="row" style="margin-left: -12px;">
            <?php if(!empty($upcommingEvent)): ?>
                <?php foreach($upcommingEvent as $event): ?>
                    <div class="col-md-4" style="padding: 0 5px;">
                        <a href="event_view.php?event_id=<?php echo $event['id'] ?>"><div class="thumbnail calen_item">
                                <img src="upload/events/<?php echo $event['images']; ?>" width="100%" height="100px">
                                <h6 class="text-capitalize"><?php echo $event['event_name'] ?></h6>
                            </div></a>
                    </div>
                <?php endforeach; ?>
            <?php else: ?>
                <div class="col-md-12">
                    <p>No Event Found</p>
                </div>
            <?php endif; ?>
        </div>
        <a href="org-event-calender.php" class="e_view" style="margin-left: 116px; border: 1px solid; padding: 0 4px;">View more</a>

    </div>
</section>


<section class="org_profile_section">

    <h3 style="margin: 9px 0px 5px;">Media Blog</h3>
    <div class="calen_items">
        <div class="row" style="margin-left: -12px;">
            <?php if(!empty($upcommingBlogs)): ?>
                <?php foreach($upcommingBlogs as $blog):
                    $images = explode('___', $blog['images']);
                    ?>
                    <div class="col-md-4" style="padding: 0 5px;">
                        <a href="blog_view.php?opportunity_id=<?php echo $blog['id'] ?>"><div class="thumbnail calen_item">
                                <?php if($blog['type'] == 'image'){?>
                                    <img src="upload/blogs/<?php echo $images[0]; ?>" width="100%" height="100px" alt="<?php echo $blog["title"]; ?>">
                                <?php }
                                else {
                                    $url_link = '';
                                    if ($blog['url_type'] == 'Youtube') {
                                        $url2Embed = str_replace("watch?v=", "embed/", $blog['urls']);
                                        $url_link = str_replace("[URL]", $url2Embed, $blog['url_data']);
                                        $url_link = str_replace('height="250"', 'height="100"', $url_link);
                                    } elseif ($blog['url_type'] == 'SoundCloud') {
                                        $url_link = str_replace("[URL]", $blog['urls'], $blog['url_data']);
                                        $url_link = str_replace('height="258"', 'height="100"', $url_link);
                                    } elseif ($blog['url_type'] == 'DailyMotion') {
                                        $url2Embed = str_replace("https://www.dailymotion.com/video", "https://www.dailymotion.com/embed/video",$blog['urls']);
                                        $url_link = str_replace("[URL]", $url2Embed, $blog['url_data']);
                                        $url_link = str_replace('height="250"', 'height="100"', $url_link);
                                    } else {
                                        echo '<div width="100%" height="258" style="margin-bottom: 0px;"><h1>Something Missing</h1></div>';
                                    }
                                    echo $url_link;
                                }?>

                                <h6 class="text-capitalize"><?php echo $blog['title'] ?></h6>
                            </div></a>
                    </div>
                <?php endforeach; ?>
            <?php else: ?>
                <div class="col-md-12">
                    <p>No Media Blog Found</p>
                </div>
            <?php endif; ?>
        </div>
        <a href="media_blogs.php" class="e_view" style="margin-left: 116px; border: 1px solid; padding: 0 4px;">View more</a>

    </div>
</section>

<!-- <section class="org_profile_section">

    <h3 style="margin: 9px 0px 5px;">Opportunity</h3>
    <div class="calen_items">
        <div class="row" style="margin-left: -12px;">
            <?php if(!empty($upcommingOpportunity)): ?>
                <?php foreach($upcommingOpportunity as $opportunity):
                    $images = explode('___', $opportunity['images']);
                    ?>
                    <div class="col-md-4" style="padding: 0 5px;">
                        <a href="opportunity_view.php?opportunity_id=<?php echo $opportunity['id'] ?>"><div class="calen_item">
                                <img src="upload/opportunities/<?php echo $images[0]; ?>" width="100%" height="100px">
                                <h6 class="text-capitalize"><?php echo $opportunity['title'] ?></h6>
                            </div></a>
                    </div>
                <?php endforeach; ?>
            <?php else: ?>
                <div class="col-md-12">
                    <p>No Opportunity Found</p>
                </div>
            <?php endif; ?>
        </div>
        <a href="opportunities.php" class="e_view" style="margin-left: 116px; border: 1px solid; padding: 0 4px;">View more</a>

    </div>
</section> -->


</div>
<!--end right-->
</div>
<!--end modal-body-->
</div>
<!--end modal-content-->
</div>
<!--end modal-dialog-->


<script>
    (function($){
        /**
         * jQuery function to prevent default anchor event and take the href * and the title to make a share popup
         *
         * @param  {[object]} e           [Mouse event]
         * @param  {[integer]} intWidth   [Popup width defalut 500]
         * @param  {[integer]} intHeight  [Popup height defalut 400]
         * @param  {[boolean]} blnResize  [Is popup resizeabel default true]
         */
        $.fn.customerPopup = function (e, intWidth, intHeight, blnResize) {
            // Prevent default anchor event
            e.preventDefault();
            // Set values for window
            intWidth = intWidth || '500';
            intHeight = intHeight || '400';
            strResize = (blnResize ? 'yes' : 'no');
            // Set title and open popup with focus on it
            var strTitle = ((typeof this.attr('title') !== 'undefined') ? this.attr('title') : 'Social Share'),
                strParam = 'width=' + intWidth + ',height=' + intHeight + ',resizable=' + strResize,
                objWindow = window.open(this.attr('href'), strTitle, strParam).focus();
        }
        /* ================================================== */
        $(document).ready(function ($) {
            $('.customer.share').on("click", function(e) {
                $(this).customerPopup(e);
            });
        });
    }(jQuery));
</script>

