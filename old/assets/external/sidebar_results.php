<?php

// ---------------------------------------------------------------------------------------------------------------------
// Here comes your script for loading from the database.
// ---------------------------------------------------------------------------------------------------------------------

// Remove this example in your live site -------------------------------------------------------------------------------

ob_start();
include('../../dbConfig.php');
include 'data.php';

// This code can be used if you want to load data from JSON file

//$file_contents = file_get_contents("items.json");
//$data = json_decode($file_contents, true);

ob_end_clean();

// End of example ------------------------------------------------------------------------------------------------------

if( !empty($_POST['markers']) ){

    for( $i=0; $i < count($data); $i++){
        for( $e=0; $e < count($_POST['markers']); $e++){
            if( $data[$i]['id'] == $_POST['markers'][$e] ){
                $id=$data[$i]['id'];
                echo
                '<div class="result-item" data-id="'. $data[$i]['id'] .'">';

                    // Ribbon ------------------------------------------------------------------------------------------

                    if( !empty($data[$i]['ribbon']) ){
                        echo
                            '<figure class="ribbon">'. $data[$i]['ribbon'] .'</figure>';
                    }

                    echo
                    '<a href="'. $data[$i]['url'] .'">';

                    // Title -------------------------------------------------------------------------------------------

                    if( !empty($data[$i]['title']) ){
                        echo
                            '<h3 onclick="">'. $data[$i]['title'] .'</h3>';
                    }

                    echo
                        '<div class="result-item-detail">';

                            // Image thumbnail -------------------------------------------------------------------------

                            if( !empty($data[$i]['logo']) ){
                                echo
                                '<div class="image" style="background-image: url('. $data[$i]['logo'].')">';
                                    if( !empty($data[$i]['additional_info']) ){
                                        echo
                                        '<figure>'. $data[$i]['additional_info'] .'</figure>';
                                    }

                                    // Price ---------------------------------------------------------------------------

                                    if( !empty($data[$i]['price']) ){
                                        echo
                                            '<div class="price">'. $data[$i]['price'] .'</div>';
                                    }
                                echo
                                '</div>';
                            }
                            else {
                                echo
                                '<div class="image" style="background-image: url(assets/img/items/default.png)">';
                                    if( !empty($data[$i]['additional_info']) ){
                                        echo
                                        '<figure>'. $data[$i]['additional_info'] .'</figure>';
                                    }

                                    // Price ---------------------------------------------------------------------------

                                    if( !empty($data[$i]['price']) ){
                                        echo
                                            '<figure class="price">'. $data[$i]['price'] .'</figure>';
                                    }
                                echo
                                '</div>';
                            }

                            echo
                            '<div class="description">';
                                if( !empty($data[$i]['location']) ){
                                    echo
                                        '<h5><i class="fa fa-map-marker"></i>'. $data[$i]['location'] .'</h5>';
                                }

                                // Rating ------------------------------------------------------------------------------

                                if( !empty($data[$i]['rating']) ){
                                    echo
                                        '<div class="rating-passive"data-rating="'. $data[$i]['rating'] .'">
                                            <span class="stars"></span>
                                            <span class="reviews">'. $data[$i]['reviews_number'] .'</span>
                                        </div>';
                                }

                                // Category ----------------------------------------------------------------------------

                                if( !empty($data[$i]['category']) ){
                                    echo
                                        '<div class="label label-default">'. $data[$i]['category'] .'</div>';
                                }
                                echo "<br>";
                                echo '<div style="display: inline-flex;align-items: center" class="info_items list-inline">
                                        <div style="background-color:#E43533;margin: 2px;padding:3px;color:#fff;border-radius:5px;">
                                            <i class="fa fa-eye"></i>
                                            <span>'.$counter->getCount($id).'</span>
                                        </div>
                                        <div style="background-color:#E43533;margin: 2px;padding:3px;color:#fff;border-radius:5px;" class="info_item" title="Event">
                                            <i class="fa fa-calendar"></i>
                                            <span>'.$event->getEventsCountByOrgID($id).'</span>
                                        </div>
                                        <!--<div style="background-color:#E43533;margin: 2px;padding:3px;color:#fff;border-radius:5px;" class="info_item" title="Opportunity">
                                            <i class="fa fa-th"></i>
                                            <span>'.$opportunity->getOpportunitiesCountByOrgID($id).'</span>
                                        </div>-->
                                        <div style="background-color:#E43533;margin: 2px;padding:3px;color:#fff;border-radius:5px;" class="info_item" title="Media Blog">
                                            <i class="fa fa-film"></i>
                                            <span>'.$blog->getBlogsCountByOrgID($id).'</span>
                                        </div>
                                    </div>'
                                ;

                                if( !empty($data[$i]['iao']) ) {
                                    $iao = explode(",", $data[$i]['iao']);
                                    echo '<ul class="tags">';
                                    foreach ($iao as $v) {
                                        echo '<li style="width:24px; text-align:center;"><span style="border-radius: 5px;" class="' . $listing->getIaoIcon($v) . '"></span></li>
                        ';
                                    }
                                    echo '</ul>';
                                }

                                // Description -------------------------------------------------------------------------

                                if( !empty($data[$i]['description']) ){
                                    echo
                                        '<p>'. $data[$i]['description'] .'</p>';
                                }
                            echo
                            '</div>
                        </div>
                    </a>
                    <!--<div class="controls-more">
                        <ul>
                            <li><a href="#" class="add-to-favorites">Add to favorites</a></li>
                            <li><a href="#" class="add-to-watchlist">Add to watchlist</a></li>
                        </ul>
                    </div>-->
                </div>';

            }
        }
    }

}