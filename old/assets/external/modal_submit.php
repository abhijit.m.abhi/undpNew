<?php
require('../../dbConfig.php');
global $type;
$username=$_SESSION['username'];
$type=$_SESSION['type'];
$id=$_SESSION['userId'];
$user_id = $id;
$oname=$_SESSION['oname'];
$fname=$_SESSION['fname'];
$data=$listing->getAllData($id);
$oname=$data['oname'];
$oemail=$data['email'];
$facebook=$data['facebook'];
$website=$data['website'];
$result=$listing->getAllListingData($id);
$org_id=$result['id'];
$images = $listing->getAllImagesData($id);
$ltitle=$result['title'];
$category=$result['category'];
$description=$result['description'];
$address=$result['location'];
$latitude=$result['latitude'];
$longitude=$result['longitude'];
$district=$result['district'];
$postalcode=$result['postalcode'];
$country=$result['country'];
$url=$result['url'];
$lfacebook=$result['facebook'];
$twitter=$result['twitter'];
$mobile=$result['mobile'];
$fax=$result['fax'];
$telephone=$result['phone'];
$hoo=$result['hoo'];
$web = $result['wab'];
$hoodesignation=$result['hoodesignation'];
$hooemail=$result['hooemail'];
$contactperson=$result['contactperson'];
$cpdesignation=$result['cpdesignation'];
$cpphone=$result['cpperson'];
$cpemail=$result['cpemail'];
$edo=$result['edo'];
$nod=$result['nod'];
$fte=$result['fte'];
$pte=$result['pte'];
$volunteers=$result['volunteers'];
$learnabout=$result['learnabout'];
$logo=$result['logo'];
$gallery=$result['galleryfiles'];
$vogt_name = $result['vogt_name'];

$registration_no = $result['registration_no'];
$govt_reg_date = $result['govt_reg_date'];
$college_name = $result['college_name'];
$non_gov_reg = $result['non_gov_reg'];
$foreign_reg = $result['foreign_reg'];
$Foreign_reg_number = $result['reg_college'];

$ia_of_org = $result['ia_of_org'];
$aff_with_other = $result['aff_with_other'];
$aff_with_other_1 = $result['aff_with_other_1'];

$orgLogo = [];
$orgImages = [];
$prevGallery = '';
$prevLogo = '';?>
<link href="assets/vendors/fileuploader/src/jquery.fileuploader.css" media="all" rel="stylesheet">
<?php
if(count($images)) {
    foreach($images as $imageKey => $image) {
        $imageId = $image['id'];
        $imageType = $image['type'];
        $imagePath = $image['path'];

        if($image['type'] == 'logo') {
            $orgLogo = $image;
            $prevLogo .= '<span style="padding:0 5px" data-id="'.  $imageId .'" id="event_'.$imageId.'"><img width="50px" height="50px" src="'.$imagePath.'"><span aria-hidden="true" style="cursor:pointer" onclick="removeImge('.$imageId.')">×</span></span>';
        }
        else {

            $prevGallery .= '<span style="padding:0 5px" data-id="'.$imageId.'" id="event_'.$imageId.'"><img width="50px" height="50px" src="'.$imagePath.'"><span aria-hidden="true" style="cursor:pointer" onclick="removeImge('.$imageId.')">×</span></span>';
        }
    }
}
echo'<div class="modal-dialog width-800px" role="document" data-latitude="'.$latitude.'" data-longitude="'.$longitude.'" data-marker-drag="true">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="section-title">
            </div>
        </div>
        <div class="modal-body">

        <div id="listingMessage"></div>
            <form class="form inputs-underline" id="listingForm" method="POST" action="" >
                <input type="hidden" name="org-id" id="org-id" value="'.$org_id.'">
                <input type="hidden" name="user-id" id="user-id" value="'.$user_id.'">
                <section>
                    <h3>Update your information</h3>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label for="title">Name of Organization *</label>
                                <input type="text" class="form-control" required="required" name="title" id="title" value="'.$oname.'">
                            </div>
                             <p class="note">e.g. United Nations Volunteers (UNV) programme</p>
                            <!--end form-group-->
                        </div>
                        <!--end col-md-9-->
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label for="category">The Scope of Your Organization *</label>
                                <select class="form-control selectpicker" name="category" id="category" required="required">
                                    <option value="">Scope</option>
                                    <option value="Local"';$iao=explode(",",$result["category"]);if(in_array("Local",$iao)){echo "selected";} echo '>Local</option>
                                    <option value="National" ';$iao=explode(",",$result["category"]);if(in_array("National",$iao)){echo "selected";} echo '>National</option>
                                    <option value="International" ';$iao=explode(",",$result["category"]);if(in_array("International",$iao)){echo "selected";} echo '>International</option>
                                </select>
                            </div>
                            <!--end form-group-->
                        </div>
                        <!--col-md-3-->
                    </div>
                    <!--end row-->
                    <div class="form-group">
                        <label for="description">Tell the world what your organisation is doing *</label>
                        <textarea class="form-control" id="description" required="required" rows="4" name="description">'.$description.'</textarea>
                    </div>
                    <p class="note">Your Vision, Mission or Objective ( 200 to 3000 characters).</p>
                    <!--end form-group-
                    <div class="form-group">
                        <label for="tags">Tags</label>
                        <input type="text" class="form-control" name="tags" id="tags" placeholder="+ Add tag">
                    </div>
                    <!--end form-group-->
                </section>
                <section>
                    <h3>Organizaion Contact Information</h3>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="form-group">
                                <label for="address-autocomplete">Full Address *</label>
                                <input type="text" class="form-control" name="address" id="address-autocomplete" required="required" value="'.$address.'">
                            </div>
                            <!--end form-group-->
                            <div class="map height-200px shadow" id="map-modal"></div>
                            <!--end map-->
                            <div class="form-group hidden">
                                <input type="text" class="form-control" id="latitude" name="latitude" hidden="" value="'.$latitude.'">
                                <input type="text" class="form-control" id="longitude" name="longitude" hidden="" value="'.$longitude.'">
                            </div>
                            <p class="note">Enter the exact address or drag the map marker to position</p>
                        </div>
                        <!--end col-md-6-->
                        <div class="col-md-12 col-sm-12">
                            <div class="form-group col-md-4 col-sm-4">
                                <label for="region">District *</label>
                                <select class="form-control selectpicker" name="district" required="required" id="region">
                                    <option value="">Select Region</option>
                                    <option value="Bagerhat" ';$iao=explode(",",$result["district"]);if(in_array("Bagerhat",$iao)){echo "selected";} echo '>Bagerhat</option>
                                    <option value="Bandarban" ';$iao=explode(",",$result["district"]);if(in_array("Bandarban",$iao)){echo "selected";} echo '>Bandarban</option>
                                    <option value="Barguna" ';$iao=explode(",",$result["district"]);if(in_array("Barguna",$iao)){echo "selected";} echo '>Barguna</option>
                                    <option value="Barisal" 
                                    ';$iao=explode(",",$result["district"]);if(in_array("Barisal",$iao)){echo "selected";} echo '>Barisal</option>
                                    <option value="Bhola" 
                                     ';$iao=explode(",",$result["district"]);if(in_array("Bhola",$iao)){echo "selected";} echo '>Bhola</option>
                                    <option value="Bogra" 
                                    ';$iao=explode(",",$result["district"]);if(in_array("Bogra",$iao)){echo "selected";} echo '>Bogra</option>
                                    <option value="Brahmmanbaria" 
                                     ';$iao=explode(",",$result["district"]);if(in_array("Brahmmanbaria",$iao)){echo "selected";} echo '>Brahmmanbaria</option>
                                    <option value="Chandpur" 
                                     ';$iao=explode(",",$result["district"]);if(in_array("Chandpur",$iao)){echo "selected";} echo '>Chandpur</option>
                                    <option value="Chapai Nawabganj" ';$iao=explode(",",$result["district"]);if(in_array("Chapai Nawabgani",$iao)){echo "selected";} echo '>Chapai Nawabganj</option>
                                    <option value="Chittagong" 
                                    ';$iao=explode(",",$result["district"]);if(in_array("Chittagong",$iao)){echo "selected";} echo '>Chittagong</option>
                                    <option value="Chuadanga" 
                                     ';$iao=explode(",",$result["district"]);if(in_array("Chuadanga",$iao)){echo "selected";} echo '>Chuadanga</option>
                                    <option value="Comilla" 
                                     ';$iao=explode(",",$result["district"]);if(in_array("Comilla",$iao)){echo "selected";} echo '>Comilla</option>
                                    <option value="CoxsBazar" 
                                    ';$iao=explode(",",$result["district"]);if(in_array("CoxsBazar",$iao)){echo "selected";} echo '>Cox’s  Bazar</option>
                                    <option value="Dhaka" 
                                     ';$iao=explode(",",$result["district"]);if(in_array("Dhaka",$iao)){echo "selected";} echo '>Dhaka</option>
                                    <option value="Dinajpur" 
                                     ';$iao=explode(",",$result["district"]);if(in_array("Dinajpur",$iao)){echo "selected";} echo '>Dinajpur</option>
                                    <option value="Faridpur" 
                                    ';$iao=explode(",",$result["district"]);if(in_array("Faridpur",$iao)){echo "selected";} echo '>Faridpur</option>
                                    <option value="Feni" 
                                     ';$iao=explode(",",$result["district"]);if(in_array("Feni",$iao)){echo "selected";} echo '>Feni</option>
                                    <option value="Gaibandha" 
                                     ';$iao=explode(",",$result["district"]);if(in_array("Barguna",$iao)){echo "selected";} echo '>Gaibandha</option>
                                    <option value="Gazipur" 
                                     ';$iao=explode(",",$result["district"]);if(in_array("Gazipur",$iao)){echo "selected";} echo '>Gazipur</option>
                                    <option value="Gopalganj" 
                                     ';$iao=explode(",",$result["district"]);if(in_array("Gopalganj",$iao)){echo "selected";} echo '>Gopalganj</option>
                                    <option value="Habiganj" 
                                     ';$iao=explode(",",$result["district"]);if(in_array("Habiganj",$iao)){echo "selected";} echo '>Habiganj</option>
                                    <option value="Jamalpur" 
                                     ';$iao=explode(",",$result["district"]);if(in_array("Jamalpur",$iao)){echo "selected";} echo '>Jamalpur</option>
                                    <option value="Jessore" 
                                     ';$iao=explode(",",$result["district"]);if(in_array("Jessore",$iao)){echo "selected";} echo '>Jessore</option>
                                    <option value="Jhalakhati" 
                                     ';$iao=explode(",",$result["district"]);if(in_array("Jhalakhati",$iao)){echo "selected";} echo '>Jhalakhati</option>
                                    <option value="Jhenaidah" 
                                     ';$iao=explode(",",$result["district"]);if(in_array("Jhenaidah",$iao)){echo "selected";} echo '>Jhenaidah</option>
                                    <option value="Joypurhat" 
                                    ';$iao=explode(",",$result["district"]);if(in_array("Joypurhat",$iao)){echo "selected";} echo '>Joypurhat</option>
                                    <option value="Khagrachhari" 
                                     ';$iao=explode(",",$result["district"]);if(in_array("Khagrachhari",$iao)){echo "selected";} echo '>Khagrachhari</option>
                                    <option value="Khulna" 
                                     ';$iao=explode(",",$result["district"]);if(in_array("Khulna",$iao)){echo "selected";} echo '>Khulna</option>
                                    <option value="Kishoreganj" 
                                     ';$iao=explode(",",$result["district"]);if(in_array("Kishoreganj",$iao)){echo "selected";} echo '>Kishoreganj</option>
                                    <option value="Kurigram" 
                                    ';$iao=explode(",",$result["district"]);if(in_array("Kurigram",$iao)){echo "selected";} echo '>Kurigram</option>
                                    <option value="Kushtia" 
                                     ';$iao=explode(",",$result["district"]);if(in_array("Khustia",$iao)){echo "selected";} echo '>Kushtia</option>
                                    <option value="Lakshmipur" 
                                     ';$iao=explode(",",$result["district"]);if(in_array("Lakshmipur",$iao)){echo "selected";} echo '>Lakshmipur</option>
                                    <option value="Lalmonirhat" 
                                     ';$iao=explode(",",$result["district"]);if(in_array("Lalmonirhat",$iao)){echo "selected";} echo '>Lalmonirhat</option>
                                    <option value="Madaripur" 
                                     ';$iao=explode(",",$result["district"]);if(in_array("Madaripur",$iao)){echo "selected";} echo '>Madaripur</option>
                                    <option value="Magura" 
                                    ';$iao=explode(",",$result["district"]);if(in_array("Magura",$iao)){echo "selected";} echo '>Magura</option>
                                    <option value="Manikganj" 
                                     ';$iao=explode(",",$result["district"]);if(in_array("Manikganj",$iao)){echo "selected";} echo '>Manikganj</option>
                                    <option value="Maulvibazar" 
                                    ';$iao=explode(",",$result["district"]);if(in_array("Maulvibazar",$iao)){echo "selected";} echo '>Maulvibazar</option>
                                    <option value="Meherpur" 
                                    ';$iao=explode(",",$result["district"]);if(in_array("Meherpur",$iao)){echo "selected";} echo '>Meherpur</option>
                                    <option value="Munshiganj" 
                                     ';$iao=explode(",",$result["district"]);if(in_array("Munshiganj",$iao)){echo "selected";} echo '>Munshiganj</option>
                                    <option value="Mymensingh" 
                                     ';$iao=explode(",",$result["district"]);if(in_array("Mymensingh",$iao)){echo "selected";} echo '>Mymensingh</option>
                                    <option value="Naogaon" 
                                     ';$iao=explode(",",$result["district"]);if(in_array("Naogaon",$iao)){echo "selected";} echo '>Naogaon</option>
                                    <option value="Narail" 
                                     ';$iao=explode(",",$result["district"]);if(in_array("Narail",$iao)){echo "selected";} echo '>Narail</option>
                                    <option value="Narayanganj" 
                                     ';$iao=explode(",",$result["district"]);if(in_array("Narayanganj",$iao)){echo "selected";} echo '>Narayanganj</option>
                                    <option value="Narsingdi" 
                                     ';$iao=explode(",",$result["district"]);if(in_array("Narsingdi",$iao)){echo "selected";} echo '>Narsingdi</option>
                                    <option value="Natore" 
                                     ';$iao=explode(",",$result["district"]);if(in_array("Natore",$iao)){echo "selected";} echo '>Natore</option>
                                    <option value="Netrakona" 
                                     ';$iao=explode(",",$result["district"]);if(in_array("Netrakona",$iao)){echo "selected";} echo '>Netrakona</option>
                                    <option value="Nilphamari" 
                                     ';$iao=explode(",",$result["district"]);if(in_array("Nilphamari",$iao)){echo "selected";} echo '>Nilphamari</option>
                                    <option value="Noakhali" 
                                     ';$iao=explode(",",$result["district"]);if(in_array("Noakhali",$iao)){echo "selected";} echo '>Noakhali</option>
                                    <option value="Pabna" 
                                    ';$iao=explode(",",$result["district"]);if(in_array("Pabna",$iao)){echo "selected";} echo '>Pabna</option>
                                    <option value="Panchagarh" 
                                     ';$iao=explode(",",$result["district"]);if(in_array("Panchagarh",$iao)){echo "selected";} echo '>Panchagarh</option>
                                    <option value="Patuakhali" 
                                     ';$iao=explode(",",$result["district"]);if(in_array("Patuakhali",$iao)){echo "selected";} echo '>Patuakhali</option>
                                    <option value="Pirojpur" 
                                    ';$iao=explode(",",$result["district"]);if(in_array("Pirojpur",$iao)){echo "selected";} echo '>Pirojpur</option>
                                    <option value="Rajbari" 
                                     ';$iao=explode(",",$result["district"]);if(in_array("Rajbari",$iao)){echo "selected";} echo '>Rajbari</option>
                                    <option value="Rajshahi" 
                                     ';$iao=explode(",",$result["district"]);if(in_array("Rajshahi",$iao)){echo "selected";} echo '>Rajshahi</option>
                                    <option value="Rangamati" 
                                     ';$iao=explode(",",$result["district"]);if(in_array("Rangamati",$iao)){echo "selected";} echo '>Rangamati</option>
                                    <option value="Rangpur" 
                                     ';$iao=explode(",",$result["district"]);if(in_array("Rangpur",$iao)){echo "selected";} echo '>Rangpur</option>
                                     <option value="Satkhira" 
                                     ';$iao=explode(",",$result["district"]);if(in_array("Satkhira",$iao)){echo "selected";} echo '>Satkhira</option>
                                    <option value="Shariatpur" 
                                     ';$iao=explode(",",$result["district"]);if(in_array("Shariatpur",$iao)){echo "selected";} echo '>Shariatpur</option>
                                    <option value="Sherpur" 
                                     ';$iao=explode(",",$result["district"]);if(in_array("Sherpur",$iao)){echo "selected";} echo '>Sherpur</option>
                                    <option value="Sirajganj" 
                                    ';$iao=explode(",",$result["district"]);if(in_array("Sirajganj",$iao)){echo "selected";} echo '>Sirajganj</option>
                                    <option value="Sunamganj" 
                                   ';$iao=explode(",",$result["district"]);if(in_array("Sunamganj",$iao)){echo "selected";} echo '>Sunamganj</option>
                                    <option value="Sylhet" 
                                     ';$iao=explode(",",$result["district"]);if(in_array("Sylhet",$iao)){echo "selected";} echo '>Sylhet</option>
                                    <option value="Tangail" 
                                     ';$iao=explode(",",$result["district"]);if(in_array("Tangail",$iao)){echo "selected";} echo '>Tangail</option>
                                    <option value="Thakurgaon" 
                                     ';$iao=explode(",",$result["district"]);if(in_array("Thakurgaon",$iao)){echo "selected";} echo '>Thakurgaon</option>
                                </select>
                            </div>
                            <!--end form-group-->
                            <div class="form-group col-md-4 col-sm-4">
                                <label for="phone">Postal Code</label>
                                <input type="text" class="form-control" name="postalcode" id="postalcode" value="'.$postalcode.'">
                            </div>
                            <!--end form-group-->
                             <div class="form-group col-md-4 col-sm-4">
                                <label for="email">Country *</label>
                                <input type="text" class="form-control" name="country" required="required" id="country" value="'.$country.'">
                            </div>
                            </div>
                            <div class="col-md-12 col-sm-12">
                            <div class="form-group col-md-4 col-sm-4">
                                <label for="email">Org Telephone Number*</label>
                                <input type="text" required="required" class="form-control" name="telephone" id="telephone" placeholder="" value="'.$telephone.'">
                            </div>
                            <!--end form-group-->
                            <div class="form-group col-md-4 col-sm-4">
                                <label for="website">Org Mobile Number*</label>
                                <input type="text" required="required" class="form-control" name="mobile" id="mobile" placeholder="" value="'.$mobile.'">
                            </div>
                            <!--end form-group-->
                            <!--end form-group-->
                            <div class="form-group col-md-4 col-sm-4">
                                <label for="website">Fax</label>
                                <input type="text" class="form-control" name="fax" id="fax" placeholder="" value="'.$fax.'">
                            </div>
                            <!--end form-group-->
                             <!--end form-group-->
                            <div class="form-group col-md-6 col-sm-6">
                                <label for="website">Org E-mail*</label>
                                <input type="text" class="form-control" name="email" required="required" id="email" value="'.$oemail.'">
                                <p class="note"></p>
                            </div>
                            
                            <!--end form-group-->
                            <!--end form-group-->
                            <div class="form-group col-md-6 col-sm-6">
                                <label for="website">Website</label>
                                <input type="text" class="form-control" name="website" id="website" value="'.$website.'">
                            </div>
                            <!--end form-group-->
                              <section class="col-md-12 col-sm-12">
                            
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label for="facebook">Facebook URL *</label>
                                            <input type="text" class="form-control" name="facebook" required="required" id="facebook" value="'.$facebook.'">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <!--end form-group-->
                                        <div class="form-group">
                                            <label for="twitter">Twitter URL</label>
                                            <input type="text" class="form-control" name="twitter" id="twitter" placeholder="" value="'.$twitter.'">
                                        </div>
                                    </div>

                                    <!--end col-md-6-->
                                </div>
                                <!--end row-->
                            </section>
                                <!--end form-group-->
                                <div class="form-group col-md-4 col-sm-4">
                                    <label for="hoo">Head of Organization *</label>
                                    <input type="text" class="form-control" name="hoo" id="hoo" required="required" value="'.$hoo.'">
                                </div>
                                <!--end form-group-->
                                 <!--end form-group-->
                                <div class="form-group col-md-4 col-sm-4">
                                    <label for="hoodestination">Designation *</label>
                                    <input type="text" class="form-control" name="hoodestination" id="hoodestination" required="required" value="'.$hoodesignation.'">
                                </div>
                                <!--end form-group-->
                                <!--end form-group-->
                                <div class="form-group col-md-4 col-sm-4">
                                    <label for="hooemail">Email Address</label>
                                    <input type="text" class="form-control" name="hooemail" id="hooemail" value="'.$hooemail.'">
                                </div>
                                <!--end form-group-->
                                <!--end form-group-->
                                <div class="form-group col-md-3 col-sm-3">
                                    <label for="contactperson">Name of the contact person with in your organization *</label>
                                    <input type="text" class="form-control" name="contactperson" required="required" id="contactperson" value="'.$contactperson.'">
                                </div>
                                <!--end form-group-->
                                 <!--end form-group-->
                                <div class="form-group col-md-3 col-sm-3">
                                    <label for="cpdestination">Destination *</label>
                                    <input type="text" class="form-control" name="cpdestination" required="required" id="cpdestination" value="'.$cpdesignation.'">
                                </div>
                                <!--end form-group-->
                                <!--end form-group-->
                                <div class="form-group col-md-3 col-sm-3">
                                    <label for="cpperson">Phone *</label>
                                    <input type="text" class="form-control" name="cpphone" required="required" id="cpphone" value="'.$cpphone.'">
                                </div>
                                <!--end form-group-->
                                <!--end form-group-->
                                <div class="form-group col-md-3 col-sm-3">
                                    <label for="cpemail">Email Address * </label>
                                    <input type="text" class="form-control" name="cpemail" id="cpemail" required="required" value="'.$cpemail.'">
                                </div>
                                <!--end form-group-->
                                <!--end form-group-->
                            <div class="row col-md-12 col-sm-12">
                                <div class="form-group col-md-4 col-sm-4">
                                    <label for="edo">Establishment Date of Organization *</label>
                                    <input type="text" class="form-control date_picker" required="required" name="edo" id="edo_2" value="'.$edo.'">
                                </div>
                                <!--end form-group-->
                                <!--end form-group-->
                                <div class="form-group col-md-8 col-sm-8">
                                    <label for="wab>Working Area in Bangladesh *</label>
                                    <br><br>
                                    <label class="radio-inline">
                                    <input type="radio" value="All district" class="alldisban" name="wab" id="wab" '; if("All district"==$web){echo "checked";} echo '>All district in bangladesh
                                    </label>
                                    <label class="radio-inline">
                                    <input type="radio" value="particular district" class="partdis" name="wab" id="wab" '; if("particular district"==$web){echo "checked";} echo '>Particular Districts
                                    </label>
                                  
                                </div>

                                <div class="div_wrap">
                                    <div class="form-group col-md-12 col-sm-12">
                                        <h4>Number of district/s *</h4>
                                        <input type="text" name="nod" id="nod" class="form-control" value="'.$nod.'">
                                    </div>
                                    <div class="form-group col-md-12 col-sm-12">
                                        <h5>Division area</h5>
                                        <!--  <select class="form-control selectpicker" name="divarea[]" id="divarea_2">
                                            <option value="rediv"></option>
                                            <option value="Dhaka" ';$iao=explode(",",$result["divarea"]);if(in_array("Dhaka",$iao)){echo "selected";} echo '>Dhaka Division</option>
                                            <option value="Chittagong" ';$iao=explode(",",$result["divarea"]);if(in_array("Chittagong",$iao)){echo "selected";} echo '>Chittagong Division</option>
                                            <option value="Rajshahi" ';$iao=explode(",",$result["divarea"]);if(in_array("Rajshahi",$iao)){echo "selected";} echo '>Rajshahi Division</option>
                                            <option value="Khulna" ';$iao=explode(",",$result["divarea"]);if(in_array("Khulna",$iao)){echo "selected";} echo '>Khulna Division</option>
                                            <option value="Barisal" ';$iao=explode(",",$result["divarea"]);if(in_array("Barisal",$iao)){echo "selected";} echo '>Barisal Division</option>
                                            <option value="Sylhet" ';$iao=explode(",",$result["divarea"]);if(in_array("sylhet",$iao)){echo "selected";} echo '>Sylhet Division</option>
                                            <option value="Rangpur" ';$iao=explode(",",$result["divarea"]);if(in_array("Rangpur",$iao)){echo "selected";} echo '>Rangpur Division</option>
                                            <option value="Mymensingh" ';$iao=explode(",",$result["divarea"]);if(in_array("Mymensingh",$iao)){echo "selected";} echo '>Mymensingh Division</option>
                                        </select>
                                             -->
                                        <div id="rediv" class="division_item"></div>
                                               
                                        <div id="Dhaka" class="division_item">
                                            <h6>Dhaka Division *</h6>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="divarea[]" ';$iao=explode(",",$result["divarea"]);if(in_array("Dhaka",$iao)){echo "checked";} echo ' value="Dhaka">
                                                    Dhaka
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Faridpur" ';$iao=explode(",",$result["divarea"]);if(in_array("Faridpur",$iao)){echo "checked";} echo '>
                                                    Faridpur
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Gazipur" ';$iao=explode(",",$result["divarea"]);if(in_array("Gazipur",$iao)){echo "checked";} echo '>
                                                    Gazipur
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Gopalganj" ';$iao=explode(",",$result["divarea"]);if(in_array("Gopalganj",$iao)){echo "checked";} echo '>
                                                    Gopalganj
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Kishoreganj" ';$iao=explode(",",$result["divarea"]);if(in_array("Kishoreganj",$iao)){echo "checked";} echo '>
                                                    Kishoreganj
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Madaripur" ';$iao=explode(",",$result["divarea"]);if(in_array("Madaripur",$iao)){echo "checked";} echo '>
                                                    Madaripur
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Manikganj" ';$iao=explode(",",$result["divarea"]);if(in_array("Manikganj",$iao)){echo "checked";} echo '>
                                                    Manikganj
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Munshiganj" ';$iao=explode(",",$result["divarea"]);if(in_array("Munshiganj",$iao)){echo "checked";} echo '>
                                                    Munshiganj
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Narayanganj" ';$iao=explode(",",$result["divarea"]);if(in_array("Narayanganj",$iao)){echo "checked";} echo '>
                                                    Narayanganj
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Narsingdi" ';$iao=explode(",",$result["divarea"]);if(in_array("Narsingdi",$iao)){echo "checked";} echo '>
                                                    Narsingdi
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Rajbari" ';$iao=explode(",",$result["divarea"]);if(in_array("Rajbari",$iao)){echo "checked";} echo '>
                                                    Rajbari
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Shariatpur" ';$iao=explode(",",$result["divarea"]);if(in_array("Shariatpur",$iao)){echo "checked";} echo '>
                                                    Shariatpur
                                                </label>
                                                <label>
                                                    <input type="checkbox"name="divarea[]" value="Tangail" ';$iao=explode(",",$result["divarea"]);if(in_array("Tangail",$iao)){echo "checked";} echo '>
                                                    Tangail
                                                </label>
                                            </div>
                                        </div>

                                        <div id="Chittagong" class="division_item">
                                            <h6>Chittagong Division *</h6>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Bandarban" ';$iao=explode(",",$result["divarea"]);if(in_array("Bandarban",$iao)){echo "checked";} echo '>
                                                    Bandarban
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Brahmmanbaria" ';$iao=explode(",",$result["divarea"]);if(in_array("Brahmmanbaria",$iao)){echo "checked";} echo '>
                                                    Brahmmanbaria
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Chandpur" ';$iao=explode(",",$result["divarea"]);if(in_array("Chandpur",$iao)){echo "checked";} echo '>
                                                    Chandpur
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Chittagong" ';$iao=explode(",",$result["divarea"]);if(in_array("Chittagong",$iao)){echo "checked";} echo '>
                                                    Chittagong
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Comilla" ';$iao=explode(",",$result["divarea"]);if(in_array("Comilla",$iao)){echo "checked";} echo '>
                                                    Comilla
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="CoxsBazar" ';$iao=explode(",",$result["divarea"]);if(in_array("CoxsBazar",$iao)){echo "checked";} echo '>
                                                    Cox’s  Bazar
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Feni" ';$iao=explode(",",$result["divarea"]);if(in_array("Feni",$iao)){echo "checked";} echo '>
                                                    Feni
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Khagrachhari" ';$iao=explode(",",$result["divarea"]);if(in_array("Khagrachhari",$iao)){echo "checked";} echo '>
                                                    Khagrachhari
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Lakshmipur" ';$iao=explode(",",$result["divarea"]);if(in_array("Lakshmipur",$iao)){echo "checked";} echo '>
                                                    Lakshmipur
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Noakhali" ';$iao=explode(",",$result["divarea"]);if(in_array("Noakhali",$iao)){echo "checked";} echo '>
                                                    Noakhali
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Rangamati" ';$iao=explode(",",$result["divarea"]);if(in_array("Rangamati",$iao)){echo "checked";} echo '>
                                                    Rangamati
                                                </label>
                                            </div>
                                        </div>


                                        <div id="Rajshahi" class="division_item">
                                            <h6>Rajshahi Division *</h6>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Bogra" ';$iao=explode(",",$result["divarea"]);if(in_array("Bogra",$iao)){echo "checked";} echo '>
                                                    Bogra
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="ChapaiNawabganj" ';$iao=explode(",",$result["divarea"]);if(in_array("ChapaiNawabganj",$iao)){echo "checked";} echo '>
                                                    Chapai Nawabganj
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Joypurhat" ';$iao=explode(",",$result["divarea"]);if(in_array("Joypurhat",$iao)){echo "checked";} echo '>
                                                    Joypurhat
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Naogaon" ';$iao=explode(",",$result["divarea"]);if(in_array("Naogaon",$iao)){echo "checked";} echo '>
                                                    Naogaon
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Natore" ';$iao=explode(",",$result["divarea"]);if(in_array("Natore",$iao)){echo "checked";} echo '>
                                                    Natore
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Pabna" ';$iao=explode(",",$result["divarea"]);if(in_array("Pabna",$iao)){echo "checked";} echo '>
                                                    Pabna
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Rajshahi" ';$iao=explode(",",$result["divarea"]);if(in_array("Rajshahi",$iao)){echo "checked";} echo '>
                                                    Rajshahi
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Sirajganj" ';$iao=explode(",",$result["divarea"]);if(in_array("Sirajganj",$iao)){echo "checked";} echo '>
                                                    Sirajganj
                                                </label>
                                            </div>
                                        </div>


                                        <div id="Khulna" class="division_item">
                                            <h6>Khulna Division *</h6>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Bagerhat" ';$iao=explode(",",$result["divarea"]);if(in_array("Bagerhat",$iao)){echo "checked";} echo '>
                                                    Bagerhat
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Chuadanga" ';$iao=explode(",",$result["divarea"]);if(in_array("Chuadanga",$iao)){echo "checked";} echo '>
                                                    Chuadanga
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Jhenaidah" ';$iao=explode(",",$result["divarea"]);if(in_array("Jhenaidah",$iao)){echo "checked";} echo '>
                                                    Jhenaidah
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Jessore" ';$iao=explode(",",$result["divarea"]);if(in_array("Jessore",$iao)){echo "checked";} echo '>
                                                    Jessore
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Khulna" ';$iao=explode(",",$result["divarea"]);if(in_array("Khulna",$iao)){echo "checked";} echo '>
                                                    Khulna
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Kushtia" ';$iao=explode(",",$result["divarea"]);if(in_array("Kushtia",$iao)){echo "checked";} echo '>
                                                    Kushtia
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Magura" ';$iao=explode(",",$result["divarea"]);if(in_array("Magura",$iao)){echo "checked";} echo '>
                                                    Magura
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Meherpur" ';$iao=explode(",",$result["divarea"]);if(in_array("Meherpur",$iao)){echo "checked";} echo '>
                                                    Meherpur
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Narail" ';$iao=explode(",",$result["divarea"]);if(in_array("Narail",$iao)){echo "checked";} echo '>
                                                    Narail
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Satkhira" ';$iao=explode(",",$result["divarea"]);if(in_array("Satkhira",$iao)){echo "checked";} echo '>
                                                    Satkhira
                                                </label>
                                            </div>
                                        </div>

                                        <div id="Barisal" class="division_item">
                                            <h6>Barisal Division *</h6>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Barisal" ';$iao=explode(",",$result["divarea"]);if(in_array("Barisal",$iao)){echo "checked";} echo '>
                                                    Barisal
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Bhola" ';$iao=explode(",",$result["divarea"]);if(in_array("Bhola",$iao)){echo "checked";} echo '>
                                                    Bhola
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Barguna" ';$iao=explode(",",$result["divarea"]);if(in_array("Barguna",$iao)){echo "checked";} echo '>
                                                    Barguna
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Jhalakhati" ';$iao=explode(",",$result["divarea"]);if(in_array("Jhalakhati",$iao)){echo "checked";} echo '>
                                                    Jhalakhati
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Pirojpur" ';$iao=explode(",",$result["divarea"]);if(in_array("Pirojpur",$iao)){echo "checked";} echo '>
                                                    Pirojpur
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Patuakhali" ';$iao=explode(",",$result["divarea"]);if(in_array("Patuakhali",$iao)){echo "checked";} echo '>
                                                    Patuakhali
                                                </label>
                                            </div>
                                        </div>

                                        <div id="Sylhet" class="division_item">
                                            <h6>Sylhet Division *</h6>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Habiganj" ';$iao=explode(",",$result["divarea"]);if(in_array("Habiganj",$iao)){echo "checked";} echo '>
                                                    Habiganj
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Maulvibazar" ';$iao=explode(",",$result["divarea"]);if(in_array("Maulvibazar",$iao)){echo "checked";} echo '>
                                                    Maulvibazar
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Sunamganj" ';$iao=explode(",",$result["divarea"]);if(in_array("Sunamganj",$iao)){echo "checked";} echo '>
                                                    Sunamganj
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Sylhet" ';$iao=explode(",",$result["divarea"]);if(in_array("Sylhet",$iao)){echo "checked";} echo '>
                                                    Sylhet
                                                </label>
                                            </div>
                                        </div>

                                        <div id="Rangpur" class="division_item">
                                            <h6>Rangpur Division *</h6>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Dinajpur" ';$iao=explode(",",$result["divarea"]);if(in_array("Dinajpur",$iao)){echo "checked";} echo '>
                                                    Dinajpur
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Gaibandha" ';$iao=explode(",",$result["divarea"]);if(in_array("Gazipur",$iao)){echo "checked";} echo '>
                                                    Gaibandha
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Kurigram" ';$iao=explode(",",$result["divarea"]);if(in_array("Kurigram",$iao)){echo "checked";} echo '>
                                                    Kurigram
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Lalmonirhat" ';$iao=explode(",",$result["divarea"]);if(in_array("Lalmonirhat",$iao)){echo "checked";} echo '>
                                                    Lalmonirhat
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Nilphamari" ';$iao=explode(",",$result["divarea"]);if(in_array("Nilphamari",$iao)){echo "checked";} echo '>
                                                    Nilphamari
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Panchagarh" ';$iao=explode(",",$result["divarea"]);if(in_array("Panchagarh",$iao)){echo "checked";} echo '>
                                                    Panchagarh
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Rangpur" ';$iao=explode(",",$result["divarea"]);if(in_array("Rangpur",$iao)){echo "checked";} echo '>
                                                    Rangpur
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Thakurgaon" ';$iao=explode(",",$result["divarea"]);if(in_array("Thakurgaon",$iao)){echo "checked";} echo '>
                                                    Thakurgaon
                                                </label>
                                            </div>
                                        </div>

                                        <div id="Mymensingh" class="division_item">
                                            <h6>Mymensingh Division *</h6>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Jamalpur" ';$iao=explode(",",$result["divarea"]);if(in_array("Jamalpur",$iao)){echo "checked";} echo '>
                                                    Jamalpur
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Mymensingh" ';$iao=explode(",",$result["divarea"]);if(in_array("Mymensingh",$iao)){echo "checked";} echo '>
                                                    Mymensingh
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Netrakona" ';$iao=explode(",",$result["divarea"]);if(in_array("Netrakona",$iao)){echo "checked";} echo '>
                                                    Netrakona
                                                </label>
                                                <label>
                                                    <input type="checkbox" name="divarea[]" value="Sherpur" ';$iao=explode(",",$result["divarea"]);if(in_array("Sherpur",$iao)){echo "checked";} echo '>
                                                    Sherpur
                                                </label>
                                            </div>
                                        </div>

                                       <!-- <a href="#" class="btn addMoreDiv">Add More</a> -->
                                    </div>
                                </div>

                            </div>
                            <!--end form-group-->
                            
                        </div>
                        <!--end col-md-12-->
                    </div>
                </section>
                <!--number of employee section-->
                <section>
                <h3>Number of Employees</h3>
                <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="col-md-4 col-sm-4">
                    <label for="">Full Time *</label>
                    <input type="text" name="fte" required="required" class="form-control" id="fte" value="'.$fte.'"> 
                    </div>
                    <div class="col-md-4 col-sm-4">
                    <label for="">Part-Time *</label>
                    <input type="text" name="pte" required="required" class="form-control" id="" value="'.$pte.'">
                    </div>
                    <div class="col-md-4 col-sm-4">
                    <label for="">Volunteers *</label>
                    <input type="text" name="volunteers" required="required" class="form-control" id="volunteers"  value="'.$volunteers.'">
                    </div>
                </div>    
                
                </div>
                <h3>Interest Area of the organization *</h3>
                <div class="row">
                <?php $iao=explode(",",$result["iao"]); ?>
                <div class="col-md-12 col-sm-12">
                <div class="col-sm-6 col-md-6">
                <div class="checkbox">
                  <label><input type="checkbox" name="iao[]" value="Agriculture"';$iao=explode(",",$result["iao"]);if(in_array("Agriculture",$iao)){echo "checked";} echo '> Agriculture</label>
                </div>
                <div class="checkbox">
                  <label><input type="checkbox" name="iao[]" value="Education & Training" ';$iao=explode(",",$result["iao"]);if(in_array("Education & Training",$iao)){echo "checked";} echo ' >Education & Training</label>
                </div>
                <div class="checkbox">
                  <label><input type="checkbox" name="iao[]" value="Model UN"';$iao=explode(",",$result["iao"]);if(in_array("Model UN",$iao)){echo  "checked";} echo '>Model UN</label>
                </div>
                <div class="checkbox">
                  <label><input type="checkbox" name="iao[]" value="Science & Technology"';$iao=explode(",",$result["iao"]);if(in_array("Science & Technology",$iao)){echo  "checked";} echo '>Science & Technology</label>
                </div>
                <div class="checkbox">
                  <label><input type="checkbox" name="iao[]" value="Art & Culture"';$iao=explode(",",$result["iao"]);if(in_array("Art & Culture",$iao)){echo  "checked";} echo '>Art & Culture (Music, Dance, Literature, Paint, etc.)</label>
                </div>
                <div class="checkbox">
                  <label><input type="checkbox" name="iao[]" value="Law & Justice"';$iao=explode(",",$result["iao"]);if(in_array("Law & Justice",$iao)){echo  "checked";} echo '>Law & Justice (Democracy, Good Governance, Policies, etc.)</label>
                </div>
                <div class="checkbox">
                  <label><input type="checkbox" name="iao[]" value="Climate & Environment" ';$iao=explode(",",$result["iao"]);if(in_array("Climate & Environment",$iao)){echo  "checked";} echo '>Climate & Environment (Disaster Management, Climate Change, Deforestation, Pollution, Biodiversity, Global Warming, etc.)</label>
                </div>
                 <div class="checkbox">
                  <label><input type="checkbox" name="iao[]" value="Media & Communication"';$iao=explode(",",$result["iao"]);if(in_array("Media & Communication",$iao)){echo  "checked";} echo '>Media & Communication (Film, Photography, Journalism, Communication, etc.)</label>
                </div>
                
                </div>
                <div class="col-md-6 col-sm-6">
                <div class="checkbox">
                  <label><input type="checkbox" name="iao[]" value="Debate"';$iao=explode(",",$result["iao"]);if(in_array("Debate",$iao)){echo  "checked";} echo'>Debate</label>
                </div>
                <div class="checkbox">
                  <label><input type="checkbox" name="iao[]" value="Leadership" ';$iao=explode(",",$result["iao"]);if(in_array("Leadership",$iao)){echo  "checked";} echo '>Leadership</label>
                </div>
                <div class="checkbox">
                  <label><input type="checkbox" name="iao[]" value="Tourism"';$iao=explode(",",$result["iao"]);if(in_array("Tourism",$iao)){echo  "checked";} echo'>Tourism</label>
                </div>
                <div class="checkbox">
                  <label><input type="checkbox" name="iao[]" value="Volunteerism & Community Development"';$iao=explode(",",$result["iao"]);if(in_array("Volunteerism & Community Development",$iao)){echo  "checked";} echo'>Volunteerism & Community Development</label>
                </div>
                <div class="checkbox">
                  <label><input type="checkbox" name="iao[]" value="Sports"';$iao=explode(",",$result["iao"]);if(in_array("Sports",$iao)){echo "checked";} echo'>Sports (Football, Cricket, Chess, Handball, etc.)</label>
                </div>
                <div class="checkbox">
                  <label><input type="checkbox" name="iao[]" value="Human Rights & Social Inclusion" ';$iao=explode(",",$result["iao"]);if(in_array("Human Rights & Social Inclusion",$iao)){echo  "checked";} echo'>Human Rights & Social Inclusion (LGBT, Disability, Gender, Women and Children, Peace, etc.)</label>
                </div>
                <div class="checkbox">
                  <label><input type="checkbox" name="iao[]" value="employment & Entrepreneurship" ';$iao=explode(",",$result["iao"]);if(in_array("employment & Entrepreneurship",$iao)){echo  "checked";} echo'>Employment & Entrepreneurship (Career, Entrepreneur, Startup, Social Business, etc.)</label>
                </div>
                 <div class="checkbox">
                  <label><input type="checkbox" name="iao[]" value="others" ';$iao=explode(",",$result["iao"]);if(in_array("others",$iao)){echo  "checked";} echo'><input type="text" class="form-control" name="ia_of_org" value="'.$ia_of_org.'"> </label>
                </div>
                
                </div>
                
                </div>
                
                </div>
                 <h3>Please specify your registration / affiliation with others</h3>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="col-md-6 col-sm-6">
                            <h6><strong>Registration Authority *</strong></h6>
                             <div class="checkbox">
                                <label><input type="checkbox" name="ra[]" class="regaut" value="Govt" ';$iao=explode(",",$result["rauthority"]);if(in_array("Govt",$iao)){echo "checked";} echo'>Registered Under Bangladesh Govt.</label>
                            </div>
                            <div class="checkbox">
                                  <label><input type="checkbox" name="ra[]" class="affwfaut" value="Foreign Authority" ';$iao=explode(",",$result["rauthority"]);if(in_array("Foreign Authority",$iao)){echo "checked";} echo'>Affiliation With Foreign Authority</label>
                            </div>
                            
                        </div>
                        <div class="col-md-6 col-sm-6">
                             <div class="checkbox">
                                <label><input type="checkbox" name="ra[]" class="regunuc" value="University / College" ';$iao=explode(",",$result["rauthority"]);if(in_array("University / College",$iao)){echo "checked";} echo'>Registered Under University / College</label>
                            </div>
                             <div class="checkbox">
                                <label><input type="checkbox" name="ra[]" value="Not Registered" ';$iao=explode(",",$result["rauthority"]);if(in_array("Not Registered",$iao)){echo "checked";} echo' >Not Registered</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" name="ra[]" value="others" ';$iao=explode(",",$result["rauthority"]);if(in_array("others",$iao)){echo "checked";} echo'><input type="text" name="aff_with_other" value="'.$aff_with_other.'" class="form-control"> </label>
                            </div>
                        </div>


                        <div class="col-md-12 col-sm-12" id="regau">
                            <h3>Select from List of Govt. body</h3>
                            <div class="row">
                                <div class="col-md-6   col-sm-12">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="ra_1[]"  value="City Corporation" ';$vogt_name=explode(",",$result["vogt_name"]);if(in_array("City Corporation",$vogt_name)){echo "checked";} echo'>
                                            City Corporation
                                        </label>
                                        <label>
                                            <input type="checkbox" name="ra_1[]"  value="Ministry of Agriculture" ';$vogt_name=explode(",",$result["vogt_name"]);if(in_array("Ministry of Agriculture",$vogt_name)){echo "checked";} echo'>
                                            Ministry of Agriculture
                                        </label>
                                        <label>
                                            <input type="checkbox" name="ra_1[]"  value="Microcredit Regulatory Authority (MRA)" ';$vogt_name=explode(",",$result["vogt_name"]);if(in_array("Microcredit Regulatory Authority (MRA)",$vogt_name)){echo "checked";} echo'>
                                            Microcredit Regulatory Authority (MRA)
                                        </label>
                                        <label>
                                            <input type="checkbox" name="ra_1[]"  value="Register of Joint Stock Companies and Firms" ';$vogt_name=explode(",",$result["vogt_name"]);if(in_array("Register of Joint Stock Companies and Firms",$vogt_name)){echo "checked";} echo'>
                                            Register of Joint Stock Companies and Firms
                                        </label>
                                        <label>
                                            <input type="checkbox" name="ra_1[]"  value="Department of Youth Development / Ministry of Youth Affairs & Sports" ';$vogt_name=explode(",",$result["vogt_name"]);if(in_array("Department of Youth Development / Ministry of Youth Affairs & Sports",$vogt_name)){echo "checked";} echo' >
                                            Department of Youth Development / Ministry of Youth Affairs & Sports
                                        </label>
                                        <label>
                                            <input type="checkbox" name="ra_1[]" value="others" ';$vogt_name=explode(",",$result["vogt_name"]);if(in_array("others",$vogt_name)){echo "checked";} echo'>
                                            <input type="text" name="aff_with_other_1"  id="aff_with_other_1" value="'.$aff_with_other_1.'" />
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6  col-sm-12">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="ra_1[]"  value="NGO Affairs Bureau" ';$vogt_name=explode(",",$result["vogt_name"]);if(in_array("NGO Affairs Bureau",$vogt_name)){echo "checked";} echo' >
                                            NGO Affairs Bureau
                                        </label>
                                        <label>
                                            <input type="checkbox" name="ra_1[]"  value="Ministry of Health and Family Wellfare" ';$vogt_name=explode(",",$result["vogt_name"]);if(in_array("Ministry of Health and Family Wellfare",$vogt_name)){echo "checked";} echo'>
                                            Ministry of Health and Family Wellfare
                                        </label>
                                        <label>
                                            <input type="checkbox" name="ra_1[]"  value="Registered Board Deed / Trust Deed" ';$vogt_name=explode(",",$result["vogt_name"]);if(in_array("Registered Board Deed / Trust Deed",$vogt_name)){echo "checked";} echo'>
                                            Registered Board Deed / Trust Deed
                                        </label>
                                        <label>
                                            <input type="checkbox" name="ra_1[]"  value="Department of Social Services / Ministry of Social Welfare" ';$vogt_name=explode(",",$result["vogt_name"]);if(in_array("Department of Social Services / Ministry of Social Welfare",$vogt_name)){echo "checked";} echo' >
                                            Department of Social Services / Ministry of Social Welfare
                                        </label>
                                        <label>
                                            <input type="checkbox" name="ra_1[]"  value="Department of Women Affairs / Ministry of Women and Children Affairs" ';$vogt_name=explode(",",$result["vogt_name"]);if(in_array("Department of Women Affairs / Ministry of Women and Children Affairs",$vogt_name)){echo "checked";} echo'>
                                            Department of Women Affairs / Ministry of Women and Children Affairs
                                        </label>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="col-md-6  col-sm-6">
                                        <h4>GOVT. Registration number *</h4>
                                        <input type="text" name="registration_no" id="registration_no" class="form-control" value="'.$registration_no.'">
                                    </div>
                                    <div class="col-md-6  col-sm-6">
                                        <h4>GOVT. Registration date *</h4>
                                        <input type="text" name="govt_reg_date" id="from_reg" class="form-control date_picker" value="'.$govt_reg_date.'">
                                    </div>
                                </div>


                            </div>
                        </div>



                        <div class="col-md-12" id="reguc">
                            <h4>Please specify the name of university / college</h4>
                            <input type="text" name="college_name" id="college_name" value="'.$college_name.'" class="form-control">
                        </div>

                       

                        <div class="col-md-12"  id="afwfau">
                            <div class="row">
                                <div class="col-md-7">
                                    <h4>Foreign Authority Registration number</h4>
                                    <input type="text" name="Foreign_reg_number" id="Foreign_reg_number" value="'.$Foreign_reg_number.'" class="form-control">
                                </div>
                                <div class="col-md-5">
                                    <h4>Foreign Authority Registration Date</h4>
                                    <input type="text" name="non_gov_reg" id="non_gov_reg" class="form-contro" value="'.$non_gov_reg.'">
                                </div>
                                <div class="col-md-12">
                                    <h4>Please specify the name of foreign registration authority</h4>
                                    <input type="text" name="foreign_reg" id="foreign_reg" class="form-control" value="'.$foreign_reg.'">
                                </div>
                            </div>
                        </div>

                    </div>
               
                
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <label>How did you learn about Youthopia.bangla ? *</label>
                        <input type="text" class="form-control" name="learnabout" required value="'.$learnabout.'">
                    </div>
                </div>
                
                </section>
                <!--number of employee section end-->
              
                <section>
                    <h3>Gallery</h3><p>Ratio : width: 350px X height: 200px; </p>
                    <div class="file-upload-previews">'.$prevGallery.'</div>
                    
                    <input type="file" name="gallery-files">
                    <!--<div class="file-upload">
                        <input type="file" class="file-upload-input with-preview" id="galleryfiles" maxlength="10" accept="gif|jpg|png" name="galleryfiles[]" multiple />
                            <span>Click or drag image here</span>
                            <output id="list"></output>
                    </div>-->
                    <!--end form-group-->
                </section>
                 <section>
                    <h3>logo</h3><p>Ratio : width: 100px X height:  100px</p>
                    <div class="file-upload-previews">'.$prevLogo.'</div>
                    <input type="file" name="logo-files">
                    <!--<div class="file-upload">
                        <input type="file" class="file-upload-input with-preview" id="files" maxlength="10" accept="gif|jpg|png" name="files" />
                       
                            <span>Click or drag image here</span>
                             <output id="listLogo"></output>
                    </div>-->
                    <!--end form-group-->
                </section>
                <hr>
                <input hidden type="text" name="listingData" id="listingData" value="listingData">
                <section class="center">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-rounded"> Submit Listing</button>
                    </div>
                    <!--end form-group-->
                </section>
            </form>
            <!--end form-->
        </div>
        <!--end modal-body-->
    </div>
    <!--end modal-content-->
</div>
<!--end modal-dialog-->';?>
<style type="text/css">
    .thumb {
      width: 50px;
      height: 50px;
    }

    #list {
      position: absolute;
      top: 0px;
    }
    #list div {
      float: left;
      margin-right: 10px;
    }
</style>

<script type="text/javascript">
    $(document).ready(function(){

        var regautbtn = $('.regaut'),
            afwfabtn = $('.affwfaut'),
            reguucbtn = $('.regunuc'),
            regau = $('#regau').hide(),
            afwfa = $('#afwfau').hide(),
            reguuc = $('#reguc').hide(),
            
            alldisban = $('.alldisban'),
            partdis = $('.partdis'),
            max_fild = 8,
            wrp = $('.div_wrap').hide(),
            add = $('.addMoreDiv'),
            x = 0;
           
            <?php 
            
            $ra=explode(",",$result["rauthority"]); 
             if(in_array("Govt",$ra))
             {
                ?>
                
                $('#regau').show();
                
                <?php 
             }
            ?>
            
            <?php
            if(in_array("Foreign Authority",$ra))
             {
                ?>
                
                $('#afwfau').show();
                
                <?php 
             }
            ?>
            
            <?php
            if(in_array("University / College",$ra))
             {
                ?>
                
                $('#reguc').show();
                
                <?php 
             }
            ?>
            
            <?php
            if("particular district"==$web)
             {
                ?>
                  
                   $('.div_wrap').show();
                
                
                <?php 
             }
            ?>
            
        alldisban.on('click', function(){
            wrp.hide();
        }); 
        partdis.on('click', function(){
            wrp.toggle('slow');
        });        
        regautbtn.on('click', function(){
          if(jQuery(this).is(":checked")){
               regau.show('slow');
            }
        else{
            
            regau.hide('slow');
        }
            
            //afwfa.hide();
            //reguuc.hide();
        });
        afwfabtn.on('click', function(){
            //regau.hide();
            //afwfa.toggle('slow');
            //reguuc.hide();
            
            if(jQuery(this).is(":checked")){
               afwfa.show('slow');
            }
        else{
            
            afwfa.hide('slow');
        }
        });
        reguucbtn.on('click', function(){
           // regau.hide();
            //afwfa.hide();
            //reguuc.toggle('slow');
            if(jQuery(this).is(":checked")){
               reguuc.show('slow');
            }
        else{
            
            reguuc.hide('slow');
        }
        });

    var max_fild = 8,
     
        add = $('.addMoreDiv'),
        x = 0;

        $(add).on('click', function(e){
            e.preventDefault();

            if (x < max_fild) {
                x++;
                $(wrp).append('<div class="form-group col-md-12 col-sm-12"><a href="#" class="remove_items">Remove</a><h5>Division area</h5> <select class="form-control selectpicker" name="divarea" id="divarea"><option value="rediv"></option><option value="dhkdis">Dhaka Division</option><option value="ctgdis">Chittagong Division</option><option value="rajdis">Rajshahi Division</option><option value="khuldis">Khulna Division</option><option value="baridis">Barisal Division</option><option value="syldis">Sylhet Division</option><option value="rongdis">Rangpur Division</option><option value="mymedis">Mymensingh Division</option> </select><div id="rediv" class="division_item"></div><div id="dhkdis" class="division_item"><h6>Dhaka Division *</h6><div class="checkbox"> <label> <input type="checkbox" value="Dhaka"> Dhaka </label> <label> <input type="checkbox" value="Faridpur"> Faridpur </label> <label> <input type="checkbox" value="Gazipur"> Gazipur </label> <label> <input type="checkbox" value="Gopalganj"> Gopalganj </label> <label> <input type="checkbox" value="Kishoreganj"> Kishoreganj </label> <label> <input type="checkbox" value="Madaripur"> Madaripur </label> <label> <input type="checkbox" value="Manikganj"> Manikganj </label> <label> <input type="checkbox" value="Munshiganj"> Munshiganj </label> <label> <input type="checkbox" value="Narayanganj"> Narayanganj </label> <label> <input type="checkbox" value="Narsingdi"> Narsingdi </label> <label> <input type="checkbox" value="Rajbari"> Rajbari </label> <label> <input type="checkbox" value="Shariatpur"> Shariatpur </label> <label> <input type="checkbox" value="Tangail"> Tangail </label></div></div><div id="ctgdis" class="division_item"><h6>Chittagong Division *</h6><div class="checkbox"> <label> <input type="checkbox" value="Bandarban"> Bandarban </label> <label> <input type="checkbox" value="Brahmmanbaria"> Brahmmanbaria </label> <label> <input type="checkbox" value="Chandpur"> Chandpur </label> <label> <input type="checkbox" value="Chittagong"> Chittagong </label> <label> <input type="checkbox" value="Comilla"> Comilla </label> <label> <input type="checkbox" value="Cox’s  Bazar"> Cox’s  Bazar </label> <label> <input type="checkbox" value="Feni"> Feni </label> <label> <input type="checkbox" value="Khagrachhari"> Khagrachhari </label> <label> <input type="checkbox" value="Lakshmipur"> Lakshmipur </label> <label> <input type="checkbox" value="Noakhali"> Noakhali </label> <label> <input type="checkbox" value="Rangamati"> Rangamati </label></div></div><div id="rajdis" class="division_item"><h6>Rajshahi Division *</h6><div class="checkbox"> <label> <input type="checkbox" value="Bogra"> Bogra </label> <label> <input type="checkbox" value="Chapai Nawabganj"> Chapai Nawabganj </label> <label> <input type="checkbox" value="Joypurhat"> Joypurhat </label> <label> <input type="checkbox" value="Naogaon"> Naogaon </label> <label> <input type="checkbox" value="Natore"> Natore </label> <label> <input type="checkbox" value="Pabna"> Pabna </label> <label> <input type="checkbox" value="Rajshahi"> Rajshahi </label> <label> <input type="checkbox" value="Sirajganj"> Sirajganj </label></div></div><div id="khuldis" class="division_item"><h6>Khulna Division *</h6><div class="checkbox"> <label> <input type="checkbox" value="Bagerhat"> Bagerhat </label> <label> <input type="checkbox" value="Chuadanga"> Chuadanga </label> <label> <input type="checkbox" value="Jhenaidah"> Jhenaidah </label> <label> <input type="checkbox" value="Jessore"> Jessore </label> <label> <input type="checkbox" value="Khulna"> Khulna </label> <label> <input type="checkbox" value="Kushtia"> Kushtia </label> <label> <input type="checkbox" value="Magura"> Magura </label> <label> <input type="checkbox" value="Meherpur"> Meherpur </label> <label> <input type="checkbox" value="Narail"> Narail </label> <label> <input type="checkbox" value="Satkhira"> Satkhira </label></div></div><div id="baridis" class="division_item" class="division_item"><h6>Barisal Division *</h6><div class="checkbox"> <label> <input type="checkbox" value="Barisal"> Barisal </label> <label> <input type="checkbox" value="Bhola"> Bhola </label> <label> <input type="checkbox" value="Barguna"> Barguna </label> <label> <input type="checkbox" value="Jhalakhati"> Jhalakhati </label> <label> <input type="checkbox" value="Pirojpur"> Pirojpur </label> <label> <input type="checkbox" value="Patuakhali"> Patuakhali </label></div></div><div id="syldis" class="division_item"><h6>Sylhet Division *</h6><div class="checkbox"> <label> <input type="checkbox" value="Habiganj"> Habiganj </label> <label> <input type="checkbox" value="Maulvibazar"> Maulvibazar </label> <label> <input type="checkbox" value="Sunamganj"> Sunamganj </label> <label> <input type="checkbox" value="Sylhet"> Sylhet </label></div></div><div id="rongdis" class="division_item"><h6>Rangpur Division *</h6><div class="checkbox"> <label> <input type="checkbox" value="Dinajpur"> Dinajpur </label> <label> <input type="checkbox" value="Gaibandha"> Gaibandha </label> <label> <input type="checkbox" value="Kurigram"> Kurigram </label> <label> <input type="checkbox" value="Lalmonirhat"> Lalmonirhat </label> <label> <input type="checkbox" value="Nilphamari"> Nilphamari </label> <label> <input type="checkbox" value="Panchagarh"> Panchagarh </label> <label> <input type="checkbox" value="Rangpur"> Rangpur </label> <label> <input type="checkbox" value="Thakurgaon"> Thakurgaon </label></div></div><div id="mymedis" class="division_item"><h6>Mymensingh Division *</h6><div class="checkbox"> <label> <input type="checkbox" value="Jamalpur"> Jamalpur </label> <label> <input type="checkbox" value="Mymensingh"> Mymensingh </label> <label> <input type="checkbox" value="Netrakona"> Netrakona </label> <label> <input type="checkbox" value="Sherpur"> Sherpur </label></div></div></div>');
            }
        });

        $(wrp).on('click', '.remove_items', function(e){
            e.preventDefault();
            $(this).parent('div').remove();
            x--;
        });

    $("#divarea").change(function(){
        $(this).find("option:selected").each(function(){
            var optionValue = $(this).attr("value");
            if(optionValue){
                $(".division_item").not("#" + optionValue).hide();
                $("#" + optionValue).show();
            } else{
                $(".division_item").hide();
            }
        });
    }).change();

         function handleFileSelect(evt) {
                var files = evt.target.files; // FileList object

                // Loop through the FileList and render image files as thumbnails.
                for (var i = 0, f; f = files[i]; i++) {

                  // Only process image files.
                  if (!f.type.match('image.*')) {
                    continue;
                  }

                  var reader = new FileReader();

                  // Closure to capture the file information.
                  reader.onload = (function(theFile) {
                    return function(e) {
                      // Render thumbnail.
                      var span = document.createElement('div');
                      span.setAttribute('class', 'img_wrpv');
                      span.innerHTML = ['<a href="#" style="color:red; font-size:18px; position: relative; z-index: 200;" class="img_revg">x</a><img width="50px" height="50px" class="thumb" src="', e.target.result,
                                        '" title="', escape(theFile.name), '"/>'].join('');
                      document.getElementById('list').insertBefore(span, null);
                    };
                  })(f);

                  // Read in the image file as a data URL.
                  reader.readAsDataURL(f);

                  //remove image
                    $('.img_revg').on('click', function(e){
                        e.preventDefault();
                        var parent = $(this).parent();
                        console.log(parent);
                        parent.remove();
                        x--;
                    });

                }
              }
               function handleFileSelectLogo(evt) {
                var files = evt.target.files; // FileList object

                // Loop through the FileList and render image files as thumbnails.
                for (var i = 0, f; f = files[i]; i++) {

                  // Only process image files.
                  if (!f.type.match('image.*')) {
                    continue;
                  }

                  var reader = new FileReader();

                  // Closure to capture the file information.
                  reader.onload = (function(theFile) {
                    return function(e) {
                    
                      // Render thumbnail.
                      var span = document.createElement('div');
                      span.setAttribute('class', 'img_wrp');
                     
                      span.innerHTML = ['<a href="javascript:void(0)" style="color:red; font-size:18px; position: relative; z-index: 99000;" class="img_rev" onclick="getAlert()">x</a><img width="50px" height="50px" class="thumb" src="', e.target.result,
                                        '" title="', escape(theFile.name), '"/>'].join('');
                                        
                      document.getElementById('listLogo').insertBefore(span, null);
                    };
                  })(f);

                  // Read in the image file as a data URL.
                  reader.readAsDataURL(f);

                  //remove image
                    $('.img_rev').on('click', function(e){
                        e.preventDefault();
                   
                        var parent = $(this).parent();
                        console.log(parent);
                        parent.remove();
                        
                    });

                }
              }
               $('.img_rev').on('click', function(e){
                        e.preventDefault();
                   
                        var parent = $(this).parent();
                        console.log(parent);
                        parent.remove();
                        
                    });

//               document.getElementById('galleryfiles').addEventListener('change', handleFileSelect, true);
//               document.getElementById('files').addEventListener('change', handleFileSelectLogo, true);

             
});
</script>
<script>
jQuery(document).ready(function() {

  $('#listingForm').on('submit', function (e) {

        var fd = new FormData(this);
        //var submit=$('#submitIndividual');
        $.ajax({
            type: "POST",
            url: "process.php",
            data: fd,
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                //alert(1);
                $('#listingMessage').html(data);
            }
        });

        e.preventDefault();
        return false;


    });

});
</script>
    
 <script>
  jQuery( function() {
    jQuery( "#edo_2" ).datepicker({ dateFormat: 'yy-mm-dd' });
    jQuery( "#from_reg" ).datepicker({ dateFormat: 'yy-mm-dd' });
    jQuery( "#non_gov_reg" ).datepicker({ dateFormat: 'yy-mm-dd' });

  } );
  
  function removeImge(imgID){
      
      $.ajax({
            type: "GET",
            url: "process.php",
            data: 'removeProfileImage='+imgID,
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                jQuery('#event_'+imgID).remove();
            }
        });
      

  }
 
  </script>
<script src="assets/vendors/fileuploader/src/jquery.fileuploader.min.js" type="text/javascript"></script>
<script src="assets/vendors/fileuploader/examples/another-default-upload/js/custom.js" type="text/javascript"></script>
