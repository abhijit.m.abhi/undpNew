<?php
echo'
<div class="modal-dialog width-400px" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="section-title">
                <h2>Register</h2>
            </div>
        </div>
        <div class="modal-body">

            <div class="signup_modal_tab_wrp">
                <h6>SIGN UP AS</h6>
                <ul class="signup_modal_tab">
                    <li class="active"><a href="#individual" data-toggle="tab" id="Ind">individual</a></li>
                    <li><a href="#organisation" data-toggle="tab" id="Org">organization</a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div id="individual" class="tab-pane active">

                        <div class="signup_individual_form">
                            <div class="social_signup">
                                <a class="facebook_icon" href="#"><i class="fa fa-facebook"></i></a>
                                <span>/</span>
                                <a class="gplus_icon" href="#"><i class="fa fa-google-plus"></i></a>
                                <p>or,</p>
                            </div>  
                            <div class="signup_modal_form individual">
                                <div id="messageIndividual"></div>
                                <form id="formIndividual" action="" method="POST" enctype="multipart/form-data">
                                    <div class="form_group">
                                        <input class="signup_ind_name" type="text" name="fname" id="fname" placeholder="Full Name"/>
                                    </div>
                                    <div class="form_group">
                                        <input class="signup_ind_contact" type="text" name="phoneNumber" id="phoneNumber" placeholder="Contact Number"/>
                                    </div>
                                    <div class="form_group">
                                        <input class="signup_ind_birth date_picker" type="text" id="date" name="date" placeholder="YY-MM-DD"/>
                                    </div>
                                    <div class="form_group">
                                        <select class="signup_ind_gender" id="gender" name="gender">
                                            <option value="Male">Male</option>
                                            <option value="Female">Female</option>
                                            <option value="Others">Others</option>
                                        </select>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="form_group">
                                        <div class="input-group">
                                            <label class="input-group-btn">
                                                <span class="btn btn-primary btn-xs" style="font-size:14px;padding: 10px 10px">
                                                    Browse&hellip; <input type="file" style="display: none;" name="file" id="file" multiple>
                                                </span>
                                            </label>
                                            <input type="text" class="form-control" disabled style="border-radius:0px;" readonly placeholder="Upload Your Logo">
                                        </div>
                                    </div>
                                    <div class="form_group">
                                        <input class="signup_ind_email" type="email" name="email" id="email" placeholder="Email ID"/>
                                    </div>
                                    <div class="form_group">
                                        <input class="signup_ind_pass1" type="password" name="password" id="password" placeholder="Password"/>
                                    </div>
                                    <div class="form_group">
                                        <input class="signup_ind_pass2" type="password" name="re_pass" id="re_pass" placeholder="Conﬁrm Password"/>
                                    </div>
                                    <div class="signup_modal_chkbox">
                                        <div><input class="signup_ind_chkbox" type="checkbox" id="agree" name="agree" value="agree" checked />
                                        <label for="agree">I ACCEPT <a href="#">TERMS &amp; CONDITION</a></label></div>
                                        <input type="hidden" name="individual" id="individual" value="individual" >
                                        <input hidden type="text" name="submitIndividual" id="submitIndividual" value="submitIndividual">
                                        <button type="submit" class="submitindiorg" id="submitIndividual" name="submitIndividual">Request for Registration</button>
                                    </div>
                                </form>
                            </div>  
                        </div>
                    </div>
                    <div id="organisation" class="tab-pane fade">
                        <div class="signup_organization_form">
                            <div class="signup_modal_form organisation">
                                <div id="messageOrganization"></div>
                                <form  id="formOrganization" action="" method="POST" enctype="multipart/form-data">
                                    <div class="form_group">
                                        <input class="signup_org_name" type="text" name="oname" id="oname" placeholder="Organization Name"/>
                                    </div>
                                    <div class="form_group">
                                        <input class="signup_org_email" type="email" name="oemail" id="oemail" placeholder="Organization Email"/>
                                    </div>
                                    <div class="form_group">
                                        <input class="signup_org_fb" type="text" name="facebook" id="facebook" placeholder="Facebook Page Link"/>
                                    </div>
                                    <div class="form_group">
                                        <input class="signup_org_web" type="text" name="website" id="website" placeholder="Website"/>
                                    </div>
                                    <div class="form_group">
                                        <input class="signup_org_contact_name" type="text" name="fname" id="fname" placeholder="Contact Person Name"/>
                                    </div>
                                    <div class="form_group">
                                        <input class="signup_org_contact_email" type="email" name="email" id="email" placeholder="Contact Person Email"/>
                                    </div>
                                    <div class="form_group">
                                        <input class="signup_org_mobile" type="text" name="number" id="number" placeholder="Contact Person Mobile Number"/>
                                    </div>
                                    <div class="form_group">
                                        <div class="input-group">
                                            <label class="input-group-btn">
                                                <span class="btn btn-primary btn-xs" style="font-size:14px;padding: 10px 10px">
                                                    Browse&hellip; <input type="file" style="display: none;" name="file" id="file" multiple>
                                                </span>
                                            </label>
                                            <input type="text" class="form-control" disabled style="border-radius:0px;" readonly placeholder="Upload Your Logo">
                                        </div>
                                    </div>
                                    <div class="form_group">
                                        <input class="signup_org_password1" type="password" name="password" id="password" placeholder="Password"/>
                                    </div>
                                    <div class="form_group">
                                        <input class="signup_org_password2" type="password" name="re_pass" id="re_pass" placeholder="Conﬁrm Password"/>
                                    </div>
                                    <div class="signup_modal_chkbox">
                                        <div><input class="signup_ind_chkbox" type="checkbox" id="i_accept2" name="agree" id="agree" value="agree" checked/>
                                        <label for="i_accept2">I ACCEPT <a href="#">TERMS &amp; CONDITION</a></label></div>
                                        <input type="hidden" name="organization" id="organization" value="organization" >
                                        <input type="hidden" name="submitOrganization" id="submitOrganization" value="submitOrganization">
                                        <button type="submit" class="submitindiorg" id="submitOrganization" name="submitOrganization">Request for Registration</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <hr>

            <p class="center note">By clicking on “Register Now” button you are accepting the <a href="terms-and-conditions.html" class="underline">Terms & Conditions</a></p>
            <!--end form-->
        </div>
        <!--end modal-body-->
    </div>
    <!--end modal-content-->
</div>
<!--end modal-dialog-->

'?>

<style type="text/css">
.signup_modal_tab {
  list-style: none;
  margin-bottom: 8px;
  padding: 0px;
}
.signup_modal_tab li {
  display: inline-block;
  background: #eee;
  margin-right: 10px;
  font-size: 16px;
  padding: 7px 10px;
  text-transform: capitalize;
  border: 1px solid #ccc;
}

.signup_modal_tab {
  list-style: none;
  margin-bottom: 8px;
  padding: 0px;
}
.signup_modal_tab li {
  display: inline-block;
  background: #eee;
  margin-right: 10px;
  font-size: 16px;
  padding: 7px 10px;
  text-transform: capitalize;
  border: 1px solid #ccc;
}
.social_signup {
  text-align: center;
}
.facebook_icon {
}
.social_signup a {
  width: 40px;
  height: 40px;
  display: inline-block;
  line-height: 40px;
  color: #fff;
}
.facebook_icon {
  background: #5D7FBF;
  border-radius: 50%;
  margin-right: 10px;
}
.gplus_icon {
  background: #E43433;
  border-radius: 50%;
  margin-left: 10px;
}

.individual .form_group {
    margin-bottom: 15px;
}
.organisation .form_group {
    margin-bottom: 15px;
}
.submitindiorg {
    background: #01263A;
    box-shadow: none;
    border-color: #01263A;
    color: #fff;
    padding: 10px;
    outline: none;
    margin-top: 15px;
}
</style>