$(document).ready(function() {
	
	// enable fileupload plugin
	$('input[name="individual-file"]').fileuploader({
        extensions: ['jpg', 'jpeg', 'png', 'gif', 'bmp'],
        limit: 1,
        maxSize: 5,
        fileMaxSize: 5,
        required: true,
        addMore: false,
		upload: {
            url: 'assets/vendors/fileuploader/examples/default-upload/php/ajax_upload_file.php',
            data: null,
            type: 'POST',
            enctype: 'multipart/form-data',
            start: true,
            synchron: true,
            beforeSend: function(item) {
				var input = $('#custom_file_name');
				
				// set the POST field
				if(input.length)
					item.upload.data.custom_name = input.val();
				
				// reset input value
				input.val("");
			},
            onSuccess: function(result, item) {
                var data = JSON.parse(result),
					nameWasChanged = false;
				
				// get the new file name
                if(data.isSuccess && data.files[0]) {
					nameWasChanged = item.name != data.files[0].name;
					
                    item.name = data.files[0].name;
                    $('#individual-hidden').html('<input type="hidden" name="individual-photo" id="individual-photo" value="'+item.name+'" />');
                    $("#submitIndividual").removeAttr("disabled");
                    window.onbeforeunload = function() {
                        return "Are you sure do not want to save register data?";
                    }
                }
                
				// make HTML changes
				if(nameWasChanged)
					item.html.find('.column-title div').animate({opacity: 0}, 400);
                item.html.find('.column-actions').append('<a class="fileuploader-action fileuploader-action-remove fileuploader-action-success" title="Remove"><i></i></a>');
                setTimeout(function() {
					item.html.find('.column-title div').attr('title', item.name).text(item.name).animate({opacity: 1}, 400);
                    item.html.find('.progress-bar2').fadeOut(400);
                }, 400);
            },
            onError: function(item) {
				var progressBar = item.html.find('.progress-bar2');
				
				// make HTML changes
				if(progressBar.length > 0) {
					progressBar.find('span').html(0 + "%");
                    progressBar.find('.fileuploader-progressbar .bar').width(0 + "%");
					item.html.find('.progress-bar2').fadeOut(400);
				}
                
                item.upload.status != 'cancelled' && item.html.find('.fileuploader-action-retry').length == 0 ? item.html.find('.column-actions').prepend(
                    '<a class="fileuploader-action fileuploader-action-retry" title="Retry"><i></i></a>'
                ) : null;
            },
            onProgress: function(data, item) {
                var progressBar = item.html.find('.progress-bar2');
				
				// make HTML changes
                if(progressBar.length > 0) {
                    progressBar.show();
                    progressBar.find('span').html(data.percentage + "%");
                    progressBar.find('.fileuploader-progressbar .bar').width(data.percentage + "%");
                }
            },
            onComplete: null,
        },
		onRemove: function(item) {
			// send POST request
			$.post('assets/vendors/fileuploader/examples/default-upload/php/ajax_remove_file.php', {
				file: item.name},
                function(data,status){
                    $('#individual-hidden').html('');
                    $("#submitIndividual").attr("disabled","disabled");
                    window.onbeforeunload = null;
			});
		}
	});
    // enable fileupload plugin
    $('input[name="organization-file"]').fileuploader({
        extensions: ['jpg', 'jpeg', 'png', 'gif', 'bmp'],
        limit: 1,
        maxSize: 5,
        fileMaxSize: 5,
        required: true,
        addMore: false,
        upload: {
            url: 'assets/vendors/fileuploader/examples/default-upload/php/org_ajax_file_upload.php',
            data: null,
            type: 'POST',
            enctype: 'multipart/form-data',
            start: true,
            synchron: true,
            beforeSend: function(item) {
                var input = $('#custom_file_name');

                // set the POST field
                if(input.length)
                    item.upload.data.custom_name = input.val();

                // reset input value
                input.val("");
            },
            onSuccess: function(result, item) {
                var data = JSON.parse(result),
                    nameWasChanged = false;

                // get the new file name
                if(data.isSuccess && data.files[0]) {
                    nameWasChanged = item.name != data.files[0].name;

                    item.name = data.files[0].name;
                    $('#organization-hidden').html('<input type="hidden" name="organization-photo" id="organization-photo" value="'+item.name+'" />');
                    $("#submitOrganization").removeAttr("disabled");
                    window.onbeforeunload = function() {
                        return "Are you sure do not want to save register data?";
                    }
                }

                // make HTML changes
                if(nameWasChanged)
                    item.html.find('.column-title div').animate({opacity: 0}, 400);
                item.html.find('.column-actions').append('<a class="fileuploader-action fileuploader-action-remove fileuploader-action-success" title="Remove"><i></i></a>');
                setTimeout(function() {
                    item.html.find('.column-title div').attr('title', item.name).text(item.name).animate({opacity: 1}, 400);
                    item.html.find('.progress-bar2').fadeOut(400);
                }, 400);
            },
            onError: function(item) {
                var progressBar = item.html.find('.progress-bar2');

                // make HTML changes
                if(progressBar.length > 0) {
                    progressBar.find('span').html(0 + "%");
                    progressBar.find('.fileuploader-progressbar .bar').width(0 + "%");
                    item.html.find('.progress-bar2').fadeOut(400);
                }

                item.upload.status != 'cancelled' && item.html.find('.fileuploader-action-retry').length == 0 ? item.html.find('.column-actions').prepend(
                        '<a class="fileuploader-action fileuploader-action-retry" title="Retry"><i></i></a>'
                    ) : null;
            },
            onProgress: function(data, item) {
                var progressBar = item.html.find('.progress-bar2');

                // make HTML changes
                if(progressBar.length > 0) {
                    progressBar.show();
                    progressBar.find('span').html(data.percentage + "%");
                    progressBar.find('.fileuploader-progressbar .bar').width(data.percentage + "%");
                }
            },
            onComplete: null,
        },
        onRemove: function(item) {
            // send POST request
            $.post('assets/vendors/fileuploader/examples/default-upload/php/org_ajax_file_remove.php', {
                file: item.name},
                function(data,status){
                    $('#organization-hidden').html('');
                    $("#submitOrganization").attr("disabled","disabled");
                    window.onbeforeunload = null;
            });
        }
    });
    $('[data-toggle="tooltip"]').tooltip();
});

