<?php
include('../../../../../../connection_form.php');
include('../../../src/class.fileuploader.php');

// initialize FileUploader
$FileUploader = new FileUploader('logo-files', array(
    'limit' => 1,
    'maxSize' => 5,
    'fileMaxSize' => 5,
    'extensions' => ['jpg', 'jpeg', 'png', 'gif', 'bmp'],
    'required' => false,
    'uploadDir' => '../../../../../../upload/organization/logo/',
    'title' => '{timestamp}--{file_name}',
    'replace' => false,
    'listInput' => true,
    'files' => null
));

// call to upload the files
$data = $FileUploader->upload();

if ($data['isSuccess'] && count($data['files']) > 0) {
    $uploadedFiles = $data['files'];
    foreach ($uploadedFiles as $uploadedFile){

        $org_id = $_POST['org_id'];
        $user_id = $_POST['user_id'];
        $image_path = 'upload/organization/logo/'.$uploadedFile['name'];

        $checkImage = "SELECT * FROM `organization_images` WHERE `organization_id`='$org_id' AND `type`='logo' LIMIT 1";
        $result = $conn->query($checkImage);
        if ($result->num_rows > 0) {
//            $row = $result->fetch_assoc();
//            $org_id = $row['organization_id'];
            $sql = "UPDATE `organization_images` SET `path`='$image_path' WHERE `organization_id`=$org_id AND `type`='logo'";
            $conn->query($sql);
        }
        else{
            $sql = "INSERT INTO `organization_images` (`organization_id`, `path`,  `type`) VALUES ('$org_id', '$image_path', 'logo')";
            $conn->query($sql);
        }
        $sql = "UPDATE `listing` SET `logo`='$image_path', `marker_image`='$image_path' WHERE `id`=$org_id";
        $conn->query($sql);
        $sql = "UPDATE `users` SET `image`='$image_path' WHERE `userId`=$user_id";
        $conn->query($sql);

    }
}

// export to js
echo json_encode($data);
exit;