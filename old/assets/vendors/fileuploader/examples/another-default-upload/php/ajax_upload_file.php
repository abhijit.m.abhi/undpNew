<?php
    include('../../../../../../connection_form.php');
    include('../../../src/class.fileuploader.php');


	// initialize FileUploader
    $FileUploader = new FileUploader('gallery-files', array(
        'limit' => 5,
        'maxSize' => 25,
        'fileMaxSize' => 5,
        'extensions' => ['jpg', 'jpeg', 'png', 'gif', 'bmp'],
        'required' => false,
        'uploadDir' => '../../../../../../upload/organization/gallery/',
        'title' => '{timestamp}--{file_name}',
        'replace' => false,
        'listInput' => true,
        'files' => null
    ));
	
	// call to upload the files
    $data = $FileUploader->upload();

    if ($data['isSuccess'] && count($data['files']) > 0) {
        $uploadedFiles = $data['files'];
        foreach ($uploadedFiles as $uploadedFile){

            $org_id = $_POST['org_id'];
            $image_path = 'upload/organization/gallery/'.$uploadedFile['name'];

            $sql = "INSERT INTO `organization_images` (`organization_id`, `path`,  `type`) VALUES ('$org_id', '$image_path', 'gallery')";
            $conn->query($sql);

        }
    }

	// export to js
	echo json_encode($data);
	exit;