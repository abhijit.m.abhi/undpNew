<!DOCTYPE html>

<html lang="en-US">
<head>
  <meta charset="UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="author" content="ThemeStarz">

  <link href="<?php echo base_url(); ?>portalAssets/fonts/font-awesome.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url(); ?>portalAssets/fonts/elegant-fonts.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700,900,400italic' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="<?php echo base_url(); ?>portalAssets/bootstrap/css/bootstrap.css" type="text/css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>portalAssets/css/owl.carousel.css" type="text/css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>portalAssets/css/style.css" type="text/css">

  <title>Locations - Directory Listing HTML Template</title>
  <style>
  /*.form-control,btn{
    max-height:20px;
  }*/
  #page-header{
    background-color:green!important;
  }
  .secondary-nav{
    vertical-align:middle;
  }
  .rcorners2 {
    border-radius: 50%;
    border: 2px solid #fff;
    padding: 20px;
    width: 100%;
    height: auto;
    color: #fff;
    background-color: #3b5998;
    text-align: center;
    vertical-align: middle;
}
.rcorners2:hover{
  border: 2px solid #000;
  background-color: #4611a7;
}
.alignMent{
  vertical-align: middle;
}
.giFont{
 font-size: 30px;
 text-align: center;
}
.bottomText{
  display: none;
}
.socialIcon{
  background-color: #fff;
  color: blue;
}
#page-content{
  background:url(<?php echo base_url(); ?>portalAssets/img/home_bg.jpg);
  background-size: 100% 100%;
}
.registerBg{
  border:2px solid #3b5998;
  border-radius: 5px;
  min-height:510px;
}
.registerBg h3{
  text-align: center;
  margin-top: 10px;
}


  </style>
</head>

<body>
  <div class="page-wrapper">
    <header id="page-header">
      <nav>
        <div class="left">
          <a href="index.html" class="brand"><img src="<?php echo base_url(); ?>portalAssets/img/logo.png" alt=""></a>
        </div>
        <!--end left-->
        <div class="right">
          <div class="primary-nav has-mega-menu">

            <ul class="navigation">
              <li class=""><a href="#">Home</a>
              </li>
              <li><a href="blog.html">Directory</a></li>
              <li><a href="contact.html">Opportunities</a></li>
              <li><a href="contact.html">Media Blog</a></li>
              <li><a href="contact.html">Skill Development</a></li>
            </ul>
            <!--end navigation-->
          </div>
          <!--end primary-nav-->
          <div class="secondary-nav">
            <div class="row">
              <div class=" inputField col-md-5">
                <input type="text" class="form-control" name="emailAddress" id="emailAddress" placeholder="Email Address">
              </div>
              <div class="inputField col-md-5">
                <input type="password" class="form-control" name="password" id="password" placeholder="password">
              </div>
              <div class="inputField col-md-2">
                <button type="submit" class="btn btn-primary btn-xs">Login</button>
              </div>
            </div>
            <div class="row">
              <div class="col-md-5">
              </div>
              <div class="col-md-5">
                <center>
                  <!-- <a href="#"> Forgot your password?  </a> -->
                </center>
              </div>
            </div>
          </div>
          <!--end secondary-nav-->

          <div class="nav-btn">
            <i></i>
            <i></i>
            <i></i>
          </div>
        </div>
      </nav>
    </header>
    <div id="page-content" style="">
      <div class="container" >
        <!--
        <ol class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">Pages</a></li>
        <li class="active">Contact</li>
      </ol>
      <!--end breadcrumb-->
      <!--end page-title-->
      <section>
        <div class="col-md-7">
          <section class="page-title center">
          <img src="<?php echo base_url(); ?>portalAssets/img/logoBig.png"> <br><br>
          <b>Login With </b>   <a  href="#" class="circle-icon socialIcon">
            <i class="social_facebook"></i></a>
          </section>
          <a href="#" class="circleElement">
            <div class="col-md-2 center">
              <h4> <b>Directory</b></4>
              <div class="rcorners2">
                  <div class="alignMent">
                      <span class="glyphicon glyphicon-road giFont"></span><br>
                  </div>
              </div>
                <span class="bottomText">Directory</span>
            </div>
          </a>
          <a href="#" class="circleElement">
            <div class="col-md-2 center">
              <h4> <b>Calendar</b></4>
              <div class="rcorners2">
                  <div class="alignMent">
                      <span class="glyphicon glyphicon-calendar giFont"></span><br>
                  </div>
              </div>
                <span class="bottomText">Calendar</span>
            </div>
          </a>
          <a href="#" class="circleElement">
            <div class="col-md-2 center">
              <h4> <b>Opportunities</b></4>
              <div class="rcorners2">
                  <div class="alignMent">
                      <span class="	glyphicon glyphicon-hand-up giFont"></span><br>
                  </div>
              </div>
                <span class="bottomText">Opportunities</span>
            </div>
          </a>
          <a href="#" class="circleElement">
            <div class="col-md-2 center">
              <h4> <b>Media Blog</b></4>
              <div class="rcorners2">
                  <div class="alignMent">
                      <span class="	glyphicon glyphicon-folder-open giFont"></span><br>
                  </div>
              </div>
              <span class="bottomText">Media Blog</span>
            </div>
          </a>
          <a href="#" class="circleElement">
            <div class="col-md-2 center">
              <h4> <b>Skills </b></4>
              <div class="rcorners2">
                  <div class="alignMent">
                      <span class="	glyphicon glyphicon-certificate giFont"></span><br>
                  </div>
              </div>
              <span class="bottomText">skills</span>
            </div>
          </a>
        </div>
        <div class="col-md-4 registerBg">
          <div class="row">
            <h3><b> Register now</b> </h3>
            <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#home">Organization</a></li>
              <li><a data-toggle="tab" href="#menu1">Individual</a></li>

            </ul>

            <div class="tab-content">
              <div id="home" class="tab-pane fade in active">
                <p>Organization Registration</p>
              </div>
              <div id="menu1" class="tab-pane fade">
                <center>
                  <p>Individual Registration</p>
                </center>

<?php echo form_open_multipart('portal/individualRegis'); ?>
                <div class="col-md-12">
                  <div class="form-group">
                    <label><small>Enter Full Name</small></label>
                    <input type="text" class="form-control" name="fname" id="" placeholder="Full Name" required>
                  </div>
                </div>
                <div class=" inputField col-md-6">
                  <div class="form-group">
                      <label><small>Mobile Number</small></label>
                  <input type="text" class="form-control" name="number" id="" placeholder="Mobile Number" required>
                </div>
                </div>
                <div class="  col-md-6">
                  <div class="form-group">
                      <label><small>Email Address</small></label>
                  <input type="email" class="form-control" name="email" id="" placeholder="Email Address" required>
                </div>
                </div>
                <div class="  col-md-6">
                  <div class="form-group">
                      <label><small>Enter Password</small></label>
                  <input type="password" class="form-control" name="password" id="" placeholder="Password" required>
                </div>
                </div>
                <div class="  col-md-6">
                  <div class="form-group">
                      <label><small>Re Enter Password</small></label>
                  <input type="password" class="form-control" name="password_conf" id="" placeholder="Re Enter Password" required>
                </div>
                </div>

                <div class="  col-md-6">
                  <div class="form-group">
                      <label><small>Date Of Birth</small></label>
                  <input type="text" class="form-control date_picker" name="date" id="" placeholder="Date Of Birth" required>
                </div>
                </div>
                <div class="  col-md-6">
                  <div class="form-group">
                      <label><small>Upload Image</small></label>
                  <input type="file" class="form-control" name="main_image" id="" placeholder="Upload Photo" required>
                </div>
                </div>
                <div class="col-md-12">

                </div>
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary btn-sm pull-right">Register</button>
                </div>
                <?php echo form_close(); ?>
              </div>
            </div>
          </div>

        </div>
        <div class="col-md-12">

        </div>
      </section>
    </div>
    <!--end container-->
  </div>
  <!--end page-content-->

  <footer id="page-footer">
    <div class="footer-wrapper">
      <div class="block">
        <div class="container">
          <div class="vertical-aligned-elements">
            <div class="element width-50">
              <a href="http://www.unv.org/" target="_blank"><img src="<?php echo base_url(); ?>portalAssets/img/unv.png"></a>
            </div>
            <div class="element width-50 text-align-right">
              <a href="#" class="circle-icon"><i class="social_twitter"></i></a>
              <a href="#" class="circle-icon"><i class="social_facebook"></i></a>
              <a href="#" class="circle-icon"><i class="social_youtube"></i></a>
            </div>
          </div>

          <!--end background-wrapper-->
        </div>
      </div>
      <div class="footer-navigation">
        <div class="container">
          <div class="vertical-aligned-elements">
            <div class="element width-50">(C) 2017 Your Company, All right reserved</div>
            <div class="element width-50 text-align-right">
              <a href="index.html">Home</a>
              <a href="listing-grid-right-sidebar.html">Cafe Registration</a>
              <a href="submit.html">About Us</a>
              <a href="contact.html">Disclaimer</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!--end page-footer-->
</div>
<!--end page-wrapper-->
<a href="#" class="to-top scroll" data-show-after-scroll="600"><i class="arrow_up"></i></a>

<script type="text/javascript" src="<?php echo base_url(); ?>portalAssets/js/jquery-2.2.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>portalAssets/js/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>portalAssets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>portalAssets/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyBEDfNcQRmKQEyulDN8nGWjLYPm8s4YB58&libraries=places"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>portalAssets/js/richmarker-compiled.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>portalAssets/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>portalAssets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>portalAssets/js/custom.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>portalAssets/js/maps.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/portal-js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
  $(".rcorners2").mouseover(function(){
      $pb=$(this).parents("div.col-md-2:first");
      $element=$pb.find(".bottomText");
      $element.show();

  });
});
$(document).ready(function(){
  $(".rcorners2").mouseout(function(){
      $pb=$(this).parents("div.col-md-2:first");
      $element=$pb.find(".bottomText");
      $element.hide();

  });
});
$(document).ready(function () {
  $('.date_picker').datepicker({
    format: "yyyy-mm-dd"
  });
});
</script>

</body>
