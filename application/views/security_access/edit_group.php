<?php echo form_open("securityAccess/editGroup/$group_details->USERGRP_ID"); ?>
<?php 
$CI =& get_instance();
$CI->load->model('security_model')
	//$model = $this->load->model('security_model');
?>
<input type="hidden" name="txtOrgId" value="<?php echo $hid; ?>"/>
<p class="m-b-25 m-t-25 c-black f-500">Create new group for organization where user will be assigned later.</p>
<div class="form-group">
	<label>Enter Group Name Here</label>
	<input type="text" class="form-control" name="txtGroupName" value="<?php echo $group_details->USERGRP_NAME; ?>"
	required="required"/>
</div>
<table class="table table-striped table-bordered" id="sample_1">
<label>Front End Module</label>
<?php
			$module_list = $this->User->get_all_module_list();
			?>
			<?php foreach ($module_list as $module_lists) { ?>
<tr>
<td>
				<label><input type="checkbox" name="moduleId[]" 
				<?php 
						$permission=$CI->security_model->getGroupPermission($group_details->USERGRP_ID,$module_lists->MODULE_ID,'None');
						if($permission>0)
						{
							echo "checked";
						}

					?>
				value="<?php echo $module_lists->MODULE_ID; ?>"></label>
						<?php echo $module_lists->MODULE_NAME; ?>	</td>
						<td>
					<label><input name="chkpages[<?php echo $module_lists->MODULE_ID; ?>][]" 
					<?php 
						$permission=$CI->security_model->getGroupPermission($group_details->USERGRP_ID,$module_lists->MODULE_ID,'V');
						if($permission>0)
						{
							echo "checked";
						}

					?>
					 value="V" id="chkInsert" style="margin-right:5px" type="checkbox">View Only</label>
					<label><input name="chkpages[<?php echo $module_lists->MODULE_ID; ?>][]" 
					<?php 
						$permission=$CI->security_model->getGroupPermission($group_details->USERGRP_ID,$module_lists->MODULE_ID,'A');
						if($permission>0)
						{
							echo "checked";
						}

					?>
					value="A" id="chkInsert" style="margin-right:5px" type="checkbox">Approval/delete/reject</label>
					<label><input name="chkpages[<?php echo $module_lists->MODULE_ID; ?>][]" 
					<?php 
						$permission=$CI->security_model->getGroupPermission($group_details->USERGRP_ID,$module_lists->MODULE_ID,'E');
						if($permission>0)
						{
							echo "checked";
						}

					?>
					value="E" id="chkInsert" style="margin-right:5px" type="checkbox">Assign Person/Organization</label>

						<?php
						/*
				$chkCreate = array(
					'name' => 'chkpages['.$module_lists->MODULE_ID.'][]',
					'id' => 'chkInsert',
					'value' => 'V',
					'style' => 'margin-right:5px',
					'checked' =>$group_details->PERMISSION 'checked' : ''
					);
				echo form_checkbox($chkCreate) . "View only ";
				$chkView = array(
					'name' => 'chkpages['.$module_lists->MODULE_ID.'][]',
					'id' => 'chkView',
					'value' => 'A',
					'style' => 'margin-right:5px',
					'checked' => (('A',$group_details->PERMISSION)) ? 'checked' : ''
					);
				echo form_checkbox($chkView) . "Approval/delete/reject ";
				$chkUpdate = array(
					'name' => 'chkpages['.$module_lists->MODULE_ID.'][]',
					'id' => 'chkUpdate',
					'value' => 'E',
					'style' => 'margin-right:5px',
					'checked' => (('E', $group_details->PERMISSION)) ? 'checked' : ''
					);
				echo form_checkbox($chkUpdate) . "Assign Person/Organization ";

				*/?></span></p>
			
			</td>
			</tr>
			<?php } ?>
</table>


<!-- <div class="container">
	<label>Front End Module</label>
	<div class="row">
		<div class="col-sm-12">
			<?php
			$module_list = $this->User->get_all_module_list();
			?>
			<?php foreach ($module_list as $module_lists) { ?>
				<div class="col-md-12">
				<div class="col-md-4">
				<input type="checkbox" name="moduleId[]" value="<?php echo $module_lists->MODULE_ID; ?>">
						<?php echo $module_lists->MODULE_NAME; ?>	
				</div>
				<div class="col-md-8">	
			<p><span>
			<?php
				$chkCreate = array(
					'name' => 'chkpages['.$module_lists->MODULE_ID.'][]',
					'id' => 'chkInsert',
					'value' => 'I',
					'style' => 'margin-right:5px',
					);
				echo form_checkbox($chkCreate) . "View only ";
				$chkView = array(
					'name' => 'chkpages['.$module_lists->MODULE_ID.'][]',
					'id' => 'chkView',
					'value' => 'V',
					'style' => 'margin-right:5px',
					);
				echo form_checkbox($chkView) . "Approval/delete/reject ";
				$chkUpdate = array(
					'name' => 'chkpages['.$module_lists->MODULE_ID.'][]',
					'id' => 'chkUpdate',
					'value' => 'U',
					'style' => 'margin-right:5px',
					);
				echo form_checkbox($chkUpdate) . "Assign Person/Organization ";

				?></span></p>
				</div>
				</div>	
			<?php } ?>
			</div>

		</div>
	</div>  -->



	<button class="btn btn-primary" type="submit">Submit</button>
	<?php echo form_close(); ?>

