<?php if (!empty($applicant)): ?>
    <table class="table table-striped table-bordered table-hover gridTable">
        <thead>
        <tr>
            <th>SN</th>
            <th>Applicant ID</th>
            <th>Name</th>
            <th>Gender</th>
            <th>Mobile No</th>
            <th>Email</th>
            <th>Session</th>
            <th>Amount</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $sn = 1;
        foreach ($applicant as $row):
            ?>
            <tr class="gradeX" id="row_<?php echo $row->APPLICANT_ID; ?>">
                <td><span><?php echo $sn++; ?></span></td>
                <td><?php echo $row->APPLICANT_ID ?></td>
                <td>
                    <div class="feed-element">
                        <a class="pull-left" href="#">
                            <img class="img-circle" src="<?php echo base_url().'upload/applicant_photo/'.$row->STUD_PHOTO; ?>" alt="image">
                        </a>
                        <div class="media-body ">
                        <a href="#"><?php echo $row->FULL_NAME_EN?></a>
                        </div>
                    </div>
                </td>
                <td><?php echo $row->LKP_NAME; ?></td>
                <td><?php echo $row->MOBILE_NO; ?></td>
                <td><?php echo $row->EMAIL_ADRESS; ?></td>
                <td><?php echo $row->SESSION_NAME; ?></td>
                <td><?php echo $row->CR_AMT."/="; ?></td>
                <!-- <td><?php echo $row->PROGRAM_NAME; ?>
                <?php echo $row->DEPT_NAME; ?></td> -->
                <td>
                    <a class="label label-danger" href="<?php echo base_url('payment/applicant_payment/' . $row->APPLICANT_ID); ?>"
                       title="Click For Payment">payment</a>
                </td>
            </tr>
        <?php
        endforeach;
        ?>
        </tbody>
        <tfoot>
        <tr>
            <th>SN</th>
            <th>Applicant ID</th>
            <th>Name</th>
            <th>Gender</th>
            <th>Mobile No</th>
            <th>Email</th>
            <th>Session</th>
            <th>Amount</th>
            <th>Action</th>
        </tr>
        </tfoot>
    </table>

<?php else: ?>
    <h3 class="text-danger text-center ">No data found !!</h3>
<?php endif; ?>