 <style type="text/css">
     img.img-circled{
          max-width: 150px;
          height: auto;
          vertical-align: middle;
            }
    </style>

       <section>
                    
                    <div class="row">
                    <h3>Organizaion information</h3>
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label for="title">Name of Organization:</label>
                                <?php echo $organization_detail->title;  ?>
                            </div>
                             
                            <!--end form-group-->
                        </div>
                        <!--end col-md-9-->
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label for="category">The Scope of Your Organization:</label>
                               <?php echo $organization_detail->category;  ?>
                            </div>
                            <!--end form-group-->
                        </div>
                        <!--col-md-3-->
                    </div>
                    <!--end row-->
                    <div class="form-group">
                        <label for="description">Tell the world what your organisation is doing:</label>
                        <?php echo $organization_detail->description;  ?>
                    </div>
                    
                
                </section>

                         <section>
                    <h3>Organizaion Contact Information</h3>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="form-group">
                                <label for="address-autocomplete">Full Address: </label>
                                
                             <?php echo $organization_detail->location;  ?>
                            </div>
                            <!--end form-group-->
                            <div class="map height-200px shadow" id="map-modal"></div>
                            <!--end map-->
                            <div class="form-group hidden">
                                <input type="text" class="form-control" id="latitude" name="latitude" hidden="" value="'.$latitude.'">
                                <input type="text" class="form-control" id="longitude" name="longitude" hidden="" value="'.$longitude.'">
                            </div>
                            <p class="note">Enter the exact address or drag the map marker to position</p>
                        </div>
                        <!--end col-md-6-->
                        <div class="col-md-12 col-sm-12">
                            <div class="form-group col-md-4 col-sm-4">
                                <label for="region">District: </label>
                                 <?php echo $organization_detail->district;  ?>
                               </div>
                            <!--end form-group-->
                            <div class="form-group col-md-4 col-sm-4">
                                <label for="phone">Postal Code: </label>
                               
                                 <?php echo $organization_detail->postalcode;  ?>
                            </div>
                            <!--end form-group-->
                             <div class="form-group col-md-4 col-sm-4">
                                <label for="email">Country:</label>
                                <?php echo $organization_detail->country;  ?>
                            </div>
                            </div>
                            <div class="col-md-12 col-sm-12">
                            <div class="form-group col-md-4 col-sm-4">
                                <label for="email">Org Telephone Number:</label>
                                
                            <?php echo $organization_detail->phone;  ?>
                            </div>
                            <!--end form-group-->
                            <div class="form-group col-md-4 col-sm-4">
                                <label for="website">Org Mobile Number: </label>
                               <?php echo $organization_detail->mobile;  ?>
                                
                            </div>
                            <!--end form-group-->
                            <!--end form-group-->
                            <div class="form-group col-md-4 col-sm-4">
                                <label for="website">Fax</label>
                                <?php echo $organization_detail->fax;  ?>
                            </div>
                            <!--end form-group-->
                             <!--end form-group-->
                            <div class="form-group col-md-6 col-sm-6">
                                <label for="website">Org E-mail</label>
                                 <?php echo $organization_detail->oemail;  ?>
                                
                            </div>
                            
                            <!--end form-group-->
                            <!--end form-group-->
                            <div class="form-group col-md-6 col-sm-6">
                                <label for="website">Website</label>
                                
                                <?php echo $organization_detail->website;  ?>
                            </div>
                            <!--end form-group-->
                              <section class="col-md-12 col-sm-12">
                            
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label for="facebook">Facebook URL </label>
                                            <?php echo $organization_detail->facebook;  ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <!--end form-group-->
                                        <div class="form-group">
                                            <label for="twitter">Twitter URL</label>
                                            <?php echo $organization_detail->twitter;  ?>
                                        </div>
                                    </div>

                                    <!--end col-md-6-->
                                </div>
                                <!--end row-->
                            </section>
                                <!--end form-group-->
                                <div class="form-group col-md-4 col-sm-4">
                                    <label for="hoo">Head of Organization </label>
                                    
                                   <?php echo $organization_detail->hoo;  ?>
                                </div>
                                <!--end form-group-->
                                 <!--end form-group-->
                                <div class="form-group col-md-4 col-sm-4">
                                    <label for="hoodestination">Designation </label>
                                   <?php echo $organization_detail->hoodesignation;  ?>
                                </div>
                                <!--end form-group-->
                                <!--end form-group-->
                                <div class="form-group col-md-4 col-sm-4">
                                    <label for="hooemail">Email Address</label>
                                    <?php echo $organization_detail->hooemail;  ?>
                                </div>
                                <!--end form-group-->
                                <!--end form-group-->
                                <div class="form-group col-md-3 col-sm-3">
                                    <label for="contactperson">Name of the contact person with in your organization </label>
                                    
                              <?php echo $organization_detail->contactperson;  ?>
                               </div>
                                <!--end form-group-->
                                 <!--end form-group-->
                                <div class="form-group col-md-3 col-sm-3">
                                    <label for="cpdestination">Destination </label>
                                    <?php echo $organization_detail->cpdesignation;  ?>
                                </div>
                                <!--end form-group-->
                                <!--end form-group-->
                                <div class="form-group col-md-3 col-sm-3">
                                    <label for="cpperson">Phone </label>
                                   <?php echo $organization_detail->cpperson;  ?>
                                </div>
                                <!--end form-group-->
                                <!--end form-group-->
                                <div class="form-group col-md-3 col-sm-3">
                                    <label for="cpemail">Email Address  </label>
                                  <?php echo $organization_detail->cpemail;  ?>
                                </div>
                                <!--end form-group-->
                                <!--end form-group-->
                            <div class="row col-md-12 col-sm-12">
                                <div class="form-group col-md-4 col-sm-4">
                                    <label for="edo">Establishment Date of Organization </label>
                                    <?php echo $organization_detail->edo;  ?>
                                </div>
                                <!--end form-group-->
                                <!--end form-group-->
                                <div class="form-group col-md-8 col-sm-8">
                                    <label for="wab">Working Area in Bangladesh </label>
                                    <br><br>
                                     <?php echo $organization_detail->wab;  ?>
                                  
                                </div>

                                <div class="form-group">
                                    <div class="form-group col-md-12 col-sm-12">
                                        <h4>Number of district/s </h4>
                                         <?php echo $organization_detail->nod;  ?>
                                    </div>
                                    <div class="form-group col-md-12 col-sm-12">
                                        <h5>Division area</h5>
                                       
                                        <!-- <div id="rediv" class="division_item">
                                         <?php $array = explode(',', $data["divarea"]);
                                       $i = 1;
                                      foreach($array as $val){
                                          echo $val.', ';
                                            echo "<br />";
                                          $i++;
                                      }
                                      ?> -->
                                        
                                        </div>
                                    
                                    </div>
                                 </div>
                                </div>

                            </div>
                            <!--end form-group-->
                            
                        </div>
                        <!--end col-md-12-->
                    
                </section>


                 <section>
                <h3>Number of Employees</h3>
                <div class="form-group">
                
                <div class="col-md-12 col-sm-12">
                    <div class="col-md-4 col-sm-4">
                    <label for="">Full Time </label>
                     <?php echo $organization_detail->fte;  ?>
                    
                   
                    </div>
                    <div class="col-md-4 col-sm-4">
                    <label for="">Part-Time </label>
                      <?php echo $organization_detail->pte;  ?>
                    </div>
                    <div class="col-md-4 col-sm-4">
                    <label for="">Volunteers </label>
                   
                  <?php echo $organization_detail->volunteers;  ?>
                    </div>
                </div>    
                
                </div>
                <h3>Interest Area of the organization </h3>
            <!--     <div class="form-group">
                <?php $iao=explode(",",$result["iao"]); ?>
                <div class="col-md-12 col-sm-12">
                <?php echo $result["iao"]; ?>

                </div> -->
                 <?php echo $organization_detail->iao;  ?>
                
                </div>
                 <h3>Please specify your registration / affiliation with others</h3>
                <div class="form-group">
                    <div class="col-md-12 col-sm-12">
                        <div class="col-md-6 col-sm-6">
                            <h6><strong>Registration Authority </strong></h6>
                             
                           <!--  <?php echo $result["rauthority"]; ?> -->
                           <?php echo $organization_detail->rauthority;  ?>
                            
                        </div>
                        


                        <div class="col-md-12 col-sm-12" id="regau">
                            <h3>Select from List of Govt. body</h3>
                            <div class="form-group">
                            
                                <div class="col-md-6   col-sm-12">
                                  <!--   <?php echo $result["vogt_name"].', '.$aff_with_other_1; ?> -->
                                     <?php echo $organization_detail->vogt_name;  ?>
                                </div>
                                

                                <div class="col-md-12">
                                    <div class="col-md-6  col-sm-6">
                                        <h4>GOVT. Registration number </h4>
                                        <?php echo $organization_detail->registration_no;  ?>
                                        
                                    </div>
                                    <div class="col-md-6  col-sm-6">
                                        <h4>GOVT. Registration date </h4>
                                        
                                      <?php echo $organization_detail->govt_reg_date;  ?>
                                        
                                    </div>
                                </div>


                            </div>
                        </div>



                        <div class="col-md-12" id="reguc">
                            <h4>Please specify the name of university / college</h4>
                             <?php echo $organization_detail->college_name;  ?>
                        </div>

                       

                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-md-7">
                                    <h4>Foreign Authority Registration number</h4>
                                     <?php echo $organization_detail->reg_college;  ?>
                                </div>
                                <div class="col-md-5">
                                    <h4>Foreign Authority Registration Date</h4>
                                     <?php echo $organization_detail->non_gov_reg;  ?>
                                </div>
                                <div class="col-md-12">
                                    <h4>Please specify the name of foreign registration authority</h4>
                                      <?php echo $organization_detail->foreign_reg;  ?>
                                   
                                </div>
                            </div>
                        </div>

                    </div>
               
                
                </div>
                <div class="form-group">
                    <div class="col-md-12 col-sm-12">
                        <label>How did you learn about Youthopia.bangla ? </label>
                         <?php echo $organization_detail->learnabout;  ?>
                       
                    </div>
                </div>
                
                </section>
                <!--number of employee section end-->
              
                <section>
                    <h3>Gallery</h3><p>Ratio : width: 350px X height: 200px; </p>
                    <div class="file-upload-previews"><?php echo $organization_detail->galleryfiles;  ?> </div>
                    
                    <!--end form-group-->
                </section>
                 <section>
                    <h3>logo</h3><p>Ratio : width: 100px X height:  100px</p>
                    <div class="file-upload-previews"><?php echo $organization_detail->logo;  ?></div>
                    
                    <!--end form-group-->
                </section>