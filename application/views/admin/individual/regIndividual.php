
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Registered Individual List</h5>

                <div class="ibox-tools">
                  <!--   <span class="btn btn-primary btn-xs pull-right  " data-toggle="modal" href="#modalDefault">Create
                        New </span> -->

                </div>

            </div>
                  <?php echo $this->session->flashdata('msg'); ?>

            <div class="ibox-content">
                <div class="table-responsive contentArea">
                    <div class="row">

                    </div>
                    <br>

                    <?php
                    if (!empty($regIndividual)) {
                        ?>
                         <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Full Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Date Of Birth</th>
                                <th>Gender</th>
                                <th>Type</th>
                                <th>Login With</th>
                                <th>Status</th>
                                <th>Reg. Date</th>
                                <th>Last Login</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i = 1;
                            foreach ($regIndividual as $regIndividuals) {
                                ?>
                                <tr id="userId_row_<?php echo $regIndividuals->userId; ?>">
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $regIndividuals->fname; ?></td>
                                    <td><?php echo $regIndividuals->email; ?></td>
                                    <td><?php echo $regIndividuals->number; ?></td>
                                    <td><?php echo $regIndividuals->date; ?></td>
                                    <td><?php echo $regIndividuals->gender; ?></td>
                                    <td><?php echo $regIndividuals->type; ?></td>
                                    <td class="center">
                                    <?php

                                    if (!empty($regIndividuals->facebookId)) {
                                        echo  '<span class="btn btn-success btn-xs">Facebook</span>';
                                    } elseif (!empty($regIndividuals->gmail_id)) {
                                        echo  '<span class="btn btn-success btn-xs">Gmail</span>';
                                    } else {
                                        echo  '<span class="btn btn-success btn-xs">Website</span>';
                                    }
                                    ?>
                                    </td>
                                    <td class="center"><?php echo ($regIndividuals->active == 1) ? 'Active' : 'Inactive'; ?></td>
                                    <td><?php echo $regIndividuals->created_at; ?></td>
                                    <td><?php echo $regIndividuals->login_time; ?></td>
                                 
                                      

                                    <td>
                                      <?php if($rejectPerm>0){ ?>
                                        <span class="btn btn-danger btn-xs mailSendBan"
                                                  USER_ID="<?php echo $regIndividuals->userId; ?>"
                                                  FULL_NAME="<?php echo $regIndividuals->fname; ?>"
                                                   USER_MAIL="<?php echo $regIndividuals->email; ?>"
                                               title="Click For Banned"><i
                                                class="fa fa-ban"></i></span>
                                           <?php 
                                        }
                                        ?>

                                                <?php if($viewPerm>0){ ?>
                                          <a class="btn btn-danger btn-xs"
                                              href="<?php echo site_url("admin/regIndividual_detail/" . $regIndividuals->userId); ?>" title="Details"><i
                                                class="fa fa-eye"></i></a>
                                                   <?php 
                                        }
                                        ?>

                                                 <span class="btn btn-danger btn-xs mailSendBtn"
                                                  USER_ID="<?php echo $regIndividuals->userId; ?>"
                                                  FULL_NAME="<?php echo $regIndividuals->fname; ?>"
                                                   USER_MAIL="<?php echo $regIndividuals->email; ?>"
                                               title="Click For Send Mail"><i
                                                class="fa fa-envelope"></i></span>
                                                 <?php if($rejectPerm>0){ ?>
                                                 <span class="btn btn-danger btn-xs indvDltBtn"
                                              userId="<?php echo $regIndividuals->userId; ?>" title="Click For Delete"><i
                                                class="fa fa-remove"></i></span>
                                                   <?php 
                                        }
                                        ?>
                                                                                             

                                    </td>
                                </tr>
                            <?php
                              $i++;
                            }

                            ?>
                            </tbody>
                        </table>
                    <?php
                    } else {
                        echo "<p class='text-danh text-danger'><br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No Data Found</p>";
                    }
                    ?>
                </div>
            </div>

        </div>


        <script>
    $(document).ready(function() {
        $('#example1').DataTable( {
            "scrollX": true,
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'pdf',
                    orientation:'landscape',
                    exportOptions: {
                        columns: [0,1,2,3,4,5,6,7,8,9,10]
                    }
                },

                {
                    extend: 'excel',
                    exportOptions: {
                        columns: [0,1,2,3,4,5,6,7,8,9,10]
                    }
                }

            ]
        } );
    } );
</script>




<script src="<?php echo base_url(); ?>assets/security_access/bootgrid/jquery.bootgrid.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#data-table-basic").bootgrid({
            css: {
                icon: 'md icon',
                iconColumns: 'md-view-module',
                iconDown: 'md-expand-more',
                iconRefresh: 'md-refresh',
                iconUp: 'md-expand-less'
            }
        });
        $(document).on("click", "#createModule", function () {
            var mod_name = $("#txtModuleName").val();
            var mod_name_bn = $("#txtModuleNameBn").val();
            var sl_no = $("#SL_NO").val();
            var status = $("#ACTIVE_STATUS").prop('checked');

            $.ajax({
                type: "POST",
                url: "<?php echo site_url('securityAccess/createModule'); ?>",
                data: {mod_name: mod_name, mod_name_bn: mod_name_bn, sl_no: sl_no, status: status},
                success: function (result) {
                    $(".modal_msg").html(result);
                    window.location.reload(true);
                }
            });
        });
        $(document).on("click", ".statusType", function () {
            var status = $(this).attr('status');
            var linkId = $(this).attr('data-linkId');
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('admin/setup/changeModuleStatus'); ?>",
                data: {linkid: linkId, status: status},
                success: function (result) {
                    window.location.reload(true);
                }
            });
        });

       


        $(".modDltBtn").click(function () {
            if (confirm("Are you want to delete?")) {
                var mod_id = $(this).attr('mod_id');
                $.ajax({
                    url: '<?php echo site_url('securityAccess/delete_module_from_db'); ?>',
                    type: 'POST',
                    data: {mod_id: mod_id},
                    success: function (data) {
                        if (data == 1) {
                            alert('Module deleted Successfully');
                            $('#mod_row_' + mod_id).remove();
                        }
                    }
                });
            } else {
                return false;
            }
        });
    });

   $(".indvDltBtn").click(function () {
            if (confirm("Are you want to delete?")) {
                var userId = $(this).attr('userId');
                $.ajax({
                    url: '<?php echo site_url('admin/delete_individual_from_db'); ?>',
                    type: 'POST',
                    data: {userId: userId},
                    success: function (data) {
                        if (data == 1) {
                            alert('Individual deleted Successfully');
                            $('#userId_row_' + userId).remove();
                        }
                    }
                });
            } else {
                return false;
            }
        });

    $(".mailSendBtn").click(function () {
            if (confirm("Are you want to send mail?")) {
                var USER_ID = $(this).attr('USER_ID');
                var USER_MAIL =$(this).attr('USER_MAIL');
                var FULL_NAME = $(this).attr('FULL_NAME');
                $.ajax({
                    url: '<?php echo site_url('admin/again_send_mail_regIndividual'); ?>',
                    type: 'POST',
                    data: {USERID: USER_ID,FULL_NAME: FULL_NAME,USER_MAIL: USER_MAIL},
                    success: function (data) {
                        if (data == 1) {
                            alert('Mail Send Successfully');
                            
                        }
                    }
                });
            } else {
                return false;
            }
        });
      $(".mailSendBan").click(function () {
            if (confirm("Are you want to Banned Registered Individual?")) {
                var USER_ID = $(this).attr('USER_ID');
                var USER_MAIL =$(this).attr('USER_MAIL');
                //alert('USER_MAIL');
                var FULL_NAME = $(this).attr('FULL_NAME');
                $.ajax({
                    url: '<?php echo site_url('admin/again_send_mail_ban_individual'); ?>',
                    type: 'POST',
                    data: {USERID: USER_ID,FULL_NAME: FULL_NAME,USER_MAIL: USER_MAIL},
                    success: function (data) {
                        if (data == 1) {
                            alert('Mail Send Successfully');
                            
                        }
                    }
                });
            } else {
                return false;
            }
        });





</script>