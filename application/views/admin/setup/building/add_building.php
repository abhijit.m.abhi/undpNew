<div class="block-flat">
    <form class="form-horizontal frmContent" id="" action="" method="post">
        <?php
        if ($ac_type == 2) {
            ?>
            <input type="hidden" name="BR_TYPE_ID" class="rowID" value="<?php echo $building->BR_TYPE_ID ?>"/>
        <?php
        }
        ?>
        <span class="frmMsg"></span>

        <div class="form-group">
            <label class="col-lg-3 control-label">Building Name<span class="text-danger">*</span></label>

            <div class="col-lg-5">
                <input type="text" id="building_name" name="building_name"
                       value="<?php echo ($ac_type == 2) ? $building->BR_TYPE_NAME : '' ?>"
                       class="form-control required">
                <span class="validation"></span>
                <span class="help-block m-b-none ">e.g.  Academic Building.</span>
            </div>
        </div>
        <div class="hr-line-dashed"></div>
        <div class="form-group">
            <label class="col-lg-3 control-label">Building Short Name<span class="text-danger">*</span></label>

            <div class="col-lg-5">
                <input type="text" id="building_name" name="building_name_s"
                       value="<?php echo ($ac_type == 2) ? $building->BR_SHORT_NAME : '' ?>"
                       class="form-control required">
                <span class="validation"></span>
                <span class="help-block m-b-none ">e.g. AC.</span>
            </div>
        </div>
        <div class="hr-line-dashed"></div>
        <div class="form-group">
            <label class="col-lg-3 control-label">Active?</label>

            <div class="col-lg-5">
                <?php
                $ACTIVE_STATUS = ($ac_type == 2) ? $building->ACTIVE_STATUS : '';
                $checked = ($ac_type == 2) ? (($building->ACTIVE_STATUS == '1') ? TRUE : FALSE) : '';
                ?>
                <label class="control-label">
                    <?php
                    $data = array(
                        'name' => 'status',
                        'id' => 'status',
                        'class' => 'checkBoxStatus',
                        'value' => $ACTIVE_STATUS,
                        'checked' => $checked
                    );
                    echo form_checkbox($data);
                    ?>
                </label>

            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-offset-3 col-lg-10">
                <span class="modal_msg pull-left"></span>
                <?php if ($ac_type == 2) { ?>
                    <input type="button" class="btn btn-primary btn-sm formSubmit" data-action="setup/updateBuilding"
                           data-su-action="setup/buildingById" value="Update">
                <?php } else { ?>
                    <input type="button" class="btn btn-primary btn-sm formSubmit" data-action="setup/createBuilding"
                           data-su-action="setup/buildingList" data-type="list" value="submit">
                <?php
                }
                ?>
                <input type="reset" class="btn btn-default btn-sm" value="Reset">
                <span class="loadingImg"></span>
            </div>
        </div>
    </form>
</div>
<div class="hr-line-dashed"></div>
</form>
</div>
<script>
    $(document).on('click', '.checkBoxStatus', function () {
        var status = ($(this).is(':checked') ) ? 1 : 0;
        $("#status").val(status);
    });
</script>