 <style type="text/css">
     img.img-circled{
          max-width: 150px;
          height: auto;
          vertical-align: middle;
            }
    </style>

    <style>
  
    .slider_img{ height: 200px; width: 200px; }
   </style> 

    
    <form class="form-horizontal frmContent" id="event" method="post">
    <div class="block-flat">
        <span class="frmMsg"></span><br>
        <h3 class="box-title">Event Details</h3>
    <table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
      
         
        <tr>
            <th>Name</th>
            <td><?php echo $eventDetails->Title?></td>
        </tr>
        <tr>
            <th>Email</th>
            <td><?php echo $eventDetails->oemail?></td>
        </tr>
        <tr>
            <th>Event Name</th>
            <td><?php echo $eventDetails->event_name?></td>
        </tr>

          <tr>
            <th>Event Image</th>
            <td> 
            <?php if($eventDetails->images != ""){
                      $images = explode('___', $eventDetails->images); 
                      ?>
                              <?php foreach ($images as $image): ?>
                          <div class="col-sm-3">
                           <img  class="img-responsive" src="<?php echo base_url(); ?>upload/events/<?php echo $image; ?>"/>
                           </div>
                            <?php endforeach; ?>
                     
                      <?php } else{?>
                       <img style="max-height: 200px; width: auto; padding-bottom: 4px;" src="<?php echo base_url(); ?>assets/img/avatar.png" alt="User Photo"/>
               
                     
                      <?php } ?> <br>
                      
            </td>
        </tr>
        <tr>
            <th>Category</th>
            <td><?php echo $eventDetails->category?></td>
        </tr>
        <tr>
            <th>Start Date</th>
            <td><?php echo $eventDetails->start_date?></td>
        </tr>
        <tr>
            <th>End Date</th>
            <td><?php echo $eventDetails->end_date?></td>
        </tr>
        <tr>
            <th>Start Time</th>
            <td><?php echo $eventDetails->start_time?></td>
        </tr>

        <tr>
            <th>End Time</th>
            <td><?php echo $eventDetails->end_time?></td>
        </tr>
          <tr>
            <th>Address</th>
            <td><?php echo $eventDetails->address?></td>
        </tr>
        <tr>
            <th>District</th>
            <td><?php echo $eventDetails->district?></td>
        </tr>
        <tr>
            <th>Latitude</th>
            <td><?php echo $eventDetails->latitude?></td>
        </tr>
        <tr>
            <th>Longitude</th>
            <td><?php echo $eventDetails->longitude?></td>
        </tr>
          <tr>
            <th>Seat Limit</th>
            <td><?php echo $eventDetails->seat_limit?></td>
        </tr>
        <tr>
            <th>Event Fee</th>
            <td><?php echo $eventDetails->event_fee?></td>
        </tr>

        <tr>
            <th>Registration Link</th>
            <td><?php echo $eventDetails->registration_link?></td>
        </tr>

          <tr>
            <th>Total View</th>
             <td><?php echo $eventDetails->view_count?></td>
           
         </tr>
          <tr>
            <th>EMO</th>
             <td><?php echo $eventDetails->TOTAL_EMO?></td>
           
         </tr>

          <tr>
            <th>Interest</th>
             <td><?php echo $eventDetails->TOTAL_INT?></td>
           
         </tr>
           <tr>
            <th>Going</th>
             <td><?php echo $eventDetails->TOTAL_GOING?></td>
           
         </tr>
           
    </table>

    </div>        
    <div class="hr-line-dashed"></div>

</form>