
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h4>Create User</h4>
            </div>
            <div class="ibox-content">
                <br>

                <div class="row">
                    <?php echo form_open_multipart('admin/adduserManager', "class='form-horizontal margin-none'"); ?>
                    <div class="msg">
                        <?php
                        if (validation_errors() != false) {
                            ?>
                            <div class="alert alert-danger">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo validation_errors(); ?>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
               <!--      <div class="form-group">
                        <label class="col-sm-2 control-label" for="id">User</label>

                    
                        <br clear="all"/>
                    </div> -->
                    <div class="form-group">
                       
                        <label class="col-sm-2 control-label">User Name</label>

                        <div class="col-sm-4">
                           
                                <input type="text" name="admin_username" class="form-control" id="admin_username"
                                       value="<?php echo set_value('admin_username'); ?>" placeholder="Enter User Name"/>
                            
                        </div>
                        <br clear="all"/>
                    </div>
                    <div class="form-group">
                        
                         <label class="col-sm-2 control-label">Email</label>

                        <div class="col-sm-4">
                            
                                <input type="email" name="admin_email" id="admin_email" class="form-control"
                                       value="<?php echo set_value('admin_email'); ?>" placeholder="Enter Email Address"/>
                            
                        </div>
                        <br clear="all"/>
                    </div>
          

                    <div class="form-group">
                    
                    <label class="col-sm-2 control-label">Select Your User Type:</label>
                        <div class="col-sm-4">
                         <select name="admin_type" id="admin_type" class="form-control" required>
                         <option value="">Select Type</option>
                         <option value="1">Admin</option>
                         <option value="0">User</option>
                         </select>
                         
                         </div>
                    </div>

                        <div class="form-group">
                   
                    <label class="col-sm-2 control-label">Select Status:</label>
                         <div class="col-sm-4">
                         <select name="status" id="status" class="form-control" required>
                         <option value="">Select Status</option>
                         <option value="1">Active</option>
                         <option value="0">Inactive</option>
                         </select>
                         
                         </div>
                    </div>

                        <div class="form-group">
              
                 <label class="col-sm-2 control-label">Password:</label>
                    <div class="col-sm-4">
                    <input type="password" name="admin_pass" class="form-control" id="admin_pass" value="">
                    </div>
                </div>
                  
                    <button class="col-sm-offset-2 btn btn-primary btn-sm " type="submit">Submit</button>
                    <?php echo form_close(); ?>
                </div>

            </div>
        </div>