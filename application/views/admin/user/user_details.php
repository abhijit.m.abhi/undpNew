        <style type="text/css">
         img.img-circled{
              max-width: 150px;
              height: auto;
              vertical-align: middle;
                }
        </style>

        <h3 class="box-title">User Details</h3>
        <table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
            <tr>
                <th width="20%">ID</th>
                <td><?php echo $user_detail->userId?></td>
            </tr>
            <tr>
                <th width="20%">Full Name</th>
                <td><?php echo $user_detail->fname?></td>
            </tr>
            <tr>
                <th>Email</th>
                <td><?php  echo $user_detail->email?></td>
            </tr>
            <tr>
                <th>Facebook ID</th>
                <td><?php  echo $user_detail->facebookId?></td>
            </tr>

              <tr>
                <th>Gmail ID</th>
                <td><?php  echo $user_detail->gmail_id?></td>
            </tr>
            <tr>
                <th>Date of Birth</th>
                <td><?php echo $user_detail->date?></td>
            </tr>
            <tr>
                <th>Contact Number</th>
                <td><?php echo $user_detail->number?></td>
            </tr>
            <tr>
                <th>Gender</th>
                <td><?php echo $user_detail->gender?></td>
            </tr>
            <tr>
                <th>Photo</th>
                <td> 


                 <?php if ($user_detail->login_method =='D') : ?>
                        <?php
                            $user_img = "".$user_detail->image;
                            ?>
                            <pull-left>
                            <img class="img-circled"  src="<?php echo base_url($user_img) ?>" alt="" />
                          </pull-left>
                     

                  <?php elseif($user_detail->login_method=='F') : ?>
                    <?php 
                    $user_img_fb =$user_detail->image;
                          ?>
                    <pull-left>
                        <img class="img-circled" src="<?php echo $user_img_fb; ?>" alt=""/>
                         </pull-left>

                <?php elseif($user_detail->login_method=='G') : ?>
                    <?php 
                     $user_img_gmail =$user_detail->image;
                     ?>
                  <pull-left>
                  <img class="img-circled" src="<?php echo $user_img_gmail; ?>" alt=""/>
                 </pull-left>
                    <?php else : ?>
                  <pull-left>
                  <img class="img-circled" src="<?php echo base_url(); ?>assets/img/avatar.png" alt=""/>
                  </pull-left>




                 <?php endif; ?>

               </td>
            </tr>
            <tr>
                <th>Type</th>
                <td><?php echo $user_detail->type?></td>
            </tr>
            <tr>
                <th>Organization name</th>
                <td><?php echo $user_detail->oname?></td>
            </tr>
            <tr>
                <th>Organization Email</th>
                <td><?php echo $user_detail->oemail?></td>
            </tr>
            <tr>
                <th>Organization Facebook URL</th>
                <td><?php echo $user_detail->facebook?></td>
            </tr>
            <tr>
                <th>Organization Website URL</th>
                <td><?php echo $user_detail->website?></td>
            </tr>
            <tr>
                <th>Has Organization</th>
                <td><?php echo ($user_detail->type =="organization") ? '<span class="btn btn-success btn-xs">Yes</span>' : '<span class="btn btn-danger btn-xs">No</span>'; ?></td>
            </tr>

              <tr>
                <th>Status</th>
                <td><?php echo ($user_detail->active == 1) ? '<span class="btn btn-success btn-xs">Active</span>' : '<span class="btn btn-danger btn-xs">Inactive</span>'; ?></td>
            </tr>
                <tr>
                <th>Registration Date & Time</th>
                <td><?php echo $user_detail->created_at?></td>
            </tr>
             <tr>
                <th>Login With </th>
                <td><?php echo ($user_detail->facebookId!= '') ? '<span class="btn btn-success btn-xs">Facebook</span>' : '<span class="btn btn-danger btn-xs">Website</span>'; ?></td>
            </tr>
        </table>

    <script src="<?php echo base_url(); ?>assets/security_access/bootgrid/jquery.bootgrid.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#data-table-basic").bootgrid({
                css: {
                    icon: 'md icon',
                    iconColumns: 'md-view-module',
                    iconDown: 'md-expand-more',
                    iconRefresh: 'md-refresh',
                    iconUp: 'md-expand-less'
                }
            });
            
        });
    </script>