
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>User List</h5>

                <div class="ibox-tools">
                 

                </div>

            </div>

            <div class="ibox-content">
                <div class="table-responsive contentArea">
                    <div class="row">

                    </div>
                    <br>

                    <?php
                    if (!empty($user_list)) {
                        ?>
                     <table id="example1" class="table table-bordered table-striped" width="100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Register Date-Time</th>
                                <th>Status</th>
                                <th>Subscriber Type</th>
                                <th>Login With</th>
                                <th>Last Login </th>
                                <th>Permission</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i = 1;
                            foreach ($user_list as $user_lists) {
                                ?>
                                <tr id="user_row_<?php echo $user_lists->userId; ?>">
                                    <td><?php echo $i; ?></td>
                                    <td>
                                    <?php

                                    if ($user_lists->type=='individual') {
                                       echo $user_lists->fname;
                                    } elseif ($user_lists->type=='organization') {
                                        echo $user_lists->oname;
                                    } else {
                                        echo  '';
                                    }
                                    ?></td>
                                    <td><?php echo $user_lists->email; ?></td>
                                    <td><?php echo $user_lists->created_at; ?></td>
                                    <td class="center"><?php echo ($user_lists->active == 1) ? '<span class="btn btn-success btn-xs">Active</span>' : '<span class="btn btn-danger btn-xs">Inactive</span>'; ?></td>
                                    <td class="center"><span class="btn btn-success btn-xs"><?php echo $user_lists->type; ?></span></td>
                                      <td class="center">  <?php

                                    if (!empty($user_lists->facebookId)) {
                                        echo  '<span class="btn btn-success btn-xs">Facebook</span>';
                                    } elseif (!empty($user_lists->gmail_id)) {
                                        echo  '<span class="btn btn-success btn-xs">Gmail</span>';
                                    } else {
                                        echo  '<span class="btn btn-success btn-xs">Website</span>';
                                    }
                                    ?></td>
                                      <td><?php echo $user_lists->login_time; ?></td>
                                      <td align="center">   <?php if($rejectPerm>0){ ?>
                                        <span class="btn btn-warning btn-xs openModal " data-action="admin/userModule/<?php echo $user_lists->userId; ?>" link_id="<?php echo $user_lists->userId; ?>" title="Assign Module"><i
                                            class="fa fa-check-square-o"></i></span>
                                   
                                        <?php 
                                        }
                                        ?></td>
                                      

                                    <td>
                               


                                        <!-- <span class="btn btn-warning btn-xs openModal"
                                              data-action="#" title="Ban"><i
                                                class="fa fa-ban"></i></span> -->
                                        <?php if($viewPerm>0){ ?>
                                        <a class="btn btn-danger btn-xs"
                                            href="<?php echo site_url("admin/user_detail/" . $user_lists->userId); ?>" title="Details"><i
                                            class="fa fa-eye"></i></a>
                                        <?php 
                                        }
                                        ?>

                                         <?php if($rejectPerm>0){ ?>
                                                <span class="btn btn-danger btn-xs userDltBtn"
                                              userId="<?php echo $user_lists->userId; ?>" title="Click For Delete"><i
                                                class="fa fa-remove"></i></span>
                                          <?php 
                                        }
                                        ?>
                                    </td>
                                </tr>
                            <?php
                              $i++;
                            }

                            ?>
                            </tbody>
                        </table>
                    <?php
                    } else {
                        echo "<p class='text-danh text-danger'><br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No Data Found</p>";
                    }
                    ?>
                </div>
            </div>

        </div>
                <script>
    $(document).ready(function() {
        $('#example1').DataTable( {
            "scrollX": true,
            dom: 'Bfrtip',
            buttons: [
                {
                    //extend: 'pdf',
                    extend: 'pdfHtml5',
                    orientation:'landscape',
                    pageSize: 'LEGAL',
                    exportOptions: {
                        columns: [0,1,2,3,4,5,6,7]
                    }
                },

                {
                    extend: 'excel',
                    exportOptions: {
                        columns: [0,1,2,3,4,5,6,7]
                    }
                }

            ]
        } );
    } );
</script>





<!-- <div class="modal fade" id="modalDefault" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span
                        aria-hidden="true">×</span></button>
                <h4 id="gridModalLabel" class="modal-title">Create Module</h4>
            </div>
            <div class="modal-body">

                <div class="form-group">
                    <label class="col-sm-3 control-label" for="txtModuleName">Module Name</label>

                    <div class="col-sm-8">
                        <div class="fg-line">
                            <input type="text" name="txtModuleName" class="form-control" id="txtModuleName" value=""
                                   placeholder="Enter Module Name" />
                        </div>
                    </div>
                    <br clear="all" />
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="txtModuleNameBn">Module Name Bangla</label>

                    <div class="col-sm-8">
                        <div class="fg-line">
                            <input type="text" name="txtModuleNameBn" id="txtModuleNameBn" class="form-control" value=""
                                   placeholder="Enter Module Bangla Name" />
                        </div>
                    </div>
                    <br clear="all" />
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="SL_NO">Serial No</label>

                    <div class="col-sm-8">
                        <div class="fg-line">
                            <input type="text" name="SL_NO" id="SL_NO" class="form-control" value=""
                                   placeholder="Enter Module Serial" />
                        </div>
                    </div>
                    <br clear="all" />
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="SL_NO">Module Icon</label>

                    <div class="col-sm-8">
                        <div class="fg-line">
                            <input type="text" name="MODULE_ICON" id="MODULE_ICON" class="form-control" value=""
                                   placeholder="Enter Module Icon" />
                        </div>
                    </div>
                    <br clear="all" />
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="txtModLink">Action </label>

                    <div class="col-sm-8">
                        <div class="fg-line">
                            <?php
                            $chkStatus = array(
                                'name' => 'ACTIVE_STATUS',
                                'id' => 'ACTIVE_STATUS',
                                'value' => '1',
                                'style' => 'margin-right:5px'
                            );
                            echo form_checkbox($chkStatus);
                            ?>
                        </div>
                    </div>
                    <br clear="all" />
                </div>

            </div>
            <div class="modal-footer">
                <span class="modal_msg pull-left" style="color: green"></span>
                <button type="button" class="btn btn-primary" id="createModule">Save changes</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div> -->
<script src="<?php echo base_url(); ?>assets/security_access/bootgrid/jquery.bootgrid.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#data-table-basic").bootgrid({
            css: {
                icon: 'md icon',
                iconColumns: 'md-view-module',
                iconDown: 'md-expand-more',
                iconRefresh: 'md-refresh',
                iconUp: 'md-expand-less'
            }
        });
        $(document).on("click", "#createModule", function () {
            var mod_name = $("#txtModuleName").val();
            var mod_name_bn = $("#txtModuleNameBn").val();
            var sl_no = $("#SL_NO").val();
            var status = $("#ACTIVE_STATUS").prop('checked');

            $.ajax({
                type: "POST",
                url: "<?php echo site_url('securityAccess/createModule'); ?>",
                data: {mod_name: mod_name, mod_name_bn: mod_name_bn, sl_no: sl_no, status: status},
                success: function (result) {
                    $(".modal_msg").html(result);
                    window.location.reload(true);
                }
            });
        });
        $(document).on("click", ".statusType", function () {
            var status = $(this).attr('status');
            var linkId = $(this).attr('data-linkId');
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('admin/setup/changeModuleStatus'); ?>",
                data: {linkid: linkId, status: status},
                success: function (result) {
                    window.location.reload(true);
                }
            });
        });

           $(".userDltBtn").click(function () {
            if (confirm("Are you want to delete?")) {
                var userId = $(this).attr('userId');
                $.ajax({
                    url: '<?php echo site_url('admin/delete_userList_from_db'); ?>',
                    type: 'POST',
                    data: {userId: userId},
                    success: function (data) {
                        if (data == 1) {
                            alert('User deleted Successfully');
                            $('#user_row_' + userId).remove();
                        }
                    }
                });
            } else {
                return false;
            }
        });


        $(".modDltBtn").click(function () {
            if (confirm("Are you want to delete?")) {
                var mod_id = $(this).attr('mod_id');
                $.ajax({
                    url: '<?php echo site_url('securityAccess/delete_module_from_db'); ?>',
                    type: 'POST',
                    data: {mod_id: mod_id},
                    success: function (data) {
                        if (data == 1) {
                            alert('Module deleted Successfully');
                            $('#mod_row_' + mod_id).remove();
                        }
                    }
                });
            } else {
                return false;
            }
        });
    });
</script>