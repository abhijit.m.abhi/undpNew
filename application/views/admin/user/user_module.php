        <style type="text/css">
           img.img-circled{
              max-width: 150px;
              height: auto;
              vertical-align: middle;
          }
      </style>

      <h3 class="box-title">Assign User Module</h3>
      <form action="<?php echo base_url('admin/userModule/'.$userId) ?>" method="post">
      <input type="hidden" value="<?php echo $userId; ?>" name="userId">
      <table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">

        <?php foreach ($modules as $userModules) { ?>
        <tr>
            <td>
                <input type="checkbox" name="moduleId[]"  
                <?php 
                  foreach($prevModule as $pm)
                  {
                    if($pm->MODULE_ID==$userModules->MODULE_ID)
                    {
                      echo "checked";
                    }
                  }
                ?>
                value="<?php echo $userModules->MODULE_ID; ?>">
                <?php echo $userModules->MODULE_NAME; ?>   
                </td>
            </tr>
            <?php } ?>
        </table>
        <button type="submit" class="btn btn-primary btn-md">Submit</button>
        </form>

        <script src="<?php echo base_url(); ?>assets/security_access/bootgrid/jquery.bootgrid.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#data-table-basic").bootgrid({
                    css: {
                        icon: 'md icon',
                        iconColumns: 'md-view-module',
                        iconDown: 'md-expand-more',
                        iconRefresh: 'md-refresh',
                        iconUp: 'md-expand-less'
                    }
                });

            });
        </script>