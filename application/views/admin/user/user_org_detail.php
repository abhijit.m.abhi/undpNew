 <style type="text/css">
     img.img-circled{
          max-width: 150px;
          height: auto;
          vertical-align: middle;
            }
    </style>

    
    <form class="form-horizontal frmContent" id="event" method="post">
    <div class="block-flat">
        <span class="frmMsg"></span><br>
        <h3 class="box-title">User Details</h3>
    <table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
      
            <th>Photo</th>
            <td>  <?php if($user_detail->image != ""){
                      $user_img = "".$user_detail->image;
                      ?>
                      <pull-left>
                        <img class="img-circled"  src="<?php echo base_url($user_img) ?>" alt="User Photo" />
                      </pull-left>
                      <?php } else {?>
                      <img class="img-circled" src="<?php echo base_url(); ?>asset/img/avatar.png" alt="User Photo"/>
                      <?php } ?> <br>
                      <center>
                        
                      </center></td>
        </tr>
        <tr>
            <th>Type</th>
            <td><?php echo $user_detail->type?></td>
        </tr>
        <tr>
            <th>Organization name</th>
            <td><?php echo $user_detail->oname?></td>
        </tr>
        <tr>
            <th>Organization Email</th>
            <td><?php echo $user_detail->oemail?></td>
        </tr>
        <tr>
            <th>Organization Facebook URL</th>
            <td><?php echo $user_detail->facebook?></td>
        </tr>
        <tr>
            <th>Organization Website URL</th>
            <td><?php echo $user_detail->website?></td>
        </tr>
        <tr>
            <th>Has Organization</th>
            <td><?php echo ($user_detail->type =="organization") ? '<span class="btn btn-success btn-xs">Yes</span>' : '<span class="btn btn-danger btn-xs">No</span>'; ?></td>
        </tr>

          <tr>
            <th>Status</th>
            <td><?php echo ($user_detail->active == 1) ? '<span class="btn btn-success btn-xs">Active</span>' : '<span class="btn btn-danger btn-xs">Inactive</span>'; ?></td>
        </tr>
            <tr>
            <th>Registration Date & Time</th>
            <td><?php echo $user_detail->created_at?></td>
        </tr>
         <tr>
            <th>Login With </th>
            <td><?php echo ($user_detail->facebookId!= '') ? '<span class="btn btn-success btn-xs">Facebook</span>' : '<span class="btn btn-danger btn-xs">Website</span>'; ?></td>
        </tr>
    </table>
     <div class="form-group"><label class="col-lg-3 control-label">Active?</label>

                    <div class="col-lg-9">
                    <?php echo form_checkbox(array('name' => 'ACTIVE_FLAG', 'id' => 'ACTIVE_FLAG', 'value' => $user_detail->active, 'checked' => ($user_detail->active == 2) ? TRUE : FALSE)); ?>
                    <input type="hidden" name="FULL_NAME" id="FULL_NAME" value="<?php echo $user_detail->oname; ?>">
                    <input type="hidden" name="USER_MAIL" id="USER_MAIL" value="<?php echo $user_detail->oemail; ?>">          
                    </div>
                </div>
    </div>        
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <div class="col-lg-offset-2 col-lg-10"> 
            <input type="hidden" name="USER_ID" id="USER_ID" value="<?php echo  $user_detail->userId; ?>">
            <input type="button" class="btn btn-primary btn-sm formApproveSubmit" value="Submit">    
            <input type="reset" class="btn btn-default btn-sm" value="Reset">
            <span class="loadingImg"></span>
        </div>
    </div>
</form>