
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Inclusive Bangladesh</h5>

                <div class="ibox-tools">
                    <!-- <span class="btn btn-primary btn-xs pull-right  " data-toggle="modal" href="#modalDefault">Create
                        New </span> -->

                </div>

            </div>

            <div class="ibox-content">
                <div class="table-responsive contentArea">
                    <div class="row">

                    </div>
                    <br>

                    <?php
                    if (!empty($media_blog_list)) {
                        ?>
                      <table id="example1" class="table table-bordered table-striped" width="100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Media Blog Title</th>
                                <th>Media Blog Type</th>
                                <th>URL Type</th>
                                <th>Post Date</th>
                                <th>Total View</th>
                                <th>Interest</th>
                                <th>View</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i = 1;
                            foreach ($media_blog_list as $media_blog_lists) {
                                ?>
                                <tr id="media_blog_row_<?php echo $media_blog_lists->id; ?>">
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $media_blog_lists->Title; ?></td>
                                    <td><?php echo $media_blog_lists->oemail; ?></td>
                                    <td><?php echo $media_blog_lists->title; ?></td>
                                    <td><?php echo $media_blog_lists->type; ?></td>
                                    <td><?php echo $media_blog_lists->url_type; ?></td>
                                    <td><?php echo $media_blog_lists->post_date; ?></td>
                                    <td><?php echo $media_blog_lists->view_count; ?></td>
                                    <td><?php echo $media_blog_lists->TOTAL_INT; ?></td>
                                
                                    <td> <?php if($viewPerm>0){ ?>
                                     <a class="label label-primary  mediaBlogDetails"
                                                       data-visitor-org="<?php echo $media_blog_lists->id; ?>"
                                                       title="Click For Detail"><i
                                                class="fa fa-eye"></i></a>
                                        <?php 
                                        }
                                        ?>
                                                </td>

                                    <td class="text-center">
                                    <?php if($rejectPerm>0){ ?>
                                                 <a class="btn btn-danger btn-xs   mediaBlogDltBtn"
                                                       id="<?php echo $media_blog_lists->id; ?>"
                                                       title="Click For Delete"><i
                                                class="fa fa-remove"></i></a>
                                            <?php 
                                            }
                                            ?> 
                                                
                                    </td>

                                </tr>
                            <?php
                              $i++;
                            }

                            ?>
                            </tbody>
                        </table>
                    <?php
                    } else {
                        echo "<p class='text-danh text-danger'><br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No Data Found</p>";
                    }
                    ?>
                </div>
            </div>

        </div>
        <div class="modal inmodal appModal">
    <div class="modal-dialog">
        <div class="modal-content animated">
            <div class="modal-header">
                <button data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span><span
                            class="sr-only">Close</span></button>
                <h4 class="modal-title"></h4>
                <small class="font-bold"></small>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-white" type="button">Close</button>
            </div>
        </div>
    </div>
</div>

        <script>
    $(document).ready(function() {
        $('#example1').DataTable( {
            "scrollX": true,
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'pdf',
                    orientation:'landscape',
                    exportOptions: {
                        columns: [0,1,2,3,4,5,6,7,8]
                    }
                },

                {
                    extend: 'excel',
                    exportOptions: {
                        columns: [0,1,2,3,4,5,6,7,8]
                    }
                }

            ]
        } );
    } );
</script>



<script src="<?php echo base_url(); ?>assets/security_access/bootgrid/jquery.bootgrid.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () 
    {
        $("#data-table-basic").bootgrid({
            css: {
                icon: 'md icon',
                iconColumns: 'md-view-module',
                iconDown: 'md-expand-more',
                iconRefresh: 'md-refresh',
                iconUp: 'md-expand-less'
            }
        });
         $(".mailSendBtn").click(function () {
            if (confirm("Are you want to send mail?")) {
                var USER_ID = $(this).attr('USER_ID');
                var USER_MAIL =$(this).attr('USER_MAIL');
                var FULL_NAME = $(this).attr('FULL_NAME');
                $.ajax({
                    url: '<?php echo site_url('admin/again_send_mail_review'); ?>',
                    type: 'POST',
                    data: {USERID: USER_ID,FULL_NAME: FULL_NAME,USER_MAIL: USER_MAIL},
                    success: function (data) {
                        if (data == 1) {
                            alert('Mail Send Successfully');
                            
                        }
                    }
                });
            } else {
                return false;
            }
        });
        $(document).on("click", "#createModule", function () {
            var mod_name = $("#txtModuleName").val();
            var mod_name_bn = $("#txtModuleNameBn").val();
            var sl_no = $("#SL_NO").val();
            var status = $("#ACTIVE_STATUS").prop('checked');

            $.ajax({
                type: "POST",
                url: "<?php echo site_url('securityAccess/createModule'); ?>",
                data: {mod_name: mod_name, mod_name_bn: mod_name_bn, sl_no: sl_no, status: status},
                success: function (result) {
                    $(".modal_msg").html(result);
                    window.location.reload(true);
                }
            });
        });
        $(document).on("click", ".statusType", function () {
            var status = $(this).attr('status');
            var linkId = $(this).attr('data-linkId');
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('admin/setup/changeModuleStatus'); ?>",
                data: {linkid: linkId, status: status},
                success: function (result) {
                    window.location.reload(true);
                }
            });
        });


        $(".modDltBtn").click(function () {
            if (confirm("Are you want to delete?")) {
                var mod_id = $(this).attr('mod_id');
                $.ajax({
                    url: '<?php echo site_url('securityAccess/delete_module_from_db'); ?>',
                    type: 'POST',
                    data: {mod_id: mod_id},
                    success: function (data) {
                        if (data == 1) {
                            alert('Module deleted Successfully');
                            $('#mod_row_' + mod_id).remove();
                        }
                    }
                });
            } else {
                return false;
            }
        }); 

$(document).on("click", ".inclusivebdApprove", function () {
        var id = $(this).attr("data-visitor-org");
        //alert(orgId);
        $(".appModal").modal();
        $.ajax({
            type: "POST",
            data: {id: id},
            url: "<?php echo site_url() ?>/Admin/inclusive_bd_approve_detail",
            beforeSend: function () {
                $(".appModal .modal-title").html("Inclusive Bangladesh Details");
                $(".appModal .modal-body").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
            },
            success: function (data) {
                $(".appModal .modal-body").html(data);
                if (data == 'Mail Send successfully') {

                }
            }
        });
    });


$(document).on("click", ".mediaBlogDetails", function () {
        var id = $(this).attr("data-visitor-org");
        //alert(orgId);
        $(".appModal").modal();
        $.ajax({
            type: "POST",
            data: {id: id},
            url: "<?php echo site_url() ?>/Admin/mediaBlogDetails",
            beforeSend: function () {
                $(".appModal .modal-title").html("Media Blog Details");
                $(".appModal .modal-body").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
            },
            success: function (data) {
                $(".appModal .modal-body").html(data);
                if (data == 'Mail Send successfully') {

                }
            }
        });
    });

$(document).on("click", ".orgDetails", function () {
        var orgId = $(this).attr("data-visitor-org");
        //alert(orgId);
        $(".appModal").modal();
        $.ajax({
            type: "POST",
            data: {orgId: orgId},
            url: "<?php echo site_url() ?>/Admin/user_org_detail",
            beforeSend: function () {
                $(".appModal .modal-title").html("Organization Details");
                $(".appModal .modal-body").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
            },
            success: function (data) {
                $(".appModal .modal-body").html(data);
                if (data == 'Mail Send successfully') {

                }
            }
        });
    });


 $(document).on("click", ".formApproveSubmit", function () {
        if (confirm("Are You Sure?")) {
            var id = $("#id").val();
            //alert(USER_ID);
            var active_status = $("#active_status").val();
          
            $.ajax({
                type: "post",
                url: '<?php echo site_url('admin/update_status') ?>',
                data: {id: id, active_status: active_status},
                success: function (data) {
                   // alert(data);
                    $(".frmMsg").html(data);
                 window.location.reload(true);
                }
            });
        } else {
            return false;
        }
    });
  $(".mediaBlogDltBtn").click(function () {
            if (confirm("Are you want to delete?")) {
                var id = $(this).attr('id');
                $.ajax({
                    url: '<?php echo site_url('admin/deleteMediaBlog'); ?>',
                    type: 'POST',
                    data: {id: id},
                    success: function (data) {
                        if (data == 1) {
                            alert('Media Blog deleted Successfully');
                            $('#media_blog_row_' + id).remove();
                        }
                    }
                });
            } else {
                return false;
            }
        });
    });


</script>