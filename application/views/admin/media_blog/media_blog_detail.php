 <style type="text/css">
     img.img-circled{
          max-width: 150px;
          height: auto;
          vertical-align: middle;
            }
    </style>

    
    <form class="form-horizontal frmContent" id="event" method="post">
    <div class="block-flat">
        <span class="frmMsg"></span><br>
        <h3 class="box-title">Media Blog Details</h3>
    <table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
      
         
        <tr>
            <th>Name</th>
            <td><?php echo $media_blog_detail->Title?></td>
        </tr>
        <tr>
            <th>Email</th>
            <td><?php echo $media_blog_detail->oemail?></td>
        </tr>
        <tr>
            <th style="width: 30%;">Media Blog Title</th>
            <td><?php echo $media_blog_detail->title?></td>
        </tr>

        <tr>
            <th>Media Blog Image</th>
            <td> 
               <?php if($media_blog_detail->images != ""){
                      $images = explode('___', $media_blog_detail->images); 
                      ?>
                              <?php foreach ($images as $image): ?>
                          <div class="col-sm-3">
                           <img  class="img-responsive" src="<?php echo base_url(); ?>upload/blogs/<?php echo $image; ?>"/>
                           </div>
                            <?php endforeach; ?>
                     
                      <?php } else{?>
                       <img style="max-height: 200px; width: auto; padding-bottom: 4px;" src="<?php echo base_url(); ?>assets/img/avatar.png" alt="User Photo"/>
               
                     
                      <?php } ?> <br>
           </td>
        </tr>
        <tr>
            <th>Media Blog Type</th>
            <td><?php echo $media_blog_detail->type?></td>
        </tr>
        <tr>
            <th>URL Type</th>
            <td><?php echo $media_blog_detail->url_type?></td>
        </tr>
        <tr>
            <th>Post Date</th>
            <td><?php echo $media_blog_detail->post_date?></td>
        </tr>

          <tr>
            <th>Total View</th>
             <td><?php echo $media_blog_detail->view_count?></td>
           
         </tr>
          <tr>
            <th>Total Interest</th>
             <td><?php echo $media_blog_detail->interest_count?></td>
           
         </tr>
           
    </table>

    </div>        
    <div class="hr-line-dashed"></div>

</form>