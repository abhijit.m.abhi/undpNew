<style>
    .error {
        border-color: #EBCCD1;
        color: #B94A48;
    }
    .modal-header h4 {
    color: #681156;
    margin-top: 15px;
    font-size: 25px;
    margin-bottom: 8px;
    border-bottom: 1px solid #ddd !important;
    padding-bottom: 20px !important;
}
</style>
<div>
  <?php echo form_open("portal/update_inactive_account/" . $user_detail->userId); ?> 

    <div class="msg">
        <?php
        if (validation_errors() != false) {
            ?>
            <div class="alert alert-danger">
                <button data-dismiss="alert" class="close" type="button">×</button>
                <?php echo validation_errors(); ?>
            </div>
            <?php
        }
        ?>
    </div>
    <div class="form-group">
       <label for="">Do You want to Inactive your profile?</label>
       <button type="submit" class="btn btn-info">Yes</button>
       <button type="button" class="btn btn-info" data-dismiss="modal" aria-hidden="true">
        No</button>
        <input type="hidden" name="redirectUrl" value="<?php echo $url; ?>">
        <input type="hidden" id="inactiveValue" value="0" name="inactiveValue">
        </div>
<?php echo form_close(); ?>
</div>
