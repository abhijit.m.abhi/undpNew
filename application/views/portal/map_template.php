<!DOCTYPE html>

<html lang="en-US">
<?php $this->load->view('portal/template/header'); ?>

<!-- ******************************************************************************************** -->

<!-- CSS STARTS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>event/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>event/fonts/stylesheet.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>event/css/font-awesome.min.css">
<link href="<?php echo base_url(); ?>assets/vendors/fileuploader/src/jquery.fileuploader.css" media="all"
      rel="stylesheet">
<link rel="stylesheet" href="<?php echo base_url(); ?>event/css/owl.carousel.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>event/css/owl.theme.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>event/css/datepicker.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>old/css/style.css"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>event/css/styl.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>event/css/responsive.css">
<style>
    .date-highligh {
        color: #f00;
        /*text-decoration: underline;*/
        outline: 2px solid #ff0000;
    }
</style>



<link rel="stylesheet" href="<?php echo base_url(); ?>old/css/icon_style.css" type="text/css">


<!-- ******************************************************************************************** -->

<body>
<div class="page-wrapper">
    <?php $this->load->view('portal/template/menu.php'); ?>
    <div id="" style="">
        <div class="container">
            <section>
                <?php
                echo $_content;

                ?>
            </section>
        </div>
    </div>
    <footer id="page-footer">
        <?php $this->load->view('portal/template/footer') ?>
    </footer>
</div>
<?php $this->load->view('portal/template/modal.php'); ?>
<!--end page-wrapper-->
<a href="#" class="to-top scroll" data-show-after-scroll="600"><i class="arrow_up"></i></a>

<script type="text/javascript" src="<?php echo base_url(); ?>portalAssets/js/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>portalAssets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>portalAssets/js/bootstrap-select.min.js"></script>
<!--<script type="text/javascript" src="--><?php //echo base_url(); ?><!--portalAssets/js/richmarker-compiled.js"></script>-->
<script type="text/javascript" src="<?php echo base_url(); ?>portalAssets/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>portalAssets/js/jquery.validate.min.js"></script>
<!-- <script type="text/javascript" src="<?php echo base_url(); ?>portalAssets/js/custom.js"></script> -->
<script type="text/javascript" src="<?php echo base_url(); ?>portalAssets/js/maps.js"></script>
<script src="<?php echo base_url(); ?>assets/js/portal-js/bootstrap-datetimepicker.min.js"></script>

<!-- ******************************************************************************************** -->

<!-- JS STARTS -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>event/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>event/js/selectivizr-min.js"></script>
<script src="<?php echo base_url(); ?>event/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>event/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>event/js/owl.carousel.min.js"></script>
<script src="<?php echo base_url(); ?>event/js/ajax.js"></script>
<script src="<?php echo base_url(); ?>old/assets/js/bootstrap-select.min.js"></script>
<!--<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyAsJfFqX2xpb8xlgFvECm4Mo0jXFtyp1zM&libraries=places"></script>-->
<!--<script type="text/javascript"-->
<!--        src="https://maps.google.com/maps/api/js?key=AIzaSyAsJfFqX2xpb8xlgFvECm4Mo0jXFtyp1zM&libraries=places"></script>-->
<script src="<?php echo base_url(); ?>old/assets/js/richmarker-compiled.js"></script>
<script src="<?php echo base_url(); ?>old/assets/js/maps.js"></script>
<script src="<?php echo base_url(); ?>old/assets/js/icheck.min.js"></script>
<script src="<?php echo base_url(); ?>old/assets/vendors/fileuploader/src/jquery.fileuploader.min.js"
        type="text/javascript"></script>
<script src="<?php echo base_url(); ?>old/assets/vendors/fileuploader/examples/default-upload/js/custom.js"
        type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>old/assets/js/ajax.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>old/assets/js/custom.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>old/assets/js/myscript.js"></script>




<!--<script type="text/javascript" src="--><?php //base_url(); ?><!--old/assets/js/markerclusterer_packed.js"></script>-->
<!--<script type="text/javascript" src="--><?php //base_url(); ?><!--old/assets/js/infobox.js"></script>-->
<script type="text/javascript" src="<?php echo base_url(); ?>old/assets/js/jquery.fitvids.js"></script>
<script src="<?php echo base_url(); ?>event/js/jquery.downCount.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>old/assets/js/jquery.trackpad-scroll-emulator.min.js"></script>
<!--<script type="text/javascript" src="assets/js/bootstrap-datepicker.js"></script>-->
<script type="text/javascript" src="<?php echo base_url(); ?>old/assets/js/jquery.nouislider.all.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>old/assets/js/myscript.js"></script>



<script src="<?php echo base_url(); ?>old/js/main.js"></script>
<script src="<?php echo base_url(); ?>old/js/ajax.js"></script>
<script src="<?php echo base_url(); ?>old/js/fbzahed.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.date_picker').datepicker({
            format: "yyyy-mm-dd"
        });
    });
</script>




<script type="text/javascript">
    $(document).ready(function () {
        $("#event-calender-slider").owlCarousel({
            autoPlay: 3000,
            pagination: false,
            navigation: true,
            itemsCustom: [[0, 1], [479, 2], [768, 5], [991, 6], [1199, 6]],
        });
        $("#previous-event-calender-slider").owlCarousel({
            autoPlay: 3000,
            pagination: false,
            navigation: true,
            itemsCustom: [[0, 1], [479, 2], [768, 5], [991, 6], [1199, 6]],
        });



        var active_dates = <?php echo json_encode($eventsDate);?>;

        // convert array of objects into array of property
        var finalArray = active_dates.map(function (obj) {
            return obj.start_date;
        });

        var active_dates = finalArray;

        $(function () {

            window.prettyPrint && prettyPrint();
            var calender = $('#event-calender').on('show', function (e) {

                $('.day');
            }).datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
                todayHighlight: true,
                beforeShowDay: function (date) {
                    var d = date;
                    var curr_date = ("0" + d.getDate()).slice(-2);
                    var curr_month = ("0" + (d.getMonth() + 1)).slice(-2); //Months are zero based
                    var curr_year = d.getFullYear();
                    var formattedDate = curr_year + "-" + curr_month + "-" + curr_date;

                    if ($.inArray(formattedDate, active_dates) != -1) {

                        return {
                            tooltip: 'Event',
                            classes: 'date-highligh'
                        };
                    }
                    return;
                }
            }).on('changeDate', function (e) {

                $.ajax({
                    url: 'calSearch',
                    type: 'POST',
                    data: {date: e.format(), action: 'cal_search'}
                }).done(function (response) {
                    //                            console.log(response);
                    $('#search_result_container').html(response);
                });
                //                        console.log(e.format());
            });

        });
    });
</script>

<script>
    $(function () {
        $('#search_btn').on('click', function () {

            var category = $('#category').val();
            var district = $('#district').val();
            var organization = $('#organization').val();
            var search_month = $('#search_month').val();
            var search_year = $('#search_year').val();
            var search_text = $('#search_text').val();
            $.ajax({
                url: 'calSearch',
                data: {
                    category: category,
                    district: district,
                    organization: organization,
                    search_month: search_month,
                    search_year: search_year,
                    search_text: search_text,
                    action: 'search'
                },
                type: 'POST'
            }).done(function (response) {
                $('#search_result_container').html(response);
//                    console.log(response);
            });
        })
    })

</script>


<!-- ******************************************************************************************** -->


<script type="text/javascript">
    $(document).ready(function () {
        $(".rcorners2").mouseover(function () {
            $pb = $(this).parents("div.col-md-2:first");
            $element = $pb.find(".bottomText");
            $element.show();

        });
    });
    $(document).ready(function () {
        $(".rcorners2").mouseout(function () {
            $pb = $(this).parents("div.col-md-2:first");
            $element = $pb.find(".bottomText");
            $element.hide();

        });
    });
    $(document).ready(function () {
        $('.date_picker').datepicker({
            format: "yyyy-mm-dd"
        });
    });
</script>

</body>
