<style media="screen">
    #easyPaginate, #easyPaginateBlog {
        width: 100%;
    }

    .easyPaginateNav {
        clear: both;
        margin-top: 20px;
        text-align: center;
    }

    .easyPaginateNav a {
        padding: 8px;
    }

    .easyPaginateNav a.current {
        font-weight: bold;
        text-decoration: underline;
        font-size: 24px;
    }

    #easyPaginate, #easyPaginateBlog {
        display: -webkit-flex; /* Safari */
        display: flex;
        flex-wrap: wrap;
    }
</style>
<style>
    .panelD .panel-heading, .panelD .panel-footer {
        background-color: #fff !important;
    }

    .panel-image img.panel-image-preview {
        width: 100%;
        border-radius: 4px 4px 0px 0px;
    }

    .panel-heading ~ .panel-image img.panel-image-preview {
        border-radius: 0px;
    }

    .panel-heading .list-inline {
        margin: 0px 0px 0px -5px !important;
    }

    .panel-body {
        display: block;
        padding: 2px 5px 0px;
    }

    .panel-body blockquote {
        margin: 5px 0 5px;
    }

    .panel-image ~ .panel-footer a {
        padding: 0px 10px;
        color: rgb(100, 100, 100);
    }

    .panel-image ~ .panel-footer span {
        color: rgb(100, 100, 100);
    }

    .panel-footer .list-inline {
        margin: 0 0 0 -15px !important;
    }

    .panel-image.hide-panel-body ~ .panel-body {
        height: 0px;
        padding: 0px;
    }

    /*==========  Mobile First Method  ==========*/
    /* Custom, iPhone Retina */
    @media only screen and (max-width: 320px) {
        .level-line-up {
            position: relative;
            top: -4px;
        }
    }
</style>

<style>
    .new_rz_box {
        display: block;
        margin-top: 40px;
        padding: 10px 0;
        border-bottom: 1px solid #ccc;
    }

    .new_rz_box h6 {
        color: #446678;
        text-transform: uppercase;
    }

    .new_rz_box h5 {
        color: #446678;
        text-transform: uppercase;
    }
.new_rz_box2 input {
        border: 1px solid #d9d9d9 !important;
    }
    .new_rz_box h3 {
        color: #446678;
        text-transform: uppercase;
    }

    .new_rz_box h4 {
        color: #333;
    }

    .new_rz_box2 {
        display: block;
        padding: 10px 0;
    }

    .new_rz_box2 h3 {
        color: #446678;
        margin-bottom: 10px;
    }

    .new_rz_box2 input {
        border: 1px solid #d9d9d9;
        width: 100%;
        padding: 5px;
    }

    .new_rz_box2 textarea {
        border: 1px solid #d9d9d9;
        width: 100%;
        padding: 10px 20px;
    }

    .new_rz_box2 select {
        border: 1px solid #d9d9d9;
        width: 100%;
        padding: 5px 10px;
    }

    .row > div {
        display: inline-block;

    }

    .rz_button3 {
        margin-top: 40px;
        border: 0;
        background-color: #012F49;
        color: #FFF;
        font-size: 25px;
        text-align: center;
        padding: 10px 20px;
        text-transform: uppercase;
        border-radius: 10px;
    }

    .rz_button3:hover {
        color: #FFF;
    }
</style>
<style type="text/css">
    .thumb {
        width: 50px;
        height: 50px;
    }

    #list {
        position: absolute;
        top: 0px;
    }

    #list div {
        float: left;
        margin-right: 10px;
    }
</style>


<div class="others_page_wrp">
    <section class="org_map_wrp">
        <div class="container">
            <br>
            <div class="col-lg-12 text-center">
                <form action="<?php echo site_url() . '/blog/searchBlogPost' ?>" class="" method="post">
                    <!--                --><?php //$categoryList = array('Competitions', 'Conferences', 'Exchange-programs', 'Fellowships', 'Internships', 'Scholarships', 'Workshops', 'Miscellaneous', 'Training'); ?>
                    <div class="row new_rz_box2">
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <select name="blog_type">
                                    <option value="">All</option>
                                    <option value="image" <?php echo ($blog_ty == 'image') ? 'selected="selected"' : '' ?> >
                                        Image
                                    </option>
                                    <option value="audio" <?php echo ($blog_ty == 'audio') ? 'selected="selected"' : '' ?>>
                                        Audio
                                    </option>
                                    <option value="video" <?php echo ($blog_ty == 'video') ? 'selected="selected"' : '' ?>>
                                        Video
                                    </option>
                                </select>
                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <select name="order">
                                    <option value="">Select order</option>
                                    <option value="desc" <?php echo ($or == 'desc') ? 'selected="selected"' : '' ?>>
                                        Newest to oldest
                                    </option>
                                    <option value="asc" <?php echo ($or == 'asc') ? 'selected="selected"' : '' ?>>Oldest
                                        to newest
                                    </option>
                                    <option value="today" <?php echo ($or == 'today') ? 'selected="selected"' : '' ?>>
                                        Today's post
                                    </option>
                                    <option value="week" <?php echo ($or == 'week') ? 'selected="selected"' : '' ?>>This
                                        week post
                                    </option>
                                </select>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <input type="text" class="" placeholder="Search for..."
                                       name="search" value="<?php if (!empty($src)) {
                                    echo $src;
                                } ?>"
                                       style="min-width: 120px;     border-radius: 6px; margin: 0 3px 0 7px;   padding: 5px;">
                            </div>


                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                <input type="submit" class="btn btn-default btn-block search-btn" type="button" name="search_btn"
                                       value="Search">
                            </div>

                        </div>
                    </div>
                </form>
            </div>

            <?php


            if (isset($this->session->userdata['user_logged_user']['userId'])) {
                $id = $this->session->userdata['user_logged_user']['userId'];
            } else {
                $id = '';
            }
            ?>

            <p style="text-align: center;margin: 10px;">
            <hr>
            <hr>
            </p>

            <?php foreach ($blog_posts as $row) : ?>


                <?php
                $interests = $this->blog_model->getBlogInterest($row->id);

                $likeReaction = 0;
                $smileReaction = 0;
                $loveReaction = 0;
                $wowReaction = 0;
                ?>

                <?php foreach ($interests as $interest) { ?>
                    <?php
                    if ($interest->type == '1') {
                        $likeReaction = $interest->interest_count;
                    } elseif ($interest->type == '2') {
                        $smileReaction = $interest->interest_count;
                    } elseif ($interest->type == '3') {
                        $loveReaction = $interest->interest_count;
                    } elseif ($interest->type == '4') {
                        $wowReaction = $interest->interest_count;
                    }

                    ?>

                <?php } ?>


                <div class="col-xs-12 col-sm-6 col-md-4 show_block_elem">
                    <div class="panel panelD" style="border: 1px solid #ddd">
                        <div class="thumbnail panel-image embed-responsive embed-responsive-16by9"
                             style="margin-bottom: 2px">
                            <?php if ($row->type == 'image') { ?>

                                <?php $images = explode('___', $row->images); ?>

                                <a href="<?php echo site_url() . '/blog/blogView/' .

                                    $row->id; ?>"
                                   target="_blank"><img style="height: 200px; width: auto"
                                                        src="<?php echo base_url() . "upload/blogs/" . $images[0]; ?>"
                                                        alt="<?php echo $row->title; ?>"/></a>
                            <?php } else {
                                $url_link = '';
                                if ($row->url_type == 'Youtube') {
                                    $url2Embed = str_replace("watch?v=", "embed/", $row->urls);
                                    $url_link = str_replace("[URL]", $url2Embed, $row->url_data);
                                } elseif ($row->url_type == 'SoundCloud') {
                                    $url_link = str_replace("[URL]", $row->urls, $row->url_data);
                                } elseif ($row->url_type == 'DailyMotion') {
                                    $url2Embed = str_replace("https://www.dailymotion.com/video", "https://www.dailymotion.com/embed/video", $row->urls);
                                    $url_link = str_replace("[URL]", $url2Embed, $row->url_data);
                                } elseif ($row->url_type == 'Vimeo') {
                                    $url2Embed = str_replace("https://vimeo.com/", "https://player.vimeo.com/video/", $row->urls);
                                    $url_link = str_replace("[URL]", $url2Embed, $row->url_data);
                                } else {
                                    echo '<div width="100%" height="258" style="margin-bottom: 0px;"><h1>Something Missing</h1></div>';
                                }
                                echo $url_link;
                            }
                            ?>
                        </div>
                        <div class="" style="padding: 7px; font-size: 14px;">
                            <button class="btn btn-link" id="like-button<?php echo $row->id; ?>"
                                    onclick="reactionFunction(1, <?php echo $row->id . ', ' . $id ?>)"
                                    style="padding-right: 0; text-decoration: none; background-color: #002f49; border-radius: 10px; padding: 0 5px; color: #ffffff"
                                    title="Like">
                                            <span id="like-count<?php echo $row->id; ?>"
                                                  style="color: #ffffff"><?php echo $likeReaction; ?></span> <img
                                        src="<?php echo base_url(); ?>assets/img2/icon/like.png" aria-hidden="true"
                                        width="12" height="12"
                                        style="margin: 0 4px 2px"/>
                            </button>
                            <button class="btn btn-link" id="smiley-button<?php echo $row->id; ?>"
                                    onclick="reactionFunction(2, <?php echo $row->id . ', ' . $id ?>)"
                                    style="padding-right: 0; text-decoration: none; background-color: #23a797; border-radius: 10px; padding: 0 5px; color: #ffffff"
                                    title="Smiley">
                                            <span id="smiley-count<?php echo $row->id; ?>"
                                                  style="color: #ffffff"><?php echo $smileReaction; ?></span> <img
                                        src="<?php echo base_url(); ?>assets/img2/icon/smile.png" aria-hidden="true"
                                        width="12" height="12"
                                        style="margin: 0 4px 2px"/>
                            </button>
                            <button class="btn btn-link" id="love-button<?php echo $row->id; ?>"
                                    onclick="reactionFunction(3, <?php echo $row->id . ', ' . $id ?>)"
                                    style="padding-right: 0; text-decoration: none; background-color: #e43733; border-radius: 10px; padding: 0 5px; color: #ffffff"
                                    title="Love">
                                            <span id="love-count<?php echo $row->id; ?>"
                                                  style="color: #ffffff"><?php echo $loveReaction; ?></span> <img
                                        src="<?php echo base_url(); ?>assets/img2/icon/heart.png" aria-hidden="true"
                                        width="12" height="12"
                                        style="margin: 0 4px 2px"/>
                            </button>
                            <button class="btn btn-link" id="wow-button<?php echo $row->id; ?>"
                                    onclick="reactionFunction(4, <?php echo $row->id . ', ' . $id ?>)"
                                    style="padding-right: 0; text-decoration: none; background-color: #79ad48; border-radius: 10px; padding: 0 5px; color: #ffffff"
                                    title="Wow">
                                            <span id="wow-count<?php echo $row->id; ?>"
                                                  style="color: #ffffff"><?php echo $wowReaction; ?></span> <img
                                        src="<?php echo base_url(); ?>assets/img2/icon/WOW.png" aria-hidden="true"
                                        width="12" height="12"
                                        style="margin: 0 4px 2px"/>
                            </button>

                        </div>
                        <div class="panel-heading"
                             style=" min-height:100px; max-height: 100px; overflow-y: hidden;">
                            <a style="color: #E43533;"
                               href="<?php echo site_url() . '/blog/blogView/' . $row->id; ?>"
                               target="_blank"><h3
                                        style="color: #E43533; text-align: center; text-transform: uppercase; min-height: 57px;"><?php echo $row->title; ?></h3>
                            </a>
                        </div>
                        <div class="panel-body">
                            <h4 style="color: #78AE48">Posted
                                on: <?php echo date("M d y", strtotime($row->post_date)) ?></h4>
                            <h4 style="color: #78AE48">Posted
                                by: <?php echo $row->organization_name; ?></h4>
                        </div>
                        <hr style="margin: 0.5em;">

                        <div class="panel-body clearfix" style="padding:3px">
                            <div class="list-inline clearfix">

                    <span style="margin-top: 5px;">
                                        <i class="fa fa-tags fa-flip-horizontal" aria-hidden="true"></i> <span
                                style="background: lightgray; padding: 6px 12px; font-size: 14px; border-radius: 25px; text-transform: capitalize;"><?php echo $row->type; ?></span></span>
                                <p class="pull-right">
                                    <i style="color: #012F49" class="fa fa-share-alt" aria-hidden="true"></i>
                                    <a class="facebook customer share"
                                       style="color: #4267b2;"
                                       href="http://www.facebook.com/sharer.php?u=<?php // echo $actual_link . '/blog_view.php?blog_id=' . $row->id; ?>"
                                       title="Facebook share" target="_blank"><i class="fa fa fa-facebook fa-lg"
                                                                                 style="margin: 0 8px;"
                                                                                 aria-hidden="true"></i></a>
                                    <a class="twitter customer share"
                                       style="color: #1b95e0;"
                                       href="http://twitter.com/share?url=<?php // echo $actual_link . '/blog_view.php?blog_id=' . $row->id; ?>&amp;hashtags=youthopiabangla"
                                       title="Twitter share" target="_blank"><i class="fa fa fa-twitter fa-lg"
                                                                                style="margin: 0 8px;"
                                                                                aria-hidden="true"></i></a>
                                    <a class="google_plus customer share"
                                       style="color: #db4437;"
                                       href="https://plus.google.com/share?url=<?php // echo $actual_link . '/blog_view.php?blog_id=' . $row->id; ?>"
                                       title="Google Plus Share" target="_blank"><i
                                                class="fa fa fa-google-plus fa-lg" style="margin: 0 8px;"
                                                aria-hidden="true"></i></a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

            <?php endforeach; ?>


        </div>
    </section>
</div>


<script>
    function reactionFunction(type, blog_id, user_id = null) {

        if (!user_id) {
            alert('Please login to perform this task');
            return false;
        }
        $.ajax({
            url: 'calSearch',
            type: 'POST',
            data: {
                blog_type: type,
                user_id: user_id,
                blog_id: blog_id,
                action: 'blogInterest'
            }
        }).done(function (response) {
            var res = $.parseJSON(response);
            console.log(res);

//            console.log( Object.keys(res).length );


            $('#like-count' + res.blog_id).html(0);
            $('#smiley-count' + res.blog_id).html(0);
            $('#love-count' + res.blog_id).html(0);
            $('#wow-count' + res.blog_id).html(0);

            res.count.forEach(function (counts) {
                if (counts.type == '1') {
                    $('#like-count' + res.blog_id).html(counts.interest_count);
                }
                else if (counts.type == '2') {
                    $('#smiley-count' + res.blog_id).html(counts.interest_count);
                }
                else if (counts.type == '3') {
                    $('#love-count' + res.blog_id).html(counts.interest_count);
                }
                else if (counts.type == '4') {
                    $('#wow-count' + res.blog_id).html(counts.interest_count);
                }
                else {
                }
            });
            if (!res.ack) {
                alert(res.msg);
            }
        });
    }
</script>