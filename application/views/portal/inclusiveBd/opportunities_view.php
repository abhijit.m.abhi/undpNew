<style type="text/css">
    .frame {
        height: 205px;
        display: table;
        width: 100%;
        text-align: center;
        padding: 0px;
    }
    .helper22 {
        display: table-cell;
        text-align: center;
        vertical-align: middle;
        height: 200px;
    }
    .frame span>img {
        vertical-align: middle;
        display: inline-block !important;
        width: auto !important;
        max-height: 100%;
        max-width: 100%;
    }
</style>







<div id="opportunities" class="">
    <!-- <h3>Upcomming Events</h3> -->

    <div class="others_page_wrp">
        <section class="org_map_wrp">
            <div class="container">
                <br>
                <div class="col-lg-12 text-center">
                    <form action="<?php echo site_url() . 'inclusiveBd/searchInclusivePost' ?>" class="" method="post">

                        <div class="row new_rz_box2">
                            <div class="col-lg-12 text-center">
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">

                                    <select class="form-control" name="LOOKUP_DATA_ID" id="LOOKUP_DATA_ID"
                                            data-tags="true" data-placeholder="Select Category" data-allow-clear="true">
                                        <option value="">Select Category</option>
                                        <?php foreach($category as $categorys) : ?>
                                            <option value="<?php echo $categorys->LOOKUP_DATA_ID; ?>" <?php echo ($cate == $categorys->LOOKUP_DATA_ID) ? 'selected' : '' ?> ><?php echo $categorys->LOOKUP_DATA_NAME; ?></option>
                                        <?php endforeach; ?>

                                    </select>
                                </div>

                                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                                    <select name="order">
                                        <option value="">Select order</option>
                                        <option value="desc" <?php echo ($or == 'desc') ? 'selected' : '' ?>>Newest to oldest</option>
                                        <option value="asc" <?php echo ($or == 'asc') ? 'selected' : '' ?>>Oldest to newest</option>
                                        <option value="today" <?php echo ($or == 'today') ? 'selected' : '' ?>>Today's post</option>
                                        <option value="week" <?php echo ($or == 'week') ? 'selected' : '' ?>>This week post</option>
                                    </select>
                                </div>

                                <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">
                                    <input list="search" type="text" class="" placeholder="Search for..."
                                           name="search" value="<?php echo (!empty($src)) ? $src : ''; ?>"
                                           style="min-width: 120px;     border-radius: 6px; margin: 0 3px 0 7px;   padding: 5px;">

                                    <datalist id="search">
                                        <?php foreach ($inclusive_search_list as $i_list) : ?>
                                        <option value="<?php echo $i_list->title; ?>">
                                            <?php endforeach; ?>
                                    </datalist>

                                </div>



                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                    <input type="submit" class="btn btn-default btn-block" type="button" name="search_btn"
                                           value="Search">

                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                    <?php
                                    $portalUser=$this->session->userdata('user_logged_user');
                                    //echo '<pre>';print_r($session_info);exit;
                                    ?>
                                    <?php
                                    if($this->session->userdata('user_logged_user')){
                                        ?>
                                        <div class="dropdown">
                                            <button class="btn btn-default1 btn-block dropdown-toggle" type="button" data-toggle="dropdown">Create
                                                <span class="caret"></span></button>
                                            <ul class="dropdown-menu">
                                                <li><a href="<?php echo site_url();?>inclusiveBd/createInclusivebd/1">YOUTH ACTION</a></li>
                                                <li><a href="<?php echo site_url();?>inclusiveBd/createInclusivebd/2">OPPORTUNITY</a></li>
                                                <li><a href="<?php echo site_url();?>inclusiveBd/createInclusivebd/3">NEWS</a></li>
                                            </ul>
                                        </div>

                                        <?php
                                    }else{ ?>

                                        <?php
                                    }

                                    ?>


                                    <!-- <a class="btn btn-default1 btn-block" type="button" href="<?php echo site_url();?>/InclusiveBd/createInclusivebd">Create</a> -->

                                </div>


                            </div>
                        </div>
                    </form>
                </div>

                <?php


                if (isset($this->session->userdata['user_logged_user']['userId'])) {
                    $id = $this->session->userdata['user_logged_user']['userId'];
                     $linkShare = $id;
                } else {
                    $id = '';
                    $linkShare = 0;
                }
                ?>

                <p style="text-align: center;margin: 10px;">
                <hr>
                <hr>
                </p>

                <?php foreach ($inclusive_posts_y as $row) : ?>


                    <?php
                    $interests = $this->inclusivebd_model->getInclusiveInterest2($row->id, $id);
                    $inter = $this->inclusivebd_model->getInclusiveInterest($row->id);


                    $likeReaction = 0;
                    $smileReaction = 0;
                    $loveReaction = 0;
                    $wowReaction = 0;


                    $action = '';
                    $action_name = '';
                    ?>

                    <?php foreach ($interests as $interest) { ?>
                        <?php
                        if ($interest->type == '1') {
                            $likeReaction = $interest->interest_count;
                            $action = 'like';
                            $action_name = 'LIKE';
                        } elseif ($interest->type == '2') {
                            $smileReaction = $interest->interest_count;
                            $action = 'haha';
                            $action_name = 'HAHA';
                        } elseif ($interest->type == '3') {
                            $loveReaction = $interest->interest_count;
                            $action = 'love';
                            $action_name = 'LOVE';
                        } elseif ($interest->type == '4') {
                            $wowReaction = $interest->interest_count;
                            $action = 'wow';
                            $action_name = 'WOW';
                        }

                        ?>

                    <?php } ?>

                    <?php foreach ($inter as $intere) : ?>
                        <?php
                        if ($intere->type == '1') {
                            $likeReaction = $intere->interest_count;
                        } elseif ($intere->type == '2') {
                            $smileReaction = $intere->interest_count;
                        } elseif ($intere->type == '3') {
                            $loveReaction = $intere->interest_count;
                        } elseif ($intere->type == '4') {
                            $wowReaction = $intere->interest_count;
                        }
                        ?>
                    <?php endforeach; ?>


                    <div class="col-xs-12 col-sm-6 col-md-4 show_block_elem">
                        <div class="panel panelD" style="border: 1px solid #ddd">
                        <a href="<?php echo site_url() . '/InclusiveBd/inclusiveBdView/' .

                                        $row->id; ?>"
                                       target="_blank">
                            <div class="thumbnail panel-image embed-responsive embed-responsive-16by9 frame"
                                 style="margin-bottom: 2px">
                                <?php if ($row->type == '') { ?>

                                    <?php $images = explode('___', $row->images); ?>

                                    <span class="helper22"><img style=""
                                                                                   src="<?php echo base_url() . "upload/inclusivebd/" . $images[0]; ?>"
                                                                                   alt="<?php echo $row->title; ?>"/></span>
                                <?php } else {
                                    $url_link = '';
                                    if ($row->url_type == 'Youtube') {
                                        $url2Embed = str_replace("watch?v=", "embed/", $row->urls);
                                        $url_link = str_replace("[URL]", $url2Embed, $row->url_data);
                                    } elseif ($row->url_type == 'SoundCloud') {
                                        $url_link = str_replace("[URL]", $row->urls, $row->url_data);
                                    } elseif ($row->url_type == 'DailyMotion') {
                                        $url2Embed = str_replace("video", "embed/video", $row->urls);
                                        $url_link = str_replace("[URL]", $url2Embed, $row->url_data);
                                    } elseif ($row->url_type == 'Vimeo') {
                                        $url2Embed = str_replace("vimeo.com", "player.vimeo.com/video", $row->urls);
                                        $url_link = str_replace("[URL]", $url2Embed, $row->url_data);
                                    } else {
                                        echo '<div width="100%" height="258" style="margin-bottom: 0px;"><h1>Something Missing</h1></div>';
                                    }
                                    echo $url_link;
                                }
                                ?>
                            </div></a>
                            <div class="in">
                                <div align="center">

                                    <div id="" style="padding-left: 5px; padding-top: 5px;">
                                        <input type="hidden" class="c_id" name="c_id"
                                               value="<?php echo $row->id; ?>">
                                        <div style="" align="left">

                                          <span id="Fb_Imo"><a class="FB_reactions this_cls_<?php echo $row->id; ?>"
                                                               data-reactions-type='horizontal'
                                                               data-emoji-id="<?php echo (!empty($action)) ? $action : ''; ?>"
                                                               data-user-id="<?php echo $id; ?>"
                                                               data-unique-id="<?php echo $row->id; ?>"
                                                               data-emoji-class="<?php echo (!empty($action)) ? $action : ''; ?>">
                                                    <span style=""><?php echo (!empty($action_name)) ? $action_name : 'LIKE'; ?></span>
                                                </a>
                                            </span>
                                            <span class="my_buttons"
                                                  style="padding: 7px; font-size: 14px;">

                                        <span><button class="c_btn" style="border-radius: 12px" href=""> <img
                                                    style="height: 20px; width: auto;"
                                                    src="<?php echo base_url() . 'fb_imo/emojis/like.svg'; ?>"
                                                    class="emoji"> <span
                                                    id="like-count<?php echo $row->id; ?>"><?php echo $likeReaction; ?></span></button></span>
                                        <span><button class="c_btn" style="border-radius: 12px" href=""> <img
                                                    style="height: 20px; width: auto;"
                                                    src="<?php echo base_url() . 'fb_imo/emojis/love.svg'; ?>"
                                                    class="emoji"> <span
                                                    id="love-count<?php echo $row->id; ?>"> <?php echo $loveReaction; ?> </span> </button></span>
                                        <span><button class="c_btn" style="border-radius: 12px" href=""> <img
                                                    style="height: 20px; width: auto;"
                                                    src="<?php echo base_url() . 'fb_imo/emojis/haha.svg'; ?>"
                                                    class="emoji"> <span
                                                    id="smiley-count<?php echo $row->id; ?>"> <?php echo $smileReaction; ?> </span> </button></span>
                                        <span><button class="c_btn" style="border-radius: 12px" href=""> <img
                                                    style="height: 20px; width: auto;"
                                                    src="<?php echo base_url() . 'fb_imo/emojis/wow.svg'; ?>"
                                                    class="emoji"> <span
                                                    id="wow-count<?php echo $row->id; ?>"> <?php echo $wowReaction; ?> </span> </button></span>
                                    </span>


                                        </div>
                                    </div>
                                </div>
                            </div><br>
                            <div class="panel-heading"
                                 style=" min-height:100px; max-height: 100px; overflow-y: hidden;">
                                <a style="color: #E43533;"
                                   href="<?php echo site_url() . '/InclusiveBd/inclusiveBdView/' . $row->id; ?>"
                                   target="_blank"><h3
                                        style="color: #E43533; text-align: center; text-transform: uppercase; min-height: 57px;"><?php echo $row->title; ?></h3>
                                </a>
                            </div>
                            <div class="panel-body">
                                <h4 style="color: #78AE48">Posted
                                    on: <?php echo date("M d Y", strtotime($row->post_date)) ?></h4>
                                <h4 style="color: #78AE48">Posted
                                    by: <?php echo $row->fname; ?></h4>
                            </div>
                            <hr style="margin: 0.5em;">

                            <div class="panel-body clearfix" style="padding:3px">
                                <div class="list-inline clearfix">
                                    <?php $actual_base_link = base_url(); ?>
                                    <?php $actual_controller = $this->uri->segment('1'); ?>
                                    <?php $actual_link = $actual_base_link . $actual_controller; ?>
                                     <?php $url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                                    $actual_link_report=str_replace("/","--",$url);
                                     ?>

                                    <span style="margin-top: 5px;">
                                        <i class="fa fa-tags fa-flip-horizontal" aria-hidden="true"></i> <span
                                            style="background: lightgray; padding: 6px 12px; font-size: 12px; border-radius: 25px; text-transform: capitalize;">
                                                                                                            <?php
                                                                                                            if ($row->type == '') {
                                                                                                                echo 'Image';
                                                                                                            } else{

                                                                                                            }

                                                                                                            ?></span>
                                      <i class="fa fa-tags fa-flip-horizontal" aria-hidden="true"></i> <span
                                            style="background: lightgray; padding: 6px 12px; font-size: 12px; border-radius: 25px; text-transform: capitalize;"><?php echo $row->LOOKUP_DATA_NAME; ?></span></span>
                                            <hr style="margin: 0.5em; margin-top: 20px;">
                                    <p class="pull-right">
                                        <i style="color: #012F49" class="fa fa-share-alt"
                                           aria-hidden="true"></i>
                                        <a class="facebook customer share"
                                           style="color: #4267b2;"
                                           href="http://www.facebook.com/sharer/sharer.php?u=<?php echo $actual_link . '/inclusiveBdView/' . $row->id; ?>"
                                           title="Facebook share" target="_blank"><i
                                                class="fa fa fa-facebook fa-lg"
                                                style="margin: 0 8px;"
                                                aria-hidden="true"></i></a>
                                        <a class="twitter customer share"
                                           style="color: #1b95e0;"
                                           href="http://twitter.com/share?url=<?php echo $actual_link . '/inclusiveBdView/' . $row->id; ?>&amp;hashtags=youthopiabangla"
                                           title="Twitter share" target="_blank"><i
                                                class="fa fa fa-twitter fa-lg"
                                                style="margin: 0 8px;"
                                                aria-hidden="true"></i></a>
                                        <a class="google_plus customer share"
                                           style="color: #db4437;"
                                           href="https://plus.google.com/share?url=<?php echo $actual_link . '/inclusiveBdView/' . $row->id; ?>"
                                           title="Google Plus Share" target="_blank"><i
                                                class="fa fa fa-google-plus fa-lg"
                                                style="margin: 0 8px;" aria-hidden="true"></i></a>
                                        <a class="email modalLink" data-modal-size="modal-md"
                                           style="color: #79B04A;"
                                           href="<?php echo site_url().'/InclusiveBd/report_problem/'.$row->id.'/'.$linkShare.'/'.$actual_link_report  ?>"
                                           title="Share"><i
                                                class="fa fa-envelope fa-lg"
                                                style="margin: 0 8px;" aria-hidden="true"></i></a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php endforeach; ?>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <?php echo $this->pagination->create_links(); ?>
                    </div>
                </div>


            </div>
        </section>
    </div>


</div>