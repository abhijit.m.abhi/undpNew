<style media="screen">
    #easyPaginate, #easyPaginateBlog {
        width: 100%;
    }

    .easyPaginateNav {
        clear: both;
        margin-top: 20px;
        text-align: center;
    }

    .easyPaginateNav a {
        padding: 8px;
    }

    .easyPaginateNav a.current {
        font-weight: bold;
        text-decoration: underline;
        font-size: 24px;
    }

    #easyPaginate, #easyPaginateBlog {
        display: -webkit-flex; /* Safari */
        display: flex;
        flex-wrap: wrap;
    }
</style>
<style>
    .panelD .panel-heading, .panelD .panel-footer {
        background-color: #fff !important;
    }

    .panel-image img.panel-image-preview {
        width: 100%;
        border-radius: 4px 4px 0px 0px;
    }

    .panel-heading ~ .panel-image img.panel-image-preview {
        border-radius: 0px;
    }

    .panel-heading .list-inline {
        margin: 0px 0px 0px -5px !important;
    }

    .panel-body {
        display: block;
        padding: 2px 5px 0px;
    }

    .panel-body blockquote {
        margin: 5px 0 5px;
    }

    .panel-image ~ .panel-footer a {
        padding: 0px 10px;
        color: rgb(100, 100, 100);
    }

    .panel-image ~ .panel-footer span {
        color: rgb(100, 100, 100);
    }

    .panel-footer .list-inline {
        margin: 0 0 0 -15px !important;
    }

    .panel-image.hide-panel-body ~ .panel-body {
        height: 0px;
        padding: 0px;
    }

    /*==========  Mobile First Method  ==========*/
    /* Custom, iPhone Retina */
    @media only screen and (max-width: 320px) {
        .level-line-up {
            position: relative;
            top: -4px;
        }
    }
</style>

<style>
    .new_rz_box {
        display: block;
        margin-top: 40px;
        padding: 10px 0;
        border-bottom: 1px solid #ccc;
    }

    .new_rz_box h6 {
        color: #446678;
        text-transform: uppercase;
    }
.new_rz_box2 input {
        border: 1px solid #d9d9d9 !important;
    }
    .new_rz_box h5 {
        color: #446678;
        text-transform: uppercase;
    }

    .new_rz_box h3 {
        color: #446678;
        text-transform: uppercase;
    }

    .new_rz_box h4 {
        color: #333;
    }

    .new_rz_box2 {
        display: block;
        padding: 10px 0;
    }

    .new_rz_box2 h3 {
        color: #446678;
        margin-bottom: 10px;
    }

    .new_rz_box2 input {
        border: 1px solid #d9d9d9;
        width: 100%;
        padding: 5px;
    }

    .new_rz_box2 textarea {
        border: 1px solid #d9d9d9;
        width: 100%;
        padding: 10px 20px;
    }

    .new_rz_box2 select {
        border: 1px solid #d9d9d9;
        width: 100%;
        padding: 5px 10px;
    }

    .row > div {
        display: inline-block;

    }

    .rz_button3 {
        margin-top: 40px;
        border: 0;
        background-color: #012F49;
        color: #FFF;
        font-size: 25px;
        text-align: center;
        padding: 10px 20px;
        text-transform: uppercase;
        border-radius: 10px;
    }

    .rz_button3:hover {
        color: #FFF;
    }
</style>

<style>
    .event_calender_slider {
        margin-bottom: 60px;
    }

    .rz_button {
        background-color: #012F49;
        color: #FFF;
        text-align: center;
        display: block;
        margin-top: 10px;
        border-radius: 3px;
    }

    .rz_button2 {
        background-color: #e63433;
        color: #FFF;
        font-size: 15px;
        text-align: center;
        padding: 6px 12px;
        text-transform: uppercase;
        border-radius: 25px;
    }

    .rz_button2:hover {
        color: #FFF;
    }

    .event_calender_slider:after {
        color: #FFFFFF;
        content: "PASSESD EVENTS";
        font-family: 'ralewaysemibold';
        font-size: 15px;
        left: 0;
        line-height: 1.3;
        position: absolute;
        text-align: center;
        top: 35%;
        width: 106px;
    }
</style>

<style type="text/css">
    .thumb {
        width: 50px;
        height: 50px;
    }

    #list {
        position: absolute;
        top: 0px;
    }

    #list div {
        float: left;
        margin-right: 10px;
    }

       .rz_button3 {
        background-color: #e63433;
        color: #FFF;
        font-size: 15px;
        text-align: center;
        padding: 6px 12px;
        text-transform: uppercase;
        border-radius: 25px;
    }

    .rz_button3:hover {
       
        color: #FFF;
    }
    .btn.btn-default1 {
    background-color: #d43f3a;
    color: white;
    border-color: #d43f3a;
    border-radius: 25px;
}
</style>


<div class="others_page_wrp">
    <section class="org_map_wrp">
        <div class="container">
            <br>
            <div class="col-lg-12 text-center">
                <form action="<?php echo site_url() . '/InclusiveBd/searchInclusiveBdPost' ?>" class="" method="post">
                    <!--                --><?php //$categoryList = array('Competitions', 'Conferences', 'Exchange-programs', 'Fellowships', 'Internships', 'Scholarships', 'Workshops', 'Miscellaneous', 'Training'); ?>
                    <input type="hidden" name="user_id" value="<?php echo $this->session->userdata['user_logged_user']['userId']; ?>"/>
                    <div class="row new_rz_box2">
                        <div class="col-lg-12 text-center">
                              <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">

                               <select name="LOOKUP_DATA_ID">
                                    <option value="">All</option>
                                     
                                    <option value="1" <?php echo ($category_name== 'Youth Actions') ? 'selected="selected"' : '' ?> >
                                        Youth Actions
                                    </option>
                                    <option value="2" <?php echo ($category_name == 'Opportunities') ? 'selected="selected"' : '' ?>>
                                        Opportunities
                                    </option>
                                    <option value="3" <?php echo ($category_name == 'Area Based News') ? 'selected="selected"' : '' ?>>
                                        Area Based News
                                    </option>
                                     
                                </select>
                             <!--    <select name="blog_type">
                                    <option value="">All</option>
                                    <option value="image" <?php echo ($blog_ty == 'image') ? 'selected="selected"' : '' ?> >
                                        Image
                                    </option>
                                    <option value="audio" <?php echo ($blog_ty == 'audio') ? 'selected="selected"' : '' ?>>
                                        Audio
                                    </option>
                                    <option value="video" <?php echo ($blog_ty == 'video') ? 'selected="selected"' : '' ?>>
                                        Video
                                    </option>
                                </select> -->
                            </div>

                          <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                                <select name="order">
                                    <option value="">Select order</option>
                                    <option value="desc" <?php echo ($or == 'desc') ? 'selected="selected"' : '' ?>>
                                        Newest to oldest
                                    </option>
                                    <option value="asc" <?php echo ($or == 'asc') ? 'selected="selected"' : '' ?>>Oldest
                                        to newest
                                    </option>
                                    <option value="today" <?php echo ($or == 'today') ? 'selected="selected"' : '' ?>>
                                        Today's post
                                    </option>
                                    <option value="week" <?php echo ($or == 'week') ? 'selected="selected"' : '' ?>>This
                                        week post
                                    </option>
                                </select>
                            </div>


                   <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">
                                <input list="search" type="text" class="" placeholder="Search for..."
                                       name="search"
                                       style="min-width: 120px;     border-radius: 6px; margin: 0 3px 0 7px;   padding: 5px;">

                                <datalist id="search">
                                    <?php foreach ($inclusive_search_list as $i_list) : ?>
                                    <option value="<?php echo $i_list->title; ?>">
                                        <?php endforeach; ?>
                                </datalist>

                            </div>


                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                <input type="submit" class="btn btn-default btn-block search-btn" type="button" name="search_btn"
                                       value="Search">

                            </div>
                           <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">

                               <?php
                           $portalUser=$this->session->userdata('user_logged_user');
                          //echo '<pre>';print_r($session_info);exit;
                           ?>
                          <?php
                           if($this->session->userdata('user_logged_user')){
                            ?>
                              <div class="dropdown">
                                <button class="btn btn-default1 btn-block dropdown-toggle" type="button" data-toggle="dropdown">Create
                                <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                  <li><a href="<?php echo site_url();?>/InclusiveBd/createInclusivebd/1">YOUTH ACTION</a></li>
                                  <li><a href="<?php echo site_url();?>/InclusiveBd/createInclusivebd/2">OPPORTUNITY</a></li>
                                  <li><a href="<?php echo site_url();?>/InclusiveBd/createInclusivebd/3">NEWS</a></li>
                                </ul>
                              </div>

                             <?php
                            }else{ ?>

                                  <?php
                            }

                           ?>

                               
                                      <!--   <a class="btn btn-default1 btn-block" type="button" href="<?php echo site_url();?>/InclusiveBd/createInclusivebd">Create</a> -->

                            </div>


                        </div>
                    </div>
                </form>
            </div>

            <?php


            if (isset($this->session->userdata['user_logged_user']['userId'])) {
                $id = $this->session->userdata['user_logged_user']['userId'];
            } else {
                $id = '';
            }
            ?>

            <p style="text-align: center;margin: 10px;">
            <hr>
            <hr>
            </p>


            <div class="row">

                <?php
                foreach ($incliusiveBds as $incliusiveBd) {
                    $imgName = explode('___', $incliusiveBd->images);
                    $imgName = $imgName[0];
                    $postTitle = $incliusiveBd->title;
                    if (strlen($postTitle) > 80) {
                        // truncate string
                        $postTitle = substr($postTitle, 0, 80);
                        // make sure it ends in a word so assassinate doesn't become ass...
                        $postTitle = substr($postTitle, 0, strrpos($postTitle, ' ')) . '....';
                    }
                    ?>
                  <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <a href="<?php echo site_url().'/InclusiveBd/inclusiveBdView/'.$incliusiveBd->id; ?>" target="_blank">
              <!--                                <a href="#" data-modal-external-file="event_view.php?event_id=-->
              <?php //echo $event['id'];
              ?><!--" data-target="modal-submit">-->
              <div class="panel panelD" style="border: 1px solid #ddd">
                <div class="thumbnail panel-image embed-responsive embed-responsive-16by9" style="margin-bottom: 2px">
                  <?php if ($incliusiveBd->type == ''): ?>
                       
                     <img style=" border-radius: 4px 4px 0px 0px;border-radius: 0px;"
                    src="<?php echo base_url().'upload/inclusivebd/' . $imgName ?>"
                    alt="<?php echo $incliusiveBd->title; ?>"/>
                  <?php elseif ($incliusiveBd->type == 'News'): ?>
                     <img style="max-height: 200px; width: auto; padding-bottom: 4px;" src="<?php echo base_url(); ?>assets/img/news.jpg" alt="NO PIC"/>

              
                   
                       
                   
                  <?php elseif ($incliusiveBd->type == 'image') : ?>
                    <img style=" border-radius: 4px 4px 0px 0px;border-radius: 0px;"
                    src="<?php echo base_url().'upload/inclusivebd/' . $imgName ?>"
                    alt="<?php echo $incliusiveBd->title; ?>"/>
                <?php else :
                    $url_link = '';
                    if ($incliusiveBd->url_type == 'Youtube') {
                      $url2Embed = str_replace("watch?v=", "embed/", $incliusiveBd->urls);
                      $url_link = str_replace("[URL]", $url2Embed, $incliusiveBd->url_data);
                    } elseif ($incliusiveBd->url_type == 'SoundCloud') {
                      $url_link = str_replace("[URL]", $incliusiveBd->urls, $incliusiveBd->url_data);
                    } elseif ($incliusiveBd->url_type == 'DailyMotion') {
                      $url2Embed = str_replace("https://www.dailymotion.com/video", "https://www.dailymotion.com/embed/video", $incliusiveBd->urls);
                      $url_link = str_replace("[URL]", $url2Embed, $incliusiveBd['url_data']);
                    } elseif ($incliusiveBd->url_type == 'Vimeo') {
                      $url2Embed = str_replace("https://vimeo.com/", "https://player.vimeo.com/video/", $incliusiveBd->urls);
                      $url_link = str_replace("[URL]", $url2Embed, $incliusiveBd->url_data);
                    } else {
                      //echo '<div width="100%" height="258" style="margin-bottom: 0px;"><h1>Something Missing</h1></div>';
                      //echo "";
                    }
                    echo $url_link;
                    endif; ?>
                </div>
                <div class="opportunity_calender_txt">
                  <p style="min-height: 50px; max-height: 50px; overflow-y: hidden"><?php echo $postTitle; ?></p>
                  <p>By
                    <span style="min-height: 20px; max-height: 20px; overflow-y: hidden"><?php echo $incliusiveBd->oname; ?> </span><br/><span
                    class="event_dates"> <?php echo date('d M, Y', strtotime($incliusiveBd->post_date)); ?></span><br>
                  </p>

                </p>

              </div>
              <p style="overflow-y: hidden"> <span style="float: left; margin: 6px 2px;">
                <i class="fa fa-tags fa-flip-horizontal" aria-hidden="true"></i>
                <span style="background: lightgray; padding: 6px 12px; font-size: 13px; border-radius: 25px;"><?php echo $incliusiveBd->LOOKUP_DATA_NAME; ?></span></span>
              </p>
              <p style="overflow-y: hidden">Status: <?php
              if ($incliusiveBd->active_status=='Y') {
                echo '<span style="color:orange;font-size:12px;">Approved</span>';
              } elseif ($incliusiveBd->active_status=='N') {
                echo '<span style="color:green;">Waiting for approval </span>';
              }
              elseif ($incliusiveBd->active_status=='R') {
                echo '<span style="color:red;">Rejected</span>';
              } else {
                echo " ";
              }
              ?>

              <p><a class="rz_button" href="<?php echo site_url().'/InclusiveBd/editInclusiveBdPost/'.$incliusiveBd->id; ?>">Edit</a>
              </p>
              <p><a class="rz_button" onClick="return confirm('Are you sure you want to delete?')"
                href="<?php echo site_url().'/InclusiveBd/deleteInclusiveBdPost/'.$incliusiveBd->id; ?>">Delete</a>
              </p>
            </div>
          </a>
        </div>
                    <?php
                }
                ?>
            </div>

        </div>
    </section>
</div>


<script>
    function reactionFunction(type, blog_id, user_id = null) {

        if (!user_id) {
            alert('Please login to perform this task');
            return false;
        }
        $.ajax({
            url: 'calSearch',
            type: 'POST',
            data: {
                blog_type: type,
                user_id: user_id,
                blog_id: blog_id,
                action: 'blogInterest'
            }
        }).done(function (response) {
            var res = $.parseJSON(response);
            console.log(res);

//            console.log( Object.keys(res).length );


            $('#like-count' + res.blog_id).html(0);
            $('#smiley-count' + res.blog_id).html(0);
            $('#love-count' + res.blog_id).html(0);
            $('#wow-count' + res.blog_id).html(0);

            res.count.forEach(function (counts) {
                if (counts.type == '1') {
                    $('#like-count' + res.blog_id).html(counts.interest_count);
                }
                else if (counts.type == '2') {
                    $('#smiley-count' + res.blog_id).html(counts.interest_count);
                }
                else if (counts.type == '3') {
                    $('#love-count' + res.blog_id).html(counts.interest_count);
                }
                else if (counts.type == '4') {
                    $('#wow-count' + res.blog_id).html(counts.interest_count);
                }
                else {
                }
            });
            if (!res.ack) {
                alert(res.msg);
            }
        });
    }
</script>