<div id="area_based_news" class="">
    <!-- <h3>Upcomming Events</h3> -->

    <div class="others_page_wrp">
        <section class="org_map_wrp">
            <div class="container">
                <br>
                <div class="col-lg-12 text-center">
                    <form action="<?php echo site_url() . 'inclusiveBd/searchInclusivePost' ?>" class="" method="post">

                        <div class="row new_rz_box2">
                            <div class="col-lg-12 text-center">
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">

                                    <select class="form-control" name="LOOKUP_DATA_ID" id="LOOKUP_DATA_ID"
                                            data-tags="true" data-placeholder="Select Category" data-allow-clear="true">
                                        <option value="">Select Category</option>
                                        <?php foreach($category as $categorys) : ?>
                                            <option value="<?php echo $categorys->LOOKUP_DATA_ID; ?>" <?php echo ($cate == $categorys->LOOKUP_DATA_ID) ? 'selected' : '' ?> ><?php echo $categorys->LOOKUP_DATA_NAME; ?></option>
                                        <?php endforeach; ?>

                                    </select>

                                </div>

                                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                                    <select name="order">
                                        <option value="">Select order</option>
                                        <option value="desc" <?php echo ($or == 'desc') ? 'selected' : '' ?>>Newest to oldest</option>
                                        <option value="asc" <?php echo ($or == 'asc') ? 'selected' : '' ?>>Oldest to newest</option>
                                        <option value="today" <?php echo ($or == 'today') ? 'selected' : '' ?>>Today's post</option>
                                        <option value="week" <?php echo ($or == 'week') ? 'selected' : '' ?>>This week post</option>
                                    </select>
                                </div>

                                <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">
                                    <input list="search" type="text" class="" placeholder="Search for..."
                                           name="search" value="<?php echo (!empty($src)) ? $src : ''; ?>"
                                           style="min-width: 120px;     border-radius: 6px; margin: 0 3px 0 7px;   padding: 5px;">

                                    <datalist id="search">
                                        <?php foreach ($inclusive_search_list as $i_list) : ?>
                                        <option value="<?php echo $i_list->title; ?>">
                                            <?php endforeach; ?>
                                    </datalist>

                                </div>


                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                    <input type="submit" class="btn btn-default btn-block" type="button" name="search_btn"
                                           value="Search">

                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                    <?php
                                    $portalUser=$this->session->userdata('user_logged_user');
                                    //echo '<pre>';print_r($session_info);exit;
                                    ?>
                                    <?php
                                    if($this->session->userdata('user_logged_user')){
                                        ?>
                                        <div class="dropdown">
                                            <button class="btn btn-default1 btn-block dropdown-toggle" type="button" data-toggle="dropdown">Create
                                                <span class="caret"></span></button>
                                            <ul class="dropdown-menu">
                                                <li><a href="<?php echo site_url();?>inclusiveBd/createInclusivebd/1">YOUTH ACTION</a></li>
                                                <li><a href="<?php echo site_url();?>inclusiveBd/createInclusivebd/2">OPPORTUNITY</a></li>
                                                <li><a href="<?php echo site_url();?>inclusiveBd/createInclusivebd/3">NEWS</a></li>
                                            </ul>
                                        </div>

                                        <?php
                                    }else{ ?>

                                        <?php
                                    }

                                    ?>


                                    <!-- <a class="btn btn-default1 btn-block" type="button" href="<?php echo site_url();?>/InclusiveBd/createInclusivebd">Create</a> -->

                                </div>


                            </div>
                        </div>
                    </form>
                </div>

                <?php


                if (isset($this->session->userdata['user_logged_user']['userId'])) {
                    $id = $this->session->userdata['user_logged_user']['userId'];
                     $linkShare = $id;
                } else {
                    $id = '';
                    $linkShare = 0;
                }
                ?>

                <p style="text-align: center;margin: 10px;">
                <hr>
                <hr>
                </p>

                <?php foreach ($inclusive_posts_y as $row) : ?>


                    <?php
                    $interests = $this->inclusivebd_model->getInclusiveInterest2($row->id, $id);
                    $inter = $this->inclusivebd_model->getInclusiveInterest($row->id);


                    $likeReaction = 0;
                    $smileReaction = 0;
                    $loveReaction = 0;
                    $wowReaction = 0;

                    $action = '';
                    $action_name = '';
                    ?>

                    <?php foreach ($interests as $interest) { ?>
                        <?php
                        if ($interest->type == '1') {
                            $likeReaction = $interest->interest_count;
                            $action = 'like';
                            $action_name = 'LIKE';
                        } elseif ($interest->type == '2') {
                            $smileReaction = $interest->interest_count;
                            $action = 'haha';
                            $action_name = 'HAHA';
                        } elseif ($interest->type == '3') {
                            $loveReaction = $interest->interest_count;
                            $action = 'love';
                            $action_name = 'LOVE';
                        } elseif ($interest->type == '4') {
                            $wowReaction = $interest->interest_count;
                            $action = 'wow';
                            $action_name = 'WOW';
                        }

                        ?>

                    <?php } ?>

                    <?php foreach ($inter as $intere) : ?>
                        <?php
                        if ($intere->type == '1') {
                            $likeReaction = $intere->interest_count;
                        } elseif ($intere->type == '2') {
                            $smileReaction = $intere->interest_count;
                        } elseif ($intere->type == '3') {
                            $loveReaction = $intere->interest_count;
                        } elseif ($intere->type == '4') {
                            $wowReaction = $intere->interest_count;
                        }
                        ?>
                    <?php endforeach; ?>


                    <div class="col-xs-12 col-sm-6 col-md-4 show_block_elem">
                        <div class="panel panelD" style="border: 1px solid #ddd">
                                <a href="<?php echo site_url() . '/InclusiveBd/inclusiveBdView/' .

                                        $row->id; ?>"
                                       target="_blank">
                            <div class="thumbnail panel-image embed-responsive embed-responsive-16by9 frame"
                                 style="margin-bottom: 2px">
                             <img style="" src="<?php echo base_url(); ?>assets/img/news.jpg" alt="NO PIC"/>
                            </div></a>

                            <div class="panel-body">
                                <a style="color: #E43533;"
                                   href="<?php echo site_url() . 'inclusiveBd/inclusiveBdView/' . $row->id; ?>"
                                   target="_blank"><h3
                                        style="color: #E43533; text-align: center; text-transform: uppercase;"><?php echo $row->title; ?></h3>
                                </a>
                 <!--                 <div style="text-overflow: ellipsis;word-wrap: break-word;overflow: hidden;height:4.4em; max-height: 4.4em;line-height: 1.4em;-webkit-line-clamp: 3;display: -webkit-box;-webkit-box-orient: vertical;">   
                              <p style="text-align: justify;"><?php echo htmlspecialchars_decode(html_entity_decode(substr($row->description,0,550))); ?></p></div> -->
                                <h4 style="color: #78AE48">Posted
                                    on: <?php echo date("M d Y", strtotime($row->post_date)) ?></h4>
                                <h4 style="color: #78AE48">Posted
                                    by: <?php echo $row->fname; ?></h4>
                                <hr style="margin: 0.5em;">
                                <p>
                                    <!--    <button type="button" class="btn btn-primary"
                                                                href="<?php echo site_url() . 'inclusiveBd/inclusiveBdView/' . $row->id; ?>"
                                                                style="margin-bottom: 4px; margin-right: 14px;"

                                                        >Read more
                                                        </button> -->
                                    <a href="<?php echo site_url() . 'inclusiveBd/inclusiveBdView/' . $row->id; ?>"
                                       target="_blank" class="btn btn-primary"  style="margin-bottom: 4px; margin-right: 14px;">Read more</a>
                                    <span style="float: right; margin: 6px 2px;">
                                                        <i class="fa fa-tags fa-flip-horizontal" aria-hidden="true"></i>
                                                        <span style="background: lightgray; padding: 6px 12px; font-size: 14px; border-radius: 25px;"><?php echo $row->LOOKUP_DATA_NAME; ?></span></span>
                                </p>
                            </div>
                            <hr style="margin: 0.5em;">

                            <div class="panel-body clearfix" style="padding:3px">
                                <div class="list-inline clearfix">
                                    <?php $actual_base_link = base_url(); ?>
                                    <?php $actual_controller = $this->uri->segment('1'); ?>
                                    <?php $actual_link = $actual_base_link . $actual_controller; ?>
                                     <?php $url= $url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                                    $actual_link_report=str_replace("/","--",$url);
                                     ?>

                                    <!--      <span style="margin-top: 5px;">
                                        <i class="fa fa-tags fa-flip-horizontal" aria-hidden="true"></i> <span
                                style="background: lightgray; padding: 6px 12px; font-size: 12px; border-radius: 25px; text-transform: capitalize;"><?php echo $row->type; ?></span>
                                      <i class="fa fa-tags fa-flip-horizontal" aria-hidden="true"></i> <span
                                style="background: lightgray; padding: 6px 12px; font-size: 12px; border-radius: 25px; text-transform: capitalize;"><?php // echo $row->LOOKUP_DATA_NAME; ?></span></span> -->
                                    <p class="pull-right">
                                        <i style="color: #012F49" class="fa fa-share-alt"
                                           aria-hidden="true"></i>
                                        <a class="facebook customer share"
                                           style="color: #4267b2;"
                                           href="http://www.facebook.com/sharer/sharer.php?u=<?php echo $actual_link . '/inclusiveBdView/' . $row->id; ?>"
                                           title="Facebook share" target="_blank"><i
                                                class="fa fa fa-facebook fa-lg"
                                                style="margin: 0 8px;"
                                                aria-hidden="true"></i></a>
                                        <a class="twitter customer share"
                                           style="color: #1b95e0;"
                                           href="http://twitter.com/share?url=<?php echo $actual_link . '/inclusiveBdView/' . $row->id; ?>&amp;hashtags=youthopiabangla"
                                           title="Twitter share" target="_blank"><i
                                                class="fa fa fa-twitter fa-lg"
                                                style="margin: 0 8px;"
                                                aria-hidden="true"></i></a>
                                        <a class="google_plus customer share"
                                           style="color: #db4437;"
                                           href="https://plus.google.com/share?url=<?php echo $actual_link . '/inclusiveBdView/' . $row->id; ?>"
                                           title="Google Plus Share" target="_blank"><i
                                                class="fa fa fa-google-plus fa-lg"
                                                style="margin: 0 8px;" aria-hidden="true"></i></a>
                                        <a class="email modalLink" data-modal-size="modal-md"
                                           style="color: #79B04A;"
                                           href="<?php echo site_url().'/InclusiveBd/report_problem/'.$row->id.'/'.$linkShare.'/'.$actual_link_report  ?>"
                                           title="Share"><i
                                                class="fa fa-envelope fa-lg"
                                                style="margin: 0 8px;" aria-hidden="true"></i></a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php endforeach; ?>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <?php echo $this->pagination->create_links(); ?>
                    </div>
                </div>


            </div>
        </section>
    </div>


</div>