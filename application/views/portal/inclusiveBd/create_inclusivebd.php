<style>
    .new_rz_box {
        display: block;
        margin-top: 40px;
        padding: 10px 0;
        border-bottom: 1px solid #ccc;
    }

    .new_rz_box h6 {
        color: #446678;
        text-transform: uppercase;
    }

    .new_rz_box h5 {
        color: #446678;
        text-transform: uppercase;
    }
.new_rz_box2 input {
        border: 1px solid #d9d9d9 !important;
    }
    .new_rz_box h3 {
        color: #446678;
        text-transform: uppercase;
    }

    .new_rz_box h4 {
        color: #333;
    }

    .new_rz_box2 {
        display: block;
        padding: 10px 0;
    }

    .new_rz_box2 h3 {
        color: #446678;
        margin-bottom: 10px;
    }

    .new_rz_box2 input {
        border: 1px solid #d9d9d9;
        width: 100%;
        padding: 5px;
    }

    .new_rz_box2 textarea {
        border: 1px solid #d9d9d9;
        width: 100%;
        padding: 10px 20px;
    }

    .new_rz_box2 select {
        border: 1px solid #d9d9d9;
        width: 100%;
        padding: 5px 10px;
    }

    .rz_button3 {
        margin-top: 40px;
        border: 0;
        background-color: #012F49;
        color: #FFF;
        font-size: 25px;
        text-align: center;
        padding: 10px 20px;
        text-transform: uppercase;
        border-radius: 10px;
    }

    .rz_button3:hover {
        color: #FFF;
    }
</style>
<style type="text/css">
    .thumb {
        width: 50px;
        height: 50px;
    }

    #list {
        position: absolute;
        top: 0px;
    }

    #list div {
        float: left;
        margin-right: 10px;
    }

</style>
  <div class="row">
    <div class="col-xs-12 col-sm-5">
      <img src="<?php echo base_url(); ?>portalAssets/img/designbanner.png"
                                        class="img-responsive"/>
    </div>
     <div class="col-xs-12 col-sm-2">
     <h1 style="font-size:26px;color: red;" align="center">My Inclusive Bangladesh</h1>

    </div>
    <div class="col-xs-12 col-sm-5">
      <img src="<?php echo base_url(); ?>portalAssets/img/designbanner.png"
                                         class="img-responsive"/>
    </div>
  </div>


<div class="others_page_wrp">
    <section class="org_map_wrp">
        <div class="container">

            <form class="box" method="post" action="<?php echo site_url().'inclusiveBd/createNewInclusiveBdPost'?>" enctype="multipart/form-data">

            <input type="hidden" name="user_id" value="<?php echo $this->session->userdata['user_logged_user']['userId']; ?>"/>
                 <?php if ($cat_id === '3'): ?>
                 <input type="hidden" name="type_value" value="type_value"/>
                  <?php endif; ?>
                   <?php if ($cat_id === '2'): ?>
                 <input type="hidden" name="type_value_op" value="type_value_op"/>
                  <?php endif; ?>
        <!--         <div class="row new_rz_box">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <h5>Name of Organization *</h5>
                        <h4><?php echo (!empty($organizationData->title)) ? $organizationData->title : ''; ?></h4>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <h6>Full Address *</h6>
                        <h4><?php echo (!empty($organizationData->location)) ? $organizationData->location : ''; ?></h4>
                    </div>
                </div> -->

                <div class="row new_rz_box2">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h3>Post Title *</h3>
                        <input type="text" name="title" value="" placeholder="Post title" maxlength="100" required/>
                    </div>
                </div>

                  <div class="row new_rz_box2 dis">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h3>Category</h3>
<!--  <select name="LOOKUP_DATA_ID">
                                    <option value="">All</option>
                                     
                                    <option value="1" <?php echo ($category_name== 'Youth Actions') ? 'selected="selected"' : '' ?> >
                                        Youth Actions
                                    </option>
                                    <option value="2" <?php echo ($category_name == 'Opportunities') ? 'selected="selected"' : '' ?>>
                                        Opportunities
                                    </option>
                                    <option value="3" <?php echo ($category_name == 'Area Based News') ? 'selected="selected"' : '' ?>>
                                        Area Based News
                                    </option>
                                     
                                </select> -->
                 <?php
                   
                     $options = array('' => '--Select Category--');
                     foreach ($category as $category) {
                     $options["$category->LOOKUP_DATA_ID"] = $category->LOOKUP_DATA_NAME;
                     }
                     $category = set_value('LOOKUP_DATA_ID');
                     echo form_dropdown('LOOKUP_DATA_ID', $options,$cat_id, 'id="category" style="width:100%;" class="new_rz_box2 select" data-placeholder="Select category" ');
                     ?>

                    </div>
                </div>
              <?php if ($cat_id === '2'): ?>
                 <div class="row new_rz_box2">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h3>Gallery</h3>
                        <input type="file" name="files" limit="5">
                    </div>
                </div>
                <?php endif; ?>
            <?php if ($cat_id === '1'): ?>

                <div class="row new_rz_box2">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h3>Post Type *</h3>
                        <div class="btn-group blog-type" data-toggle="buttons">
                            <label class="btn btn-info">
                                <input type="radio" id="blog-image" name="blog_type" value="image" autocomplete="off"
                                       required> Image
                            </label>
                            <label class="btn btn-info">
                                <input type="radio" id="blog-audio" name="blog_type" value="audio" autocomplete="off"
                                       required> Audio
                            </label>
                            <label class="btn btn-info">
                                <input type="radio" id="blog-video" name="blog_type" value="video" autocomplete="off"
                                       required> Video
                            </label>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
                <div id="blk-image" class="toHide" style="display:none; padding: 0px; margin: 0px;">
                    <div class="row new_rz_box2">
                        <div class="col-xs-12">
                            <h3>Gallery</h3>
                            <input type="file" name="files" limit="5">
                        </div>
                    </div>
                </div>
                <div id="blk-audio" class="toHide" style="display:none; padding: 0px; margin: 0px;">
                    <div class="row new_rz_box2">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <h3>Soundcloud URL *</h3>
                            <input type="text" name="audio_url" value=""
                                   placeholder="Please provide Soundcloud URL address. Otherwise it will not work."/>
                        </div>
                    </div>
                </div>
                <div id="blk-video" class="toHide" style="display:none; padding: 0px; margin: 0px;">
                    <div class="row new_rz_box2">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <h3>Video URL Type *</h3>
                            <div class="btn-group video-url-type" data-toggle="buttons">
                                <?php foreach ($url_types as $url_type) {
                                    if ($url_type->id == 2) {
                                    } else {
                                        echo '<label class="btn btn-warning">
                                                <input type="radio" id="url-' . $url_type->name . '" text="' . $url_type->name . '" name="video_url_type" value="' . $url_type->id . '" autocomplete="off"> ' . $url_type->name . '
                                                </label>';
                                    }
                                } ?>
                            </div>
                        </div>
                    </div>
                    <div class="row new_rz_box2">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <h3><span id="video-url-name">Video</span> URL *</h3>
                            <input type="text" name="video_url" value="" placeholder="Video URL address"/>
                        </div>
                    </div>
                </div>

                <div class="row new_rz_box2">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h3>Published Date *</h3>
                        <!-- <p><?php echo date("Y-m-d"); ?></p> -->
                        <p>
                            <input type="text" value="<?php echo date("d-m-Y"); ?>" name="post_date" id="post_date"
                                   placeholder="Start" readonly style="background-color: #d9d9d9;">
                        </p>
                    </div>
                </div>
                <div class="row new_rz_box2">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h3>Description *</h3>
                        <textarea class="redactor" name="description" id="description" rows="3" required></textarea>
                    </div>
                </div>



                <p style="text-align:center; margin-bottom:60px;">
                    <button class="rz_button3" type="submit" name="submitBlog">Publish</button>
                </p>


            </form>

        </div>
    </section>
</div>


<script src="<?php echo base_url();?>old/assets/vendors/ckeditor2/ckeditor.js"></script>
<script src="<?php echo base_url();?>old/assets/vendors/ckeditor2/samples/js/sample.js"></script>
<script> $('select option:not(:selected)').each(function(){ $(this).attr('disabled', 'disabled'); }); </script>
 <script>
// $(".row new_rz_box2 dis").attr("disabled",true);
// </script>
<script>
//    $(function () {
        $('.blog-type input[type=radio]').on('change', function () {

//            alert($(this).val());

            $('.toHide').hide();
            $("#blk-" + $(this).val()).fadeIn(1000);
        });


        $('.video-url-type input[type=radio]').on('change', function () {
            $("#video-url-name").text($(this).attr("text"));
        });
//    });
</script>

<script>
    //initSample();
</script>
<script>
    function handleFileSelect(evt) {
        var files = evt.target.files; // FileList object

        // Loop through the FileList and render image files as thumbnails.
        for (var i = 0, f; f = files[i]; i++) {

            // Only process image files.
            if (!f.type.match('image.*')) {
                continue;
            }

            var reader = new FileReader();

            // Closure to capture the file information.
            reader.onload = (function (theFile) {
                return function (e) {
                    // Render thumbnail.
                    var span = document.createElement('div');
                    span.setAttribute('class', 'img_wrpv');
                    span.innerHTML = ['<a href="#" style="color:red; font-size:18px; position: relative; z-index: 200;" class="img_revg">x</a><img width="50px" height="50px" class="thumb" src="', e.target.result,
                        '" title="', escape(theFile.name), '"/>'].join('');
                    document.getElementById('list').insertBefore(span, null);
                };
            })(f);

            // Read in the image file as a data URL.
            reader.readAsDataURL(f);

            //remove image
            $('.img_revg').on('click', function (e) {
                e.preventDefault();
                var parent = $(this).parent();
                parent.remove();
            });

        }
    }

    document.getElementById('galleryfiles').addEventListener('change', handleFileSelect, true);
</script>