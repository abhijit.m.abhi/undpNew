<style media="screen">
    #easyPaginate, #easyPaginateBlog {
        width: 100%;
    }

    .easyPaginateNav {
        clear: both;
        margin-top: 20px;
        text-align: center;
    }

    .easyPaginateNav a {
        padding: 8px;
    }

    .easyPaginateNav a.current {
        font-weight: bold;
        text-decoration: underline;
        font-size: 24px;
    }

    #easyPaginate, #easyPaginateBlog {
        display: -webkit-flex; /* Safari */
        display: flex;
        flex-wrap: wrap;
    }
</style>
<style>
    .panelD .panel-heading, .panelD .panel-footer {
        background-color: #fff !important;
    }

    .panel-image img.panel-image-preview {
        width: 100%;
        border-radius: 4px 4px 0px 0px;
    }

    .panel-heading ~ .panel-image img.panel-image-preview {
        border-radius: 0px;
    }

    .panel-heading .list-inline {
        margin: 0px 0px 0px -5px !important;
    }

    .panel-body {
        display: block;
        padding: 2px 5px 0px;
    }

    .panel-body blockquote {
        margin: 5px 0 5px;
    }

    .panel-image ~ .panel-footer a {
        padding: 0px 10px;
        color: rgb(100, 100, 100);
    }

    .panel-image ~ .panel-footer span {
        color: rgb(100, 100, 100);
    }

    .panel-footer .list-inline {
        margin: 0 0 0 -15px !important;
    }

    .panel-image.hide-panel-body ~ .panel-body {
        height: 0px;
        padding: 0px;
    }

    /*==========  Mobile First Method  ==========*/
    /* Custom, iPhone Retina */
    @media only screen and (max-width: 320px) {
        .level-line-up {
            position: relative;
            top: -4px;
        }
    }
</style>

<style>
    .new_rz_box {
        display: block;
        margin-top: 40px;
        padding: 10px 0;
        border-bottom: 1px solid #ccc;
    }

    .new_rz_box h6 {
        color: #446678;
        text-transform: uppercase;
    }

    .new_rz_box h5 {
        color: #446678;
        text-transform: uppercase;
    }

    .new_rz_box h3 {
        color: #446678;
        text-transform: uppercase;
    }
.new_rz_box2 input {
        border: 1px solid #d9d9d9 !important;
    }
    .new_rz_box h4 {
        color: #333;
    }

    .new_rz_box2 {
        display: block;
        padding: 10px 0;
    }

    .new_rz_box2 h3 {
        color: #446678;
        margin-bottom: 10px;
    }

    .new_rz_box2 input {
        border: 1px solid #d9d9d9;
        width: 100%;
        padding: 5px;
    }

    .new_rz_box2 textarea {
        border: 1px solid #d9d9d9;
        width: 100%;
        padding: 10px 20px;
    }

    .new_rz_box2 select {
        border: 1px solid #d9d9d9;
        width: 100%;
        padding: 5px 10px;
    }

    .row > div {
        display: inline-block;

    }

    .rz_button3 {
        margin-top: 40px;
        border: 0;
        background-color: #012F49;
        color: #FFF;
        font-size: 25px;
        text-align: center;
        padding: 10px 20px;
        text-transform: uppercase;
        border-radius: 10px;
    }

    .rz_button3:hover {
        color: #FFF;
    }
    .btn.btn-default1 {
    background-color: #d43f3a;
    color: white;
    border-color: #d43f3a;
    border-radius: 25px;
}
</style>
<style type="text/css">
    .thumb {
        width: 50px;
        height: 50px;
    }

    #list {
        position: absolute;
        top: 0px;
    }

    #list div {
        float: left;
        margin-right: 10px;
    }
    .nav22>li.active>a, .nav22>li.active>a:focus, .nav22>li.active>a:hover {
        cursor: default;
        border: 4px solid #a5a4a4;
        border-bottom-color: transparent;
        font-weight: bold;
        background: url("<?php echo base_url("portalAssets/img/Designed-button.png") ?>") no-repeat;
        width: 100%;
        background-size: cover;
        color: white;
    }
    .nav22>li>a {
        border: 4px solid transparent;
    }
</style>

  <div class="row">
    <div class="col-sm-5">
      <img src="<?php echo base_url(); ?>portalAssets/img/designbanner.png"
                                       />
    </div>
     <div class="col-sm-2">
     <h1 style="font-size:34px;color: red;" align="center">Inclusive Bangladesh</h1>
     
    </div>
    <div class="col-sm-5">
      <img src="<?php echo base_url(); ?>portalAssets/img/designbanner.png"
                                       />
    </div>
  </div>
 
 <br>

<ul class="nav nav-justified nav22">
  <li  style="text-align: center;" class="active"><a   data-toggle="tab" href="#youth_actions" >Youth Actions</a></li>
  <!-- <li><a data-toggle="tab" href="#EventOngoing">On-Going</a></li> -->
  <li style="text-align: center;"><a data-toggle="tab" href="#opportunities">Opportunities</a></li>

  <li style="text-align: center;"><a data-toggle="tab" href="#area_based_news">Area Based News</a></li>
</ul>

<div class="tab-content">
<div id="youth_actions" class="tab-pane fade in active">
    <!-- <h3>Upcomming Events</h3> -->
                
<div class="others_page_wrp">
    <section class="org_map_wrp">
        <div class="container">
            <br>
            <div class="col-lg-12 text-center">
                <form action="<?php echo site_url() . '/InclusiveBd/searchInclusiveBdAllPost' ?>" class="" method="post">
                    <!--                --><?php //$categoryList = array('Competitions', 'Conferences', 'Exchange-programs', 'Fellowships', 'Internships', 'Scholarships', 'Workshops', 'Miscellaneous', 'Training'); ?>
                      <div class="row new_rz_box2">
                        <div class="col-lg-12 text-center">
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                   
                               <select name="LOOKUP_DATA_ID">
                                    <option value="">All</option>
                                     
                                    <option value="1" <?php echo ($category_name== 'Youth Actions') ? 'selected="selected"' : '' ?> >
                                        Youth Actions
                                    </option>
                                    <option value="2" <?php echo ($category_name == 'Opportunities') ? 'selected="selected"' : '' ?>>
                                        Opportunities
                                    </option>
                                    <option value="3" <?php echo ($category_name == 'Area Based News') ? 'selected="selected"' : '' ?>>
                                        Area Based News
                                    </option>
                                     
                                </select>
                    </div>

                            <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                                <select name="order">
                                    <option value="">Select order</option>
                                    <option value="desc">Newest to oldest</option>
                                    <option value="asc">Oldest to newest</option>
                                    <option value="today">Today's post</option>
                                    <option value="week">This week post</option>
                                </select>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">
                                <input type="text" class="" placeholder="Search for..."
                                       name="search"
                                       style="min-width: 120px;     border-radius: 6px; margin: 0 3px 0 7px;   padding: 5px;">
                            </div>


                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                <input type="submit" class="btn btn-default btn-block search-btn" type="button" name="search_btn"
                                       value="Search">

                            </div>
                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                           <?php
                           $portalUser=$this->session->userdata('user_logged_user');
                          //echo '<pre>';print_r($session_info);exit;
                           ?>
                          <?php
                           if($this->session->userdata('user_logged_user')){
                            ?>
                              <div class="dropdown">
                                <button class="btn btn-default1 btn-block dropdown-toggle" type="button" data-toggle="dropdown">Create
                                <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                  <li><a href="<?php echo site_url();?>/InclusiveBd/createInclusivebd/1">YOUTH ACTION</a></li>
                                  <li><a href="<?php echo site_url();?>/InclusiveBd/createInclusivebd/2">OPPORTUNITY</a></li>
                                  <li><a href="<?php echo site_url();?>/InclusiveBd/createInclusivebd/3">NEWS</a></li>
                                </ul>
                              </div>

                             <?php
                            }else{ ?>

                                  <?php
                            }

                           ?>


                                        <!-- <a class="btn btn-default1 btn-block" type="button" href="<?php echo site_url();?>/InclusiveBd/createInclusivebd">Create</a> -->

                            </div>


                        </div>
                    </div>
                </form>
            </div>

            <?php


            if (isset($this->session->userdata['user_logged_user']['userId'])) {
                $id = $this->session->userdata['user_logged_user']['userId'];
            } else {
                $id = '';
            }
            ?>

            <p style="text-align: center;margin: 10px;">
            <hr>
            <hr>
            </p>

            <?php foreach ($inclusive_posts as $row) : ?>


                <?php
                $interests = $this->inclusivebd_model->getInclusiveInterest($row->id);

                $likeReaction = 0;
                $smileReaction = 0;
                $loveReaction = 0;
                $wowReaction = 0;
                ?>

                <?php foreach ($interests as $interest) { ?>
                    <?php
                    if ($interest->type == '1') {
                        $likeReaction = $interest->interest_count;
                    } elseif ($interest->type == '2') {
                        $smileReaction = $interest->interest_count;
                    } elseif ($interest->type == '3') {
                        $loveReaction = $interest->interest_count;
                    } elseif ($interest->type == '4') {
                        $wowReaction = $interest->interest_count;
                    }

                    ?>

                <?php } ?>


                <div class="col-xs-12 col-sm-6 col-md-4 show_block_elem">
                    <div class="panel panelD" style="border: 1px solid #ddd">
                        <div class="thumbnail panel-image embed-responsive embed-responsive-16by9"
                             style="margin-bottom: 2px">
                            <?php if ($row->type == 'image') { ?>

                                <?php $images = explode('___', $row->images); ?>

                                <a href="<?php echo site_url() . '/InclusiveBd/inclusiveBdView/' .

                                    $row->id; ?>"
                                   target="_blank"><img style="height: 200px; width: auto"
                                                        src="<?php echo base_url() . "upload/inclusivebd/" . $images[0]; ?>"
                                                        alt="<?php echo $row->title; ?>"/></a>
                            <?php } else {
                                $url_link = '';
                                if ($row->url_type == 'Youtube') {
                                    $url2Embed = str_replace("watch?v=", "embed/", $row->urls);
                                    $url_link = str_replace("[URL]", $url2Embed, $row->url_data);
                                } elseif ($row->url_type == 'SoundCloud') {
                                    $url_link = str_replace("[URL]", $row->urls, $row->url_data);
                                } elseif ($row->url_type == 'DailyMotion') {
                                    $url2Embed = str_replace("https://www.dailymotion.com/video", "https://www.dailymotion.com/embed/video", $row->urls);
                                    $url_link = str_replace("[URL]", $url2Embed, $row->url_data);
                                } elseif ($row->url_type == 'Vimeo') {
                                    $url2Embed = str_replace("https://vimeo.com/", "https://player.vimeo.com/video/", $row->urls);
                                    $url_link = str_replace("[URL]", $url2Embed, $row->url_data);
                                } else {
                                    echo '<div width="100%" height="258" style="margin-bottom: 0px;"><h1>Something Missing</h1></div>';
                                }
                                echo $url_link;
                            }
                            ?>
                        </div>
                        <div class="" style="padding: 7px; font-size: 14px;">
                            <button class="btn btn-link" id="like-button<?php echo $row->id; ?>"
                                    onclick="reactionFunction(1, <?php echo $row->id . ', ' . $id ?>)"
                                    style="padding-right: 0; text-decoration: none; background-color: #002f49; border-radius: 10px; padding: 0 5px; color: #ffffff"
                                    title="Like">
                                            <span id="like-count<?php echo $row->id; ?>"
                                                  style="color: #ffffff"><?php echo $likeReaction; ?></span> <img
                                        src="<?php echo base_url(); ?>assets/img2/icon/like.png" aria-hidden="true"
                                        width="12" height="12"
                                        style="margin: 0 4px 2px"/>
                            </button>
                            <button class="btn btn-link" id="smiley-button<?php echo $row->id; ?>"
                                    onclick="reactionFunction(2, <?php echo $row->id . ', ' . $id ?>)"
                                    style="padding-right: 0; text-decoration: none; background-color: #23a797; border-radius: 10px; padding: 0 5px; color: #ffffff"
                                    title="Smiley">
                                            <span id="smiley-count<?php echo $row->id; ?>"
                                                  style="color: #ffffff"><?php echo $smileReaction; ?></span> <img
                                        src="<?php echo base_url(); ?>assets/img2/icon/smile.png" aria-hidden="true"
                                        width="12" height="12"
                                        style="margin: 0 4px 2px"/>
                            </button>
                            <button class="btn btn-link" id="love-button<?php echo $row->id; ?>"
                                    onclick="reactionFunction(3, <?php echo $row->id . ', ' . $id ?>)"
                                    style="padding-right: 0; text-decoration: none; background-color: #e43733; border-radius: 10px; padding: 0 5px; color: #ffffff"
                                    title="Love">
                                            <span id="love-count<?php echo $row->id; ?>"
                                                  style="color: #ffffff"><?php echo $loveReaction; ?></span> <img
                                        src="<?php echo base_url(); ?>assets/img2/icon/heart.png" aria-hidden="true"
                                        width="12" height="12"
                                        style="margin: 0 4px 2px"/>
                            </button>
                            <button class="btn btn-link" id="wow-button<?php echo $row->id; ?>"
                                    onclick="reactionFunction(4, <?php echo $row->id . ', ' . $id ?>)"
                                    style="padding-right: 0; text-decoration: none; background-color: #79ad48; border-radius: 10px; padding: 0 5px; color: #ffffff"
                                    title="Wow">
                                            <span id="wow-count<?php echo $row->id; ?>"
                                                  style="color: #ffffff"><?php echo $wowReaction; ?></span> <img
                                        src="<?php echo base_url(); ?>assets/img2/icon/WOW.png" aria-hidden="true"
                                        width="12" height="12"
                                        style="margin: 0 4px 2px"/>
                            </button>

                        </div>
                        <div class="panel-heading"
                             style=" min-height:100px; max-height: 100px; overflow-y: hidden;">
                            <a style="color: #E43533;"
                               href="<?php echo site_url() . '/InclusiveBd/inclusiveBdView/' . $row->id; ?>"
                               target="_blank"><h3
                                        style="color: #E43533; text-align: center; text-transform: uppercase; min-height: 57px;"><?php echo $row->title; ?></h3>
                            </a>
                        </div>
                        <div class="panel-body">
                            <h4 style="color: #78AE48">Posted
                                on: <?php echo date("M d Y", strtotime($row->post_date)) ?></h4>
                            <h4 style="color: #78AE48">Posted
                                by: <?php echo $row->fname; ?></h4>
                        </div>
                        <hr style="margin: 0.5em;">

                        <div class="panel-body clearfix" style="padding:3px">
                            <div class="list-inline clearfix">

                    <span style="margin-top: 5px;">
                                        <i class="fa fa-tags fa-flip-horizontal" aria-hidden="true"></i> <span
                                style="background: lightgray; padding: 6px 12px; font-size: 12px; border-radius: 25px; text-transform: capitalize;"><?php echo $row->type; ?></span>
                                      <i class="fa fa-tags fa-flip-horizontal" aria-hidden="true"></i> <span
                                style="background: lightgray; padding: 6px 12px; font-size: 12px; border-radius: 25px; text-transform: capitalize;"><?php echo $row->LOOKUP_DATA_NAME; ?></span></span>
                                <p class="pull-right">
                                    <i style="color: #012F49" class="fa fa-share-alt" aria-hidden="true"></i>
                                    <a class="facebook customer share"
                                       style="color: #4267b2;"
                                       href="http://www.facebook.com/sharer.php?u=<?php // echo $actual_link . '/blog_view.php?blog_id=' . $row->id; ?>"
                                       title="Facebook share" target="_blank"><i class="fa fa fa-facebook fa-lg"
                                                                                 style="margin: 0 8px;"
                                                                                 aria-hidden="true"></i></a>
                                    <a class="twitter customer share"
                                       style="color: #1b95e0;"
                                       href="http://twitter.com/share?url=<?php // echo $actual_link . '/blog_view.php?blog_id=' . $row->id; ?>&amp;hashtags=youthopiabangla"
                                       title="Twitter share" target="_blank"><i class="fa fa fa-twitter fa-lg"
                                                                                style="margin: 0 8px;"
                                                                                aria-hidden="true"></i></a>
                                    <a class="google_plus customer share"
                                       style="color: #db4437;"
                                       href="https://plus.google.com/share?url=<?php // echo $actual_link . '/blog_view.php?blog_id=' . $row->id; ?>"
                                       title="Google Plus Share" target="_blank"><i
                                                class="fa fa fa-google-plus fa-lg" style="margin: 0 8px;"
                                                aria-hidden="true"></i></a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

            <?php endforeach; ?>


        </div>
    </section>
</div>

                                                           
</div>
      

<div id="opportunities" class="tab-pane fade ">
    <!-- <h3>Upcomming Events</h3> -->
                
<div class="others_page_wrp">
    <section class="org_map_wrp">
        <div class="container">
            <br>
            <div class="col-lg-12 text-center">
                <form action="<?php echo site_url() . '/InclusiveBd/searchInclusiveBdAllPost' ?>" class="" method="post">
                    <!--                --><?php //$categoryList = array('Competitions', 'Conferences', 'Exchange-programs', 'Fellowships', 'Internships', 'Scholarships', 'Workshops', 'Miscellaneous', 'Training'); ?>
                      <div class="row new_rz_box2">
                        <div class="col-lg-12 text-center">
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <select name="LOOKUP_DATA_ID">
                                    <option value="">All</option>
                                     
                                    <option value="1" <?php echo ($category_name== 'Youth Actions') ? 'selected="selected"' : '' ?> >
                                        Youth Actions
                                    </option>
                                    <option value="2" <?php echo ($category_name == 'Opportunities') ? 'selected="selected"' : '' ?>>
                                        Opportunities
                                    </option>
                                    <option value="3" <?php echo ($category_name == 'Area Based News') ? 'selected="selected"' : '' ?>>
                                        Area Based News
                                    </option>
                                     
                                </select>
                            </div>

                            <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                                <select name="order">
                                    <option value="">Select order</option>
                                    <option value="desc">Newest to oldest</option>
                                    <option value="asc">Oldest to newest</option>
                                    <option value="today">Today's post</option>
                                    <option value="week">This week post</option>
                                </select>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">
                                <input type="text" class="" placeholder="Search for..."
                                       name="search"
                                       style="min-width: 120px;     border-radius: 6px; margin: 0 3px 0 7px;   padding: 5px;">
                            </div>


                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                <input type="submit" class="btn btn-default btn-block" type="button" name="search_btn"
                                       value="Search">

                            </div>
                           <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                           <?php
                           $portalUser=$this->session->userdata('user_logged_user');
                          //echo '<pre>';print_r($session_info);exit;
                           ?>
                          <?php
                           if($this->session->userdata('user_logged_user')){
                            ?>
                              <div class="dropdown">
                                <button class="btn btn-default1 btn-block dropdown-toggle" type="button" data-toggle="dropdown">Create
                                <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                  <li><a href="<?php echo site_url();?>/InclusiveBd/createInclusivebd/1">YOUTH ACTION</a></li>
                                  <li><a href="<?php echo site_url();?>/InclusiveBd/createInclusivebd/2">OPPORTUNITY</a></li>
                                  <li><a href="<?php echo site_url();?>/InclusiveBd/createInclusivebd/3">NEWS</a></li>
                                </ul>
                              </div>

                             <?php
                            }else{ ?>

                                  <?php
                            }

                           ?>


                                        <!-- <a class="btn btn-default1 btn-block" type="button" href="<?php echo site_url();?>/InclusiveBd/createInclusivebd">Create</a> -->

                            </div>


                        </div>
                    </div>
                </form>
            </div>

            <?php


            if (isset($this->session->userdata['user_logged_user']['userId'])) {
                $id = $this->session->userdata['user_logged_user']['userId'];
            } else {
                $id = '';
            }
            ?>

            <p style="text-align: center;margin: 10px;">
            <hr>
            <hr>
            </p>

            <?php foreach ($inclusive_posts as $row) : ?>


                <?php
                $interests = $this->inclusivebd_model->getInclusiveInterest($row->id);

                $likeReaction = 0;
                $smileReaction = 0;
                $loveReaction = 0;
                $wowReaction = 0;
                ?>

                <?php foreach ($interests as $interest) { ?>
                    <?php
                    if ($interest->type == '1') {
                        $likeReaction = $interest->interest_count;
                    } elseif ($interest->type == '2') {
                        $smileReaction = $interest->interest_count;
                    } elseif ($interest->type == '3') {
                        $loveReaction = $interest->interest_count;
                    } elseif ($interest->type == '4') {
                        $wowReaction = $interest->interest_count;
                    }

                    ?>

                <?php } ?>


                <div class="col-xs-12 col-sm-6 col-md-4 show_block_elem">
                    <div class="panel panelD" style="border: 1px solid #ddd">
                        <div class="thumbnail panel-image embed-responsive embed-responsive-16by9"
                             style="margin-bottom: 2px">
                            <?php if ($row->type == 'image') { ?>

                                <?php $images = explode('___', $row->images); ?>

                                <a href="<?php echo site_url() . '/InclusiveBd/inclusiveBdView/' .

                                    $row->id; ?>"
                                   target="_blank"><img style="height: 200px; width: auto"
                                                        src="<?php echo base_url() . "upload/inclusivebd/" . $images[0]; ?>"
                                                        alt="<?php echo $row->title; ?>"/></a>
                            <?php } else {
                                $url_link = '';
                                if ($row->url_type == 'Youtube') {
                                    $url2Embed = str_replace("watch?v=", "embed/", $row->urls);
                                    $url_link = str_replace("[URL]", $url2Embed, $row->url_data);
                                } elseif ($row->url_type == 'SoundCloud') {
                                    $url_link = str_replace("[URL]", $row->urls, $row->url_data);
                                } elseif ($row->url_type == 'DailyMotion') {
                                    $url2Embed = str_replace("https://www.dailymotion.com/video", "https://www.dailymotion.com/embed/video", $row->urls);
                                    $url_link = str_replace("[URL]", $url2Embed, $row->url_data);
                                } elseif ($row->url_type == 'Vimeo') {
                                    $url2Embed = str_replace("https://vimeo.com/", "https://player.vimeo.com/video/", $row->urls);
                                    $url_link = str_replace("[URL]", $url2Embed, $row->url_data);
                                } else {
                                    echo '<div width="100%" height="258" style="margin-bottom: 0px;"><h1>Something Missing</h1></div>';
                                }
                                echo $url_link;
                            }
                            ?>
                        </div>
                        <div class="" style="padding: 7px; font-size: 14px;">
                            <button class="btn btn-link" id="like-button<?php echo $row->id; ?>"
                                    onclick="reactionFunction(1, <?php echo $row->id . ', ' . $id ?>)"
                                    style="padding-right: 0; text-decoration: none; background-color: #002f49; border-radius: 10px; padding: 0 5px; color: #ffffff"
                                    title="Like">
                                            <span id="like-count<?php echo $row->id; ?>"
                                                  style="color: #ffffff"><?php echo $likeReaction; ?></span> <img
                                        src="<?php echo base_url(); ?>assets/img2/icon/like.png" aria-hidden="true"
                                        width="12" height="12"
                                        style="margin: 0 4px 2px"/>
                            </button>
                            <button class="btn btn-link" id="smiley-button<?php echo $row->id; ?>"
                                    onclick="reactionFunction(2, <?php echo $row->id . ', ' . $id ?>)"
                                    style="padding-right: 0; text-decoration: none; background-color: #23a797; border-radius: 10px; padding: 0 5px; color: #ffffff"
                                    title="Smiley">
                                            <span id="smiley-count<?php echo $row->id; ?>"
                                                  style="color: #ffffff"><?php echo $smileReaction; ?></span> <img
                                        src="<?php echo base_url(); ?>assets/img2/icon/smile.png" aria-hidden="true"
                                        width="12" height="12"
                                        style="margin: 0 4px 2px"/>
                            </button>
                            <button class="btn btn-link" id="love-button<?php echo $row->id; ?>"
                                    onclick="reactionFunction(3, <?php echo $row->id . ', ' . $id ?>)"
                                    style="padding-right: 0; text-decoration: none; background-color: #e43733; border-radius: 10px; padding: 0 5px; color: #ffffff"
                                    title="Love">
                                            <span id="love-count<?php echo $row->id; ?>"
                                                  style="color: #ffffff"><?php echo $loveReaction; ?></span> <img
                                        src="<?php echo base_url(); ?>assets/img2/icon/heart.png" aria-hidden="true"
                                        width="12" height="12"
                                        style="margin: 0 4px 2px"/>
                            </button>
                            <button class="btn btn-link" id="wow-button<?php echo $row->id; ?>"
                                    onclick="reactionFunction(4, <?php echo $row->id . ', ' . $id ?>)"
                                    style="padding-right: 0; text-decoration: none; background-color: #79ad48; border-radius: 10px; padding: 0 5px; color: #ffffff"
                                    title="Wow">
                                            <span id="wow-count<?php echo $row->id; ?>"
                                                  style="color: #ffffff"><?php echo $wowReaction; ?></span> <img
                                        src="<?php echo base_url(); ?>assets/img2/icon/WOW.png" aria-hidden="true"
                                        width="12" height="12"
                                        style="margin: 0 4px 2px"/>
                            </button>

                        </div>
                        <div class="panel-heading"
                             style=" min-height:100px; max-height: 100px; overflow-y: hidden;">
                            <a style="color: #E43533;"
                               href="<?php echo site_url() . '/InclusiveBd/inclusiveBdView/' . $row->id; ?>"
                               target="_blank"><h3
                                        style="color: #E43533; text-align: center; text-transform: uppercase; min-height: 57px;"><?php echo $row->title; ?></h3>
                            </a>
                        </div>
                        <div class="panel-body">
                            <h4 style="color: #78AE48">Posted
                                on: <?php echo date("M d Y", strtotime($row->post_date)) ?></h4>
                            <h4 style="color: #78AE48">Posted
                                by: <?php echo $row->fname; ?></h4>
                        </div>
                        <hr style="margin: 0.5em;">

                        <div class="panel-body clearfix" style="padding:3px">
                            <div class="list-inline clearfix">

                    <span style="margin-top: 5px;">
                                        <i class="fa fa-tags fa-flip-horizontal" aria-hidden="true"></i> <span
                                style="background: lightgray; padding: 6px 12px; font-size: 12px; border-radius: 25px; text-transform: capitalize;"><?php echo $row->type; ?></span>
                                      <i class="fa fa-tags fa-flip-horizontal" aria-hidden="true"></i> <span
                                style="background: lightgray; padding: 6px 12px; font-size: 12px; border-radius: 25px; text-transform: capitalize;"><?php echo $row->category; ?></span></span>
                                <p class="pull-right">
                                    <i style="color: #012F49" class="fa fa-share-alt" aria-hidden="true"></i>
                                    <a class="facebook customer share"
                                       style="color: #4267b2;"
                                       href="http://www.facebook.com/sharer.php?u=<?php // echo $actual_link . '/blog_view.php?blog_id=' . $row->id; ?>"
                                       title="Facebook share" target="_blank"><i class="fa fa fa-facebook fa-lg"
                                                                                 style="margin: 0 8px;"
                                                                                 aria-hidden="true"></i></a>
                                    <a class="twitter customer share"
                                       style="color: #1b95e0;"
                                       href="http://twitter.com/share?url=<?php // echo $actual_link . '/blog_view.php?blog_id=' . $row->id; ?>&amp;hashtags=youthopiabangla"
                                       title="Twitter share" target="_blank"><i class="fa fa fa-twitter fa-lg"
                                                                                style="margin: 0 8px;"
                                                                                aria-hidden="true"></i></a>
                                    <a class="google_plus customer share"
                                       style="color: #db4437;"
                                       href="https://plus.google.com/share?url=<?php // echo $actual_link . '/blog_view.php?blog_id=' . $row->id; ?>"
                                       title="Google Plus Share" target="_blank"><i
                                                class="fa fa fa-google-plus fa-lg" style="margin: 0 8px;"
                                                aria-hidden="true"></i></a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

            <?php endforeach; ?>


        </div>
    </section>
</div>

                                                           
</div>


<div id="area_based_news" class="tab-pane fade ">
    <!-- <h3>Upcomming Events</h3> -->
                
<div class="others_page_wrp">
    <section class="org_map_wrp">
        <div class="container">
            <br>
            <div class="col-lg-12 text-center">
                <form action="<?php echo site_url() . '/InclusiveBd/searchInclusiveBdAllPost' ?>" class="" method="post">
                    <!--                --><?php //$categoryList = array('Competitions', 'Conferences', 'Exchange-programs', 'Fellowships', 'Internships', 'Scholarships', 'Workshops', 'Miscellaneous', 'Training'); ?>
                      <div class="row new_rz_box2">
                        <div class="col-lg-12 text-center">
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <select name="LOOKUP_DATA_ID">
                                    <option value="">All</option>
                                     
                                    <option value="1" <?php echo ($category_name== 'Youth Actions') ? 'selected="selected"' : '' ?> >
                                        Youth Actions
                                    </option>
                                    <option value="2" <?php echo ($category_name == 'Opportunities') ? 'selected="selected"' : '' ?>>
                                        Opportunities
                                    </option>
                                    <option value="3" <?php echo ($category_name == 'Area Based News') ? 'selected="selected"' : '' ?>>
                                        Area Based News
                                    </option>
                                     
                                </select>
                            </div>

                            <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                                <select name="order">
                                    <option value="">Select order</option>
                                    <option value="desc">Newest to oldest</option>
                                    <option value="asc">Oldest to newest</option>
                                    <option value="today">Today's post</option>
                                    <option value="week">This week post</option>
                                </select>
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">
                                <input type="text" class="" placeholder="Search for..."
                                       name="search"
                                       style="min-width: 120px;     border-radius: 6px; margin: 0 3px 0 7px;   padding: 5px;">
                            </div>


                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                <input type="submit" class="btn btn-default btn-block" type="button" name="search_btn"
                                       value="Search">

                            </div>
                      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                           <?php
                           $portalUser=$this->session->userdata('user_logged_user');
                          //echo '<pre>';print_r($session_info);exit;
                           ?>
                          <?php
                           if($this->session->userdata('user_logged_user')){
                            ?>
                              <div class="dropdown">
                                <button class="btn btn-default1 btn-block dropdown-toggle" type="button" data-toggle="dropdown">Create
                                <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                  <li><a href="<?php echo site_url();?>/InclusiveBd/createInclusivebd/1">YOUTH ACTION</a></li>
                                  <li><a href="<?php echo site_url();?>/InclusiveBd/createInclusivebd/2">OPPORTUNITY</a></li>
                                  <li><a href="<?php echo site_url();?>/InclusiveBd/createInclusivebd/3">NEWS</a></li>
                                </ul>
                              </div>

                             <?php
                            }else{ ?>

                                  <?php
                            }

                           ?>


                                        <!-- <a class="btn btn-default1 btn-block" type="button" href="<?php echo site_url();?>/InclusiveBd/createInclusivebd">Create</a> -->

                            </div>


                        </div>
                    </div>
                </form>
            </div>

            <?php


            if (isset($this->session->userdata['user_logged_user']['userId'])) {
                $id = $this->session->userdata['user_logged_user']['userId'];
            } else {
                $id = '';
            }
            ?>

            <p style="text-align: center;margin: 10px;">
            <hr>
            <hr>
            </p>

            <?php foreach ($inclusive_posts as $row) : ?>


                <?php
                $interests = $this->inclusivebd_model->getInclusiveInterest($row->id);

                $likeReaction = 0;
                $smileReaction = 0;
                $loveReaction = 0;
                $wowReaction = 0;
                ?>

                <?php foreach ($interests as $interest) { ?>
                    <?php
                    if ($interest->type == '1') {
                        $likeReaction = $interest->interest_count;
                    } elseif ($interest->type == '2') {
                        $smileReaction = $interest->interest_count;
                    } elseif ($interest->type == '3') {
                        $loveReaction = $interest->interest_count;
                    } elseif ($interest->type == '4') {
                        $wowReaction = $interest->interest_count;
                    }

                    ?>

                <?php } ?>


                <div class="col-xs-12 col-sm-6 col-md-4 show_block_elem">
                    <div class="panel panelD" style="border: 1px solid #ddd">
                        <div class="thumbnail panel-image embed-responsive embed-responsive-16by9"
                             style="margin-bottom: 2px">
                            <?php if ($row->type == 'image') { ?>

                                <?php $images = explode('___', $row->images); ?>

                                <a href="<?php echo site_url() . '/InclusiveBd/inclusiveBdView/' .

                                    $row->id; ?>"
                                   target="_blank"><img style="height: 200px; width: auto"
                                                        src="<?php echo base_url() . "upload/inclusivebd/" . $images[0]; ?>"
                                                        alt="<?php echo $row->title; ?>"/></a>
                            <?php } else {
                                $url_link = '';
                                if ($row->url_type == 'Youtube') {
                                    $url2Embed = str_replace("watch?v=", "embed/", $row->urls);
                                    $url_link = str_replace("[URL]", $url2Embed, $row->url_data);
                                } elseif ($row->url_type == 'SoundCloud') {
                                    $url_link = str_replace("[URL]", $row->urls, $row->url_data);
                                } elseif ($row->url_type == 'DailyMotion') {
                                    $url2Embed = str_replace("https://www.dailymotion.com/video", "https://www.dailymotion.com/embed/video", $row->urls);
                                    $url_link = str_replace("[URL]", $url2Embed, $row->url_data);
                                } elseif ($row->url_type == 'Vimeo') {
                                    $url2Embed = str_replace("https://vimeo.com/", "https://player.vimeo.com/video/", $row->urls);
                                    $url_link = str_replace("[URL]", $url2Embed, $row->url_data);
                                } else {
                                    echo '<div width="100%" height="258" style="margin-bottom: 0px;"><h1>Something Missing</h1></div>';
                                }
                                echo $url_link;
                            }
                            ?>
                        </div>
                        <div class="" style="padding: 7px; font-size: 14px;">
                            <button class="btn btn-link" id="like-button<?php echo $row->id; ?>"
                                    onclick="reactionFunction(1, <?php echo $row->id . ', ' . $id ?>)"
                                    style="padding-right: 0; text-decoration: none; background-color: #002f49; border-radius: 10px; padding: 0 5px; color: #ffffff"
                                    title="Like">
                                            <span id="like-count<?php echo $row->id; ?>"
                                                  style="color: #ffffff"><?php echo $likeReaction; ?></span> <img
                                        src="<?php echo base_url(); ?>assets/img2/icon/like.png" aria-hidden="true"
                                        width="12" height="12"
                                        style="margin: 0 4px 2px"/>
                            </button>
                            <button class="btn btn-link" id="smiley-button<?php echo $row->id; ?>"
                                    onclick="reactionFunction(2, <?php echo $row->id . ', ' . $id ?>)"
                                    style="padding-right: 0; text-decoration: none; background-color: #23a797; border-radius: 10px; padding: 0 5px; color: #ffffff"
                                    title="Smiley">
                                            <span id="smiley-count<?php echo $row->id; ?>"
                                                  style="color: #ffffff"><?php echo $smileReaction; ?></span> <img
                                        src="<?php echo base_url(); ?>assets/img2/icon/smile.png" aria-hidden="true"
                                        width="12" height="12"
                                        style="margin: 0 4px 2px"/>
                            </button>
                            <button class="btn btn-link" id="love-button<?php echo $row->id; ?>"
                                    onclick="reactionFunction(3, <?php echo $row->id . ', ' . $id ?>)"
                                    style="padding-right: 0; text-decoration: none; background-color: #e43733; border-radius: 10px; padding: 0 5px; color: #ffffff"
                                    title="Love">
                                            <span id="love-count<?php echo $row->id; ?>"
                                                  style="color: #ffffff"><?php echo $loveReaction; ?></span> <img
                                        src="<?php echo base_url(); ?>assets/img2/icon/heart.png" aria-hidden="true"
                                        width="12" height="12"
                                        style="margin: 0 4px 2px"/>
                            </button>
                            <button class="btn btn-link" id="wow-button<?php echo $row->id; ?>"
                                    onclick="reactionFunction(4, <?php echo $row->id . ', ' . $id ?>)"
                                    style="padding-right: 0; text-decoration: none; background-color: #79ad48; border-radius: 10px; padding: 0 5px; color: #ffffff"
                                    title="Wow">
                                            <span id="wow-count<?php echo $row->id; ?>"
                                                  style="color: #ffffff"><?php echo $wowReaction; ?></span> <img
                                        src="<?php echo base_url(); ?>assets/img2/icon/WOW.png" aria-hidden="true"
                                        width="12" height="12"
                                        style="margin: 0 4px 2px"/>
                            </button>

                        </div>
                        <div class="panel-heading"
                             style=" min-height:100px; max-height: 100px; overflow-y: hidden;">
                            <a style="color: #E43533;"
                               href="<?php echo site_url() . '/InclusiveBd/inclusiveBdView/' . $row->id; ?>"
                               target="_blank"><h3
                                        style="color: #E43533; text-align: center; text-transform: uppercase; min-height: 57px;"><?php echo $row->title; ?></h3>
                            </a>
                        </div>
                        <div class="panel-body">
                            <h4 style="color: #78AE48">Posted
                                on: <?php echo date("M d Y", strtotime($row->post_date)) ?></h4>
                            <h4 style="color: #78AE48">Posted
                                by: <?php echo $row->fname; ?></h4>
                        </div>
                        <hr style="margin: 0.5em;">

                        <div class="panel-body clearfix" style="padding:3px">
                            <div class="list-inline clearfix">

                    <span style="margin-top: 5px;">
                                        <i class="fa fa-tags fa-flip-horizontal" aria-hidden="true"></i> <span
                                style="background: lightgray; padding: 6px 12px; font-size: 12px; border-radius: 25px; text-transform: capitalize;"><?php echo $row->type; ?></span>
                                      <i class="fa fa-tags fa-flip-horizontal" aria-hidden="true"></i> <span
                                style="background: lightgray; padding: 6px 12px; font-size: 12px; border-radius: 25px; text-transform: capitalize;"><?php echo $row->LOOKUP_DATA_NAME; ?></span></span>
                                <p class="pull-right">
                                    <i style="color: #012F49" class="fa fa-share-alt" aria-hidden="true"></i>
                                    <a class="facebook customer share"
                                       style="color: #4267b2;"
                                       href="http://www.facebook.com/sharer.php?u=<?php // echo $actual_link . '/blog_view.php?blog_id=' . $row->id; ?>"
                                       title="Facebook share" target="_blank"><i class="fa fa fa-facebook fa-lg"
                                                                                 style="margin: 0 8px;"
                                                                                 aria-hidden="true"></i></a>
                                    <a class="twitter customer share"
                                       style="color: #1b95e0;"
                                       href="http://twitter.com/share?url=<?php // echo $actual_link . '/blog_view.php?blog_id=' . $row->id; ?>&amp;hashtags=youthopiabangla"
                                       title="Twitter share" target="_blank"><i class="fa fa fa-twitter fa-lg"
                                                                                style="margin: 0 8px;"
                                                                                aria-hidden="true"></i></a>
                                    <a class="google_plus customer share"
                                       style="color: #db4437;"
                                       href="https://plus.google.com/share?url=<?php // echo $actual_link . '/blog_view.php?blog_id=' . $row->id; ?>"
                                       title="Google Plus Share" target="_blank"><i
                                                class="fa fa fa-google-plus fa-lg" style="margin: 0 8px;"
                                                aria-hidden="true"></i></a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

            <?php endforeach; ?>


        </div>
    </section>
</div>

                                                           
</div>


</div>



 <script type="text/javascript">
$("#LOOKUP_DATA_ID option[id='1']").attr("selected", "selected");
</script>
<script>
    function reactionFunction(type, blog_id, user_id = null) {

        if (!user_id) {
            alert('Please login to perform this task');
            return false;
        }
        $.ajax({
            url: 'calSearch',
            type: 'POST',
            data: {
                blog_type: type,
                user_id: user_id,
                blog_id: blog_id,
                action: 'blogInterest'
            }
        }).done(function (response) {
            var res = $.parseJSON(response);
            console.log(res);

//            console.log( Object.keys(res).length );


            $('#like-count' + res.blog_id).html(0);
            $('#smiley-count' + res.blog_id).html(0);
            $('#love-count' + res.blog_id).html(0);
            $('#wow-count' + res.blog_id).html(0);

            res.count.forEach(function (counts) {
                if (counts.type == '1') {
                    $('#like-count' + res.blog_id).html(counts.interest_count);
                }
                else if (counts.type == '2') {
                    $('#smiley-count' + res.blog_id).html(counts.interest_count);
                }
                else if (counts.type == '3') {
                    $('#love-count' + res.blog_id).html(counts.interest_count);
                }
                else if (counts.type == '4') {
                    $('#wow-count' + res.blog_id).html(counts.interest_count);
                }
                else {
                }
            });
            if (!res.ack) {
                alert(res.msg);
            }
        });
    }
</script>