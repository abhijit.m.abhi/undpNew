<!DOCTYPE html>

<html lang="en-US">
<?php $this->load->view('portal/template/header'); ?>



<!-- ******************************************************************************************** -->
<link rel="shortcut icon" href="<?php echo base_url(); ?>favicon.ico"/>
<!-- CSS STARTS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>event/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>event/fonts/stylesheet.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>event/css/font-awesome.min.css">
<link href="<?php echo base_url(); ?>assets/vendors/fileuploader/src/jquery.fileuploader.css" media="all"
      rel="stylesheet">
<link rel="stylesheet" href="<?php echo base_url(); ?>event/css/owl.carousel.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>event/css/owl.theme.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>event/css/datepicker.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>old/css/style.css"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>event/css/style.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>event/css/responsive.css">

<link rel="stylesheet" href="<?php echo base_url();?>old/assets/vendors/twemoji-awesome-gh-pages/twemoji-awesome.css">

<link href="<?php echo base_url();?>old/assets/vendors/fileuploader/examples/thumbnails/css/jquery.fileuploader-theme-thumbnails.css"
      media="all" rel="stylesheet">

<style>
    .date-highligh {
        color: #f00;
        /*text-decoration: underline;*/
        outline: 2px solid #ff0000;
    }

</style>


<link rel="stylesheet" href="<?php echo base_url(); ?>old/css/icon_style.css" type="text/css">

<!-- ******************************************************************************************** -->
<body>
<div class="page-wrapper">
    <?php $this->load->view('portal/template/menu.php'); ?>

    <div id="" style="">
        <div class="container">
            <section>
                <?php
                echo $_content;

                ?>
            </section>
        </div>
    </div>
    <?php $this->load->view('portal/template/modal'); ?>
    <footer id="page-footer">
        <?php $this->load->view('portal/template/footer') ?>
    </footer>
</div>

<!-- JS/CSS for FB Imo  -->
<script src="<?php echo base_url(); ?>fb_imo/jquery-2.1.4.js"></script>


<!--end page-wrapper-->
<a href="#" class="to-top scroll" data-show-after-scroll="600"><i class="arrow_up"></i></a>

<script type="text/javascript" src="<?php echo base_url(); ?>portalAssets/js/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>portalAssets/bootstrap/js/bootstrap.min.js"></script>
<!--<script type="text/javascript" src="--><?php //echo base_url(); ?><!--portalAssets/js/bootstrap-select.min.js"></script>-->
<!--<script type="text/javascript" src="--><?php //echo base_url(); ?><!--portalAssets/js/richmarker-compiled.js"></script>-->
<script type="text/javascript" src="<?php echo base_url(); ?>portalAssets/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>portalAssets/js/jquery.validate.min.js"></script>
<!-- <script type="text/javascript" src="<?php echo base_url(); ?>portalAssets/js/custom.js"></script> -->
<script type="text/javascript" src="<?php echo base_url(); ?>portalAssets/js/maps.js"></script>
<script src="<?php echo base_url(); ?>assets/js/portal-js/bootstrap-datetimepicker.min.js"></script>

<!-- slider STARTS -->
<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<!-- <link rel="stylesheet" type="text/css" href="sliderstyle.css"> -->
<link rel="stylesheet" href="<?php echo base_url(); ?>portalAssets/slider/sliderstyle.css" type="text/css">
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.transit/0.9.12/jquery.transit.min.js"></script>
<!-- <script type="text/javascript" src="slider.jquery.js"></script> -->
<script src="<?php echo base_url(); ?>portalAssets/slider/slider.jquery.js"></script>

<!-- ******************************************************************************************** -->

<!-- JS STARTS -->
<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyAsJfFqX2xpb8xlgFvECm4Mo0jXFtyp1zM&libraries=places"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>event/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>event/js/selectivizr-min.js"></script>
<script src="<?php echo base_url(); ?>event/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>event/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>event/js/owl.carousel.min.js"></script>
<script src="<?php echo base_url(); ?>event/js/ajax.js"></script>
<script src="<?php echo base_url(); ?>old/assets/js/bootstrap-select.min.js"></script>
<!--<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyAsJfFqX2xpb8xlgFvECm4Mo0jXFtyp1zM&libraries=places"></script>-->
<!--<script type="text/javascript"-->
<!--        src="https://maps.google.com/maps/api/js?key=AIzaSyAsJfFqX2xpb8xlgFvECm4Mo0jXFtyp1zM&libraries=places"></script>-->
<!--<script src="--><?php //echo base_url(); ?><!--old/assets/js/richmarker-compiled.js"></script>-->
<script src="<?php echo base_url(); ?>old/assets/js/maps.js"></script>
<script src="<?php echo base_url(); ?>old/assets/js/icheck.min.js"></script>
<script src="<?php echo base_url(); ?>old/assets/vendors/fileuploader/src/jquery.fileuploader.min.js"
        type="text/javascript"></script>
<script src="<?php echo base_url(); ?>old/assets/vendors/fileuploader/examples/default-upload/js/custom.js"
        type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>old/assets/js/ajax.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>old/assets/js/custom.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>old/assets/js/myscript.js"></script>




<!--<script type="text/javascript" src="--><?php //base_url(); ?><!--old/assets/js/markerclusterer_packed.js"></script>-->
<!--<script type="text/javascript" src="--><?php //base_url(); ?><!--old/assets/js/infobox.js"></script>-->
<script type="text/javascript" src="<?php echo base_url(); ?>old/assets/js/jquery.fitvids.js"></script>
<script src="<?php echo base_url(); ?>event/js/jquery.downCount.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>old/assets/js/jquery.trackpad-scroll-emulator.min.js"></script>
<!--<script type="text/javascript" src="assets/js/bootstrap-datepicker.js"></script>-->
<script type="text/javascript" src="<?php echo base_url(); ?>old/assets/js/jquery.nouislider.all.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>old/assets/js/myscript.js"></script>



<script src="<?php echo base_url(); ?>old/js/main.js"></script>
<script src="<?php echo base_url(); ?>old/js/ajax.js"></script>
<script src="<?php echo base_url(); ?>old/js/fbzahed.js"></script>
<!--<script src="--><?php //echo base_url(); ?><!--old/js/selection.json"></script>-->


<script type="text/javascript" src="<?php echo base_url(); ?>old/js/jquery.easyPaginate.js"></script>

<script src="<?php echo base_url(); ?>old/assets/vendors/fileuploader/examples/thumbnails/js/custom.js" type="text/javascript"></script>


<script type="text/javascript">
    $(document).ready(function () {
        $('.date_picker').datepicker({
            format: "yyyy-mm-dd"
        });
    });
</script>

<script>
    $(function () {
        $('#search_btn').on('click', function () {

            var category = $('#category').val();
            var district = $('#district').val();
            var organization = $('#organization').val();
            var search_month = $('#search_month').val();
            var search_year = $('#search_year').val();
            var search_text = $('#search_text').val();
            $.ajax({
                url: 'calSearch',
                data: {
                    category: category,
                    district: district,
                    organization: organization,
                    search_month: search_month,
                    search_year: search_year,
                    search_text: search_text,
                    action: 'search'
                },
                type: 'POST'
            }).done(function (response) {
                $('#search_result_container').html(response);
//                    console.log(response);
            });
        })
    })

</script>


<!-- ******************************************************************************************** -->


<script type="text/javascript">
    $(document).ready(function () {
        $(".rcorners2").mouseover(function () {
            $pb = $(this).parents("div.col-md-2:first");
            $element = $pb.find(".bottomText");
            $element.show();

        });
    });
    $(document).ready(function () {
        $(".rcorners2").mouseout(function () {
            $pb = $(this).parents("div.col-md-2:first");
            $element = $pb.find(".bottomText");
            $element.hide();

        });
    });
    $(document).ready(function () {
        $('.date_picker').datepicker({
            format: "yyyy-mm-dd"
        });
    });
</script>
<script type="text/javascript" src="<?php echo base_url('portalAssets/tree/abixTreeList.min.js') ?>"></script>

</body>

<!-- JS/CSS for FB Imo  -->
<?php if($this->uri->segment(1) == 'blog' || $this->uri->segment(2) == 'eventView' || $this->uri->segment(1) == 'opportunity' || $this->uri->segment(1) == 'InclusiveBd' || $this->uri->segment(1) == 'inclusiveBd' ) : ?>
    <?php if($this->uri->segment(2) == 'createOpportunity' || $this->uri->segment(2) == 'editOpportunityPost') : ?>

    <?php else : ?>
        <script src="<?php echo base_url(); ?>fb_imo/jquery-ui_1.12.1_.min.js"></script>
    <?php endif; ?>
<?php endif; ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>fb_imo/stylesheet.css">
<script src="<?php echo base_url(); ?>fb_imo/facebook-reactions.js"></script>
<script src="<?php echo base_url(); ?>fb_imo/facebook-reactions-event.js"></script>
<script src="<?php echo base_url(); ?>fb_imo/facebook-reactions-opportunity.js"></script>
<script src="<?php echo base_url(); ?>fb_imo/facebook-reactions-inclusive.js"></script>


<link rel="stylesheet" href="<?php echo base_url(); ?>assets/redactor/redactor.css"/>
<script src="<?php echo base_url(); ?>assets/redactor/redactor.min.js"></script>

<script type="text/javascript">
    $(document).ready(
        function () {
            $('.redactor').redactor();
        }
    );
</script>





