

	<link rel="stylesheet" href="<?php echo base_url('portalAssets/tree/default/style.min.css') ?>" />

  <!--
<div id="html" class="demo">

		<ul>
			<li data-jstree='{ "opened" : false }'>Root node
				<ul>
					<li data-jstree='{ "selected" : false }'>Child node 1</li>
					<li>Child node 2
						<ul>
							<li data-jstree='{ "selected" : false }'>habizabi2
								<ul>
							<li data-jstree='{ "selected" : true }'>habizabi2</li>
						</ul>
							</li>
						</ul>
					</li>
				</ul>
			</li>
			<li data-jstree='{ "opened" : true }'>Root node
		</ul>

	</div>
    -->


<style media="screen">
  .customDiv{
    height: 150px;
    overflow: auto;
  }
</style>
<div class="col-md-3">
  <div class="panel panel-default">
    <div class="panel-heading">Folder Directory</div>
     <p><a href="#" id="tree-expand-all">Expand all</a> | <a href="#" id="tree-collapse-all">Collapse all</a></p>
    <div class="panel-body">
      <div id="html" class="demo">
      <?php
        $session=1;
				function createTree($tree, $session,$parentId)
				{

						foreach ($tree as $key => $cat) {
								$cat_id = $cat['SD_ID'];
                if($cat_id==$parentId)
                {
                  $className='true';
                }
                else
                {
                  $className='false';
                }

                $url=base_url('SkillDevelopment/index/'.$cat['SD_ID']);

								if ($session == 1) {
                  ?>
                  <li class="" data-jstree='{ "selected" : <?php echo $className; ?>}'><a class="chdCls" href="<?php echo $url; ?>"><?php echo $cat['SD_NAME']; ?></a>
                    <?php

								} else {
										echo '<li class=""> <button>'.$cat['SD_NAME'].'</button>';
								}
								if (!empty($cat['children'])) {
									echo "<ul>";
										createTree($cat['children'], $session,$parentId);
									echo "</ul>";
								}
								echo '</li>';
						}

				}
				echo "<ul>";


				createTree($tree, $session,$parentId);
				"</ul>";
				?>
      </div>
    </div>

  </div>
</div>
<div class="col-md-9">
  <div class="panel panel-info">
      <div class="panel-heading">
        <div class="row">
          <div class="col-md-9">
            <ol class="breadcrumb">
              <li><a href="<?php echo base_url("SkillDevelopment/index/0") ?>">Root</a></li>
            <?php
              if(!empty($directoryTree))
              {
                foreach($directoryTree as $dt)
                {
                  ?>
                    <li><a href="<?php echo base_url("SkillDevelopment/index/$dt->SD_ID") ?>"><?php echo $dt->SD_NAME;?></a></li>
                  <?php

                }

              }

            ?>
            </ol>
          </div>
          <div class="col-md-3">
            <?php
              if(isset($this->session->userdata['user_logged_user']))
              {
            ?>
            <button type="button"
            class="btn btn-xl btn-primary modalLink pull-right"
            href="<?php echo base_url('SkillDevelopment/createDirectory'.'/'.$parentId) ?>"
            title="Create new directory"
            data-modal-size="modal-sm"
            name="button">Create New Folder</button>
            <?php
          }
            ?>
          </div>
        </div>


      </div>
      <div class="panel-body">
        <?php
        if(($parentId>0) AND (isset($this->session->userdata['user_logged_user'])) )
        {
          ?>
          <div class="col-md-2 customDiv col-sm-4">

            <a class="modalLink "
            href="<?php echo base_url('SkillDevelopment/addNewElement/'.$parentId); ?>"
            title="Add New Element"
            data-modal-size="modal-md"
            >
              <cente><span class="glyphicon glyphicon-upload" style="font-size:100px; color:white;background-color:green"></span>
                <h6 style="color:#000; margin-top:3px">Add New</h6></a></center>
              </center>

          </a>

          </div>
          <?php
        }
        ?>
        <?php foreach($directories as $row){ ?>
          <div class="col-md-2 customDiv col-sm-4">
            <center>
            <a href="<?php echo base_url("SkillDevelopment/index").'/'.$row->SD_ID; ?>"
              <span class="glyphicon glyphicon-folder-close" style="font-size:100px;color:#D8BA52;"></span>
          <h6 style="color:#000; margin-top:3px"><?php echo $row->SD_NAME; ?></h6></a></center>
          </div>
        <?php }
        ?>
      <?php foreach($files as $file) {?>
        <div class="col-md-2 customDiv col-sm-4">
          <?php if($file->ELEMENT_TYPE=='F'){ ?>
          <center>
          <a href="<?php echo base_url($file->FILE_PATH)?>" target="blank"
            <span class="glyphicon glyphicon-paperclip" style="font-size:100px;color:#000;"></span>
        <h6 style="color:#000; margin-top:3px"><b><?php echo $file->ELEMENT_TITLE; ?></b> <small> (<?php echo $file->ELEMENT_EXT; ?>) </small></h6></a>
      </center>
        <?php
        }
        else
        {
          ?>
          <center>
          <a href="<?php echo $file->ELEMENT_URL; ?>" target="blank"
            <span class="glyphicon glyphicon-globe" style="font-size:100px;color:blue;"></span>
        <h6 style="color:#000; margin-top:3px"><?php echo $file->ELEMENT_TITLE; ?></h6></a>
      </center>
          <?php
        }
        ?>
        </div>
      <?php } ?>


      </div>
    </div>

</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('portalAssets/tree/default/jstree.min.js') ?>">

</script>
<script type="text/javascript">
$('#html').jstree();
  $(document).on("click", "a.chdCls", function () {
    //alert("okay");
        var url=$(this).attr("href");
        $(this).removeClass('jstree-anchor');
        $(this).removeClass('jstree-clicked');
        window.location.replace(url);
    });
  //$("a.chdCls").removeClass('jstree-anchor');
  //$("a.chdCls").removeClass('jstree-clicked');

</script>
