<style type="text/css">
    .modal-header h4 {
        color: #681156;
        margin-top: 15px;
        font-size: 25px;
        margin-bottom: 8px;
        border-bottom: 1px solid #ddd !important;
        padding-bottom: 20px !important;
    }
</style>


<?php echo form_open('blog/reportProblemFormInsert', "class='form-vertical' id='report_form'"); ?>
      <img src="<?php echo base_url(); ?>assets/img/loader.gif" id="gif" style="display: none; margin: 0 auto;">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label" for="name">Email From *</label>
                        <input type="email" required="required" class="form-control" name="name" id="name"
                               placeholder="Please Enter Your Mail" value="<?php echo (!empty($user_detail->email) ? $user_detail->email : ""); ?>"  >
                                <input type="hidden" name="redirectUrl" value="<?php echo $url; ?>">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label" for="name">Email To *</label>
                        <input type="email" required="required" class="form-control" name="email" id="email"
                               placeholder="Please Enter Sending Mail"  >
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label" for="subject">Subject</label>
                        <input type="text" class="form-control" name="subject" id="subject"
                               placeholder="Please Enter Subject" required="required">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label" for="message">Message *</label>
                        <textarea name="message" id="message" placeholder="Please Enter Your Message" class="form-control"
                                  rows="3" required="required" spellcheck="true"></textarea>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label" for="link">Link *</label>
                        <input name="link" id="link" placeholder="link" class="form-control"
                               value="<?php echo $link; ?>" required="required" readonly>
                    </div>
                </div>
                <input type="hidden" id="reportForm" name="reportForm" value="$user_detail->userId">

                <div class="col-md-12">
                    <div class="form-group">
                        <button type="submit" id="btn" class="btn btn-success btn-block btn-sm">Submit</button>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
      

         <script type="text/javascript">
                    $('#report_form').submit(function() {
                    $('#gif').css('display', 'block');
                    });
                </script>
        <script type="text/javascript">

    

