

<link rel="stylesheet" href="<?php echo base_url('portalAssets/tree/default/style.min.css') ?>" />
<?php
$userId=0;
$url= 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
$actual_link=str_replace("/","--",$url);
?>
<!--
<div id="html" class="demo">

<ul>
<li data-jstree='{ "opened" : false }'>Root node
<ul>
<li data-jstree='{ "selected" : false }'>Child node 1</li>
<li>Child node 2
<ul>
<li data-jstree='{ "selected" : false }'>habizabi2
<ul>
<li data-jstree='{ "selected" : true }'>habizabi2</li>
</ul>
</li>
</ul>
</li>
</ul>
</li>
<li data-jstree='{ "opened" : true }'>Root node
</ul>

</div>
-->

<style media="screen">

.btn-xs{
	padding: 0px 0px!important;
	width: 49%;
}
.btn-delete{
	width: 100%;
}
h6{
	color:#000; margin-top:3px;font-size:12px
}

.chl-tree a>i{
	display: none !important;
}
.chl-tree a{
	background-color: #ADC801;
	padding: 0px 10px;
	text-align: center;
	margin-bottom: 5px;
	color: #fff ! important;
	height: auto;
	font-size: 14px;
	border-radius: 4px;
}

.chl-tree a:hover{
	background-color: #ADC801;
	font-size: 14px;
}
.jstree-default .jstree-clicked {
	background: #ADC801;
	margin-bottom: 5px;
	padding: 0px 10px;
	transition: none;
	height: auto;
	border-radius: 0px;
	box-shadow: none;
	font-size: 14px;
	border-radius: 4px;
	white-space: none;
}
.chl-tree ul>li a{
	padding: 0px 35px;
	margin-bottom: 5px;
	background-color: #E43434 !important;
}
</style>
<div class="col-md-3">
	<div class="panel panel-default">
		<div class="panel-heading">Folder Directory</div>

		<div class="panel-body">
			<div id="html" class="demo">
				<?php
				function getDocUrl($docType)
				{
					if($docType=='ppt' OR $docType=='pptx')
					{
						$docTypeUrl=base_url('portalAssets/img/fileType/pptDoc.jpg');
					}
					else if($docType=='doc' OR $docType=='docx')
					{
						$docTypeUrl=base_url('portalAssets/img/fileType/wordDoc.jpg');
					}
					else if($docType=='pdf')
					{
						$docTypeUrl=base_url('portalAssets/img/fileType/pdfDoc.jpg');
					}
					else if($docType=='web')
					{
						$docTypeUrl=base_url('portalAssets/img/fileType/websiteDoc.jpg');
					}
					else
					{
						$docTypeUrl='';
					}
					return $docTypeUrl;
				}
				$session=1;
				function createTree($tree, $session,$parentId)
				{

					foreach ($tree as $key => $cat) {
						$cat_id = $cat['SD_ID'];
						if($cat_id==$parentId)
						{
							$className='true';
						}
						else
						{
							$className='false';
						}

						$url=base_url('SkillDevelopment/index/'.$cat['SD_ID']);

						if ($session == 1) {
							?>
							<li class="chl-tree " data-jstree='{ "selected" : <?php echo $className; ?>}'><a class="chdCls" href="<?php echo $url; ?>"><?php echo $cat['SD_NAME']; ?></a>
								<?php

							} else {
								echo '<li class=""> <button>'.$cat['SD_NAME'].'</button>';
							}
							if (!empty($cat['children'])) {
								echo "<ul>";
								createTree($cat['children'], $session,$parentId);
								echo "</ul>";
							}
							echo '</li>';
						}

					}
					echo "<ul>";
					createTree($tree, $session,$parentId);
					"</ul>";
					?>
				</div>
			</div>

		</div>
	</div>
	<?php
	$userInfo=$this->session->userdata('user_logged_user');
	if(isset($userInfo['userId']))
	{
		$permUserId=$userInfo['userId'];
	}
	else
	{
		$permUserId=0;
	}
	$prm=$this->db->query("SELECT COUNT(*) PER FROM front_user_permission WHERE MODULE_ID=5 AND USER_ID=$permUserId")->row();

	//  $eventPermission= getPermission(2,$permission);
	?>
	<style media="screen">
	.path{

	}
	.breadCrump1{
		color:#01737E;
		text-align: center;
		background-color: #fff;
	}
	.breadcrumb{
		border: 1px solid #01737E;
		padding: 5px 5px!important;
	}
	.breadCrump2{
		background-color: #01737E;
		color: #fff;
	}
	.breadCrump2 a{

		color: #fff;
	}
	.searchBox{
		border-radius: 2px;
		margin-left: 15px;
		margin-bottom: 5px;
		text-align: right;
	}
	input[type=text] {
		width: 130px;
		-webkit-transition: width 0.4s ease-in-out;
		transition: width 0.4s ease-in-out;
	}

	</style>
	<style>
	input.searchBox {
		width: 130px;
		box-sizing: border-box;
		border: 2px solid #ccc;
		border-radius: 4px;
		font-size: 16px;
		background-color: white;
		background-image: url('searchicon.png');
		background-position: 10px 10px;
		background-repeat: no-repeat;
		padding: 5px 20px 5px 40px;
		-webkit-transition: width 0.4s ease-in-out;
		transition: width 0.4s ease-in-out;
	}

	input.searchBox:focus {
		width: 100%;
	}
</style>
<div class="col-md-9">
	<div class="panel panel-info">
		<div class="panel-heading">
			<div class="row">
				<div class="col-md-4">
					<input type="hidden" class="searchUrl" value="<?php echo site_url('SkillDevelopment/searchElement'); ?>">
					<input list="browsers" class="searchBox form-control" placeholder="Search" >
					<datalist id="browsers">
						<?php
						foreach($documents as $doc)
						{
							echo "<option value='$doc->ELEMENT_TITLE'>";
						}
						?>
					</datalist>
				</div>
			</div>
			<div class="row">
				<div class="col-md-9">
					<div class="col-md-2">
						<ol class="breadcrumb breadCrump1">
							<li class="path">path</li>
						</ol>
					</div>
					<div class="col-md-10">
						<?php
						if(!empty($directoryTree))
						{
							?>
							<ol class="breadcrumb breadCrump2">

								<?php

								foreach($directoryTree as $dt)
								{
									?>
									<li><a href="<?php echo base_url("SkillDevelopment/index/$dt->SD_ID") ?>"><?php echo $dt->SD_NAME;?></a></li>
									<?php

								}



								?>
							</ol>
							<?php
						}
						?>
					</div>

				</div>

			</div>


		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-12">
					<div class="searchResult">

					</div>
				</div>
			</div>
			<div class="row">
				<?php foreach($directories as $row){ ?>

					<div class="col-md-2 customDiv col-sm-4" style="border:1px dotted gray;padding-top:5px">

						<center>
							<a href="<?php echo base_url("SkillDevelopment/index").'/'.$row->SD_ID; ?>">
								<h6 style=""><?php echo $row->SD_NAME; ?></h6>
								<span class="glyphicon glyphicon-folder-close" style="font-size:73px;color:#D8BA52;"></span>
							</a>
							<?php if($row->CRE_BY==$userId){
								?>
								<div class="col-md-12">

									<button type="button" href="<?php echo base_url('SkillDevelopment/editDirectory/'.$row->SD_ID.'/'.$parentId); ?>" title="Edit Directory" data-modal-size="modal-sm"  class="btn btn-warning btn-xs pull-left modalLink" name="button"><span class="glyphicon glyphicon-edit"></span></button>
									<a href="<?php echo base_url("SkillDevelopment/deleteDirectoryFile/d/$row->SD_ID/$actual_link") ?>"><button type="button"  class="btn btn-danger btn-xs pull-right" title="Delete Directory"name="button"><span class="glyphicon glyphicon-trash"></span></button></a>
								</div>

								<?php
							}
							?>
						</center>
					</div>
				<?php }
				?>
				<?php foreach($files as $file) {?>
					<div class="col-md-2 customDiv col-sm-4">
						<?php if($file->ELEMENT_TYPE=='F'){ ?>
							<center>

								<a href="<?php echo base_url($file->FILE_PATH)?>" target="blank">
									<h6 style="color:#000; margin-top:3px"><b><?php echo $file->ELEMENT_TITLE; ?></b> <small> (<?php echo $file->ELEMENT_EXT; ?>)</small></h6>
									<img src="<?php echo getDocUrl($file->ELEMENT_EXT); ?>" class="img-responsive">
								</a>
								<?php if($file->CRE_BY==$userId){
									$url=  base_url("SkillDevelopment/deleteDirectoryFile/f/$file->ELEMENT_ID/$actual_link");
									?>

									<a href="<?php echo $url; ?>"><button type="button"  class="btn btn-danger btn-xs btn-delete" title="Delete Directory"name="button"><span class="glyphicon glyphicon-trash"></span></button></a>
									<?php
								}
								?>
							</center>
							<?php
						}
						else
						{
							?>
							<center>
								<a href="<?php echo $file->ELEMENT_URL; ?>" target="blank">
									<h6 style="color:#000; margin-top:3px"><?php echo $file->ELEMENT_TITLE; ?> <small>( URL)</small></h6>
									<img src="<?php echo getDocUrl('web'); ?>" class="img-responsive">

								</a>
								<?php if($file->CRE_BY==$userId){
									$url=  base_url("SkillDevelopment/deleteDirectoryFile/w/$file->ELEMENT_ID/$actual_link");
									?>

									<a href="<?php echo $url; ?>"><button type="button"  class="btn btn-danger btn-xs btn-delete" title="Delete Directory"name="button"><span class="glyphicon glyphicon-trash"></span></button></a>
									<?php
								}
								?>
							</center>
							<?php
						}
						?>
					</div>
				<?php } ?>
			</div>
		</div>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('portalAssets/tree/default/jstree.min.js') ?>">

		</script>
		<script type="text/javascript">
		$('#html').jstree();
		$(document).on("click", "a.chdCls", function () {
			//alert("okay");
			var url=$(this).attr("href");
			$(this).removeClass('jstree-anchor');
			$(this).removeClass('jstree-clicked');
			window.location.replace(url);
		});
		//$("a.chdCls").removeClass('jstree-anchor');
		//$("a.chdCls").removeClass('jstree-clicked');

		</script>
		<script type="text/javascript">

		$(document).on("change", "input.searchBox", function () {
			var searchUrl=$("input.searchUrl").val();
			var searchKey=$(this).val();
			if(searchKey=='')
			{
				searchKey=0;
			}
			var searchUrl=searchUrl+'/'+searchKey;
			//alert(url);
			$.ajax({
				type: "GET",
				url: searchUrl,
				success: function (data) {
					$("div.searchResult").html(data);
					//  $("div.ajaxLoad").fadeIn(1500);
					//  $('div.ajaxLoader').hide();
				}
			});

		});
		//$("a.chdCls").removeClass('jstree-anchor');
		//$("a.chdCls").removeClass('jstree-clicked');

		</script>
