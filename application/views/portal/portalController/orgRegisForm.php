<link rel="stylesheet" href="<?php echo base_url('portalAssets/homePage') ?>/css/main.css">

<?php
$this->load->library('facebook');
$authUrl=$this->facebook->login_url();
?>
<style media="screen">
.modal .modal-header, .modal .modal-body{
  text-align:center;
}
.pac-card{
  margin: 0px 0px 0 0!important;
  padding: 5px;
}
.modal-dialog{
  width: 400px !important;
  margin-left: 60%;
}
.modal-dialog h4{
  border-bottom: 1px solid #ddd;
  padding-bottom: 20px;
}
.firstBlock,.secondBlock,.thirdBlock{
  height: 450px!important;
}
.sub-btn
{
  margin-bottom: 0px;
}
.pre-btn{
  margin-bottom: 15px !important;
}
.form-control{
  height: 30px ! important;
}
.fileuploader {
    padding: 0px !important;
    background: transparent !important;
    margin:7px 0px !important;
  }
.fileuploader-input-caption {
    padding: 7px 10px !important;
  }
.fileuploader-input-button {
    padding: 7px 20px !important;
}
.modal-footer{
  padding: 0px !important;
}
input[type=text] {
    width: 100% ! important;
}
</style>

<form action="<?php echo site_url('portal/organizationRegis') ?>" method="post" class="formSubmit" enctype='multipart/form-data' novalidate  autocomplete="off">
  <div class="firstBlock">
    <div class="col-md-12">
      <div class="form-group">
        <label for="exampleInputEmail1">ORGANIZATION NAME <span>*</span></label>
        <input type="text"    class="form-control" name="oname" id="oname" required >
        <input type="hidden" name="redirectUrl" value="<?php echo $url; ?>" value="">
      </div>
      <div class="form-group">
        <label >ORGANIZATION EMAIL <span>*</span></label>
        <input type="email" name="oemail" class="form-control emailCheck" id="oemail" required >
        <div class="checkUsermail"></div>
      </div>
      <div class="form-group">
        <label for="exampleInputPassword1">PASSWORD <span>*</span></label>
        <input type="password" class="form-control minLength passwordInd password" name="password" id="password" required>
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">REPEAT PASSWORD <span>*</span></label>
        <input type="password" class="form-control password_conf " name="password_conf_org" id="password_conf"  required>
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">FACEBOOK PAGE URL <span>*</span> </label>
        <input type="text" class="form-control" name="facebook" id="facebook" required  >
      </div>


    </div>
    <div class="row">
      <center>
        <button type="button" class="btn btn-warning   nextBtn" id="signUp">NEXT PAGE</button>
      </center>
    </div>

  </div>
  <div class="secondBlock" style="display:none">

    <div class="col-md-12">
      <div class="form-group">
        <label for="exampleInputEmail1">WEBSITE<span></span></label>
        <input type="text" class="form-control" name="website" id="website" >
      </div>
      <div class="form-group">
        <label  >CONTACT PERSON NAME <span>*</span>  </small></label>
        <input type="text" class="form-control" name="contactPerson" id="cpName"  required="required">
      </div>
      <div class= "form-group customFgp">
        <label  >CONTACT PERSON EMAIL <span>*</span> </small></label>
        <input type="email" class="form-control" name="cpEmail" id="cpEmail"  required="required">
      </div>
      <div class= "form-group customFgp">
        <label  >CONTACT PERSON CELL NO <span>*</span>  </small></label>
        <input type="number" class="form-control onlyNumber" name="number" value=""  id="cpCell"  id="number_org" required="required">
      </div>
      <div class="form-group customFgp">
        <label>Upload Image</label>
        <input type="file" class="form-control orgImg" name="files" id="orgImg" placeholder="Email Address" required>
      </div>
      <button type="button" class="btn btn-warning pre-btn prevButton pull-left"  >PREV PAGE</button>
      <button type="button" class="btn btn-warning pre-btn thirdBtn pull-right"  >NEXT PAGE</button>
    </div>

  </div>
  <div class="thirdBlock" style="display:none">
    <div class="form-group">
      <label for="exampleInputEmail1">ADDRESS <span>*</span></label>
      <div class="mapArea">
      </div>
    </div>
    <label for="checkbox"> <input type="checkbox" name="agree" value="agree" class="checkBox">  I ACCEPT <a href="<?php echo base_url();?>upload/Terms_and_Conditions.pdf" target="_blank"><span>TERMS &amp; CONDITIONS</span></a> AND <a href="<?php echo base_url();?>upload/Privacy_Policy.pdf" target="_blank"><span>PRIVACY POLICY</span></a></label>
    <button type="button" class="btn btn-warning pre-btn prevButtonLast"  >PREV PAGE</button>
    <button type="submit" class="btn btn-default pre-btn sub-btn submitButton">REQUEST FOR REGISTRATION</button>
  </div>
  <?php echo form_close(); ?>
  <script type="text/javascript">
  function isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
  };
  $(document).ready(function(){
    $("button.thirdBtn").click(function(){
      var cpName =$("input#cpName").val();
      var cpEmail =$("input#cpEmail").val();
      var cpCell =$("input#cpCell").val();
      var orgLogo=$("input.orgImg").val();
      if(cpName=='')
      {
        swal({
          text: "Please Enter Contact Person Name",
        });

      }
       else if(cpEmail=='')
      {
        swal({
          text: "Please Enter Contact Person Email",
        });

      }
    else if(orgLogo=='')
     {
       swal({
         text: "Please Select Organization Logo",
       });

     }
       else if(cpCell=='')
      {
        swal({
          text: "Please Enter Contact Person Mobile Number",
        });
      }
      else
      {
        $("div.secondBlock").hide();
        $("div.firstBlock").hide();
        $("div.thirdBlock").show();
        var url = '<?php echo site_url('portal/googleMapLoad') ?>';
        $.ajax({
          type: "POST",
          url: url,
          dataType : 'html',
          success: function(data1) {
            $("div.mapArea").html(data1);
          }
        });
      }


    })
  });
  $(document).ready(function(){
    $("button.prevButtonLast").click(function(){
      $("div.secondBlock").show();
      $("div.firstBlock").hide();
      $("div.thirdBlock").hide();

    })
  });

  $(document).ready(function(){
    $("button.nextBtn").click(function(){
      var orgName=$("input#oname").val();
      var emailId=$("input#oemail").val();
      var password = $(".passwordInd").val();
      var passwordConf  = $(".password_conf").val();
      var facebook=$("input#facebook").val();


      var validEmail=isValidEmailAddress(emailId);
      //alert(validEmail);
      //  alert(orgAddress);


      if(orgName=='')
      {
        swal({
          text: "Please Enter Organization Name",
        });


      }
      else if(emailId=='' || validEmail==false)
      {
        swal({
          text: "Please Enter Organization Email Correctly",
        });
      }
      else if(password=='')
      {
        swal({
          text: "Please Enter Password",
        });

        e.preventDefault();
      }
      else if(passwordConf=='')
      {
        swal({
          text: "Please Enter Confirm Password",
        });
        e.preventDefault();
      }

      else if(facebook=='')
      {
        swal({
          text: "Please Enter Facebook ID",
        });
      }

      else
      {

        $("div.secondBlock").show();
        $("div.firstBlock").hide();
        $("div.thirdBlock").hide();
      }

      //
      // $("div.secondBlock").show();
      // $("div.firstBlock").hide();
      // $("div.thirdBlock").hide();

      // var url = '<?php echo site_url('portal/googleMapLoad') ?>';
      // $.ajax({
      //   type: "POST",
      //   url: url,
      //   dataType : 'html',
      //   success: function(data1) {
      //     $("div.mapArea").html(data1);
      //   }
      // });
      // $("div.secondBlock").show();
      // $("div.firstBlock").hide();
    });




    $("button.prevButton").click(function(){
      $("div.firstBlock").show();
      $("div.secondBlock").hide();
      $("div.thirdBlock").hide();
    });

  });

  </script>

  <script type="text/javascript">
  $(document).on("blur", ".emailCheck", function() {
    var email = $(this).val();
    //alert("okay");
    if(email!='')
    {
      var url = '<?php echo site_url('portal/checkUserEmail') ?>';
      $.ajax({
        type: "POST",
        url: url,
        dataType : 'html',
        data: {email: email},
        success: function(data1) {
          //console.log(data1);
          if(data1 == 1){
            $(this).val('');
            $('.checkUsermail').html("<span class='emailExist' style='color:red'>This Email ID Already Registered</span>");
            $("input.emailCheck").val("");
          }else{
            $('.checkUsermail').html('');
          }

        }
      });
    }
    else
    {
      $("span.emailExist").remove();
    }
  });
  $(function () {
    $("#password_conf").on('blur', function(){
      var password = $(".passwordInd").val();
      var confirmPassword = $(".password_conf").val();
      if (password != confirmPassword) {
        swal("Error!","passwords miss match.");
        $(".password").val('');
        $(".password_conf").val('');
        return false;
      }
      return true;
    });
  });

  $('.formSubmit').on('submit', function (e) {
    //var emailId=$("input#oemail").val();
    var orgAddress=$("input.orgAddress").val();
    //  e.preventDefault();
    //var cpName =$("input#cpName").val();
    //var cpEmail =$("input#cpEmail").val();
    //var cpCell =$("input#cpCell").val();
    //  var checkBox=$("input.checkBox").val();
    if($('input.checkBox').prop('checked'))
    {
      var checkBox=1;
    }
    else
    {
      var checkBox=0;
    }
    if(checkBox==0)
    {
      swal({
        text: "Please Check Agreemen",
      });

      e.preventDefault();
    }


    if(orgAddress=='')
    {
      swal({
        text: "Please Enter Organization Address",
      });
      e.preventDefault();
    }


    else
    {
      //  e.preventDefault();
    }


  });
  </script>
  <script type="text/javascript">
  $('input[name="files"]').fileuploader({
    // Options will go here
  });
  </script>
  <script src="<?php echo base_url(); ?>assets/js/portal-js/jquery.alphanum.js"></script>
<script type="text/javascript">
$(document).ready(function () {
 $('.onlyNumber').numeric();

});
</script>
