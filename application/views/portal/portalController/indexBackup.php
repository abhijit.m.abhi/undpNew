<style media="screen">
  .customFgp{
    margin-top: 0px!important;
    margin-bottom: 0px!important;
  }
  .registerBg{
    border:2px solid #3b5998;
    border-radius: 5px;
    min-height:450px;
  }
</style>

<div class="col-md-12">
  <?php
    echo $this->session->flashdata('user_check');
  ?>
</div>
  <div class="col-md-7">
  <section class="page-title center">
  <img src="<?php echo base_url(); ?>portalAssets/img/logoBig.png"> <br><br>
  <b>Login With </b> <?php if (!empty($authUrl)) {
   echo '<a  href="'.$authUrl.'" class="circle-icon socialIcon">
    <i class="social_facebook"></i></a>';
  }
  ?>
  </section>
  <a href="#" class="circleElement">
    <div class="col-md-2 center">
      <h4> <b>Directory</b></4>
      <div class="rcorners2">
          <div class="alignMent">
              <span class="glyphicon glyphicon-road giFont"></span><br>
          </div>
      </div>
        <span class="bottomText">Directory</span>
    </div>
  </a>
  <a href="#" class="circleElement">
    <div class="col-md-2 center">
      <h4> <b>Calendar</b></4>
      <div class="rcorners2">
          <div class="alignMent">
              <a href="<?php echo site_url('event/eventCalendar'); ?>"><span class="glyphicon glyphicon-calendar giFont"></span></a><br>
          </div>
      </div>
        <span class="bottomText">Calendar</span>
    </div>
  </a>
  <a href="#" class="circleElement">
    <div class="col-md-2 center">
      <h4> <b>Opportunities</b></4>
      <div class="rcorners2">
          <div class="alignMent">
              <span class="	glyphicon glyphicon-hand-up giFont"></span><br>
          </div>
      </div>
        <span class="bottomText">Opportunities</span>
    </div>
  </a>
  <a href="#" class="circleElement">
    <div class="col-md-2 center">
      <h4> <b>Media Blog</b></4>
      <div class="rcorners2">
          <div class="alignMent">
              <span class="	glyphicon glyphicon-folder-open giFont"></span><br>
          </div>
      </div>
      <span class="bottomText">Media Blog</span>
    </div>
  </a>
  <a href="#" class="circleElement">
    <div class="col-md-2 center">
      <h4> <b>Skills </b></4>
      <div class="rcorners2">
          <div class="alignMent">
              <span class="	glyphicon glyphicon-certificate giFont"></span><br>
          </div>
      </div>
      <span class="bottomText">skills</span>
    </div>
  </a>
</div>
<?php $portalUser=$this->session->userdata('user_logged_user');
if ($portalUser) {
  $display ="none";
} else{
  $display ="";
}
?>
<div style="display: <?php echo $display; ?>;" class="col-md-5 registerBg">
  <div class="row">
    <h3><b> Register now</b> </h3>
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#home">Organization</a></li>
      <li><a data-toggle="tab" href="#menu1">Individual</a></li>

    </ul>

    <div class="tab-content">
      <div id="home" class="tab-pane fade in active">
      <?php echo form_open_multipart('portal/organizationRegis'); ?>
       <?php echo $this->session->flashdata('msg'); ?>
       <?php echo $this->session->flashdata('Error'); ?>
       <div class="msg">
         <?php
         if (validation_errors() != false) {
          ?>
          <div class="alert alert-danger">
            <button data-dismiss="alert" class="close" type="button">×</button>
            <?php echo validation_errors(); ?>
          </div>
          <?php
        }
        ?>
      </div>
      <div class="col-md-6">
        <div class= "form-group customFgp">
           <label ><small>Organization Name</small></label>
           <?php echo form_input(array('class' => 'form-control', 'placeholder' => 'Organization Name', 'id' => 'oname', 'name' => 'oname', 'required' => 'required','value' => set_value('oname'))); ?>
        </div>
        <div class= "form-group customFgp">
           <label ><small>Organization Email </small></label>
           <?php echo form_input(array('class' => 'form-control emailCheckOrg', 'placeholder' => 'Organization Email', 'id' => 'oemail', 'name' => 'oemail','type' => 'email',  'required' => 'required')); ?>
           <div id="checkOrgEmail"></div>
        </div>
        <div class= "form-group customFgp">
           <label  ><small>Facebook Page Link </small></label>
           <input type="text" class="form-control" name="facebook" placeholder="Facebook Page Link" required="required">
        </div>
        <div class="form-group customFgp">
            <label><small>Upload Image</small></label>
        <input type="file" class="form-control" name="main_image" id="" placeholder="Email Address" required>
      </div>

      </div>
      <div class="col-md-6">
        <div class= "form-group customFgp">
           <label  ><small>Website</label>
           <input type="text" class="form-control" name="website" placeholder="website" required="required">
        </div>
        <div class= "form-group customFgp">
           <label  >Contact Person Name </small></label>
           <input type="text" class="form-control" name="fname" placeholder="Contact Person Name" required="required">
        </div>
        <div class= "form-group customFgp">
           <label  >Contact Person Email </small></label>
           <input type="email" class="form-control" name="email" placeholder="Contact Person Email" required="required">
        </div>

        <div class= "form-group customFgp">
           <label  >Contact Person Cell No </small></label>
           <input type="text" class="form-control" name="number"  placeholder="Contact Person Mobile Number" id="number_org" required="required">
        </div>
      </div>
      <div class="  col-md-6">
        <div class="form-group customFgp">
            <label><small>Enter Password</small></label>
        <input type="password" class="form-control minLengthOrg password_org password" name="password" id="password_org" placeholder="Password" required>
      </div>
      </div>
      <div class="  col-md-6">
        <div class="form-group customFgp">
            <label><small>Re Enter Password</small></label>
        <input type="password" class="form-control password_conf_org" name="password_conf_org" id="password_conf_org" placeholder="Confirm password" required>
      </div>
      </div>
      <div class="col-md-12" style="">
        <br>
        <?php $this->load->view('template/googleMap'); ?>
        <div class="form-group" style="color:white;">
           <p style="font-size:14px;"><input class="" type="checkbox" id="agree" name="agree"
              value="agree" required="required" checked /><label for="agree" style="color:Black;">I ACCEPT THE <a href="Terms_and_Conditions.pdf" target="_blank" style="text-decoration:underline;">TERMS &amp;CONDITIONS </a><label> and <a href="Privacy_Policy.pdf" target="_blank" style="text-decoration:underline;"> PRIVACY &amp; POLICY</a></label>
           </p>
        </div>
        <div class="form-group">
           <label class="control-label" for=""></label>
           <input type="submit" value="Request for Registration" class="btn btn-primary">
        </div>
        </div>
      <?php echo form_close(); ?>
      </div>
      <div id="menu1" class="tab-pane fade">
        <center>
          <p>Individual Registration</p>
        </center>

<?php echo form_open_multipart('portal/individualRegis'); ?>
<?php echo $this->session->flashdata('msg'); ?>
<?php echo $this->session->flashdata('Error'); ?>
<div class="msg">
   <?php
      if (validation_errors() != false) {
        ?>
   <div class="alert alert-danger">
      <button data-dismiss="alert" class="close" type="button">×</button>
      <?php echo validation_errors(); ?>
   </div>
   <?php
      }
      ?>
</div>
    <div class="col-md-6">
        <div class="form-group customFgp">
          <label><small>Enter Full Name</small></label>
          <input type="text" class="form-control" name="fname" id="" placeholder="Full Name" required>
        </div>
      </div>
        <div class=" inputField col-md-6">
          <div class="form-group customFgp">
              <label><small>Mobile Number</small></label>
          <input type="text" class="form-control" name="number" id="number" placeholder="Mobile Number" required>
        </div>
        </div>

        <div class="  col-md-6">
          <div class="form-group customFgp">
              <label><small>Email Address</small></label>
          <input type="email" class="form-control emailCheck" name="email" id="" placeholder="Email Address" required>
          <div class="checkUsermail"></div>
        </div>
        </div>
        <div class="  col-md-6">
          <div class="form-group customFgp">
              <label><small>Enter Password</small></label>
          <input type="password" class="form-control minLength passwordInd password" name="password" id="password" placeholder="Password" required>
        </div>
        </div>
        <div class="  col-md-6">
          <div class="form-group customFgp">
              <label><small>Re Enter Password</small></label>
          <input type="password" class="form-control password_conf " name="password_conf" id="password_conf" placeholder="Confirm password" required>
        </div>
        </div>

        <div class="  col-md-6">
          <div class="form-group customFgp">
              <label><small>Date Of Birth</small></label>
          <input type="text" class="form-control date_picker" name="date" id="" placeholder="Date Of Birth" required>
        </div>
        </div>
        <div class="  col-md-6">
          <div class="form-group customFgp">
              <label><small>Upload Image</small></label>
          <input type="file" class="form-control" name="main_image" id="" placeholder="Email Address" required>
        </div>
        </div>
        <div class="  col-md-6">
          <div class="form-group ">
              <label><small>Gender</small></label>
              <select class="form-control" style="padding:0px" name="gender" required="required">
                <option value="Male">Male</option>
                <option value="Female">Female</option>
                <option value="Others">Others</option>
              </select>
        </div>
        </div>
        <div class="col-md-12">
          <div class= "form-group customFgp" style="color:white;">
             <p style="font-size:14px;"><input class="" type="checkbox" id="agree" name="agree"
                value="agree" required="required"  checked/><label for="agree" style="color:white;"> I ACCEPT THE <a href="upload/Terms_and_Conditions.pdf" target="_blank" style="color: white; text-decoration:underline;">TERMS &amp;CONDITIONS </a> and <a href="upload/Privacy_Policy.pdf" target="_blank" style="color: white; text-decoration:underline;"> PRIVACY &amp; POLICY</a></label>
             </p>
          </div>
        </div>
        <div class="col-md-12">
            <button type="submit" class="btn btn-primary btn-sm pull-right">Register</button>
        </div>
        <?php echo form_close(); ?>
      </div>
    </div>
  </div>
</div>
 <script src="<?php echo base_url(); ?>assets/js/portal-js/jquery.alphanum.js"></script>
 <script type="text/javascript">
   function validateFeedback() {
     var number = document.getElementById("number").value;
     var reg = new RegExp("[0-9 .-]*");
     return reg.test(number);
   }
  </script>
 <script type="text/javascript">
 $(document).ready(function () {
   $('#number').numeric();
   $('#number_org').numeric();

 });
 </script>
 <script type="text/javascript">
 $('.minLengthOrg').on('blur', function(){
   if($(this).val().length < 6){
     alert('You have to enter at least 6 digit!');
     $(".password").val('');
   }
 });
</script>
<script type="text/javascript">
 $(function () {
   $("#password_conf_org").on('blur', function(){
     var password = $(".password_org").val();
     var confirmPassword = $(".password_conf_org").val();
     if (password != confirmPassword) {
       alert("These passwords miss match.");
       $(".password_org").val('');
       $(".password_conf_org").val('');
       return false;
     }
     return true;
   });
 });
</script>
 <script type="text/javascript">
 $('.minLength').on('blur', function(){
   if($(this).val().length < 6){
     alert('You have to enter at least 6 digit!');
     $(".password").val('');
   }
 });
</script>
 <script type="text/javascript">
 $(function () {
   $("#password_conf").on('blur', function(){
     var password = $(".passwordInd").val();
     var confirmPassword = $(".password_conf").val();
     if (password != confirmPassword) {
       alert("These passwords miss match.");
       $(".password").val('');
       $(".password_conf").val('');
       return false;
     }
     return true;
   });
 });
 </script>
<script type="text/javascript">
$(document).on("blur", ".emailCheck", function() {
  var email = $(this).val();
  //alert("okay");
  if(email!='')
  {
    var url = '<?php echo site_url('portal/checkUserEmail') ?>';
    $.ajax({
      type: "POST",
      url: url,
      dataType : 'html',
      data: {email: email},
      success: function(data1) {
        //console.log(data1);
        if(data1 == 1){
          $(this).val('');
          $('.checkUsermail').html("<span class='emailExist' style='color:red'>This Email ID Already Registered By Individual</span>");
        }else{
          $('.checkUsermail').html('');
        }

      }
    });
  }
  else
  {
    $("span.emailExist").remove();
  }

});
$(document).on("blur", ".emailCheckOrg", function() {
  var email = $(this).val();
  //  alert("kk");
  //alert("okay");
  if(email!='')
  {
    var url = '<?php echo site_url('portal/checkUserEmail') ?>';
    $.ajax({

      type: "POST",
      url: url,
      dataType : 'html',
      data: {email: email},
      success: function(data1) {
        //console.log(data1);
        if(data1 == 1){
         // alert(data1);
          $(".emailCheckOrg").val('');
          $('#checkOrgEmail').html("<span class='emailExist' style='color:red'>This Email ID Already Registered By Organization</span>");
        }else{
          $('#checkOrgEmail').html('');
        }

      }
    });
  }
  else
  {
    $("span.emailExist").remove();
  }

});
</script>
<script type="text/javascript">
var password = document.getElementById("password1")
, confirm_password = document.getElementById("confirm_password1");
var length=password.length;
function validatePassword(){
  var length=password.value.length;
if(password.value != confirm_password.value) {

  confirm_password.setCustomValidity("Passwords Don't Match");
}
else if(length<6)
{
  confirm_password.setCustomValidity("Minimum Password Length Is 6");
}

else {
  confirm_password.setCustomValidity('');
}
}
password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;
</script>
