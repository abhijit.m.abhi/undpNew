<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->

<html class="no-js" lang=""> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>Home</title>
  <link rel="shortcut icon" href="<?php echo base_url(); ?>favicon.ico"/>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="apple-touch-icon" href="apple-touch-Media-blog.svg">
  <link rel="stylesheet" href="<?php echo base_url('portalAssets/homePage') ?>/css/font-awesome.min.css">
  <script type="text/javascript" src="<?php echo base_url(); ?>portalAssets/js/jquery-2.2.1.min.js"></script>
  <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyAsJfFqX2xpb8xlgFvECm4Mo0jXFtyp1zM&libraries=places"></script>
  <link rel="stylesheet" href="<?php echo base_url('portalAssets/homePage') ?>/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url('portalAssets/homePage/icons/styles.css'); ?>">
<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>event/css/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style1.css"> -->
  <link rel="stylesheet" href="<?php echo base_url('portalAssets/homePage') ?>/css/main.css">
  <script src="<?php echo base_url('portalAssets/homePage') ?>/js/vendor/modernizr-2.8.3.min.js"></script>
  <style type="text/css">
  .aft-login-menu{
    text-align: center;
  }
  .aft-login-menu P{
    text-align: center;
    color: #fff;
  }
  #header {
    height: 84vh;
  }
  footer {
    height: 15vh;
    padding:0px;
  }
  @media (max-width: 820px){

    #header {
      height: 100%;
    }
    footer {
      height: 100%;
      padding:10px 0px;
    }
  }
  .logo >a>img {
    height: 70px;
    margin-top: 25px;
  }

  @media (min-width: 768px){
    .sidebar-nav .navbar li a {
      padding-top: 5px;
    }
  }
  @media (min-width: 768px) and (max-width: 1024px){
    .modal-dialog {
        margin-left: 40% !important;
      }
  }

  @media (min-width: 1600px){
    footer {
      padding:10px 0px;
    }
  }

  .menu-item {
    border: 2px solid #fff;
    padding: 20px;
    border-radius: 0%;
    text-align: center;
    position: relative;
    width: 120px;
    height: 120px;
    display: inline-block;
    margin-bottom: 10px;
  }

  .menu-item:hover {
    border: 5px solid #FBC433;
    width: 125px;
    height: 125px;
    border-radius: 0% !important;
}
  .bigIcon{
    font-size: 70px;
    color:#fff;
  }

  .afterLogin:hover>.menu-item{
    border:5px solid #FBC433;
    width: 125px;
    height: 125px;
    border-radius: 0%;
    transition: .5s;
  }

  .menu-item>img {
    width: 65px!important;
    position: relative!important;
    display: inline-block!important;
    margin-top: 25px!important;

  }

  .afterLogin:hover>.menu-item>.bigIcon{
    color: #FBC433;
  }
  .menu-item>img:hover{
  //  background-color: #FBC433;
  }

  body{
    height: 100%;
  }
  .logo>a{
    display: block;
  }
  .menuImg img{
    width: 40px;
    height: auto;
  }

  ul li a{
    /*//  border:1px solid #FBC433;
    //  width: 100%;
    //height: 125px;*/
    color:#fff;

  }
  .miniIcon{
    font-size: 38px;
    color:#fff
  }
  ul li a:hover>.menuImg>.miniIcon{
    color:#FBC433;
    /*//border:2px solid #FBC433;
    //background-color: #FBC433;*/
  }
  ul li a:hover>.col-md-9{
    color:#FBC433;
  }
  .banglaTitle{
    display: none;
  }
  .banglaTitle>span{
    border-top: 2px solid #FBC433;
    color:#FBC433!important;
    padding-top: 3px;
  }
  .banglaTitle2{
    display: none;
  }
  .afterLogin{
    margin-bottom: 20px;
  }
  .afterLogin:hover>p, .afterLogin:hover>p>span{
    color: #FBC433;
    text-decoration: none;
  }

/*  .menuImg>div:before{
    border:2px solid #fff;
    padding: 5px;
  }*/

.magic-text{
  margin-top: -15px;
}
/*a:hover .menuImg>div:before{
  border:2px solid #FBC433;
  }*/

.menuImg{
  margin-bottom: 5px;
}

.row-padd{
  margin: 0px;
}
.row-padd a{
  padding: 0px !important;
}
.row-padd p{
  margin-top: 13px;
}
.row-padd:hover p{
  margin-top: 0px;
 /* -moz-transition: 0.2s ease-in-out;
    -webkit-transition: 0.2s ease-in-out;
    transition: 0.2s ease-in-out;*/
}

@media (min-width: 1010px) and (max-width: 1200px){
  .menuImg {
      margin-right: 15px;
  }
}
@media (min-height: 980px) and (max-height: 1050px){
    #header {
      height: 84vh;
    }
    footer {
      height: 15vh;
    }
  }

  .afterLoginMenu{
    text-decoration: none !important;
  }






/*organization form*/

.form h3 {
    color: #E43533;
    font-weight: 600;
}

.form label {
    color: #012F49;
        filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=70);
    opacity: 0.7;
    font-size: 10px;
    font-weight: 800;
    text-transform: uppercase;
    margin-bottom: 0;
    position: relative;
    bottom: -5px;
}


.note {
    filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=50);
    opacity: 0.5;
    -moz-transform-style: preserve-3d;
    -webkit-transform-style: preserve-3d;
    transform-style: preserve-3d;
    color: #000;
    vertical-align: middle;
    font-size: 12px;
    margin: 5px 0;
}

textarea.form-control{
  height: 34px !important;
}

.form-group>h4, .modal h5{
  font-size: 14px;
      font-weight: 700;
      font-family: 'Raleway', sans-serif;
}

.modal h6{
  font-size: 12px;
      font-weight: 700;
      font-family: 'Raleway', sans-serif;
}

.btn.btn-primary:hover, .btn.btn-primary:active, .btn.btn-primary:active:hover, .btn.btn-primary:focus, .btn.btn-primary:active:focus {
    background-color: #012F49;
    color: #fff;
    border-color: #012F49;
}
.form .btn[type=submit] {
    padding: 13px;
    margin-top: -2px;
}

.btn.btn-rounded {
    -moz-border-radius: 30px;
    -webkit-border-radius: 30px;
    border-radius: 30px;
    margin: 0 auto;
    display: inherit;
}
.btn.btn-primary {
    background-color: #012F49;
    color: #fff;
    border-color: #012F49;
    border-radius: 30px;
}

section>h3{
  margin-top: 0px !important;
}
.portal-icon{
  display: block;
  height: 55px;
  width: 100%;
}
.miniIcon{
  border:2px solid #fff;
  display: block;
  width: 50px;
  height: 50px;
 
  background-size: 30px auto;
}
/*a:hover .menuImg>div:before{
  border:2px solid #FBC433;
  }*/
.row:hover>.portal-icon>.menuImg>.miniIcon{
  border:2px solid #FBC433;
  display: block;
  width: 50px;
  height: 50px;
  padding: 5px;
  
}

.dir{
   background: url(<?php echo base_url(); ?>portalAssets/homePage/img/icon/Directory.png) no-repeat 50% 50%;
   background-size: 30px auto;
}
.row:hover>.portal-icon>.menuImg>.dir{
    background: url(<?php echo base_url(); ?>portalAssets/homePage/img/icon/Directory_yellow.png) no-repeat 50% 50%;
    background-size: 30px auto;
}

.cal{
   background: url(<?php echo base_url(); ?>portalAssets/homePage/img/icon/Calender.png) no-repeat 50% 50%;
   background-size: 30px auto;
}
.row:hover>.portal-icon>.menuImg>.cal{
    background: url(<?php echo base_url(); ?>portalAssets/homePage/img/icon/Calender_yellow.png) no-repeat 50% 50%;
    background-size: 30px auto;
}


.blog{
   background: url(<?php echo base_url(); ?>portalAssets/homePage/img/icon/Media-blog.png) no-repeat 50% 50%;
   background-size: 30px auto;
}
.row:hover>.portal-icon>.menuImg>.blog{
    background: url(<?php echo base_url(); ?>portalAssets/homePage/img/icon/Media-blog_yellow.png) no-repeat 50% 50%;
    background-size: 30px auto;
}

.opp{
   background: url(<?php echo base_url(); ?>portalAssets/homePage/img/icon/Opportunity.png) no-repeat 50% 50%;
   background-size: 30px auto;
}
.row:hover>.portal-icon>.menuImg>.opp{
    background: url(<?php echo base_url(); ?>portalAssets/homePage/img/icon/Opportunity_yellow.png) no-repeat 50% 50%;
    background-size: 30px auto;
}
.skill{
   background: url(<?php echo base_url(); ?>portalAssets/homePage/img/icon/Skill-development.png) no-repeat 50% 50%;
   background-size: 30px auto;
}
.row:hover>.portal-icon>.menuImg>.skill{
    background: url(<?php echo base_url(); ?>portalAssets/homePage/img/icon/Skill-development_yellow.png) no-repeat 50% 50%;
    background-size: 30px auto;
}
.inclu{
   background: url(<?php echo base_url(); ?>portalAssets/homePage/img/icon/Inclusive-Bangladesh.png) no-repeat 50% 50%;
   background-size: 30px auto;
}
.row:hover>.portal-icon>.menuImg>.inclu{
    background: url(<?php echo base_url(); ?>portalAssets/homePage/img/icon/Inclusive-Bangladesh_yellow.png) no-repeat 50% 50%;
    background-size: 30px auto;
}


/*bigIcon*/

/*.bigiIcon{
  border:2px solid #fff;
  display: block;
  width: 50px;
  height: 50px;
  background-size: 30px auto;
}*/
/*a:hover .menuImg>div:before{
  border:2px solid #FBC433;
  }*/
/*.row:hover>.portal-icon>.menuImg>.miniIcon{
  border:2px solid #FBC433;
  display: block;
  width: 50px;
  height: 50px;
  padding: 5px;
  
}*/

.dir2{
   background: url(<?php echo base_url(); ?>portalAssets/homePage/img/icon/Directory.png) no-repeat 50% 50%;
   background-size: 60px auto;
   display: block;
   height: 100%;
}

.afterLogin:hover>.menu-item>.dir2{
    background: url(<?php echo base_url(); ?>portalAssets/homePage/img/icon/Directory_yellow.png) no-repeat 50% 50%;
     background-size: 60px auto;
   display: block;
   height: 100%;
}

.cal2{
   background: url(<?php echo base_url(); ?>portalAssets/homePage/img/icon/Calender.png) no-repeat 50% 50%;
    background-size: 60px auto;
   display: block;
   height: 100%;
}
.afterLogin:hover>.menu-item>.cal2{
    background: url(<?php echo base_url(); ?>portalAssets/homePage/img/icon/Calender_yellow.png) no-repeat 50% 50%;
     background-size: 60px auto;
   display: block;
   height: 100%;
}


.blog2{
   background: url(<?php echo base_url(); ?>portalAssets/homePage/img/icon/Media-blog.png) no-repeat 50% 50%;
    background-size: 60px auto;
   display: block;
   height: 100%;
}
.afterLogin:hover>.menu-item>.blog2{
    background: url(<?php echo base_url(); ?>portalAssets/homePage/img/icon/Media-blog_yellow.png) no-repeat 50% 50%;
     background-size: 60px auto;
   display: block;
   height: 100%;
}

.opp2{
   background: url(<?php echo base_url(); ?>portalAssets/homePage/img/icon/Opportunity.png) no-repeat 50% 50%;
    background-size: 60px auto;
   display: block;
   height: 100%;
}
.afterLogin:hover>.menu-item>.opp2{
    background: url(<?php echo base_url(); ?>portalAssets/homePage/img/icon/Opportunity_yellow.png) no-repeat 50% 50%;
     background-size: 60px auto;
   display: block;
   height: 100%;
}
.skill2{
   background: url(<?php echo base_url(); ?>portalAssets/homePage/img/icon/Skill-development.png) no-repeat 50% 50%;
    background-size: 60px auto;
   display: block;
   height: 100%;
}
.afterLogin:hover>.menu-item>.skill2{
    background: url(<?php echo base_url(); ?>portalAssets/homePage/img/icon/Skill-development_yellow.png) no-repeat 50% 50%;
     background-size: 60px auto;
   display: block;
   height: 100%;
}
.inclu2{
   background: url(<?php echo base_url(); ?>portalAssets/homePage/img/icon/Inclusive-Bangladesh.png) no-repeat 50% 50%;
    background-size: 60px auto;
   display: block;
   height: 100%;
}
.afterLogin:hover>.menu-item>.inclu2{
    background: url(<?php echo base_url(); ?>portalAssets/homePage/img/icon/Inclusive-Bangladesh_yellow.png) no-repeat 50% 50%;
     background-size: 60px auto;
   display: block;
   height: 100%;
}



</style>



</head>
<body>
  <div class="mainContent">
    <?php
    //$url= 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
    $url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $actual_link=str_replace("/","--",$url);
    ?>

    <?php
    $errorMsg=$this->session->flashdata('error');
    $successMsg=$this->session->flashdata('success');
    //echo$this->form_validation->set_message('validationError');
    ?>
    <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <?php
    $portalUser=$this->session->userdata('user_logged_user');
    // echo "<pre>";
    // print_r($portalUser);
    // exit;

    ?>
    <?php
    if (ISSET($portalUser)) {
     $userId=$portalUser['userId'];
    $userDataFormdb= $this->db->query("select u.fname,u.image,u.oname  from users u WHERE u.userId=$userId")->row();
    }
?>

    <section id="header">
      <?php if(!empty($portalUser)){ ?>
        <div class="container">
          <!--          <div class="row">-->
          <!--              <div class="col-xs-6 col-sm-6 logo">-->
          <!--                  <a href="#"><img src="--><?php //echo base_url('portalAssets/homePage') ?><!--/img/logo.png"></a>-->
          <!--              </div>-->
          <!--          </div>-->
          <div class="row">
            <div class="col-xs-4 col-sm-6 logo">
              <a href="<?php echo site_url('portal'); ?>"><img src="<?php echo base_url('portalAssets/homePage') ?>/img/logo.png" class="img-responsive"></a>
            </div>
            <div class="col-xs-8 col-sm-6">
              <div class="account-holder">
                <div class="ac-pic"><img src="<?php
                if($portalUser['login_method']=='D')
                {
                  echo base_url($userDataFormdb->image);
                  $dropDown="dropdown";
                  $caret='<span class="caret"></span>';
                }
                else
                {
                  $dropDown='';
                  $caret='';
                  echo $userDataFormdb->image;
                }

                // echo base_url($portalUser['image']);
                ?>"></div>
                <div class="dropdown ac-holder-dropdown">
                  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="<?php echo $dropDown; ?>" aria-haspopup="true" aria-expanded="true">
                    <?php
                    if($portalUser['type']=='organization')
                    {
                      echo $userDataFormdb->oname;
                    }
                    else
                    {
                      echo $userDataFormdb->fname;
                    }
                    echo $caret;
                    ?>

                  </button>
                  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                    <?php
                    if($portalUser['type']=='individual')
                    {
                      ?>
                      <?php
                      if($portalUser['login_method']=='D')
                      {
                        ?>
                        <li><a href="<?php echo site_url("/portal/visitor_profile_edit_data/".$portalUser['userId']."/".$actual_link) ?>" class="modalLink" data-modal-size="modal-lg" title="Edit User">Edit Profile</a></li>
                        <li><a href="<?php echo site_url("portal/visitor_profile_edit_password/".$portalUser['userId']) ?>" class="modalLink" data-modal-size="modal-md" title="Reset Password" >Reset Password</a></li>
                        <li><a href="<?php echo site_url().'/portal/inactive_account/'.$portalUser['userId']."/".$actual_link ?>" class="modalLink" title="Inactive Account" data-modal-size="modal-md">Inactive Account</a></li>
                       </ul>
                        <!-- <span style="font-weight: 800; font-size: 16px; color: #ffffff;">Please Complete Your Profile!</span> -->
                      </li>
                      <?php
                    }
                    ?>

                    <?php
                  }
                  else
                  {
                    ?>
                    <?php
                    if(isset($portalUser['userId']))
                    {
                      $permUserId=$portalUser['userId'];
                    }
                    else
                    {
                      $permUserId=0;
                    }
                    $this->load->view('portal/globalPhpFunction');
                    $permission=getUserPermission($permUserId);
                    $eventPermission= getPermission(2,$permission);
                    $mediaBlogPermission= getPermission(3,$permission);
                    $opportunitiesPermission= getPermission(4,$permission);
                    $inclussiveBdPermission= getPermission(5,$permission);

                  ?>
                    <li><a href="<?php echo site_url("/portal/visitor_profile_edit_data_org/".$portalUser['userId']."/".$actual_link) ?>" class="modalLink" data-modal-size="modal-lg" title="Edit Organization">Edit Profile</a></li>
                    <?php
                      if($eventPermission>0)
                      {
                        echo '<li><a href="'.site_url('event/eventList').'">My Events</a></li>';
                      }
                      if($mediaBlogPermission>0)
                      {
                        echo '<li><a href="'.site_url('blog/blogList').'">My Media Blog</a></li>';
                      }
                      if($opportunitiesPermission>0)
                      {
                        echo '<li><a href="'.site_url('Opportunity/opportunityList').'">My Opportunities</a></li>';
                      }
                      if($inclussiveBdPermission>0)
                      {
                        echo '<li><a href="'.site_url('InclusiveBd/inclusivebdList').'">My Inclusive Bangladesh</a></li>';
                      }
                    ?>
                     <li><a href="<?php echo site_url("portal/report_problem/".$portalUser['userId']."/".$actual_link) ?>" class="modalLink" data-modal-size="modal-md" title="Send Us A Quick Message">Report Problem</a></li>
                    <li><a href="<?php echo site_url("portal/visitor_profile_edit_password/".$portalUser['userId']) ?>" class="modalLink" data-modal-size="modal-md" title="Reset Password" >Reset Password</a></li>
                    <li><a href="<?php echo site_url().'/portal/inactive_account/'.$portalUser['userId']."/".$actual_link ?>" class="modalLink" title="Inactive Account" data-modal-size="modal-md">Inactive Account</a></li>

                    <?php
                  }
                  ?>
                </ul>
              </div>
              <a href="<?php echo site_url("auth/userLogout/".$actual_link); ?>">LOG OUT</a>
            </div>
          </div>
        </div>
        <!--start alert message when user logged user-->
        <?php
        $portalUser=$this->session->userdata('user_logged_user');
        $errorMsg=$this->session->flashdata('error');
        $successMsg=$this->session->flashdata('success');
        ?>
         <?php if (!empty($portalUser)) {  ?>
          <?php
          if (!empty($errorMsg)) {
            ?>
            <div class="container"><div class="row"><div class="col-sm-10">
            <div class="alert alert-danger">
              <button data-dismiss="alert" class="close close2" type="button">×</button>
              <?php echo $errorMsg; ?>
            </div>
            </div>
            </div>
            </div>
            <?php
          }
          ?>
          <?php
          if (!empty($successMsg)) {
            ?>
            <div class="container"><div class="row"><div class="col-sm-10">
            <div class="alert alert-success">
              <button data-dismiss="alert" class="close close2" type="button">×</button>
              <?php echo $successMsg; ?>
            </div>
            </div>
            </div>
            </div>
            <?php
          }

          ?>

      <?php } ?>
      <!--end alert message when user logged user -->
        <div class="row">
          <div class="aft-login-menu">
            <a href="<?php echo base_url('Orgdirectory') ?>" class="afterLoginMenu">
              <div class="col-sm-2 afterLogin">
                <p>DERECTORY</p>
                <div class="menu-item">
                  <div class=" bigIcon dir2"></div>
                </div>
                <p class="banglaTitle"><span>জংশন</span></p>
              </div>
            </a>
            <a href="<?php echo site_url('Event/eventCalendar') ?>" class="afterLoginMenu">
              <div class="col-sm-2 afterLogin">
                <p>CELENDER</p>
                <div class="menu-item">
                  <div class="bigIcon cal2"></div>
                </div>
                <p class="banglaTitle"><span>দিনোপাখ্যান</span></p>
              </div>
            </a>
            <a href="<?php echo site_url('opportunity/opportunities') ?>" class="afterLoginMenu">
              <div class="col-sm-2 afterLogin">
                <p>OPPORTUNITIES</p>
                <div class="menu-item">
                  <div class=" bigIcon opp2"></div>
                </div>
                <p class="banglaTitle"><span>দূরবীক্ষণ</span></p>
              </div>
            </a>
            <a href="<?php echo site_url('blog/mediaBlogs') ?>" class="afterLoginMenu">

              <div class="col-sm-2 afterLogin">
                <p>MEDIA BLOG</p>
                <div class="menu-item">
                  <div class="bigIcon blog2"></div>
                </div>
                <p class="banglaTitle"><span>প্রতিবিম্ব</span></p>
              </div>
            </a>

            <a href="<?php echo base_url('SkillDevelopment'); ?>" class="afterLoginMenu">
              <div class="col-sm-2  skill-img afterLogin">
                <p>SKILLS DEVELOPMENT</p>
                <div class="menu-item">
                  <div class="bigIcon skill2"></div>
                </div>
                <p class="banglaTitle"><span>শান ঘর</span></p>
              </div>
            </a>
            <a href="<?php echo base_url("InclusiveBd/inclusiveBdAllList") ?>" class="afterLoginMenu">
              <div class="col-sm-2 skill-img afterLogin">
                <p>INCLUSIVE BANGLADESH</p>
                <div class=" menu-item">
                  <div class="bigIcon inclu2"></div>
                </div>
                <p class="banglaTitle"><span>সমগ্র বাংলাদেশ</span></p>
              </div>
            </a>


          </div>
        </div>
      </div>

      <?php
    }
    if(empty($portalUser)){
      ?>
      <style media="screen">
      .modal-dialog{
        margin-left: 60%;
        margin-top: 3%;
        width: 400px;
      }
      </style>
      <div class="container">
        <div class="row">
          <div class="col-xs-6 col-sm-6 logo">
            <a href="<?php echo site_url('portal'); ?>"><img src="<?php echo base_url('portalAssets/homePage') ?>/img/logo.png"></a>
          </div>
          <div class="col-xs-6 col-sm-6">
          </div>
        </div>
        <div class="row">
          <div class="col-sm-4">
            <div class="sidebar-nav">
              <div class="navbar navbar-default" role="navigation">
                <div class="navbar-header">

                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <!-- <span class="visible-xs navbar-brand">Youthopia</span> -->
                </div>
                <div class="navbar-collapse collapse sidebar-navbar-collapse">
                  <ul class="nav navbar-nav ">
                    <li>
                      <div class="row row-padd">
                        <a href="<?php echo base_url('Orgdirectory') ?>" class="portal-icon">
                          <div class="col-xs-2 col-sm-4 col-md-2 menuImg">
                            <div class="miniIcon dir"></div>
                          </div>
                          <div class="col-xs-8 col-sm-7 col-md-9">
                            <p>DIRECTORY</p>
                            <h6 class="banglaTitle2">জংশন</h6>
                          </div>
                        </a>
                      </div>
                    </li>
                      <li>
                        <div class="row row-padd">
                          <a href="<?php echo site_url('Event/eventCalendar') ?>" class="portal-icon">
                            <div class="col-xs-2 col-sm-4 col-md-2 menuImg">
                               <div class=" miniIcon cal"></div>
                            </div>
                            <div class="col-xs-8 col-sm-7 col-md-9">
                              <p>CALENDAR</p>
                              <h6 class="banglaTitle2">দিনোপাখ্যান</h6>
                            </div>
                          </a>
                        </div>
                      </li>
                        <li>
                          <div class="row row-padd">
                            <a href="<?php echo base_url('blog/mediaBlogs') ?>" class="portal-icon">
                              <div class="col-xs-2 col-sm-4 col-md-2 menuImg">
                                 <div class=" miniIcon blog"></div>
                              </div>
                              <div class="col-xs-8 col-sm-7 col-md-9">
                                <p>MEDIA BLOG</p>
                                <h6 class="banglaTitle2">প্রতিবিম্ব</h6>
                              </div>
                            </a>
                          </div>
                        </li>
                        <li>
                            <div class="row row-padd">
                              <a href="<?php echo site_url('opportunity/opportunities') ?>" class="portal-icon">
                                <div class="col-xs-2 col-sm-4 col-md-2 menuImg">
                                   <div class=" miniIcon opp"></div>
                                </div>
                                <div class="col-xs-8 col-sm-7 col-md-9">
                                  <p>OPPORTUNITIES</p>
                                  <h6 class="banglaTitle2">দূরবীক্ষণ</h6>
                                </div>
                              </a>
                            </div>
                        </li>
                        <li>
                            <div class="row row-padd">
                              <a href="<?php echo base_url('SkillDevelopment'); ?>" class="portal-icon">
                                <div class="col-xs-2 col-sm-4 col-md-2 menuImg">
                                  <div class=" miniIcon skill"></div>
                                </div>
                                <div class="col-xs-8 col-sm-7 col-md-9">
                                  <p>SKILLS DEVELOPMENT</p>
                                  <h6 class="banglaTitle2">শান ঘর</h6>
                                </div>
                              </a>
                            </div>
                          </li>
                          <li>
                              <div class="row row-padd">
                                <a href="<?php echo base_url("InclusiveBd/inclusiveBdAllList") ?>" class="portal-icon">
                                  <div class="col-xs-2 col-sm-4 col-md-2 menuImg">
                                    <div class="miniIcon inclu"></div>
                                  </div>
                                  <div class="col-xs-8 col-sm-7 col-md-9">
                                    <p>INCLUSIVE BANGLADESH</p>
                                    <h6 class="banglaTitle2">সমগ্র বাংলাদেশ</h6>
                                  </div>
                                </a>
                              </div>
                            </li>
                            </ul>
                          </div><!--/.nav-collapse -->
                        </div>
                      </div>
                      <div class="clearfix"></div>
                      <div class="magic-text">
                        <p class="bold-text">
                          <span>Building the country <span class="r-text">WE </span>want</br>Revealing the culture <span class="r-text">WE</span> are part of</span>
                        </p>
                      </div>
                    </div>
                    <div class="col-sm-8  modal-message">
                      <?php
                      if (!empty($errorMsg)) {
                        ?>
                        <div class="alert alert-danger">
                          <button data-dismiss="alert" class="close" type="button">×</button>
                          <?php echo $errorMsg; ?>
                        </div>
                        <?php
                      }

                      ?>
                      <?php
                      if (!empty($successMsg)) {
                        ?>
                        <div class="alert alert-success">
                          <button data-dismiss="alert" class="close" type="button">×</button>
                          <?php echo $successMsg; ?>
                        </div>
                        <?php
                      }

                      ?>
                      <h1>WELCOME TO THE </br> LARGEST YOUTH NETWORK</br>IN BANGLADESH</h1>
                      <button class="btn btn-primary btn-lg homeModal" title="WELCOME, FRIEND!" href="<?php echo site_url('portal/loginModalForm/'.$actual_link); ?>">
                        LOG IN
                      </button>
                      <!-- Modal -->
                      <!--
                      <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                      <div class="modal-content">
                      <div class="modal-header">
                      <h4 class="modal-title" id="myModalLabel">LOGIN ERROR</h4>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-default  modal-dismiss" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                  </div>
                </div>
              </div>
            </div> -->
            <button class="btn btn-primary btn-lg individual-btn homeModal" title="SIGN UP AS INDIVIDUAL"  href="<?php echo site_url('portal/individualRegisForm/'.$actual_link); ?>">
              SIGN UP AS INDIVIDUAL
            </button>
            <!-- Modal -->

            <button class="btn btn-primary btn-lg organization-btn homeModal " title="SIGN UP AS ORGANIZATION" href="<?php echo site_url('portal/orgRegisForm/'.$actual_link); ?>">
              SIGN UP AS ORGANIZATION
            </button>
          </div>
        </div>
      </div>
      <?php
    }
    ?>

  </section>

  <?php $this->load->view('portal/template/modal'); ?>
  <?php $this->load->view('portal/template/footer'); ?>

  <!--
  <footer>
  <div class="container">
  <div class="row">
  <div class="col-sm-3  footer_col1 text-center">

  <a href="#" data-toggle="modal" data-target="#about_us_modal"><b>ABOUT US</b></a>
  <a href="#" data-toggle="modal" data-target="#about_us_disclaimer"><b>DISCLAIMER</b></a>
</div>
<div class="col-sm-3  footer_col2 text-center">
<a href="http://www.unv.org/" target="_blank"><img src="<?php echo base_url('portalAssets/homePage') ?>/img/unvbd.png" alt="" class="unvbd_img">
</a>
</div>
<div class="col-sm-3 footer_col2 text-center">
<a href="http://www.bd.undp.org/" target="_blank"><img src="<?php echo base_url('portalAssets/homePage') ?>/img/undp.png" alt="" class="undp_img"></a>
</div>
<div class="col-sm-3  footer_col3 text-left">
<a href="https://www.facebook.com/youthopia.bangla" target="_blank" class="facebook_icon"><i class="fa fa-facebook"></i></a>
<a href="https://twitter.com/youthopiabangla" target="_blank" class="twitter_icon"><i class="fa fa-twitter"></i></a>
<a href="#contact_envelope" data-toggle="modal" class="email_icon"><i class="fa fa-envelope-o"></i></a>
</div>
</div>
<div class="clearfix"></div>
</div>
</footer>
-->
</div> <!-- /container -->
<script>window.jQuery || document.write('<script src="<?php echo base_url('portalAssets/homePage') ?>js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

<script src="<?php echo base_url('portalAssets/homePage') ?>/js/vendor/bootstrap.min.js"></script>

<script src="<?php echo base_url('portalAssets/homePage') ?>/js/plugins.js"></script>
<script src="<?php echo base_url('portalAssets/homePage') ?>/js/main.js"></script>
<script src="<?php echo base_url(); ?>assets/js/portal-js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">

$(document).ready(function(){
  $("a.afterLoginMenu").mouseenter(function(){
    $bt=$(this);
    $bt.find("p.banglaTitle").show();
    //alert($bt.html());
    //$(this).hide();
  });
  $("a.afterLoginMenu").mouseleave(function(){
    $bt=$(this);
    $bt.find("p.banglaTitle").hide();
  });
});

$(document).ready(function(){
  $("li").mouseenter(function(){
    $bt=$(this);
    $bt.find(".banglaTitle2").show();
  });
  $("li").mouseleave(function(){
    $bt=$(this);
    $bt.find(".banglaTitle2").hide();
  });
});
</script>
</div>
</body>
</html>
