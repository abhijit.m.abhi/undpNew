 <style type="text/css">
    .thumb {
        width: 10px;
        height: 10px;
    }

    #list {
        position: absolute;
        top: 0px;
    }

    #list div {
        float: left;
        margin-right: 5px;
    }

    .hovereffect {
        height: 100px;
        margin: auto;
        overflow: hidden;
        transition: all 1s ease 0s;
    }

    .z-img img {
        transition: all 0.2s ease 0s;
    }

    .hovereffect:hover .z-img img {
        transform: scale(1.1);
    }

    .z-text {
        position: absolute;
        width: 40%;
        height: 100px;
        border: 1px solid gray;
        top: -19px;
        z-index: 111;
        display: none;
        color: white;
        text-align: center;
        padding: 5px;
        border: 1px solid white;

    }
    .select-gen{
        border-radius: 0px !important;
        color: #333 !important;
        padding: 0px !important;
        height: 35px !important;
        max-height: auto !important;
    }
    /*.form-group > select{
        margin-top: 14px !important;
    }*/
    .select-gen >option{
        color: #333 !important;
        padding:0px !important;

    }
     .select-gen>button{
       padding: 0px !important;
       border-radius: 0px !important;
   }
   .select-gen>button>span{
       color:#333 !important;
   }
    .division_item{
        position: relative;
        display: inline-block;
        width: 100%;
    }
    .radio label, .checkbox label {
        min-height: 20px;
        padding-left: 30px;
    }

    label{
        font-weight: 700 !important;
        display: inline-block;
    }

    .radio label, .checkbox label {
        min-height: 20px;
        padding-left: 20px !important;
        margin-right: 5px;
    }

    .radio input[type=radio], .radio-inline input[type=radio], .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox] {
        margin-top: 1px;
    }
    .check-style>.checkbox>label{
        width: 100%;
        padding-bottom: 10px;
    }
    .form h3 {
        font-size: 18px;
        margin-top: 20px;
    }
    .modal-content{
        padding-top: 0px !important;
    }
    .modal-title{
        color: #E43533 !important;
        margin-top: 15px;
        font-size: 25px;
        margin-bottom: 8px;
    }
    hr{
        margin: 10px 0px !important;
    }

    .modal-footer {
        padding: 15px;
        text-align: right;
        border-top: 1px solid #e5e5e5;
    }

    .ac-holder-dropdown >button {
        background-color: transparent !important;
        color: #fff !important;
        border: 0px !important;
        outline: 0px !important;
        box-shadow: none !important;
    }


.ac-holder-dropdown >button {
    background-color: transparent !important;
    color: #fff !important;
    border: 0px !important;
    outline: 0px !important;
    box-shadow: none !important;
}
.modal-header h4 {
    border-bottom: 0px;
    padding-bottom: 0px;
}

.z-img{
    height: 150px;
    display: table;
    width: 100%;
    text-align: center;
    padding: 0px;
    border: 1px solid #ddd;
    border-radius: 4px;
}

.helpppp{
    display: table-cell;
    text-align: center;
    vertical-align: middle;
    height: 147px;
}
.z-img>span>img{
    vertical-align: middle;
    display: inline-block !important;
    width: auto !important;
    max-height: 100%;
    max-width: 100%;
}
</style>
<style>
    .new_rz_box {
        display: block;
        margin-top: 40px;
        padding: 10px 0;
        border-bottom: 1px solid #ccc;
    }

    .new_rz_box h6 {
        color: #446678;
        text-transform: uppercase;
    }

    .new_rz_box h5 {
        color: #446678;
        text-transform: uppercase;
    }

    .new_rz_box h3 {
        color: #446678;
        text-transform: uppercase;
    }

    .new_rz_box h4 {
        color: #333;
    }

    .new_rz_box2 {
        display: block;
        padding: 10px 0;
    }

    .new_rz_box2 h3 {
        color: #446678;
        margin-bottom: 10px;
    }

    .new_rz_box2 input {
        border: 1px solid #d9d9d9;
        width: 100%;
        padding: 5px;
    }

    .new_rz_box2 textarea {
        border: 1px solid #d9d9d9;
        width: 100%;
        padding: 10px 20px;
    }

    .new_rz_box2 select {
        border: 1px solid #d9d9d9;
        width: 100%;
        padding: 5px 10px;
    }

    .rz_button3 {
        margin-top: 40px;
        border: 0;
        background-color: #012F49;
        color: #FFF;
        font-size: 25px;
        text-align: center;
        padding: 10px 20px;
        text-transform: uppercase;
        border-radius: 10px;
    }

    .rz_button3:hover {
        color: #FFF;
    }
</style>
<style type="text/css">
    .thumb {
        width: 50px;
        height: 50px;
    }

    #list {
        position: absolute;
        top: 0px;
    }

    #list div {
        float: left;
        margin-right: 10px;
    }

    .hovereffect {
        height: 150px;
        margin: auto;
        overflow: hidden;
        transition: all 1s ease 0s;
    }

    .z-img img {
        transition: all 0.6s ease 0s;
    }

    .hovereffect:hover .z-img img {
        transform: scale(1.9);
    }

    .z-text {
        position: absolute;
        width: 90%;
        height: 200px;
        border: 1px solid gray;
        top: -199px;
        z-index: 1111;
        display: none;
        color: white;
        text-align: center;
        padding: 10px;
        border: 1px solid white;

    }

.modal-header h4 {
    margin-top: 15px;
    font-size: 25px;
    margin-bottom: 8px;
    border-bottom: 1px solid #ddd !important;
    padding-bottom: 20px !important;
}
</style>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>event/css/style.css">
<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style1.css"> -->
<?php
$division_area_result= explode(",", $user_detail->divarea);
$iao_result= explode(",", $user_detail->iao);
$rauthority_result= explode(",", $user_detail->rauthority);
//print_r($rauthority_result);exit();
$vogt_name_result= explode(",", $user_detail->vogt_name);
 //echo "<pre>";print_r($division_area_result);exit();
?>
<script src="<?php echo base_url(); ?>assets/js/portal-js/bootstrap-datetimepicker.min.js"></script>
<link href="<?php echo base_url(); ?>assets/vendors/fileuploader/src/jquery.fileuploader.css" media="all" rel="stylesheet">
<link href="<?php echo base_url();?>old/assets/vendors/fileuploader/examples/thumbnails/css/jquery.fileuploader-theme-thumbnails.css" media="all" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/vendors/fileuploader/src/jquery.fileuploaderOrgLogo.min.js"></script>

    <div id="listingMessage"></div>
    <form action="<?php echo site_url('portal/visitor_profile_org_form_insert/'. $user_detail->userId) ?>" method="post" id="listingForm"  class="formSubmit form inputs-underline" enctype='multipart/form-data'>
        <input type="hidden" name="userId" id="userId" value="<?php echo $user_detail->userId; ?>">
        <input type="hidden" name="listingId" id="listingId" value="<?php echo $user_detail->id; ?>">
        <section>
            <h3>Update your information</h3>
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="form-group">
                        <label for="title">Name of Organization *</label>
                        <input type="text" class="form-control" required="required" name="oname" id="" value="<?php echo $user_detail->oname; ?>">
                        <input type="hidden" name="redirectUrl" value="<?php echo $url; ?>">
                    </div>
                    <p class="note">e.g. United Nations Volunteers (UNV) programme</p>
                    <!--end form-group-->
                </div>
                <!--end col-md-9-->
                <div class="col-md-6 col-sm-6">
                    <div class="form-group">
                        <label for="category">The Scope of Your Organization *</label>
                        <select class="form-control select-gen" name="category" id="category" required="required" style="margin-top: 0px !important;">
                            <option value="">Scope</option>
                            <option value="Local"<?php echo ($user_detail->category == 'Local') ? 'selected' : '' ?>>Local</option>
                            <option value="National"<?php echo ($user_detail->category == 'National') ? 'selected' : '' ?>>National</option>
                            <option value="International"<?php echo ($user_detail->category == 'International') ? 'selected' : '' ?>>International</option>
                        </select>
                    </div>
                    <!--end form-group-->
                </div>
                <!--col-md-3-->
            </div>
            <!--end row-->
            <div class="form-group">
                <label for="description">Tell the world what your organisation is doing *</label>
                <textarea class="form-control" id="description"  required="required" rows="4" name="description"><?php echo $user_detail->description; ?></textarea>
            </div>
            <p class="note">Your Vision, Mission or Objective ( 200 to 3000 characters).</p>
        </section>
        <section>
            <h3>Organizaion Contact Information</h3>
            <div class="row">
              <div class="col-md-12 col-sm-12">
                 <!--end form-group-->
                 <div class="form-group">
                    <label for="address-autocomplete">Full Address *</label>
                    <input type="text" class="form-control orgAddress" name="addressLoc" id="address-autocomplete" required="required" placeholder="Enter a location" value="<?php echo $user_detail->location; ?>">
                </div>
                 <div style="height: 200px; width: 100%; display: block">
                    <div id="map-canvas" style="width: 100%; height: 100%"></div>
                    <div class="mapArea"></div>
                </div>
              <!--end map-->
              <div class="form-group hidden">
                <input type="text" class="form-control hidden" id="latitude" name="latitude" hidden="" value="<?php echo $user_detail->latitude; ?>">
                <input type="text" class="form-control hidden" id="longitude" name="longitude" hidden="" value="<?php echo $user_detail->longitude; ?>">
            </div>
            <p class="note">Enter the exact address or drag the map marker to position</p>
        </div>
        <!--end col-md-6-->
        <div class="col-md-12 col-sm-12">
            <div class="form-group col-md-4 col-sm-4">
                <label for="region">District *</label>
                <select class="form-control select-gen" name="district" required="required" id="region" style="margin-top: 0px !important;">
                    <option value="">Select Region</option>
                    <option value="Bagerhat"<?php echo ($user_detail->district == 'Bagerhat') ? 'selected' : '' ?>>Bagerhat</option>
                    <option value="Bandarban"<?php echo ($user_detail->district == 'Bandarban') ? 'selected' : '' ?>>Bandarban</option>
                    <option value="Barguna"<?php echo ($user_detail->district == 'Barguna') ? 'selected' : '' ?>>Barguna</option>
                    <option value="Barisal"<?php echo ($user_detail->district == 'Barisal') ? 'selected' : '' ?>>Barisal</option>
                    <option value="Bhola"<?php echo ($user_detail->district == 'Bhola') ? 'selected' : '' ?>>Bhola</option>
                    <option value="Bogra"<?php echo ($user_detail->district == 'Bogra') ? 'selected' : ''  ?>>Bogra</option>
                    <option value="Brahmmanbaria"<?php echo ($user_detail->district == 'Brahmmanbaria') ? 'selected' : '' ?>>Brahmmanbaria</option>
                    <option value="Chandpur"<?php echo ($user_detail->district == 'Chandpur') ? 'selected' : '' ?>>Chandpur</option>
                    <option value="Chapai Nawabganj"<?php echo ($user_detail->district == 'Chapai Nawabganj') ? 'selected' : '' ?>>Chapai Nawabganj</option>
                    <option value="Chittagong"<?php echo ($user_detail->district == 'Chittagong') ? 'selected' : '' ?>>Chittagong</option>
                    <option value="Chuadanga"<?php echo ($user_detail->district == 'Chuadanga') ? 'selected' : '' ?>>Chuadanga</option>
                    <option value="Comilla"<?php echo ($user_detail->district == 'Comilla') ? 'selected' : '' ?>>Comilla</option>
                    <option value="CoxsBazar"<?php echo ($user_detail->district == 'CoxsBazar') ? 'selected' : '' ?>>Cox’s Bazar</option>
                    <option value="Dhaka"<?php echo ($user_detail->district == 'Dhaka') ? 'selected' : '' ?>>Dhaka</option>
                    <option value="Dinajpur"<?php echo ($user_detail->district == 'Dinajpur') ? 'selected' : '' ?>>Dinajpur</option>
                    <option value="Faridpur"<?php echo ($user_detail->district == 'Faridpur') ? 'selected' : '' ?>>Faridpur</option>
                    <option value="Feni"<?php echo ($user_detail->district == 'Feni') ? 'selected' : '' ?>>Feni</option>
                    <option value="Gaibandha"<?php echo ($user_detail->district == 'Gaibandha') ? 'selected' : '' ?>>Gaibandha</option>
                    <option value="Gazipur"<?php echo ($user_detail->district == 'Gazipur') ? 'selected' : '' ?>>Gazipur</option>
                    <option value="Gopalganj"<?php echo ($user_detail->district == 'Gopalganj') ? 'selected' : '' ?>>Gopalganj</option>
                    <option value="Habiganj"<?php echo ($user_detail->district == 'Habiganj') ? 'selected' : '' ?>>Habiganj</option>
                    <option value="Jamalpur"<?php echo ($user_detail->district == 'Jamalpur') ? 'selected' : '' ?>>Jamalpur</option>
                    <option value="Jessore"<?php echo ($user_detail->district == 'Jessore') ? 'selected' : '' ?>>Jessore</option>
                    <option value="Jhalakhati"<?php echo ($user_detail->district == 'Jhalakhati') ? 'selected' : '' ?>>Jhalakhati</option>
                    <option value="Jhenaidah"<?php echo ($user_detail->district == 'Jhenaidah') ? 'selected' : '' ?>>Jhenaidah</option>
                    <option value="Joypurhat"<?php echo ($user_detail->district == 'Joypurhat') ? 'selected' : '' ?>>Joypurhat</option>
                    <option value="Khagrachhari"<?php echo ($user_detail->district == 'Khagrachhari') ? 'selected' : '' ?>>Khagrachhari</option>
                    <option value="Khulna"<?php echo ($user_detail->district == 'Khulna') ? 'selected' : '' ?>>Khulna</option>
                    <option value="Kishoreganj"<?php echo ($user_detail->district == 'Kishoreganj') ? 'selected' : '' ?>>Kishoreganj</option>
                    <option value="Kurigram"<?php echo ($user_detail->district == 'Kurigram') ? 'selected' : '' ?>>Kurigram</option>
                    <option value="Kushtia"<?php echo ($user_detail->district == 'Kushtia') ? 'selected' : '' ?>>Kushtia</option>
                    <option value="Lakshmipur"<?php echo ($user_detail->district == 'Lakshmipur') ? 'selected' : '' ?>>Lakshmipur</option>
                    <option value="Lalmonirhat"<?php echo ($user_detail->district == 'Lalmonirhat') ? 'selected' : '' ?>>Lalmonirhat</option>
                    <option value="Madaripur"<?php echo ($user_detail->district == 'Madaripur') ? 'selected' : '' ?>>Madaripur</option>
                    <option value="Magura"<?php echo ($user_detail->district == 'Magura') ? 'selected' : '' ?>>Magura</option>
                    <option value="Manikganj"<?php echo ($user_detail->district == 'Manikganj') ? 'selected' : '' ?>>Manikganj</option>
                    <option value="Maulvibazar"<?php echo ($user_detail->district == 'Maulvibazar') ? 'selected' : '' ?>>Maulvibazar</option>
                    <option value="Meherpur"<?php echo ($user_detail->district == 'Meherpur') ? 'selected' : '' ?>>Meherpur</option>
                    <option value="Munshiganj"<?php echo ($user_detail->district == 'Munshiganj') ? 'selected' : '' ?>>Munshiganj</option>
                    <option value="Mymensingh"<?php echo ($user_detail->district == 'Mymensingh') ? 'selected' : '' ?>>Mymensingh</option>
                    <option value="Naogaon"<?php echo ($user_detail->district == 'Naogaon') ? 'selected' : '' ?>>Naogaon</option>
                    <option value="Narail"<?php echo ($user_detail->district == 'Narail') ? 'selected' : '' ?>>Narail</option>
                    <option value="Narayanganj"<?php echo ($user_detail->district == 'Narayanganj') ? 'selected' : '' ?>>Narayanganj</option>
                    <option value="Narsingdi"<?php echo ($user_detail->district == 'Narsingdi') ? 'selected' : '' ?>>Narsingdi</option>
                    <option value="Natore"<?php echo ($user_detail->district == 'Natore') ? 'selected' : '' ?>>Natore</option>
                    <option value="Netrakona"<?php echo ($user_detail->district == 'Netrakona') ? 'selected' : '' ?>>Netrakona</option>
                    <option value="Nilphamari"<?php echo ($user_detail->district == 'Nilphamari') ? 'selected' : '' ?>>Nilphamari</option>
                    <option value="Noakhali"<?php echo ($user_detail->district == 'Noakhali') ? 'selected' : '' ?>>Noakhali</option>
                    <option value="Pabna"<?php echo ($user_detail->district == 'Pabna') ? 'selected' : '' ?>>Pabna</option>
                    <option value="Panchagarh"<?php echo ($user_detail->district == 'Panchagarh') ? 'selected' : '' ?>>Panchagarh</option>
                    <option value="Patuakhali"<?php echo ($user_detail->district == 'Patuakhali') ? 'selected' : '' ?>>Patuakhali</option>
                    <option value="Pirojpur"<?php echo ($user_detail->district == 'Pirojpur') ? 'selected' : '' ?>>Pirojpur</option>
                    <option value="Rajbari"<?php echo ($user_detail->district == 'Rajbari') ? 'selected' : '' ?>>Rajbari</option>
                    <option value="Rajshahi"<?php echo ($user_detail->district == 'Rajshahi') ? 'selected' : '' ?>>Rajshahi</option>
                    <option value="Rangamati"<?php echo ($user_detail->district == 'Rangamati') ? 'selected' : '' ?>>Rangamati</option>
                    <option value="Rangpur"<?php echo ($user_detail->district == 'Rangpur') ? 'selected' : '' ?>>Rangpur</option>
                    <option value="Satkhira"<?php echo ($user_detail->district == 'Satkhira') ? 'selected' : '' ?>>Satkhira</option>
                    <option value="Shariatpur"<?php echo ($user_detail->district == 'Shariatpur') ? 'selected' : '' ?>>Shariatpur</option>
                    <option value="Sherpur"<?php echo ($user_detail->district == 'Sherpur') ? 'selected' : '' ?>>Sherpur</option>
                    <option value="Sirajganj"<?php echo ($user_detail->district == 'Sirajganj') ? 'selected' : '' ?>>Sirajganj</option>
                    <option value="Sunamganj"<?php echo ($user_detail->district == 'Sunamganj') ? 'selected' : '' ?>>Sunamganj</option>
                    <option value="Sylhet"<?php echo ($user_detail->district == 'Sylhet') ? 'selected' : '' ?>>Sylhet</option>
                    <option value="Tangail"<?php echo ($user_detail->district == 'Tangail') ? 'selected' : '' ?>>Tangail</option>
                    <option value="Thakurgaon"<?php echo ($user_detail->district == 'Thakurgaon') ? 'selected' : '' ?>>Thakurgaon</option>
                </select>
            </div>
            <!--end form-group-->
            <div class="form-group col-md-4 col-sm-4">
                <label for="phone">Postal Code</label>
                <input type="text" class="form-control onlyNumber" name="postalcode" id="postalcode" value="<?php echo $user_detail->postalcode; ?>">
            </div>
            <!--end form-group-->
            <div class="form-group col-md-4 col-sm-4">
                <label for="email">Country *</label>
                <input type="text" class="form-control"  name="country" required="required" id="country"  value="<?php echo $user_detail->country; ?>">
            </div>
        </div>
        <div class="col-md-12 col-sm-12">
            <div class="form-group col-md-4 col-sm-4">
                <label for="email">Org Telephone Number*</label>
                <input type="text" required="required" class="form-control onlyNumber" name="telephone" id="telephone" placeholder="" value="<?php echo $user_detail->phone; ?>">
            </div>
            <!--end form-group-->
            <div class="form-group col-md-4 col-sm-4">
                <label for="website">Org Mobile Number*</label>
                <input type="text" required="required" class="form-control onlyNumber" name="mobile" id="mobile" placeholder="" value="<?php echo $user_detail->mobile; ?>">
            </div>
            <!--end form-group-->
            <!--end form-group-->
            <div class="form-group col-md-4 col-sm-4">
                <label for="website">Fax</label>
                <input type="text" class="form-control onlyNumber" name="fax" id="fax" placeholder="" value="<?php echo $user_detail->fax; ?>">
            </div>
            <!--end form-group-->
            <!--end form-group-->
            <div class="form-group col-md-6 col-sm-6">
                <label for="website">Org E-mail*</label>
                <input type="email" class="form-control" name="email" required="required" id="email" value="<?php echo $user_detail->oemail; ?>">
                <p class="note"></p>
            </div>

            <!--end form-group-->
            <!--end form-group-->
            <div class="form-group col-md-6 col-sm-6">
                <label for="website">Website</label>
                <input type="text" class="form-control" name="website" id="website" value="<?php echo $user_detail->website; ?>">
            </div>
            <!--end form-group-->
            <section class="col-md-12 col-sm-12">

                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                            <label for="facebook">Facebook URL *</label>
                            <input type="text" class="form-control" name="facebook" required="required" id="facebook" value="<?php echo $user_detail->facebook; ?>">
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <!--end form-group-->
                        <div class="form-group">
                            <label for="twitter">Twitter URL</label>
                            <input type="text" class="form-control" name="twitter" id="twitter" placeholder="" value="<?php echo $user_detail->twitter; ?>">
                        </div>
                    </div>

                    <!--end col-md-6-->
                </div>
                <!--end row-->
            </section>
            <!--end form-group-->
            <div class="form-group col-md-4 col-sm-4">
                <label for="hoo">Head of Organization *</label>
                <input type="text" class="form-control" name="hoo" id="hoo" required="required" value="<?php echo $user_detail->hoo; ?>">
            </div>
            <!--end form-group-->
            <!--end form-group-->
            <div class="form-group col-md-4 col-sm-4">
                <label for="hoodestination">Designation *</label>
                <input type="text" class="form-control" name="hoodestination" id="hoodestination" required="required" value="<?php echo $user_detail->hoodesignation; ?>">
            </div>
            <!--end form-group-->
            <!--end form-group-->
            <div class="form-group col-md-4 col-sm-4">
                <label for="hooemail">Email Address</label>
                <input type="email" class="form-control" name="hooemail" id="hooemail" value="<?php echo $user_detail->hooemail; ?>">
            </div>
            <!--end form-group-->
            <!--end form-group-->
            <div class="form-group col-md-3 col-sm-3">
                <label for="contactperson">Name of the contact person with in your organization *</label>
                <input type="text" class="form-control" name="contactperson" required="required" id="contactperson" value="<?php echo $user_detail->contactperson; ?>">
            </div>
            <!--end form-group-->
            <!--end form-group-->
            <div class="form-group col-md-3 col-sm-3">
                <label for="cpdestination">Destination *</label>
                <input type="text" class="form-control" name="cpdestination" required="required" id="cpdestination" value="<?php echo $user_detail->cpdesignation; ?>">
            </div>
            <!--end form-group-->
            <!--end form-group-->
            <div class="form-group col-md-3 col-sm-3">
                <label for="cpperson">Phone *</label>
                <input type="text" class="form-control onlyNumber" name="cpphone" required="required" id="cpphone" value="<?php echo $user_detail->cpperson; ?>">
            </div>
            <!--end form-group-->
            <!--end form-group-->
            <div class="form-group col-md-3 col-sm-3">
                <label for="cpemail">Email Address * </label>
                <input type="email" class="form-control" name="cpemail" id="cpemail" required="required" value="<?php echo $user_detail->cpemail; ?>">
            </div>
            <!--end form-group-->
            <!--end form-group-->
            <div class="row col-md-12 col-sm-12">
                <div class="form-group col-md-4 col-sm-4">
                    <label for="edo">Establishment Date of Organization *</label>
                    <input type="text" name="edo" id="edo_2" required="required" class="form-control date_picker"  placeholder="DD-MM-YY"  value="<?php echo date('d-m-Y', strtotime($user_detail->edo)); ?>">
                </div>
                <!--end form-group-->
                <!--end form-group-->
                <div class="form-group col-md-8 col-sm-8">
                    <!-- <label for="wab>Working Area in Bangladesh *</label> -->

                    <label class="radio-inline">
                        <input type="radio" value="All district" class="alldisban HIDE_SHOW_STATUS" name="wab" id="PARTICULAR_DISTRICT_STATUS"<?php echo ($user_detail->wab == 'All district') ? 'checked' : '' ?>>All district in bangladesh
                    </label>
                    <label class="radio-inline" style="margin-top: 3px;">
                        <input type="radio" value="particular district" class="partdis HIDE_SHOW_STATUS" name="wab" id="PARTICULAR_DISTRICT_STATUS"<?php echo ($user_detail->wab == 'particular district') ? 'checked' : '' ?>>Particular Districts
                    </label>

                </div>

                <div class="div_wrap show_div_wrap " style="display: none">
                    <div class="form-group col-md-12 col-sm-12">
                        <h4>Number of district/s *</h4>
                        <input type="text" name="nod" id="nod" class="form-control onlyNumber" value="<?php echo $user_detail->nod; ?>">
                    </div>

                    <div class="form-group col-md-12 col-sm-12">
                        <h5>Division area</h5>
                        <div id="rediv" class="division_item"></div>
                        <div id="Dhaka" class="division_item">
                            <h6>Dhaka Division *</h6>
                            <div class="checkbox">

                                <?php
                                   // echo $key = array_search('Dhaka',$division_area_result);
                                    //print_r($division_area_result);exit();
                                function getCheckedCt($cityName,$array)
                                {
                                    $i=0;
                                    foreach($array as $key=>$value)
                                    {
                                        if($value==$cityName)
                                        {
                                            $i++;
                                        }
                                    }
                                    if($i>0)
                                    {
                                        $checked='checked';
                                    }
                                    else
                                    {
                                        $checked='';
                                    }
                                    return $checked;


                                }
                                         //$key = array_search('greend', $array);
                                        // print_r($division_area_result);exit();


                                ?>
                                <label>
                                    <input type="checkbox" name="divarea[]"  value="Dhaka"
                                    <?php echo getCheckedCt('Dhaka',$division_area_result); ?>
                                    >
                                    Dhaka
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Faridpur"
                                    <?php echo getCheckedCt('Faridpur',$division_area_result); ?>
                                    >
                                    Faridpur
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Gazipur" <?php echo getCheckedCt('Gazipur',$division_area_result); ?>>
                                    Gazipur
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Gopalganj"<?php echo getCheckedCt('Gopalganj',$division_area_result); ?>>
                                    Gopalganj
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Kishoreganj"<?php echo getCheckedCt('Kishoreganj',$division_area_result); ?>>
                                    Kishoreganj
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Madaripur"<?php echo getCheckedCt('Madaripur',$division_area_result); ?>>
                                    Madaripur
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Manikganj"<?php echo getCheckedCt('Manikganj',$division_area_result); ?>>
                                    Manikganj
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Munshiganj"<?php echo getCheckedCt('Munshiganj',$division_area_result); ?>>
                                    Munshiganj
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Narayanganj"<?php echo getCheckedCt('Narayanganj',$division_area_result); ?>>
                                    Narayanganj
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Narsingdi"<?php echo getCheckedCt('Narsingdi',$division_area_result); ?>>
                                    Narsingdi
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Rajbari"<?php echo getCheckedCt('Rajbari',$division_area_result); ?>>
                                    Rajbari
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Shariatpur"<?php echo getCheckedCt('Shariatpur',$division_area_result); ?>>
                                    Shariatpur
                                </label>
                                <label>
                                    <input type="checkbox"name="divarea[]" value="Tangail"<?php echo getCheckedCt('Tangail',$division_area_result); ?>>
                                    Tangail
                                </label>

                            </div>
                        </div>

                        <div id="Chittagong" class="division_item">
                            <h6>Chittagong Division *</h6>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Bandarban"<?php echo getCheckedCt('Bandarban',$division_area_result); ?>>
                                    Bandarban
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Brahmmanbaria"<?php echo getCheckedCt('Brahmmanbaria',$division_area_result); ?>>
                                    Brahmmanbaria
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Chandpur"<?php echo getCheckedCt('Chandpur',$division_area_result); ?>>
                                    Chandpur
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Chittagong"<?php echo getCheckedCt('Chittagong',$division_area_result); ?>>
                                    Chittagong
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Comilla"<?php echo getCheckedCt('Comilla',$division_area_result); ?>>
                                    Comilla
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="CoxsBazar"<?php echo getCheckedCt('CoxsBazar',$division_area_result); ?>>
                                    Cox’s ?Bazar
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Feni"<?php echo getCheckedCt('Feni',$division_area_result); ?>>
                                    Feni
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Khagrachhari"<?php echo getCheckedCt('Khagrachhari',$division_area_result); ?>>
                                    Khagrachhari
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Lakshmipur"<?php echo getCheckedCt('Lakshmipur',$division_area_result); ?>>
                                    Lakshmipur
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Noakhali"<?php echo getCheckedCt('Noakhali',$division_area_result); ?>>
                                    Noakhali
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Rangamati"<?php echo getCheckedCt('Rangamati',$division_area_result); ?>>
                                    Rangamati
                                </label>
                            </div>
                        </div>


                        <div id="Rajshahi" class="division_item">
                            <h6>Rajshahi Division *</h6>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Bogra"<?php echo getCheckedCt('Bogra',$division_area_result); ?>>
                                    Bogra
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="ChapaiNawabganj"<?php echo getCheckedCt('ChapaiNawabganj',$division_area_result); ?>>
                                    Chapai Nawabganj
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Joypurhat"<?php echo getCheckedCt('Joypurhat',$division_area_result); ?>>
                                    Joypurhat
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Naogaon"<?php echo getCheckedCt('Naogaon',$division_area_result); ?>>
                                    Naogaon
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Natore"<?php echo getCheckedCt('Natore',$division_area_result); ?>>
                                    Natore
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Pabna"<?php echo getCheckedCt('Pabna',$division_area_result); ?>>
                                    Pabna
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Rajshahi"<?php echo getCheckedCt('Rajshahi',$division_area_result); ?>>
                                    Rajshahi
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Sirajganj"<?php echo getCheckedCt('Sirajganj',$division_area_result); ?>>
                                    Sirajganj
                                </label>
                            </div>
                        </div>


                        <div id="Khulna" class="division_item">
                            <h6>Khulna Division *</h6>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Bagerhat"<?php echo getCheckedCt('Bagerhat',$division_area_result); ?>>
                                    Bagerhat
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Chuadanga"<?php echo getCheckedCt('Chuadanga',$division_area_result); ?>>
                                    Chuadanga
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Jhenaidah"<?php echo getCheckedCt('Jhenaidah',$division_area_result); ?>>
                                    Jhenaidah
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Jessore"<?php echo getCheckedCt('Jessore',$division_area_result); ?>>
                                    Jessore
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Khulna"<?php echo getCheckedCt('Khulna',$division_area_result); ?>>
                                    Khulna
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Kushtia"<?php echo getCheckedCt('Kushtia',$division_area_result); ?>>
                                    Kushtia
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Magura"<?php echo getCheckedCt('Magura',$division_area_result); ?>>
                                    Magura
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Meherpur"<?php echo getCheckedCt('Meherpur',$division_area_result); ?>>
                                    Meherpur
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Narail"<?php echo getCheckedCt('Narail',$division_area_result); ?>>
                                    Narail
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Satkhira"<?php echo getCheckedCt('Satkhira',$division_area_result); ?>>
                                    Satkhira
                                </label>
                            </div>
                        </div>

                        <div id="Barisal" class="division_item">
                            <h6>Barisal Division *</h6>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Barisal"<?php echo getCheckedCt('Barisal',$division_area_result); ?>>
                                    Barisal
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Bhola"<?php echo getCheckedCt('Bhola',$division_area_result); ?>>
                                    Bhola
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Barguna"<?php echo getCheckedCt('Barguna',$division_area_result); ?>>
                                    Barguna
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Jhalakhati"<?php echo getCheckedCt('Jhalakhati',$division_area_result); ?>>
                                    Jhalakhati
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Pirojpur"<?php echo getCheckedCt('Pirojpur',$division_area_result); ?>>
                                    Pirojpur
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Patuakhali"<?php echo getCheckedCt('Patuakhali',$division_area_result); ?>>
                                    Patuakhali
                                </label>
                            </div>
                        </div>

                        <div id="Sylhet" class="division_item">
                            <h6>Sylhet Division *</h6>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Habiganj"<?php echo getCheckedCt('Habiganj',$division_area_result); ?>>
                                    Habiganj
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Maulvibazar"<?php echo getCheckedCt('Maulvibazar',$division_area_result); ?>>
                                    Maulvibazar
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Sunamganj"<?php echo getCheckedCt('Sunamganj',$division_area_result); ?>>
                                    Sunamganj
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Sylhet"<?php echo getCheckedCt('Sylhet',$division_area_result); ?>>
                                    Sylhet
                                </label>
                            </div>
                        </div>

                        <div id="Rangpur" class="division_item">
                            <h6>Rangpur Division *</h6>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Dinajpur"<?php echo getCheckedCt('Dinajpur',$division_area_result); ?>>
                                    Dinajpur
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Gaibandha"<?php echo getCheckedCt('Gaibandha',$division_area_result); ?>>
                                    Gaibandha
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Kurigram"<?php echo getCheckedCt('Kurigram',$division_area_result); ?>>
                                    Kurigram
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Lalmonirhat"<?php echo getCheckedCt('Lalmonirhat',$division_area_result); ?>>
                                    Lalmonirhat
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Nilphamari"<?php echo getCheckedCt('Nilphamari',$division_area_result); ?>>
                                    Nilphamari
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Panchagarh"<?php echo getCheckedCt('Panchagarh',$division_area_result); ?>>
                                    Panchagarh
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Rangpur"<?php echo getCheckedCt('Rangpur',$division_area_result); ?>>
                                    Rangpur
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Thakurgaon"<?php echo getCheckedCt('Thakurgaon',$division_area_result); ?>>
                                    Thakurgaon
                                </label>
                            </div>
                        </div>

                        <div id="Mymensingh" class="division_item">
                            <h6>Mymensingh Division *</h6>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Jamalpur"<?php echo getCheckedCt('Jamalpur',$division_area_result); ?>>
                                    Jamalpur
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Mymensingh"<?php echo getCheckedCt('Mymensingh',$division_area_result); ?>>
                                    Mymensingh
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Netrakona"<?php echo getCheckedCt('Netrakona',$division_area_result); ?>>
                                    Netrakona
                                </label>
                                <label>
                                    <input type="checkbox" name="divarea[]" value="Sherpur"<?php echo getCheckedCt('Sherpur',$division_area_result); ?>>
                                    Sherpur
                                </label>
                            </div>
                        </div>

                        <!--  <a href="#" class="btn addMoreDiv">Add More</a>  -->
                    </div>
                </div>

            </div>
            <!--end form-group-->

        </div>
        <!--end col-md-12-->
    </div>
</section>
<!--number of employee section-->
<section>
    <h3>Number of Employees</h3>
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="col-md-4 col-sm-4">
                <label for="">Full Time *</label>
                <input type="text" name="fte" required="required" class="form-control onlyNumber" id="fte" value="<?php echo $user_detail->fte; ?>">
            </div>
            <div class="col-md-4 col-sm-4">
                <label for="">Part-Time *</label>
                <input type="text" name="pte" required="required" class="form-control onlyNumber" id="" value="<?php echo $user_detail->pte; ?>">
            </div>
            <div class="col-md-4 col-sm-4">
                <label for="">Volunteers *</label>
                <input type="text" name="volunteers" required="required" class="form-control onlyNumber" id="volunteers"  value="<?php echo $user_detail->volunteers; ?>">
            </div>
        </div>

    </div>
    <h3>Interest Area of the organization *</h3>
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="col-sm-6 col-md-6 check-style">
                <div class="checkbox">
                    <?php
               // echo $key = array_search('Dhaka',$division_area_result);
                //print_r($division_area_result);exit();
                    function getCheckediao($iaoName,$iao_array)
                    {
                        $i=0;
                        foreach($iao_array as $key=>$value)
                        {
                            if($value==$iaoName)
                            {
                                $i++;
                            }
                        }
                        if($i>0)
                        {
                            $checked='checked';
                        }
                        else
                        {
                            $checked='';
                        }
                        return $checked;


                    }
                     //$key = array_search('greend', $array);
                    // print_r($division_area_result);exit();


                    ?>
                    <label><input type="checkbox" name="iao[]" value="Agriculture"<?php echo getCheckediao('Agriculture',$iao_result); ?>> Agriculture</label>
                </div>
                <div class="checkbox">
                  <label><input type="checkbox" name="iao[]" value="Education & Training"<?php echo getCheckediao('Education & Training',$iao_result); ?>>Education & Training</label>
              </div>
              <div class="checkbox">
                  <label><input type="checkbox" name="iao[]" value="Model UN"<?php echo getCheckediao('Model UN',$iao_result); ?>>Model UN</label>
              </div>
              <div class="checkbox">
                  <label><input type="checkbox" name="iao[]" value="Science & Technology"<?php echo getCheckediao('Science & Technology',$iao_result); ?>>Science & Technology</label>
              </div>
              <div class="checkbox">
                  <label><input type="checkbox" name="iao[]" value="Art & Culture"<?php echo getCheckediao('Art & Culture',$iao_result); ?>>Art & Culture (Music, Dance, Literature, Paint, etc.)</label>
              </div>
              <div class="checkbox">
                  <label><input type="checkbox" name="iao[]" value="Law & Justice"<?php echo getCheckediao('Law & Justice',$iao_result); ?>>Law & Justice (Democracy, Good Governance, Policies, etc.)</label>
              </div>
              <div class="checkbox">
                  <label><input type="checkbox" name="iao[]" value="Climate & Environment"<?php echo getCheckediao('Climate & Environment',$iao_result); ?>>Climate & Environment (Disaster Management, Climate Change, Deforestation, Pollution, Biodiversity, Global Warming, etc.)</label>
              </div>
              <div class="checkbox">
                  <label><input type="checkbox" name="iao[]" value="Media & Communication"<?php echo getCheckediao('Media & Communication',$iao_result); ?>>Media & Communication (Film, Photography, Journalism, Communication, etc.)</label>
              </div>

          </div>
          <div class="col-md-6 col-sm-6 check-style">
            <div class="checkbox">
              <label><input type="checkbox" name="iao[]" value="Debate"<?php echo getCheckediao('Debate',$iao_result); ?>>Debate</label>
          </div>
          <div class="checkbox">
              <label><input type="checkbox" name="iao[]" value="Leadership"<?php echo getCheckediao('Leadership',$iao_result); ?>>Leadership</label>
          </div>
          <div class="checkbox">
              <label><input type="checkbox" name="iao[]" value="Tourism"<?php echo getCheckediao('Tourism',$iao_result); ?>>Tourism</label>
          </div>
          <div class="checkbox">
              <label><input type="checkbox" name="iao[]" value="Volunteerism & Community Development"<?php echo getCheckediao('Volunteerism & Community Development',$iao_result); ?>>Volunteerism & Community Development</label>
          </div>
          <div class="checkbox">
              <label><input type="checkbox" name="iao[]" value="Sports"<?php echo getCheckediao('Sports',$iao_result); ?>>Sports (Football, Cricket, Chess, Handball, etc.)</label>
          </div>
          <div class="checkbox">
              <label><input type="checkbox" name="iao[]" value="Human Rights & Social Inclusion"<?php echo getCheckediao('Human Rights & Social Inclusion',$iao_result); ?>>Human Rights & Social Inclusion (LGBT, Disability, Gender, Women and Children, Peace, etc.)</label>
          </div>
          <div class="checkbox">
              <label><input type="checkbox" name="iao[]" value="employment & Entrepreneurship"<?php echo getCheckediao('employment & Entrepreneurship',$iao_result); ?>>Employment & Entrepreneurship (Career, Entrepreneur, Startup, Social Business, etc.)</label>
          </div>
          <div class="checkbox">
              <label><input type="checkbox" name="iao[]" value="others"<?php echo getCheckediao('others',$iao_result); ?>>Others </label>
          </div>

      </div>

  </div>

</div>
<h3>Please specify your registration / affiliation with others</h3>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="col-md-6 col-sm-6 check-style">
            <h6><strong>Registration Authority *</strong></h6>
            <div class="checkbox">
               <?php
               // echo $key = array_search('Dhaka',$division_area_result);
                //print_r($division_area_result);exit();
               function getCheckedRauthority($rauthorityName,$rauthority_array)
               {
                $i=0;
                foreach($rauthority_array as $key=>$value)
                {
                    if($value==$rauthorityName)
                    {
                        $i++;
                    }
                }
                if($i>0)
                {
                    $checked='checked';
                }
                else
                {
                    $checked='';
                }
                return $checked;


            }
                     //$key = array_search('greend', $array);
                    // print_r($division_area_result);exit();


            ?>
            <label><input type="checkbox" name="ra[]" class="regaut" value="Govt"<?php echo getCheckedRauthority('Govt',$rauthority_result); ?>>Registered Under Bangladesh Govt.</label>
        </div>
        <div class="checkbox">
          <label><input type="checkbox" name="ra[]" class="affwfaut" value="Foreign Authority"<?php echo getCheckedRauthority('Foreign Authority',$rauthority_result); ?>>Affiliation With Foreign Authority</label>
      </div>

  </div>
  <div class="col-md-6 col-sm-6 check-style">
     <div class="checkbox">
        <label><input type="checkbox" name="ra[]" class="regunuc" value="University / College"<?php echo getCheckedRauthority('University / College',$rauthority_result); ?>>Registered Under University / College</label>
    </div>
    <div class="checkbox">
        <label><input type="checkbox" name="ra[]" value="Not Registered"<?php echo getCheckedRauthority('Not Registered',$rauthority_result); ?>>Not Registered</label>
    </div>
    <div class="checkbox">
        <label><input type="checkbox" name="ra[]" value="others"<?php echo getCheckedRauthority('others',$rauthority_result); ?>>Others </label>
    </div>
</div>

<div class="col-md-12 col-sm-12" id="regau">
    <h3>Select from List of Govt. body</h3>
    <div class="row">
        <div class="col-md-6   col-sm-12 check-style">
            <div class="checkbox">
                <?php
               // echo $key = array_search('Dhaka',$division_area_result);
                //print_r($division_area_result);exit();
                function getCheckedVogtName($vogt_nameName,$vogt_name_array)
                {
                    $i=0;
                    foreach($vogt_name_array as $key=>$value)
                    {
                        if($value==$vogt_nameName)
                        {
                            $i++;
                        }
                    }
                    if($i>0)
                    {
                        $checked='checked';
                    }
                    else
                    {
                        $checked='';
                    }
                    return $checked;


                }
                     //$key = array_search('greend', $array);
                    // print_r($division_area_result);exit();


                ?>
                <label>
                    <input type="checkbox" name="ra_1[]"  value="City Corporation"<?php echo getCheckedVogtName('City Corporation',$vogt_name_result); ?>>
                    City Corporation
                </label>
                <label>
                    <input type="checkbox" name="ra_1[]"  value="Ministry of Agriculture"<?php echo getCheckedVogtName('Ministry of Agriculture',$vogt_name_result); ?>>
                    Ministry of Agriculture
                </label>
                <label>
                    <input type="checkbox" name="ra_1[]"  value="Microcredit Regulatory Authority (MRA)"<?php echo getCheckedVogtName('Microcredit Regulatory Authority (MRA)',$vogt_name_result); ?>>
                    Microcredit Regulatory Authority (MRA)
                </label>
                <label>
                    <input type="checkbox" name="ra_1[]"  value="Register of Joint Stock Companies and Firms"<?php echo getCheckedVogtName('Register of Joint Stock Companies and Firms',$vogt_name_result); ?>>
                    Register of Joint Stock Companies and Firms
                </label>
                <label>
                    <input type="checkbox" name="ra_1[]"  value="Department of Youth Development / Ministry of Youth Affairs & Sports"<?php echo getCheckedVogtName('Department of Youth Development / Ministry of Youth Affairs & Sports',$vogt_name_result); ?>>
                    Department of Youth Development / Ministry of Youth Affairs & Sports
                </label>
                <label>
                    <input type="checkbox" name="ra_1[]" value="others"<?php echo getCheckedVogtName('others',$vogt_name_result); ?>>Others
                </label>
            </div>
        </div>
        <div class="col-md-6  col-sm-12 check-style">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="ra_1[]"  value="NGO Affairs Bureau"<?php echo getCheckedVogtName('NGO Affairs Bureau',$vogt_name_result); ?>>
                    NGO Affairs Bureau
                </label>
                <label>
                    <input type="checkbox" name="ra_1[]"  value="Ministry of Health and Family Wellfare"<?php echo getCheckedVogtName('Ministry of Health and Family Wellfare',$vogt_name_result); ?>>
                    Ministry of Health and Family Wellfare
                </label>
                <label>
                    <input type="checkbox" name="ra_1[]"  value="Registered Board Deed / Trust Deed"<?php echo getCheckedVogtName('Registered Board Deed / Trust Deed',$vogt_name_result); ?>>
                    Registered Board Deed / Trust Deed
                </label>
                <label>
                    <input type="checkbox" name="ra_1[]"  value="Department of Social Services / Ministry of Social Welfare"<?php echo getCheckedVogtName('Department of Social Services / Ministry of Social Welfare',$vogt_name_result); ?>>
                    Department of Social Services / Ministry of Social Welfare
                </label>
                <label>
                    <input type="checkbox" name="ra_1[]"  value="Department of Women Affairs / Ministry of Women and Children Affairs"<?php echo getCheckedVogtName('Department of Women Affairs / Ministry of Women and Children Affairs',$vogt_name_result); ?>>
                    Department of Women Affairs / Ministry of Women and Children Affairs
                </label>
            </div>
        </div>

        <div class="col-md-12">
            <div class="col-md-6  col-sm-6">
                <h4>GOVT. Registration number *</h4>
                <input type="text" name="registration_no" id="registration_no" class="form-control onlyNumber" value="<?php echo $user_detail->registration_no; ?>">
            </div>
            <div class="col-md-6  col-sm-6">
                <h4>GOVT. Registration date *</h4>
                <input type="text" name="govt_reg_date" id="from_reg" class="form-control date_picker" placeholder="DD-MM-YY" value="<?php echo date('d-m-Y', strtotime($user_detail->govt_reg_date)); ?>">
            </div>
        </div>

    </div>
</div>

<div class="col-md-12" id="reguc">
    <h4>Please specify the name of university / college</h4>
    <input type="text" name="college_name" id="college_name" value="<?php echo $user_detail->college_name; ?>" class="form-control">
</div>

<div class="col-md-12"  id="afwfau">
    <div class="row">
        <div class="col-md-7">
            <h4>Foreign Authority Registration number</h4>
            <input type="text" name="Foreign_reg_number" id="Foreign_reg_number" value="<?php echo $user_detail->reg_college; ?>" class="form-control onlyNumber">
        </div>
        <div class="col-md-5">
            <h4>Foreign Authority Registration Date</h4>
            <input type="text" name="non_gov_reg" id="non_gov_reg" class="form-contro date_picker" placeholder="DD-MM-YY" value="<?php echo date('d-m-Y', strtotime($user_detail->non_gov_reg)); ?>">
        </div>
        <div class="col-md-12">
            <h4>Please specify the name of foreign registration authority</h4>
            <input type="text" name="foreign_reg" id="foreign_reg" class="form-control" value="<?php echo $user_detail->foreign_reg; ?>">
        </div>
    </div>
</div>

</div>

</div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <label>How did you learn about Youthopia.bangla ? *</label>
         <input type="text" class="form-control" name="learnabout" required value="<?php echo $user_detail->learnabout; ?>">
    </div>
</div>

</section>

<span id="blk-image" class="toHide" style="display:block; padding: 0px; margin: 0px;">
    <?php if (!empty($user_detail->id)) { ?>
    <div class="row new_rz_box2">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
       <?php  if($org_image_detail){ ?>
        <h3>Old Images</h3>
            <?php } foreach ($org_image_detail as $image): ?>
                <div class="col-sm-3">
                    <div class="hovereffect">
                        <div class="z-text">
                            <button type="button" style="margin-top: 30%"
                            class="btn btn-danger delete-button" title="Delete"
                            org-id="<?php echo $image->id; ?>"
                            image-name="<?php echo $image->path ?>"><i class="fa fa-remove"
                            aria-hidden="true"></i></button>
                        </div>
                        <div class="z-img">
                            <span class="helpppp">
                            <img src="<?php echo base_url(); ?><?php echo $image->path ?>" alt="post img"
                            class="image  img-responsive postImg margin10"
                            ></span>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <?php } ?>
    <div class="row new_rz_box2">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3>Gallery</h3>
            <input type="file" name="files">
        </div>
    </div>
</span>

<!--number of employee section end-->
<span id="blk-image" class="toHide"   padding: 0px; margin: 0px;">
    <?php if (!empty($user_detail->image)) { ?>
    <div class="row new_rz_box2">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-sm-3">
                <div>
                    <img src="<?php echo base_url(); ?><?php echo $user_detail->image ?>" alt="logo img"
                    width="100" height="100">
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
</span>
<section>
    <h3>logo</h3><p>Ratio : width: 100px X height:  100px</p>
    <div class=""></div>
    <input type="file" name="logoImg">
</section>
<hr>
<input hidden type="text" name="listingData" id="listingData" value="listingData">
<section class="center">
    <div class="form-group">
        <button type="submit" class="btn btn-primary btn-rounded"> Submit Listing</button>
    </div>
    <?php echo form_close(); ?>
    <!--end form-group-->
</section>
</form>
<!--end form-->
</div>
<!--end modal-body-->
</div>
<!--end modal-content-->
</div>
<!--end modal-dialog-->
<script type="text/javascript">
 var wab_value = '<?php echo($user_detail->wab);?>';
   //alert(wab_value);
   $(document).ready(function () {
        //$(document).on('click','#PARTICULAR_DISTRICT_STATUS',function(){
            //var status=$(this).val();
            if('particular district' == wab_value){
                $(".div_wrap").show();
            }else{
                $(".div_wrap").hide();
            }

        });

    </script>

    <script type="text/javascript">
        $(document).on('click','.HIDE_SHOW_STATUS',function(){
            var status=$(this).val();
            //alert(status);
            if(status =='particular district'){
                $(".show_div_wrap").show();

            }else{
                $(".show_div_wrap").hide();
            }
        });

    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            var regautbtn = $('.regaut'),
            regau = $('#regau').hide()
            <?php
            $ra=explode(",",$user_detail->rauthority);
            if(in_array("Govt",$ra))
            {
                ?>

                $('#regau').show();

                <?php
            }
            ?>
            regautbtn.on('click', function(){
              if(jQuery(this).is(":checked")){
                 regau.show('slow');
                 $("#registration_no").prop("required", true);
                 $("#from_reg").prop("required", true);
             }
             else{

                regau.hide('slow');
                $("#registration_no").prop("required", false);
                $("#from_reg").prop("required", false);
            }

            //afwfa.hide();
            //reguuc.hide();
        });

        });

    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            var afwfabtn = $('.affwfaut'),
            afwfa = $('#afwfau').hide()
            <?php
            $ra=explode(",",$user_detail->rauthority);
            if(in_array("Foreign Authority",$ra))
            {
                ?>

                $('#afwfau').show();

                <?php
            }
            ?>
            afwfabtn.on('click', function(){
            //regau.hide();
            //afwfa.toggle('slow');
            //reguuc.hide();

            if(jQuery(this).is(":checked")){
             afwfa.show('slow');
         }
         else{

            afwfa.hide('slow');
        }
    });

        });

    </script>
    <script type="text/javascript">
        $(document).ready(function () {
         reguucbtn = $('.regunuc'),
         reguuc = $('#reguc').hide()
         <?php
         $ra=explode(",",$user_detail->rauthority);
         if(in_array("University / College",$ra))
         {
            ?>

            $('#reguc').show();

            <?php
        }
        ?>
        reguucbtn.on('click', function(){
           // regau.hide();
            //afwfa.hide();
            //reguuc.toggle('slow');
            if(jQuery(this).is(":checked")){
             reguuc.show('slow');
         }
         else{

            reguuc.hide('slow');
        }
    });


    });

</script>
<script type="text/javascript">
  $('input[name="logoImg"]').fileuploader({
            // Options will go here
        });
    </script>
    <script>
        $(document).ready(function () {
            $('.hovereffect').hover(function () {
                $(this).children('.z-text').css('top', '0').fadeToggle(600);
            });
        });

    </script>
    <script>
        $(document).ready(function () {
            $(".delete-button").click(function () {
                var org_id = $(this).attr('org-id');
            //alert(org_id);
            var image_name = $(this).attr('image-name');
            var removeImage = $(this).parent().parent().parent();
            $.ajax({
                url: '<?php echo site_url('portal/orgEditImageDelete') ?>',
                type: "post",
                data: {
                    org_id: org_id,
                    img_name: image_name,
                    action: 'orgEditImgDelete'
                }
            }).done(function (response) {
                //console.log(response);
                var res = $.parseJSON(response);
                alert(res.msg);
                if (res.ack) {
                    removeImage.fadeOut();
                }
            })
        });
        });
    </script>
   <!--  <script type="text/javascript">
       jQuery( function() {
           jQuery( "#edo_2" ).datepicker({ dateFormat: 'dd-mm-yy' });
           jQuery( "#from_reg" ).datepicker({ dateFormat: 'dd-mm-yy' });
           jQuery( "#non_gov_reg" ).datepicker({ dateFormat: 'dd-mm-yy' });
   
       } );
   </script> -->
    <script type="text/javascript">
 $(document).ready(function () {
   $('.date_picker').datepicker({
     format: "dd-mm-yyyy"
 });
});

</script>
  <script src="<?php echo base_url(); ?>assets/js/portal-js/jquery.alphanum.js"></script>
 <script type="text/javascript">
 $(document).ready(function () {
   $('.onlyNumber').numeric();

 });
 </script>
 <script type="text/javascript">
$(document).ready(function(){
    var url = '<?php echo site_url('portal/googleOrgEditMapLoad') ?>';
        $.ajax({
          type: "POST",
          url: url,
          dataType : 'html',
          success: function(data1) {
            $("div.mapArea").html(data1);
          }
        });

        });
</script>
    <script src="<?php echo base_url(); ?>old/assets/vendors/fileuploader/src/jquery.fileuploader.min.js"
        type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>old/assets/vendors/fileuploader/examples/thumbnails/js/custom.js" type="text/javascript"></script>
