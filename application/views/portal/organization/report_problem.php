<style type="text/css">
	.modal-header h4 {
    color: #681156;
    margin-top: 15px;
    font-size: 25px;
    margin-bottom: 8px;
    border-bottom: 1px solid #ddd !important;
    padding-bottom: 20px !important;
}
input[type=text] {
    width:100% ! important;
}

textarea.form-control{
	height: auto !important;
	max-height: none !important;
}
</style>
 <?php echo form_open("portal/report_problem_form_insert/" . $user_detail->userId, "id='report_form'");  ?>
		  <img src="<?php echo base_url(); ?>assets/img/loader.gif" id="gif" style="display: none; margin: 0 auto;"> 
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label" for="name">Name *</label>
						<input type="text" required="required" class="form-control" name="name" id="name"
						placeholder="Please Enter Your Full Name">
						 <input type="hidden" name="redirectUrl" value="<?php echo $url; ?>">
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label" for="email">Email *</label>
						<input type="email" required="required" class="form-control" name="email" id="email"
						placeholder="Please Enter Your Email">
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label" for="subject">Subject</label>
						<input type="text" class="form-control" name="subject" id="subject"
						placeholder="Please Enter Your Subject" required="required">
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label" for="message">Message *</label>
						<textarea name="message" id="message" placeholder="Please Enter Your Message" class="form-control"
						rows="3" required="required" spellcheck="true" ></textarea>
					</div>
				</div>
				<input type="hidden" id="contactForm" name="contactForm" value="$user_detail->userId">

				<div class="col-md-12">
					<div class="form-group">
						<button type="submit" id="" class="btn btn-success btn-block btn-sm">Submit</button>
					</div>
				</div>
			</div>
	<?php echo form_close(); ?>

	<script type="text/javascript">
			$('#report_form').submit(function() {
		    $('#gif').css('display', 'block');
			});
		</script>
	



