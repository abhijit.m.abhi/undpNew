<style>
    .event_calender_slider {
        margin-bottom: 60px;
    }

    .rz_button {
        background-color: #012F49;
        color: #FFF;
        text-align: center;
        display: block;
        margin-top: 10px;
        border-radius: 3px;
    }

    .rz_button2 {
        background-color: #e63433;
        color: #FFF;
        font-size: 15px;
        text-align: center;
        padding: 6px 12px;
        text-transform: uppercase;
        border-radius: 25px;
    }

    .rz_button2:hover {
        color: #FFF;
    }

    .event_calender_slider:after {
        color: #FFFFFF;
        content: "PASSESD OPPORTUNITY";
        font-family: 'ralewaysemibold';
        font-size: 15px;
        left: 0;
        line-height: 1.3;
        position: absolute;
        text-align: center;
        top: 35%;
        width: 106px;
    }

    .frame{
        height: 120px;
        display: table;
        width: 100%;
        text-align: center;
        padding: 0px;
    }
    .helper{
        display: table-cell;
        text-align: center;
        vertical-align: middle;
        height: 118px;
    }
    .frame>span>img {
        vertical-align: middle;
        display: inline-block !important;
        width: auto !important;
        max-height: 100%;
        max-width: 100%;
    }
    .frame2{
        height: 150px;
        display: table;
        width: 100%;
        text-align: center;
        padding: 0px;
    }
    .helper22{
        display: table-cell;
        text-align: center;
        vertical-align: middle;
        height: 150px;
    }
    .frame2>span>img {
        vertical-align: middle;
        display: inline-block !important;
        width: auto !important;
        max-height: 100%;
        max-width: 100%;
    }
</style>

<div class="others_page_wrp">
    <section class="org_map_wrp">
        <div class="container">
            <br/>

            <div class="col-md-12">
                <h2 class="pull-left" style="color:#012F49;">MY OPPORTUNITIES</h2>

                <?php
                  if($permission->PER>0)
                  {
                    ?>
                <a class="rz_button2 pull-right" href="<?php echo site_url();?>opportunity/createOpportunity">Create New Opportunity</a>
                <?php
              }
                ?>
            </div>
            <br/> <br/>
            <hr/>

            <?php if (!empty($opportunitylist)) : ?>

            <div class="row">

                <?php
                foreach ($opportunitylist as $opportunitylists) {
                    $imgName = explode('___', $opportunitylists->images);
                    $imgName = $imgName[0];
                    $postTitle = $opportunitylists->title;
                    if (strlen($postTitle) > 80) {
                        // truncate string
                        $postTitle = substr($postTitle, 0, 80);
                        // make sure it ends in a word so assassinate doesn't become ass...
                        $postTitle = substr($postTitle, 0, strrpos($postTitle, ' ')) . '....';
                    }
                    ?>
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <a href="<?php echo site_url().'opportunity/opportunityView/'.$opportunitylists->id; ?>" target="_blank">
                            <!--                                <a href="#" data-modal-external-file="event_view.php?event_id=-->
                            <?php //echo $opportunity['id'];
                            ?><!--" data-target="modal-submit">-->
                            <div class="panel panelD" style="border: 1px solid #ddd">
                                <div class="thumbnail panel-image embed-responsive embed-responsive-16by9" style="margin-bottom: 2px">
                                <div class="frame2">
                                    <span class="helper22">
                                        <img style=" border-radius: 4px 4px 0px 0px;border-radius: 0px;"
                                             src="<?php echo base_url().'upload/opportunities/' . $imgName ?>"
                                             alt="<?php echo $opportunitylists->title; ?>"/>
                                    </span>
                                </div>

                                </div>
                                <div class="opportunity_calender_txt">
                                    <p style="min-height: 50px; max-height: 50px; overflow-y: hidden"><?php echo $postTitle; ?></p>
                                    <p>By
                                        <span style="min-height: 20px; max-height: 20px; overflow-y: hidden"><?php echo $opportunitylists->org_name; ?> </span><br/><span
                                            class="event_dates">From <?php echo date('d M, Y', strtotime($opportunitylists->start_date)); ?><br>
                                            To <?php echo date('d M, Y', strtotime($opportunitylists->end_date)); ?></span>
                                    </p>
                                </div>

                                <p><a class="rz_button" href="<?php echo site_url().'opportunity/editOpportunityPost/'.$opportunitylists->id; ?>">Edit</a>
                                </p>
                                <p><a class="rz_button" onClick="return confirm('Are you sure you want to delete?')"
                                      href="<?php echo site_url().'opportunity/deleteOpportunityPost/'.$opportunitylists->id; ?>">Delete</a>
                                </p>
                            </div>
                        </a>
                    </div>
                    <?php
                }
                ?>
            </div>

            <?php else: ?>
                <p class="alert alert-warning">No Opportunity post to show!</p>
            <?php endif; ?>

            <h2 style="margin:40px 0 0 0; color:#012F49;">PREVIOUS OPPORTUNITIES</h2><br>

            <?php if(!empty($previousOpportunities)) : ?>

            <div class="previous-event-calender">
                <div id="previous-event-calender-slider">
                    <?php foreach ($previousOpportunities as $opportunity) :
                        $imgName = explode('___', $opportunity->images);
                        $imgName = $imgName[0]; ?>
                        <a target="_blank" href="<?php echo site_url().'opportunity/opportunityView/'.$opportunity->id; ?>">
                            <div class="thumbnail" style="margin-bottom: 5px;">

                                <div class="frame" style=" border-bottom: 1px solid #ddd;">
                                <span class="helper">
                                     <img style="" src="<?php echo base_url();
                                    echo 'upload/opportunities/' . $imgName ?>" alt="">
                                </span>

                                </div>

                                <div class="event_calender_txt">
                                    <p><?php echo $opportunity->category; ?></p>
                                    <p>By <?php echo $opportunity->title; ?> <br/><span
                                                class="event_dates">FROM <?php echo date('d M, Y', strtotime($opportunity->start_date)); ?>
                                            TO <?php echo date('d M, Y', strtotime($opportunity->end_date)); ?></span></p>
                                </div>
                            </div>
                        </a>
                    <?php endforeach; ?>
                </div>
            </div>

            <?php else: ?>
                <p class="alert alert-warning">No previous Opportunity post to show!</p>
            <?php endif; ?>


        </div>
    </section>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        $("#event-calender-slider").owlCarousel({
            autoPlay: 3000,
            pagination: false,
            navigation: true,
            itemsCustom: [[0, 1], [479, 2], [768, 4], [991, 5], [1199, 5]],
        });

        $(function () {
            window.prettyPrint && prettyPrint();

        });

        $("#previous-event-calender-slider").owlCarousel({
            autoPlay: 3000,
            pagination: false,
            navigation: true,
            itemsCustom: [[0, 1], [479, 2], [768, 5], [991, 6], [1199, 5]],
        });
    });
</script>
<script>
    $(document).ready(function () {
        $("#event-calender-slider2").owlCarousel({
            autoPlay: 3000,
            pagination: false,
            navigation: true,
            itemsCustom: [[0, 1], [479, 2], [768, 4], [991, 5], [1199, 5]],
        });

    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.date_picker').datepicker({
            format: "yyyy-mm-dd"
        });
    });
</script>
