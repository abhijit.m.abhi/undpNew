<!--  **************************** FB Imo Start *************************************************** -->
<script>
    $(document).ready(function () {
        $('.FB_reactions').facebookReactions3({
            postUrl: "<?php echo base_url() . 'opportunity/fbImo';?>"
        });

        $('.FB_reactions').on('click', function () {

            var $this = $(this);
            var user_id = $this.attr('data-user-id');
            var control_id = $this.attr('data-unique-id');
            var value = $this.attr('data-emoji-id');
            var imo_track = 'imo_track';

//            alert(value);

            if (user_id.length === 0 || user_id == null) {
                alert("Please login to perform this task!");
            } else {
                $.ajax({
                    type: 'POST',
                    url: '<?php echo site_url() . '/opportunity/fbImo';?>',
                    data: {user_id: user_id, control_id: control_id, value: value, imo_track: imo_track}, // our data object
                    success: function (response) {

                        var res = $.parseJSON(response);
                        console.log(res);
//                    alert(res.un_chg);
                        if (res.un_chg == 1) {
                            $('.this_cls_' + res.opp_id).attr('data-emoji-id', 'like');
                        }
                        else if (res.un_chg == 2) {
                            $('.this_cls_' + res.opp_id).attr('data-emoji-id', '');
                        }


                        console.log(Object.keys(res).length);

                        $('#like-count' + res.opp_id).html(0);
                        $('#smiley-count' + res.opp_id).html(0);
                        $('#love-count' + res.opp_id).html(0);
                        $('#wow-count' + res.opp_id).html(0);

                        res.count.forEach(function (counts) {
                            if (counts.type == '1') {
                                $('#like-count' + res.opp_id).html(counts.interest_count);
                            }
                            else if (counts.type == '2') {
                                $('#smiley-count' + res.opp_id).html(counts.interest_count);
                            }
                            else if (counts.type == '3') {
                                $('#love-count' + res.opp_id).html(counts.interest_count);
                            }
                            else if (counts.type == '4') {
                                $('#wow-count' + res.opp_id).html(counts.interest_count);
                            }
                            else {
                            }
                        });
                        if (!res.ack) {
                            alert(res.msg);
                        }


                    },
                    error: function () {

                        // alert('<p>An error has occurred</p>');
                        console.log('An error has occurred');
                    }
                });

            }


        });
    });

</script>

<style>
    .c_btn {
        background-color:; /* Green */
        border: none;
        color: darkblue;
        padding: 04px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 13px;
        margin: 4px 2px;
        cursor: pointer;
        font-weight: 400;
    }
</style>


<!--  **************************** FB Imo End *************************************************** -->


<style media="screen">
    #easyPaginate, #easyPaginateOpportunity {
        width: 100%;
    }

    .easyPaginateNav {
        clear: both;
        margin-top: 20px;
        text-align: center;
    }

    .easyPaginateNav a {
        padding: 8px;
    }

    .easyPaginateNav a.current {
        font-weight: bold;
        text-decoration: underline;
        font-size: 24px;
    }

    #easyPaginate, #easyPaginateOpportunity {
        display: -webkit-flex; /* Safari */
        display: flex;
        flex-wrap: wrap;
    }
</style>

<style>
    .panelD .panel-heading, .panelD .panel-footer {
        background-color: #fff !important;
    }

    .panel-image img.panel-image-preview {
        width: 100%;
        border-radius: 4px 4px 0px 0px;
    }

    .panel-heading ~ .panel-image img.panel-image-preview {
        border-radius: 0px;
    }

    .panel-heading .list-inline {
        margin: 0px 0px 0px -5px !important;
    }

    .panel-body {
        display: block;
        padding: 2px 5px 0px;
    }

    .panel-body blockquote {
        margin: 5px 0 5px;
    }

    .panel-image ~ .panel-footer a {
        padding: 0px 10px;
        color: rgb(100, 100, 100);
    }

    .panel-image ~ .panel-footer span {
        color: rgb(100, 100, 100);
    }

    .panel-footer .list-inline {
        margin: 0 0 0 -15px !important;
    }

    .panel-image.hide-panel-body ~ .panel-body {
        height: 0px;
        padding: 0px;
    }

    /*==========  Mobile First Method  ==========*/
    /* Custom, iPhone Retina */
    @media only screen and (max-width: 320px) {
        .level-line-up {
            position: relative;
            top: -4px;
        }
    }

    .frame2 {
        height: 200px;
        display: table;
        width: 100%;
        text-align: center;
        padding: 0px;
        border-bottom: 1px solid #ddd;
    }

    .helper222 {
        display: table-cell;
        text-align: center;
        vertical-align: middle;
        height: 200px;
    }

    .frame2 > span > img {
        vertical-align: middle;
        display: inline-block !important;
        width: auto !important;
        max-height: 100%;
        max-width: 100%;
    }
</style>

<style>
    .new_rz_box {
        display: block;
        margin-top: 40px;
        padding: 10px 0;
        border-bottom: 1px solid #ccc;
    }

    .new_rz_box h6 {
        color: #446678;
        text-transform: uppercase;
    }

    .new_rz_box h5 {
        color: #446678;
        text-transform: uppercase;
    }

    .new_rz_box h3 {
        color: #446678;
        text-transform: uppercase;
    }

    .new_rz_box h4 {
        color: #333;
    }

    .new_rz_box2 {
        display: block;
        padding: 10px 0;
    }

    .new_rz_box2 h3 {
        color: #446678;
        margin-bottom: 10px;
    }

    .new_rz_box2 input {
        border: 1px solid #d9d9d9;
        width: 100%;
        padding: 5px;
    }

    .new_rz_box2 input {
        border: 1px solid #d9d9d9 !important;
    }

    .new_rz_box2 textarea {
        border: 1px solid #d9d9d9;
        width: 100%;
        padding: 10px 20px;
    }

    .new_rz_box2 select {
        border: 1px solid #d9d9d9;
        width: 100%;
        padding: 5px 10px;
    }

    .row > div {
        display: inline-block;

    }

    .rz_button3 {
        margin-top: 40px;
        border: 0;
        background-color: #012F49;
        color: #FFF;
        font-size: 25px;
        text-align: center;
        padding: 10px 20px;
        text-transform: uppercase;
        border-radius: 10px;
    }

    .rz_button3:hover {
        color: #FFF;
    }

    .rz_button2 {
        background-color: #e63433;
        color: #FFF;
        font-size: 15px;
        text-align: center;
        padding: 6px 12px;
        text-transform: uppercase;
        border-radius: 25px;
    }

    .rz_button2:hover {
        color: #FFF;
    }
</style>
<style type="text/css">
    .thumb {
        width: 50px;
        height: 50px;
    }

    #list {
        position: absolute;
        top: 0px;
    }

    #list div {
        float: left;
        margin-right: 10px;
    }
</style>


<?php
$userInfo = $this->session->userdata('user_logged_user');
if (isset($userInfo['userId'])) {
    $permUserId = $userInfo['userId'];
} else {
    $permUserId = 0;
}
$prm = $this->db->query("SELECT COUNT(*) PER FROM front_user_permission WHERE MODULE_ID=4 AND USER_ID=$permUserId")->row();

?>


<div class="others_page_wrp">
    <section class="org_map_wrp">
        <div class="container">
            <h1 style="text-align: center; margin: 24px; color: #E43533;">Opportunities for you!</h1>
            <div class="col-lg-12 text-center">
                <form action="<?php echo site_url() . 'opportunity/searchOpportunities' ?>" class="" method="post">
                    <!--                --><?php //$categoryList = array('Competitions', 'Conferences', 'Exchange-programs', 'Fellowships', 'Internships', 'Scholarships', 'Workshops', 'Miscellaneous', 'Training'); ?>
                    <div class="row new_rz_box2">
                        <div class="row col-md-offset-1 col-md-8">
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <select name="category">
                                    <option value="">Select Category</option>
                                    <option value="Competition" <?php echo ($cat_ty == 'Competition') ? 'selected="selected"' : '' ?>>
                                        Competition
                                    </option>
                                    <option value="Conference" <?php echo ($cat_ty == 'Conference') ? 'selected="selected"' : '' ?>>
                                        Conference
                                    </option>
                                    <option value="Exchange-program" <?php echo ($cat_ty == 'Exchange-program') ? 'selected="selected"' : '' ?>>
                                        Exchange-program
                                    </option>
                                    <option value="Fellowship" <?php echo ($cat_ty == 'Fellowship') ? 'selected="selected"' : '' ?>>
                                        Fellowship
                                    </option>
                                    <option value="Internship" <?php echo ($cat_ty == 'Internship') ? 'selected="selected"' : '' ?>>
                                        Internship
                                    </option>
                                    <option value="Scholarship" <?php echo ($cat_ty == 'Scholarship') ? 'selected="selected"' : '' ?>>
                                        Scholarship
                                    </option>
                                    <option value="Workshop" <?php echo ($cat_ty == 'Workshop') ? 'selected="selected"' : '' ?>>
                                        Workshop
                                    </option>
                                    <option value="Training" <?php echo ($cat_ty == 'Training') ? 'selected="selected"' : '' ?>>
                                        Training
                                    </option>
                                    <option value="Miscellaneous" <?php echo ($cat_ty == 'Miscellaneous') ? 'selected="selected"' : '' ?>>
                                        Miscellaneous
                                    </option>
                                </select>
                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <select name="order">
                                    <option value="">Select order</option>
                                    <option value="desc" <?php echo ($or == 'desc') ? 'selected="selected"' : '' ?>>
                                        Newest to oldest
                                    </option>
                                    <option value="asc" <?php echo ($or == 'asc') ? 'selected="selected"' : '' ?>>Oldest
                                        to newest
                                    </option>
                                    <option value="today" <?php echo ($or == 'today') ? 'selected="selected"' : '' ?>>
                                        Today's post
                                    </option>
                                    <option value="week" <?php echo ($or == 'week') ? 'selected="selected"' : '' ?>>This
                                        week post
                                    </option>
                                </select>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <input list="search" type="text" class="" placeholder="Search for..."
                                       name="search" value="<?php if (!empty($src)) {
                                    echo $src;
                                } ?>"
                                       style="min-width: 120px;     border-radius: 6px; margin: 0 3px 0 7px;   padding: 5px;">

                                <datalist id="search">
                                    <?php foreach ($opportunity_title_list

                                    as $op_list) : ?>
                                    <option value="<?php echo $op_list->title; ?>">
                                        <?php endforeach; ?>
                                </datalist>
                            </div>


                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                <input type="submit" class="btn btn-default btn-block" type="button" name="search_btn"
                                       value="Search">
                            </div>

                        </div>


                        <?php if ($prm->PER > 0) : ?>
                            <div class="col-lg-3 col-md-2 col-sm-12 col-xs-12">
                                <a class="rz_button2 pull-right"
                                   href="<?php echo site_url(); ?>Opportunity/createOpportunity">Create New
                                    Opportunity</a>
                            </div>
                        <?php endif; ?>

                    </div>
                </form>
            </div>

            <p style="text-align: center;margin: 10px;">
            <hr>
            <hr>
            </p>

            <?php $i = ''; ?>

            <?php
            if (isset($this->session->userdata['user_logged_user']['userId'])) {
                $id = $this->session->userdata['user_logged_user']['userId'];
                $linkShare = $id;
            } else {
                $id = '';
                $linkShare = 0;
            }
            ?>


            <div class="tabbable-panel">
                <div class="tabbable-line">

                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_events">
                            <div id="easyPaginate">
                                <?php
                                $i = 0;

                                ?>

                                <?php foreach ($opportunities as $row) : ?>

                                    <?php
                                    $interests = $this->opportunity_model->getOppInterest2($row->id, $id);
                                    $inter = $this->opportunity_model->getOppInterest($row->id);


                                    $likeReaction = 0;
                                    $smileReaction = 0;
                                    $loveReaction = 0;
                                    $wowReaction = 0;
                                    $action = '';
                                    $action_name = '';
                                    ?>

                                    <?php foreach ($interests as $interest) { ?>
                                        <?php
                                        if ($interest->type == '1') {
                                            $likeReaction = $interest->interest_count;
                                            $action = 'like';
                                            $action_name = 'LIKE';
                                        } elseif ($interest->type == '2') {
                                            $smileReaction = $interest->interest_count;
                                            $action = 'haha';
                                            $action_name = 'HAHA';
                                        } elseif ($interest->type == '3') {
                                            $loveReaction = $interest->interest_count;
                                            $action = 'love';
                                            $action_name = 'LOVE';
                                        } elseif ($interest->type == '4') {
                                            $wowReaction = $interest->interest_count;
                                            $action = 'wow';
                                            $action_name = 'WOW';
                                        }
                                        ?>

                                    <?php } ?>

                                    <?php foreach ($inter as $intere) : ?>
                                        <?php
                                        if ($intere->type == '1') {
                                            $likeReaction = $intere->interest_count;
                                        } elseif ($intere->type == '2') {
                                            $smileReaction = $intere->interest_count;
                                        } elseif ($intere->type == '3') {
                                            $loveReaction = $intere->interest_count;
                                        } elseif ($intere->type == '4') {
                                            $wowReaction = $intere->interest_count;
                                        }
                                        ?>
                                    <?php endforeach; ?>


                                    <?php
                                    $i++;
                                    $description = html_entity_decode($row->description, ENT_COMPAT, 'UTF-8');
                                    $images = explode('___', $row->images);
                                    ?>
                                    <div class="col-xs-12 col-sm-6 col-md-4 show_block_elem">
                                        <div class="thumbnail">
                                            <div class="frame2">
                                            <span class="helper222">
                                            <img
                                                    src="<?php echo base_url() . "upload/opportunities/" . $images[0]; ?>"
                                                    alt="<?php echo $row->title; ?>"></span>
                                            </div>


                                            <div class="in">
                                                <div align="center">


                                                    <div id="" style="padding-left: 5px; padding-top: 5px;">
                                                        <input type="hidden" class="c_id" name="c_id"
                                                               value="<?php // echo $row->id; ?>">
                                                        <div style="" align="left">

                                     <span id="Fb_Imo"><a class="FB_reactions this_cls_<?php echo $row->id; ?>"
                                                          data-reactions-type='horizontal'
                                                          data-emoji-id="<?php echo (!empty($action)) ? $action : ''; ?>"
                                                          data-user-id="<?php echo $id; ?>"
                                                          data-unique-id="<?php echo $row->id; ?>"
                                                          data-emoji-class="<?php echo (!empty($action)) ? $action : ''; ?>">
                                        <span style=""><?php echo (!empty($action_name)) ? $action_name : 'LIKE'; ?></span>
                                    </a></span>

                                                            <span class="my_buttons"
                                                                  style="padding: 7px; font-size: 14px;">

                                        <span><button class="c_btn" style="border-radius: 12px" href=""> <img
                                                        style="height: 20px; width: auto;"
                                                        src="<?php echo base_url() . 'fb_imo/emojis/like.svg'; ?>"
                                                        class="emoji"> <span
                                                        id="like-count<?php echo $row->id; ?>"><?php echo $likeReaction; ?></span></button></span>
                                        <span><button class="c_btn" style="border-radius: 12px" href=""> <img
                                                        style="height: 20px; width: auto;"
                                                        src="<?php echo base_url() . 'fb_imo/emojis/love.svg'; ?>"
                                                        class="emoji"> <span
                                                        id="love-count<?php echo $row->id; ?>"> <?php echo $loveReaction; ?> </span> </button></span>
                                        <span><button class="c_btn" style="border-radius: 12px" href=""> <img
                                                        style="height: 20px; width: auto;"
                                                        src="<?php echo base_url() . 'fb_imo/emojis/haha.svg'; ?>"
                                                        class="emoji"> <span
                                                        id="smiley-count<?php echo $row->id; ?>"> <?php echo $smileReaction; ?> </span> </button></span>
                                        <span><button class="c_btn" style="border-radius: 12px" href=""> <img
                                                        style="height: 20px; width: auto;"
                                                        src="<?php echo base_url() . 'fb_imo/emojis/wow.svg'; ?>"
                                                        class="emoji"> <span
                                                        id="wow-count<?php echo $row->id; ?>"> <?php echo $wowReaction; ?> </span> </button></span>
                                    </span>

                                                        </div>

                                                    </div>
                                                </div>

                                            </div>
                                            <br>

                                            <?php $url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                                            $actual_link_report = str_replace("/", "--", $url);
                                            ?>


                                            <div class="caption">
                                                <h3 style="color: #E43533; text-align: center; text-transform: uppercase; min-height: 57px;"><?php echo $row->title; ?></h3>
                                                <h4 style="color: #78AE48">Posted
                                                    on: <?php echo date("M d Y", strtotime($row->start_date)); ?></h4>
                                                <h4 style="color: #78AE48">End Date
                                                    on: <?php echo date("M d Y", strtotime($row->end_date)); ?></h4>
                                                <div style="text-overflow: ellipsis;word-wrap: break-word;overflow: hidden;height:4.4em; max-height: 4.4em;line-height: 1.4em;-webkit-line-clamp: 3;display: -webkit-box;-webkit-box-orient: vertical;"><?php echo htmlspecialchars_decode($description);; ?></div>
                                                <hr style="margin: 0.5em;">
                                                <p>
                                                    <button type="button" class="btn btn-primary modalLink"
                                                            style="margin-bottom: 4px; margin-right: 14px;"
                                                            href="<?php echo site_url() . 'opportunity/viewModal/' . $row->id . '/' . $actual_link_report; ?>"
                                                            title=""
                                                    >Read more
                                                    </button>
                                                    <span style="float: right; margin: 6px 2px;">
                                                        <i class="fa fa-tags fa-flip-horizontal" aria-hidden="true"></i>
                                                        <span style="background: lightgray; padding: 6px 12px; font-size: 14px; border-radius: 25px;"><?php echo $row->category; ?></span></span>
                                                </p>

                                                <?php $actual_base_link = base_url(); ?>
                                                <?php $actual_controller = $this->uri->segment('1'); ?>
                                                <?php $actual_link = $actual_base_link . $actual_controller; ?>

                                                <hr style="margin: 0.5em;">
                                                <p class="pull-right">
                                                    <i style="color: #012F49" class="fa fa-share-alt"
                                                       aria-hidden="true"></i>
                                                    <a class="facebook customer share"
                                                       style="color: #4267b2;"
                                                       href="http://www.facebook.com/sharer.php?u=<?php echo $actual_link . '/opportunity_view.php?opportunity_id=' . $row->id; ?>"
                                                       title="Facebook share" target="_blank"><i
                                                                class="fa fa fa-facebook fa-lg" style="margin: 0 8px;"
                                                                aria-hidden="true"></i></a>
                                                    <a class="twitter customer share"
                                                       style="color: #1b95e0;"
                                                       href="http://twitter.com/share?url=<?php echo $actual_link . '/opportunity_view.php?opportunity_id=' . $row->id; ?>&amp;hashtags=youthopiabangla"
                                                       title="Twitter share" target="_blank"><i
                                                                class="fa fa fa-twitter fa-lg" style="margin: 0 8px;"
                                                                aria-hidden="true"></i></a>
                                                    <a class="google_plus customer share"
                                                       style="color: #db4437;"
                                                       href="https://plus.google.com/share?url=<?php echo $actual_link . '/opportunity_view.php?opportunity_id=' . $row->id; ?>"
                                                       title="Google Plus Share" target="_blank"><i
                                                                class="fa fa fa-google-plus fa-lg"
                                                                style="margin: 0 8px;" aria-hidden="true"></i></a>
                                                    <a class="email modalLink" data-modal-size="modal-md"
                                                       style="color: #79B04A;"
                                                       href="<?php echo site_url() . '/opportunity/report_problem/' . $row->id . '/' . $linkShare . '/' . $actual_link_report ?>"
                                                       title="Share"><i
                                                                class="fa fa-envelope fa-lg"
                                                                style="margin: 0 8px;" aria-hidden="true"></i></a>
                                                </p>
                                                <br/>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-center">
                                <?php echo $this->pagination->create_links(); ?>
                            </div>
                        </div>


                    </div>
                </div>
            </div>


        </div>
    </section>
</div>

<script type="text/javascript" src="<?php echo base_url(); ?>old/js/jquery.easyPaginate.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#event-calender-slider").owlCarousel({
            autoPlay: 3000,
            pagination: false,
            navigation: true,
            itemsCustom: [[0, 1], [479, 2], [768, 5], [991, 6], [1199, 6]],
        });
        $(function () {
            window.prettyPrint && prettyPrint();
            $('#event-calender').datepicker({
                //                        format: 'mm-dd-yyyy'
                format: 'yyyy-mm-dd'
            }).on('changeDate', function (e) {
                $.ajax({
                    url: 'ajax.php?action=cal_search',
                    type: 'POST',
                    data: {date: e.format()}
                }).done(function (response) {
                    //                            console.log(response);
                    $('#search_result_container').html(response);
                });
                //                        console.log(e.format());
            });
        });
    });
</script>
<!-- JS ENDS -->
<script>
    $(function () {
        $('#search_btn').on('click', function () {
            var category = $('#category').val();
            var district = $('#district').val();
            var organization = $('#organization').val();
            var search_month = $('#search_month').val();
            var search_year = $('#search_year').val();
            var search_text = $('#search_text').val();
            $.ajax({
                url: 'ajax.php?action=search',
                data: {
                    category: category,
                    district: district,
                    organization: organization,
                    search_month: search_month,
                    search_year: search_year,
                    search_text: search_text
                },
                type: 'POST'
            }).done(function (response) {
                $('#search_result_container').html(response);
                //                    console.log(response);
            });
        })
    });
    //    $(function () {
    //        $('#easyPaginate').easyPaginate({
    //            paginateElement: '.show_block_elem',
    //            elementsPerPage: 9,
    //            effect: 'default'
    //        });
    //        $('#easyPaginateOpportunity').easyPaginate({
    //            paginateElement: '.show_block_elem',
    //            elementsPerPage: 9,
    //            effect: 'default'
    //        });
    //    });
</script>
<script>
    ;(function ($) {
        /**
         * jQuery function to prevent default anchor event and take the href * and the title to make a share popup
         *
         * @param  {[object]} e           [Mouse event]
         * @param  {[integer]} intWidth   [Popup width defalut 500]
         * @param  {[integer]} intHeight  [Popup height defalut 400]
         * @param  {[boolean]} blnResize  [Is popup resizeabel default true]
         */
        $.fn.customerPopup = function (e, intWidth, intHeight, blnResize) {
            // Prevent default anchor event
            e.preventDefault();
            // Set values for window
            intWidth = intWidth || '500';
            intHeight = intHeight || '400';
            strResize = (blnResize ? 'yes' : 'no');
            // Set title and open popup with focus on it
            var strTitle = ((typeof this.attr('title') !== 'undefined') ? this.attr('title') : 'Social Share'),
                strParam = 'width=' + intWidth + ',height=' + intHeight + ',resizable=' + strResize,
                objWindow = window.open(this.attr('href'), strTitle, strParam).focus();
        }
        /* ================================================== */
        $(document).ready(function ($) {
            $('.customer.share').on("click", function (e) {
                $(this).customerPopup(e);
            });
        });
    }(jQuery));
</script>
<script>
    $(function () {
        $('#bs-example-modal-lg-row').on('show.bs.modal', function (e) {
            $title = $(e.relatedTarget).attr('data-title');
            $.ajax({
                type: "GET",
                url: "opportunity_view2.php",
                data: {opportunity_id: $title},
                beforeSend: function () {
                    $("#loading-image").show();
                },
                success: function (result) {
                    $("#modal-data-from-view2").html(result);
                    $("#loading-image").hide();
                }
            });
        });
    });

</script>
<script src="https://apis.google.com/js/platform.js" async defer></script>