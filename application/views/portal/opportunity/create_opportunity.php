<style>
    .new_rz_box {
        display: block;
        margin-top: 40px;
        padding: 10px 0;
        border-bottom: 1px solid #ccc;
    }

    .new_rz_box h6 {
        color: #446678;
        text-transform: uppercase;
    }

    .new_rz_box h5 {
        color: #446678;
        text-transform: uppercase;
    }

    .new_rz_box h3 {
        color: #446678;
        text-transform: uppercase;
    }

    .new_rz_box h4 {
        color: #333;
    }

    .new_rz_box2 {
        display: block;
        padding: 10px 0;
    }

    .new_rz_box2 h3 {
        color: #446678;
        margin-bottom: 10px;
    }

    .new_rz_box2 input {
        border: 1px solid #d9d9d9;
        width: 100%;
        padding: 5px;
    }

    .new_rz_box2 textarea {
        border: 1px solid #d9d9d9;
        width: 100%;
        padding: 10px 20px;
    }

    .new_rz_box2 select {
        border: 1px solid #d9d9d9;
        width: 100%;
        padding: 5px 10px;
    }

    .rz_button3 {
        margin-top: 40px;
        border: 0;
        background-color: #012F49;
        color: #FFF;
        font-size: 25px;
        text-align: center;
        padding: 10px 20px;
        text-transform: uppercase;
        border-radius: 10px;
    }

    .rz_button3:hover {
        color: #FFF;
    }
</style>
<style type="text/css">
    .thumb {
        width: 50px;
        height: 50px;
    }

    #list {
        position: absolute;
        top: 0px;
    }

    #list div {
        float: left;
        margin-right: 10px;
    }
.new_rz_box2 input {
        border: 1px solid #d9d9d9 !important;
    }
</style>

<div class="others_page_wrp">
    <section class="org_map_wrp">
        <div class="container">

            <form class="box" method="post" action="<?php echo site_url().'opportunity/createNewOpportunityPost'?>" enctype="multipart/form-data">

                <input type="hidden" name="user_id" value="<?php echo $this->session->userdata['user_logged_user']['userId']; ?>"/>
                <div class="row new_rz_box">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <h5>Name of Organization *</h5>
                        <h4><?php echo (!empty($organizationData->title)) ? $organizationData->title : ''; ?></h4>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <h6>Full Address *</h6>
                        <h4><?php echo (!empty($organizationData->location)) ? $organizationData->location : ''; ?></h4>
                    </div>
                </div>

                 <div class="row new_rz_box2">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h3>Job Title *</h3>
                        <input type="text" name="title" value="" placeholder="Event title" maxlength="255" required/>
                    </div>
                </div>
                <div class="row new_rz_box2">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h3>Category *</h3>
                        <select name="category" required>
                            <option value="">Select category</option>
                            <option value="Competition">Competition</option>
                            <option value="Conference">Conference</option>
                            <option value="Exchange-program">Exchange-program</option>
                            <option value="Fellowship">Fellowship</option>
                            <option value="Internship">Internship</option>
                            <option value="Scholarship">Scholarship</option>
                            <option value="Workshop">Workshop</option>
                            <option value="Training">Training</option>
                            <option value="Miscellaneous">Miscellaneous</option>

                        </select>
                    </div>
                </div>
                <div class="row new_rz_box2">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h3>Gallery</h3>
                        <input type="file" name="files">
                    </div>
                </div>
                <div class="row new_rz_box2">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h3>Description *</h3>
                        <textarea class="redactor" name="description" id="description" rows="3" required></textarea>
                    </div>
                </div>
                <div class="row new_rz_box2">
                    <div class="col-sm-12 col-xs-12">
                        <h3>Start Date *</h3>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <p><i style="position:absolute; right:15%; top:25%;"
                                      class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                      <input type="text" name="start_date" id="datepicker" placeholder="Start date" required/>
                                </p>
                            </div>
<!--                            <h3>End Date </h3>-->
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <p><i style="position:absolute; right:15%; top:25%;"
                                      class="glyphicon glyphicon-calendar fa fa-calendar"></i><input type="text"
                                                                                                     name="end_date"
                                                                                                     id="datepicker2"
                                                                                                     placeholder="End date"/>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row new_rz_box2">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="district">District *</label>
                            <select name="district" id="district">
                                <option value="">Select District</option>
                                <?php foreach ($city as $cities): ?>
                                    <option value="<?php echo $cities->name; ?>"><?php echo$cities->name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row new_rz_box2">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h3>Job Registration Link</h3>
                        <input type="text" name="registration_link" value=""
                               placeholder="http://www.facebook.com/opportunity/"/>
                    </div>
                </div>
                <p style="text-align:center; margin-bottom:60px;">
                    <button class="rz_button3" type="submit" name="submitOpportunity">Publish Opportunity</button>
                </p>


            </form>

        </div>
    </section>
</div>


<script src="<?php echo base_url();?>old/assets/vendors/ckeditor2/ckeditor.js"></script>
<script src="<?php echo base_url();?>old/assets/vendors/ckeditor2/samples/js/sample.js"></script>

<script>
    $(function () {
        $("#datepicker").datepicker({format: 'dd-mm-yyyy'});
        $("#datepicker2").datepicker({format: 'dd-mm-yyyy'});
    });
</script>

<script>
//    $(function () {
        $('.blog-type input[type=radio]').on('change', function () {

//            alert($(this).val());

            $('.toHide').hide();
            $("#blk-" + $(this).val()).fadeIn(1000);
        });


        $('.video-url-type input[type=radio]').on('change', function () {
            $("#video-url-name").text($(this).attr("text"));
        });
//    });
</script>

<script>
//    initSample();
</script>
<script>
    function handleFileSelect(evt) {
        var files = evt.target.files; // FileList object

        // Loop through the FileList and render image files as thumbnails.
        for (var i = 0, f; f = files[i]; i++) {

            // Only process image files.
            if (!f.type.match('image.*')) {
                continue;
            }

            var reader = new FileReader();

            // Closure to capture the file information.
            reader.onload = (function (theFile) {
                return function (e) {
                    // Render thumbnail.
                    var span = document.createElement('div');
                    span.setAttribute('class', 'img_wrpv');
                    span.innerHTML = ['<a href="#" style="color:red; font-size:18px; position: relative; z-index: 200;" class="img_revg">x</a><img width="50px" height="50px" class="thumb" src="', e.target.result,
                        '" title="', escape(theFile.name), '"/>'].join('');
                    document.getElementById('list').insertBefore(span, null);
                };
            })(f);

            // Read in the image file as a data URL.
            reader.readAsDataURL(f);

            //remove image
            $('.img_revg').on('click', function (e) {
                e.preventDefault();
                var parent = $(this).parent();
                parent.remove();
            });

        }
    }

    document.getElementById('galleryfiles').addEventListener('change', handleFileSelect, true);
</script>