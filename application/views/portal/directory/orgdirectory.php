<!DOCTYPE html>

<html lang="en-US">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="ThemeStarz">
  <link rel="shortcut icon" href="<?php echo base_url(); ?>favicon.ico"/>
    <link href="<?php echo base_url(); ?>map_assets/fonts/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>map_assets/fonts/elegant-fonts.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700,900,400italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo base_url('portalAssets/homePage') ?>/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>map_assets/css/zabuto_calendar.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>map_assets/css/owl.carousel.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/icon_style.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style1.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>map_assets/css/trackpad-scroll-emulator.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>map_assets/css/jquery.nouislider.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>map_assets/css/style.css" type="text/css">
<script type="text/javascript" src="<?php echo base_url(); ?>map_assets/js/jquery-2.2.1.min.js"></script>


    <title>Organization Directory</title>
<style type="text/css">
  li.list{
    list-style: none;
    display: block;

  }
  li.list a:hover{
  color:green;
  }


  .select-gen{
        border-radius: 0px !important;
        color: #333 !important;
        padding: 0px !important;
        margin-top: 14px !important;
    }
    .select-gen >option{
        color: #333 !important;
        padding:0px !important;

    }

    select.selectpicker {
    display: block !important;

  }
</style>
</head>

<body class="homepage">
<?php $this->load->view('portal/template/modal'); ?>
<div class="page-wrapper">
    <?php $this->load->view('portal/template/menu'); ?>
    <!--end page-header-->

    <div id="page-content">
        <div class="hero-section full-screen has-map has-sidebar">
            <div class="map-wrapper">
                <div class="geo-location">
                    <i class="fa fa-map-marker"></i>
                </div>
                <div class="map" id="map-homepage"></div>
            </div>
            <!--end map-wrapper-->
            <div class="results-wrapper">
                <div class="form search-form inputs-underline">
                    <form>
                        <div class="section-title">
                            <h2>Search</h2>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="title" placeholder="Enter keyword">
                        </div>
                        <!--end form-group-->
                        <div class="row">
                          <div class="col-md-12 col-sm-12">
                              <div class="form-group">
                                  <select class="form-control selectpicker" name="district">
                                      <option value="">Location</option>
                                      <option value="Bagerhat">Bagerhat</option>
                                      <option value="Bandarban">Bandarban</option>
                                      <option value="Barguna">Barguna</option>
                                      <option value="Barisal">Barisal</option>
                                      <option value="Bhola">Bhola</option>
                                      <option value="Bogra">Bogra</option>
                                      <option value="Brahmmanbaria">Brahmmanbaria</option>
                                      <option value="Chandpur">Chandpur</option>
                                      <option value="Chapai Nawabganj">Chapai Nawabganj</option>
                                      <option value="Chittagong">Chittagong</option>
                                      <option value="Chuadanga">Chuadanga</option>
                                      <option value="Comilla">Comilla</option>
                                      <option value="CoxsBazar">Cox’s Bazar</option>
                                      <option value="Dhaka">Dhaka</option>
                                      <option value="Dinajpur">Dinajpur</option>
                                      <option value="Faridpur">Faridpur</option>
                                      <option value="Feni">Feni</option>
                                      <option value="Gaibandha">Gaibandha</option>
                                      <option value="Gazipur">Gazipur</option>
                                      <option value="Gopalganj">Gopalganj</option>
                                      <option value="Habiganj">Habiganj</option>
                                      <option value="Jamalpur">Jamalpur</option>
                                      <option value="Jessore">Jessore</option>
                                      <option value="Jhalakhati">Jhalakhati</option>
                                      <option value="Jhenaidah">Jhenaidah</option>
                                      <option value="Joypurhat">Joypurhat</option>
                                      <option value="Khagrachhari">Khagrachhari</option>
                                      <option value="Khulna">Khulna</option>
                                      <option value="Kishoreganj">Kishoreganj</option>
                                      <option value="Kurigram">Kurigram</option>
                                      <option value="Kushtia">Kushtia</option>
                                      <option value="Lakshmipur">Lakshmipur</option>
                                      <option value="Lalmonirhat">Lalmonirhat</option>
                                      <option value="Madaripur">Madaripur</option>
                                      <option value="Magura">Magura</option>
                                      <option value="Manikganj">Manikganj</option>
                                      <option value="Maulvibazar">Maulvibazar</option>
                                      <option value="Meherpur">Meherpur</option>
                                      <option value="Munshiganj">Munshiganj</option>
                                      <option value="Mymensingh">Mymensingh</option>
                                      <option value="Naogaon">Naogaon</option>
                                      <option value="Narail">Narail</option>
                                      <option value="Narayanganj">Narayanganj</option>
                                      <option value="Narsingdi">Narsingdi</option>
                                      <option value="Natore">Natore</option>
                                      <option value="Netrakona">Netrakona</option>
                                      <option value="Nilphamari">Nilphamari</option>
                                      <option value="Noakhali">Noakhali</option>
                                      <option value="Pabna">Pabna</option>
                                      <option value="Panchagarh">Panchagarh</option>
                                      <option value="Patuakhali">Patuakhali</option>
                                      <option value="Pirojpur">Pirojpur</option>
                                      <option value="Rajbari">Rajbari</option>
                                      <option value="Rajshahi">Rajshahi</option>
                                      <option value="Rangamati">Rangamati</option>
                                      <option value="Rangpur">Rangpur</option>
                                      <option value="Satkhira">Satkhira</option>
                                      <option value="Shariatpur">Shariatpur</option>
                                      <option value="Sherpur">Sherpur</option>
                                      <option value="Sirajganj">Sirajganj</option>
                                      <option value="Sunamganj">Sunamganj</option>
                                      <option value="Sylhet">Sylhet</option>
                                      <option value="Tangail">Tangail</option>
                                      <option value="Thakurgaon">Thakurgaon</option>
                                  </select>
                                </div>
                                <!--end form-group-->
                            </div>
                            <!--end col-md-6-->
                            <div class="col-md-12 col-sm-12">
                                <div class="form-group">
                                  <?php
                                  $category = $this->db->query("SELECT a.*,(SELECT COUNT(iao) FROM users u
left join listing l on u.userid=l.userid
 WHERE l.iao  LIKE CONCAT('%', a.iao_name, '%') and l.latitude!='') iaoNumber  FROM iao a")->result();
                                  //print_r($category);
                                   ?>
                                  <select class="form-control selectpicker" name="iao">
                                      <option value="">Category</option>
                                      <?php
                                      foreach ($category as $key => $value) {
                                          ?>
                                          <option value="<?php  echo  base64_encode($value->iao_name); ?>"><?php  echo $value->iao_name; ?></option>
                                      <?php } ?>
                                  </select>
                                </div>
                                <!--end form-group-->
                            </div>
                            <div class="col-md-12 col-sm-12">
                                <div class="form-group">
                                    <select class="form-control selectpicker select-gen" name="category">
                                        <option value="">Scope of Your Organisation</option>
                                        <option value="local">Local</option>
                                        <option value="national">National</option>
                                        <option value="international">International</option>
                                    </select>
                                </div>
                                <!--end form-group-->
                            </div>
                            <!--end col-md-6-->
                        </div>
                        <!--end row-->
                        <!--end row-->
                        <div class="form-group">
                            <button type="submit" data-ajax-response="map" data-ajax-data-file="<?php echo base_url(); ?>assets/external/data_2.php" data-ajax-auto-zoom="1" class="btn btn-primary pull-right"><i class="fa fa-search"></i></button>
                        </div>
                        <!--end form-group-->
                    </form>
                    <!--end form-hero-->
                </div>
                <div class="results">
                    <div class="tse-scrollable">
                        <div class="tse-content">
                            <div class="section-title">
                                <h2>Search Results<span class="results-number"></span></h2>
                            </div>
                            <!--end section-title-->
                            <div class="results-content"></div>
                            <!--end results-content-->
                        </div>
                        <!--end tse-content-->
                    </div>
                    <!--end tse-scrollable-->
                </div>
                <!--end results-->
            </div>
            <!--end results-wrapper-->
        </div>
        <!--end hero-section-->


        <!--end block-->
        <div class="container"><hr></div>
        <div class="block">
            <div class="container">
                <div class="section-title">
                    <div class="center">
                        <h2>Browse Our Listings</h2>
                    </div>
                </div>
                <!--end section-title-->
                <h3 style="color: green;"><b>Interest area of the organisation</b></h3>
                <div class="row">
                    <div class="col-md-12">
                      <?php
                      // echo '<pre>';
                      // print_r($category);
                      foreach ($category as $key => $values) {
                      ?>
                      <div class="col-md-6">
                        <li class="list"><a href="javascript:void(0)" data-ajax-response="map"
                             data-ajax-data-file="assets/external/data_2.php?iao=<?php echo base64_encode($values->iao_name); ?>"
                             data-ajax-auto-zoom="1"><span class="<?php echo $values->iao_icon; ?>"></span>&nbsp;
                              <?php echo $values->iao_name." ($values->iaoNumber) "; ?> </a>
                      </li>
                      </div>


                      <?php } ?>
                    </div>
                </div>
                <!--end categories-list-->
            </div>
            <!--end container-->
        </div>
        <!--end block-->

        <!--end block-->

        <!--end block-->




        <!--end block-->

        <!--end block-->

        <!--end container-->


        <!--end block-->
    </div>
    <!--end page-content-->


    <footer id="page-footer">
        <?php $this->load->view('portal/template/footer') ?>
    </footer>
    <!--end page-footer-->
</div>
<!--end page-wrapper-->
<a href="#" class="to-top scroll" data-show-after-scroll="600"><i class="arrow_up"></i></a>

<style media="screen">
  .customizer {
    display: none;
  }
</style>
<script type="text/javascript" src="<?php echo base_url(); ?>map_assets/js/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>map_assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>map_assets/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyBEDfNcQRmKQEyulDN8nGWjLYPm8s4YB58&libraries=places"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>map_assets/js/richmarker-compiled.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>map_assets/js/markerclusterer_packed.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>map_assets/js/infobox.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>map_assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>map_assets/js/jquery.fitvids.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>map_assets/js/icheck.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>map_assets/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>map_assets/js/jquery.trackpad-scroll-emulator.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>map_assets/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>map_assets/js/jquery.nouislider.all.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>map_assets/js/custom.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>map_assets/js/maps.js"></script>

<script>
    var optimizedDatabaseLoading = 0;
    var _latitude = 23.8101;
    var _longitude = 90.4312;
    var element = "map-homepage";
    var markerTarget = "modal"; // use "sidebar", "infobox" or "modal" - defines the action after click on marker
    var sidebarResultTarget = "modal"; // use "sidebar", "modal" or "new_page" - defines the action after click on marker
    var showMarkerLabels = false; // next to every marker will be a bubble with title
    var mapDefaultZoom = 7; // default zoom
    heroMap(_latitude,_longitude, element, markerTarget, sidebarResultTarget, showMarkerLabels, mapDefaultZoom);
</script>

</body>
