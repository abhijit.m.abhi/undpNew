<?php

function getUserPermission($permUserId)
{
  $CI =& get_instance();
  $CI->load->model('portal_model');
  $permission=$CI->portal_model->getMenuPermission($permUserId);
  return $permission;
}
function getPermission($moduleId,$permission)
{
  $count=0;
  foreach($permission as $key=>$val)
  {
    $realValue= $permission[$key]['MODULE_ID'];
    if($realValue==$moduleId)
    {
      $count++;
    }
  }
  return $count;
}
?>
