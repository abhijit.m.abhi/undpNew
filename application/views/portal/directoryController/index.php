


  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/leaflet/leaflet.css" />
              <script src="<?php echo base_url(); ?>assets/leaflet/leaflet.js"></script>
              <style type="text/css">
                 #map{ height: 100% }
              </style>



<div id="page-content">
  <div class="hero-section full-screen has-map has-sidebar">
    <div class="map-wrapper">
      <div id="map" style="width:100%;height:600px; position:absolute;z-index:10;"></div>
    </div>
    <!--end map-wrapper-->
    <div class="results-wrapper">
      <div class="form search-form inputs-underline">
        <form>
          <div class="form-group">
            <input type="text" class="form-control" name="title" placeholder="Enter keyword">
          </div>
          <!--end form-group-->
          <div class="row">
            <div class="col-md-12 col-sm-12">
              <div class="form-group">
                <select class="form-control selectpicker" name="district">
                  <option value="">Location</option>
                  <option value="Bagerhat">Bagerhat</option>
                  <option value="Bandarban">Bandarban</option>
                  <option value="Barguna">Barguna</option>
                  <option value="Barisal">Barisal</option>
                  <option value="Bhola">Bhola</option>
                  <option value="Bogra">Bogra</option>
                  <option value="Brahmmanbaria">Brahmmanbaria</option>
                  <option value="Chandpur">Chandpur</option>
                  <option value="Chapai Nawabganj">Chapai Nawabganj</option>
                  <option value="Chittagong">Chittagong</option>
                  <option value="Chuadanga">Chuadanga</option>
                  <option value="Comilla">Comilla</option>
                  <option value="CoxsBazar">Cox’s Bazar</option>
                  <option value="Dhaka">Dhaka</option>
                  <option value="Dinajpur">Dinajpur</option>
                  <option value="Faridpur">Faridpur</option>
                  <option value="Feni">Feni</option>
                  <option value="Gaibandha">Gaibandha</option>
                  <option value="Gazipur">Gazipur</option>
                  <option value="Gopalganj">Gopalganj</option>
                  <option value="Habiganj">Habiganj</option>
                  <option value="Jamalpur">Jamalpur</option>
                  <option value="Jessore">Jessore</option>
                  <option value="Jhalakhati">Jhalakhati</option>
                  <option value="Jhenaidah">Jhenaidah</option>
                  <option value="Joypurhat">Joypurhat</option>
                  <option value="Khagrachhari">Khagrachhari</option>
                  <option value="Khulna">Khulna</option>
                  <option value="Kishoreganj">Kishoreganj</option>
                  <option value="Kurigram">Kurigram</option>
                  <option value="Kushtia">Kushtia</option>
                  <option value="Lakshmipur">Lakshmipur</option>
                  <option value="Lalmonirhat">Lalmonirhat</option>
                  <option value="Madaripur">Madaripur</option>
                  <option value="Magura">Magura</option>
                  <option value="Manikganj">Manikganj</option>
                  <option value="Maulvibazar">Maulvibazar</option>
                  <option value="Meherpur">Meherpur</option>
                  <option value="Munshiganj">Munshiganj</option>
                  <option value="Mymensingh">Mymensingh</option>
                  <option value="Naogaon">Naogaon</option>
                  <option value="Narail">Narail</option>
                  <option value="Narayanganj">Narayanganj</option>
                  <option value="Narsingdi">Narsingdi</option>
                  <option value="Natore">Natore</option>
                  <option value="Netrakona">Netrakona</option>
                  <option value="Nilphamari">Nilphamari</option>
                  <option value="Noakhali">Noakhali</option>
                  <option value="Pabna">Pabna</option>
                  <option value="Panchagarh">Panchagarh</option>
                  <option value="Patuakhali">Patuakhali</option>
                  <option value="Pirojpur">Pirojpur</option>
                  <option value="Rajbari">Rajbari</option>
                  <option value="Rajshahi">Rajshahi</option>
                  <option value="Rangamati">Rangamati</option>
                  <option value="Rangpur">Rangpur</option>
                  <option value="Satkhira">Satkhira</option>
                  <option value="Shariatpur">Shariatpur</option>
                  <option value="Sherpur">Sherpur</option>
                  <option value="Sirajganj">Sirajganj</option>
                  <option value="Sunamganj">Sunamganj</option>
                  <option value="Sylhet">Sylhet</option>
                  <option value="Tangail">Tangail</option>
                  <option value="Thakurgaon">Thakurgaon</option>
                </select>
              </div>
              <!--end form-group-->
            </div>
            <!--end col-md-6-->
            <div class="col-md-12 col-sm-12">
              <div class="form-group">

              </div>
              <!--end form-group-->
            </div>
            <!--end col-md-6-->
          </div>
          <!--end row-->
          <div class="row">
            <div class="col-md-12 col-sm-12">
              <div class="form-group">
                <select class="form-control selectpicker" name="category">
                  <option value="">Scope of Your Organisation</option>
                  <option value="local">Local</option>
                  <option value="national">National</option>
                  <option value="international">International</option>
                </select>
              </div>
              <!--end form-group-->
            </div>
            <!--end col-md-6-->
          </div>
          <!--end row-->
          <div class="form-group">
            <button type="submit" data-ajax-response="map"
            data-ajax-data-file="assets/external/data_2.php" data-ajax-auto-zoom="1"
            class="btn btn-primary pull-right"><i class="fa fa-search"></i></button>
          </div>
          <!--end form-group-->
        </form>
        <!--end form-hero-->
      </div>
      <div class="results">
        <div class="tse-scrollable">
          <div class="tse-content">
            <div class="section-title">
              <h2>Search Results<span class="results-number"></span></h2>
            </div>
            <!--end section-title-->
            <div class="results-content"></div>
            <!--end results-content-->
          </div>
          <!--end tse-content-->
        </div>
        <!--end tse-scrollable-->
      </div>
      <!--end results-->
    </div>
    <!--end results-wrapper-->
    <!--end search-form-->
  </div>


  <div class="container">

  </div>

</div>
<leaflet-marker id="icon-ref" latitude="51.5" longitude="-0.8" title="L.icon()">
</leaflet-marker>


<leaflet-map longitude="77.6" latitude="12.95" zoom="12">

  <leaflet-polygon color="#f00">
    <leaflet-point longitude="77.5700" latitude="12.9700"> </leaflet-point>
    <leaflet-point longitude="77.6034" latitude="13.0001"> </leaflet-point>
    <leaflet-point longitude="77.6006" latitude="12.9547"> </leaflet-point>

     Hi, I am a <b>polygon</b>.
  </leaflet-polygon>
<button type="button" class="zoomButton" name="button">Zoom</button>
<script type="text/javascript">
var map = new L.Map('map', {center: new L.LatLng(23.872461135014387, 90.38903832435608), zoom: 8});
   var osm = new L.TileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
   map.addLayer(osm);
   $.getJSON("<?php echo base_url(); ?>assets/leaflet/mapdata.php",function(data){
     var ratIcon = L.icon({
       iconUrl: '<?php echo base_url(); ?>assets/leaflet/final.png',
       iconSize: [12,24]
     });
     L.geoJson(data,{

       pointToLayer: function(feature,latlng){
         var marker = L.marker(latlng,{icon: ratIcon});

         marker.bindPopup(feature.properties.title);

         return marker;
       }
     }).addTo(map);
     
   });



   $(".zoomButton").click(function(){

   });
</script>
