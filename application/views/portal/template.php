<!DOCTYPE html>

<html lang="en-US">
<?php  $this->load->view('portal/template/header'); ?>

<body>
  <div class="page-wrapper">
    <?php $this->load->view('portal/template/menu.php'); ?>
    <div id="page-content" style="">



      <div class="container">
        <?php
        if($pageTitle!='Home')
        {
          ?>



      <section class="page-title">
        <ol class="breadcrumb">
        <li><a href="<?php echo base_url('portal') ?>">Home</a></li>
        <li><a href="#"><?php echo $pageTitle; ?></a></li>

      </ol>
        <h1><b><?php echo $pageTitle; ?></b></h1>
      </section>
      <?php
    }
    ?>


    <!--
    <ol class="breadcrumb">
    <li><a href="#">Home</a></li>
    <li><a href="#">Pages</a></li>
    <li class="active">Contact</li>
  </ol>
  <!--end breadcrumb-->
  <!--end page-title-->
  <section>

    <?php
    //exit;
    echo $_content;

    ?>

  </section>
</div>


<!--end container-->
</div>
<!--end page-content-->
  <?php $this->load->view('portal/template/modal'); ?>
<footer id="page-footer">
  <?php $this->load->view('portal/template/footer') ?>
</footer>
<!--end page-footer-->
</div>

<!--end page-wrapper-->
<a href="#" class="to-top scroll" data-show-after-scroll="600"><i class="arrow_up"></i></a>

<script type="text/javascript" src="<?php echo base_url(); ?>portalAssets/js/markerclusterer_packed.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>portalAssets/js/infobox.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>portalAssets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>portalAssets/js/jquery.fitvids.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>portalAssets/js/icheck.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>portalAssets/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>portalAssets/js/jquery.trackpad-scroll-emulator.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>portalAssets/js/custom.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>portalAssets/js/maps.js"></script>
<script src="<?php echo base_url(); ?>assets/js/portal-js/bootstrap-datetimepicker.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBedWlklrhQ5Y6NE2EG37Vtx-y8Nk0FYX8&libraries=places&callback=initMap"
>
</script>
<script type="text/javascript">

$(document).ready(function(){
  $(".rcorners2").mouseover(function(){
    $pb=$(this).parents("div.col-md-2:first");
    $element=$pb.find(".bottomText");
    $element.show();

  });
});
$(document).ready(function(){
  $(".rcorners2").mouseout(function(){
    $pb=$(this).parents("div.col-md-2:first");
    $element=$pb.find(".bottomText");
    $element.hide();

  });
});
$(document).ready(function () {
  $('.date_picker').datepicker({
    format: "yyyy-mm-dd"
  });
});
</script>
<script>

 var optimizedDatabaseLoading = 0;
 var _latitude = 40.7344458;
 var _longitude = -73.86704922;
 var element = "map-homepage";
 var markerTarget = "sidebar"; // use "sidebar", "infobox" or "modal" - defines the action after click on marker
 var sidebarResultTarget = "sidebar"; // use "sidebar", "modal" or "new_page" - defines the action after click on marker
 var showMarkerLabels = false; // next to every marker will be a bubble with title
 var mapDefaultZoom = 14; // default zoom
 heroMap(_latitude,_longitude, element, markerTarget, sidebarResultTarget, showMarkerLabels, mapDefaultZoom);

var optimizedDatabaseLoading = 0;
var _latitude = 23.6850;
var _longitude = 90.3563;
var element = "map-homepage";
var markerTarget = "modal"; // use "sidebar", "infobox" or "modal" - defines the action after click on marker
var sidebarResultTarget = "modal"; // use "sidebar", "modal" or "new_page" - defines the action after click on marker
var showMarkerLabels = false; // next to every marker will be a bubble with title
var mapDefaultZoom = 7; // default zoom
heroMap(_latitude, _longitude, element, markerTarget, sidebarResultTarget, showMarkerLabels, mapDefaultZoom);

</script>
</body>
