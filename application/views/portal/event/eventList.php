<style>
    .event_calender_slider {
        margin-bottom: 60px;
    }

    .rz_button {
        background-color: #012F49;
        color: #FFF;
        text-align: center;
        display: block;
        margin-top: 10px;
        border-radius: 3px;
    }

    .rz_button2 {
        background-color: #e63433;
        color: #FFF;
        font-size: 15px;
        text-align: center;
        padding: 6px 12px;
        text-transform: uppercase;
        border-radius: 25px;
    }

    .rz_button2:hover {
        color: #FFF;
    }

    .event_calender_slider:after {
        color: #FFFFFF;
        content: "PASSESD EVENTS";
        font-family: 'ralewaysemibold';
        font-size: 15px;
        left: 0;
        line-height: 1.3;
        position: absolute;
        text-align: center;
        top: 35%;
        width: 106px;
    }
.frame {
    height: 185px;
    display: table;
    width: 100%;
    text-align: center;
    padding: 0px;
}

.helper22 {
    display: table-cell;
    text-align: center;
    vertical-align: middle;
    height: 185px;
}

.frame>span>img {
    vertical-align: middle;
    display: inline-block !important;
    width: auto  !important;
    max-height: 100%;
    max-width: 100%;
}

.frame2 {
    height: 120px;
    display: table;
    width: 100%;
    text-align: center;
    padding: 0px;
}

.helper222 {
    display: table-cell;
    text-align: center;
    vertical-align: middle;
    height: 120px;
}

.frame2>span>img {
    vertical-align: middle;
    display: inline-block !important;
    width: auto  !important;
    max-height: 100%;
    max-width: 100%;
}
.thumb-a{
    display: block;
    border:1px solid #7F96AA;
    border-radius: 4px;
    padding-bottom: 15px;
        margin-right: 5px;
}
.thumb-a>.frame2{
    margin: 0px;
}

.roww-padd{
    padding: 0px 15px;
}
</style>


<div class="others_page_wrp">
    <section class="org_map_wrp">
        <div class="container">
            <br>
            <div class="col-md-12">
                <h2 class="pull-left" style="color:#012F49;">MY EVENTS</h2>
                <?php
                if($permission->PER>0)
                {
                    ?>
                    <a class="rz_button2 pull-right" href="<?php echo site_url(); ?>event/createEvent">Create new Event</a>
                    <?php
                }
                ?>

            </div>
            <br/> <br/>
            <hr/>

            <div class="row">

                <?php if(!empty($upcomingEvents)) : ?>
                <?php
                foreach ($upcomingEvents as $event) {

                    $imgName = explode('___', $event->images);
                    $imgName = $imgName[0];

                    ?>
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <a href="<?php echo site_url() . 'event/eventView/' . $event->id; ?>" target="_blank">

                            <div class="panel panelD" style="border: 1px solid #ddd">

                                <div class="thumbnail panel-image frame" style="margin-bottom: 2px">
                                    <span class="helper22">
                                        <img style="height: auto; width: auto; " src="<?php echo base_url();
                                        echo 'upload/events/' . $imgName ?>" alt="" class="img-responsive"/>
                                    </span>
                                </div>

                                <div class="event_calender_txt">
                                    <p><?php echo $event->event_name; ?></p>
                                    <p>By <?php echo $event->title; ?> <br/><span
                                                class="event_dates">From <?php echo date('d M, Y', strtotime($event->start_date)); ?>
                                            To <?php echo date('d M, Y', strtotime($event->end_date)); ?></span></p>
                                </div>

                                <p><a class="rz_button"
                                      href="<?php echo site_url() . 'event/editEvent/' . $event->id; ?>">Edit</a></p>
                                <p><a class="rz_button" onClick="return confirm('Are you sure, you want to delete?')"
                                      href="<?php echo site_url() . 'event/deleteEvent/' . $event->id; ?>">Delete</a>
                                </p>
                            </div>
                        </a>
                    </div>
                    <?php
                }
                ?>
                <?php else: ?>
                    <p class="alert alert-warning">No Events to show!</p>
                <?php endif; ?>
            </div><br>

            <h2 style=" color:#012F49;">PREVIOUS EVENTS</h2>

            <div class="row">
                <?php if(!empty($previousEvents)) : ?>
            <div class="event_calender_slider roww-padd">
                <div id="event-calender-slider2">

                    <?php foreach ($previousEvents as $event) :
                        $imgName = explode('___', $event->images);
                        $imgName = $imgName[0];
                     ?>
                        <a target="_blank" href="<?php echo site_url() . 'event/eventView/' . $event->id; ?>" class="thumb-a">
                            <div class="slideritem thumbnail frame2" style="margin-bottom: 5px;">
                                <span class="helper222">
                                    <img style="" src="<?php echo base_url();
                                    echo 'upload/events/' . $imgName ?>" alt=""/>
                                </span>
                            </div>
                            <div class="event_calender_txt">
                                <p><?php echo $event->event_name; ?></p>
                                <p>By <?php echo $event->title; ?> <br/><span
                                            class="event_dates">FROM <?php echo date('d M, Y', strtotime($event->start_date)); ?>
                                        TO <?php echo date('d M, Y', strtotime($event->end_date)); ?></span></p>
                            </div>

                        </a>
                    <?php endforeach; ?>
                </div>
            </div>

                <?php else : ?>
                    <p class="alert alert-warning">No  previous Events to show!</p>
                <?php endif; ?>
            </div>


        </div>
    </section>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        $("#event-calender-slider").owlCarousel({
            autoPlay: 3000,
            pagination: false,
            navigation: true,
            itemsCustom: [[0, 1], [479, 2], [768, 4], [991, 5], [1199, 5]],
        });
        $(function () {
            window.prettyPrint && prettyPrint();
        });
    });
</script>
<script>
    $(document).ready(function () {
        $("#event-calender-slider2").owlCarousel({
            autoPlay: 3000,
            pagination: false,
            navigation: true,
            itemsCustom: [[0, 1], [479, 2], [768, 4], [991, 5], [1199, 5]],
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.date_picker').datepicker({
            format: "yyyy-mm-dd"
        });
    });

    var a_href = $('#facebook_url_for_signup').attr('href');

    $('#facebook_url_for_login').attr('href', a_href);
</script>
<!-- JS ENDS -->
