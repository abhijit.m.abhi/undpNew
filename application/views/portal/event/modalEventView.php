<style>
    section.org_profile_section {
        padding: 10px;
        border: 1px solid red;
        margin-bottom: 5px;
    }
</style>

<div class="others_page_wrp">
    <section class="calender_event_view_wrp">
        <div class="container">
            <div class="calender_event_view_top">
                <div class="event_view_top_left">
                    <span class="view_avatar"><img src="<?php echo base_url();
                        echo $eventDetails->image; ?>" alt=""
                                                   style="width: 70px;"/></span>
                    <div class="event_view_top_left_title">
                        <a class="" href="<?php echo site_url() . 'event/viewEventModal/' . $eventDetails->id; ?>">
                            <h2
                                    style="margin: 12px 0px 0px 0px; color: #012F49;"><?php echo $eventDetails->org_name; ?></h2>
                        </a>
                        <span><?php echo $organizationDetails->location; ?></span>
                    </div><br>
                    <div class="pull-left">
                        <span type="button" class="btn btn-info btn-xs"> <span class="badge"><i
                                        class="fa fa-eye"></i> <?php echo $organizationDetails->visitcounter; ?></span></span>
                        <button type="button" class="btn btn-success btn-xs">Events: <span
                                    class="badge"><?php echo $eventCount->ev_count; ?></span></button>
                        <button type="button" class="btn btn-danger btn-xs">Media Post: <span
                                    class="badge"><?php echo $blogCount->op_count; ?></span></button>
                    </div>

                    <div class="clearfix"></div>
                </div>
            </div>


            <div class="calender_event_view_slider">
                <div id="event_view_slider">
                    <?php if (!empty($allImagesData)) : ?>
                        <?php foreach ($allImagesData as $image) : ?>
                            <?php if ($image->type == 'gallery') : ?>
                                <div class="slideritem thumbnail" style="margin-bottom: 5px"><img
                                            style="height: 390px; width: auto;"
                                            src="<?php echo base_url(); ?><?php echo $image->path; ?>"
                                            alt=""/></div>
                            <?php endif; ?>

                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>

                <div class="clearfix"></div>
            </div>


            <div class="col-sm-5 col-xs-12 event_view_left">
                <div class="event_view_left_map">
                    <h3>Location</h3> <br>
                    <p> <?php echo $organizationDetails->location; ?> </p>
                    <div id="google_map" style="width: 100%; height: 195px;"></div>
                </div>
                <div class="directory_profile_contact">
                    <h3>Contact</h3>
                    <ul>
                        <li>
                            <span><i class="glyphicon glyphicon-map-marker"></i></span> <?php echo $organizationDetails->location; ?>
                        </li>
                        <?php if (!empty($organizationDetails->phone)): ?>
                            <li><span><i class="fa fa-phone-square"></i></span> <a
                                        href="tel:<?php echo $organizationDetails->phone; ?>"><?php echo $organizationDetails->phone; ?></a>
                            </li>
                        <?php endif; ?>
                        <?php if (!empty($organizationDetails->mobile)): ?>
                            <li><span><i class="fa fa-phone"></i></span> <a
                                        href="tel:<?php echo $organizationDetails->mobile; ?>"><?php echo $organizationDetails->mobile; ?></a>
                            </li>
                        <?php endif; ?>

                        <?php if (!empty($organizationDetails->url)): ?>
                            <li><span><i class="fa fa-globe"></i></span> <a
                                        href="<?php echo $organizationDetails->url; ?>"
                                        target="_blank"><?php echo $organizationDetails->url; ?></a></li>
                        <?php endif; ?>
                        <?php if (!empty($organizationDetails->org_email)): ?>
                            <li><span><i class="fa fa-envelope-o"></i></span> <a
                                        href="mailto:<?php echo $organizationDetails->org_email; ?>"><?php echo $organizationDetails->org_email; ?></a>
                            </li>
                        <?php endif; ?>
                        <?php if (!empty($organizationDetails->facebook)): ?>
                            <li><span><i class="fa fa-facebook"></i></span> <a
                                        href="<?php echo $organizationDetails->facebook; ?>"
                                        target="_blank"><?php echo $organizationDetails->facebook; ?></a></li>
                        <?php endif; ?>
                        <?php if (!empty($organizationDetails->twitter)): ?>
                            <li><span><i class="fa fa-twitter"></i></span> <a
                                        href="<?php echo $organizationDetails->twitter; ?>"
                                        target="_blank"><?php echo $organizationDetails->twitter; ?></a></li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>


            <div class="col-sm-7 col-xs-12 event_view_right">
                <div class="event_view_right_top">
                    <h3>About</h3>
                    <div class="read-more" data-collapse-height="75"><p
                                class="directory_profile_text1"><?php echo htmlspecialchars_decode(html_entity_decode($organizationDetails->description)); ?></p>
                    </div>
                    <!--                            <a href="" class="directory_profile_more_btn">More</a>-->
                </div>
                <br>
<!---->
<!--                <section>-->
<!--                    <h3>INTEREST AREA OF THE ORGANIZATION</h3>-->
<!--                    <ul class="tags">-->
<!--                        --><?php //if (!empty($organizationDetails->iao)) : ?>
<!--                            --><?php //$iao = explode(",", $organizationDetails->iao); ?>
<!---->
<!--                            --><?php //foreach ($iao as $v) : ?>
<!---->
<!---->
<!--                                <li style="width:24px; text-align:center;" title="--><?php //echo $v; ?><!--"><span-->
<!--                                            class="--><?php //echo $this->event_model->getIaoIcon($v); ?><!--"></span></li>-->
<!--                            --><?php //endforeach; ?>
<!--                        --><?php //endif; ?>
<!--                    </ul>-->
<!--                </section>-->


                <!--                <div class="latest_reviews_box event_view_calender_relavent">-->
                <!--                    --><?php //if (!empty($relEventData)): ?>
                <!--                        <h4>Other Events</h4>-->
                <!--                        --><?php //foreach ($relEventData as $event): $images = explode('___', $event->images);
                //                            $image = $images[0]; ?>
                <!--                            <div class="col-xs-3">-->
                <!--                                <a href="--><?php //echo site_url() ?><!--/event/eventView/-->
                <?php //echo $event->id; ?><!--">-->
                <!--                                    <div class="thumbnail" style="margin-bottom: 5px">-->
                <!--                                        <img style="height: 120px;" src="-->
                <?php //echo base_url(); ?><!--upload/events/--><?php //echo $image; ?><!--" alt=""/>-->
                <!--                                    </div>-->
                <!--                                    <div class="event_calender_txt">-->
                <!--                                        <p>--><?php //echo $event->event_name; ?><!--</p>-->
                <!--                                        <p>By --><?php //echo $event->title; ?>
                <!--                                            <br>-->
                <!--                                            START <span-->
                <!--                                                    class="event_dates">-->
                <?php //echo date('d M, Y', strtotime($event->start_date)); ?><!--</span>-->
                <!--                                            <br>-->
                <!--                                            END <span-->
                <!--                                                    class="event_dates">-->
                <?php //echo date('d M, Y', strtotime($event->end_date)); ?><!--</span>-->
                <!--                                        </p>-->
                <!--                                    </div>-->
                <!--                                </a>-->
                <!--                            </div>-->
                <!--                        --><?php //endforeach; ?>
                <!--                    --><?php //else: ?>
                <!--                        <p> No Relevant Event Found </p>-->
                <!--                    --><?php //endif; ?>
                <!--                    <div class="clearfix"></div>-->
                <!--                </div>-->


                <div class="latest_reviews_box event_view_calender_relavent">
                    <h4>Events</h4>
                    <?php if (!empty($relEventData)): ?>
                    <div id="event-calender-slider2">

                        <?php foreach ($relEventData as $event): $images = explode('___', $event->images);
                            $image = $images[0]; ?>

                            <a href="<?php echo site_url() ?>blog/blogView/<?php echo $event->id; ?>">
                                <div class="thumbnail" style="margin-bottom: 5px">
                                    <img style="height: 120px; width: auto;" src="<?php echo base_url(); ?>upload/events/<?php echo $image; ?>" alt=""/>
                                </div>
                                <div class="event_calender_txt">
                                    <!--                                    <p>--><?php //echo $blog->event_name; ?><!--</p>-->
                                    <p>By <?php echo $event->title; ?>
                                        <br>
                                        START <span
                                                class="event_dates"><?php echo date('d M, Y', strtotime($event->start_date)); ?></span>

<!--                                        END <span-->
<!--                                                class="event_dates">--><?php //echo date('d M, Y', strtotime($event->end_date)); ?><!--</span>-->
                                    </p>
                                </div>
                            </a>

                        <?php endforeach; ?>
                    </div>

                    <?php else: ?>
                        <p> No relevant event post found </p>
                    <?php endif; ?>


                </div>
                <div class="clearfix"></div>
                <div class="latest_reviews_box event_view_calender_relavent">
                    <h4>Media Blogs</h4>
                    <?php if (!empty($relBlogData)): ?>
                        <div id="event-calender-slider2">

                            <?php foreach ($relBlogData as $blog): $images = explode('___', $blog->images);
                                $image = $images[0]; ?>

                                <a href="<?php echo site_url() ?>blog/blogView/<?php echo $blog->id; ?>">
                                    <div class="thumbnail" style="margin-bottom: 5px">
                                        <img style="height: 120px; width: auto;" src="<?php echo base_url(); ?>upload/blogs/<?php echo $image; ?>" alt=""/>
                                    </div>
                                    <div class="event_calender_txt">
    <!--                                    <p>--><?php //echo $blog->event_name; ?><!--</p>-->
                                        <p>By <?php echo $blog->title; ?>
                                            <br>
                                            Posted On <span
                                                    class="event_dates"><?php echo date('d M, Y', strtotime($blog->post_date)); ?></span>
                                            <br>
                                            END <span
                                                    class="event_dates"><?php // echo date('d M, Y', strtotime($blog->end_date)); ?></span>
                                        </p>
                                    </div>
                                </a>

                            <?php endforeach; ?>
                        </div>

                    <?php else: ?>
                        <p> No relevant blog post found </p>
                    <?php endif; ?>

                </div>


            </div>









        </div>
    </section>
</div>

<!--<script src="https://apis.google.com/js/platform.js" async defer></script>-->
<!---->
<!--<script>-->
<!--    var optimizedDatabaseLoading = 0;-->
<!--    var _latitude = 23.6850;-->
<!--    var _longitude = 90.3563;-->
<!--    var element = "map-homepage";-->
<!--    var markerTarget = "modal"; // use "sidebar", "infobox" or "modal" - defines the action after click on marker-->
<!--    var sidebarResultTarget = "modal"; // use "sidebar", "modal" or "new_page" - defines the action after click on marker-->
<!--    var showMarkerLabels = false; // next to every marker will be a bubble with title-->
<!--    var mapDefaultZoom = 7; // default zoom-->
<!--    heroMap(_latitude, _longitude, element, markerTarget, sidebarResultTarget, showMarkerLabels, mapDefaultZoom);-->
<!--</script>-->


<script>
    $(document).ready(function () {
        $("#event_view_slider").owlCarousel({
            autoPlay: 3000,
            pagination: false,
            navigation: true,
            singleItem: true,
        });


        $('.countdown').downCount(
            {
                date: '<?php echo date('m/d/Y H:i:s', strtotime($eventDetails->start_date . ' ' . $eventDetails->start_time)) ?>',
                offset: +6
            }, function () {

            }
        );

        $('#interested_btn').on('click', function () {
            var user_id = $(this).data('user-id');
            var event_id = $(this).data('event-id');
            if (!user_id) {
                alert('Please login to perform this task');
                return false;
            }
            $.ajax({
                url: '../calSearch',
                type: 'POST',
                data: {
                    user_id: user_id,
                    event_id: event_id,
                    action: 'interest'
                }
            }).done(function (response) {
                var res = $.parseJSON(response);
                $('#interested_count').html(res.count);
                if (!res.ack) {
                    alert(res.msg);
                }
            });
        });

        $('#going_btn').on('click', function () {

            var user_id = $(this).data('user-id');
            var event_id = $(this).data('event-id');
            if (!user_id) {
                alert('Please login to perform this task');
                return false;
            }
            $.ajax({
                url: '../calSearch',
                type: 'POST',
                data: {
                    user_id: user_id,
                    event_id: event_id,
                    action: 'going'
                }
            }).done(function (response) {
                var res = $.parseJSON(response);
                $('#going_count').html(res.count);
                if (!res.ack) {
                    alert(res.msg);
                }
            });
        })
    });
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgGR-bhDxioHdiX8mtFYkNliRAY6CsLao&callback=initMap" async
        defer>
</script>
<script>
    function initMap() {
        // Styles a map in night mode.
        var latLong = {
            lat: <?php echo $organizationDetails->latitude; ?>,
            lng: <?php echo $organizationDetails->longitude; ?>};
        var map = new google.maps.Map(document.getElementById('google_map'), {
            center: latLong,
            zoom: 16,
            styles: [{"elementType": "geometry", "stylers": [{"color": "#f5f5f5"}]}, {
                "elementType": "labels.icon",
                "stylers": [{"visibility": "off"}]
            }, {
                "elementType": "labels.text.fill",
                "stylers": [{"color": "#616161"}]
            }, {
                "elementType": "labels.text.stroke",
                "stylers": [{"color": "#f5f5f5"}]
            }, {
                "featureType": "administrative.land_parcel",
                "elementType": "labels.text.fill",
                "stylers": [{"color": "#bdbdbd"}]
            }, {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [{"color": "#eeeeee"}]
            }, {
                "featureType": "poi",
                "elementType": "labels.text.fill",
                "stylers": [{"color": "#757575"}]
            }, {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [{"color": "#e5e5e5"}]
            }, {
                "featureType": "poi.park",
                "elementType": "labels.text.fill",
                "stylers": [{"color": "#9e9e9e"}]
            }, {
                "featureType": "road",
                "elementType": "geometry",
                "stylers": [{"color": "#ffffff"}]
            }, {
                "featureType": "road.arterial",
                "elementType": "labels.text.fill",
                "stylers": [{"color": "#757575"}]
            }, {
                "featureType": "road.highway",
                "elementType": "geometry",
                "stylers": [{"color": "#dadada"}]
            }, {
                "featureType": "road.highway",
                "elementType": "labels.text.fill",
                "stylers": [{"color": "#616161"}]
            }, {
                "featureType": "road.local",
                "elementType": "labels.text.fill",
                "stylers": [{"color": "#9e9e9e"}]
            }, {
                "featureType": "transit.line",
                "elementType": "geometry",
                "stylers": [{"color": "#e5e5e5"}]
            }, {
                "featureType": "transit.station",
                "elementType": "geometry",
                "stylers": [{"color": "#eeeeee"}]
            }, {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [{"color": "#c9c9c9"}]
            }, {"featureType": "water", "elementType": "labels.text.fill", "stylers": [{"color": "#9e9e9e"}]}]
        });

        var marker = new google.maps.Marker({
            position: latLong,
            map: map,
            icon: '<?php echo base_url(); ?>old/assets/img/marker.png'
        });
    }
</script>



<script>
    function initializeReadMore() {

        $.ajax({
            type: "GET",
            url: "<?php echo base_url();?>old/assets/js/readmore.min.js",
            success: readMoreCallBack,
            dataType: "script",
            cache: true
        });

        function readMoreCallBack() {
            var collapseHeight;
            var $readMore = $(".read-more");
            if ($readMore.attr("data-collapse-height")) {
                collapseHeight = parseInt($readMore.attr("data-collapse-height"), 10);
            } else {
                collapseHeight = 55;
            }
            $readMore.readmore({
                speed: 500,
                collapsedHeight: collapseHeight,
                blockCSS: 'display: inline-block; width: auto; min-width: 120px;',
                moreLink: '<a href="#" class="btn btn-primary btn-xs btn-light-frame btn-framed btn-rounded">More<i class="icon_plus"></i></a>',
                lessLink: '<a href="#" class="btn btn-primary btn-xs btn-light-frame btn-framed btn-rounded">Less<i class="icon_minus-06"></i></a>'
            });
        }
    }


    initializeReadMore();
</script>

<script>
    $(document).ready(function () {
        $("#event-calender-slider2").owlCarousel({
            autoPlay: 3000,
            pagination: false,
            navigation: true,
            items : 3
        });
    });
</script>

