<style type="text/css">
    .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover {
        border: 1px solid #a5a4a4;
        border-bottom-color: transparent;
        font-weight: bold;
        font-family: 'ralewaybold';
        background-color: #012F49;
        color: white;
    }

.frame {
    height: 120px;
    display: table;
    width: 100%;
    text-align: center;
    padding: 0px;
}

.helper22 {
    display: table-cell;
    text-align: center;
    vertical-align: middle;
    height: 120px;
}

.frame>span>img {
    vertical-align: middle;
    display: inline-block !important;
    width: auto  !important;
    max-height: 100%;
    max-width: 100%;
}

.frame2 {
    height: 120px;
    display: table;
    text-align: center;
    padding: 0px;
}

.helper222 {
    display: table-cell;
    text-align: center;
    vertical-align: middle;
    height: 120px;
}

.frame2>span>img {
    vertical-align: middle;
    display: inline-block !important;
    width: auto  !important;
    max-height: 100%;
    max-width: 100%;
}

    .rz_button2 {
        background-color: #e63433 !important;
        color: #FFF;
        font-size: 15px;
        text-align: center;
        padding: 6px 12px !important;
        text-transform: uppercase;
        border-radius: 25px !important;
        border: 0px !important;
    }

    .rz_button2:hover {
        background-color: #e63433 !important;
        color: #FFF;
    }

    .event_calender_event input[type="search"] {
        width: 25% !important;
    }

</style>



<div class="others_page_wrp">
    <section class="org_map_wrp">
        <div class="container">

            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#EventUpcomming">Upcoming Events</a></li>
                <!-- <li><a data-toggle="tab" href="#EventOngoing">On-Going</a></li> -->
                <li><a data-toggle="tab" href="#EventPrevious">Previous Events</a></li>

            </ul>

            <div class="tab-content">
                <div id="EventUpcomming" class="tab-pane fade in active">
                    <!-- <h3>Upcomming Events</h3> -->
                    <div class="event_calender_slider">
                        <div id="event-calender-slider">
                            <?php foreach ($upcomingEvents as $event) : ?>
                               <?php $imgName = explode('___', $event->images);
                                $image = $imgName[0];
                               ?>
                                <a href="<?php echo site_url() ?>event/eventView/<?php echo $event->id; ?>"
                                   target="_blank">
                                    <div class="slideritem thumbnail">

                                        <div class="text-center frame"
                                             style="/*height: 120px; border-bottom: 1px solid #ddd; vertical-align: text-top;*/  ">
                                             <span class="helper22">
                                            <img style="/*max-height: 120px; width: auto;*/ "
                                                 src="<?php echo base_url(); ?>upload/events/<?php echo $image ?>"
                                                 alt="" class="img-responsive"/></span>
                                        </div>


                                        <div class="event_calender_txt">
                                            <p style="min-height: 34px;"><?php echo $event->event_name; ?></p>
                                            <p>By <?php echo $event->title; ?> <br/><span
                                                        class="event_dates">START <?php echo date('d M, Y', strtotime($event->start_date)) ?>
                                                        <?php if(($event->end_date >= date("Y-m-d")) && ($event->start_date <= date("Y-m-d")) ) : ?>
                                                            <span style="color: red;"><b>(On Going)</b></span>
                                                        <?php endif; ?>
                                                    <br>END <?php echo date('d M, Y', strtotime($event->end_date)) ?></span>
                                            </p>
                                        </div>
                                    </div>
                                </a>
                            <?php endforeach; ?>

                        </div>
                    </div>
                </div>

                <div id="EventPrevious" class="tab-pane fade">
                    <!-- <h3>Previous Events</h3> -->
                    <div class="previous-event-calender-slider">
                        <div id="previous-event-calender-slider">
                            <?php foreach ($previousEvent as $event) : ?>
                                <?php $imgName = explode('___', $event->images);
                                $imgName = $imgName[0];
                                ?>
                                <a target="_blank" href="<?php echo site_url() . 'event/eventView/' . $event->id; ?>">
                                    <div class="slideritem thumbnail">

                                        <div class="text-center frame"
                                             style=" border-bottom: 1px solid #ddd;   ">
                                             <span class="helper22">
                                            <img style="" src="<?php echo base_url();
                                            echo 'upload/events/' . $imgName ?>" alt="" class="img-responsive"/></span>
                                        </div>

                                        <div class="event_calender_txt">
                                            <p><?php echo $event->event_name; ?></p>
                                            <p>By <?php echo $event->title; ?> <br/><span
                                                        class="event_dates">FROM <?php echo date('d M, Y', strtotime($event->start_date)); ?>
                                                    TO <?php echo date('d M, Y', strtotime($event->end_date)); ?></span>
                                            </p>
                                        </div>
                                    </div>
                                </a>
                            <?php endforeach; ?>

                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="event_calender_slider">
                <div id="event-calender-slider">
                    <?php foreach ($upcomingEvents as $event) :
                $image = $event->images; ?>
                        <a href="<?php echo site_url() ?>/event/eventView/<?php echo $event->id; ?>" target="_blank">
                            <div class="slideritem thumbnail">
                                <img  style="height: 120px; width: auto;" src="<?php echo base_url(); ?>upload/events/<?php echo $image ?>" alt=""/>
                                <div class="event_calender_txt">
                                    <p style="min-height: 34px;"><?php echo $event->event_name; ?></p>
                                        <p>By <?php echo $event->title; ?> <br/><span
                                            class="event_dates">START <?php echo date('d M, Y', strtotime($event->start_date)) ?>
                                            <br>END <?php echo date('d M, Y', strtotime($event->end_date)) ?></span>
                                    </p>
                                </div>
                            </div>
                        </a>
                    <?php endforeach; ?>
                </div>
            </div> -->

            <div class="event_calender_searchbox">
                <div class="event_calender_searc_row1 text-center">

                    <?php $monthList = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December'); ?>
                    <select name="search_month" id="search_month" class="event_calender_month">
                        <option value="">Month</option>
                        <?php foreach ($monthList as $month_id => $month_name): ?>
                            <option value="<?php echo $month_id ?>"><?php echo $month_name ?></option>
                        <?php endforeach; ?>
                    </select>
                    <select name="search_year" id="search_year" class="event_calender_year">
                        <option value="">Year</option>
                        <?php for ($i = -2; $i < 3; $i++): ?>
                            <option value="<?php echo date('Y') - $i; ?>"><?php echo date('Y') - $i; ?></option>
                        <?php endfor; ?>
                    </select>
                    <span class="event_calender_event">
                                <input list="search_text1" type="search" name="search_text1" id="search_text"
                                       placeholder="SEARCH EVENT"/>
                                <button type="submit"></button>

                        <datalist id="search_text1">
                            <?php foreach ($event_title_list

                            as $e_list) : ?>
                            <option value="<?php echo $e_list->event_name; ?>">
                            <option value="<?php echo $e_list->category; ?>">
                        <?php endforeach; ?>
                        </datalist>

                    </span>
                    <span>
                        <button type="button" id="search_btn" class="btn btn-primary btn-rounded btn-xs"
                                name="search_btn">Search
                        </button>
                    </span>


                    <?php if($permission->PER > 0 ) : ?>
                    <span>
                            <a class="rz_button2" href="<?php echo site_url();?>event/createEvent">Create New Event</a>
                    </span>

                    <?php endif; ?>



                    <div class="clearfix"></div>
                </div>

                <hr>

                <div class="event_calender_searc_row2">

                </div>


                <div class="event_calender_content">
                    <div class="col-sm-6 col-xs-12 event_calender_content_left">
                        <div id="event-calender" class="event_calender_widget"></div>
                    </div>
                    <div class="col-sm-6 col-xs-12 event_calender_content_right" id="search_result_container">
                        <div class="event_calender_date">
                            <p>SOME UPCOMING EVENTS</p>
                        </div>
                        <?php

                        foreach ($someEvent as $event):
                            $images = explode('___', $event->images);
                            $image = $images[0];
                            $interest = $this->event_model->getInterestCount($event->id);
                            $going = $this->event_model->getGoingCount($event->id);
                            ?>
                            <a href="<?php echo site_url() ?>event/eventView/<?php echo $event->id; ?>" target="_blank"
                               ;>
                                <div class="event_calender_content_box" style="margin-bottom: 10px;">
                                    <h5>
                                        <img src="<?php echo base_url(); ?><?php echo $event->image; ?>"> <?php echo $event->title ?>
                                    </h5>
                                    <div class="col-xs-3 thumbnail frame2">
                                        <span class="helper222">
                                            <img alt="" src="<?php echo base_url(); ?>upload/events/<?php echo $image; ?>">
                                        </span>
                                    </div>
                                    <div class="col-xs-9">
                                        <h6><?php echo $event->event_name; ?></h6>
                                        <ul>
                                            <li>
                                                DATE: <?php echo date('d F, Y', strtotime($event->start_date)) . ' - ' . date('d F, Y', strtotime($event->end_date)) ?></li>
                                            <!-- <li>CATEGORY: <?php echo $event->category; ?></li> -->
                                            <li>LOCATION: <?php echo $event->address ?></li>
                                            <li>SEAT
                                                LIMIT: <?php echo ($event->seat_limit > 0) ? $event->seat_limit . ' Person' : 'Unlimited' ?></li>
                                            <li>EVENT
                                                FEE: <?php echo ($event->event_fee > 0) ? $event->event_fee . ' Taka' : 'Free' ?></li>
                                            <!--                                            <li><a href="-->
                                            <?php //echo $event['registration_link']
                                            ?><!--" target="_blank">REGISTRATION LINK </a></li>-->
                                        </ul>
                                        <div class="share_box_wrp">
                                            <a title="Going"><span class="share_box interested_box"> <img id="g_id" style="width: auto; height: 18px;" src="<?php echo base_url().'assets/img2/running.png'; ?>"> <span><?php echo $going->int_count; ?></span></span></a>

                                            <a title="Interested"><span class="share_box interested_box"> <img id="i_id" style="width: auto; height: 18px;" src="<?php echo base_url().'assets/img2/star.png'; ?>"> <span><?php echo $interest->int_count; ?></span></span>

                                            <span class="likes_box"><i
                                                        class="fa fa-eye"></i> <span><?php echo $event->view_count ?></span></span></a>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        <?php endforeach; ?>
                    </div>
                    <div class="clearfix"></div>
                </div>


            </div>

        </div>
    </section>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        $("#event-calender-slider").owlCarousel({
            autoPlay: 3000,
            pagination: false,
            navigation: true,
            itemsCustom: [[0, 1], [479, 2], [768, 5], [991, 6], [1199, 6]],
        });
        $("#previous-event-calender-slider").owlCarousel({
            autoPlay: 3000,
            pagination: false,
            navigation: true,
            itemsCustom: [[0, 1], [479, 2], [768, 5], [991, 6], [1199, 6]],
        });


        var active_dates = <?php echo json_encode($eventsDate);?>;
		//console.log(active_dates);

        // convert array of objects into array of property
        var finalArray = active_dates.map(function (obj) {
            return obj.start_date;
        });

        var active_dates = finalArray;

        $(function () {

            window.prettyPrint && prettyPrint();
            var calender = $('#event-calender').on('show', function (e) {
                $('.day');
            }).datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
                todayHighlight: true,
                beforeShowDay: function (date) {
                    var d = date;
                    var curr_date = ("0" + d.getDate()).slice(-2);
                    var curr_month = ("0" + (d.getMonth() + 1)).slice(-2); //Months are zero based
                    var curr_year = d.getFullYear();

                    var formattedDate = curr_year + "-" + curr_month + "-" + curr_date;

                    if ($.inArray(formattedDate, active_dates) != -1) {

                        return {
                            tooltip: 'Event',
                            classes: 'date-highligh'
                        };
                    }
                    return;
                }

            }).on('changeDate', function (e) {

                $.ajax({
                    url: 'calSearch',
                    type: 'POST',
                    data: {date: e.format(), action: 'cal_search'}
                }).done(function (response) {
//                                                console.log(response);
                    $('#search_result_container').html(response);
                });
                //                        console.log(e.format());
            });

        });
    });

    $('#g_id').attr(("title", "Going"));

</script>
