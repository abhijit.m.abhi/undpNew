<style>
    @font-face {
        font-family: SolaimanLipi;
        src: url('fonts/SolaimanLipi_new.ttf');
    }
    .aboutus_modal_content {
        border: 8px solid #7F96AA;
        margin: 20px auto;
        padding: 20px 5px;
        position: relative;
        width: 82%;
    }
footer{
       background-color: #f8f8f8;
    }
.unv-img{
    float: right;
    margin-top: 15px;
}
.undp-img{
    float: left;
}
@media (max-width: 750px){
    .footer_col2, .footer_col3{
        text-align: center;
    }
    .unv-img, .undp-img{
        float: none;
    }
}
.aboutus_modal_content > h1:before, .aboutus_modal_content > h1:after {
    width: 100%;
    z-index: -1;
}
.aboutus_modal_content > h1 span {
    background-color: #fff;
}
.modal-dialog2{
    margin-left: auto;
    margin-top: 30px;
    width: auto;
}
@media (min-width: 768px)
{
   .modal-dialog2 {
        width: 600px;
    }
}
@media (min-width: 1200px){
    .modal-dialog2 {
        width: 800px !important;
    }
}
.abt-tab>li>a{
    color: #333;
    font-weight: 700;
}
.abt-tab>li.active>a, .abt-tab>li.active>a:hover, .abt-tab>li.active>a:focus{
    background-color: #012F49;
    color: #fff;
    font-weight: 700;
}
.tab-pane>p, .aboutus_modal_text p{
    font-size: 18px;
    margin-bottom: 20px;
}
.tab-pane>h4, .aboutus_modal_text>h4{
    font-weight: 700;
}
.disclaimer-text{
    text-align: center;
    color: #F9C332;
    font-size: 45px;
    margin-bottom: 30px;
    position: relative;
    font-weight: 700;
    margin-top: 0px;
    margin-bottom: 15px;
    background-color: #012F49;
    border: 2px solid #012F49;
}
.disclaimer-text>span{
    border: 1px solid #012F49;
    padding: 0px 10px;
}
button.close{
    position: absolute;
    right: -12px;
    top: -10px;
    background-color: #fff;
    border-radius: 50%;
    padding: 2px 6px;
    z-index: 1000;
    opacity: 1;
}
button.close:hover{
    opacity: 1;
}
.footer_col1 a{
    text-decoration: none;
}
.footer_col1 a>b:focus, {
    outline: 0;
}
.footer_col1 a:focus{
    outline: 0;
}


</style>
 <?php
 $url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $actual_link=str_replace("/","--",$url);
    ?>
<footer>
  
  <div class="container">
    <div class="row">
      <div class="col-sm-3  footer_col1 text-center">

        <a href="#" data-toggle="modal" data-target="#about_us_modal"><b>ABOUT US</b></a>
        <a href="#" data-toggle="modal" data-target="#about_us_disclaimer"><b>DISCLAIMER</b></a>
      </div>
      <div class="col-sm-4  footer_col2">
        <a href="http://www.unv.org/" target="_blank" class="unv-img"><img src="<?php echo base_url('portalAssets/homePage') ?>/img/unvbd.png" alt="" class="unvbd_img">
        </a>
      </div>
      <div class="col-sm-2 footer_col2">
        <a href="http://www.bd.undp.org/" target="_blank"  class="undp-img"><img src="<?php echo base_url('portalAssets/homePage') ?>/img/undp.png" alt="" class="undp_img"></a>
      </div>
      <div class="col-sm-3  footer_col3 text-left">
        <a href="https://www.facebook.com/youthopia.bangla" target="_blank" class="facebook_icon"><i class="fa fa-facebook"></i></a>
        <a href="https://twitter.com/youthopiabangla" target="_blank" class="twitter_icon"><i class="fa fa-twitter"></i></a>
         <a href="<?php echo site_url("portal/report_problem_front_page/".$actual_link) ?>"  class="email_icon modalLink" style="color: #fff !important" data-modal-size="modal-md" title="Send Us A Quick Message"><i class="fa fa-envelope-o"></i></a>
      </div>
    </div>
    <div class="clearfix"></div>
  </div>
</footer>

<div class="modal fade aboutus_modal_wrp" id="about_us_modal">
    <div class="modal-dialog modal-dialog2">
        <button data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span><span
          class="sr-only">Close</span></button>
        <div class="modal-content">
            <div class="aboutus_modal_content">

                <h1 class="disclaimer-text"><span>ABOUT US</span></h1>
                <div class="aboutus_modal_text text-left" style="margin-top: 10px">
                    <ul class="nav nav-tabs abt-tab">
                        <li class="active"><a data-toggle="tab" href="#menu1">English</a></li>
                        <li><a data-toggle="tab" href="#home">বাংলা</a></li>

                    </ul>
                    <div class="tab-content">
                        <div id="menu1" class="tab-pane fade in active">
                            <p style="margin-top: 10px;">Youthopia.bangla is a space created with Bangladeshi youth in
                                mind. Here you can dream about your ideal place and shape it along with likeminded
                                people.</p>
                            <h4 style="font-size: x-large;">Vision</h4>
                            <p style="text-align: justify">A country of equal opportunities for all, backed by knowledge
                                sharing, participation &amp; innovation.</p>
                            <h4 style="font-size: x-large;">Mission</h4>
                            <p style="text-align: justify">To build bridges between the dreams of the Bangladeshi youth
                                &amp; the reality of the country by facilitating the access to the information required to
                                take informed decisions.</p>
                            <br>
                        </div>

                        <div id="home" class="tab-pane fade" style="font-family: SolaimanLipi">
                            <p style="margin-top: 15px;">ইয়ুথওপিয়া.বাংলা বাংলাদেশের একমাত্র ইয়ুথ নেটওয়ার্ক যা দেশের
                                উন্নয়নে যুবাদের অবদান, তাদের কর্মকাণ্ড ও চাহিদার প্রতিচ্ছবি। এই নেটওয়ার্কের মাধ্যমে
                                তরুণরা যথাযথ তথ্যের ভিত্তিতে সিধান্ত গ্রহণ, সমমনা সংগঠনদের সাথে সংযোগ স্থাপন এবং নিজেদের
                                দক্ষতা বৃদ্ধিতে সচেষ্ট হবে। বাংলাদেশকে নিয়ে এ দেশের যুবসমাজ যে স্বপ্ন দেখে, তার
                                বাস্তবায়ন তরুণরা ইয়ুথওপিয়া.বাংলাকে সাথে নিয়ে এগিয়ে যাবে।</p>
                            <h4 style="font-size: x-large;">লক্ষ্য </h4>
                            <p style="text-align: justify">উদ্ভাবন, অংশগ্রহণ ও তথ্যের সহজপ্রাপতার মধ্য দিয়ে একটি
                                সম্ভবনাময় বাংলাদেশ গড়ে তোলা।</p>
                            <h4 style="font-size: x-large;">উদ্দেশ্য </h4>
                            <p style="text-align: justify">যথাযথ ও যুগোপযোগী তথ্য সরবরাহের মাধ্যমে বাংলাদেশের তরুণদের
                                স্বপ্ন ও বাস্তবতার মাঝে সেতু বন্ধন তৈরি করা।</p>
                            <br>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- About Us Model END  -->
<!-- Disclaimer Model START -->
<div class="modal fade aboutus_modal_wrp" id="about_us_disclaimer">
    <div class="modal-dialog modal-dialog2">
    <button data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span><span
          class="sr-only">Close</span></button>
        <div class="modal-content">
            <div class="aboutus_modal_content">

                <h1 class="disclaimer-text"><span>DISCLAIMER</span></h1>
                <div class="aboutus_modal_text text-left">
                    <h4>Disclaimer</h4>
                    <p style="text-align: justify">
                        Last updated: April 03, 2017
                        <br>
                        <br>
                        The information contained on <a href="https://www.youthopiabangla.org">https://www.youthopiabangla.org</a>
                        website (the "Service") is for general information purposes only.
                        <br>
                        <br>
                        Youthopia.bangla assumes no responsibility for errors or omissions in the contents on the
                        Service.
                        <br>
                        <br>
                        In no event shall Youthopia.bangla be liable for any special, direct, indirect, consequential,
                        or incidental damages or any damages whatsoever, whether in an action of contract, negligence or
                        other tort, arising out of or in connection with the use of the Service or the contents of the
                        Service. Youthopia.bangla reserves the right to make additions, deletions, or modification to
                        the contents on the Service at any time without prior notice.
                        <br>
                        <br>
                        Youthopia.bangla does not warrant that the website is free of viruses or other harmful
                        components.
                        <br><br>
                    </p>
                    <h4>External links disclaimer</h4>
                    <p>https://www.youthopiabangla.org website may contain links to external websites that are not provided
                    or maintained by or in any way affiliated with Youthopia.bangla</p>
                    <br>
                    <br>
                    <p>Please note that the Youthopia.bangla does not guarantee the accuracy, relevance, timeliness, or
                    completeness of any information on these external websites.</p>
                    <label>Please find details in our <a href="<?php echo base_url();?>upload/Terms_and_Conditions.pdf" target="_blank"><u> TERMS &amp;
                                CONDITIONS</u></a></label> <label>and <a href="<?php echo base_url();?>upload/Privacy_Policy.pdf" target="_blank"><u>
                                Privacy &amp; Policy</u></a></label>
                </div>
            </div>
        </div>
    </div>
</div>
