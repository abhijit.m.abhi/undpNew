<?php
$userInfo=$this->session->userdata('user_logged_user');
$session_info_fb = $this->session->userdata("userData");
 //echo "<pre>";
 //print_r($session_info_fb);
// exit;

if(!empty($userInfo or $session_info_fb));
{
  $portalUser=$this->session->userdata('user_logged_user');
  $portalUserFb = $this->session->userdata("userData");
  //=$_SESSION['user_logged_user'];
}
?>
<header id="page-header">
  <nav>
    <div class="left">
      <a href="index.html" class="brand"><img src="<?php echo base_url(); ?>portalAssets/img/logo.png" alt=""></a>
    </div>
    <!--end left-->
    <div class="right">
      <div class="primary-nav has-mega-menu">

        <ul class="navigation">
          <li class=""><a href="#">Home</a>
          </li>
          <li><a href="blog.html">Directory</a></li>
          <li><a href="contact.html">Opportunities</a></li>
          <li><a href="contact.html">Media Blog</a></li>
          <li><a href="contact.html">Skill Development</a></li>
        </ul>
        <!--end navigation-->
      </div>
      <!--end primary-nav-->
      <div class="secondary-nav">
        <?php
        if(ISSET($portalUser) || ISSET ($portalUserFb))
        {
          if($portalUser['type']=='individual' or $portalUserFb['type']=="individual")
          {
            ?>

            <ul class="navigation">
              <li class="active has-child"><a href="#nav-homepages">
                <img src="<?php echo base_url($portalUser['image']) ?>" class="img-rounded" height="30px" width="auto" alt="">
                <img src="<?php echo base_url($portalUserFb['image']) ?>" class="img-rounded" height="30px" width="auto" alt="">
                <?php echo $portalUser['fname']; ?></a>
                 <?php echo $portalUserFb['fname']; ?></a>
                <div class="wrapper">
                  <div id="nav-homepages" class="nav-wrapper">
                    <ul>
                 <li> <a  href="#"class="visitorProfileEdit"
                 edit-data="<?php echo $portalUser["userId"]; ?>"
                  >Edit Profile</a></li>
                  <li> <a  href="#"class="visitorProfileEditPassword"
                   edit-password="<?php echo $portalUser["userId"]; ?>"
                    >Reset Password</a></li>
                      <li> <a  href="#"class="inactiveAccount"
                   inactive-account="<?php echo $portalUser["userId"]; ?>"
                    >Inactive Account</a></li>
                      <li><a href="<?php echo site_url("auth/userLogout"); ?>">Logout</a></li></li>
                    </ul>
                  </div>
                </div>
              </li>
            </ul>
            <?php
          }
          else
          {
            ?>
            <ul class="navigation">
              <li class="active has-child"><a href="#nav-homepages"><?php echo $portalUser['oname']; ?></a>
                <div class="wrapper">
                  <div id="nav-homepages" class="nav-wrapper">
                    <ul>
                      <li><a href="index-map-version-1.html">Map Full Screen Sidebar Results</a></li>
                      <li><a href="listing.php">Edit Profile</a></li>
                      <li><a href="">My Events</a></li>
                      <li><a href="">My Media Blog</a></li>
                      <li><a href="#report_problem" data-toggle="modal">Report Problem</a></li>
                      <li><a href="#reset_password" data-toggle="modal">Reset Password</a></li>
                      <li><a href="#In_active_account_modal" data-toggle="modal">Inactive Account</a></li>
                      <li><a href="<?php echo site_url("auth/userLogout"); ?>">Logout</a></li></li>
                    </ul>
                  </div>
                </div>
              </li>
            </ul>
            <?php
          }
        }
        else
        {


          ?>
          <?php echo form_open('auth/userLogin', "class='form-vertical'"); ?>
          <div class="row">
            <div class=" inputField col-md-5">
              <input type="text" class="form-control" name="email" id="emailAddress" placeholder="Email Address">
            </div>
            <div class="inputField col-md-5">
              <input type="password" class="form-control" name="password" id="password" placeholder="password">
            </div>
            <div class="inputField col-md-2">
              <button type="submit" class="btn btn-primary btn-xs">Login</button>
            </div>
          </div>
          <?php

          echo form_close();
        }
        ?>
        <div class="row">
          <div class="col-md-5">
          </div>
          <div class="col-md-5">
            <center>
              <!-- <a href="#"> Forgot your password?  </a> -->
            </center>
          </div>
        </div>
      </div>
      <!--end secondary-nav-->

      <div class="nav-btn">
        <i></i>
        <i></i>
        <i></i>
      </div>
    </div>
  </nav>
</header>
<script type="text/javascript">
    $(document).ready(function () {
        $('.date_picker').datepicker({
            format: "yyyy-mm-dd"
        });
    });
</script>
<script>
    $(document).ready(function () {
        $('[data-toggle="popover"]').popover();
    });
</script>
 <div class="modal inmodal appModal">
    <div class="modal-dialog">
        <div class="modal-content animated">
            <div class="modal-header">
                <button data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span><span
                class="sr-only">Close</span></button>
                <h4 class="modal-title"></h4>
                <hr>
                <small class="font-bold"></small>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-white" type="button">Close</button>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/security_access/bootgrid/jquery.bootgrid.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {

      $("#data-table-basic").bootgrid({
            css: {
                icon: 'md icon',
                iconColumns: 'md-view-module',
                iconDown: 'md-expand-more',
                iconRefresh: 'md-refresh',
                iconUp: 'md-expand-less'
            }
        });
    $(document).on("click", ".visitorProfileEdit", function () {
        var userId = $(this).attr("edit-data");
        //alert(userId);
        $(".appModal").modal();
        $.ajax({
            type: "POST",
            data: {userId: userId},
            url: "<?php echo site_url() ?>/portal/visitor_profile_edit_data",
            beforeSend: function () {
                $(".appModal .modal-title").html("Edit Profile");
                $(".appModal .modal-body").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
            },
            success: function (data) {
                $(".appModal .modal-body").html(data);
                if (data == 'Profile Edit successfully') {

                }
            }
        });
    });
    $(document).on("click", ".formEditProfileSubmit", function () {
        if (confirm("Are You Sure?")) {
            var USER_ID = $("#USER_ID").val();
            //alert(USER_ID);
            var fname = $("#fname").val();
            var date = $("#date").val();
            var gender = $("#gender").val();
            var number = $("#number").val();
           // var main_image = $("#main_image").val();
            var email = $("#email").val();
            $.ajax({
                type: "post",
                url: '<?php echo site_url('portal/update_visitor_profile_data') ?>',
                data: {USER_ID: USER_ID, fname: fname,date: date,gender: gender,number:number,email:email},
                success: function (data) {
                    window.setTimeout(function(){
          window.location.href = "loggedin-individual.php";
          }, 1000);
                    $(".frmMsg").html(data);
                }
            });
        } else {
            return false;
        }
    });

     $(document).on("click", ".visitorProfileEditPassword", function () {
        var userId = $(this).attr("edit-password");
        //alert(userId);
        $(".appModal").modal();
        $.ajax({
            type: "POST",
            data: {userId: userId},
            url: "<?php echo site_url() ?>/portal/visitor_profile_edit_password",
            beforeSend: function () {
                $(".appModal .modal-title").html("Password Reset Request");
                $(".appModal .modal-body").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
            },
            success: function (data) {
                $(".appModal .modal-body").html(data);
                if (data == 'Password Edit successfully') {

                }
            }
        });
    });


      $(document).on("click", ".inactiveAccount", function () {
        var userId = $(this).attr("inactive-account");
        //alert(userId);
        $(".appModal").modal();
        $.ajax({
            type: "POST",
            data: {userId: userId},
            url: "<?php echo site_url() ?>/portal/inactive_account",
            beforeSend: function () {
                $(".appModal .modal-title").html("Inactive Account");
                $(".appModal .modal-body").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
            },
            success: function (data) {
                $(".appModal .modal-body").html(data);
                if (data == 'Password Edit successfully') {

                }
            }
        });
    });



 $(document).on("click", ".formApproveSubmit", function () {
        if (confirm("Are You Sure?")) {
            var USER_ID = $("#USER_ID").val();
            //alert(USER_ID);
            var ACTIVE_FLAG = $("#ACTIVE_FLAG").val();
            var FULL_NAME = $("#FULL_NAME").val();
            var USER_MAIL = $("#USER_MAIL").val();
            $.ajax({
                type: "post",
                url: '<?php echo site_url('admin/update_visitor') ?>',
                data: {USER_ID: USER_ID, ACTIVE_FLAG: ACTIVE_FLAG,FULL_NAME: FULL_NAME,USER_MAIL: USER_MAIL},
                success: function (data) {
                    $(".frmMsg").html(data);
                 window.location.reload(true);
                }
            });
        } else {
            return false;
        }
    });
  $(".orgDltBtn").click(function () {
            if (confirm("Are you want to delete?")) {
                var org_id = $(this).attr('org_id');
                $.ajax({
                    url: '<?php echo site_url('admin/delete_review_org_from_db'); ?>',
                    type: 'POST',
                    data: {org_id: org_id},
                    success: function (data) {
                        if (data == 1) {
                            alert('Organization review deleted Successfully');
                            $('#org_row_' + org_id).remove();
                        }
                    }
                });
            } else {
                return false;
            }
        });

   $(".mailSendBtn").click(function () {
            if (confirm("Are you want to send mail?")) {
                var USER_ID = $(this).attr('USER_ID');
                var USER_MAIL =$(this).attr('USER_MAIL');
                var FULL_NAME = $(this).attr('FULL_NAME');
                $.ajax({
                    url: '<?php echo site_url('admin/again_send_mail_review'); ?>',
                    type: 'POST',
                    data: {USERID: USER_ID,FULL_NAME: FULL_NAME,USER_MAIL: USER_MAIL},
                    success: function (data) {
                        if (data == 1) {
                            alert('Mail Send Successfully');

                        }
                    }
                });
            } else {
                return false;
            }
        });
    });

</script>
