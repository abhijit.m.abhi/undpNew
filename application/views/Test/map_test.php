<div class="row new_rz_box2">
    <div class="col-md-12 col-sm-12">
        <div class="form-group">
            <label for="address-autocomplete">Full Address *</label>
            <input type="text" class="form-control" name="address" id="address-autocomplete"
                   required="required" value="<?php echo $event_info->address; ?>">
        </div>
        <!--end form-group-->
        <!--end map-->
        <div class="form-group hidden">
            <input type="text" class="form-control" id="latitude" name="latitude" hidden="" value="<?php echo $event_info->latitude; ?>">
            <input type="text" class="form-control" id="longitude" name="longitude" hidden="" value="<?php echo $event_info->longitude; ?>">
        </div>
    </div>
</div>
<div style="height: 200px; width: 100%; display: block">
    <div id="map-canvas" style="width: 100%; height: 100%"></div>
</div>


<!-- JS/CSS for FB Imo  -->
<script src="<?php echo base_url(); ?>fb_imo/jquery-2.1.4.js"></script>

<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyCgGR-bhDxioHdiX8mtFYkNliRAY6CsLao&libraries=places"></script>
<script>
    var markers = [];
    function initialize() {
        geocoder = new google.maps.Geocoder();
        var input = document.getElementById('address-autocomplete');
        var autocomplete = new google.maps.places.Autocomplete(input);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();
            $('#latitude').val(place.geometry.location.lat());
            $('#longitude').val(place.geometry.location.lng());
            codeAddress();
        });

        var mapOptions = {
            center: new google.maps.LatLng(23.778593, 90.398086),
            zoom: 15,
            styles: [{"elementType": "geometry", "stylers": [{"color": "#f5f5f5"}]}, {
                "elementType": "labels.icon",
                "stylers": [{"visibility": "off"}]
            }, {
                "elementType": "labels.text.fill",
                "stylers": [{"color": "#616161"}]
            }, {
                "elementType": "labels.text.stroke",
                "stylers": [{"color": "#f5f5f5"}]
            }, {
                "featureType": "administrative.land_parcel",
                "elementType": "labels.text.fill",
                "stylers": [{"color": "#bdbdbd"}]
            }, {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [{"color": "#eeeeee"}]
            }, {
                "featureType": "poi",
                "elementType": "labels.text.fill",
                "stylers": [{"color": "#757575"}]
            }, {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [{"color": "#e5e5e5"}]
            }, {
                "featureType": "poi.park",
                "elementType": "labels.text.fill",
                "stylers": [{"color": "#9e9e9e"}]
            }, {
                "featureType": "road",
                "elementType": "geometry",
                "stylers": [{"color": "#ffffff"}]
            }, {
                "featureType": "road.arterial",
                "elementType": "labels.text.fill",
                "stylers": [{"color": "#757575"}]
            }, {
                "featureType": "road.highway",
                "elementType": "geometry",
                "stylers": [{"color": "#dadada"}]
            }, {
                "featureType": "road.highway",
                "elementType": "labels.text.fill",
                "stylers": [{"color": "#616161"}]
            }, {
                "featureType": "road.local",
                "elementType": "labels.text.fill",
                "stylers": [{"color": "#9e9e9e"}]
            }, {
                "featureType": "transit.line",
                "elementType": "geometry",
                "stylers": [{"color": "#e5e5e5"}]
            }, {
                "featureType": "transit.station",
                "elementType": "geometry",
                "stylers": [{"color": "#eeeeee"}]
            }, {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [{"color": "#c9c9c9"}]
            }, {"featureType": "water", "elementType": "labels.text.fill", "stylers": [{"color": "#9e9e9e"}]}]
        };
        map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
        var ex_latitude = $('#latitude').val();
        var ex_longitude = $('#longitude').val();
        if (ex_latitude != '' && ex_longitude != '') {

            map.setCenter(new google.maps.LatLng(ex_latitude, ex_longitude));//center the map over the result

            var marker = new google.maps.Marker(
                {
                    map: map,
                    draggable: true,
                    //icon: "https://www.ghurbo.com/uploads/global/default/marker.png",
                    animation: google.maps.Animation.DROP,
                    position: new google.maps.LatLng(ex_latitude, ex_longitude)
                });
            markers.push(marker);
            google.maps.event.addListener(marker, 'dragend', function () {
                var marker_positions = marker.getPosition();
                $('#latitude').val(marker_positions.lat());
                $('#longitude').val(marker_positions.lng());
            });
        }
    }

    function codeAddress() {

        var address = $('#address-autocomplete').val();
        // var address = [main_address,city, state, country].join();
        if (address != '') {
            setAllMap(null); //Clears the existing marker
            geocoder.geocode({address: address}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    $('#latitude').val(results[0].geometry.location.lat());
                    $('#longitude').val(results[0].geometry.location.lng());
                    map.setCenter(results[0].geometry.location);//center the map over the result

                    //place a marker at the location
                    var marker = new google.maps.Marker(
                        {
                            map: map,
                            draggable: true,
                            animation: google.maps.Animation.DROP,
                            position: results[0].geometry.location
                        });
                    markers.push(marker);
                    google.maps.event.addListener(marker, 'dragend', function () {
                        var marker_positions = marker.getPosition();
                        $('#latitude').val(marker_positions.lat());
                        $('#longitude').val(marker_positions.lng());
                    });
                    var bounds = new google.maps.LatLngBounds();
                    for (var i = 0; i < markers.length; i++) {
                        bounds.extend(markers[i].getPosition());
                    }
                    map.fitBounds(bounds);
                } else {
                    alert('Geocode was not successful for the following reason: ' + status);
                }
            });
        }
        else {
            alert('You must enter at least Address');
        }
    }

    function setAllMap(map) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    }

    google.maps.event.addDomListener(window, 'load', initialize);
</script>