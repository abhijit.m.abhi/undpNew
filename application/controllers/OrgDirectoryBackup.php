<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class OrgDirectoryBackup extends CI_Controller
{
  function __construct() {
    /**
    * Class constructor
    *
    * Runs the auth function.
    * Create a new authentication controller instance.
    * @return  void
    */
    parent::__construct();
    $this->load->helper(array('form'));
    $this->load->library("form_validation");
    $this->load->model("auth_model");
    $this->load->model('utilities');
    $this->load->library('upload');
    $this->load->library('session');
    $this->load->library('facebook');
    $this->load->helper('url');
    $this->load->helper(array(
      'html',

      'form'
    ));
    // $this->load->model('Menu_model');
  }
  public function index()
  {
    $data['pageTitle']='Organization Directory';
    $category=$this->db->query("SELECT * FROM iao")->result();
    $data['content_view_page'] = 'portal/directoryController/index';
    $this->ptemplate->display($data);

  }
  private function pr($data)
  {
    echo "<pre>";
    print_r($data);
    exit;
  }
}
