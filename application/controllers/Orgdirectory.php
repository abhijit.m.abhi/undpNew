<?php

defined('BASEPATH') OR exit('No direct script access allowed');



/**
 * Org directory controller
 *
 * @author MD. KHAYRUL HASAN
 * @param string $message
 * @param array $context
 * @return null
 */

class Orgdirectory extends CI_Controller
{

    private $user;

    public function __construct()
    {
        parent::__construct();
        // if ($this->session->userdata('logged_in') == FALSE) {
        //     redirect('auth/login', 'refresh');
        // }
        //$this->user = $this->session->userdata("logged_in");
        $this->load->model('direction_model');
    }


    /**
     * Org directory controller
     *
     * @author MD. KHAYRUL HASAN
     * @param string $message
     * @param array $context
     * @return null
     */
    function index()
    {
        $this->directory();
    }



    /**
     * Org directory controller
     *
     * @author MD. KHAYRUL HASAN
     * @param string $message
     * @param array $context
     * @return null
     */
    function directory(){

      $this->load->view('portal/directory/orgdirectory');
    }




    /**
     * Org directory controller
     *
     * @author MD. KHAYRUL HASAN
     * @param string $message
     * @param array $context
     * @return null
     */
    function closest_locations(){

        $location =json_decode( preg_replace('/\\\"/',"\"",$_POST['data']));
        $lan=$location->longitude;
        $lat=$location->latitude;
        $ServiceId=$location->ServiceId;
        $base = base_url();
        $providers= $this->direction_model->get_closest_locations($lan,$lat,$ServiceId);
        $indexed_providers = array_map('array_values', $providers);
        // this loop will change retrieved results to add links in the info window for the provider
        $x = 0;
        foreach($indexed_providers as $arrays => &$array){
            foreach($array as $key => &$value){
                if($key === 1){
                    $pieces = explode(",", $value);
                    $value = "$pieces[1]<a href='$base$pieces[0]'>More..</a>";
                }

                $x++;
            }
        }
        echo json_encode($indexed_providers,JSON_UNESCAPED_UNICODE);
    }



}
