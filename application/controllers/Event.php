<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Event extends CI_Controller
{
    /**
     * @methodName eventCalendar
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function eventCalendar()
    {
        $userId = $this->getUserId();
        $data['permission'] = $this->Portal_model->getUserPermission(2, $userId);
        $data['eventsDate'] = $this->event_model->eventsDate();
        $data['upcomingEvents'] = $this->event_model->upcomingEvent();
        $data['previousEvent'] = $this->event_model->previousEvent();
        $data['someEvent'] = $this->event_model->upcomingEventByLimit(2);
        $data['event_title_list'] = $this->event_model->findAllEventTitleList();

        $data['content_view_page'] = 'portal/event/eventCalendar';
        $this->event_template->display($data);
    }

    public function pr($data)
    {
        echo "<pre>";
        print_r($data);
        exit;
    }

    /**
     * @methodName calSearch
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function calSearch()
    {
        $action = (!empty($_POST['action'])) ? $_POST['action'] : '';

        switch ($action) {
            case 'search':


                $result = $this->event_model->filter($_POST);

                $html = '';
                $monthText = '';
                if (!empty($_POST['search_year'])) {
                    $monthText = $_POST['search_year'];
                }

                if (!empty($_POST['search_month'])) {
                    if (empty($_POST['search_year'])) {
                        $monthText = date('Y');
                    }
                    $monthList = array('1' => 'January', '2' => 'February', '3' => 'March', '4' => 'April', '5' => 'May', '6' => 'June', '7' => 'July', '8' => 'August', '9' => 'September', '10' => 'October', '11' => 'November', '12' => 'December');
                    $monthText = $monthList[$_POST['search_month']] . ' ' . $monthText;
                }
                if (!empty($monthText)) {
                    $html = '<div class="event_calender_date"><p>EVENTS FOR ' . $monthText . '</p></div>';
                }
                if (!empty($result)) {
                    foreach ($result as $event) {
                        $base_url = base_url();
                        $site_url = site_url();

                        $images = explode('___', $event->images);
                        $image = $images[0];
                        $interest = $this->event_model->getInterestCount($event->id);
                        $going = $this->event_model->getGoingCount($event->id);
                        $seatLimit = ($event->seat_limit > 0) ? $event->seat_limit . ' Person' : 'Unlimited';
                        $event_cost = ($event->event_fee > 0) ? $event->event_fee . ' Taka' : 'Free';
                        $html .= '<a target="_blank" href=" ' . $site_url . 'event/eventView/' . $event->id . '"><div class="event_calender_content_box" style="margin-bottom: 10px;">
                                <h5><img src=" ' . $base_url . '' . $event->image . '" />' . $event->org_name . '</h5>
                                <div class="col-xs-3"><img alt="" src="' . $base_url . 'upload/events/' . $image . '"></div>
                                <div class="col-xs-9">
                                    <h6>' . $event->event_name . '</h6>
                                    <ul>
                                        <li>DATE: ' . date('d F, Y', strtotime($event->start_date)) . '-' . date('d F, Y', strtotime($event->end_date)) . '</li>
                                        <li>CATEGORY: ' . $event->category . '</li>
                                        <li>ADDRESS: ' . $event->address . '</li>
                                        <li>SEAT LIMIT: ' . $seatLimit . '</li>
                                        <li>EVENT FEE: ' . $event_cost . '</li>
                                    </ul>
                                    <div class="share_box_wrp">
                                    <span class="share_box interested_box"><img style="width: auto; height: 18px;" src="' . $base_url . 'assets/img2/running.png"
                                    > <span>' . $going->int_count . '</span></span>
                                         <span class="share_box interested_box"><img style="width: auto; height: 18px;" src="' . $base_url . 'assets/img2/star.png"
                                    > <span>' . $interest->int_count . '</span></span>

                                         <span class="likes_box"><i class="fa fa-eye"></i> <span>' . $event->view_count . '</span></span>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div></a>';
                    }
                } else {
                    $html .= '<p class="alert-danger">No Event Found</p>';
                }
                echo $html;
                exit;
                break;
            case 'cal_search':

                $date = (!empty($_POST['date'])) ? $_POST['date'] : '';
                $validDate = DateTime::createFromFormat('Y-m-d', $date);

                if ($validDate === false) {
                    echo '<p class="alert-danger">Invalid Date Format</p>';
                    exit;
                }

                $result = $this->event_model->filterByDate($date);

                $html = '<div class="event_calender_date"><p>EVENTS FOR ' . date('F', strtotime($date)) . '</p><span>' . date('d', strtotime($date)) . '</span></div>';
                if (!empty($result)) {
                    foreach ($result as $event) {
                        $base_url = base_url();
                        $eventView_url = site_url() . 'event/eventView/';

                        $images = explode('___', $event->images);
                        $image = $images[0];
                        $interest = $this->event_model->getInterestCount($event->id);
                        $going = $this->event_model->getGoingCount($event->id);
                        $seatLimit = ($event->seat_limit > 0) ? $event->seat_limit . ' Person' : 'Unlimited';
                        $event_cost = ($event->event_fee > 0) ? $event->event_fee . ' Taka' : 'Free';
                        $html .= '<a target="_blank" href="' . $eventView_url . $event->id . '"><div class="event_calender_content_box" style="margin-bottom: 10px;">
                                <h5><img src=" ' . $base_url . '' . $event->image . '" />' . $event->org_name . '</h5>
                                <div class="col-xs-3"><img alt="" src="' . $base_url . 'upload/events/' . $image . '"></div>
                                <div class="col-xs-9">
                                    <h6>' . $event->event_name . '</h6>
                                    <ul>
                                        <li>DATE: ' . date('d F, Y', strtotime($event->start_date)) . '-' . date('d F, Y', strtotime($event->end_date)) . '</li>
                                        <li>CATEGORY: ' . $event->category . '</li>
                                        <li>ADDRESS: ' . $event->address . '</li>
                                        <li>SEAT LIMIT: ' . $seatLimit . '</li>
                                        <li>EVENT FEE: ' . $event_cost . '</li>
                                    </ul>
                                    <div class="share_box_wrp">
                                         <span class="share_box interested_box"><img style="width: auto; height: 18px;" src="' . $base_url . 'assets/img2/running.png"
                                    > <span>' . $going->int_count . '</span></span>
                                         <span class="share_box interested_box"><img style="width: auto; height: 18px;" src="' . $base_url . 'assets/img2/star.png"
                                    > <span>' . $interest->int_count . '</span></span>

                                         <span class="likes_box"><i class="fa fa-eye"></i> <span>' . $event->view_count . '</span></span>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div></a>';
                    }
                } else {
                    $html .= '<p class="alert-danger">No Event Found</p>';
                }
                echo $html;
                exit;
                break;
            case 'interest':
                $user_id = $_POST['user_id'];
                $event_id = $_POST['event_id'];
                $result = $this->event_model->updateInterest($user_id, $event_id);
                $msg = '';
                if (!$result['ack']) {
                    $msg = $result['msg'];
                }

                //get the new request
                $interestCount = $this->event_model->getInterestCount($event_id);
                if (!empty($msg)) {
                    $ack = false;
                } else {
                    $ack = true;
                }
                echo json_encode(array('ack' => $ack, 'msg' => $msg, 'count' => $interestCount->int_count));
                exit;
                break;
//            case 'opportunityInterest':
//                $user_id = $_POST['user_id'];
//                $opportunity_id = $_POST['opportunity_id'];
//                $result = $opportunity->updateInterest($user_id, $opportunity_id);
//                $msg = '';
//                if(!$result['ack']){
//                    $msg = $result['msg'];
//                }
//
//                //get the new request
//                $interestCount = $opportunity->getInterestCount($opportunity_id);
//                if(!empty($msg)){
//                    $ack = false;
//                }else{
//                    $ack = true;
//                }
//                echo json_encode(array('ack' => $ack, 'msg' => $msg, 'count' => $interestCount['int_count'])); exit;
//                break;
            case 'going':

                $user_id = $_POST['user_id'];
                $event_id = $_POST['event_id'];
                $result = $this->event_model->updateGoing($user_id, $event_id);
                $msg = '';
                if (!$result['ack']) {
                    $msg = $result['msg'];
                }


                //get the new request
                $interestCount = $this->event_model->getGoingCount($event_id);
                if (!empty($msg)) {
                    $ack = false;
                } else {
                    $ack = true;
                }
                echo json_encode(array('ack' => $ack, 'msg' => $msg, 'count' => $interestCount->int_count));
                exit;
                break;
//            case 'blogInterest':
//
//                $blog_type = $_POST['blog_type'];
//                $user_id = $_POST['user_id'];
//                $blog_id = $_POST['blog_id'];
//
//                $result = $blog->updateInterest($blog_type, $user_id, $blog_id);
//                $msg = '';
//                if(!$result['ack']){
//                    $msg = $result['msg'];
//                }
//                //get the new request
//                $interestCount = $blog->getBlogInterest($blog_id);
//                if(!empty($msg)){
//                    $ack = false;
//                }else{
//                    $ack = true;
//                }
//                echo json_encode(array('ack' => $ack, 'msg' => $msg, 'count' => $interestCount, 'blog_id' => $blog_id)); exit;
//                break;
//            case 'blogImageDelete':
//                $blog_id = $_POST['blog_id'];
//                $image_name = $_POST['img_name'];
//                $result = $blog->deleteBlogImage($blog_id, $image_name);
//                $msg = $result['msg'];
//                if(!$result['ack']){
//                    $ack = false;
//                }else{
//                    $ack = true;
//                }
//                echo json_encode(array('ack' => $ack, 'msg' => $msg, 'blog_id' => $blog_id)); exit;
//                break;
            case 'eventImageDelete':

                $event_id = $_POST['event_id'];
                $image_name = $_POST['img_name'];
                $result = $this->event_model->deleteEventImage($event_id, $image_name);
                $msg = $result['msg'];
                if (!$result['ack']) {
                    $ack = false;
                } else {
                    $ack = true;
                }
                echo json_encode(array('ack' => $ack, 'msg' => $msg, 'event_id' => $event_id));
                exit;
                break;
        }

    }


    /**
     * @methodName getUserId
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */
    private function getUserId()
    {

        if (!empty($this->session->userdata['user_logged_user'])) {
            $userId = $data['user_id'] = $this->session->userdata['user_logged_user']['userId'];
        } else {
            $userId = '';
        }
        return $userId;
    }

    /**
     * @methodName eventView
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */
    function eventView()
    {
        $event_id = $this->uri->segment(3);

        if (!empty($this->session->userdata['user_logged_user'])) {
            $data['user_id'] = $this->session->userdata['user_logged_user']['userId'];
        } else {
            $data['user_id'] = "";
        }

        $this->event_model->increaseView($event_id);
        $data['eventDetails'] = $this->event_model->getEventDetailsById($event_id);
        $data['interest'] = $this->event_model->getInterestCount($event_id);
        $data['going'] = $this->event_model->getGoingCount($event_id);
        $data['organizationDetails'] = $this->event_model->getOrganizaionData($data['eventDetails']->organization_id);
        $data['relEventData'] = $this->event_model->relEvent($event_id);

        $data['content_view_page'] = 'portal/event/eventView';

//        echo "<pre>"; print_r($data['eventDetails'] ); exit;

        $this->event_template->display($data);
    }

    /**
     * @methodName fbImo
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function fbImo()
    {
        $control_id = $this->input->post('control_id');
        $value = $this->input->post('value');
        $user_id = $this->input->post('user_id');

        $imo_track = $this->input->post('imo_track');

        if ($value == 'like') {
            $imo_type = 1;
        } elseif ($value == 'haha') {
            $imo_type = 2;
        } elseif ($value == 'love') {
            $imo_type = 3;
        } elseif ($value == 'wow') {
            $imo_type = 4;
        } elseif ($value == 'sad') {
            $imo_type = 5;
        } elseif ($value == 'angry') {
            $imo_type = 6;
        }

        if ($imo_track == 'imo_track' && $value == '') {
            $result = $this->event_model->updateInterest2(1, $user_id, $control_id);

            $msg = '';
            if (!$result['ack']) {
                $msg = $result['msg'];
            }
            $interestCount = $this->event_model->getEventInterest($control_id);
            if (!empty($msg)) {
                $ack = false;
            } else {
                $ack = true;
            }
            echo json_encode(array('ack' => $ack, 'msg' => $msg, 'count' => $interestCount, 'event_id' => $control_id, 'un_chg' => 1));
        } elseif ($imo_track == 'imo_track') {
            $result = $this->event_model->deleteUpdateInterest($imo_type, $user_id, $control_id);
//            echo $result['ack']; exit;
            $msg = '';
            if (!$result['ack']) {
                $msg = $result['msg'];
            }
            $interestCount = $this->event_model->getEventInterest($control_id);

//            echo $interestCount; exit;
            if (!empty($msg)) {
                $ack = false;
            } else {
                $ack = true;
            }
            echo json_encode(array('ack' => $ack, 'msg' => $msg, 'count' => $interestCount, 'event_id' => $control_id, 'un_chg' => 2));
        } else {


            $result = $this->event_model->updateImoInterest($imo_type, $user_id, $control_id);
            $msg = '';
            if (!$result['ack']) {
                $msg = $result['msg'];
            }
            //get the new request
            $interestCount = $this->event_model->getEventInterest($control_id);
            if (!empty($msg)) {
                $ack = false;
            } else {
                $ack = true;
            }
            echo json_encode(array('ack' => $ack, 'msg' => $msg, 'count' => $interestCount, 'event_id' => $control_id));

//        echo 'Control ID: '.$control_id.' Value: '.$value.' User ID: '.$user_id;
        }

    }

    /**
     * @methodName eventList
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function eventList()
    {
        $userId = $this->getUserId();
        $m = $data['permission'] = $this->Portal_model->getUserPermission(2, $userId);
        if ($m->PER == 0) {
            redirect('event/eventCalendar');
        }

        $user_id = $this->session->userdata['user_logged_user']['userId'];

        $org_id = $this->db->query("SELECT ls.id FROM listing ls WHERE ls.userId = $user_id")->row();

        $data['upcomingEvents'] = $this->event_model->myEvents($org_id->id);
        $data['previousEvents'] = $this->event_model->previousEventByUser($org_id->id);


        $data['content_view_page'] = 'portal/event/eventList';
        $this->event_template->display($data);
    }

    /**
     * @methodName createEvent
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function createEvent()
    {
        $userId = $this->getUserId();
        $m = $this->Portal_model->getUserPermission(2, $userId);
        if ($m->PER == 0) {
            redirect('event/eventCalendar');
        }

        $data['org_name'] = $this->session->userdata['user_logged_user']['oname'];
        $data['timeRange'] = $this->event_model->timeGenerator();
        $data['cities'] = $this->event_model->allCity();

        $userId = $this->session->userdata['user_logged_user']['userId'];
        $org_id = $this->db->select('id')->get_where('listing', array('userId' => $userId))->row();

        $data['organizationDetails'] = $this->event_model->getOrganizaionData($org_id->id);

        $data['content_view_page'] = 'portal/event/createEvent';
        $this->event_template->display($data);
    }

    /**
     * @methodName viewModal
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function viewEventModal()
    {

        $event_id = $this->uri->segment(3);

        if (!empty($this->session->userdata['user_logged_user'])) {
            $data['user_id'] = $this->session->userdata['user_logged_user']['userId'];
        } else {
            $data['user_id'] = "";
        }

        $org_id = $this->db->select('organization_id')->get_where('events', array('id' => $event_id))->row();
//        echo $org_id->organization_id; exit;
        $data['eventDetails'] = $this->event_model->getEventDetailsById($event_id);
        $data['interest'] = $this->event_model->getInterestCount($event_id);
        $data['going'] = $this->event_model->getGoingCount($event_id);
        $this->event_model->increaseOrgView($data['eventDetails']->organization_id);
        $data['organizationDetails'] = $this->event_model->getOrganizaionData($data['eventDetails']->organization_id);
        $data['eventCount'] = $this->event_model->getEventsCountByOrgID($data['eventDetails']->organization_id);
        $data['blogCount'] = $this->event_model->getBlogsCountByOrgID($data['eventDetails']->organization_id);
        $data['allImagesData'] = $this->event_model->getAllImagesData($data['eventDetails']->organization_id);
        $data['relBlogData'] = $this->event_model->relBlogByThisOrg($org_id->organization_id);
        $data['relEventData'] = $this->event_model->relEventByThisOrg($org_id->organization_id);

//        echo "<pre>"; print_r($data['allImagesData']); exit;

        $data['content_view_page'] = 'portal/event/modalEventView';
        $this->event_template->display($data);
    }

    /**
     * @methodName createNewEvent
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function createNewEvent()
    {
        $user_id = $this->session->userdata['user_logged_user']['userId'];

        $org_id = $this->db->query("SELECT ls.id FROM listing ls WHERE ls.userId = $user_id")->row();

        $event_name = $this->input->post('event_name');
        $event_category = $this->input->post('event_category');
        $event_description = $this->input->post('event_description');
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');
        if (empty($end_date)) {
            $end_date = $start_date;
        }
        $start_time = $this->input->post('start_time');
        $end_time = $this->input->post('end_time');
        $seat_limit = $this->input->post('seat_limit');
        $event_cost = $this->input->post('event_cost');
        $district = $this->input->post('district');
        $address = $this->input->post('address');
        $latitude = $this->input->post('latitude');
        $longitude = $this->input->post('longitude');
        $registration_link = $this->input->post('registration_link');
        $mobile = $this->input->post('mobile');
        $phone = $this->input->post('phone');
        $website = $this->input->post('website');
        $facebook = $this->input->post('facebook');
        $twitter = $this->input->post('twitter');


        $FileUploader = new FileUploader('files', array(
            'limit' => 5,
            'maxSize' => 10,
            'fileMaxSize' => 2,
            'extensions' => ['jpg', 'jpeg', 'png', 'gif', 'bmp', 'JPG', 'JPEG'],
            'required' => true,
            'uploadDir' => 'upload/events/',
            'title' => '{timestamp}--{file_name}',
            'replace' => false,
            'listInput' => true,
            'files' => null
        ));

        // call to upload the files
        $data = $FileUploader->upload();

        // if uploaded and success
        if ($data['isSuccess'] && count($data['files']) > 0) {
            // get uploaded files
            $uploadedFiles = $data['files'];
            $images = '';
            $first = true;
            foreach ($uploadedFiles as $uploadedFile) {
                if ($first == false) {
                    $images .= '___';
                }
                $images .= $uploadedFile['name'];
                $first = false;
            }
        }

        $event_info = array(
            'images' => $images,
            'organization_id' => $org_id->id,
            'event_name' => $event_name,
            'category' => $event_category,
            'description' => $event_description,
            'start_date' => date('Y-m-d', strtotime($start_date)),
            'end_date' => date('Y-m-d', strtotime($end_date)),
            'start_time' => $start_time,
            'end_time' => $end_time,
            'seat_limit' => $seat_limit,
            'event_fee' => $event_cost,
            'district' => $district,
            'address' => $address,
            'latitude' => $latitude,
            'longitude' => $longitude,
            'registration_link' => $registration_link,
            'mobile' => $mobile,
            'phone' => $phone,
            'website' => $website,
            'facebook' => $facebook,
            'twitter' => $facebook,

        );


        $this->db->insert('events', $event_info);
        $this->session->set_flashdata('success', 'Successfully Created.');
        redirect('event/eventList');

    }


    /**
     * @methodName createNewEvent
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function deleteEvent()
    {
        $event_id = $this->uri->segment(3);

        $result = $this->event_model->deleteEvent($event_id);

        if ($result == "Deleted") {
            redirect('event/eventList', 'refresh');
        }

    }

    /**
     * @methodName editEvent
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function editEvent()
    {
        $userId = $this->getUserId();
        $m = $this->Portal_model->getUserPermission(2, $userId);
        if ($m->PER == 0) {
            redirect('event/eventCalendar');
        }
        $event_id = $this->uri->segment(3);

        $data['timeRange'] = $this->event_model->timeGenerator();
        $data['cities'] = $this->event_model->allCity();
        $data['event_info'] = $this->event_model->getEventInfoByThisId($event_id);
        $data['organizationDetails'] = $this->event_model->getOrganizaionData($data['event_info']->organization_id);

        $data['content_view_page'] = 'portal/event/editEvent';
        $this->event_template->display($data);
    }

    /**
     * @methodName insertEditedEvent
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function insertEditedEvent()
    {
        $event_id = $this->uri->segment(3);

        $org_id = $this->input->post('org_id');
        $event_name = $this->input->post('event_name');
        $event_category = $this->input->post('event_category');
        $event_description = $this->input->post('event_description');
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');
        $start_time = $this->input->post('start_time');
        $end_time = $this->input->post('end_time');
        $seat_limit = $this->input->post('seat_limit');
        $event_cost = $this->input->post('event_cost');
        $district = $this->input->post('district');
        $address = $this->input->post('address');
        $latitude = $this->input->post('latitude');
        $longitude = $this->input->post('longitude');
        $registration_link = $this->input->post('registration_link');
        $img = $this->input->post('img');
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $phone = $this->input->post('phone');
        $website = $this->input->post('website');
        $facebook = $this->input->post('facebook');
        $twitter = $this->input->post('twitter');


        $hasFile = $this->input->post('fileuploader-list-files');
        $ext_image = $this->input->post('ext_image');

        if (!empty($hasFile)) {

            $FileUploader = new FileUploader('files', array(
                'limit' => 5,
                'maxSize' => 10,
                'fileMaxSize' => 2,
                'extensions' => ['jpg', 'jpeg', 'png', 'gif', 'bmp', 'JPG', 'JPEG'],
                'required' => true,
                'uploadDir' => 'upload/events/',
                'title' => '{timestamp}--{file_name}',
                'replace' => false,
                'listInput' => true,
                'files' => null
            ));

            // call to upload the files
            $data = $FileUploader->upload();

            // if uploaded and success
            if ($data['isSuccess'] && count($data['files']) > 0) {
                // get uploaded files
                $uploadedFiles = $data['files'];
                $images = '';
                $first = true;
                foreach ($uploadedFiles as $uploadedFile) {
                    if ($first == false) {
                        $images .= '___';
                    }
                    $images .= $uploadedFile['name'];
                    $first = false;
                }
            }

            if ($ext_image != '' || $ext_image != null) {

                $event_info = array(
                    'images' => $ext_image . '___' . $images,
                    'organization_id' => $org_id,
                    'event_name' => $event_name,
                    'category' => $event_category,
                    'description' => $event_description,
                    'start_date' => date('Y-m-d', strtotime($start_date)),
                    'end_date' => date('Y-m-d', strtotime($end_date)),
                    'start_time' => $start_time,
                    'end_time' => $end_time,
                    'seat_limit' => $seat_limit,
                    'event_fee' => $event_cost,
                    'district' => $district,
                    'address' => $address,
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'registration_link' => $registration_link,
                    'email' => $email,
                    'mobile' => $mobile,
                    'phone' => $phone,
                    'website' => $website,
                    'facebook' => $facebook,
                    'twitter' => $twitter,
                );

            } else {


                $event_info = array(
                    'images' => $images,
                    'organization_id' => $org_id,
                    'event_name' => $event_name,
                    'category' => $event_category,
                    'description' => $event_description,
                    'start_date' => date('Y-m-d', strtotime($start_date)),
                    'end_date' => date('Y-m-d', strtotime($end_date)),
                    'start_time' => $start_time,
                    'end_time' => $end_time,
                    'seat_limit' => $seat_limit,
                    'event_fee' => $event_cost,
                    'district' => $district,
                    'address' => $address,
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'registration_link' => $registration_link,
                    'email' => $email,
                    'mobile' => $mobile,
                    'phone' => $phone,
                    'website' => $website,
                    'facebook' => $facebook,
                    'twitter' => $twitter,
                );
            }

        } else {
            $event_info = array(
                'organization_id' => $org_id,
                'event_name' => $event_name,
                'category' => $event_category,
                'description' => $event_description,
                'start_date' => date('Y-m-d', strtotime($start_date)),
                'end_date' => date('Y-m-d', strtotime($end_date)),
                'start_time' => $start_time,
                'end_time' => $end_time,
                'seat_limit' => $seat_limit,
                'event_fee' => $event_cost,
                'district' => $district,
                'address' => $address,
                'latitude' => $latitude,
                'longitude' => $longitude,
                'registration_link' => $registration_link,
                'email' => $email,
                'mobile' => $mobile,
                'phone' => $phone,
                'website' => $website,
                'facebook' => $facebook,
                'twitter' => $twitter,
            );
        }

        $this->db->update('events', $event_info, array('id' => $event_id));
        $this->session->set_flashdata('success', 'Successfully Updated.');
        redirect('event/eventList');

    }


    /**
     * @methodName report_problem
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    public function report_problem($event_id,$id,$url)
    {
        if (!empty($id)) {
            $data['user_detail'] = $this->db->query("select u.* from users u
                                                  WHERE u.userId= $id order by u.userId")->row();
        }
         $data['url']=$url;
        $data['link'] = site_url() . 'event/eventView/' . $event_id;

        $this->load->view('portal/event/report_problem', $data);

    }

    /**
     * @methodName reportProblemFormInsert
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    public function reportProblemFormInsert()
    {
    if($_POST)
        {
            $redirectUrl=$this->input->post('redirectUrl');
            $redirect=str_replace("--","/",$redirectUrl);
            $name = $this->input->post('name');
            $email = $this->input->post('email');
            $subject = $this->input->post('subject');
            $message = $this->input->post('message');
            $link = $this->input->post('link');

            if (!empty($name) || !empty($email) || !empty($subject) || !empty($message)) {
               /* echo "<div class='alert alert-danger'>
                Required field can not be empty<button data-dismiss='alert' style='
                float: right;background-color: white;color: black;' class='' type='button'>×</button>
                </div>";*/
          /*  } else {*/
                $body = "Email From: " . $name . "<br>" . $message . "<br>" . $link;

                $to = $email;
                require 'gmail_app/class.phpmailer.php';
                $mail = new PHPMailer;
                $mail->IsSMTP();
                $mail->Host = "mail.harnest.com";
                $mail->Port = "465";
                $mail->SMTPAuth = true;
                $mail->Username = "dev@atilimited.net";
                $mail->Password = "@ti321$#";
                $mail->SMTPSecure = 'ssl';
                $mail->From = "noreply@youthopiabangla.org";
                $mail->FromName = "Youthopia.bangla";
                $mail->AddAddress($to);
                $mail->IsHTML(TRUE);
                $mail->Subject = $subject;
                $mail->Body = $body;
                if ($mail->Send()) {
                     $this->session->set_flashdata('success',' Your message has been received.We will get back to you as soon as possible.');
                }

            }
             else{
          $this->session->set_flashdata('Error','You are not successfully message send!.');
        }
       redirect($redirect);

        }
    }
}


