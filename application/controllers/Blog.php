<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller
{

     private function getUserId()
     {

       if (!empty($this->session->userdata['user_logged_user'])) {
           $userId=$data['user_id'] = $this->session->userdata['user_logged_user']['userId'];
       } else {
          $userId='';
       }
       return $userId;
     }

    /**
     * @methodName mediaBlogs
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function mediaBlogs()
    {
      $userId=$this->getUserId();
      $data['permission']=$this->Portal_model->getUserPermission(3,$userId);
      $result_per_page = 9;

        $config['base_url'] = site_url() . '/blog/mediaBlogs/';
        $config['total_rows'] = $this->blog_model->count_items();
        $config['per_page'] = $result_per_page;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
        $this->pagination->initialize($config);

        $offset = (!empty($this->uri->segment(3))) ? $this->uri->segment(3) : 0;

        $data['blog_posts'] = $this->blog_model->getAllBlogPosts($result_per_page, $offset);
        $data['blog_title_list'] = $this->blog_model->findAllBlogTitleList();

        $data['content_view_page'] = 'portal/blog/media_blogs';
        $this->event_template->display($data);
    }

    /**
     * @methodName searchBlogPost
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function searchBlogPost()
    {
        $blog_type = $this->input->post('blog_type');
        $order = $this->input->post('order');
        $search = $this->input->post('search');

        $data['blog_ty'] = $blog_type;
        $data['or'] = $order;
        $data['src'] = $search;

        $result_per_page = 9;

        $data['blog_posts'] = $this->blog_model->searchPost($blog_type, $order, $search, $result_per_page);

        $config['base_url'] = site_url() . '/blog/paginateBlogposts/';
        $config['total_rows'] = $this->session->userdata["blog_row_count"]["num_rows"];
        $config['per_page'] = $result_per_page;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
        $this->pagination->initialize($config);

        $this->session->set_userdata('blog_pagination', array(
            'category' => $blog_type,
            'order' => $order,
            'search' => $search,
        ));

        $data['blog_title_list'] = $this->blog_model->findAllBlogTitleList();

//        echo "<pre>"; print_r($data['blog_title_list'] ); exit;

        $data['content_view_page'] = 'portal/blog/search_media_blogs';
        $this->event_template->display($data);
    }

    /**
     * @methodName paginateBlogposts
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function paginateBlogposts()
    {
        $blog_type = $this->session->userdata["blog_pagination"]["category"];
        $order = $this->session->userdata["blog_pagination"]["order"];
        $search = $this->session->userdata["blog_pagination"]["search"];
        $num_rows = $this->session->userdata["blog_row_count"]["num_rows"];

        $data['blog_ty'] = $blog_type;
        $data['or'] = $order;
        $data['src'] = $search;

        $offset = (!empty($this->uri->segment(3))) ? $this->uri->segment(3) : 0;

        $result_per_page = 9;

        $data['blog_posts'] = $this->blog_model->searchPost($blog_type, $order, $search, $result_per_page, $offset);

        $config['base_url'] = site_url() . '/blog/paginateBlogposts/';
        $config['total_rows'] = $num_rows;
        $config['per_page'] = $result_per_page;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
        $this->pagination->initialize($config);

        $data['blog_title_list'] = $this->blog_model->findAllBlogTitleList();

        $data['content_view_page'] = 'portal/blog/search_media_blogs';
        $this->event_template->display($data);
    }


    /**
     * @methodName blogView
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function blogView()
    {
        $blog_id = $this->uri->segment(3);
        $this->blog_model->increaseView($blog_id);
        $data['blogDetails'] = $this->blog_model->getBlogData($blog_id);
        $data['organizationDetails'] = $this->blog_model->getOrganizaionData($data['blogDetails']->organization_id);
        $data['organizationLogo'] = $this->blog_model->organizationLogo($data['organizationDetails']->userId);
        $data['blogComments'] = $this->blog_model->getBlogComment($blog_id);

        $data['content_view_page'] = 'portal/blog/blog_view';
        $this->event_template->display($data);
    }

    /**
     * @methodName calSearch
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function calSearch()
    {
        $action = (!empty($_POST['action'])) ? $_POST['action'] : '';

        switch ($action) {

            case 'blogInterest':

                $blog_type = $_POST['blog_type'];
                $user_id = $_POST['user_id'];
                $blog_id = $_POST['blog_id'];

                $result = $this->blog_model->updateInterest($blog_type, $user_id, $blog_id);
                $msg = '';
                if (!$result['ack']) {
                    $msg = $result['msg'];
                }
                //get the new request
                $interestCount = $this->blog_model->getBlogInterest($blog_id);
                if (!empty($msg)) {
                    $ack = false;
                } else {
                    $ack = true;
                }
                echo json_encode(array('ack' => $ack, 'msg' => $msg, 'count' => $interestCount, 'blog_id' => $blog_id));
                exit;
                break;

            case 'blogImageDelete':
                $blog_id = $_POST['blog_id'];
                $image_name = $_POST['img_name'];
                $result = $this->blog_model->deleteBlogImage($blog_id, $image_name);
                $msg = $result['msg'];
                if (!$result['ack']) {
                    $ack = false;
                } else {
                    $ack = true;
                }
                echo json_encode(array('ack' => $ack, 'msg' => $msg, 'blog_id' => $blog_id));
                exit;
                break;
        }

    }

    /**
     * @methodName blogList
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function blogList()
    {
      $userId=$this->getUserId();
      $m=$data['permission']=$this->Portal_model->getUserPermission(3,$userId);
      if($m->PER==0)
      {
        redirect('blog/mediaBlogs');
      }

        $user_id = $this->session->userdata['user_logged_user']['userId'];

        $data['blogs'] = $this->blog_model->getBlog($user_id);

        $data['content_view_page'] = 'portal/blog/blog_list';
        $this->event_template->display($data);
    }

    /**
     * @methodName createBlog
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function createBlog()
    {
      $userId=$this->getUserId();
      $m=$data['permission']=$this->Portal_model->getUserPermission(3,$userId);
      if($m->PER==0)
      {
        redirect('blog/mediaBlogs');
      }
        $user_id = $this->session->userdata['user_logged_user']['userId'];

        $data['organizationData'] = $this->blog_model->getOrganization($user_id);
        $data['url_types'] = $this->blog_model->urlTypes();

//        echo "<pre>"; print_r($data['url_types']); exit;

        $data['content_view_page'] = 'portal/blog/create_blog';
        $this->event_template->display($data);
    }

    /**
     * @methodName createNewBlogPost
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function createNewBlogPost()
    {

        $user_id = $this->input->post('user_id');
        $org_id = $this->db->query("SELECT ls.id FROM listing ls WHERE ls.userId = $user_id")->row();


        $title = $this->input->post('title');
        $type = $this->input->post('blog_type');
        $audio_url_type = $this->input->post('audio_url_type');
        $audioUrl = $this->input->post('audio_url');
        $video_url_type = $this->input->post('video_url_type');
        $videoUrl = $this->input->post('video_url');
        $post_date = $this->input->post('post_date');
        $description = $this->input->post('description');
        $urlType = '';
        $urls = '';
        $images = '';

        if ($type == 'audio') {
            $urlType = 2;
            $urls = $audioUrl;
        } elseif ($type == 'video') {
            $urlType = $video_url_type;
            $urls = $videoUrl;
        } elseif ($type == 'image') {
            $urlType = null;
            // initialize FileUploader
            $FileUploader = new FileUploader('files', array(
                'limit' => 5,
                'maxSize' => 10,
                'fileMaxSize' => 2,
                'extensions' => ['jpg', 'jpeg', 'png', 'gif', 'bmp', 'JPG', 'JPEG'],
                'required' => true,
                'uploadDir' => 'upload/blogs/',
                'title' => '{timestamp}--{file_name}',
                'replace' => false,
                'listInput' => true,
                'files' => null
            ));

            // call to upload the files
            $data = $FileUploader->upload();

            // if uploaded and success
            if ($data['isSuccess'] && count($data['files']) > 0) {
                // get uploaded files
                $uploadedFiles = $data['files'];
                $images = '';
                $first = true;
                foreach ($uploadedFiles as $uploadedFile) {
                    if ($first == false) {
                        $images .= '___';
                    }
                    $images .= $uploadedFile['name'];
                    $first = false;
                }
            }


        }

        $newPostData = array(

            'organization_id' => $org_id->id,
            'title' => $title,
            'type' => $type,
            'urls' => $urls,
            'url_type_id' => $urlType,
            'post_date' => date('Y-m-d', strtotime($post_date)),
            'images' => $images,
            'description' => htmlentities($description),

        );


//        echo "<pre>"; print_r($newPostData); exit;

        $this->db->insert('blog', $newPostData);
        $this->session->set_flashdata('success', 'Successfully Created.');
        redirect('blog/blogList', 'refresh');
    }


    /**
     * @methodName deleteBlogPost
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function deleteBlogPost()
    {
        $blog_id = $this->uri->segment(3);

        $result = $this->blog_model->deletePost($blog_id);

        if ($result == "Deleted") {
            redirect('blog/blogList', 'refresh');
        }
    }

    /**
     * @methodName editBlogPost
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function editBlogPost()
    {
        $blog_id = $this->uri->segment(3);

        $user_id = $this->session->userdata['user_logged_user']['userId'];
        $data['organizationData'] = $this->blog_model->getOrganization($user_id);
        $data['blogData'] = $this->blog_model->getBlogData($blog_id);
        $data['url_types'] = $this->blog_model->urlTypes();

        $data['content_view_page'] = 'portal/blog/edit_blog';
        $this->event_template->display($data);

    }

    /**
     * @methodName insertEditedBlogPost
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function insertEditedBlogPost()
    {
        $blog_id = $this->uri->segment(3);

        $org_id = $this->input->post('org_id');
        $title = $this->input->post('title');
        $type = $this->input->post('blog_type');
        $audio_url_type = $this->input->post('audio_url_type');
        $audioUrl = $this->input->post('audio_url');
        $video_url_type = $this->input->post('video_url_type');
        $videoUrl = $this->input->post('video_url');
        $post_d = $this->input->post('post_date');
        $post_date = date('Y-m-d', strtotime($post_d));

        $description = htmlentities($this->input->post('description'));

        $hasFile = $this->input->post('fileuploader-list-files');
        $ext_image = $this->input->post('ext_image');

        $urlType = '';
        $urls = '';

        if ($type == 'audio') {
            $urlType = 2;
            $urls = $audioUrl;
        } elseif ($type == 'video') {
            $urlType = $video_url_type;
            $urls = $videoUrl;
        } elseif ($type == 'image') {
            $urlType = null;
            // initialize FileUploader

            if (!empty($hasFile)) {

                $FileUploader = new FileUploader('files', array(
                    'limit' => 5,
                    'maxSize' => 10,
                    'fileMaxSize' => 2,
                    'extensions' => ['jpg', 'jpeg', 'png', 'gif', 'bmp', 'JPG', 'JPEG' ],
                    'required' => true,
                    'uploadDir' => 'upload/blogs/',
                    'title' => '{timestamp}--{file_name}',
                    'replace' => false,
                    'listInput' => true,
                    'files' => null
                ));

                // call to upload the files
                $data = $FileUploader->upload();

                // if uploaded and success
                if ($data['isSuccess'] && count($data['files']) > 0) {
                    // get uploaded files
                    $uploadedFiles = $data['files'];
                    $images = '';
                    $first = true;
                    foreach ($uploadedFiles as $uploadedFile) {
                        if ($first == false) {
                            $images .= '___';
                        }
                        $images .= $uploadedFile['name'];
                        $first = false;
                    }
                }

                if ($ext_image != '' || $ext_image != null) {

                    $newPostData = array(
                        'title' => $title,
                        'type' => $type,
                        'urls' => $urls,
                        'url_type_id' => $urlType,
                        'post_date' => $post_date,
                        'images' => $ext_image . '___' . $images,
                        'description' => htmlentities($description),

                    );
                } else {
                    $newPostData = array(

                        'title' => $title,
                        'type' => $type,
                        'urls' => $urls,
                        'url_type_id' => $urlType,
                        'post_date' => $post_date,
                        'images' => $images,
                        'description' => htmlentities($description),

                    );
                }

                $this->db->update('blog', $newPostData, array('id' => $blog_id));
                $this->session->set_flashdata('success', 'Successfully Updated.');
                redirect('blog/blogList');


            } else {

                $newPostData = array(
                    'title' => $title,
                    'type' => $type,
                    'urls' => $urls,
                    'url_type_id' => $urlType,
                    'post_date' => $post_date,
                    'description' => $description,

                );


                $this->db->update('blog', $newPostData, array('id' => $blog_id));
                $this->session->set_flashdata('success', 'Successfully Updated.');
                redirect('blog/blogList');
            }

        }


        //unlink image
        if(!empty($ext_image))
        {
            $arr = explode('___', $ext_image);
            $imgPath = 'upload/blogs/';

            for($i=0; $i<count($arr); $i++)
            {
                unlink($imgPath . $arr[$i]);
            }
        }

        $newPostData = array(
            'title' => $title,
            'type' => $type,
            'urls' => $urls,
            'url_type_id' => $urlType,
            'post_date' => $post_date,
            'description' => $description,
            'images' => ''

        );

        $this->db->update('blog', $newPostData, array('id' => $blog_id));
        $this->session->set_flashdata('success', 'Successfully Updated.');
        redirect('blog/blogList');

    }

    /**
     * @methodName submitComment
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function submitComment()
    {
        $blog_id = $this->uri->segment(3);

        $user_id = $this->session->userdata['user_logged_user']['userId'];
        $description = $this->input->post('description');

        $result = $this->blog_model->addComment($description, $blog_id, $user_id);

        if ($result == "Inserted") {
            redirect('blog/blogView/' . $blog_id, 'refresh');
        }
    }

    /**
     * @methodName editBlogPost
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function deleteComment()
    {
        $blog_id = $this->uri->segment(3);
        $comment_id = $this->uri->segment(4);
        $user_id = $this->session->userdata['user_logged_user']['userId'];

        $result = $this->blog_model->deleteComment($comment_id, $user_id);

        if ($result == 'Deleted') {
            redirect('blog/blogView/' . $blog_id, 'refresh');
        }
    }


    /**
     * @methodName fbImo
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function fbImo()
    {
        $control_id = $this->input->post('control_id');
        $value = $this->input->post('value');
        $user_id = $this->input->post('user_id');

        $imo_track = $this->input->post('imo_track');

        if ($value == 'like') {
            $blog_type = 1;
        } elseif ($value == 'haha') {
            $blog_type = 2;
        } elseif ($value == 'love') {
            $blog_type = 3;
        } elseif ($value == 'wow') {
            $blog_type = 4;
        } elseif ($value == 'sad') {
            $blog_type = 5;
        } elseif ($value == 'angry') {
            $blog_type = 6;
        }

        if ($imo_track == 'imo_track' && $value == '') {
            $result = $this->blog_model->updateInterest(1, $user_id, $control_id);

            $msg = '';
            if (!$result['ack']) {
                $msg = $result['msg'];
            }
            $interestCount = $this->blog_model->getBlogInterest($control_id);
            if (!empty($msg)) {
                $ack = false;
            } else {
                $ack = true;
            }
            echo json_encode(array('ack' => $ack, 'msg' => $msg, 'count' => $interestCount, 'blog_id' => $control_id, 'un_chg' => 1));
        } elseif ($imo_track == 'imo_track') {
            $result = $this->blog_model->deleteUpdateInterest($blog_type, $user_id, $control_id);
//            echo $result['ack']; exit;
            $msg = '';
            if (!$result['ack']) {
                $msg = $result['msg'];
            }
            $interestCount = $this->blog_model->getBlogInterest($control_id);

//            echo $interestCount; exit;
            if (!empty($msg)) {
                $ack = false;
            } else {
                $ack = true;
            }
            echo json_encode(array('ack' => $ack, 'msg' => $msg, 'count' => $interestCount, 'blog_id' => $control_id, 'un_chg' => 2));
        } else {
            $result = $this->blog_model->updateInterest($blog_type, $user_id, $control_id);
            $msg = '';
            if (!$result['ack']) {
                $msg = $result['msg'];
            }
            //get the new request
            $interestCount = $this->blog_model->getBlogInterest($control_id);
            if (!empty($msg)) {
                $ack = false;
            } else {
                $ack = true;
            }
            echo json_encode(array('ack' => $ack, 'msg' => $msg, 'count' => $interestCount, 'blog_id' => $control_id));

//        echo 'Control ID: '.$control_id.' Value: '.$value.' User ID: '.$user_id;
        }

    }


    /**
     * @methodName report_problem
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    public function report_problem($blog_id, $id,$url)
    {
        if (!empty($id)) {
            $data['user_detail'] = $this->db->query("select u.* from users u
                                                  WHERE u.userId= $id order by u.userId")->row();
        }
         $data['url']=$url;

        $data['link'] = site_url() . 'blog/blogView/' . $blog_id;

        $this->load->view('portal/blog/report_problem', $data);

    }

    /**
     * @methodName reportProblemFormInsert
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    public function reportProblemFormInsert()
    {
       if($_POST)
        {
            $redirectUrl=$this->input->post('redirectUrl');
            $redirect=str_replace("--","/",$redirectUrl);
            $name = $this->input->post('name');
            $email = $this->input->post('email');
            $subject = $this->input->post('subject');
            $message = $this->input->post('message');
            $link = $this->input->post('link');

            if (!empty($name) || !empty($email) || !empty($subject) || !empty($message)) {

                $body = "Email From: ".$name."<br>".$message . "<br>" . $link;

                $to = $email;
                require 'gmail_app/class.phpmailer.php';
                $mail = new PHPMailer;
                $mail->IsSMTP();
                $mail->Host = "mail.harnest.com";
                $mail->Port = "465";
                $mail->SMTPAuth = true;
                $mail->Username = "dev@atilimited.net";
                $mail->Password = "@ti321$#";
                $mail->SMTPSecure = 'ssl';
                $mail->From = "noreply@youthopiabangla.org";
                $mail->FromName = "Youthopia.bangla";
                $mail->AddAddress($to);
                $mail->IsHTML(TRUE);
                $mail->Subject = $subject;
                $mail->Body = $body;
                 if ($mail->Send()) {
                     $this->session->set_flashdata('success',' Your message has been received.We will get back to you as soon as possible.');
                }

            }
           else{
        $this->session->set_flashdata('Error','You are not successfully message send!.');
        }
       redirect($redirect);

        }
    }



    /**
     * @methodName viewInfo
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function viewInfo($u_id)
    {
        $user_id = $u_id;

        $org_id = $this->db->select('id')->get_where('listing', array('userId' => $u_id))->row();

        $this->event_model->increaseOrgView($org_id->id);
        $data['organizationDetails'] = $this->event_model->getOrganizaionData($org_id->id);
        $data['eventCount'] = $this->event_model->getEventsCountByOrgID($org_id->id);
        $data['blogCount'] = $this->event_model->getBlogsCountByOrgID($org_id->id);
//        $data['allImagesData'] = $this->event_model->getAllImagesData($data['eventDetails']->organization_id);
        $data['relBlogData'] = $this->event_model->relBlogByThisOrg($org_id->id);
        $data['relEventData'] = $this->event_model->relEventByThisOrg($org_id->id);

        $data['content_view_page'] = 'portal/blog/userInfo';
        $this->event_template->display($data);
    }


}
