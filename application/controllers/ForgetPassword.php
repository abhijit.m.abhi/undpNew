<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class ForgetPassword extends CI_Controller
{
 function __construct() {
  parent::__construct();
  $this->load->model('utilities');
  $this->load->helper(array('form'));
  $this->load->library('form_validation');
  $this->load->library('session');
      //$this->load->library('upload');
  $this->load->helper('url');
}
    /**
     * @methodName forgetPassword
     * @access
     * @param  none
     * @author Md.Reazul Islam <reazul@atilimited.net>
     * @return
     */

    public function forgot_password() {
     $data['content_view_page'] = 'portal/forgetPassword/forgot_password_view';
     $this->event_template->display($data);

   }

   public function checkUserVaildEmail() {
    $EMAIL = $this->input->post("EMAIL");
    $check_userEmail =  $this->db->query("SELECT email FROM users WHERE  email= '$EMAIL' OR oemail = '$EMAIL'")->row();
    if (empty($check_userEmail)) {
      echo "emailNotExit";
    } else {
      echo "emailExit";
    }
  }

  public function forgetPassword() {
    $this->load->helper('string');
    $username_email = $this->input->post('username_email');
    $number = $result = $this->auth_model->getUsersData($username_email);
     //echo "<pre>";print_r($number);exit();
    if (!empty($number)) {
      echo '<div style="color:green;">We sent a Reset Link To your Email Address.</div>';
      $user_id = $number->userId;
      $useremail = $number->email;
      $useroemail = $number->oemail;
      $visitorEmail = ( $username_email == $useremail) ? $useremail : $useroemail;
     // echo "<pre>";print_r($visitorEmail);exit();
      $userFname = $number->fname;
      $userOname = $number->oname;
        $visitorName = ( $username_email == $useremail) ? $userFname : $userOname;
         //echo "<pre>";print_r($visitorName);exit();
        //$userLname = $number->LAST_NAME;
      $random_id = random_string('alnum', 25);
      $data = array(
        'USER_ID' => $user_id,
        'REQUESTED_CODE' => $random_id
        );
      $this->utilities->insertData($data, 'sa_forget_pass_request');
      $message ="<html><head></html><body>Dear<br> $visitorName,<br><br>
      You're one step away from regaining access to your Youthopia.bangla account. 
      Just click below to reset your password:<br/>
      <a style='margin-top:10px; color:white;' href='" . site_url() . "ForgetPassword/generatePassword/$random_id'><button class='btn btn-primary' style='margin-left:24%; background-color:#556a2f; padding:5px;'>Reset Password</button></a> <br/>
      <br/>Thanks<br>
      Youthopia.bangla<br/><br/>        
    </body></html>";

    $subject = "Youthopia.bangla : Password Reset Request";

          //echo $message;exit;
    if ($useremail == $username_email or $useroemail == $username_email) {
      require 'gmail_app/class.phpmailer.php';
      $mail = new PHPMailer;
      $mail->IsSMTP();
      $mail->Host = "mail.harnest.com";
      $mail->Port = "465";
      $mail->SMTPAuth = true;
      $mail->Username = "dev@atilimited.net";
      $mail->Password = "@ti321$#";
      $mail->SMTPSecure = 'ssl';
      $mail->From = "noreply@youthopiabangla.org";
      $mail->FromName = "Youthopia.bangla";
      $mail->AddAddress($visitorEmail);
          //$mail->AddReplyTo($emp_info->EMPLOYEE);
      $mail->IsHTML(TRUE);
      $mail->Subject = $subject;
      $mail->Body = $message;
      if ($mail->Send()){
        $this->session->set_flashdata('flashMessage', 'Mail send successfully please check mail.');
        redirect('portal/index', 'refresh');
      }
    } else {
      echo '<p style="color:red;">Please enter valid Email.</p>';
    }
  } else {
    echo '<div style="color:red;">Sorry, Your Address are not registered.</div>';
  }
}


public function generatePassword() {
  $random_code = $this->uri->segment(3, '');
  if ($random_code == '') {
    $this->session->set_flashdata('Error', 'Sorry ! You are Trying Invalid Way to Reset Password.');
    redirect('portal/index', 'refresh');
  }
  $data['requestinfo'] = $this->utilities->findByAttribute('sa_forget_pass_request', array('REQUESTED_CODE' => $random_code));
  if (empty($data['requestinfo'])) {
    $this->session->set_flashdata('Error', 'Sorry ! You are Trying Invalid Way to Reset Password.');
    redirect('portal/index', 'refresh');
  } else {
    if ($data['requestinfo']->IS_USED != 0) {
     $data['content_view_page'] = 'portal/forgetPassword/errorMessage';
     $this->event_template->display($data);
   } else {
     $data['content_view_page'] = 'portal/forgetPassword/generateNewPasswordPage';
     $this->event_template->display($data);


   }
 }
}

public function generateNewPassword() {
  $random_id = $this->input->post('randomCode');
  $requestInfo = $this->utilities->findByAttribute('sa_forget_pass_request', array('REQUESTED_CODE' => $random_id));
  if ($requestInfo !== 1) {
   $password =base64_encode($this->input->post('password1'));
   $new_pass = array(
    'password' => $password,
    );
   $is_used = array(
    'IS_USED' => 1,
    'UPD_DT' => date("Y-m-d:H-i-s")
    );
   $this->utilities->updateData('users', $new_pass, array('userId' => $requestInfo->USER_ID));
   $this->utilities->updateData('sa_forget_pass_request', $is_used, array('USER_ID' => $requestInfo->USER_ID));
   redirect('ForgetPassword/resetPasswordMessages', 'refresh');
 }
}

public function resetPasswordMessages() {
  $data['content_view_page'] = 'portal/forgetPassword/resetPasswordMessages';
  $this->event_template->display($data);
}


}