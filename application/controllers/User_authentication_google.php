<?php defined('BASEPATH') OR exit('No direct script access allowed');
class User_Authentication_Google extends CI_Controller
{
    function __construct(){
		parent::__construct();
		
		//load google login library
        $this->load->library('google');
		
		//load user model
		$this->load->model('user_google');
    }
     /**
     * @param 		Google defining Registration userId 
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      Process Google registration  and login data
     */
    
    public function index(){
    	//echo "<pre>";print_r($_SESSION);exit();
		//redirect to profile page if user already logged in
		if($this->session->userdata('loggedIn') == true){
			redirect('portal/index/');

		}
		
		if(isset($_GET['code'])){
			//authenticate user
			$this->google->getAuthenticate();
			
			//get user info from google
			$gpInfo = $this->google->getUserInfo();
			
            //preparing data for database insertion
			/*$userData['oauth_provider'] = 'google';
			$userData['oauth_uid'] 		= $gpInfo['id'];
            $userData['first_name'] 	= $gpInfo['given_name'];
            $userData['last_name'] 		= $gpInfo['family_name'];
            $userData['email'] 			= $gpInfo['email'];
			$userData['gender'] 		= !empty($gpInfo['gender'])?$gpInfo['gender']:'';
			$userData['locale'] 		= !empty($gpInfo['locale'])?$gpInfo['locale']:'';
            $userData['profile_url'] 	= !empty($gpInfo['link'])?$gpInfo['link']:'';
            $userData['picture_url'] 	= !empty($gpInfo['picture'])?$gpInfo['picture']:'';*/

            $userData['login_method'] = 'G';
            $userData['gmail_id'] 	= $gpInfo['id'];
            $userData['fname'] =    $gpInfo['given_name'].' '.$gpInfo['family_name'];
            $userData['image'] 	= !empty($gpInfo['picture'])?$gpInfo['picture']:'';
            $userData['gender'] 		= !empty($gpInfo['gender'])?$gpInfo['gender']:'';
            $userData['email'] 			= $gpInfo['email'];
            $userData['agree'] = 'agree';
            $userData['type'] = 'individual';
            $userData['active'] = 1;  
			
			//insert or update user data to the database
            $userID = $this->user_google->checkUser($userData);
              //echo "<pre>";print_r($userID);exit();
            $userData['userId'] = $userID; 
		
			//store status & user info in session
			$this->session->set_userdata('loggedIn', true);
			$this->session->set_userdata('user_logged_user', $userData);
			
			//redirect to profile page
			redirect('portal/index/');
		} 
		
		//google login url
		$data['loginURL'] = $this->google->loginURL();

		
		//load google login view
		$this->ptemplate->display($data);
    }
     /**
     * @param 		Google defining Registration userId 
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      Process Google registration  and Profile view
     */
	public function profile(){
		//echo "<pre>";print_r($_SESSION);exit();
		//redirect to login page if user not logged in
		if(!$this->session->userdata('loggedIn')){
			redirect('/user_authentication_google/');
		}
		
		//get user info from session
		$data['userData'] = $this->session->userdata('user_logged_user');
		
			//load user profile view
			//$this->load->view('user_authentication_google/profile',$data);
			 $data['content_view_page'] = 'portal/portalController/index';
      		$this->ptemplate->display($data);
	}

	/**
     * @param 		Google defining Registration userId 
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      Process Google registration  logout
     */
	
	public function logout(){
		//delete login status & user info from session
		$this->session->unset_userdata('loggedIn');
		$this->session->unset_userdata('user_logged_user');
        $this->session->sess_destroy();
		
		//redirect to login page
		redirect('portal/index');
    }
}
