<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Test extends CI_Controller
{
    // Controller for Test purpose


    /**
     * @methodName myTestFunction
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    function myTestFunction()
    {

    }
}