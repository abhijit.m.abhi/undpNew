<?php

Class Opportunity_model extends CI_Model
{
    /**
     * @methodName
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    ########################################### ********* Start ************ #########################################

    public function getAllOpportunites($limit = 0, $offset = 0)
    {
        return $this->db->query("SELECT opp.*, usr.fname author, usr.userId author_id, lst.title organization_name FROM opportunity as opp
                                  LEFT JOIN listing as lst on lst.id = opp.organization_id
                                  LEFT JOIN users as usr on usr.userId = lst.userId
                                  ORDER BY opp.id desc LIMIT $limit OFFSET $offset")->result();
    }

    public function searchOpportunity($category_type, $order, $search, $limit = 0, $offset = 0)
    {
        $srch = $this->db->escape_str($search);
        $sql = '';
        $type = '';

        if (!empty($order)) {
            switch ($order) {
                case ('desc') :
                    $sql = " ORDER BY opp.created_at desc";
                    break;
                case ('asc') :
                    $sql = " ORDER BY opp.created_at asc";
                    break;
                case ('today') :
                    $sql = " AND DATE(opp.created_at) = '" . date('Y-m-d') . "' ORDER BY opp.created_at desc";
                    break;
                case ('week') :
                    $_str_before_7_days = strtotime("-7 day");
                    $before_7_days = date('Y-m-d H:i:s', $_str_before_7_days);

                    $_str_after_7_days = strtotime("+7 day");
                    $after_7_days = date('Y-m-d H:i:s', $_str_after_7_days);

                    $sql = " AND opp.created_at BETWEEN ' $before_7_days' AND '$after_7_days' ORDER BY opp.created_at desc";
                    break;
                default :
                    $sql = " ORDER BY opp.created_at desc";
            }
        } else {
            $sql = " ORDER BY opp.created_at desc ";
        }

        if (!empty($category_type)) {
            $type = " AND opp.category = '$category_type' ";
        }

        $row_count = $this->db->query("SELECT opp.*, usr.fname author, usr.userId author_id, lst.title organization_name FROM opportunity as opp 
                                  LEFT JOIN listing as lst on lst.id = opp.organization_id 
                                  LEFT JOIN users as usr on usr.userId = lst.userId 
                                    WHERE ( opp.title LIKE '%$srch%' OR lst.title LIKE '%$srch%')
                                   $type $sql")->result();

        $query = $this->db->query("SELECT opp.*, usr.fname author, usr.userId author_id, lst.title organization_name FROM opportunity as opp 
                                  LEFT JOIN listing as lst on lst.id = opp.organization_id 
                                  LEFT JOIN users as usr on usr.userId = lst.userId 
                                    WHERE ( opp.title LIKE '%$srch%' OR lst.title LIKE '%$srch%')
                                   $type $sql LIMIT $limit OFFSET $offset")->result();

        $num_rows = count($row_count);

        $this->session->set_userdata('row_count', array(
            'num_rows' => $num_rows,
        ));

        return $query;
    }

    public function increaseView($opportunity_id)
    {
        $this->db->query("UPDATE opportunity SET view_count = view_count+1 WHERE id = $opportunity_id");
    }

    public function getOpportunityData($opportunity_id)
    {
        return $this->db->query("SElECT *  FROM opportunity WHERE  id = $opportunity_id")->row();
    }

    public function getOrganizationData($organization_id)
    {
        return $this->db->query("SELECT listing.*, users.email as org_email, users.fname as author, users.userId as author_id FROM listing 
                                  LEFT JOIN users ON users.userId = listing.userId 
                                  WHERE id = $organization_id")->row();
    }

    public function organizationLogo($user_id)
    {
        return $this->db->query("SELECT image FROM users 
                                    WHERE userId = $user_id ")->row();
    }

    public function relOpportunity($opportunity_id)
    {
        return $this->db->query("SELECT op.*, org.title FROM opportunity as op 
                                  INNER JOIN listing as org ON org.id = op.organization_id 
                                  WHERE op.id != $opportunity_id 
                                  AND op.start_date >= CURDATE()")->result();
    }

    public function getInterestCount($opportunity_id)
    {
        return $this->db->query("SELECT COUNT(*) as int_count FROM opportunity_interest 
                                  WHERE opportunity_id = $opportunity_id")->row();
    }

    public function updateInterest($user_id, $opportunity_id)
    {
        $count = $this->db->query("SELECT * FROM opportunity_interest WHERE opportunity_id = $opportunity_id AND user_id = $user_id")->num_rows();

        if ($count == 0) {
            if ($this->db->query("INSERT INTO opportunity_interest (opportunity_id, user_id) VALUES ($opportunity_id, $user_id )")) ;
            {
                return array('ack' => true);
            }
        } else {
            if ($this->db->query("DELETE FROM opportunity_interest WHERE opportunity_id = $opportunity_id AND user_id = $user_id")) {
                return array('ack' => true);
            }
        }
    }

    public function updateInterest2($imo_type, $user_id, $opp_id)
    {
        $count = $this->db->query("SELECT * FROM opportunity_interest WHERE opportunity_id= $opp_id AND user_id = $user_id")->num_rows();

        if ($count == 0) {
            if ($this->db->query("INSERT INTO opportunity_interest (type, opportunity_id, user_id) VALUES ($imo_type , $opp_id , $user_id)")) {
                return array('ack' => true);
            }
        } else {
            if ($this->db->query("UPDATE opportunity_interest SET type = $imo_type WHERE opportunity_id = $opp_id  AND user_id = $user_id")) {
                return array('ack' => true);
            }
        }
    }

    public function getOppInterest($oppID)
    {
        return $this->db->query("SElECT type, count(type) interest_count FROM opportunity_interest WHERE opportunity_id='$oppID' GROUP BY type")->result();
    }

    public function getOppInterest2($oppID, $userID)
    {
        return $this->db->query("SElECT type, count(type) interest_count FROM opportunity_interest WHERE opportunity_id='$oppID' AND user_id='$userID' GROUP BY type")->result();
    }


    public function getOpportunites($user_id)
    {
        return $this->db->query("SELECT op.*,org.title as org_name FROM opportunity as op 
                                    INNER JOIN listing as org ON org.id = op.organization_id 
                                    WHERE org.userId=$user_id ORDER  BY op.id DESC")->result();
    }

    public function getOrganization($user_id)
    {
        return $this->db->query("SELECT * FROM listing
                                  WHERE userId = $user_id")->row();
    }

    public function getOrganizaionData($organization_id)
    {
        return $this->db->query("SELECT listing.*, users.email as org_email, users.fname as author, users.userId as                    author_id FROM listing
                                LEFT JOIN users ON users.userId = listing.userId
                                WHERE id = $organization_id")->row();
    }


    public function cities()
    {
        return $this->db->query("SElECT * FROM cities")->result();
    }


    public function deleteOpportunityPost($opportunity_id)
    {
        if ($this->db->delete('opportunity', array('id' => $opportunity_id))) {
            return "Deleted";
        }
    }

    public function deleteOpportunityImage($opp_id, $image_name)
    {
        $result = $this->db->query("SELECT * FROM opportunity WHERE id = $opp_id")->row();

        $imgPath = 'upload/opportunities/';

        if (strpos($result->images, $image_name) !== false) {
            $key = array($image_name . '___', '___' . $image_name, $image_name);
            $value = array('', '', '');
            $images = str_replace($key, $value, $result->images, $count);

            if ($count > 0) {
                $this->db->query("UPDATE opportunity SET images = '$images' WHERE id = $result->id");
                if (file_exists($imgPath . $image_name)) {
                    unlink($imgPath . $image_name);
                    return array('ack' => true, 'msg' => 'Image deleted');
                } else {
                    return array('ack' => false, 'msg' => 'Image not found.');
                }
            }
        }
    }

    public function previousOpportunities()
    {
        return $this->db->query("SELECT opp.*, usr.fname author, usr.userId author_id, lst.title organization_name FROM opportunity as opp 
                                  LEFT JOIN listing as lst on lst.id = opp.organization_id 
                                  LEFT JOIN users as usr on usr.userId = lst.userId 
                                  WHERE opp.start_date < CURDATE() ORDER BY start_date ASC")->result();


    }


    public function previousOpportunities2($user_id)
    {
        return $this->db->query("SELECT opp.*, usr.fname author, usr.userId author_id, lst.title organization_name FROM opportunity as opp 
                                  LEFT JOIN listing as lst on lst.id = opp.organization_id 
                                  LEFT JOIN users as usr on usr.userId = lst.userId 
                                  WHERE opp.start_date < CURDATE() AND lst.userId=$user_id ORDER BY start_date ASC")->result();


    }


    function count_items()
    {
        return $this->db->count_all('opportunity');
    }

    public function findAllOpportunityTitleList()
    {
        return $this->db->query("SELECT op.title FROM opportunity op ORDER BY id DESC ")->result();
    }


    ########################################### ********* End ************ #########################################


}