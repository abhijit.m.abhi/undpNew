<?php

Class Exam_model extends CI_Model {

    public function getAllExamSetup()
    {
        return $this->db->join('exam_grade_policy b', 'a.GR_POLICY_ID = b.GR_POLICY_ID', 'left')
                        ->join('ins_degree c', 'a.DEGREE_ID = c.DEGREE_ID', 'left')
                        ->get_where('exam_grade a')
                        ->result();
    }

    public function getAllExamSetupById($gr_id)
    {
        return $this->db->join('exam_grade_policy b', 'a.GR_POLICY_ID = b.GR_POLICY_ID', 'left')
            ->join('ins_degree c', 'a.DEGREE_ID = c.DEGREE_ID', 'left')
            ->get_where('exam_grade a', array('a.GR_ID' => $gr_id))
            ->row();
    }

}