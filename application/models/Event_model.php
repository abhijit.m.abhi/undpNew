<?php

Class Event_model extends CI_Model
{
    /**
     * @methodName
     * @access
     * @param  none
     * @author Abhijit M. Abhi <abhijit@atilimited.net>
     * @return
     */

    ########################################### ********* Start ************ #########################################

    public function upcomingEvent()
    {
        return $this->db->query("SELECT ev.*, org.title FROM events as ev
                                  INNER JOIN listing as org ON org.id = ev.organization_id
                                  WHERE ev.end_date >= CURDATE() ORDER BY start_date ASC")->result();
    }

    public function myEvents($org_id)
    {
        return $this->db->query("SELECT ev.*, org.title FROM events as ev
                                  INNER JOIN listing as org ON org.id = ev.organization_id
                                  WHERE ev.end_date >= CURDATE() AND ev.organization_id = $org_id ORDER BY start_date ASC")->result();
    }

    public function increaseView($event_id)
    {
        $this->db->query("UPDATE events SET view_count = view_count+1 WHERE id = $event_id");
    }

    public function increaseOrgView($org_id)
    {
        $this->db->query("UPDATE listing SET visitcounter = visitcounter+1 WHERE id = $org_id");
    }

    public function previousEvent()
    {
        return $this->db->query("SELECT ev.*, org.title FROM events as ev
                                  INNER JOIN listing as org ON org.id = ev.organization_id
                                  WHERE ev.end_date < CURDATE()  ORDER BY start_date ASC")->result();
    }

    public function previousEventByUser($org_id)
    {
        return $this->db->query("SELECT ev.*, org.title FROM events as ev
                                  INNER JOIN listing as org ON org.id = ev.organization_id
                                  WHERE ev.end_date < CURDATE() AND ev.organization_id = $org_id ORDER BY start_date ASC")->result();
    }

    public function eventsDate()
    {
        return $this->db->query("SELECT start_date FROM events WHERE end_date >= CURDATE() ORDER BY start_date ASC")->result();
    }

    public function upcomingEventByLimit($limit = 5)
    {
        return $this->db->query("SELECT ev.*, org.title, u.image FROM events as ev
                                  INNER JOIN listing as org ON org.id = ev.organization_id
                                  INNER JOIN users as u ON u.userId = org.userId
                                  WHERE ev.end_date >= CURDATE() ORDER BY start_date ASC")->result();
    }

    public function getInterestCount($event_id)
    {
        return $this->db->query("SELECT COUNT(*) as int_count FROM event_interest WHERE event_id = $event_id")->row();
    }

    public function getGoingCount($event_id)
    {
        return $this->db->query("SELECT COUNT(*) as int_count FROM event_going WHERE event_id = $event_id")->row();
    }

    public function filterByDate($date)
    {
        return $this->db->query("SElECT ev.*, org.title as org_name, u.image FROM events as ev
                                  INNER JOIN listing as org ON org.id = ev.organization_id
                                  INNER JOIN users as u ON u.userId = org.userId
                                  WHERE '" . $date . "' BETWEEN start_date AND end_date ORDER BY start_date ASC")->result();
    }

    public function filter($data)
    {
        $nowDate = date('Y-m-d');
        $nowTime = date('H:i:s');
        $sql = "SELECT ev.*, org.title as org_name, u.image FROM events as ev INNER JOIN listing as org ON org.id = ev.organization_id INNER JOIN users as u ON u.userId = org.userId where end_date >= '$nowDate' ";
        $search = '';
        if (!empty($data['search_text'])) {
            $search = $data['search_text'];
        }
        $srch = "%" . strip_tags(html_entity_decode($search)) . "%";
        $multipleSearches = explode(' ', strip_tags(html_entity_decode($search)));
        if (sizeof($multipleSearches) > 1) {
            $first = true;
            $sql .= " AND ( ";
            foreach ($multipleSearches as $multipleSearch) {
                $multipleSearch == str_replace(' ', '', $multipleSearch);
                if ($multipleSearch != '') {
                    if ($first) {
                        $sql .= " ev.event_name LIKE '%$multipleSearch%' OR ev.category LIKE '%$multipleSearch%' OR org.title LIKE '%$multipleSearch%' OR ev.district LIKE '%$multipleSearch%' ";
                        $first = false;
                    } else {
                        $sql .= " OR ev.event_name LIKE '%$multipleSearch%' OR ev.category LIKE '%$multipleSearch%' OR org.title LIKE '%$multipleSearch%' OR ev.district LIKE '%$multipleSearch%'  ";
                    }
                }
            }
            $sql .= " ) ";
        } else {
            $sql .= " AND (ev.event_name LIKE '$srch' OR ev.category LIKE '%$srch%' OR org.title LIKE '%$srch%' OR ev.district LIKE '%$srch%'  ) ";
        }


        $where = array();

        if (!empty($data['search_year'])) {
            $where[] = "YEAR(start_date) = " . $data['search_year'];
        }

        if (!empty($data['search_month'])) {
            if (empty($data['search_year'])) {
                $where[] = "YEAR(start_date) = " . date('Y');
            }
            $where[] = "(MONTH(start_date) = " . $data['search_month'] . " OR MONTH(end_date) = " . $data['search_month'] . ")";
        }

        if (!empty($where)) {
            $sql .= ' AND ';
            $sql .= implode(" AND ", $where);
        }

        $order_by = "ORDER BY start_date DESC";
        $sql = $sql . ' ' . $order_by;

        return $this->db->query("$sql")->result();
    }

    public function getEventDetailsById($event_id)
    {
        return $this->db->query("SElECT ev.*, org.title as org_name, u.image FROM events as ev
                                  INNER JOIN listing as org ON org.id = ev.organization_id
                                  INNER JOIN users as u ON u.userId = org.userId
                                  WHERE ev.id = $event_id")->row();
    }

    public function getOrganizaionData($organization_id)
    {
        return $this->db->query("SELECT listing.*, users.*, users.email as org_email FROM listing
                                  LEFT JOIN users ON users.userId = listing.userId
                                  WHERE id = $organization_id")->row();
    }


    public function relEvent($event_id)
    {
        return $this->db->query("SELECT ev.*, org.title FROM events as ev
                                  INNER JOIN listing as org ON org.id = ev.organization_id
                                  WHERE ev.id != " . $event_id . " 
                                   ORDER BY start_date DESC")->result();
    }

    public function relBlogByThisOrg($org_id)
    {
        return $this->db->query("SELECT bg.*, org.title FROM blog as bg
                                  INNER JOIN listing as org ON org.id = bg.organization_id
                                  WHERE bg.organization_id = $org_id ORDER BY bg.post_date DESC 
                                   ")->result();
    }


    public function relEventByThisOrg($org_id)
    {
        return $this->db->query("SELECT ev.*, org.title FROM events as ev
                                  INNER JOIN listing as org ON org.id = ev.organization_id
                                  WHERE ev.organization_id = $org_id ORDER BY ev.id DESC 
                                   ")->result();
    }

    public function updateGoing($user_id, $event_id)
    {
        $count = $this->db->query("SELECT * FROM event_going WHERE event_id = $event_id  AND user_id = $user_id ")->num_rows();

        if($count == 0)
        {
            if($this->db->query("INSERT INTO event_going (event_id, user_id) VALUES ($event_id , $user_id)"))
            {
                return array('ack' => true);
            }
        }
        else
        {
            if($this->db->query("DELETE FROM event_going WHERE event_id = $event_id  AND user_id =  $user_id"))
            {
                return array('ack' => true);
            }
        }

    }

    public function updateInterest($user_id, $event_id)
    {
        $count = $this->db->query("SELECT * FROM event_interest WHERE event_id = $event_id AND user_id = $user_id")->num_rows();

        if($count == 0)
        {
            if($this->db->query("INSERT INTO event_interest (event_id, user_id) VALUES ($event_id , $user_id)"))
            {
                return array('ack' => true);
            }
        }
        else
        {
            if($this->db->query("DELETE FROM event_interest WHERE event_id = $event_id AND user_id = $user_id"))
            {
                return array('ack' => true);
            }
        }
    }

    public function updateInterest2($imo_type, $user_id, $event_id)
    {
        $count = $this->db->query("SELECT * FROM event_interest WHERE event_id= $event_id$event_id AND user_id = $user_id")->num_rows();

        if($count == 0)
        {
            if($this->db->query("INSERT INTO event_interest (type, event_id, user_id) VALUES ($imo_type , $event_id , $user_id)"))
            {
                return array('ack' => true);
            }
        }
        else
        {
            if($this->db->query("UPDATE event_interest SET type = $imo_type WHERE event_id = $event_id  AND user_id = $user_id"))
            {
                return array('ack' => true);
            }
        }
    }

    public function updateImoInterest($imo_type, $user_id, $event_id)
    {
        $count = $this->db->query("SELECT * FROM event_interest WHERE event_id= $event_id AND user_id = $user_id")->num_rows();

        if($count == 0)
        {
            if($this->db->query("INSERT INTO event_interest (type, event_id, user_id) VALUES ($imo_type , $event_id , $user_id)"))
            {
                return array('ack' => true);
            }
        }
        else
        {
            if($this->db->query("UPDATE event_interest SET type = $imo_type WHERE event_id = $event_id  AND user_id = $user_id"))
            {
                return array('ack' => true);
            }
        }
    }

    public function getEventInterest($eventID)
    {
        return $this->db->query("SElECT type, count(type) interest_count FROM event_interest WHERE event_id='$eventID' GROUP BY type")->result();
    }

    public function deleteUpdateInterest($imo_type, $user_id, $control_id)
    {
        if($this->db->delete('event_interest', array('user_id' => $user_id, 'event_id' => $control_id, 'type' => $imo_type)))
        {
            return array('ack' => true);
        }
    }

    public function getEventInterest2($eventID, $userID)
    {
        return $this->db->query("SElECT type, count(type) interest_count FROM event_interest WHERE event_id='$eventID' AND user_id = '$userID' GROUP BY type")->result();
    }

    public function timeGenerator($lower = 0, $upper = 86400, $step = 900, $format = '')
    {
         $times = array();

        if (empty($format)) {
            $format = 'g:i A';
        }

        foreach (range($lower, $upper, $step) as $increment) {
            $increment = gmdate('H:i', $increment);

            list($hour, $minutes) = explode(':', $increment);

            $date = new DateTime($hour . ':' . $minutes);

            $times[(string)$increment] = $date->format($format);
        }

        return $times;
    }

    public function allCity()
    {
         return $this->db->query("SELECT * FROM cities")->result();
    }

    public function deleteEvent($event_id)
    {
        if($this->db->delete('events', array('id' => $event_id)))
        {
            return "Deleted";
        }
    }

    public function getEventInfoByThisId($event_id)
    {
         return $this->db->query("SELECT * FROM events ev WHERE ev.id = $event_id")->row();
    }

    public function getEventsCountByOrgID($org_id)
    {
        return $this->db->query("SELECT COUNT(*) as ev_count FROM events WHERE organization_id = $org_id")->row();
    }

    public function getBlogsCountByOrgID($org_id)
    {
        return $this->db->query("SELECT COUNT(*) as op_count FROM blog WHERE organization_id = $org_id")->row();
    }

    public function getAllImagesData($org_id)
    {
        return $this->db->query("Select * FROM organization_images WHERE organization_id = (SELECT userid FROM listing WHERE id = $org_id ) ")->result();
    }

    public function getIaoIcon($iao)
    {

        $query = $this->db->query("SELECT * FROM iao WHERE iao_name = '$iao'")->row();

        return $query->iao_icon;
    }

    public function findAllEventTitleList()
    {
        return $this->db->query("SELECT e.event_name, e.category FROM events e ORDER BY id DESC ")->result();
    }

    public function deleteEventImage($event_id, $image_name)
    {
        $result = $this->db->query("SELECT * FROM events WHERE id = $event_id")->row();

        $imgPath = 'upload/events/';

        if (strpos($result->images, $image_name) !== false) {
            $key = array($image_name . '___', '___' . $image_name, $image_name);
            $value = array('', '', '');
            $images = str_replace($key, $value, $result->images, $count);

            if ($count > 0) {
                $this->db->query("UPDATE events SET images = '$images' WHERE id = $result->id");
                if (file_exists($imgPath . $image_name)) {
                    unlink($imgPath . $image_name);
                    return array('ack' => true, 'msg' => 'Image deleted');
                } else {
                    return array('ack' => false, 'msg' => 'Image not found.');
                }
            }
        }
    }



    ########################################### ********* End ************ ###########################################


}
