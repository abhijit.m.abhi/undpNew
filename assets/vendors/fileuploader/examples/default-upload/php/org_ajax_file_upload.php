<?php
include('../../../src/class.fileuploader.php');

// initialize FileUploader
$FileUploader = new FileUploader('organization-file', array(
    'limit' => 1,
    'maxSize' => 5,
    'fileMaxSize' => 5,
    'extensions' => ['jpg', 'jpeg', 'png', 'gif', 'bmp'],
    'required' => false,
    'uploadDir' => '../../../../../../upload/users/',
    'title' => '{timestamp}--{file_name}',
    'replace' => false,
    'listInput' => true,
    'files' => null
));

// call to upload the files
$data = $FileUploader->upload();

// export to js
echo json_encode($data);
exit;