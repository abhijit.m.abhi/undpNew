<?php

/*
* Title:   MySQL Points to GeoJSON
* Notes:   Query a MySQL table or view of points with x and y columns and return the results in GeoJSON format, suitable for use in OpenLayers, Leaflet, etc.
* Author:  Bryan R. McBride, GISP
* Contact: bryanmcbride.com
* GitHub:  https://github.com/bmcbride/PHP-Database-GeoJSON
*/

# Connect to MySQL database
$conn = new PDO('mysql:host=192.168.0.201;dbname=undp_live','maruf','maruf');


# Build SQL SELECT statement including x and y columns
$sql = "SELECT listing.*, (select count(events.organization_id) from events where events.organization_id = listing.id) event_num, (select count(blog.organization_id) from blog where blog.organization_id = listing.id) post_num, users.email as org_email FROM listing INNER JOIN users ON users.userId = listing.userId and active=1 ORDER BY listing.id DESC";
$rs = $conn->query($sql);

$result = $rs->fetchAll(PDO::FETCH_ASSOC);


$data=array();
foreach($result as $key=>$value)
{
  $data[$key] = array(
      'id' =>$value['id'],
      'latitude' => $value['latitude'],
      'longitude' => $value['longitude'],
      'featured' => 1,
      'title' => $value['title'],
      'marker_image' => $value['logo'],
      'iao' => $value['iao'],
      'mobile' => $value['mobile'],
      'org_email' => $value['org_email'],
      'url' => $value['url'],
      'facebook' => $value['facebook'],
      'twitter' => $value['twitter'],
      'youtube' => $value['youtube'],
      'pinterest' => $value['pinterest'],
      'org_url' => '',//$value['org_url'],
      'ribbon' => '',//$value['org_url'],
      'rauthority' => $value['rauthority'],
      'gallery' => array(
                $value['logo']
            ),
      'description' => preg_replace('/[^A-Za-z0-9\. -]/', '',$value['description']),
      'category' => $value['category'],
      'location' => $value['location'],
      'reviews_number' => "6",
      'total_view' => $value['visitcounter'],
      'total_event' => (isset($value['event_num']))? $value['event_num']:"0",
      'total_media_post' => (isset($value['post_num']))? $value['post_num']:"0",
      'contact' => "<i class='fa fa-phone'></i>",
      'schedule' => array(
          [
              'date' => date("jS F, Y", strtotime($value['edo'])),
              'time' => "",
              'location_title' => $value['location'],
              'location_address' => ""
          ]
      ),
    );
}



  echo json_encode($data);




        // foreach ($$row as $key => $value) {
          # code...
            # code...

        //     [
         //
        //       'id' => 1,
        //       'latitude' => 40.72807182,
        //       'longitude' => -73.85735035,
        //       'featured' => 1,
        //       'title' => "Marky's Restaurant",
        //       // 'location' => "63 Birch Street",
        //       // 'city' => 1,
        //       // 'phone' => "361-492-2356",
        //       // 'email' => "hello@markys.com",
        //       // 'website' => "http://www.markys.com",
        //       // 'category' => "Restaurant",
        //       // 'rating' => "4",
        //       // 'reviews_number' => "6",
        //       // 'marker_image' => "assets/img/items/1.jpg",
         //
         //
        //       // 'gallery' => array(
        //       //     "assets/img/items/1.jpg",
        //       //     "assets/img/items/2.jpg",
        //       //     "assets/img/items/12.jpg"
        //       // ),
        //       // 'tags' => array(
        //       //     "Wi-Fi",
        //       //     "Parking",
        //       //     "TV",
        //       //     "Vegetarian"
        //       // ),
        //       // 'additional_info' => "Average price $30",
        //       // 'url' => "detail.html",
        //       // 'description' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lobortis, arcu non hendrerit imperdiet, metus odio scelerisque elit, sed lacinia odio est ac felis. Nam ullamcorper hendrerit ullamcorper. Praesent quis arcu quis leo posuere ornare eu in purus. Nulla ornare rutrum condimentum. Praesent eu pulvinar velit. Quisque non finibus purus, eu auctor ipsum.",
        //       // 'reviews' => array(
        //       //     [
        //       //         'author_name' => "Jane Doe",
        //       //         'author_image' => "assets/img/person-01.jpg",
        //       //         'date' => '09.05.2016',
        //       //         'rating' => 4,
        //       //         'review_text' => "Morbi varius orci in rhoncus posuere. Sed cursus urna ut sem rhoncus lacinia. Praesentac velit dignissim, mollis purus quis, sollicitudin eros"
        //       //     ],
        //       //     [
        //       //         'author_name' => "Norma Brown",
        //       //         'author_image' => "assets/img/person-02.jpg",
        //       //         'date' => '09.05.2016',
        //       //         'rating' => 4,
        //       //         'review_text' => "Donec nec tristique sapien. Aliquam ante felis, sagittis sodales diam sollicitudin, dapibus semper turpis"
        //       //     ]
        //       // ),
        //       // 'opening_hours' => array(
        //       //     "08:00am - 11:00pm",            ),
        //  ],
