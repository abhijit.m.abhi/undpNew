<?php
include("../../dbInformation.php");
$data = [];
try{
    $con=new PDO("mysql:host={$dbHost};dbname={$dbName}",$dbUser,$dbPass);
    $con->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
}catch(PDOException $e)
{
    echo $e->getMessage();
}

try{
    $stm=$con->prepare("SELECT listing.*, users.email as org_email FROM listing INNER JOIN users ON users.userId = listing.userId and active=1 ORDER BY listing.id DESC");
    $result=$stm->execute();
    while($row=$stm->fetch(PDO::FETCH_ASSOC))
    {
        $data[]=$row;

    }


}catch(PDOException $e)
{
    echo $e->getMessage();
}
// ---------------------------------------------------------------------------------------------------------------------
// Here comes your script for loading from the database.
// ---------------------------------------------------------------------------------------------------------------------

// Remove this example in your live site -------------------------------------------------------------------------------


$newData = [];

if( @$_POST["category"] == "" && @$_POST["district"] == "" && @$_REQUEST['iao']=="" && @$_POST['title']=="" ){
    echo json_encode($data);
}else {

    $searchQuery = [];
    if(isset($_POST['category']) && $_POST['category'] != '') $searchQuery['category'] = $_POST['category'];
    if(isset($_POST['district']) && $_POST['district'] != '') $searchQuery['district'] = $_POST['district'];
    if(isset($_REQUEST['iao']) && $_REQUEST['iao'] != '') $searchQuery['iao'] = base64_decode($_REQUEST['iao']);
    if(isset($_POST['title']) && $_POST['title'] != '') $searchQuery['title'] = $_POST['title'];

    //print_r($searchQuery);

    foreach($data as $dKey => $d) {
        $pass = true;
        if(isset($searchQuery['category'])) {

            if(strcasecmp($searchQuery['category'], $d['category']) != 0) $pass = false;
        }

        if(isset($searchQuery['district'])) {

            if(strcasecmp($searchQuery['district'], $d['district']) != 0) $pass = false;
        }

        if(isset($searchQuery['iao'])) {


            $ioaArr = explode(",", $d['iao']);

            if(count($ioaArr)==0) {

                $pass = false;
            }else {


                if(in_array($searchQuery['iao'], $ioaArr)) { }
				else{
					$pass = false;
				}
            }

        }

        if(isset($searchQuery['title'])) {
            $searchQueryTitles = explode(' ', $searchQuery['title']);
            if(sizeof($searchQueryTitles) > 1){
                $isTitle = false;
                $isDist = false;
                $isCat = false;
                $isIao = false;
                foreach ($searchQueryTitles as $searchQueryTitle){
                    if((strpos(strtolower($d['title']), trim(strtolower($searchQueryTitle))) === false)){}else {$isTitle = true;}
                    if((strpos(strtolower($d['district']), trim(strtolower($searchQueryTitle))) === false)){}else {$isDist = true;}
                    if((strpos(strtolower($d['category']), trim(strtolower($searchQueryTitle))) === false)){}else {$isCat = true;}
                    if((strpos(strtolower($d['iao']), trim(strtolower($searchQueryTitle))) === false)){}else {$isIao = true;}
                }
                if($isTitle == false && $isDist == false && $isCat == false && $isIao == false){
                    $pass = false;
                }
            }
            else{
                $isTitle = false;
                $isDist = false;
                $isCat = false;
                $isIao = false;
                if((strpos(strtolower($d['title']), trim(strtolower($searchQuery['title']))) === false)){}else {$isTitle = true;}
                if((strpos(strtolower($d['district']), trim(strtolower($searchQuery['title']))) === false)){}else {$isDist = true;}
                if((strpos(strtolower($d['category']), trim(strtolower($searchQuery['title']))) === false)){}else {$isCat = true;}
                if((strpos(strtolower($d['iao']), trim(strtolower($searchQuery['title']))) === false)){}else {$isIao = true;}
                if($isTitle == false && $isDist == false && $isCat == false && $isIao == false){
                    $pass = false;
                }
            }
        }

        if($pass)
            $newData[] = $d;
    }

    echo json_encode($newData);

}

// End of example ------------------------------------------------------------------------------------------------------
