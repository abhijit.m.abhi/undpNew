<style media="screen">
   .owl-item {
      padding-top: 30px;
    }
   .owl-item img{
    width: 100% !important;
    height: 300px !important;
  }
</style>

<?php


class listing
{

  public function visitCounter($id,$qnt)
    {
        try {
            $conn = new PDO('mysql:host=192.168.0.201;dbname=undp_live','maruf','maruf');
            # Build SQL SELECT statement including x and y columns
            $count = $qnt+1;

            $sql1 = "Update listing SET visitcounter='$count' WHERE id='$id'";
            $conn->query($sql1);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }


  public function getIaoIcon($iao)
  {

      try {
        $conn = new PDO('mysql:host=192.168.0.201;dbname=undp_live','maruf','maruf');
        # Build SQL SELECT statement including x and y columns
        $sql = "SELECT * FROM iao WHERE iao_name='$iao'";
        $rs = $conn->query($sql);
        $result = $rs->fetchAll(PDO::FETCH_ASSOC);
        return $result[0]['iao_icon'];

      } catch (PDOException $e) {
          echo $e->getMessage();
      }
    }

    public function getEventsByOrgID($org_id)
    {

        $events = array();
        try {
            $conn = new PDO('mysql:host=192.168.0.201;dbname=undp_live','maruf','maruf');
            $sql = "SElECT ev.*, org.title as org_name FROM events as ev INNER JOIN listing as org ON org.id = ev.organization_id WHERE ev.organization_id='$org_id' AND end_date >= CURDATE() ORDER BY start_date DESC LIMIT 3";
            $rs = $conn->query($sql);

            while ($data = $rs->fetch(PDO::FETCH_ASSOC)) {
                $events[] = $data;
            }

            return $events;
        } catch (PDOException $e) {
            echo $e->getMessage();
            exit;
        }
    }


    public function getBlogsByOrgID($org_id)
    {
        $blog = array();
        try {
          $conn = new PDO('mysql:host=192.168.0.201;dbname=undp_live','maruf','maruf');
          $sql = "SElECT bg.*, org.title as org_name, ut.name url_type, ut.value url_data  FROM blog as bg LEFT JOIN listing as org ON org.id = bg.organization_id LEFT JOIN url_type as ut ON ut.id = bg.url_type_id WHERE bg.organization_id='$org_id' ORDER BY bg.post_date DESC LIMIT 3";
          $rs = $conn->query($sql);
            while ($data = $stm->fetch(PDO::FETCH_ASSOC)) {
                $blog[] = $data;
            }
//            $result=$stm->fetch(PDO::FETCH_ASSOC);
            return $blog;
        } catch (PDOException $e) {
            echo $e->getMessage();
            exit;
        }
    }


  }

$listing = new listing;

$currentLocation = "";

// ---------------------------------------------------------------------------------------------------------------------
// Here comes your script for loading from the database.
// ---------------------------------------------------------------------------------------------------------------------

// Remove this example in your live site and replace it with a connection to database //////////////////////////////////

ob_start();
include 'data.php';
ob_end_clean();

for( $i=0; $i < count($data); $i++){
    if( $data[$i]['id'] == $_POST['id'] ){
        $currentLocation = $data[$i]; // Loaded data must be stored in the "$currentLocation" variable
    }
}

// End of example //////////////////////////////////////////////////////////////////////////////////////////////////////

// Modal HTML code

$latitude = "";
$longitude = "";
$address = "";

if( !empty($currentLocation['latitude']) ){
    $latitude = $currentLocation['latitude'];
}

if( !empty($currentLocation['longitude']) ){
    $longitude = $currentLocation['longitude'];
}

if( !empty($currentLocation['address']) ){
    $address = $currentLocation['address'];
}

$id_details = $currentLocation['id'];

echo

'<div class="modal-item-detail modal-dialog" role="document" data-latitude="'. $latitude .'" data-longitude="'. $longitude .'" data-address="'. $address .'">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="section-title">
                      <img src="'.$currentLocation['marker_image'].'" height="120px" width="120px">
                <h2 '.$listing->visitCounter($id_details, $currentLocation['total_view']).' style="font-size:22px; color: #012f49">'. $currentLocation['title'] .'</h2>';

                // Ribbon ------------------------------------------------------------------------------------------

                if( !isset($currentLocation['ribbon']) ){
                    echo
                        '<figure class="ribbon">'. $currentLocation['ribbon'] .'</figure>';
                }

                // Rating ------------------------------------------------------------------------------------------


                echo '<div class="info_items list-inline">';
                if( isset($currentLocation['total_view']) ){

                        echo '&nbsp;&nbsp;<div class="info_item">
                            <i class="fa fa-eye"></i>
                            <span>'.$currentLocation['total_view'].'</span>
                        </div>';
                      }


                      if( isset($currentLocation['total_event']) ){
                          echo '<div class="info_items list-inline">
                              <div class="info_item">
                                  Events :
                                  <span>'.$currentLocation['total_event'].'</span>
                              </div>';
                            }

                      if( isset($currentLocation['total_media_post']) ){
                          echo '<div class="info_items list-inline">
                              <div class="info_item">
                                  Media Post :
                                  <span>'.$currentLocation['total_media_post'].'</span>
                              </div>';
                            }
                  echo '</div>';








                echo
                '<div class="controls-more">
                    <ul>
                        <li><a href="#">Add to favorites</a></li>
                        <li><a href="#">Add to watchlist</a></li>
                    </ul>
                </div>
                <!--end controls-more-->
            </div>
            <!--end section-title-->
        </div>
        <!--end modal-header-->
        <div class="modal-body">
            <div class="left">';

                // Gallery -----------------------------------------------------------------------------------------

                if( !empty($currentLocation['gallery']) ){
                    $gallery = "";
                    for($i=0; $i < count($currentLocation['gallery']); $i++){
                        $gallery .= '<img src="'. $currentLocation['gallery'][$i] .'" alt="">';
                    }
                    echo
                    '<div class="gallery owl-carousel" data-owl-nav="1" data-owl-dots="0">'. $gallery .'</div>
                    <!--end gallery-->';
                }

                echo
                '<div class="map" id="map-modal"></div>
                <!--end map-->

                <section>
                <h3>Contact</h3>';
                // Contact -----------------------------------------------------------------------------------------

                if( !empty($currentLocation['location']) ){
                    echo
                        '<h5><i class="fa fa-map-marker"></i>'. $currentLocation['location'] .'</h5>';
                }

                // Phone -------------------------------------------------------------------------------------------

                if( !empty($currentLocation['phone']) ){
                    echo
                        '<h5><i class="fa fa-phone"></i>'. $currentLocation['phone'] .'</h5>';
                }

                // Email -------------------------------------------------------------------------------------------

                if( !empty($currentLocation['email']) ){
                    echo
                        '<h5><i class="fa fa-envelope"></i>'. $currentLocation['email'] .'</h5>';
                }

                echo
                '</section>
                <section>
                    <h3>Social Share</h3>
                    <div class="social-share"></div>
                </section>
            </div>
            <!--end left -->
            <div class="right">
                <section>
                    <h3>About</h3>
                    <div class="read-more"><p>'. $currentLocation['description'] .'</p></div>
                </section>
                <!--end about-->';


                echo
                  '<section>
                    <h3>INTEREST AREA OF THE ORGANIZATION</h3>
                    <ul class="tags">';
                  if( !empty($currentLocation['iao']) ) {
                      $iao = explode(",", $currentLocation['iao']);

                      foreach ($iao as $v) {
                          echo '<li style="width:24px; text-align:center;" title="'.$v.'"><span class="fa ' . $listing->getIaoIcon($v) . '"></span></li>
                                          ';
                      }
                  }

                  echo '</ul>
                    </section>';


                    echo
                        '<section>
                                <h3>REGISTRATION / AFFILIATION</h3>
                                <ul class="tags">';
                        if( !empty($currentLocation['rauthority']) ) {
                            $iao = explode(",", $currentLocation['rauthority']);

                            foreach ($iao as $v) {
                                echo '<li style="background-color: #fff;color:red;border:1px solid red; font-size: 13px; padding: 0px 2px;">'.$v.'</li>
                                                                    ';
                            }
                        }

                        echo '</ul>
                    </section>';


                    echo
                    '<section>
                          <h3>LATEST ACTIVITY</h3>

                     </section>';

                // Tags ----------------------------------------------------------------------------------------------------------------

                if( !empty($currentLocation['tags']) ){
                    $tags = "";
                    for($i=0; $i < count($currentLocation['tags']); $i++){
                        $tags .= '<li>'. $currentLocation['tags'][$i] .'</li>';
                    }
                    echo
                        '<section>
                            <h3>Features</h3>
                            <ul class="tags">'.  $tags .'</ul>
                    </section>
                    <!--end tags-->';
                }

                // Today Menu --------------------------------------------------------------------------------------

                if( !empty($currentLocation['today_menu']) ){
                    echo
                    '<section>
                        <h3>Today menu</h3>';
                    for($i=0; $i < count($currentLocation['today_menu']); $i++){
                        echo
                            '<ul class="list-unstyled list-descriptive icon">
                                <li>
                                    <i class="fa fa-cutlery"></i>
                                    <div class="description">
                                        <strong>'. $currentLocation['today_menu'][$i]['meal_type'] .'</strong>
                                        <p>'. $currentLocation['today_menu'][$i]['meal'] .'</p>
                                    </div>
                                </li>
                            </ul>
                            <!--end list-descriptive-->';
                    }
                    echo
                    '</section>
                    <!--end today-menu-->';
                }

                // Schedule ----------------------------------------------------------------------------------------

                // if( !empty($currentLocation['schedule']) ){
                //     echo
                //     '<section>
                //         <h3>Schedule</h3>';
                //     for($i=0; $i < count($currentLocation['schedule']); $i++){
                //         echo
                //             '<ul class="list-unstyled list-schedule">
                //                 <li>
                //                     <div class="left">
                //                         <strong class="promoted">'. $currentLocation['schedule'][$i]['date'] .'</strong>
                //                         <figure>'. $currentLocation['schedule'][$i]['time'] .'</figure>
                //                     </div>
                //                     <div class="right">
                //                         <strong>'. $currentLocation['schedule'][$i]['location_title'] .'</strong>
                //                         <figure>'. $currentLocation['schedule'][$i]['location_address'] .'</figure>
                //                     </div>
                //                 </li>
                //             </ul>
                //             <!--end list-schedule-->';
                //     }
                //     echo
                //     '</section>
                //     <!--end schedule-->';
                // }

          // events
          $orgId = (isset($currentLocation['id']))?$currentLocation['id']:0;
        //  $upcommingBlogs = $listing->getBlogsByOrgID($orgId);
          $upcommingEvent = $listing->getEventsByOrgID($orgId);

            ?>
                <section class="org_profile_section" style="border: 1px solid red;">
                  <h3 style="margin: 9px 0px 5px;">Event Calendar</h3>

                  <div class="calen_items">
                      <div class="row" style="margin-left: -12px;">
                          <?php if (!empty($upcommingEvent)): ?>
                              <?php foreach ($upcommingEvent as $event): ?>
                                  <div class="col-md-4" style="padding: 0 5px;">
                                      <a href="event_view.php?event_id=<?php echo $event['id'] ?>"><div class="thumbnail calen_item">
                                              <img src="upload/events/<?php echo $event['images']; ?>" width="100%" height="100px">
                                              <h6 class="text-capitalize"><?php echo $event['event_name'] ?></h6>
                                          </div></a>
                                  </div>
                              <?php endforeach;?>
                          <?php else: ?>
                              <div class="col-md-12">
                                  <p>No Event Found</p>
                              </div>
                          <?php endif;?>
                      </div>
                      <a href="org-event-calender.php" class="e_view" style="margin-left: 116px; border: 1px solid; padding: 0 4px;">View more</a>
                  </div>
              </section>


              <!-- media blog -->
              <section class="org_profile_section" style="border: 1px solid red;">

                        <h3 style="margin: 9px 0px 5px;">Media Blog</h3>
                        <div class="calen_items">
                            <div class="row" style="margin-left: -12px;">
                                <?php if (!empty($upcommingBlogs)): ?>
                                    <?php foreach ($upcommingBlogs as $blog):
                    	$images = explode('___', $blog['images']);
                    	?>
                    	                    <div class="col-md-4" style="padding: 0 5px;">
                    	                        <a href="blog_view.php?opportunity_id=<?php echo $blog['id'] ?>"><div class="thumbnail calen_item">
                    	                                <?php if ($blog['type'] == 'image') {?>
                    	                                    <img src="upload/blogs/<?php echo $images[0]; ?>" width="100%" height="100px" alt="<?php echo $blog["title"]; ?>">
                    	                                <?php } else {
                    		$url_link = '';
                    		if ($blog['url_type'] == 'Youtube') {
                    			$url2Embed = str_replace("watch?v=", "embed/", $blog['urls']);
                    			$url_link = str_replace("[URL]", $url2Embed, $blog['url_data']);
                    			$url_link = str_replace('height="250"', 'height="100"', $url_link);
                    		} elseif ($blog['url_type'] == 'SoundCloud') {
                    		$url_link = str_replace("[URL]", $blog['urls'], $blog['url_data']);
                    		$url_link = str_replace('height="258"', 'height="100"', $url_link);
                    	} elseif ($blog['url_type'] == 'DailyMotion') {
                    		$url2Embed = str_replace("https://www.dailymotion.com/video", "https://www.dailymotion.com/embed/video", $blog['urls']);
                    		$url_link = str_replace("[URL]", $url2Embed, $blog['url_data']);
                    		$url_link = str_replace('height="250"', 'height="100"', $url_link);
                    	} else {
                    		echo '<div width="100%" height="258" style="margin-bottom: 0px;"><h1>Something Missing</h1></div>';
                    	}
                    	echo $url_link;
                    }?>
                                <h6 class="text-capitalize"><?php echo $blog['title'] ?></h6>
                            </div></a>
                    </div>
                            <?php endforeach;?>
                        <?php else: ?>
                            <div class="col-md-12">
                                <p>No Media Blog Found</p>
                            </div>
                        <?php endif;?>
                    </div>
                    <a href="media_blogs.php" class="e_view" style="margin-left: 116px; border: 1px solid; padding: 0 4px;">View more</a>
                        </div>
                    </section>
              <?php

                // Video -------------------------------------------------------------------------------------------

                if( !empty($currentLocation['video']) ){
                    echo
                    '<section>
                        <h3>Video presentation</h3>
                        <div class="video">'. $currentLocation['video'] .'</div>
                    </section>
                    <!--end video-->';
                }

                // Description list --------------------------------------------------------------------------------

                if( !empty($currentLocation['description_list']) ){
                    echo
                    '<section>
                        <h3>Listing Details</h3>';
                        for($i=0; $i < count($currentLocation['description_list']); $i++){
                            echo
                                '<dl>
                                    <dt>'. $currentLocation['description_list'][$i]['title'] .'</dt>
                                    <dd>'. $currentLocation['description_list'][$i]['value'] .'</dd>
                                </dl>
                                <!--end property-details-->';
                        }
                    echo
                    '</section>
                    <!--end description-list-->';
                }

                // Reviews -----------------------------------------------------------------------------------------

                if( !empty($currentLocation['reviews']) ){
                    echo
                    '<section>
                        <h3>Latest reviews</h3>';
                    for($i=0; $i < 2; $i++){
                        echo
                            '<div class="review">
                                <div class="image">
                                    <div class="bg-transfer" style="background-image: url('. $currentLocation['reviews'][$i]['author_image'] .')"></div>
                                </div>
                                <div class="description">
                                    <figure>
                                        <div class="rating-passive" data-rating="'. $currentLocation['reviews'][$i]['rating'] .'">
                                            <span class="stars"></span>
                                        </div>
                                        <span class="date">'. $currentLocation['reviews'][$i]['date'] .'</span>
                                    </figure>
                                    <p>'. $currentLocation['reviews'][$i]['review_text'] .'</p>
                                </div>
                            </div>
                            <!--end review-->';
                    }
                    echo
                    '</section>
                    <!--end reviews-->';
                }
            echo
            '</div>
            <!--end right-->
        </div>
        <!--end modal-body-->
    </div>
    <!--end modal-content-->
</div>
<!--end modal-dialog-->
';
