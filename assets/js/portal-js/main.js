(function ($) {
    "use strict";

    jQuery(document).ready(function($){
        /*
         * ----------------------------------------------------------------------------------------
         *  modal
         * ----------------------------------------------------------------------------------------
         */
        // wire up the OK button to dismiss the modal when shown

        // $(document).on('show.bs.modal', function (event) {
        //     if (!event.relatedTarget) {
        //         $('.modal').not(event.target).modal('hide');
        //     };
        //     if ($(event.relatedTarget).parents('.modal').length > 0) {
        //         $(event.relatedTarget).parents('.modal').modal('hide');
        //     };
        // });

        // $(document).on('shown.bs.modal', function (event) {
        //     if ($('body').hasClass('modal-open') == false) {
        //         $('body').addClass('modal-open');
        //     };
        // });


        // .modal-backdrop classes

        // $(".modal-transparent").on('show.bs.modal', function () {
        //   setTimeout( function() {
        //     $(".modal-backdrop").addClass("modal-backdrop-transparent");
        //   }, 0);
        // });
        // $(".modal-transparent").on('hidden.bs.modal', function () {
        //   $(".modal-backdrop").addClass("modal-backdrop-transparent");
        // });

        // $(".modal-fullscreen").on('show.bs.modal', function () {
        //   setTimeout( function() {
        //     $(".modal-backdrop").addClass("modal-backdrop-fullscreen");
        //   }, 0);
        // });
        // $(".modal-fullscreen").on('hidden.bs.modal', function () {
        //   $(".modal-backdrop").addClass("modal-backdrop-fullscreen");
        // });
        /*
         * ----------------------------------------------------------------------------------------
         *  Form
         * ----------------------------------------------------------------------------------------
         */

        $(document).on('change', '.btn-file :file', function() {
            var input = $(this),
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [label]);
        });

        $('.btn-file :file').on('fileselect', function(event, label) {

            var input = $(this).parents('.input-group').find(':text'),
                log = label;

            if( input.length ) {
                input.val(log);
            } else {
                if( log ) alert(log);
            }

        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#img-upload').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgInp").change(function(){
            readURL(this);
        });

    });


    jQuery(window).load(function(){
        //google code
        function onSignIn(googleUser) {
            // Useful data for your client-side scripts:
            var profile = googleUser.getBasicProfile();
            console.log("ID: " + profile.getId()); // Don't send this directly to your server!
            console.log('Full Name: ' + profile.getName());
            console.log('Given Name: ' + profile.getGivenName());
            console.log('Family Name: ' + profile.getFamilyName());
            console.log("Image URL: " + profile.getImageUrl());
            console.log("Email: " + profile.getEmail());

            // The ID token you need to pass to your backend:
            var id_token = googleUser.getAuthResponse().id_token;
            console.log("ID Token: " + id_token);
        };
        // gapi.auth2.init();
        // if (auth2.isSignedIn.get()) {
        //     var profile = auth2.currentUser.get().getBasicProfile();
        //     console.log('ID: ' + profile.getId());
        //     console.log('Full Name: ' + profile.getName());
        //     console.log('Given Name: ' + profile.getGivenName());
        //     console.log('Family Name: ' + profile.getFamilyName());
        //     console.log('Image URL: ' + profile.getImageUrl());
        //     console.log('Email: ' + profile.getEmail());
        // }


    });


}(jQuery));