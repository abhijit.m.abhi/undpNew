<style media="screen">
ul {
list-style: none;
}

ul li {
  border-radius:4px !important;
}

ul li:before {
content: '' !important;
display: none;
}
.frame {
    height: 90px;
    display: table;
    width: 100%;
    text-align: center;
    padding: 0px;
}
.helper {
    display: table-cell;
    text-align: center;
    vertical-align: middle;
    height: 88px;
}
.frame>span>img {
    vertical-align: middle;
    display: inline-block !important;
    width: auto !important;
    max-height: 100%;
    max-width: 100%;
}
</style>


<?php


class listing
{

  public function getIaoIcon($iao)
  {
      try {
        $conn = new PDO('mysql:host=192.168.0.201;dbname=undp_live','maruf','maruf');
        # Build SQL SELECT statement including x and y columns
        $sql = "SELECT * FROM iao WHERE iao_name='$iao'";
        $rs = $conn->query($sql);
        $result = $rs->fetchAll(PDO::FETCH_ASSOC);
        return $result[0]['iao_icon'];

      } catch (PDOException $e) {
          echo $e->getMessage();
      }
    }
  }

$listing = new listing;

// ---------------------------------------------------------------------------------------------------------------------
// Here comes your script for loading from the database.
// ---------------------------------------------------------------------------------------------------------------------
// Remove this example in your live site -------------------------------------------------------------------------------


ob_start();
include '../../assets/external/data.php';





// This code can be used if you want to load data from JSON file

//$file_contents = file_get_contents("items.json");
//$data = json_decode($file_contents, true);

ob_end_clean();

// End of example ------------------------------------------------------------------------------------------------------

if( !empty($_POST['markers']) ){

    for( $i=0; $i < count($data); $i++){
        for( $e=0; $e < count($_POST['markers']); $e++){
            if( $data[$i]['id'] == $_POST['markers'][$e] ){
                echo
                '<div class="result-item" data-id="'. $data[$i]['id'] .'">';

                    // Ribbon ------------------------------------------------------------------------------------------

                    if( !empty($data[$i]['ribbon']) ){
                        echo
                            '<figure class="ribbon">'. $data[$i]['ribbon'] .'</figure>';
                    }

                    echo
                    '<a href="'. $data[$i]['url'] .'">';

                    // Title -------------------------------------------------------------------------------------------

                    if( !empty($data[$i]['title']) ){
                        echo
                            '<h3>'. $data[$i]['title'] .'</h3>';
                    }

                    echo
                        '<div class="result-item-detail">';

                            // Image thumbnail -------------------------------------------------------------------------

                            if( !empty($data[$i]['marker_image']) ){
                                echo
                                '<div class="image frame" style="">
								
								<span class="helper"><img src="'.$data[$i]['marker_image'].'" style=""></span>';
                                    if( !empty($data[$i]['additional_info']) ){
                                        echo
                                        '<figure>'. $data[$i]['additional_info'] .'</figure>';
                                    }

                                    // Price ---------------------------------------------------------------------------

                                    if( !empty($data[$i]['price']) ){
                                        echo
                                            '<div class="price">'. $data[$i]['price'] .'</div>';
                                    }
                                echo
                                '</div>';
                            }
                            else {
                                echo
                                '<div class="image" style="background-image: url(assets/img/items/default.png)">';
                                    if( !empty($data[$i]['additional_info']) ){
                                        echo
                                        '<figure>'. $data[$i]['additional_info'] .'</figure>';
                                    }

                                    // Price ---------------------------------------------------------------------------

                                    if( !empty($data[$i]['price']) ){
                                        echo
                                            '<figure class="price">'. $data[$i]['price'] .'</figure>';
                                    }
                                echo
                                '</div>';
                            }

                            echo
                            '<div class="description">';
                                if( !empty($data[$i]['location']) ){
                                    echo
                                        '<h5><i class="fa fa-map-marker"></i>'. $data[$i]['location'] .'</h5>';
                                }

                                // Rating ------------------------------------------------------------------------------

                                if( !empty($data[$i]['rating']) ){
                                    echo
                                        '<div class="rating-passive"data-rating="'. $data[$i]['rating'] .'">
                                            <span class="stars"></span>
                                            <span class="reviews">'. $data[$i]['reviews_number'] .'</span>
                                        </div>';
                                }

                                // Category ----------------------------------------------------------------------------

                                if( !empty($data[$i]['category']) ){
                                    echo
                                        '<div class="label label-default">'. $data[$i]['category'] .'</div>';
                                }
                                echo '<br/>';
                                echo '<br/>';


                                echo '<div style="display: inline-flex;align-items: center" class="info_items list-inline">
                                        <div style="background-color:#E43533;margin: 2px;padding:3px;color:#fff;border-radius:5px;" title="Views">
                                            <i class="fa fa-eye"></i>
                                            <span>'.$data[$i]['total_view'].'</span>
                                        </div>
                                        <div style="background-color:#E43533;margin: 2px;padding:3px;color:#fff;border-radius:5px;" class="info_item" title="Event">
                                            <i class="fa fa-calendar"></i>
                                            <span>'.$data[$i]['total_event'].'</span>
                                        </div>
                                        <div style="background-color:#E43533;margin: 2px;padding:3px;color:#fff;border-radius:5px;" class="info_item" title="Media Blog">
                                            <i class="fa fa-film"></i>
                                            <span>'.$data[$i]['total_media_post'].'</span>
                                        </div>
                                    </div>';



                                if (!empty($data[$i]['iao'])) {
                          					$iao = explode(",", $data[$i]['iao']);
                          					echo '<ul class="tags">';
                                    foreach ($iao as $v) {
                                        echo '<li style="width:24px; text-align:center;" title="'.$v.'"><span class="fa ' . $listing->getIaoIcon($v) . '"></span></li>
                                                        ';
                                    }
                          					echo '</ul>';
                          				}

                                // Description -------------------------------------------------------------------------

                                if( !empty($data[$i]['description']) ){
                                    echo
                                        '<p>'. $data[$i]['description'] .'</p>';
                                }
                            echo
                            '</div>
                        </div>
                    </a>
                    <div class="controls-more">
                        <ul>
                            <li><a href="#" class="add-to-favorites">Add to favorites</a></li>
                            <li><a href="#" class="add-to-watchlist">Add to watchlist</a></li>
                        </ul>
                    </div>
                </div>';

            }
        }
    }

}
